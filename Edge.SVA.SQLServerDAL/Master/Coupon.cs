﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Coupon
    /// </summary>
    public partial class Coupon : ICoupon
    {
        public Coupon()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CouponNumber)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Coupon");
            strSql.Append(" where CouponNumber=@CouponNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512)			};
            parameters[0].Value = CouponNumber;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.Coupon model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Coupon(");
            strSql.Append("CouponNumber,CouponTypeID,CouponIssueDate,CouponExpiryDate,CouponActiveDate,StoreID,BatchCouponID,Status,CouponPassword,CardNumber,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponAmount,RedeemStoreID,PickupFlag,LocateStoreID,InitCardNumber,StockStatus)");
            strSql.Append(" values (");
            strSql.Append("@CouponNumber,@CouponTypeID,@CouponIssueDate,@CouponExpiryDate,@CouponActiveDate,@StoreID,@BatchCouponID,@Status,@CouponPassword,@CardNumber,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@CouponAmount,@RedeemStoreID,@PickupFlag,@LocateStoreID,@InitCardNumber,@StockStatus)");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponIssueDate", SqlDbType.DateTime),
					new SqlParameter("@CouponExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CouponActiveDate", SqlDbType.DateTime),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CouponPassword", SqlDbType.VarChar,512),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponAmount", SqlDbType.Money,8),
					new SqlParameter("@RedeemStoreID", SqlDbType.Int,4),
					new SqlParameter("@PickupFlag", SqlDbType.Int,4),
					new SqlParameter("@LocateStoreID", SqlDbType.Int,4),
					new SqlParameter("@InitCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@StockStatus", SqlDbType.Int,4)};
            parameters[0].Value = model.CouponNumber;
            parameters[1].Value = model.CouponTypeID;
            parameters[2].Value = model.CouponIssueDate;
            parameters[3].Value = model.CouponExpiryDate;
            parameters[4].Value = model.CouponActiveDate;
            parameters[5].Value = model.StoreID;
            parameters[6].Value = model.BatchCouponID;
            parameters[7].Value = model.Status;
            parameters[8].Value = model.CouponPassword;
            parameters[9].Value = model.CardNumber;
            parameters[10].Value = model.CreatedOn;
            parameters[11].Value = model.UpdatedOn;
            parameters[12].Value = model.CreatedBy;
            parameters[13].Value = model.UpdatedBy;
            parameters[14].Value = model.CouponAmount;
            parameters[15].Value = model.RedeemStoreID;
            parameters[16].Value = model.PickupFlag;
            parameters[17].Value = model.LocateStoreID;
            parameters[18].Value = model.InitCardNumber;
            parameters[19].Value = model.StockStatus;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Coupon model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Coupon set ");
            strSql.Append("CouponTypeID=@CouponTypeID,");
            strSql.Append("CouponIssueDate=@CouponIssueDate,");
            strSql.Append("CouponExpiryDate=@CouponExpiryDate,");
            strSql.Append("CouponActiveDate=@CouponActiveDate,");
            strSql.Append("StoreID=@StoreID,");
            strSql.Append("BatchCouponID=@BatchCouponID,");
            strSql.Append("Status=@Status,");
            strSql.Append("CouponPassword=@CouponPassword,");
            strSql.Append("CardNumber=@CardNumber,");
            strSql.Append("UpdatedOn=@UpdatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
            strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("CouponAmount=@CouponAmount,");
            strSql.Append("RedeemStoreID=@RedeemStoreID,");
            strSql.Append("PickupFlag=@PickupFlag,");
            strSql.Append("LocateStoreID=@LocateStoreID,");
            strSql.Append("InitCardNumber=@InitCardNumber,");
            strSql.Append("StockStatus=@StockStatus");
            strSql.Append(" where CouponNumber=@CouponNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponIssueDate", SqlDbType.DateTime),
					new SqlParameter("@CouponExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@CouponActiveDate", SqlDbType.DateTime),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CouponPassword", SqlDbType.VarChar,512),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponAmount", SqlDbType.Money,8),
					new SqlParameter("@RedeemStoreID", SqlDbType.Int,4),
					new SqlParameter("@PickupFlag", SqlDbType.Int,4),
					new SqlParameter("@LocateStoreID", SqlDbType.Int,4),
					new SqlParameter("@InitCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@StockStatus", SqlDbType.Int,4),
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime)};
            parameters[0].Value = model.CouponTypeID;
            parameters[1].Value = model.CouponIssueDate;
            parameters[2].Value = model.CouponExpiryDate;
            parameters[3].Value = model.CouponActiveDate;
            parameters[4].Value = model.StoreID;
            parameters[5].Value = model.BatchCouponID;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.CouponPassword;
            parameters[8].Value = model.CardNumber;
            parameters[9].Value = model.UpdatedOn;
            parameters[10].Value = model.CreatedBy;
            parameters[11].Value = model.UpdatedBy;
            parameters[12].Value = model.CouponAmount;
            parameters[13].Value = model.RedeemStoreID;
            parameters[14].Value = model.PickupFlag;
            parameters[15].Value = model.LocateStoreID;
            parameters[16].Value = model.InitCardNumber;
            parameters[17].Value = model.StockStatus;
            parameters[18].Value = model.CouponNumber;
            parameters[19].Value = model.CreatedOn;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CouponNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Coupon ");
            strSql.Append(" where CouponNumber=@CouponNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512)			};
            parameters[0].Value = CouponNumber;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CouponNumberlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Coupon ");
            strSql.Append(" where CouponNumber in (" + CouponNumberlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Coupon GetModel(string CouponNumber)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CouponNumber,CouponTypeID,CouponIssueDate,CouponExpiryDate,CouponActiveDate,StoreID,BatchCouponID,Status,CouponPassword,CardNumber,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponAmount,RedeemStoreID,PickupFlag,LocateStoreID,InitCardNumber,StockStatus from Coupon ");
            strSql.Append(" where CouponNumber=@CouponNumber ");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponNumber", SqlDbType.VarChar,512)			};
            parameters[0].Value = CouponNumber;

            Edge.SVA.Model.Coupon model = new Edge.SVA.Model.Coupon();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CouponNumber"] != null && ds.Tables[0].Rows[0]["CouponNumber"].ToString() != "")
                {
                    model.CouponNumber = ds.Tables[0].Rows[0]["CouponNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CouponTypeID"] != null && ds.Tables[0].Rows[0]["CouponTypeID"].ToString() != "")
                {
                    model.CouponTypeID = int.Parse(ds.Tables[0].Rows[0]["CouponTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponIssueDate"] != null && ds.Tables[0].Rows[0]["CouponIssueDate"].ToString() != "")
                {
                    model.CouponIssueDate = DateTime.Parse(ds.Tables[0].Rows[0]["CouponIssueDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponExpiryDate"] != null && ds.Tables[0].Rows[0]["CouponExpiryDate"].ToString() != "")
                {
                    model.CouponExpiryDate = DateTime.Parse(ds.Tables[0].Rows[0]["CouponExpiryDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponActiveDate"] != null && ds.Tables[0].Rows[0]["CouponActiveDate"].ToString() != "")
                {
                    model.CouponActiveDate = DateTime.Parse(ds.Tables[0].Rows[0]["CouponActiveDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StoreID"] != null && ds.Tables[0].Rows[0]["StoreID"].ToString() != "")
                {
                    model.StoreID = int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BatchCouponID"] != null && ds.Tables[0].Rows[0]["BatchCouponID"].ToString() != "")
                {
                    model.BatchCouponID = int.Parse(ds.Tables[0].Rows[0]["BatchCouponID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponPassword"] != null && ds.Tables[0].Rows[0]["CouponPassword"].ToString() != "")
                {
                    model.CouponPassword = ds.Tables[0].Rows[0]["CouponPassword"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CardNumber"] != null && ds.Tables[0].Rows[0]["CardNumber"].ToString() != "")
                {
                    model.CardNumber = ds.Tables[0].Rows[0]["CardNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedOn"] != null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString() != "")
                {
                    model.UpdatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdatedBy"] != null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString() != "")
                {
                    model.UpdatedBy = int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponAmount"] != null && ds.Tables[0].Rows[0]["CouponAmount"].ToString() != "")
                {
                    model.CouponAmount = decimal.Parse(ds.Tables[0].Rows[0]["CouponAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RedeemStoreID"] != null && ds.Tables[0].Rows[0]["RedeemStoreID"].ToString() != "")
                {
                    model.RedeemStoreID = int.Parse(ds.Tables[0].Rows[0]["RedeemStoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PickupFlag"] != null && ds.Tables[0].Rows[0]["PickupFlag"].ToString() != "")
                {
                    model.PickupFlag = int.Parse(ds.Tables[0].Rows[0]["PickupFlag"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LocateStoreID"] != null && ds.Tables[0].Rows[0]["LocateStoreID"].ToString() != "")
                {
                    model.LocateStoreID = int.Parse(ds.Tables[0].Rows[0]["LocateStoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InitCardNumber"] != null && ds.Tables[0].Rows[0]["InitCardNumber"].ToString() != "")
                {
                    model.InitCardNumber = ds.Tables[0].Rows[0]["InitCardNumber"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StockStatus"] != null && ds.Tables[0].Rows[0]["StockStatus"].ToString() != "")
                {
                    model.StockStatus = int.Parse(ds.Tables[0].Rows[0]["StockStatus"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CouponNumber,CouponTypeID,CouponIssueDate,CouponExpiryDate,CouponActiveDate,StoreID,BatchCouponID,Status,CouponPassword,CardNumber,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponAmount,RedeemStoreID,PickupFlag,LocateStoreID,InitCardNumber,StockStatus ");
            strSql.Append(" FROM Coupon ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CouponNumber,CouponTypeID,CouponIssueDate,CouponExpiryDate,CouponActiveDate,StoreID,BatchCouponID,Status,CouponPassword,CardNumber,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponAmount,RedeemStoreID,PickupFlag,LocateStoreID,InitCardNumber,StockStatus ");
            strSql.Append(" FROM Coupon ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Coupon";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Coupon ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }


		#endregion  Method
	}
}

