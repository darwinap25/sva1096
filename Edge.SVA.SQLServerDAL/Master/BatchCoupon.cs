﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:BatchCoupon
	/// </summary>
	public partial class BatchCoupon:IBatchCoupon
	{
		public BatchCoupon()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("BatchCouponID", "BatchCoupon"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string BatchCouponCode,int BatchCouponID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from BatchCoupon");
			strSql.Append(" where BatchCouponCode=@BatchCouponCode and BatchCouponID=@BatchCouponID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCouponCode", SqlDbType.VarChar,512),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4)};
			parameters[0].Value = BatchCouponCode;
			parameters[1].Value = BatchCouponID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.BatchCoupon model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into BatchCoupon(");
			strSql.Append("BatchCouponCode,SeqFrom,SeqTo,Qty,CouponTypeID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@BatchCouponCode,@SeqFrom,@SeqTo,@Qty,@CouponTypeID,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCouponCode", SqlDbType.VarChar,512),
					new SqlParameter("@SeqFrom", SqlDbType.Int,4),
					new SqlParameter("@SeqTo", SqlDbType.Int,4),
					new SqlParameter("@Qty", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.BatchCouponCode;
			parameters[1].Value = model.SeqFrom;
			parameters[2].Value = model.SeqTo;
			parameters[3].Value = model.Qty;
			parameters[4].Value = model.CouponTypeID;
			parameters[5].Value = model.CreatedOn;
			parameters[6].Value = model.UpdatedOn;
			parameters[7].Value = model.CreatedBy;
			parameters[8].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.BatchCoupon model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update BatchCoupon set ");
			strSql.Append("SeqFrom=@SeqFrom,");
			strSql.Append("SeqTo=@SeqTo,");
			strSql.Append("Qty=@Qty,");
			strSql.Append("CouponTypeID=@CouponTypeID,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where BatchCouponID=@BatchCouponID");
			SqlParameter[] parameters = {
					new SqlParameter("@SeqFrom", SqlDbType.Int,4),
					new SqlParameter("@SeqTo", SqlDbType.Int,4),
					new SqlParameter("@Qty", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4),
					new SqlParameter("@BatchCouponCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.SeqFrom;
			parameters[1].Value = model.SeqTo;
			parameters[2].Value = model.Qty;
			parameters[3].Value = model.CouponTypeID;
			parameters[4].Value = model.CreatedOn;
			parameters[5].Value = model.UpdatedOn;
			parameters[6].Value = model.CreatedBy;
			parameters[7].Value = model.UpdatedBy;
			parameters[8].Value = model.BatchCouponID;
			parameters[9].Value = model.BatchCouponCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int BatchCouponID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BatchCoupon ");
			strSql.Append(" where BatchCouponID=@BatchCouponID");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4)
};
			parameters[0].Value = BatchCouponID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string BatchCouponCode,int BatchCouponID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BatchCoupon ");
			strSql.Append(" where BatchCouponCode=@BatchCouponCode and BatchCouponID=@BatchCouponID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCouponCode", SqlDbType.VarChar,512),
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4)};
			parameters[0].Value = BatchCouponCode;
			parameters[1].Value = BatchCouponID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string BatchCouponIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BatchCoupon ");
			strSql.Append(" where BatchCouponID in ("+BatchCouponIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.BatchCoupon GetModel(int BatchCouponID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 BatchCouponID,BatchCouponCode,SeqFrom,SeqTo,Qty,CouponTypeID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy from BatchCoupon ");
			strSql.Append(" where BatchCouponID=@BatchCouponID");
			SqlParameter[] parameters = {
					new SqlParameter("@BatchCouponID", SqlDbType.Int,4)
};
			parameters[0].Value = BatchCouponID;

			Edge.SVA.Model.BatchCoupon model=new Edge.SVA.Model.BatchCoupon();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["BatchCouponID"]!=null && ds.Tables[0].Rows[0]["BatchCouponID"].ToString()!="")
				{
					model.BatchCouponID=int.Parse(ds.Tables[0].Rows[0]["BatchCouponID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BatchCouponCode"]!=null && ds.Tables[0].Rows[0]["BatchCouponCode"].ToString()!="")
				{
					model.BatchCouponCode=ds.Tables[0].Rows[0]["BatchCouponCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SeqFrom"]!=null && ds.Tables[0].Rows[0]["SeqFrom"].ToString()!="")
				{
					model.SeqFrom=int.Parse(ds.Tables[0].Rows[0]["SeqFrom"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SeqTo"]!=null && ds.Tables[0].Rows[0]["SeqTo"].ToString()!="")
				{
					model.SeqTo=int.Parse(ds.Tables[0].Rows[0]["SeqTo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Qty"]!=null && ds.Tables[0].Rows[0]["Qty"].ToString()!="")
				{
					model.Qty=int.Parse(ds.Tables[0].Rows[0]["Qty"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CouponTypeID"]!=null && ds.Tables[0].Rows[0]["CouponTypeID"].ToString()!="")
				{
					model.CouponTypeID=int.Parse(ds.Tables[0].Rows[0]["CouponTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select BatchCouponID,BatchCouponCode,SeqFrom,SeqTo,Qty,CouponTypeID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM BatchCoupon ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" BatchCouponID,BatchCouponCode,SeqFrom,SeqTo,Qty,CouponTypeID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM BatchCoupon ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "BatchCoupon";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from BatchCoupon ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

