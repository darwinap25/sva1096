﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:SVARewardRules
	/// </summary>
	public partial class SVARewardRules:ISVARewardRules
	{
		public SVARewardRules()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SVARewardRulesID", "SVARewardRules"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string SVARewardRulesCode,int SVARewardRulesID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SVARewardRules");
			strSql.Append(" where SVARewardRulesCode=@SVARewardRulesCode and SVARewardRulesID=@SVARewardRulesID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SVARewardRulesCode", SqlDbType.VarChar,64),
					new SqlParameter("@SVARewardRulesID", SqlDbType.Int,4)			};
			parameters[0].Value = SVARewardRulesCode;
			parameters[1].Value = SVARewardRulesID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.SVARewardRules model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SVARewardRules(");
			strSql.Append("CardGradeID,SVARewardType,SVARewardRulesCode,RewardAmount,RewardPoint,RewardCouponTypeID,RewardCouponCount,StartDate,EndDate,Status,Note,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CardGradeID,@SVARewardType,@SVARewardRulesCode,@RewardAmount,@RewardPoint,@RewardCouponTypeID,@RewardCouponCount,@StartDate,@EndDate,@Status,@Note,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@SVARewardType", SqlDbType.Int,4),
					new SqlParameter("@SVARewardRulesCode", SqlDbType.VarChar,64),
					new SqlParameter("@RewardAmount", SqlDbType.Money,8),
					new SqlParameter("@RewardPoint", SqlDbType.Int,4),
					new SqlParameter("@RewardCouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@RewardCouponCount", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CardGradeID;
			parameters[1].Value = model.SVARewardType;
			parameters[2].Value = model.SVARewardRulesCode;
			parameters[3].Value = model.RewardAmount;
			parameters[4].Value = model.RewardPoint;
			parameters[5].Value = model.RewardCouponTypeID;
			parameters[6].Value = model.RewardCouponCount;
			parameters[7].Value = model.StartDate;
			parameters[8].Value = model.EndDate;
			parameters[9].Value = model.Status;
			parameters[10].Value = model.Note;
			parameters[11].Value = model.CreatedOn;
			parameters[12].Value = model.UpdatedOn;
			parameters[13].Value = model.CreatedBy;
			parameters[14].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.SVARewardRules model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SVARewardRules set ");
			strSql.Append("CardGradeID=@CardGradeID,");
			strSql.Append("SVARewardType=@SVARewardType,");
			strSql.Append("RewardAmount=@RewardAmount,");
			strSql.Append("RewardPoint=@RewardPoint,");
			strSql.Append("RewardCouponTypeID=@RewardCouponTypeID,");
			strSql.Append("RewardCouponCount=@RewardCouponCount,");
			strSql.Append("StartDate=@StartDate,");
			strSql.Append("EndDate=@EndDate,");
			strSql.Append("Status=@Status,");
			strSql.Append("Note=@Note,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where SVARewardRulesID=@SVARewardRulesID");
			SqlParameter[] parameters = {
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@SVARewardType", SqlDbType.Int,4),
					new SqlParameter("@RewardAmount", SqlDbType.Money,8),
					new SqlParameter("@RewardPoint", SqlDbType.Int,4),
					new SqlParameter("@RewardCouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@RewardCouponCount", SqlDbType.Int,4),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Note", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@SVARewardRulesID", SqlDbType.Int,4),
					new SqlParameter("@SVARewardRulesCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.CardGradeID;
			parameters[1].Value = model.SVARewardType;
			parameters[2].Value = model.RewardAmount;
			parameters[3].Value = model.RewardPoint;
			parameters[4].Value = model.RewardCouponTypeID;
			parameters[5].Value = model.RewardCouponCount;
			parameters[6].Value = model.StartDate;
			parameters[7].Value = model.EndDate;
			parameters[8].Value = model.Status;
			parameters[9].Value = model.Note;
			parameters[10].Value = model.CreatedOn;
			parameters[11].Value = model.UpdatedOn;
			parameters[12].Value = model.CreatedBy;
			parameters[13].Value = model.UpdatedBy;
			parameters[14].Value = model.SVARewardRulesID;
			parameters[15].Value = model.SVARewardRulesCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SVARewardRulesID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SVARewardRules ");
			strSql.Append(" where SVARewardRulesID=@SVARewardRulesID");
			SqlParameter[] parameters = {
					new SqlParameter("@SVARewardRulesID", SqlDbType.Int,4)
			};
			parameters[0].Value = SVARewardRulesID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string SVARewardRulesCode,int SVARewardRulesID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SVARewardRules ");
			strSql.Append(" where SVARewardRulesCode=@SVARewardRulesCode and SVARewardRulesID=@SVARewardRulesID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SVARewardRulesCode", SqlDbType.VarChar,64),
					new SqlParameter("@SVARewardRulesID", SqlDbType.Int,4)			};
			parameters[0].Value = SVARewardRulesCode;
			parameters[1].Value = SVARewardRulesID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SVARewardRulesIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SVARewardRules ");
			strSql.Append(" where SVARewardRulesID in ("+SVARewardRulesIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.SVARewardRules GetModel(int SVARewardRulesID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SVARewardRulesID,CardGradeID,SVARewardType,SVARewardRulesCode,RewardAmount,RewardPoint,RewardCouponTypeID,RewardCouponCount,StartDate,EndDate,Status,Note,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy from SVARewardRules ");
			strSql.Append(" where SVARewardRulesID=@SVARewardRulesID");
			SqlParameter[] parameters = {
					new SqlParameter("@SVARewardRulesID", SqlDbType.Int,4)
			};
			parameters[0].Value = SVARewardRulesID;

			Edge.SVA.Model.SVARewardRules model=new Edge.SVA.Model.SVARewardRules();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SVARewardRulesID"]!=null && ds.Tables[0].Rows[0]["SVARewardRulesID"].ToString()!="")
				{
					model.SVARewardRulesID=int.Parse(ds.Tables[0].Rows[0]["SVARewardRulesID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardGradeID"]!=null && ds.Tables[0].Rows[0]["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SVARewardType"]!=null && ds.Tables[0].Rows[0]["SVARewardType"].ToString()!="")
				{
					model.SVARewardType=int.Parse(ds.Tables[0].Rows[0]["SVARewardType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SVARewardRulesCode"]!=null && ds.Tables[0].Rows[0]["SVARewardRulesCode"].ToString()!="")
				{
					model.SVARewardRulesCode=ds.Tables[0].Rows[0]["SVARewardRulesCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RewardAmount"]!=null && ds.Tables[0].Rows[0]["RewardAmount"].ToString()!="")
				{
					model.RewardAmount=decimal.Parse(ds.Tables[0].Rows[0]["RewardAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RewardPoint"]!=null && ds.Tables[0].Rows[0]["RewardPoint"].ToString()!="")
				{
					model.RewardPoint=int.Parse(ds.Tables[0].Rows[0]["RewardPoint"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RewardCouponTypeID"]!=null && ds.Tables[0].Rows[0]["RewardCouponTypeID"].ToString()!="")
				{
					model.RewardCouponTypeID=int.Parse(ds.Tables[0].Rows[0]["RewardCouponTypeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RewardCouponCount"]!=null && ds.Tables[0].Rows[0]["RewardCouponCount"].ToString()!="")
				{
					model.RewardCouponCount=int.Parse(ds.Tables[0].Rows[0]["RewardCouponCount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartDate"]!=null && ds.Tables[0].Rows[0]["StartDate"].ToString()!="")
				{
					model.StartDate=DateTime.Parse(ds.Tables[0].Rows[0]["StartDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndDate"]!=null && ds.Tables[0].Rows[0]["EndDate"].ToString()!="")
				{
					model.EndDate=DateTime.Parse(ds.Tables[0].Rows[0]["EndDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Note"]!=null && ds.Tables[0].Rows[0]["Note"].ToString()!="")
				{
					model.Note=ds.Tables[0].Rows[0]["Note"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SVARewardRulesID,CardGradeID,SVARewardType,SVARewardRulesCode,RewardAmount,RewardPoint,RewardCouponTypeID,RewardCouponCount,StartDate,EndDate,Status,Note,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM SVARewardRules ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SVARewardRulesID,CardGradeID,SVARewardType,SVARewardRulesCode,RewardAmount,RewardPoint,RewardCouponTypeID,RewardCouponCount,StartDate,EndDate,Status,Note,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy ");
			strSql.Append(" FROM SVARewardRules ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SVARewardRules ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SVARewardRulesID desc");
			}
			strSql.Append(")AS Row, T.*  from SVARewardRules T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SVARewardRules";
			parameters[1].Value = "SVARewardRulesID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

