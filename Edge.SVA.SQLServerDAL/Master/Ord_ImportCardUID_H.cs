﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Ord_ImportCardUID_H
	/// </summary>
	public partial class Ord_ImportCardUID_H:IOrd_ImportCardUID_H
	{
		public Ord_ImportCardUID_H()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string ImportCardNumber)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_ImportCardUID_H");
			strSql.Append(" where ImportCardNumber=@ImportCardNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64)			};
			parameters[0].Value = ImportCardNumber;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Ord_ImportCardUID_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_ImportCardUID_H(");
			strSql.Append("ImportCardNumber,ImportCardDesc1,ImportCardDesc2,ImportCardDesc3,NeedActive,NeedNewBatch,CardCount,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate)");
			strSql.Append(" values (");
			strSql.Append("@ImportCardNumber,@ImportCardDesc1,@ImportCardDesc2,@ImportCardDesc3,@NeedActive,@NeedNewBatch,@CardCount,@ApprovalCode,@ApproveStatus,@ApproveOn,@ApproveBy,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@CreatedBusDate,@ApproveBusDate)");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@ImportCardDesc1", SqlDbType.NVarChar,512),
					new SqlParameter("@ImportCardDesc2", SqlDbType.NVarChar,512),
					new SqlParameter("@ImportCardDesc3", SqlDbType.NVarChar,512),
					new SqlParameter("@NeedActive", SqlDbType.Int,4),
					new SqlParameter("@NeedNewBatch", SqlDbType.Int,4),
					new SqlParameter("@CardCount", SqlDbType.Int,4),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime)};
			parameters[0].Value = model.ImportCardNumber;
			parameters[1].Value = model.ImportCardDesc1;
			parameters[2].Value = model.ImportCardDesc2;
			parameters[3].Value = model.ImportCardDesc3;
			parameters[4].Value = model.NeedActive;
			parameters[5].Value = model.NeedNewBatch;
			parameters[6].Value = model.CardCount;
			parameters[7].Value = model.ApprovalCode;
			parameters[8].Value = model.ApproveStatus;
			parameters[9].Value = model.ApproveOn;
			parameters[10].Value = model.ApproveBy;
			parameters[11].Value = model.CreatedOn;
			parameters[12].Value = model.CreatedBy;
			parameters[13].Value = model.UpdatedOn;
			parameters[14].Value = model.UpdatedBy;
			parameters[15].Value = model.CreatedBusDate;
			parameters[16].Value = model.ApproveBusDate;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_ImportCardUID_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_ImportCardUID_H set ");
			strSql.Append("ImportCardDesc1=@ImportCardDesc1,");
			strSql.Append("ImportCardDesc2=@ImportCardDesc2,");
			strSql.Append("ImportCardDesc3=@ImportCardDesc3,");
			strSql.Append("NeedActive=@NeedActive,");
			strSql.Append("NeedNewBatch=@NeedNewBatch,");
			strSql.Append("CardCount=@CardCount,");
			strSql.Append("ApprovalCode=@ApprovalCode,");
			strSql.Append("ApproveStatus=@ApproveStatus,");
			strSql.Append("ApproveOn=@ApproveOn,");
			strSql.Append("ApproveBy=@ApproveBy,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("CreatedBusDate=@CreatedBusDate,");
			strSql.Append("ApproveBusDate=@ApproveBusDate");
			strSql.Append(" where ImportCardNumber=@ImportCardNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardDesc1", SqlDbType.NVarChar,512),
					new SqlParameter("@ImportCardDesc2", SqlDbType.NVarChar,512),
					new SqlParameter("@ImportCardDesc3", SqlDbType.NVarChar,512),
					new SqlParameter("@NeedActive", SqlDbType.Int,4),
					new SqlParameter("@NeedNewBatch", SqlDbType.Int,4),
					new SqlParameter("@CardCount", SqlDbType.Int,4),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64)};
			parameters[0].Value = model.ImportCardDesc1;
			parameters[1].Value = model.ImportCardDesc2;
			parameters[2].Value = model.ImportCardDesc3;
			parameters[3].Value = model.NeedActive;
			parameters[4].Value = model.NeedNewBatch;
			parameters[5].Value = model.CardCount;
			parameters[6].Value = model.ApprovalCode;
			parameters[7].Value = model.ApproveStatus;
			parameters[8].Value = model.ApproveOn;
			parameters[9].Value = model.ApproveBy;
			parameters[10].Value = model.CreatedOn;
			parameters[11].Value = model.CreatedBy;
			parameters[12].Value = model.UpdatedOn;
			parameters[13].Value = model.UpdatedBy;
			parameters[14].Value = model.CreatedBusDate;
			parameters[15].Value = model.ApproveBusDate;
			parameters[16].Value = model.ImportCardNumber;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string ImportCardNumber)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_ImportCardUID_H ");
			strSql.Append(" where ImportCardNumber=@ImportCardNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64)			};
			parameters[0].Value = ImportCardNumber;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ImportCardNumberlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_ImportCardUID_H ");
			strSql.Append(" where ImportCardNumber in ("+ImportCardNumberlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_ImportCardUID_H GetModel(string ImportCardNumber)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ImportCardNumber,ImportCardDesc1,ImportCardDesc2,ImportCardDesc3,NeedActive,NeedNewBatch,CardCount,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate from Ord_ImportCardUID_H ");
			strSql.Append(" where ImportCardNumber=@ImportCardNumber ");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64)			};
			parameters[0].Value = ImportCardNumber;

			Edge.SVA.Model.Ord_ImportCardUID_H model=new Edge.SVA.Model.Ord_ImportCardUID_H();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_ImportCardUID_H DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.Ord_ImportCardUID_H model=new Edge.SVA.Model.Ord_ImportCardUID_H();
			if (row != null)
			{
				if(row["ImportCardNumber"]!=null)
				{
					model.ImportCardNumber=row["ImportCardNumber"].ToString();
				}
				if(row["ImportCardDesc1"]!=null)
				{
					model.ImportCardDesc1=row["ImportCardDesc1"].ToString();
				}
				if(row["ImportCardDesc2"]!=null)
				{
					model.ImportCardDesc2=row["ImportCardDesc2"].ToString();
				}
				if(row["ImportCardDesc3"]!=null)
				{
					model.ImportCardDesc3=row["ImportCardDesc3"].ToString();
				}
				if(row["NeedActive"]!=null && row["NeedActive"].ToString()!="")
				{
					model.NeedActive=int.Parse(row["NeedActive"].ToString());
				}
				if(row["NeedNewBatch"]!=null && row["NeedNewBatch"].ToString()!="")
				{
					model.NeedNewBatch=int.Parse(row["NeedNewBatch"].ToString());
				}
				if(row["CardCount"]!=null && row["CardCount"].ToString()!="")
				{
					model.CardCount=int.Parse(row["CardCount"].ToString());
				}
				if(row["ApprovalCode"]!=null)
				{
					model.ApprovalCode=row["ApprovalCode"].ToString();
				}
				if(row["ApproveStatus"]!=null)
				{
					model.ApproveStatus=row["ApproveStatus"].ToString();
				}
				if(row["ApproveOn"]!=null && row["ApproveOn"].ToString()!="")
				{
					model.ApproveOn=DateTime.Parse(row["ApproveOn"].ToString());
				}
				if(row["ApproveBy"]!=null && row["ApproveBy"].ToString()!="")
				{
					model.ApproveBy=int.Parse(row["ApproveBy"].ToString());
				}
				if(row["CreatedOn"]!=null && row["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(row["CreatedOn"].ToString());
				}
				if(row["CreatedBy"]!=null && row["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(row["CreatedBy"].ToString());
				}
				if(row["UpdatedOn"]!=null && row["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(row["UpdatedOn"].ToString());
				}
				if(row["UpdatedBy"]!=null && row["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(row["UpdatedBy"].ToString());
				}
				if(row["CreatedBusDate"]!=null && row["CreatedBusDate"].ToString()!="")
				{
					model.CreatedBusDate=DateTime.Parse(row["CreatedBusDate"].ToString());
				}
				if(row["ApproveBusDate"]!=null && row["ApproveBusDate"].ToString()!="")
				{
					model.ApproveBusDate=DateTime.Parse(row["ApproveBusDate"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ImportCardNumber,ImportCardDesc1,ImportCardDesc2,ImportCardDesc3,NeedActive,NeedNewBatch,CardCount,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate ");
			strSql.Append(" FROM Ord_ImportCardUID_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ImportCardNumber,ImportCardDesc1,ImportCardDesc2,ImportCardDesc3,NeedActive,NeedNewBatch,CardCount,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate ");
			strSql.Append(" FROM Ord_ImportCardUID_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_ImportCardUID_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ImportCardNumber desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_ImportCardUID_H T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_ImportCardUID_H";
			parameters[1].Value = "ImportCardNumber";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

