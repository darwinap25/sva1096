﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Card_Movement
	/// </summary>
	public partial class Card_Movement:ICard_Movement
	{
		public Card_Movement()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Card_Movement"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Card_Movement");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Card_Movement model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Card_Movement(");
			strSql.Append("OprID,CardNumber,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,Points,BusDate,Txndate,OrgExpiryDate,NewExpiryDate,OrgStatus,NewStatus,CardCashDetailID,CardPointDetailID,TenderID,Additional,Remark,ApprovalCode,SecurityCode,CreatedOn,CreatedBy,StoreID,ServerCode,RegisterCode,OpenPoint,ClosePoint)");
			strSql.Append(" values (");
			strSql.Append("@OprID,@CardNumber,@RefKeyID,@RefReceiveKeyID,@RefTxnNo,@OpenBal,@Amount,@CloseBal,@Points,@BusDate,@Txndate,@OrgExpiryDate,@NewExpiryDate,@OrgStatus,@NewStatus,@CardCashDetailID,@CardPointDetailID,@TenderID,@Additional,@Remark,@ApprovalCode,@SecurityCode,@CreatedOn,@CreatedBy,@StoreID,@ServerCode,@RegisterCode,@OpenPoint,@ClosePoint)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@OprID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@RefKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefReceiveKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@OpenBal", SqlDbType.Money,8),
					new SqlParameter("@Amount", SqlDbType.Money,8),
					new SqlParameter("@CloseBal", SqlDbType.Money,8),
					new SqlParameter("@Points", SqlDbType.Int,4),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@Txndate", SqlDbType.DateTime),
					new SqlParameter("@OrgExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@NewExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@OrgStatus", SqlDbType.Int,4),
					new SqlParameter("@NewStatus", SqlDbType.Int,4),
					new SqlParameter("@CardCashDetailID", SqlDbType.Int,4),
					new SqlParameter("@CardPointDetailID", SqlDbType.Int,4),
					new SqlParameter("@TenderID", SqlDbType.Int,4),
					new SqlParameter("@Additional", SqlDbType.VarChar,512),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,512),
					new SqlParameter("@SecurityCode", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@OpenPoint", SqlDbType.Int,4),
					new SqlParameter("@ClosePoint", SqlDbType.Int,4)};
			parameters[0].Value = model.OprID;
			parameters[1].Value = model.CardNumber;
			parameters[2].Value = model.RefKeyID;
			parameters[3].Value = model.RefReceiveKeyID;
			parameters[4].Value = model.RefTxnNo;
			parameters[5].Value = model.OpenBal;
			parameters[6].Value = model.Amount;
			parameters[7].Value = model.CloseBal;
			parameters[8].Value = model.Points;
			parameters[9].Value = model.BusDate;
			parameters[10].Value = model.Txndate;
			parameters[11].Value = model.OrgExpiryDate;
			parameters[12].Value = model.NewExpiryDate;
			parameters[13].Value = model.OrgStatus;
			parameters[14].Value = model.NewStatus;
			parameters[15].Value = model.CardCashDetailID;
			parameters[16].Value = model.CardPointDetailID;
			parameters[17].Value = model.TenderID;
			parameters[18].Value = model.Additional;
			parameters[19].Value = model.Remark;
			parameters[20].Value = model.ApprovalCode;
			parameters[21].Value = model.SecurityCode;
			parameters[22].Value = model.CreatedOn;
			parameters[23].Value = model.CreatedBy;
			parameters[24].Value = model.StoreID;
			parameters[25].Value = model.ServerCode;
			parameters[26].Value = model.RegisterCode;
			parameters[27].Value = model.OpenPoint;
			parameters[28].Value = model.ClosePoint;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Card_Movement model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Card_Movement set ");
			strSql.Append("OprID=@OprID,");
			strSql.Append("CardNumber=@CardNumber,");
			strSql.Append("RefKeyID=@RefKeyID,");
			strSql.Append("RefReceiveKeyID=@RefReceiveKeyID,");
			strSql.Append("RefTxnNo=@RefTxnNo,");
			strSql.Append("OpenBal=@OpenBal,");
			strSql.Append("Amount=@Amount,");
			strSql.Append("CloseBal=@CloseBal,");
			strSql.Append("Points=@Points,");
			strSql.Append("BusDate=@BusDate,");
			strSql.Append("Txndate=@Txndate,");
			strSql.Append("OrgExpiryDate=@OrgExpiryDate,");
			strSql.Append("NewExpiryDate=@NewExpiryDate,");
			strSql.Append("OrgStatus=@OrgStatus,");
			strSql.Append("NewStatus=@NewStatus,");
			strSql.Append("CardCashDetailID=@CardCashDetailID,");
			strSql.Append("CardPointDetailID=@CardPointDetailID,");
			strSql.Append("TenderID=@TenderID,");
			strSql.Append("Additional=@Additional,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("ApprovalCode=@ApprovalCode,");
			strSql.Append("SecurityCode=@SecurityCode,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("StoreID=@StoreID,");
			strSql.Append("ServerCode=@ServerCode,");
			strSql.Append("RegisterCode=@RegisterCode,");
			strSql.Append("OpenPoint=@OpenPoint,");
			strSql.Append("ClosePoint=@ClosePoint");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@OprID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,512),
					new SqlParameter("@RefKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefReceiveKeyID", SqlDbType.Int,4),
					new SqlParameter("@RefTxnNo", SqlDbType.VarChar,512),
					new SqlParameter("@OpenBal", SqlDbType.Money,8),
					new SqlParameter("@Amount", SqlDbType.Money,8),
					new SqlParameter("@CloseBal", SqlDbType.Money,8),
					new SqlParameter("@Points", SqlDbType.Int,4),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@Txndate", SqlDbType.DateTime),
					new SqlParameter("@OrgExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@NewExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@OrgStatus", SqlDbType.Int,4),
					new SqlParameter("@NewStatus", SqlDbType.Int,4),
					new SqlParameter("@CardCashDetailID", SqlDbType.Int,4),
					new SqlParameter("@CardPointDetailID", SqlDbType.Int,4),
					new SqlParameter("@TenderID", SqlDbType.Int,4),
					new SqlParameter("@Additional", SqlDbType.VarChar,512),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,512),
					new SqlParameter("@SecurityCode", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@ServerCode", SqlDbType.VarChar,512),
					new SqlParameter("@RegisterCode", SqlDbType.VarChar,512),
					new SqlParameter("@OpenPoint", SqlDbType.Int,4),
					new SqlParameter("@ClosePoint", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.OprID;
			parameters[1].Value = model.CardNumber;
			parameters[2].Value = model.RefKeyID;
			parameters[3].Value = model.RefReceiveKeyID;
			parameters[4].Value = model.RefTxnNo;
			parameters[5].Value = model.OpenBal;
			parameters[6].Value = model.Amount;
			parameters[7].Value = model.CloseBal;
			parameters[8].Value = model.Points;
			parameters[9].Value = model.BusDate;
			parameters[10].Value = model.Txndate;
			parameters[11].Value = model.OrgExpiryDate;
			parameters[12].Value = model.NewExpiryDate;
			parameters[13].Value = model.OrgStatus;
			parameters[14].Value = model.NewStatus;
			parameters[15].Value = model.CardCashDetailID;
			parameters[16].Value = model.CardPointDetailID;
			parameters[17].Value = model.TenderID;
			parameters[18].Value = model.Additional;
			parameters[19].Value = model.Remark;
			parameters[20].Value = model.ApprovalCode;
			parameters[21].Value = model.SecurityCode;
			parameters[22].Value = model.CreatedOn;
			parameters[23].Value = model.CreatedBy;
			parameters[24].Value = model.StoreID;
			parameters[25].Value = model.ServerCode;
			parameters[26].Value = model.RegisterCode;
			parameters[27].Value = model.OpenPoint;
			parameters[28].Value = model.ClosePoint;
			parameters[29].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Card_Movement ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Card_Movement ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Card_Movement GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,OprID,CardNumber,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,Points,BusDate,Txndate,OrgExpiryDate,NewExpiryDate,OrgStatus,NewStatus,CardCashDetailID,CardPointDetailID,TenderID,Additional,Remark,ApprovalCode,SecurityCode,CreatedOn,CreatedBy,StoreID,ServerCode,RegisterCode,OpenPoint,ClosePoint from Card_Movement ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Card_Movement model=new Edge.SVA.Model.Card_Movement();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["KeyID"]!=null && ds.Tables[0].Rows[0]["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OprID"]!=null && ds.Tables[0].Rows[0]["OprID"].ToString()!="")
				{
					model.OprID=int.Parse(ds.Tables[0].Rows[0]["OprID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardNumber"]!=null && ds.Tables[0].Rows[0]["CardNumber"].ToString()!="")
				{
					model.CardNumber=ds.Tables[0].Rows[0]["CardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RefKeyID"]!=null && ds.Tables[0].Rows[0]["RefKeyID"].ToString()!="")
				{
					model.RefKeyID=int.Parse(ds.Tables[0].Rows[0]["RefKeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefReceiveKeyID"]!=null && ds.Tables[0].Rows[0]["RefReceiveKeyID"].ToString()!="")
				{
					model.RefReceiveKeyID=int.Parse(ds.Tables[0].Rows[0]["RefReceiveKeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefTxnNo"]!=null && ds.Tables[0].Rows[0]["RefTxnNo"].ToString()!="")
				{
					model.RefTxnNo=ds.Tables[0].Rows[0]["RefTxnNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OpenBal"]!=null && ds.Tables[0].Rows[0]["OpenBal"].ToString()!="")
				{
					model.OpenBal=decimal.Parse(ds.Tables[0].Rows[0]["OpenBal"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Amount"]!=null && ds.Tables[0].Rows[0]["Amount"].ToString()!="")
				{
					model.Amount=decimal.Parse(ds.Tables[0].Rows[0]["Amount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CloseBal"]!=null && ds.Tables[0].Rows[0]["CloseBal"].ToString()!="")
				{
					model.CloseBal=decimal.Parse(ds.Tables[0].Rows[0]["CloseBal"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Points"]!=null && ds.Tables[0].Rows[0]["Points"].ToString()!="")
				{
					model.Points=int.Parse(ds.Tables[0].Rows[0]["Points"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BusDate"]!=null && ds.Tables[0].Rows[0]["BusDate"].ToString()!="")
				{
					model.BusDate=DateTime.Parse(ds.Tables[0].Rows[0]["BusDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Txndate"]!=null && ds.Tables[0].Rows[0]["Txndate"].ToString()!="")
				{
					model.Txndate=DateTime.Parse(ds.Tables[0].Rows[0]["Txndate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OrgExpiryDate"]!=null && ds.Tables[0].Rows[0]["OrgExpiryDate"].ToString()!="")
				{
					model.OrgExpiryDate=DateTime.Parse(ds.Tables[0].Rows[0]["OrgExpiryDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["NewExpiryDate"]!=null && ds.Tables[0].Rows[0]["NewExpiryDate"].ToString()!="")
				{
					model.NewExpiryDate=DateTime.Parse(ds.Tables[0].Rows[0]["NewExpiryDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OrgStatus"]!=null && ds.Tables[0].Rows[0]["OrgStatus"].ToString()!="")
				{
					model.OrgStatus=int.Parse(ds.Tables[0].Rows[0]["OrgStatus"].ToString());
				}
				if(ds.Tables[0].Rows[0]["NewStatus"]!=null && ds.Tables[0].Rows[0]["NewStatus"].ToString()!="")
				{
					model.NewStatus=int.Parse(ds.Tables[0].Rows[0]["NewStatus"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardCashDetailID"]!=null && ds.Tables[0].Rows[0]["CardCashDetailID"].ToString()!="")
				{
					model.CardCashDetailID=int.Parse(ds.Tables[0].Rows[0]["CardCashDetailID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardPointDetailID"]!=null && ds.Tables[0].Rows[0]["CardPointDetailID"].ToString()!="")
				{
					model.CardPointDetailID=int.Parse(ds.Tables[0].Rows[0]["CardPointDetailID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenderID"]!=null && ds.Tables[0].Rows[0]["TenderID"].ToString()!="")
				{
					model.TenderID=int.Parse(ds.Tables[0].Rows[0]["TenderID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Additional"]!=null && ds.Tables[0].Rows[0]["Additional"].ToString()!="")
				{
					model.Additional=ds.Tables[0].Rows[0]["Additional"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ApprovalCode"]!=null && ds.Tables[0].Rows[0]["ApprovalCode"].ToString()!="")
				{
					model.ApprovalCode=ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SecurityCode"]!=null && ds.Tables[0].Rows[0]["SecurityCode"].ToString()!="")
				{
					model.SecurityCode=ds.Tables[0].Rows[0]["SecurityCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["StoreID"]!=null && ds.Tables[0].Rows[0]["StoreID"].ToString()!="")
				{
					model.StoreID=int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ServerCode"]!=null && ds.Tables[0].Rows[0]["ServerCode"].ToString()!="")
				{
					model.ServerCode=ds.Tables[0].Rows[0]["ServerCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RegisterCode"]!=null && ds.Tables[0].Rows[0]["RegisterCode"].ToString()!="")
				{
					model.RegisterCode=ds.Tables[0].Rows[0]["RegisterCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OpenPoint"]!=null && ds.Tables[0].Rows[0]["OpenPoint"].ToString()!="")
				{
					model.OpenPoint=int.Parse(ds.Tables[0].Rows[0]["OpenPoint"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ClosePoint"]!=null && ds.Tables[0].Rows[0]["ClosePoint"].ToString()!="")
				{
					model.ClosePoint=int.Parse(ds.Tables[0].Rows[0]["ClosePoint"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,OprID,CardNumber,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,Points,BusDate,Txndate,OrgExpiryDate,NewExpiryDate,OrgStatus,NewStatus,CardCashDetailID,CardPointDetailID,TenderID,Additional,Remark,ApprovalCode,SecurityCode,CreatedOn,CreatedBy,StoreID,ServerCode,RegisterCode,OpenPoint,ClosePoint ");
			strSql.Append(" FROM Card_Movement ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,OprID,CardNumber,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,Points,BusDate,Txndate,OrgExpiryDate,NewExpiryDate,OrgStatus,NewStatus,CardCashDetailID,CardPointDetailID,TenderID,Additional,Remark,ApprovalCode,SecurityCode,CreatedOn,CreatedBy,StoreID,ServerCode,RegisterCode,OpenPoint,ClosePoint ");
			strSql.Append(" FROM Card_Movement ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Card_Movement";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Card_Movement ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

