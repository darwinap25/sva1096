﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardIssueHistory
	/// </summary>
	public partial class CardIssueHistory:ICardIssueHistory
	{
		public CardIssueHistory()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CardIssueTxnID", "CardIssueHistory"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CardIssueTxnID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CardIssueHistory");
			strSql.Append(" where CardIssueTxnID=@CardIssueTxnID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssueTxnID", SqlDbType.Int,4)};
			parameters[0].Value = CardIssueTxnID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CardIssueHistory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CardIssueHistory(");
			strSql.Append("CardIssueTxnID,CardNumber,CardTypeID,CardIssueNotes,CreatedOn,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CardIssueTxnID,@CardNumber,@CardTypeID,@CardIssueNotes,@CreatedOn,@UpdatedOn,@UpdatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssueTxnID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.NVarChar,30),
					new SqlParameter("@CardTypeID", SqlDbType.NVarChar,20),
					new SqlParameter("@CardIssueNotes", SqlDbType.NVarChar,10),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.NVarChar,30)};
			parameters[0].Value = model.CardIssueTxnID;
			parameters[1].Value = model.CardNumber;
			parameters[2].Value = model.CardTypeID;
			parameters[3].Value = model.CardIssueNotes;
			parameters[4].Value = model.CreatedOn;
			parameters[5].Value = model.UpdatedOn;
			parameters[6].Value = model.UpdatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardIssueHistory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CardIssueHistory set ");
			strSql.Append("CardNumber=@CardNumber,");
			strSql.Append("CardTypeID=@CardTypeID,");
			strSql.Append("CardIssueNotes=@CardIssueNotes,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where CardIssueTxnID=@CardIssueTxnID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardNumber", SqlDbType.NVarChar,30),
					new SqlParameter("@CardTypeID", SqlDbType.NVarChar,20),
					new SqlParameter("@CardIssueNotes", SqlDbType.NVarChar,10),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.NVarChar,30),
					new SqlParameter("@CardIssueTxnID", SqlDbType.Int,4)};
			parameters[0].Value = model.CardNumber;
			parameters[1].Value = model.CardTypeID;
			parameters[2].Value = model.CardIssueNotes;
			parameters[3].Value = model.CreatedOn;
			parameters[4].Value = model.UpdatedOn;
			parameters[5].Value = model.UpdatedBy;
			parameters[6].Value = model.CardIssueTxnID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CardIssueTxnID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardIssueHistory ");
			strSql.Append(" where CardIssueTxnID=@CardIssueTxnID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssueTxnID", SqlDbType.Int,4)};
			parameters[0].Value = CardIssueTxnID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CardIssueTxnIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardIssueHistory ");
			strSql.Append(" where CardIssueTxnID in ("+CardIssueTxnIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardIssueHistory GetModel(int CardIssueTxnID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CardIssueTxnID,CardNumber,CardTypeID,CardIssueNotes,CreatedOn,UpdatedOn,UpdatedBy from CardIssueHistory ");
			strSql.Append(" where CardIssueTxnID=@CardIssueTxnID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardIssueTxnID", SqlDbType.Int,4)};
			parameters[0].Value = CardIssueTxnID;

			Edge.SVA.Model.CardIssueHistory model=new Edge.SVA.Model.CardIssueHistory();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CardIssueTxnID"]!=null && ds.Tables[0].Rows[0]["CardIssueTxnID"].ToString()!="")
				{
					model.CardIssueTxnID=int.Parse(ds.Tables[0].Rows[0]["CardIssueTxnID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardNumber"]!=null && ds.Tables[0].Rows[0]["CardNumber"].ToString()!="")
				{
					model.CardNumber=ds.Tables[0].Rows[0]["CardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardTypeID"]!=null && ds.Tables[0].Rows[0]["CardTypeID"].ToString()!="")
				{
					model.CardTypeID=ds.Tables[0].Rows[0]["CardTypeID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardIssueNotes"]!=null && ds.Tables[0].Rows[0]["CardIssueNotes"].ToString()!="")
				{
					model.CardIssueNotes=ds.Tables[0].Rows[0]["CardIssueNotes"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CardIssueTxnID,CardNumber,CardTypeID,CardIssueNotes,CreatedOn,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM CardIssueHistory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CardIssueTxnID,CardNumber,CardTypeID,CardIssueNotes,CreatedOn,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM CardIssueHistory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
			parameters[0].Value = "CardIssueHistory";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CardIssueHistory ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

