﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:LogisticsProvider
	/// </summary>
	public partial class LogisticsProvider:ILogisticsProvider
	{
		public LogisticsProvider()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("LogisticsProviderID", "LogisticsProvider"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string LogisticsProviderCode,int LogisticsProviderID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from LogisticsProvider");
			strSql.Append(" where LogisticsProviderCode=@LogisticsProviderCode and LogisticsProviderID=@LogisticsProviderID ");
			SqlParameter[] parameters = {
					new SqlParameter("@LogisticsProviderCode", SqlDbType.VarChar,64),
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4)			};
			parameters[0].Value = LogisticsProviderCode;
			parameters[1].Value = LogisticsProviderID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.LogisticsProvider model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into LogisticsProvider(");
			strSql.Append("LogisticsProviderCode,ProviderName1,ProviderName2,ProviderName3,ProviderContactTel,ProviderContact,ProviderContactEmail,OrdQueryAddr,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@LogisticsProviderCode,@ProviderName1,@ProviderName2,@ProviderName3,@ProviderContactTel,@ProviderContact,@ProviderContactEmail,@OrdQueryAddr,@Remark,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@LogisticsProviderCode", SqlDbType.VarChar,64),
					new SqlParameter("@ProviderName1", SqlDbType.NVarChar,512),
					new SqlParameter("@ProviderName2", SqlDbType.NVarChar,512),
					new SqlParameter("@ProviderName3", SqlDbType.NVarChar,512),
					new SqlParameter("@ProviderContactTel", SqlDbType.VarChar,64),
					new SqlParameter("@ProviderContact", SqlDbType.VarChar,64),
					new SqlParameter("@ProviderContactEmail", SqlDbType.VarChar,64),
					new SqlParameter("@OrdQueryAddr", SqlDbType.NVarChar,512),
					new SqlParameter("@Remark", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.LogisticsProviderCode;
			parameters[1].Value = model.ProviderName1;
			parameters[2].Value = model.ProviderName2;
			parameters[3].Value = model.ProviderName3;
			parameters[4].Value = model.ProviderContactTel;
			parameters[5].Value = model.ProviderContact;
			parameters[6].Value = model.ProviderContactEmail;
			parameters[7].Value = model.OrdQueryAddr;
			parameters[8].Value = model.Remark;
			parameters[9].Value = model.CreatedOn;
			parameters[10].Value = model.CreatedBy;
			parameters[11].Value = model.UpdatedOn;
			parameters[12].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.LogisticsProvider model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update LogisticsProvider set ");
			strSql.Append("ProviderName1=@ProviderName1,");
			strSql.Append("ProviderName2=@ProviderName2,");
			strSql.Append("ProviderName3=@ProviderName3,");
			strSql.Append("ProviderContactTel=@ProviderContactTel,");
			strSql.Append("ProviderContact=@ProviderContact,");
			strSql.Append("ProviderContactEmail=@ProviderContactEmail,");
			strSql.Append("OrdQueryAddr=@OrdQueryAddr,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where LogisticsProviderID=@LogisticsProviderID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProviderName1", SqlDbType.NVarChar,512),
					new SqlParameter("@ProviderName2", SqlDbType.NVarChar,512),
					new SqlParameter("@ProviderName3", SqlDbType.NVarChar,512),
					new SqlParameter("@ProviderContactTel", SqlDbType.VarChar,64),
					new SqlParameter("@ProviderContact", SqlDbType.VarChar,64),
					new SqlParameter("@ProviderContactEmail", SqlDbType.VarChar,64),
					new SqlParameter("@OrdQueryAddr", SqlDbType.NVarChar,512),
					new SqlParameter("@Remark", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4),
					new SqlParameter("@LogisticsProviderCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.ProviderName1;
			parameters[1].Value = model.ProviderName2;
			parameters[2].Value = model.ProviderName3;
			parameters[3].Value = model.ProviderContactTel;
			parameters[4].Value = model.ProviderContact;
			parameters[5].Value = model.ProviderContactEmail;
			parameters[6].Value = model.OrdQueryAddr;
			parameters[7].Value = model.Remark;
			parameters[8].Value = model.CreatedOn;
			parameters[9].Value = model.CreatedBy;
			parameters[10].Value = model.UpdatedOn;
			parameters[11].Value = model.UpdatedBy;
			parameters[12].Value = model.LogisticsProviderID;
			parameters[13].Value = model.LogisticsProviderCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int LogisticsProviderID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from LogisticsProvider ");
			strSql.Append(" where LogisticsProviderID=@LogisticsProviderID");
			SqlParameter[] parameters = {
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4)
			};
			parameters[0].Value = LogisticsProviderID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string LogisticsProviderCode,int LogisticsProviderID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from LogisticsProvider ");
			strSql.Append(" where LogisticsProviderCode=@LogisticsProviderCode and LogisticsProviderID=@LogisticsProviderID ");
			SqlParameter[] parameters = {
					new SqlParameter("@LogisticsProviderCode", SqlDbType.VarChar,64),
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4)			};
			parameters[0].Value = LogisticsProviderCode;
			parameters[1].Value = LogisticsProviderID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string LogisticsProviderIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from LogisticsProvider ");
			strSql.Append(" where LogisticsProviderID in ("+LogisticsProviderIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.LogisticsProvider GetModel(int LogisticsProviderID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 LogisticsProviderID,LogisticsProviderCode,ProviderName1,ProviderName2,ProviderName3,ProviderContactTel,ProviderContact,ProviderContactEmail,OrdQueryAddr,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from LogisticsProvider ");
			strSql.Append(" where LogisticsProviderID=@LogisticsProviderID");
			SqlParameter[] parameters = {
					new SqlParameter("@LogisticsProviderID", SqlDbType.Int,4)
			};
			parameters[0].Value = LogisticsProviderID;

			Edge.SVA.Model.LogisticsProvider model=new Edge.SVA.Model.LogisticsProvider();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["LogisticsProviderID"]!=null && ds.Tables[0].Rows[0]["LogisticsProviderID"].ToString()!="")
				{
					model.LogisticsProviderID=int.Parse(ds.Tables[0].Rows[0]["LogisticsProviderID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LogisticsProviderCode"]!=null && ds.Tables[0].Rows[0]["LogisticsProviderCode"].ToString()!="")
				{
					model.LogisticsProviderCode=ds.Tables[0].Rows[0]["LogisticsProviderCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProviderName1"]!=null && ds.Tables[0].Rows[0]["ProviderName1"].ToString()!="")
				{
					model.ProviderName1=ds.Tables[0].Rows[0]["ProviderName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProviderName2"]!=null && ds.Tables[0].Rows[0]["ProviderName2"].ToString()!="")
				{
					model.ProviderName2=ds.Tables[0].Rows[0]["ProviderName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProviderName3"]!=null && ds.Tables[0].Rows[0]["ProviderName3"].ToString()!="")
				{
					model.ProviderName3=ds.Tables[0].Rows[0]["ProviderName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProviderContactTel"]!=null && ds.Tables[0].Rows[0]["ProviderContactTel"].ToString()!="")
				{
					model.ProviderContactTel=ds.Tables[0].Rows[0]["ProviderContactTel"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProviderContact"]!=null && ds.Tables[0].Rows[0]["ProviderContact"].ToString()!="")
				{
					model.ProviderContact=ds.Tables[0].Rows[0]["ProviderContact"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProviderContactEmail"]!=null && ds.Tables[0].Rows[0]["ProviderContactEmail"].ToString()!="")
				{
					model.ProviderContactEmail=ds.Tables[0].Rows[0]["ProviderContactEmail"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OrdQueryAddr"]!=null && ds.Tables[0].Rows[0]["OrdQueryAddr"].ToString()!="")
				{
					model.OrdQueryAddr=ds.Tables[0].Rows[0]["OrdQueryAddr"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select LogisticsProviderID,LogisticsProviderCode,ProviderName1,ProviderName2,ProviderName3,ProviderContactTel,ProviderContact,ProviderContactEmail,OrdQueryAddr,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM LogisticsProvider ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" LogisticsProviderID,LogisticsProviderCode,ProviderName1,ProviderName2,ProviderName3,ProviderContactTel,ProviderContact,ProviderContactEmail,OrdQueryAddr,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM LogisticsProvider ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM LogisticsProvider ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.LogisticsProviderID desc");
			}
			strSql.Append(")AS Row, T.*  from LogisticsProvider T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "LogisticsProvider";
			parameters[1].Value = "LogisticsProviderID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

