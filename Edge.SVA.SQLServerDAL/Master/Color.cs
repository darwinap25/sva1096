﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Color
	/// </summary>
	public partial class Color:IColor
	{
		public Color()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ColorID", "Color"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ColorID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Color");
			strSql.Append(" where ColorID=@ColorID");
			SqlParameter[] parameters = {
					new SqlParameter("@ColorID", SqlDbType.Int,4)
			};
			parameters[0].Value = ColorID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Color model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Color(");
			strSql.Append("ColorCode,ColorName1,ColorName2,ColorName3,ColorPicFile)");
			strSql.Append(" values (");
			strSql.Append("@ColorCode,@ColorName1,@ColorName2,@ColorName3,@ColorPicFile)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ColorCode", SqlDbType.VarChar,64),
					new SqlParameter("@ColorName1", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorName2", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorName3", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorPicFile", SqlDbType.NVarChar,512)};
			parameters[0].Value = model.ColorCode;
			parameters[1].Value = model.ColorName1;
			parameters[2].Value = model.ColorName2;
			parameters[3].Value = model.ColorName3;
			parameters[4].Value = model.ColorPicFile;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Color model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Color set ");
			strSql.Append("ColorCode=@ColorCode,");
			strSql.Append("ColorName1=@ColorName1,");
			strSql.Append("ColorName2=@ColorName2,");
			strSql.Append("ColorName3=@ColorName3,");
			strSql.Append("ColorPicFile=@ColorPicFile");
			strSql.Append(" where ColorID=@ColorID");
			SqlParameter[] parameters = {
					new SqlParameter("@ColorCode", SqlDbType.VarChar,64),
					new SqlParameter("@ColorName1", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorName2", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorName3", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@ColorID", SqlDbType.Int,4)};
			parameters[0].Value = model.ColorCode;
			parameters[1].Value = model.ColorName1;
			parameters[2].Value = model.ColorName2;
			parameters[3].Value = model.ColorName3;
			parameters[4].Value = model.ColorPicFile;
			parameters[5].Value = model.ColorID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ColorID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Color ");
			strSql.Append(" where ColorID=@ColorID");
			SqlParameter[] parameters = {
					new SqlParameter("@ColorID", SqlDbType.Int,4)
			};
			parameters[0].Value = ColorID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ColorIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Color ");
			strSql.Append(" where ColorID in ("+ColorIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Color GetModel(int ColorID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ColorID,ColorCode,ColorName1,ColorName2,ColorName3,ColorPicFile from Color ");
			strSql.Append(" where ColorID=@ColorID");
			SqlParameter[] parameters = {
					new SqlParameter("@ColorID", SqlDbType.Int,4)
			};
			parameters[0].Value = ColorID;

			Edge.SVA.Model.Color model=new Edge.SVA.Model.Color();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ColorID"]!=null && ds.Tables[0].Rows[0]["ColorID"].ToString()!="")
				{
					model.ColorID=int.Parse(ds.Tables[0].Rows[0]["ColorID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ColorCode"]!=null && ds.Tables[0].Rows[0]["ColorCode"].ToString()!="")
				{
					model.ColorCode=ds.Tables[0].Rows[0]["ColorCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ColorName1"]!=null && ds.Tables[0].Rows[0]["ColorName1"].ToString()!="")
				{
					model.ColorName1=ds.Tables[0].Rows[0]["ColorName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ColorName2"]!=null && ds.Tables[0].Rows[0]["ColorName2"].ToString()!="")
				{
					model.ColorName2=ds.Tables[0].Rows[0]["ColorName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ColorName3"]!=null && ds.Tables[0].Rows[0]["ColorName3"].ToString()!="")
				{
					model.ColorName3=ds.Tables[0].Rows[0]["ColorName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ColorPicFile"]!=null && ds.Tables[0].Rows[0]["ColorPicFile"].ToString()!="")
				{
					model.ColorPicFile=ds.Tables[0].Rows[0]["ColorPicFile"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ColorID,ColorCode,ColorName1,ColorName2,ColorName3,ColorPicFile ");
			strSql.Append(" FROM Color ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ColorID,ColorCode,ColorName1,ColorName2,ColorName3,ColorPicFile ");
			strSql.Append(" FROM Color ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Color ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ColorID desc");
			}
			strSql.Append(")AS Row, T.*  from Color T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Color";
			parameters[1].Value = "ColorID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

