﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Ord_TradeManually_H
	/// </summary>
	public partial class Ord_TradeManually_H:IOrd_TradeManually_H
	{
		public Ord_TradeManually_H()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string TradeManuallyCode)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_TradeManually_H");
			strSql.Append(" where TradeManuallyCode=@TradeManuallyCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@TradeManuallyCode", SqlDbType.VarChar,64)			};
			parameters[0].Value = TradeManuallyCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Ord_TradeManually_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_TradeManually_H(");
			strSql.Append("TradeManuallyCode,CardNumber,BrandID,Busdate,TenderID,Remark,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate)");
			strSql.Append(" values (");
			strSql.Append("@TradeManuallyCode,@CardNumber,@BrandID,@Busdate,@TenderID,@Remark,@ApprovalCode,@ApproveStatus,@ApproveOn,@ApproveBy,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@CreatedBusDate,@ApproveBusDate)");
			SqlParameter[] parameters = {
					new SqlParameter("@TradeManuallyCode", SqlDbType.VarChar,64),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@Busdate", SqlDbType.DateTime),
					new SqlParameter("@TenderID", SqlDbType.Int,4),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime)};
			parameters[0].Value = model.TradeManuallyCode;
			parameters[1].Value = model.CardNumber;
			parameters[2].Value = model.BrandID;
			parameters[3].Value = model.Busdate;
			parameters[4].Value = model.TenderID;
			parameters[5].Value = model.Remark;
			parameters[6].Value = model.ApprovalCode;
			parameters[7].Value = model.ApproveStatus;
			parameters[8].Value = model.ApproveOn;
			parameters[9].Value = model.ApproveBy;
			parameters[10].Value = model.CreatedOn;
			parameters[11].Value = model.CreatedBy;
			parameters[12].Value = model.UpdatedOn;
			parameters[13].Value = model.UpdatedBy;
			parameters[14].Value = model.CreatedBusDate;
			parameters[15].Value = model.ApproveBusDate;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_TradeManually_H model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_TradeManually_H set ");
			strSql.Append("CardNumber=@CardNumber,");
			strSql.Append("BrandID=@BrandID,");
			strSql.Append("Busdate=@Busdate,");
			strSql.Append("TenderID=@TenderID,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("ApprovalCode=@ApprovalCode,");
			strSql.Append("ApproveStatus=@ApproveStatus,");
			strSql.Append("ApproveOn=@ApproveOn,");
			strSql.Append("ApproveBy=@ApproveBy,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("CreatedBusDate=@CreatedBusDate,");
			strSql.Append("ApproveBusDate=@ApproveBusDate");
			strSql.Append(" where TradeManuallyCode=@TradeManuallyCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@Busdate", SqlDbType.DateTime),
					new SqlParameter("@TenderID", SqlDbType.Int,4),
					new SqlParameter("@Remark", SqlDbType.VarChar,512),
					new SqlParameter("@ApprovalCode", SqlDbType.VarChar,64),
					new SqlParameter("@ApproveStatus", SqlDbType.Char,1),
					new SqlParameter("@ApproveOn", SqlDbType.DateTime),
					new SqlParameter("@ApproveBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CreatedBusDate", SqlDbType.DateTime),
					new SqlParameter("@ApproveBusDate", SqlDbType.DateTime),
					new SqlParameter("@TradeManuallyCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.CardNumber;
			parameters[1].Value = model.BrandID;
			parameters[2].Value = model.Busdate;
			parameters[3].Value = model.TenderID;
			parameters[4].Value = model.Remark;
			parameters[5].Value = model.ApprovalCode;
			parameters[6].Value = model.ApproveStatus;
			parameters[7].Value = model.ApproveOn;
			parameters[8].Value = model.ApproveBy;
			parameters[9].Value = model.CreatedOn;
			parameters[10].Value = model.CreatedBy;
			parameters[11].Value = model.UpdatedOn;
			parameters[12].Value = model.UpdatedBy;
			parameters[13].Value = model.CreatedBusDate;
			parameters[14].Value = model.ApproveBusDate;
			parameters[15].Value = model.TradeManuallyCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string TradeManuallyCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_TradeManually_H ");
			strSql.Append(" where TradeManuallyCode=@TradeManuallyCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@TradeManuallyCode", SqlDbType.VarChar,64)			};
			parameters[0].Value = TradeManuallyCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string TradeManuallyCodelist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_TradeManually_H ");
			strSql.Append(" where TradeManuallyCode in ("+TradeManuallyCodelist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_TradeManually_H GetModel(string TradeManuallyCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TradeManuallyCode,CardNumber,BrandID,Busdate,TenderID,Remark,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate from Ord_TradeManually_H ");
			strSql.Append(" where TradeManuallyCode=@TradeManuallyCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@TradeManuallyCode", SqlDbType.VarChar,64)			};
			parameters[0].Value = TradeManuallyCode;

			Edge.SVA.Model.Ord_TradeManually_H model=new Edge.SVA.Model.Ord_TradeManually_H();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["TradeManuallyCode"]!=null && ds.Tables[0].Rows[0]["TradeManuallyCode"].ToString()!="")
				{
					model.TradeManuallyCode=ds.Tables[0].Rows[0]["TradeManuallyCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardNumber"]!=null && ds.Tables[0].Rows[0]["CardNumber"].ToString()!="")
				{
					model.CardNumber=ds.Tables[0].Rows[0]["CardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BrandID"]!=null && ds.Tables[0].Rows[0]["BrandID"].ToString()!="")
				{
					model.BrandID=int.Parse(ds.Tables[0].Rows[0]["BrandID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Busdate"]!=null && ds.Tables[0].Rows[0]["Busdate"].ToString()!="")
				{
					model.Busdate=DateTime.Parse(ds.Tables[0].Rows[0]["Busdate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenderID"]!=null && ds.Tables[0].Rows[0]["TenderID"].ToString()!="")
				{
					model.TenderID=int.Parse(ds.Tables[0].Rows[0]["TenderID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ApprovalCode"]!=null && ds.Tables[0].Rows[0]["ApprovalCode"].ToString()!="")
				{
					model.ApprovalCode=ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ApproveStatus"]!=null && ds.Tables[0].Rows[0]["ApproveStatus"].ToString()!="")
				{
					model.ApproveStatus=ds.Tables[0].Rows[0]["ApproveStatus"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ApproveOn"]!=null && ds.Tables[0].Rows[0]["ApproveOn"].ToString()!="")
				{
					model.ApproveOn=DateTime.Parse(ds.Tables[0].Rows[0]["ApproveOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ApproveBy"]!=null && ds.Tables[0].Rows[0]["ApproveBy"].ToString()!="")
				{
					model.ApproveBy=int.Parse(ds.Tables[0].Rows[0]["ApproveBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBusDate"]!=null && ds.Tables[0].Rows[0]["CreatedBusDate"].ToString()!="")
				{
					model.CreatedBusDate=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedBusDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ApproveBusDate"]!=null && ds.Tables[0].Rows[0]["ApproveBusDate"].ToString()!="")
				{
					model.ApproveBusDate=DateTime.Parse(ds.Tables[0].Rows[0]["ApproveBusDate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TradeManuallyCode,CardNumber,BrandID,Busdate,TenderID,Remark,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate ");
			strSql.Append(" FROM Ord_TradeManually_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TradeManuallyCode,CardNumber,BrandID,Busdate,TenderID,Remark,ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CreatedBusDate,ApproveBusDate ");
			strSql.Append(" FROM Ord_TradeManually_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_TradeManually_H ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.TradeManuallyCode desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_TradeManually_H T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_TradeManually_H";
			parameters[1].Value = "TradeManuallyCode";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

