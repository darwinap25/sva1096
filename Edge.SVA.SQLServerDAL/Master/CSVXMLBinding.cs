﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CSVXMLBinding
	/// </summary>
	public partial class CSVXMLBinding:ICSVXMLBinding
	{
		public CSVXMLBinding()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.CSVXMLBinding model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into CSVXMLBinding(");
            strSql.Append("BindingCode,Description,Func,CSVColumnName,XMLFieldName,DataType,FixedValue,Length,SaveToDB,CreatedOn, CreatedBy, UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
            strSql.Append("@BindingCode,@Description,@Func,@CSVColumnName,@XMLFieldName,@DataType,@FixedValue,@Length,@SaveToDB, @CreatedOn, @CreatedBy, @UpdatedOn,@UpdatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@BindingCode", SqlDbType.VarChar,64),
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@Func", SqlDbType.VarChar,64),
					new SqlParameter("@CSVColumnName", SqlDbType.VarChar,50),
					new SqlParameter("@XMLFieldName", SqlDbType.VarChar,50),
					new SqlParameter("@DataType", SqlDbType.Int,4),
					new SqlParameter("@FixedValue", SqlDbType.VarChar,512),
					new SqlParameter("@Length", SqlDbType.Int,4),
					new SqlParameter("@SaveToDB", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.BindingCode;
			parameters[1].Value = model.Description;
			parameters[2].Value = model.Func;
			parameters[3].Value = model.CSVColumnName;
			parameters[4].Value = model.XMLFieldName;
			parameters[5].Value = model.DataType;
			parameters[6].Value = model.FixedValue;
			parameters[7].Value = model.Length;
			parameters[8].Value = model.SaveToDB;
			parameters[9].Value = model.CreatedOn;
            parameters[10].Value = model.CreatedBy;
			parameters[11].Value = model.UpdatedOn;
			parameters[12].Value = model.UpdatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.CSVXMLBinding model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("update CSVXMLBinding set ");
            strSql.Append("Description=@Description,");
			strSql.Append("Name=@Name,");
            strSql.Append("Func=@Func,");
            strSql.Append("CSVColumnName=@CSVColumnName,");
            strSql.Append("XMLFieldName=@XMLFieldName,");
            strSql.Append("DataType=@DataType,");
            strSql.Append("FixedValue=@FixedValue,");
            strSql.Append("Length=@Length,");
            strSql.Append("SaveToDB=@SaveToDB,");
			strSql.Append("CreatedOn=@CreatedOn,");
            strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
            strSql.Append(" where BindingCode=@BindingCode ");
			SqlParameter[] parameters = {				
					new SqlParameter("@Description", SqlDbType.VarChar,512),
					new SqlParameter("@Func", SqlDbType.VarChar,64),
					new SqlParameter("@CSVColumnName", SqlDbType.VarChar,50),
					new SqlParameter("@XMLFieldName", SqlDbType.VarChar,50),
					new SqlParameter("@DataType", SqlDbType.Int,4),
					new SqlParameter("@FixedValue", SqlDbType.VarChar,512),
					new SqlParameter("@Length", SqlDbType.Int,4),
					new SqlParameter("@SaveToDB", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@BindingCode", SqlDbType.VarChar,64)};
            parameters[0].Value = model.Description;
            parameters[1].Value = model.Func;
            parameters[2].Value = model.CSVColumnName;
            parameters[3].Value = model.XMLFieldName;
            parameters[4].Value = model.DataType;
            parameters[5].Value = model.FixedValue;
            parameters[6].Value = model.Length;
            parameters[7].Value = model.SaveToDB;
            parameters[8].Value = model.CreatedOn;
            parameters[9].Value = model.CreatedBy;
            parameters[10].Value = model.UpdatedOn;
            parameters[11].Value = model.BindingCode;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            strSql.Append("delete from CSVXMLBinding ");
            strSql.Append(" where BindingCode=@BindingCode  ");
            SqlParameter[] parameters = {
					new SqlParameter("@BindingCode", SqlDbType.VarChar,64)};
            parameters[0].Value = code;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.CSVXMLBinding GetModel(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 BindingCode,Description,Func,CSVColumnName,XMLFieldName,DataType,FixedValue,Length,SaveToDB, CreatedOn, CreatedBy, UpdatedOn,UpdatedBy from CSVXMLBinding ");
            strSql.Append(" where BindingCode=@BindingCode  ");
            SqlParameter[] parameters = {
					new SqlParameter("@BindingCode", SqlDbType.VarChar,64)};
            parameters[0].Value = code;

            Edge.SVA.Model.CSVXMLBinding model = new Edge.SVA.Model.CSVXMLBinding();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                if (ds.Tables[0].Rows[0]["BindingCode"] != null && ds.Tables[0].Rows[0]["BindingCode"].ToString() != "")
				{
                    model.BindingCode = ds.Tables[0].Rows[0]["BindingCode"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Description"] != null && ds.Tables[0].Rows[0]["Description"].ToString() != "")
				{
                    model.Description = ds.Tables[0].Rows[0]["Description"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Func"] != null && ds.Tables[0].Rows[0]["Func"].ToString() != "")
				{
                    model.Func = ds.Tables[0].Rows[0]["Func"].ToString();
				}
                if (ds.Tables[0].Rows[0]["CSVColumnName"] != null && ds.Tables[0].Rows[0]["CSVColumnName"].ToString() != "")
				{
                    model.CSVColumnName = ds.Tables[0].Rows[0]["CSVColumnName"].ToString();
				}
                if (ds.Tables[0].Rows[0]["XMLFieldName"] != null && ds.Tables[0].Rows[0]["XMLFieldName"].ToString() != "")
				{
                    model.XMLFieldName = ds.Tables[0].Rows[0]["XMLFieldName"].ToString();
				}
                if (ds.Tables[0].Rows[0]["DataType"] != null && ds.Tables[0].Rows[0]["DataType"].ToString() != "")
				{
                    model.DataType = int.Parse(ds.Tables[0].Rows[0]["DataType"].ToString());
				}
                if (ds.Tables[0].Rows[0]["FixedValue"] != null && ds.Tables[0].Rows[0]["FixedValue"].ToString() != "")
				{
                    model.FixedValue = ds.Tables[0].Rows[0]["FixedValue"].ToString();
				}
                if (ds.Tables[0].Rows[0]["Length"] != null && ds.Tables[0].Rows[0]["Length"].ToString() != "")
				{
                    model.Length = int.Parse(ds.Tables[0].Rows[0]["Length"].ToString());
				}
                if (ds.Tables[0].Rows[0]["SaveToDB"] != null && ds.Tables[0].Rows[0]["SaveToDB"].ToString() != "")
				{
                    model.SaveToDB = int.Parse(ds.Tables[0].Rows[0]["SaveToDB"].ToString());
				}
                if (ds.Tables[0].Rows[0]["CreatedOn"] != null && ds.Tables[0].Rows[0]["CreatedOn"].ToString() != "")
                {
                    model.CreatedOn = DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CreatedBy"] != null && ds.Tables[0].Rows[0]["CreatedBy"].ToString() != "")
                {
                    model.CreatedBy = int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
                }
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy= int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select BindingCode,Description,Func,CSVColumnName,XMLFieldName,DataType,FixedValue,Length,CreatedOn, CreatedBy, UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM CSVXMLBinding ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" BindingCode,Description,Func,CSVColumnName,XMLFieldName,DataType,FixedValue,Length, SaveToDB,CreatedOn, CreatedBy, UpdatedOn,UpdatedBy ");
            strSql.Append(" FROM CSVXMLBinding ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CSVXMLBinding";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CSVXMLBinding ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

