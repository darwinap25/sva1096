﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:School
	/// </summary>
	public partial class School:ISchool
	{
		public School()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SchoolID", "School"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SchoolID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from School");
			strSql.Append(" where SchoolID=@SchoolID");
			SqlParameter[] parameters = {
					new SqlParameter("@SchoolID", SqlDbType.Int,4)
			};
			parameters[0].Value = SchoolID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.School model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into School(");
			strSql.Append("SchoolCode,SchoolName1,SchoolName2,SchoolName3,SchoolArea1,SchoolArea2,SchoolArea3,SchoolDistrict1,SchoolDistrict2,SchoolDistrict3,SchoolNote,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@SchoolCode,@SchoolName1,@SchoolName2,@SchoolName3,@SchoolArea1,@SchoolArea2,@SchoolArea3,@SchoolDistrict1,@SchoolDistrict2,@SchoolDistrict3,@SchoolNote,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SchoolCode", SqlDbType.VarChar,64),
					new SqlParameter("@SchoolName1", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolName2", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolName3", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolArea1", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolArea2", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolArea3", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolDistrict1", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolDistrict2", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolDistrict3", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolNote", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.SchoolCode;
			parameters[1].Value = model.SchoolName1;
			parameters[2].Value = model.SchoolName2;
			parameters[3].Value = model.SchoolName3;
			parameters[4].Value = model.SchoolArea1;
			parameters[5].Value = model.SchoolArea2;
			parameters[6].Value = model.SchoolArea3;
			parameters[7].Value = model.SchoolDistrict1;
			parameters[8].Value = model.SchoolDistrict2;
			parameters[9].Value = model.SchoolDistrict3;
			parameters[10].Value = model.SchoolNote;
			parameters[11].Value = model.CreatedOn;
			parameters[12].Value = model.CreatedBy;
			parameters[13].Value = model.UpdatedOn;
			parameters[14].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.School model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update School set ");
			strSql.Append("SchoolCode=@SchoolCode,");
			strSql.Append("SchoolName1=@SchoolName1,");
			strSql.Append("SchoolName2=@SchoolName2,");
			strSql.Append("SchoolName3=@SchoolName3,");
			strSql.Append("SchoolArea1=@SchoolArea1,");
			strSql.Append("SchoolArea2=@SchoolArea2,");
			strSql.Append("SchoolArea3=@SchoolArea3,");
			strSql.Append("SchoolDistrict1=@SchoolDistrict1,");
			strSql.Append("SchoolDistrict2=@SchoolDistrict2,");
			strSql.Append("SchoolDistrict3=@SchoolDistrict3,");
			strSql.Append("SchoolNote=@SchoolNote,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where SchoolID=@SchoolID");
			SqlParameter[] parameters = {
					new SqlParameter("@SchoolCode", SqlDbType.VarChar,64),
					new SqlParameter("@SchoolName1", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolName2", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolName3", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolArea1", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolArea2", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolArea3", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolDistrict1", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolDistrict2", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolDistrict3", SqlDbType.NVarChar,512),
					new SqlParameter("@SchoolNote", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@SchoolID", SqlDbType.Int,4)};
			parameters[0].Value = model.SchoolCode;
			parameters[1].Value = model.SchoolName1;
			parameters[2].Value = model.SchoolName2;
			parameters[3].Value = model.SchoolName3;
			parameters[4].Value = model.SchoolArea1;
			parameters[5].Value = model.SchoolArea2;
			parameters[6].Value = model.SchoolArea3;
			parameters[7].Value = model.SchoolDistrict1;
			parameters[8].Value = model.SchoolDistrict2;
			parameters[9].Value = model.SchoolDistrict3;
			parameters[10].Value = model.SchoolNote;
			parameters[11].Value = model.CreatedOn;
			parameters[12].Value = model.CreatedBy;
			parameters[13].Value = model.UpdatedOn;
			parameters[14].Value = model.UpdatedBy;
			parameters[15].Value = model.SchoolID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SchoolID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from School ");
			strSql.Append(" where SchoolID=@SchoolID");
			SqlParameter[] parameters = {
					new SqlParameter("@SchoolID", SqlDbType.Int,4)
			};
			parameters[0].Value = SchoolID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SchoolIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from School ");
			strSql.Append(" where SchoolID in ("+SchoolIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.School GetModel(int SchoolID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SchoolID,SchoolCode,SchoolName1,SchoolName2,SchoolName3,SchoolArea1,SchoolArea2,SchoolArea3,SchoolDistrict1,SchoolDistrict2,SchoolDistrict3,SchoolNote,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from School ");
			strSql.Append(" where SchoolID=@SchoolID");
			SqlParameter[] parameters = {
					new SqlParameter("@SchoolID", SqlDbType.Int,4)
			};
			parameters[0].Value = SchoolID;

			Edge.SVA.Model.School model=new Edge.SVA.Model.School();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SchoolID"]!=null && ds.Tables[0].Rows[0]["SchoolID"].ToString()!="")
				{
					model.SchoolID=int.Parse(ds.Tables[0].Rows[0]["SchoolID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SchoolCode"]!=null && ds.Tables[0].Rows[0]["SchoolCode"].ToString()!="")
				{
					model.SchoolCode=ds.Tables[0].Rows[0]["SchoolCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolName1"]!=null && ds.Tables[0].Rows[0]["SchoolName1"].ToString()!="")
				{
					model.SchoolName1=ds.Tables[0].Rows[0]["SchoolName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolName2"]!=null && ds.Tables[0].Rows[0]["SchoolName2"].ToString()!="")
				{
					model.SchoolName2=ds.Tables[0].Rows[0]["SchoolName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolName3"]!=null && ds.Tables[0].Rows[0]["SchoolName3"].ToString()!="")
				{
					model.SchoolName3=ds.Tables[0].Rows[0]["SchoolName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolArea1"]!=null && ds.Tables[0].Rows[0]["SchoolArea1"].ToString()!="")
				{
					model.SchoolArea1=ds.Tables[0].Rows[0]["SchoolArea1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolArea2"]!=null && ds.Tables[0].Rows[0]["SchoolArea2"].ToString()!="")
				{
					model.SchoolArea2=ds.Tables[0].Rows[0]["SchoolArea2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolArea3"]!=null && ds.Tables[0].Rows[0]["SchoolArea3"].ToString()!="")
				{
					model.SchoolArea3=ds.Tables[0].Rows[0]["SchoolArea3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolDistrict1"]!=null && ds.Tables[0].Rows[0]["SchoolDistrict1"].ToString()!="")
				{
					model.SchoolDistrict1=ds.Tables[0].Rows[0]["SchoolDistrict1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolDistrict2"]!=null && ds.Tables[0].Rows[0]["SchoolDistrict2"].ToString()!="")
				{
					model.SchoolDistrict2=ds.Tables[0].Rows[0]["SchoolDistrict2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolDistrict3"]!=null && ds.Tables[0].Rows[0]["SchoolDistrict3"].ToString()!="")
				{
					model.SchoolDistrict3=ds.Tables[0].Rows[0]["SchoolDistrict3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SchoolNote"]!=null && ds.Tables[0].Rows[0]["SchoolNote"].ToString()!="")
				{
					model.SchoolNote=ds.Tables[0].Rows[0]["SchoolNote"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SchoolID,SchoolCode,SchoolName1,SchoolName2,SchoolName3,SchoolArea1,SchoolArea2,SchoolArea3,SchoolDistrict1,SchoolDistrict2,SchoolDistrict3,SchoolNote,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM School ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SchoolID,SchoolCode,SchoolName1,SchoolName2,SchoolName3,SchoolArea1,SchoolArea2,SchoolArea3,SchoolDistrict1,SchoolDistrict2,SchoolDistrict3,SchoolNote,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM School ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM School ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SchoolID desc");
			}
			strSql.Append(")AS Row, T.*  from School T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "School";
			parameters[1].Value = "SchoolID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

