﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CourierPosition
	/// </summary>
	public partial class CourierPosition:ICourierPosition
	{
		public CourierPosition()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CourierCode)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CourierPosition");
			strSql.Append(" where CourierCode=@CourierCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@CourierCode", SqlDbType.VarChar,512)};
			parameters[0].Value = CourierCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CourierPosition model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CourierPosition(");
			strSql.Append("CourierCode,CourierName,Status,Longitude,latitude,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CourierCode,@CourierName,@Status,@Longitude,@latitude,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@CourierCode", SqlDbType.VarChar,512),
					new SqlParameter("@CourierName", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Longitude", SqlDbType.VarChar,512),
					new SqlParameter("@latitude", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,512)};
			parameters[0].Value = model.CourierCode;
			parameters[1].Value = model.CourierName;
			parameters[2].Value = model.Status;
			parameters[3].Value = model.Longitude;
			parameters[4].Value = model.latitude;
			parameters[5].Value = model.CreatedOn;
			parameters[6].Value = model.CreatedBy;
			parameters[7].Value = model.UpdatedOn;
			parameters[8].Value = model.UpdatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CourierPosition model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CourierPosition set ");
			strSql.Append("CourierName=@CourierName,");
			strSql.Append("Status=@Status,");
			strSql.Append("Longitude=@Longitude,");
			strSql.Append("latitude=@latitude,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where CourierCode=@CourierCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@CourierName", SqlDbType.NVarChar,512),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Longitude", SqlDbType.VarChar,512),
					new SqlParameter("@latitude", SqlDbType.VarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.VarChar,512),
					new SqlParameter("@CourierCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.CourierName;
			parameters[1].Value = model.Status;
			parameters[2].Value = model.Longitude;
			parameters[3].Value = model.latitude;
			parameters[4].Value = model.CreatedOn;
			parameters[5].Value = model.CreatedBy;
			parameters[6].Value = model.UpdatedOn;
			parameters[7].Value = model.UpdatedBy;
			parameters[8].Value = model.CourierCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CourierCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CourierPosition ");
			strSql.Append(" where CourierCode=@CourierCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@CourierCode", SqlDbType.VarChar,512)};
			parameters[0].Value = CourierCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CourierCodelist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CourierPosition ");
			strSql.Append(" where CourierCode in ("+CourierCodelist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CourierPosition GetModel(string CourierCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CourierCode,CourierName,Status,Longitude,latitude,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from CourierPosition ");
			strSql.Append(" where CourierCode=@CourierCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@CourierCode", SqlDbType.VarChar,512)};
			parameters[0].Value = CourierCode;

			Edge.SVA.Model.CourierPosition model=new Edge.SVA.Model.CourierPosition();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CourierCode"]!=null && ds.Tables[0].Rows[0]["CourierCode"].ToString()!="")
				{
					model.CourierCode=ds.Tables[0].Rows[0]["CourierCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CourierName"]!=null && ds.Tables[0].Rows[0]["CourierName"].ToString()!="")
				{
					model.CourierName=ds.Tables[0].Rows[0]["CourierName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Longitude"]!=null && ds.Tables[0].Rows[0]["Longitude"].ToString()!="")
				{
					model.Longitude=ds.Tables[0].Rows[0]["Longitude"].ToString();
				}
				if(ds.Tables[0].Rows[0]["latitude"]!=null && ds.Tables[0].Rows[0]["latitude"].ToString()!="")
				{
					model.latitude=ds.Tables[0].Rows[0]["latitude"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=ds.Tables[0].Rows[0]["UpdatedBy"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CourierCode,CourierName,Status,Longitude,latitude,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM CourierPosition ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CourierCode,CourierName,Status,Longitude,latitude,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM CourierPosition ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
			parameters[0].Value = "CourierPosition";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CourierPosition ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

