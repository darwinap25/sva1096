﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Customer
	/// </summary>
	public partial class Customer:ICustomer
	{
		public Customer()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CustomerID", "Customer"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CustomerCode,int CustomerID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Customer");
			strSql.Append(" where CustomerCode=@CustomerCode and CustomerID=@CustomerID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomerCode", SqlDbType.VarChar,512),
					new SqlParameter("@CustomerID", SqlDbType.Int,4)};
			parameters[0].Value = CustomerCode;
			parameters[1].Value = CustomerID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Customer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Customer(");
			strSql.Append("CustomerCode,CustomerDesc1,CustomerDesc2,CustomerDesc3,CustomerAddress,Contact,ContactPhone,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CustomerCode,@CustomerDesc1,@CustomerDesc2,@CustomerDesc3,@CustomerAddress,@Contact,@ContactPhone,@Remark,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomerCode", SqlDbType.VarChar,512),
					new SqlParameter("@CustomerDesc1", SqlDbType.NVarChar,512),
					new SqlParameter("@CustomerDesc2", SqlDbType.NVarChar,512),
					new SqlParameter("@CustomerDesc3", SqlDbType.NVarChar,512),
					new SqlParameter("@CustomerAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@Contact", SqlDbType.NVarChar,512),
					new SqlParameter("@ContactPhone", SqlDbType.NVarChar,512),
					new SqlParameter("@Remark", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CustomerCode;
			parameters[1].Value = model.CustomerDesc1;
			parameters[2].Value = model.CustomerDesc2;
			parameters[3].Value = model.CustomerDesc3;
			parameters[4].Value = model.CustomerAddress;
			parameters[5].Value = model.Contact;
			parameters[6].Value = model.ContactPhone;
			parameters[7].Value = model.Remark;
			parameters[8].Value = model.CreatedOn;
			parameters[9].Value = model.CreatedBy;
			parameters[10].Value = model.UpdatedOn;
			parameters[11].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Customer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Customer set ");
			strSql.Append("CustomerDesc1=@CustomerDesc1,");
			strSql.Append("CustomerDesc2=@CustomerDesc2,");
			strSql.Append("CustomerDesc3=@CustomerDesc3,");
			strSql.Append("CustomerAddress=@CustomerAddress,");
			strSql.Append("Contact=@Contact,");
			strSql.Append("ContactPhone=@ContactPhone,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
            strSql.Append("CustomerCode=@CustomerCode");
			strSql.Append(" where CustomerID=@CustomerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomerDesc1", SqlDbType.NVarChar,512),
					new SqlParameter("@CustomerDesc2", SqlDbType.NVarChar,512),
					new SqlParameter("@CustomerDesc3", SqlDbType.NVarChar,512),
					new SqlParameter("@CustomerAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@Contact", SqlDbType.NVarChar,512),
					new SqlParameter("@ContactPhone", SqlDbType.NVarChar,512),
					new SqlParameter("@Remark", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CustomerID", SqlDbType.Int,4),
					new SqlParameter("@CustomerCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.CustomerDesc1;
			parameters[1].Value = model.CustomerDesc2;
			parameters[2].Value = model.CustomerDesc3;
			parameters[3].Value = model.CustomerAddress;
			parameters[4].Value = model.Contact;
			parameters[5].Value = model.ContactPhone;
			parameters[6].Value = model.Remark;
			parameters[7].Value = model.CreatedOn;
			parameters[8].Value = model.CreatedBy;
			parameters[9].Value = model.UpdatedOn;
			parameters[10].Value = model.UpdatedBy;
			parameters[11].Value = model.CustomerID;
			parameters[12].Value = model.CustomerCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CustomerID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Customer ");
			strSql.Append(" where CustomerID=@CustomerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomerID", SqlDbType.Int,4)
};
			parameters[0].Value = CustomerID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CustomerCode,int CustomerID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Customer ");
			strSql.Append(" where CustomerCode=@CustomerCode and CustomerID=@CustomerID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomerCode", SqlDbType.VarChar,512),
					new SqlParameter("@CustomerID", SqlDbType.Int,4)};
			parameters[0].Value = CustomerCode;
			parameters[1].Value = CustomerID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CustomerIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Customer ");
			strSql.Append(" where CustomerID in ("+CustomerIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Customer GetModel(int CustomerID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CustomerID,CustomerCode,CustomerDesc1,CustomerDesc2,CustomerDesc3,CustomerAddress,Contact,ContactPhone,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Customer ");
			strSql.Append(" where CustomerID=@CustomerID");
			SqlParameter[] parameters = {
					new SqlParameter("@CustomerID", SqlDbType.Int,4)
};
			parameters[0].Value = CustomerID;

			Edge.SVA.Model.Customer model=new Edge.SVA.Model.Customer();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CustomerID"]!=null && ds.Tables[0].Rows[0]["CustomerID"].ToString()!="")
				{
					model.CustomerID=int.Parse(ds.Tables[0].Rows[0]["CustomerID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CustomerCode"]!=null && ds.Tables[0].Rows[0]["CustomerCode"].ToString()!="")
				{
					model.CustomerCode=ds.Tables[0].Rows[0]["CustomerCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CustomerDesc1"]!=null && ds.Tables[0].Rows[0]["CustomerDesc1"].ToString()!="")
				{
					model.CustomerDesc1=ds.Tables[0].Rows[0]["CustomerDesc1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CustomerDesc2"]!=null && ds.Tables[0].Rows[0]["CustomerDesc2"].ToString()!="")
				{
					model.CustomerDesc2=ds.Tables[0].Rows[0]["CustomerDesc2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CustomerDesc3"]!=null && ds.Tables[0].Rows[0]["CustomerDesc3"].ToString()!="")
				{
					model.CustomerDesc3=ds.Tables[0].Rows[0]["CustomerDesc3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CustomerAddress"]!=null && ds.Tables[0].Rows[0]["CustomerAddress"].ToString()!="")
				{
					model.CustomerAddress=ds.Tables[0].Rows[0]["CustomerAddress"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Contact"]!=null && ds.Tables[0].Rows[0]["Contact"].ToString()!="")
				{
					model.Contact=ds.Tables[0].Rows[0]["Contact"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ContactPhone"]!=null && ds.Tables[0].Rows[0]["ContactPhone"].ToString()!="")
				{
					model.ContactPhone=ds.Tables[0].Rows[0]["ContactPhone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CustomerID,CustomerCode,CustomerDesc1,CustomerDesc2,CustomerDesc3,CustomerAddress,Contact,ContactPhone,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Customer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CustomerID,CustomerCode,CustomerDesc1,CustomerDesc2,CustomerDesc3,CustomerAddress,Contact,ContactPhone,Remark,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Customer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            int OrderType = 0;
            string OrderField = filedOrder;
            if (filedOrder.ToLower().EndsWith(" desc"))
            {
                OrderType = 1;
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" desc"));
            }
            else if (filedOrder.ToLower().EndsWith(" asc"))
            {
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" asc"));
            }
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Customer";
            parameters[1].Value = "*";
            parameters[2].Value = OrderField;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = OrderType;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from Customer ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

