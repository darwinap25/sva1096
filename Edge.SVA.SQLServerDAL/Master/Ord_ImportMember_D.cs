﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Ord_ImportMember_D
	/// </summary>
	public partial class Ord_ImportMember_D:IOrd_ImportMember_D
	{
		public Ord_ImportMember_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Ord_ImportMember_D"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_ImportMember_D");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Ord_ImportMember_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_ImportMember_D(");
			strSql.Append("ImportMemberNumber,RRCCardNumber,OLDCardNumber,OLDRRCCardNumber,LName,FName,VPPTS,RetailID,StoreID,CardStatus)");
			strSql.Append(" values (");
			strSql.Append("@ImportMemberNumber,@RRCCardNumber,@OLDCardNumber,@OLDRRCCardNumber,@LName,@FName,@VPPTS,@RetailID,@StoreID,@CardStatus)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportMemberNumber", SqlDbType.VarChar,64),
					new SqlParameter("@RRCCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@OLDCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@OLDRRCCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@LName", SqlDbType.VarChar,512),
					new SqlParameter("@FName", SqlDbType.VarChar,512),
					new SqlParameter("@VPPTS", SqlDbType.VarChar,64),
					new SqlParameter("@RetailID", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.VarChar,64),
					new SqlParameter("@CardStatus", SqlDbType.Int,4)};
			parameters[0].Value = model.ImportMemberNumber;
			parameters[1].Value = model.RRCCardNumber;
			parameters[2].Value = model.OLDCardNumber;
			parameters[3].Value = model.OLDRRCCardNumber;
			parameters[4].Value = model.LName;
			parameters[5].Value = model.FName;
			parameters[6].Value = model.VPPTS;
			parameters[7].Value = model.RetailID;
			parameters[8].Value = model.StoreID;
			parameters[9].Value = model.CardStatus;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_ImportMember_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_ImportMember_D set ");
			strSql.Append("ImportMemberNumber=@ImportMemberNumber,");
			strSql.Append("RRCCardNumber=@RRCCardNumber,");
			strSql.Append("OLDCardNumber=@OLDCardNumber,");
			strSql.Append("OLDRRCCardNumber=@OLDRRCCardNumber,");
			strSql.Append("LName=@LName,");
			strSql.Append("FName=@FName,");
			strSql.Append("VPPTS=@VPPTS,");
			strSql.Append("RetailID=@RetailID,");
			strSql.Append("StoreID=@StoreID,");
			strSql.Append("CardStatus=@CardStatus");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportMemberNumber", SqlDbType.VarChar,64),
					new SqlParameter("@RRCCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@OLDCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@OLDRRCCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@LName", SqlDbType.VarChar,512),
					new SqlParameter("@FName", SqlDbType.VarChar,512),
					new SqlParameter("@VPPTS", SqlDbType.VarChar,64),
					new SqlParameter("@RetailID", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.VarChar,64),
					new SqlParameter("@CardStatus", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.ImportMemberNumber;
			parameters[1].Value = model.RRCCardNumber;
			parameters[2].Value = model.OLDCardNumber;
			parameters[3].Value = model.OLDRRCCardNumber;
			parameters[4].Value = model.LName;
			parameters[5].Value = model.FName;
			parameters[6].Value = model.VPPTS;
			parameters[7].Value = model.RetailID;
			parameters[8].Value = model.StoreID;
			parameters[9].Value = model.CardStatus;
			parameters[10].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_ImportMember_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_ImportMember_D ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_ImportMember_D GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,ImportMemberNumber,RRCCardNumber,OLDCardNumber,OLDRRCCardNumber,LName,FName,VPPTS,RetailID,StoreID,CardStatus from Ord_ImportMember_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Ord_ImportMember_D model=new Edge.SVA.Model.Ord_ImportMember_D();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["KeyID"]!=null && ds.Tables[0].Rows[0]["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ImportMemberNumber"]!=null && ds.Tables[0].Rows[0]["ImportMemberNumber"].ToString()!="")
				{
					model.ImportMemberNumber=ds.Tables[0].Rows[0]["ImportMemberNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RRCCardNumber"]!=null && ds.Tables[0].Rows[0]["RRCCardNumber"].ToString()!="")
				{
					model.RRCCardNumber=ds.Tables[0].Rows[0]["RRCCardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OLDCardNumber"]!=null && ds.Tables[0].Rows[0]["OLDCardNumber"].ToString()!="")
				{
					model.OLDCardNumber=ds.Tables[0].Rows[0]["OLDCardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OLDRRCCardNumber"]!=null && ds.Tables[0].Rows[0]["OLDRRCCardNumber"].ToString()!="")
				{
					model.OLDRRCCardNumber=ds.Tables[0].Rows[0]["OLDRRCCardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LName"]!=null && ds.Tables[0].Rows[0]["LName"].ToString()!="")
				{
					model.LName=ds.Tables[0].Rows[0]["LName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FName"]!=null && ds.Tables[0].Rows[0]["FName"].ToString()!="")
				{
					model.FName=ds.Tables[0].Rows[0]["FName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["VPPTS"]!=null && ds.Tables[0].Rows[0]["VPPTS"].ToString()!="")
				{
					model.VPPTS=ds.Tables[0].Rows[0]["VPPTS"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RetailID"]!=null && ds.Tables[0].Rows[0]["RetailID"].ToString()!="")
				{
					model.RetailID=ds.Tables[0].Rows[0]["RetailID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["StoreID"]!=null && ds.Tables[0].Rows[0]["StoreID"].ToString()!="")
				{
					model.StoreID=ds.Tables[0].Rows[0]["StoreID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardStatus"]!=null && ds.Tables[0].Rows[0]["CardStatus"].ToString()!="")
				{
					model.CardStatus=int.Parse(ds.Tables[0].Rows[0]["CardStatus"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,ImportMemberNumber,RRCCardNumber,OLDCardNumber,OLDRRCCardNumber,LName,FName,VPPTS,RetailID,StoreID,CardStatus ");
			strSql.Append(" FROM Ord_ImportMember_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,ImportMemberNumber,RRCCardNumber,OLDCardNumber,OLDRRCCardNumber,LName,FName,VPPTS,RetailID,StoreID,CardStatus ");
			strSql.Append(" FROM Ord_ImportMember_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_ImportMember_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.KeyID desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_ImportMember_D T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_ImportMember_D";
			parameters[1].Value = "KeyID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

