﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CardUIDMap
	/// </summary>
	public partial class CardUIDMap:ICardUIDMap
	{
		public CardUIDMap()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CardUIDMap");
			strSql.Append(" where CardUID=@CardUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardUID", SqlDbType.VarChar,512)};
			parameters[0].Value = CardUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CardUIDMap model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CardUIDMap(");
			strSql.Append("CardUID,ImportCardNumber,BatchCardID,CardGradeID,CardNumber,Status,CreatedOn,CreatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CardUID,@ImportCardNumber,@BatchCardID,@CardGradeID,@CardNumber,@Status,@CreatedOn,@CreatedBy)");
			SqlParameter[] parameters = {
					new SqlParameter("@CardUID", SqlDbType.VarChar,512),
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@BatchCardID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CardUID;
			parameters[1].Value = model.ImportCardNumber;
			parameters[2].Value = model.BatchCardID;
			parameters[3].Value = model.CardGradeID;
			parameters[4].Value = model.CardNumber;
			parameters[5].Value = model.Status;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.CreatedBy;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardUIDMap model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CardUIDMap set ");
			strSql.Append("ImportCardNumber=@ImportCardNumber,");
			strSql.Append("BatchCardID=@BatchCardID,");
			strSql.Append("CardGradeID=@CardGradeID,");
			strSql.Append("CardNumber=@CardNumber,");
			strSql.Append("Status=@Status,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy");
			strSql.Append(" where CardUID=@CardUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ImportCardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@BatchCardID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4),
					new SqlParameter("@CardNumber", SqlDbType.VarChar,64),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@CardUID", SqlDbType.VarChar,512)};
			parameters[0].Value = model.ImportCardNumber;
			parameters[1].Value = model.BatchCardID;
			parameters[2].Value = model.CardGradeID;
			parameters[3].Value = model.CardNumber;
			parameters[4].Value = model.Status;
			parameters[5].Value = model.CreatedOn;
			parameters[6].Value = model.CreatedBy;
			parameters[7].Value = model.CardUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardUIDMap ");
			strSql.Append(" where CardUID=@CardUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardUID", SqlDbType.VarChar,512)};
			parameters[0].Value = CardUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CardUIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CardUIDMap ");
			strSql.Append(" where CardUID in ("+CardUIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardUIDMap GetModel(string CardUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CardUID,ImportCardNumber,BatchCardID,CardGradeID,CardNumber,Status,CreatedOn,CreatedBy from CardUIDMap ");
			strSql.Append(" where CardUID=@CardUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardUID", SqlDbType.VarChar,512)};
			parameters[0].Value = CardUID;

			Edge.SVA.Model.CardUIDMap model=new Edge.SVA.Model.CardUIDMap();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CardUID"]!=null && ds.Tables[0].Rows[0]["CardUID"].ToString()!="")
				{
					model.CardUID=ds.Tables[0].Rows[0]["CardUID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ImportCardNumber"]!=null && ds.Tables[0].Rows[0]["ImportCardNumber"].ToString()!="")
				{
					model.ImportCardNumber=ds.Tables[0].Rows[0]["ImportCardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BatchCardID"]!=null && ds.Tables[0].Rows[0]["BatchCardID"].ToString()!="")
				{
					model.BatchCardID=int.Parse(ds.Tables[0].Rows[0]["BatchCardID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardGradeID"]!=null && ds.Tables[0].Rows[0]["CardGradeID"].ToString()!="")
				{
					model.CardGradeID=int.Parse(ds.Tables[0].Rows[0]["CardGradeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardNumber"]!=null && ds.Tables[0].Rows[0]["CardNumber"].ToString()!="")
				{
					model.CardNumber=ds.Tables[0].Rows[0]["CardNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CardUID,ImportCardNumber,BatchCardID,CardGradeID,CardNumber,Status,CreatedOn,CreatedBy ");
			strSql.Append(" FROM CardUIDMap ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CardUID,ImportCardNumber,BatchCardID,CardGradeID,CardNumber,Status,CreatedOn,CreatedBy ");
			strSql.Append(" FROM CardUIDMap ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CardUIDMap";
            parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CardUIDMap ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }

		#endregion  Method
	}
}

