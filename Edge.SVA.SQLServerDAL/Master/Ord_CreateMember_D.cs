﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Ord_CreateMember_D
	/// </summary>
	public partial class Ord_CreateMember_D:IOrd_CreateMember_D
	{
		public Ord_CreateMember_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("KeyID", "Ord_CreateMember_D"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ord_CreateMember_D");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Ord_CreateMember_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ord_CreateMember_D(");
			strSql.Append("CreateMemberNumber,CountryCode,MobileNumber,EngFamilyName,EngGivenName,ChiFamilyName,ChiGivenName,Birthday,Gender,HomeAddress,Email,Facebook,QQ,MSN,Weibo,OtherContact)");
			strSql.Append(" values (");
			strSql.Append("@CreateMemberNumber,@CountryCode,@MobileNumber,@EngFamilyName,@EngGivenName,@ChiFamilyName,@ChiGivenName,@Birthday,@Gender,@HomeAddress,@Email,@Facebook,@QQ,@MSN,@Weibo,@OtherContact)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CreateMemberNumber", SqlDbType.VarChar,64),
					new SqlParameter("@CountryCode", SqlDbType.VarChar,64),
					new SqlParameter("@MobileNumber", SqlDbType.VarChar,64),
					new SqlParameter("@EngFamilyName", SqlDbType.VarChar,64),
					new SqlParameter("@EngGivenName", SqlDbType.VarChar,64),
					new SqlParameter("@ChiFamilyName", SqlDbType.VarChar,64),
					new SqlParameter("@ChiGivenName", SqlDbType.VarChar,64),
					new SqlParameter("@Birthday", SqlDbType.DateTime),
					new SqlParameter("@Gender", SqlDbType.Int,4),
					new SqlParameter("@HomeAddress", SqlDbType.VarChar,64),
					new SqlParameter("@Email", SqlDbType.VarChar,64),
					new SqlParameter("@Facebook", SqlDbType.VarChar,64),
					new SqlParameter("@QQ", SqlDbType.VarChar,64),
					new SqlParameter("@MSN", SqlDbType.VarChar,64),
					new SqlParameter("@Weibo", SqlDbType.VarChar,64),
					new SqlParameter("@OtherContact", SqlDbType.VarChar,64)};
			parameters[0].Value = model.CreateMemberNumber;
			parameters[1].Value = model.CountryCode;
			parameters[2].Value = model.MobileNumber;
			parameters[3].Value = model.EngFamilyName;
			parameters[4].Value = model.EngGivenName;
			parameters[5].Value = model.ChiFamilyName;
			parameters[6].Value = model.ChiGivenName;
            parameters[7].Value = DateTime.Parse(model.Birthday.ToString()).Year <= 1900 ? Convert.ToDateTime("1900-1-1") : model.Birthday;
			parameters[8].Value = model.Gender;
			parameters[9].Value = model.HomeAddress;
			parameters[10].Value = model.Email;
			parameters[11].Value = model.Facebook;
			parameters[12].Value = model.QQ;
			parameters[13].Value = model.MSN;
			parameters[14].Value = model.Weibo;
			parameters[15].Value = model.OtherContact;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CreateMember_D model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ord_CreateMember_D set ");
			strSql.Append("CreateMemberNumber=@CreateMemberNumber,");
			strSql.Append("CountryCode=@CountryCode,");
			strSql.Append("MobileNumber=@MobileNumber,");
			strSql.Append("EngFamilyName=@EngFamilyName,");
			strSql.Append("EngGivenName=@EngGivenName,");
			strSql.Append("ChiFamilyName=@ChiFamilyName,");
			strSql.Append("ChiGivenName=@ChiGivenName,");
			strSql.Append("Birthday=@Birthday,");
			strSql.Append("Gender=@Gender,");
			strSql.Append("HomeAddress=@HomeAddress,");
			strSql.Append("Email=@Email,");
			strSql.Append("Facebook=@Facebook,");
			strSql.Append("QQ=@QQ,");
			strSql.Append("MSN=@MSN,");
			strSql.Append("Weibo=@Weibo,");
			strSql.Append("OtherContact=@OtherContact");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CreateMemberNumber", SqlDbType.VarChar,64),
					new SqlParameter("@CountryCode", SqlDbType.VarChar,64),
					new SqlParameter("@MobileNumber", SqlDbType.VarChar,64),
					new SqlParameter("@EngFamilyName", SqlDbType.VarChar,64),
					new SqlParameter("@EngGivenName", SqlDbType.VarChar,64),
					new SqlParameter("@ChiFamilyName", SqlDbType.VarChar,64),
					new SqlParameter("@ChiGivenName", SqlDbType.VarChar,64),
					new SqlParameter("@Birthday", SqlDbType.DateTime),
					new SqlParameter("@Gender", SqlDbType.Int,4),
					new SqlParameter("@HomeAddress", SqlDbType.VarChar,64),
					new SqlParameter("@Email", SqlDbType.VarChar,64),
					new SqlParameter("@Facebook", SqlDbType.VarChar,64),
					new SqlParameter("@QQ", SqlDbType.VarChar,64),
					new SqlParameter("@MSN", SqlDbType.VarChar,64),
					new SqlParameter("@Weibo", SqlDbType.VarChar,64),
					new SqlParameter("@OtherContact", SqlDbType.VarChar,64),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
			parameters[0].Value = model.CreateMemberNumber;
			parameters[1].Value = model.CountryCode;
			parameters[2].Value = model.MobileNumber;
			parameters[3].Value = model.EngFamilyName;
			parameters[4].Value = model.EngGivenName;
			parameters[5].Value = model.ChiFamilyName;
			parameters[6].Value = model.ChiGivenName;
			parameters[7].Value = model.Birthday;
			parameters[8].Value = model.Gender;
			parameters[9].Value = model.HomeAddress;
			parameters[10].Value = model.Email;
			parameters[11].Value = model.Facebook;
			parameters[12].Value = model.QQ;
			parameters[13].Value = model.MSN;
			parameters[14].Value = model.Weibo;
			parameters[15].Value = model.OtherContact;
			parameters[16].Value = model.KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_CreateMember_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ord_CreateMember_D ");
			strSql.Append(" where KeyID in ("+KeyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CreateMember_D GetModel(int KeyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 KeyID,CreateMemberNumber,CountryCode,MobileNumber,EngFamilyName,EngGivenName,ChiFamilyName,ChiGivenName,Birthday,Gender,HomeAddress,Email,Facebook,QQ,MSN,Weibo,OtherContact from Ord_CreateMember_D ");
			strSql.Append(" where KeyID=@KeyID");
			SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
			parameters[0].Value = KeyID;

			Edge.SVA.Model.Ord_CreateMember_D model=new Edge.SVA.Model.Ord_CreateMember_D();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["KeyID"]!=null && ds.Tables[0].Rows[0]["KeyID"].ToString()!="")
				{
					model.KeyID=int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateMemberNumber"]!=null && ds.Tables[0].Rows[0]["CreateMemberNumber"].ToString()!="")
				{
					model.CreateMemberNumber=ds.Tables[0].Rows[0]["CreateMemberNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CountryCode"]!=null && ds.Tables[0].Rows[0]["CountryCode"].ToString()!="")
				{
					model.CountryCode=ds.Tables[0].Rows[0]["CountryCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MobileNumber"]!=null && ds.Tables[0].Rows[0]["MobileNumber"].ToString()!="")
				{
					model.MobileNumber=ds.Tables[0].Rows[0]["MobileNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EngFamilyName"]!=null && ds.Tables[0].Rows[0]["EngFamilyName"].ToString()!="")
				{
					model.EngFamilyName=ds.Tables[0].Rows[0]["EngFamilyName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EngGivenName"]!=null && ds.Tables[0].Rows[0]["EngGivenName"].ToString()!="")
				{
					model.EngGivenName=ds.Tables[0].Rows[0]["EngGivenName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ChiFamilyName"]!=null && ds.Tables[0].Rows[0]["ChiFamilyName"].ToString()!="")
				{
					model.ChiFamilyName=ds.Tables[0].Rows[0]["ChiFamilyName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ChiGivenName"]!=null && ds.Tables[0].Rows[0]["ChiGivenName"].ToString()!="")
				{
					model.ChiGivenName=ds.Tables[0].Rows[0]["ChiGivenName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Birthday"]!=null && ds.Tables[0].Rows[0]["Birthday"].ToString()!="")
				{
					model.Birthday=DateTime.Parse(ds.Tables[0].Rows[0]["Birthday"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Gender"]!=null && ds.Tables[0].Rows[0]["Gender"].ToString()!="")
				{
					model.Gender=int.Parse(ds.Tables[0].Rows[0]["Gender"].ToString());
				}
				if(ds.Tables[0].Rows[0]["HomeAddress"]!=null && ds.Tables[0].Rows[0]["HomeAddress"].ToString()!="")
				{
					model.HomeAddress=ds.Tables[0].Rows[0]["HomeAddress"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Facebook"]!=null && ds.Tables[0].Rows[0]["Facebook"].ToString()!="")
				{
					model.Facebook=ds.Tables[0].Rows[0]["Facebook"].ToString();
				}
				if(ds.Tables[0].Rows[0]["QQ"]!=null && ds.Tables[0].Rows[0]["QQ"].ToString()!="")
				{
					model.QQ=ds.Tables[0].Rows[0]["QQ"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MSN"]!=null && ds.Tables[0].Rows[0]["MSN"].ToString()!="")
				{
					model.MSN=ds.Tables[0].Rows[0]["MSN"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Weibo"]!=null && ds.Tables[0].Rows[0]["Weibo"].ToString()!="")
				{
					model.Weibo=ds.Tables[0].Rows[0]["Weibo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OtherContact"]!=null && ds.Tables[0].Rows[0]["OtherContact"].ToString()!="")
				{
					model.OtherContact=ds.Tables[0].Rows[0]["OtherContact"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select KeyID,CreateMemberNumber,CountryCode,MobileNumber,EngFamilyName,EngGivenName,ChiFamilyName,ChiGivenName,Birthday,Gender,HomeAddress,Email,Facebook,QQ,MSN,Weibo,OtherContact ");
			strSql.Append(" FROM Ord_CreateMember_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" KeyID,CreateMemberNumber,CountryCode,MobileNumber,EngFamilyName,EngGivenName,ChiFamilyName,ChiGivenName,Birthday,Gender,HomeAddress,Email,Facebook,QQ,MSN,Weibo,OtherContact ");
			strSql.Append(" FROM Ord_CreateMember_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ord_CreateMember_D ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.KeyID desc");
			}
			strSql.Append(")AS Row, T.*  from Ord_CreateMember_D T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ord_CreateMember_D";
			parameters[1].Value = "KeyID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

