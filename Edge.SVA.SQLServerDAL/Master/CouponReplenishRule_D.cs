﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:CouponReplenishRule_D
    /// </summary>
    public partial class CouponReplenishRule_D : ICouponReplenishRule_D
    {
        public CouponReplenishRule_D()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int KeyID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from CouponReplenishRule_D");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Edge.SVA.Model.CouponReplenishRule_D model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CouponReplenishRule_D(");
            strSql.Append("CouponReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority)");
            strSql.Append(" values (");
            strSql.Append("@CouponReplenishCode,@StoreID,@OrderTargetID,@MinStockQty,@RunningStockQty,@OrderRoundUpQty,@Priority)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponReplenishCode", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@OrderTargetID", SqlDbType.Int,4),
					new SqlParameter("@MinStockQty", SqlDbType.Int,4),
					new SqlParameter("@RunningStockQty", SqlDbType.Int,4),
					new SqlParameter("@OrderRoundUpQty", SqlDbType.Int,4),
					new SqlParameter("@Priority", SqlDbType.Int,4)};
            parameters[0].Value = model.CouponReplenishCode;
            parameters[1].Value = model.StoreID;
            parameters[2].Value = model.OrderTargetID;
            parameters[3].Value = model.MinStockQty;
            parameters[4].Value = model.RunningStockQty;
            parameters[5].Value = model.OrderRoundUpQty;
            parameters[6].Value = model.Priority;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.CouponReplenishRule_D model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CouponReplenishRule_D set ");
            strSql.Append("CouponReplenishCode=@CouponReplenishCode,");
            strSql.Append("StoreID=@StoreID,");
            strSql.Append("OrderTargetID=@OrderTargetID,");
            strSql.Append("MinStockQty=@MinStockQty,");
            strSql.Append("RunningStockQty=@RunningStockQty,");
            strSql.Append("OrderRoundUpQty=@OrderRoundUpQty,");
            strSql.Append("Priority=@Priority");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@CouponReplenishCode", SqlDbType.VarChar,64),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@OrderTargetID", SqlDbType.Int,4),
					new SqlParameter("@MinStockQty", SqlDbType.Int,4),
					new SqlParameter("@RunningStockQty", SqlDbType.Int,4),
					new SqlParameter("@OrderRoundUpQty", SqlDbType.Int,4),
					new SqlParameter("@Priority", SqlDbType.Int,4),
					new SqlParameter("@KeyID", SqlDbType.Int,4)};
            parameters[0].Value = model.CouponReplenishCode;
            parameters[1].Value = model.StoreID;
            parameters[2].Value = model.OrderTargetID;
            parameters[3].Value = model.MinStockQty;
            parameters[4].Value = model.RunningStockQty;
            parameters[5].Value = model.OrderRoundUpQty;
            parameters[6].Value = model.Priority;
            parameters[7].Value = model.KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CouponReplenishRule_D ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string KeyIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CouponReplenishRule_D ");
            strSql.Append(" where KeyID in (" + KeyIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.CouponReplenishRule_D GetModel(int KeyID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 KeyID,CouponReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority from CouponReplenishRule_D ");
            strSql.Append(" where KeyID=@KeyID");
            SqlParameter[] parameters = {
					new SqlParameter("@KeyID", SqlDbType.Int,4)
			};
            parameters[0].Value = KeyID;

            Edge.SVA.Model.CouponReplenishRule_D model = new Edge.SVA.Model.CouponReplenishRule_D();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["KeyID"] != null && ds.Tables[0].Rows[0]["KeyID"].ToString() != "")
                {
                    model.KeyID = int.Parse(ds.Tables[0].Rows[0]["KeyID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CouponReplenishCode"] != null && ds.Tables[0].Rows[0]["CouponReplenishCode"].ToString() != "")
                {
                    model.CouponReplenishCode = ds.Tables[0].Rows[0]["CouponReplenishCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StoreID"] != null && ds.Tables[0].Rows[0]["StoreID"].ToString() != "")
                {
                    model.StoreID = int.Parse(ds.Tables[0].Rows[0]["StoreID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderTargetID"] != null && ds.Tables[0].Rows[0]["OrderTargetID"].ToString() != "")
                {
                    model.OrderTargetID = int.Parse(ds.Tables[0].Rows[0]["OrderTargetID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MinStockQty"] != null && ds.Tables[0].Rows[0]["MinStockQty"].ToString() != "")
                {
                    model.MinStockQty = int.Parse(ds.Tables[0].Rows[0]["MinStockQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RunningStockQty"] != null && ds.Tables[0].Rows[0]["RunningStockQty"].ToString() != "")
                {
                    model.RunningStockQty = int.Parse(ds.Tables[0].Rows[0]["RunningStockQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["OrderRoundUpQty"] != null && ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString() != "")
                {
                    model.OrderRoundUpQty = int.Parse(ds.Tables[0].Rows[0]["OrderRoundUpQty"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Priority"] != null && ds.Tables[0].Rows[0]["Priority"].ToString() != "")
                {
                    model.Priority = int.Parse(ds.Tables[0].Rows[0]["Priority"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select KeyID,CouponReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority ");
            strSql.Append(" FROM CouponReplenishRule_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" KeyID,CouponReplenishCode,StoreID,OrderTargetID,MinStockQty,RunningStockQty,OrderRoundUpQty,Priority ");
            strSql.Append(" FROM CouponReplenishRule_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM CouponReplenishRule_D ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.KeyID desc");
            }
            strSql.Append(")AS Row, T.*  from CouponReplenishRule_D T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        public bool DeleteByCouponReplenishCode(string couponReplenishCode)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from CouponReplenishRule_D where CouponReplenishCode = @CouponReplenishCode");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@CouponReplenishCode",SqlDbType.VarChar,64){ Value = couponReplenishCode }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "CouponReplenishRule_D";
            parameters[1].Value = "KeyID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

