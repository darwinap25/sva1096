﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Currency
	/// </summary>
	public partial class Currency:ICurrency
	{
		public Currency()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CurrencyID", "Currency"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CurrencyCode,int CurrencyID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Currency");
			strSql.Append(" where CurrencyCode=@CurrencyCode and CurrencyID=@CurrencyID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CurrencyCode", SqlDbType.VarChar,64),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4)			};
			parameters[0].Value = CurrencyCode;
			parameters[1].Value = CurrencyID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.Currency model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Currency(");
			strSql.Append("CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3,CurrencyRate,IsLocalCurrency,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)");
			strSql.Append(" values (");
			strSql.Append("@CurrencyCode,@CurrencyName1,@CurrencyName2,@CurrencyName3,@CurrencyRate,@IsLocalCurrency,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CurrencyCode", SqlDbType.VarChar,64),
					new SqlParameter("@CurrencyName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CurrencyName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CurrencyName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CurrencyRate", SqlDbType.Decimal,9),
					new SqlParameter("@IsLocalCurrency", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4)};
			parameters[0].Value = model.CurrencyCode;
			parameters[1].Value = model.CurrencyName1;
			parameters[2].Value = model.CurrencyName2;
			parameters[3].Value = model.CurrencyName3;
			parameters[4].Value = model.CurrencyRate;
			parameters[5].Value = model.IsLocalCurrency;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.CreatedBy;
			parameters[8].Value = model.UpdatedOn;
			parameters[9].Value = model.UpdatedBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Currency model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Currency set ");
			strSql.Append("CurrencyName1=@CurrencyName1,");
			strSql.Append("CurrencyName2=@CurrencyName2,");
			strSql.Append("CurrencyName3=@CurrencyName3,");
			strSql.Append("CurrencyRate=@CurrencyRate,");
			strSql.Append("IsLocalCurrency=@IsLocalCurrency,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy");
			strSql.Append(" where CurrencyID=@CurrencyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CurrencyName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CurrencyName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CurrencyName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CurrencyRate", SqlDbType.Decimal,9),
					new SqlParameter("@IsLocalCurrency", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4),
					new SqlParameter("@CurrencyCode", SqlDbType.VarChar,64)};
			parameters[0].Value = model.CurrencyName1;
			parameters[1].Value = model.CurrencyName2;
			parameters[2].Value = model.CurrencyName3;
			parameters[3].Value = model.CurrencyRate;
			parameters[4].Value = model.IsLocalCurrency;
			parameters[5].Value = model.CreatedOn;
			parameters[6].Value = model.CreatedBy;
			parameters[7].Value = model.UpdatedOn;
			parameters[8].Value = model.UpdatedBy;
			parameters[9].Value = model.CurrencyID;
			parameters[10].Value = model.CurrencyCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CurrencyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Currency ");
			strSql.Append(" where CurrencyID=@CurrencyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CurrencyID", SqlDbType.Int,4)
			};
			parameters[0].Value = CurrencyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CurrencyCode,int CurrencyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Currency ");
			strSql.Append(" where CurrencyCode=@CurrencyCode and CurrencyID=@CurrencyID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CurrencyCode", SqlDbType.VarChar,64),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4)			};
			parameters[0].Value = CurrencyCode;
			parameters[1].Value = CurrencyID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CurrencyIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Currency ");
			strSql.Append(" where CurrencyID in ("+CurrencyIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Currency GetModel(int CurrencyID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CurrencyID,CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3,CurrencyRate,IsLocalCurrency,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Currency ");
			strSql.Append(" where CurrencyID=@CurrencyID");
			SqlParameter[] parameters = {
					new SqlParameter("@CurrencyID", SqlDbType.Int,4)
			};
			parameters[0].Value = CurrencyID;

			Edge.SVA.Model.Currency model=new Edge.SVA.Model.Currency();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["CurrencyID"]!=null && ds.Tables[0].Rows[0]["CurrencyID"].ToString()!="")
				{
					model.CurrencyID=int.Parse(ds.Tables[0].Rows[0]["CurrencyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CurrencyCode"]!=null && ds.Tables[0].Rows[0]["CurrencyCode"].ToString()!="")
				{
					model.CurrencyCode=ds.Tables[0].Rows[0]["CurrencyCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CurrencyName1"]!=null && ds.Tables[0].Rows[0]["CurrencyName1"].ToString()!="")
				{
					model.CurrencyName1=ds.Tables[0].Rows[0]["CurrencyName1"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CurrencyName2"]!=null && ds.Tables[0].Rows[0]["CurrencyName2"].ToString()!="")
				{
					model.CurrencyName2=ds.Tables[0].Rows[0]["CurrencyName2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CurrencyName3"]!=null && ds.Tables[0].Rows[0]["CurrencyName3"].ToString()!="")
				{
					model.CurrencyName3=ds.Tables[0].Rows[0]["CurrencyName3"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CurrencyRate"]!=null && ds.Tables[0].Rows[0]["CurrencyRate"].ToString()!="")
				{
					model.CurrencyRate=decimal.Parse(ds.Tables[0].Rows[0]["CurrencyRate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsLocalCurrency"]!=null && ds.Tables[0].Rows[0]["IsLocalCurrency"].ToString()!="")
				{
					model.IsLocalCurrency=int.Parse(ds.Tables[0].Rows[0]["IsLocalCurrency"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(ds.Tables[0].Rows[0]["CreatedBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedOn"]!=null && ds.Tables[0].Rows[0]["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["UpdatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdatedBy"]!=null && ds.Tables[0].Rows[0]["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(ds.Tables[0].Rows[0]["UpdatedBy"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CurrencyID,CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3,CurrencyRate,IsLocalCurrency,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Currency ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CurrencyID,CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3,CurrencyRate,IsLocalCurrency,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ");
			strSql.Append(" FROM Currency ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Currency ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CurrencyID desc");
			}
			strSql.Append(")AS Row, T.*  from Currency T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Currency";
			parameters[1].Value = "CurrencyID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

