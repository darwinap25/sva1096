﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:CouponType
	/// </summary>
	public partial class CouponType:ICouponType
	{
		public CouponType()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("CouponTypeID", "CouponType"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CouponTypeCode,int CouponTypeID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CouponType");
			strSql.Append(" where CouponTypeCode=@CouponTypeCode and CouponTypeID=@CouponTypeID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeCode", SqlDbType.VarChar,512),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)			};
			parameters[0].Value = CouponTypeCode;
			parameters[1].Value = CouponTypeID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.CouponType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CouponType(");
			strSql.Append("CouponTypeNatureID,CouponTypeCode,CouponTypeName1,CouponTypeName2,CouponTypeName3,BrandID,CouponTypeNotes,CouponNumMask,CouponNumPattern,CouponValidityDuration,CouponValidityUnit,CouponVerifyMethod,CurrencyID,CouponTypeStartDate,CouponTypeEndDate,CouponTypeAmount,CouponTypePoint,CouponTypeDiscount,Status,CouponTypeLayoutFile,CouponTypePicFile,CouponCheckdigit,CheckDigitModeID,CampaignID,LocateStore,IsMemberBind,ActiveResetExpiryDate,CouponTypeTransfer,PasswordRuleID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponTypeUnvalued,CoupontypeFixedAmount,IsImportCouponNumber,CouponforfeitControl,UIDToCouponNumber,CouponSpecifyExpiryDate,EffectScheduleID,UIDCheckDigit,CouponNumberToUID,IsConsecutiveUID,CouponNatureID,CouponTypeRank,AutoReplenish,StartDateTime,EndDateTime,CouponTypeRedeemCount,SponsorID,SponsoredValue,SupplierID,AllowDeleteCoupon,AllowShareCoupon,QRCodePrefix,MaxDownloadCoupons,CouponReturnValue,TrainingMode,UnlimitedUsage)");
			strSql.Append(" values (");
			strSql.Append("@CouponTypeNatureID,@CouponTypeCode,@CouponTypeName1,@CouponTypeName2,@CouponTypeName3,@BrandID,@CouponTypeNotes,@CouponNumMask,@CouponNumPattern,@CouponValidityDuration,@CouponValidityUnit,@CouponVerifyMethod,@CurrencyID,@CouponTypeStartDate,@CouponTypeEndDate,@CouponTypeAmount,@CouponTypePoint,@CouponTypeDiscount,@Status,@CouponTypeLayoutFile,@CouponTypePicFile,@CouponCheckdigit,@CheckDigitModeID,@CampaignID,@LocateStore,@IsMemberBind,@ActiveResetExpiryDate,@CouponTypeTransfer,@PasswordRuleID,@CreatedOn,@UpdatedOn,@CreatedBy,@UpdatedBy,@CouponTypeUnvalued,@CoupontypeFixedAmount,@IsImportCouponNumber,@CouponforfeitControl,@UIDToCouponNumber,@CouponSpecifyExpiryDate,@EffectScheduleID,@UIDCheckDigit,@CouponNumberToUID,@IsConsecutiveUID,@CouponNatureID,@CouponTypeRank,@AutoReplenish,@StartDateTime,@EndDateTime,@CouponTypeRedeemCount,@SponsorID,@SponsoredValue,@SupplierID,@AllowDeleteCoupon,@AllowShareCoupon,@QRCodePrefix,@MaxDownloadCoupons,@CouponReturnValue,@TrainingMode,@UnlimitedUsage)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeNatureID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeCode", SqlDbType.VarChar,512),
					new SqlParameter("@CouponTypeName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponTypeName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponTypeName3", SqlDbType.NVarChar,512),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeNotes", SqlDbType.NVarChar,-1),
					new SqlParameter("@CouponNumMask", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponNumPattern", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponValidityDuration", SqlDbType.Int,4),
					new SqlParameter("@CouponValidityUnit", SqlDbType.Int,4),
					new SqlParameter("@CouponVerifyMethod", SqlDbType.Int,4),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeStartDate", SqlDbType.DateTime),
					new SqlParameter("@CouponTypeEndDate", SqlDbType.DateTime),
					new SqlParameter("@CouponTypeAmount", SqlDbType.Money,8),
					new SqlParameter("@CouponTypePoint", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeDiscount", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeLayoutFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponTypePicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponCheckdigit", SqlDbType.Bit,1),
					new SqlParameter("@CheckDigitModeID", SqlDbType.Int,4),
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@LocateStore", SqlDbType.Int,4),
					new SqlParameter("@IsMemberBind", SqlDbType.Int,4),
					new SqlParameter("@ActiveResetExpiryDate", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeTransfer", SqlDbType.Int,4),
					new SqlParameter("@PasswordRuleID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeUnvalued", SqlDbType.Int,4),
					new SqlParameter("@CoupontypeFixedAmount", SqlDbType.Int,4),
					new SqlParameter("@IsImportCouponNumber", SqlDbType.Int,4),
					new SqlParameter("@CouponforfeitControl", SqlDbType.Int,4),
					new SqlParameter("@UIDToCouponNumber", SqlDbType.Int,4),
					new SqlParameter("@CouponSpecifyExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@EffectScheduleID", SqlDbType.Int,4),
					new SqlParameter("@UIDCheckDigit", SqlDbType.Int,4),
					new SqlParameter("@CouponNumberToUID", SqlDbType.Int,4),
					new SqlParameter("@IsConsecutiveUID", SqlDbType.Int,4),
					new SqlParameter("@CouponNatureID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeRank", SqlDbType.Int,4),
					new SqlParameter("@AutoReplenish", SqlDbType.Int,4),
					new SqlParameter("@StartDateTime", SqlDbType.DateTime),
					new SqlParameter("@EndDateTime", SqlDbType.DateTime),
					new SqlParameter("@CouponTypeRedeemCount", SqlDbType.Int,4),
					new SqlParameter("@SponsorID", SqlDbType.Int,4),
					new SqlParameter("@SponsoredValue", SqlDbType.Decimal,9),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@AllowDeleteCoupon", SqlDbType.Int,4),
					new SqlParameter("@AllowShareCoupon", SqlDbType.Int,4),
					new SqlParameter("@QRCodePrefix", SqlDbType.VarChar,64),
					new SqlParameter("@MaxDownloadCoupons", SqlDbType.Int,4),
					new SqlParameter("@CouponReturnValue", SqlDbType.Int,4),
					new SqlParameter("@TrainingMode", SqlDbType.Int,4),
					new SqlParameter("@UnlimitedUsage", SqlDbType.Int,4)};
			parameters[0].Value = model.CouponTypeNatureID;
			parameters[1].Value = model.CouponTypeCode;
			parameters[2].Value = model.CouponTypeName1;
			parameters[3].Value = model.CouponTypeName2;
			parameters[4].Value = model.CouponTypeName3;
			parameters[5].Value = model.BrandID;
			parameters[6].Value = model.CouponTypeNotes;
			parameters[7].Value = model.CouponNumMask;
			parameters[8].Value = model.CouponNumPattern;
			parameters[9].Value = model.CouponValidityDuration;
			parameters[10].Value = model.CouponValidityUnit;
			parameters[11].Value = model.CouponVerifyMethod;
			parameters[12].Value = model.CurrencyID;
			parameters[13].Value = model.CouponTypeStartDate;
			parameters[14].Value = model.CouponTypeEndDate;
			parameters[15].Value = model.CouponTypeAmount;
			parameters[16].Value = model.CouponTypePoint;
			parameters[17].Value = model.CouponTypeDiscount;
			parameters[18].Value = model.Status;
			parameters[19].Value = model.CouponTypeLayoutFile;
			parameters[20].Value = model.CouponTypePicFile;
			parameters[21].Value = model.CouponCheckdigit;
			parameters[22].Value = model.CheckDigitModeID;
			parameters[23].Value = model.CampaignID;
			parameters[24].Value = model.LocateStore;
			parameters[25].Value = model.IsMemberBind;
			parameters[26].Value = model.ActiveResetExpiryDate;
			parameters[27].Value = model.CouponTypeTransfer;
			parameters[28].Value = model.PasswordRuleID;
			parameters[29].Value = model.CreatedOn;
			parameters[30].Value = model.UpdatedOn;
			parameters[31].Value = model.CreatedBy;
			parameters[32].Value = model.UpdatedBy;
			parameters[33].Value = model.CouponTypeUnvalued;
			parameters[34].Value = model.CoupontypeFixedAmount;
			parameters[35].Value = model.IsImportCouponNumber;
			parameters[36].Value = model.CouponforfeitControl;
			parameters[37].Value = model.UIDToCouponNumber;
			parameters[38].Value = model.CouponSpecifyExpiryDate;
			parameters[39].Value = model.EffectScheduleID;
			parameters[40].Value = model.UIDCheckDigit;
			parameters[41].Value = model.CouponNumberToUID;
			parameters[42].Value = model.IsConsecutiveUID;
			parameters[43].Value = model.CouponNatureID;
			parameters[44].Value = model.CouponTypeRank;
			parameters[45].Value = model.AutoReplenish;
			parameters[46].Value = model.StartDateTime;
			parameters[47].Value = model.EndDateTime;
			parameters[48].Value = model.CouponTypeRedeemCount;
			parameters[49].Value = model.SponsorID;
			parameters[50].Value = model.SponsoredValue;
			parameters[51].Value = model.SupplierID;
			parameters[52].Value = model.AllowDeleteCoupon;
			parameters[53].Value = model.AllowShareCoupon;
			parameters[54].Value = model.QRCodePrefix;
			parameters[55].Value = model.MaxDownloadCoupons;
			parameters[56].Value = model.CouponReturnValue;
			parameters[57].Value = model.TrainingMode;
			parameters[58].Value = model.UnlimitedUsage;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CouponType set ");
			strSql.Append("CouponTypeNatureID=@CouponTypeNatureID,");
			strSql.Append("CouponTypeName1=@CouponTypeName1,");
			strSql.Append("CouponTypeName2=@CouponTypeName2,");
			strSql.Append("CouponTypeName3=@CouponTypeName3,");
			strSql.Append("BrandID=@BrandID,");
			strSql.Append("CouponTypeNotes=@CouponTypeNotes,");
			strSql.Append("CouponNumMask=@CouponNumMask,");
			strSql.Append("CouponNumPattern=@CouponNumPattern,");
			strSql.Append("CouponValidityDuration=@CouponValidityDuration,");
			strSql.Append("CouponValidityUnit=@CouponValidityUnit,");
			strSql.Append("CouponVerifyMethod=@CouponVerifyMethod,");
			strSql.Append("CurrencyID=@CurrencyID,");
			strSql.Append("CouponTypeStartDate=@CouponTypeStartDate,");
			strSql.Append("CouponTypeEndDate=@CouponTypeEndDate,");
			strSql.Append("CouponTypeAmount=@CouponTypeAmount,");
			strSql.Append("CouponTypePoint=@CouponTypePoint,");
			strSql.Append("CouponTypeDiscount=@CouponTypeDiscount,");
			strSql.Append("Status=@Status,");
			strSql.Append("CouponTypeLayoutFile=@CouponTypeLayoutFile,");
			strSql.Append("CouponTypePicFile=@CouponTypePicFile,");
			strSql.Append("CouponCheckdigit=@CouponCheckdigit,");
			strSql.Append("CheckDigitModeID=@CheckDigitModeID,");
			strSql.Append("CampaignID=@CampaignID,");
			strSql.Append("LocateStore=@LocateStore,");
			strSql.Append("IsMemberBind=@IsMemberBind,");
			strSql.Append("ActiveResetExpiryDate=@ActiveResetExpiryDate,");
			strSql.Append("CouponTypeTransfer=@CouponTypeTransfer,");
			strSql.Append("PasswordRuleID=@PasswordRuleID,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("CouponTypeUnvalued=@CouponTypeUnvalued,");
			strSql.Append("CoupontypeFixedAmount=@CoupontypeFixedAmount,");
			strSql.Append("IsImportCouponNumber=@IsImportCouponNumber,");
			strSql.Append("CouponforfeitControl=@CouponforfeitControl,");
			strSql.Append("UIDToCouponNumber=@UIDToCouponNumber,");
			strSql.Append("CouponSpecifyExpiryDate=@CouponSpecifyExpiryDate,");
			strSql.Append("EffectScheduleID=@EffectScheduleID,");
			strSql.Append("UIDCheckDigit=@UIDCheckDigit,");
			strSql.Append("CouponNumberToUID=@CouponNumberToUID,");
			strSql.Append("IsConsecutiveUID=@IsConsecutiveUID,");
			strSql.Append("CouponNatureID=@CouponNatureID,");
			strSql.Append("CouponTypeRank=@CouponTypeRank,");
			strSql.Append("AutoReplenish=@AutoReplenish,");
			strSql.Append("StartDateTime=@StartDateTime,");
			strSql.Append("EndDateTime=@EndDateTime,");
			strSql.Append("CouponTypeRedeemCount=@CouponTypeRedeemCount,");
			strSql.Append("SponsorID=@SponsorID,");
			strSql.Append("SponsoredValue=@SponsoredValue,");
			strSql.Append("SupplierID=@SupplierID,");
			strSql.Append("AllowDeleteCoupon=@AllowDeleteCoupon,");
			strSql.Append("AllowShareCoupon=@AllowShareCoupon,");
			strSql.Append("QRCodePrefix=@QRCodePrefix,");
			strSql.Append("MaxDownloadCoupons=@MaxDownloadCoupons,");
			strSql.Append("CouponReturnValue=@CouponReturnValue,");
			strSql.Append("TrainingMode=@TrainingMode,");
			strSql.Append("UnlimitedUsage=@UnlimitedUsage");
			strSql.Append(" where CouponTypeID=@CouponTypeID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeNatureID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeName1", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponTypeName2", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponTypeName3", SqlDbType.NVarChar,512),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeNotes", SqlDbType.NVarChar,-1),
					new SqlParameter("@CouponNumMask", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponNumPattern", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponValidityDuration", SqlDbType.Int,4),
					new SqlParameter("@CouponValidityUnit", SqlDbType.Int,4),
					new SqlParameter("@CouponVerifyMethod", SqlDbType.Int,4),
					new SqlParameter("@CurrencyID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeStartDate", SqlDbType.DateTime),
					new SqlParameter("@CouponTypeEndDate", SqlDbType.DateTime),
					new SqlParameter("@CouponTypeAmount", SqlDbType.Money,8),
					new SqlParameter("@CouponTypePoint", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeDiscount", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeLayoutFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponTypePicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CouponCheckdigit", SqlDbType.Bit,1),
					new SqlParameter("@CheckDigitModeID", SqlDbType.Int,4),
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@LocateStore", SqlDbType.Int,4),
					new SqlParameter("@IsMemberBind", SqlDbType.Int,4),
					new SqlParameter("@ActiveResetExpiryDate", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeTransfer", SqlDbType.Int,4),
					new SqlParameter("@PasswordRuleID", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeUnvalued", SqlDbType.Int,4),
					new SqlParameter("@CoupontypeFixedAmount", SqlDbType.Int,4),
					new SqlParameter("@IsImportCouponNumber", SqlDbType.Int,4),
					new SqlParameter("@CouponforfeitControl", SqlDbType.Int,4),
					new SqlParameter("@UIDToCouponNumber", SqlDbType.Int,4),
					new SqlParameter("@CouponSpecifyExpiryDate", SqlDbType.DateTime),
					new SqlParameter("@EffectScheduleID", SqlDbType.Int,4),
					new SqlParameter("@UIDCheckDigit", SqlDbType.Int,4),
					new SqlParameter("@CouponNumberToUID", SqlDbType.Int,4),
					new SqlParameter("@IsConsecutiveUID", SqlDbType.Int,4),
					new SqlParameter("@CouponNatureID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeRank", SqlDbType.Int,4),
					new SqlParameter("@AutoReplenish", SqlDbType.Int,4),
					new SqlParameter("@StartDateTime", SqlDbType.DateTime),
					new SqlParameter("@EndDateTime", SqlDbType.DateTime),
					new SqlParameter("@CouponTypeRedeemCount", SqlDbType.Int,4),
					new SqlParameter("@SponsorID", SqlDbType.Int,4),
					new SqlParameter("@SponsoredValue", SqlDbType.Decimal,9),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@AllowDeleteCoupon", SqlDbType.Int,4),
					new SqlParameter("@AllowShareCoupon", SqlDbType.Int,4),
					new SqlParameter("@QRCodePrefix", SqlDbType.VarChar,64),
					new SqlParameter("@MaxDownloadCoupons", SqlDbType.Int,4),
					new SqlParameter("@CouponReturnValue", SqlDbType.Int,4),
					new SqlParameter("@TrainingMode", SqlDbType.Int,4),
					new SqlParameter("@UnlimitedUsage", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.CouponTypeNatureID;
			parameters[1].Value = model.CouponTypeName1;
			parameters[2].Value = model.CouponTypeName2;
			parameters[3].Value = model.CouponTypeName3;
			parameters[4].Value = model.BrandID;
			parameters[5].Value = model.CouponTypeNotes;
			parameters[6].Value = model.CouponNumMask;
			parameters[7].Value = model.CouponNumPattern;
			parameters[8].Value = model.CouponValidityDuration;
			parameters[9].Value = model.CouponValidityUnit;
			parameters[10].Value = model.CouponVerifyMethod;
			parameters[11].Value = model.CurrencyID;
			parameters[12].Value = model.CouponTypeStartDate;
			parameters[13].Value = model.CouponTypeEndDate;
			parameters[14].Value = model.CouponTypeAmount;
			parameters[15].Value = model.CouponTypePoint;
			parameters[16].Value = model.CouponTypeDiscount;
			parameters[17].Value = model.Status;
			parameters[18].Value = model.CouponTypeLayoutFile;
			parameters[19].Value = model.CouponTypePicFile;
			parameters[20].Value = model.CouponCheckdigit;
			parameters[21].Value = model.CheckDigitModeID;
			parameters[22].Value = model.CampaignID;
			parameters[23].Value = model.LocateStore;
			parameters[24].Value = model.IsMemberBind;
			parameters[25].Value = model.ActiveResetExpiryDate;
			parameters[26].Value = model.CouponTypeTransfer;
			parameters[27].Value = model.PasswordRuleID;
			parameters[28].Value = model.CreatedOn;
			parameters[29].Value = model.UpdatedOn;
			parameters[30].Value = model.CreatedBy;
			parameters[31].Value = model.UpdatedBy;
			parameters[32].Value = model.CouponTypeUnvalued;
			parameters[33].Value = model.CoupontypeFixedAmount;
			parameters[34].Value = model.IsImportCouponNumber;
			parameters[35].Value = model.CouponforfeitControl;
			parameters[36].Value = model.UIDToCouponNumber;
			parameters[37].Value = model.CouponSpecifyExpiryDate;
			parameters[38].Value = model.EffectScheduleID;
			parameters[39].Value = model.UIDCheckDigit;
			parameters[40].Value = model.CouponNumberToUID;
			parameters[41].Value = model.IsConsecutiveUID;
			parameters[42].Value = model.CouponNatureID;
			parameters[43].Value = model.CouponTypeRank;
			parameters[44].Value = model.AutoReplenish;
			parameters[45].Value = model.StartDateTime;
			parameters[46].Value = model.EndDateTime;
			parameters[47].Value = model.CouponTypeRedeemCount;
			parameters[48].Value = model.SponsorID;
			parameters[49].Value = model.SponsoredValue;
			parameters[50].Value = model.SupplierID;
			parameters[51].Value = model.AllowDeleteCoupon;
			parameters[52].Value = model.AllowShareCoupon;
			parameters[53].Value = model.QRCodePrefix;
			parameters[54].Value = model.MaxDownloadCoupons;
			parameters[55].Value = model.CouponReturnValue;
			parameters[56].Value = model.TrainingMode;
			parameters[57].Value = model.UnlimitedUsage;
			parameters[58].Value = model.CouponTypeID;
			parameters[59].Value = model.CouponTypeCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CouponTypeID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponType ");
			strSql.Append(" where CouponTypeID=@CouponTypeID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)
			};
			parameters[0].Value = CouponTypeID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CouponTypeCode,int CouponTypeID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponType ");
			strSql.Append(" where CouponTypeCode=@CouponTypeCode and CouponTypeID=@CouponTypeID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeCode", SqlDbType.VarChar,512),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)			};
			parameters[0].Value = CouponTypeCode;
			parameters[1].Value = CouponTypeID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string CouponTypeIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CouponType ");
			strSql.Append(" where CouponTypeID in ("+CouponTypeIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponType GetModel(int CouponTypeID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 CouponTypeID,CouponTypeNatureID,CouponTypeCode,CouponTypeName1,CouponTypeName2,CouponTypeName3,BrandID,CouponTypeNotes,CouponNumMask,CouponNumPattern,CouponValidityDuration,CouponValidityUnit,CouponVerifyMethod,CurrencyID,CouponTypeStartDate,CouponTypeEndDate,CouponTypeAmount,CouponTypePoint,CouponTypeDiscount,Status,CouponTypeLayoutFile,CouponTypePicFile,CouponCheckdigit,CheckDigitModeID,CampaignID,LocateStore,IsMemberBind,ActiveResetExpiryDate,CouponTypeTransfer,PasswordRuleID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponTypeUnvalued,CoupontypeFixedAmount,IsImportCouponNumber,CouponforfeitControl,UIDToCouponNumber,CouponSpecifyExpiryDate,EffectScheduleID,UIDCheckDigit,CouponNumberToUID,IsConsecutiveUID,CouponNatureID,CouponTypeRank,AutoReplenish,StartDateTime,EndDateTime,CouponTypeRedeemCount,SponsorID,SponsoredValue,SupplierID,AllowDeleteCoupon,AllowShareCoupon,QRCodePrefix,MaxDownloadCoupons,CouponReturnValue,TrainingMode,UnlimitedUsage from CouponType ");
			strSql.Append(" where CouponTypeID=@CouponTypeID");
			SqlParameter[] parameters = {
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)
			};
			parameters[0].Value = CouponTypeID;

			Edge.SVA.Model.CouponType model=new Edge.SVA.Model.CouponType();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponType DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.CouponType model=new Edge.SVA.Model.CouponType();
			if (row != null)
			{
				if(row["CouponTypeID"]!=null && row["CouponTypeID"].ToString()!="")
				{
					model.CouponTypeID=int.Parse(row["CouponTypeID"].ToString());
				}
				if(row["CouponTypeNatureID"]!=null && row["CouponTypeNatureID"].ToString()!="")
				{
					model.CouponTypeNatureID=int.Parse(row["CouponTypeNatureID"].ToString());
				}
				if(row["CouponTypeCode"]!=null)
				{
					model.CouponTypeCode=row["CouponTypeCode"].ToString();
				}
				if(row["CouponTypeName1"]!=null)
				{
					model.CouponTypeName1=row["CouponTypeName1"].ToString();
				}
				if(row["CouponTypeName2"]!=null)
				{
					model.CouponTypeName2=row["CouponTypeName2"].ToString();
				}
				if(row["CouponTypeName3"]!=null)
				{
					model.CouponTypeName3=row["CouponTypeName3"].ToString();
				}
				if(row["BrandID"]!=null && row["BrandID"].ToString()!="")
				{
					model.BrandID=int.Parse(row["BrandID"].ToString());
				}
				if(row["CouponTypeNotes"]!=null)
				{
					model.CouponTypeNotes=row["CouponTypeNotes"].ToString();
				}
				if(row["CouponNumMask"]!=null)
				{
					model.CouponNumMask=row["CouponNumMask"].ToString();
				}
				if(row["CouponNumPattern"]!=null)
				{
					model.CouponNumPattern=row["CouponNumPattern"].ToString();
				}
				if(row["CouponValidityDuration"]!=null && row["CouponValidityDuration"].ToString()!="")
				{
					model.CouponValidityDuration=int.Parse(row["CouponValidityDuration"].ToString());
				}
				if(row["CouponValidityUnit"]!=null && row["CouponValidityUnit"].ToString()!="")
				{
					model.CouponValidityUnit=int.Parse(row["CouponValidityUnit"].ToString());
				}
				if(row["CouponVerifyMethod"]!=null && row["CouponVerifyMethod"].ToString()!="")
				{
					model.CouponVerifyMethod=int.Parse(row["CouponVerifyMethod"].ToString());
				}
				if(row["CurrencyID"]!=null && row["CurrencyID"].ToString()!="")
				{
					model.CurrencyID=int.Parse(row["CurrencyID"].ToString());
				}
				if(row["CouponTypeStartDate"]!=null && row["CouponTypeStartDate"].ToString()!="")
				{
					model.CouponTypeStartDate=DateTime.Parse(row["CouponTypeStartDate"].ToString());
				}
				if(row["CouponTypeEndDate"]!=null && row["CouponTypeEndDate"].ToString()!="")
				{
					model.CouponTypeEndDate=DateTime.Parse(row["CouponTypeEndDate"].ToString());
				}
				if(row["CouponTypeAmount"]!=null && row["CouponTypeAmount"].ToString()!="")
				{
					model.CouponTypeAmount=decimal.Parse(row["CouponTypeAmount"].ToString());
				}
				if(row["CouponTypePoint"]!=null && row["CouponTypePoint"].ToString()!="")
				{
					model.CouponTypePoint=int.Parse(row["CouponTypePoint"].ToString());
				}
				if(row["CouponTypeDiscount"]!=null && row["CouponTypeDiscount"].ToString()!="")
				{
					model.CouponTypeDiscount=decimal.Parse(row["CouponTypeDiscount"].ToString());
				}
				if(row["Status"]!=null && row["Status"].ToString()!="")
				{
					model.Status=int.Parse(row["Status"].ToString());
				}
				if(row["CouponTypeLayoutFile"]!=null)
				{
					model.CouponTypeLayoutFile=row["CouponTypeLayoutFile"].ToString();
				}
				if(row["CouponTypePicFile"]!=null)
				{
					model.CouponTypePicFile=row["CouponTypePicFile"].ToString();
				}
				if(row["CouponCheckdigit"]!=null && row["CouponCheckdigit"].ToString()!="")
				{
					if((row["CouponCheckdigit"].ToString()=="1")||(row["CouponCheckdigit"].ToString().ToLower()=="true"))
					{
						model.CouponCheckdigit=true;
					}
					else
					{
						model.CouponCheckdigit=false;
					}
				}
				if(row["CheckDigitModeID"]!=null && row["CheckDigitModeID"].ToString()!="")
				{
					model.CheckDigitModeID=int.Parse(row["CheckDigitModeID"].ToString());
				}
				if(row["CampaignID"]!=null && row["CampaignID"].ToString()!="")
				{
					model.CampaignID=int.Parse(row["CampaignID"].ToString());
				}
				if(row["LocateStore"]!=null && row["LocateStore"].ToString()!="")
				{
					model.LocateStore=int.Parse(row["LocateStore"].ToString());
				}
				if(row["IsMemberBind"]!=null && row["IsMemberBind"].ToString()!="")
				{
					model.IsMemberBind=int.Parse(row["IsMemberBind"].ToString());
				}
				if(row["ActiveResetExpiryDate"]!=null && row["ActiveResetExpiryDate"].ToString()!="")
				{
					model.ActiveResetExpiryDate=int.Parse(row["ActiveResetExpiryDate"].ToString());
				}
				if(row["CouponTypeTransfer"]!=null && row["CouponTypeTransfer"].ToString()!="")
				{
					model.CouponTypeTransfer=int.Parse(row["CouponTypeTransfer"].ToString());
				}
				if(row["PasswordRuleID"]!=null && row["PasswordRuleID"].ToString()!="")
				{
					model.PasswordRuleID=int.Parse(row["PasswordRuleID"].ToString());
				}
				if(row["CreatedOn"]!=null && row["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(row["CreatedOn"].ToString());
				}
				if(row["UpdatedOn"]!=null && row["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(row["UpdatedOn"].ToString());
				}
				if(row["CreatedBy"]!=null && row["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(row["CreatedBy"].ToString());
				}
				if(row["UpdatedBy"]!=null && row["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(row["UpdatedBy"].ToString());
				}
				if(row["CouponTypeUnvalued"]!=null && row["CouponTypeUnvalued"].ToString()!="")
				{
					model.CouponTypeUnvalued=int.Parse(row["CouponTypeUnvalued"].ToString());
				}
				if(row["CoupontypeFixedAmount"]!=null && row["CoupontypeFixedAmount"].ToString()!="")
				{
					model.CoupontypeFixedAmount=int.Parse(row["CoupontypeFixedAmount"].ToString());
				}
				if(row["IsImportCouponNumber"]!=null && row["IsImportCouponNumber"].ToString()!="")
				{
					model.IsImportCouponNumber=int.Parse(row["IsImportCouponNumber"].ToString());
				}
				if(row["CouponforfeitControl"]!=null && row["CouponforfeitControl"].ToString()!="")
				{
					model.CouponforfeitControl=int.Parse(row["CouponforfeitControl"].ToString());
				}
				if(row["UIDToCouponNumber"]!=null && row["UIDToCouponNumber"].ToString()!="")
				{
					model.UIDToCouponNumber=int.Parse(row["UIDToCouponNumber"].ToString());
				}
				if(row["CouponSpecifyExpiryDate"]!=null && row["CouponSpecifyExpiryDate"].ToString()!="")
				{
					model.CouponSpecifyExpiryDate=DateTime.Parse(row["CouponSpecifyExpiryDate"].ToString());
				}
				if(row["EffectScheduleID"]!=null && row["EffectScheduleID"].ToString()!="")
				{
					model.EffectScheduleID=int.Parse(row["EffectScheduleID"].ToString());
				}
				if(row["UIDCheckDigit"]!=null && row["UIDCheckDigit"].ToString()!="")
				{
					model.UIDCheckDigit=int.Parse(row["UIDCheckDigit"].ToString());
				}
				if(row["CouponNumberToUID"]!=null && row["CouponNumberToUID"].ToString()!="")
				{
					model.CouponNumberToUID=int.Parse(row["CouponNumberToUID"].ToString());
				}
				if(row["IsConsecutiveUID"]!=null && row["IsConsecutiveUID"].ToString()!="")
				{
					model.IsConsecutiveUID=int.Parse(row["IsConsecutiveUID"].ToString());
				}
				if(row["CouponNatureID"]!=null && row["CouponNatureID"].ToString()!="")
				{
					model.CouponNatureID=int.Parse(row["CouponNatureID"].ToString());
				}
				if(row["CouponTypeRank"]!=null && row["CouponTypeRank"].ToString()!="")
				{
					model.CouponTypeRank=int.Parse(row["CouponTypeRank"].ToString());
				}
				if(row["AutoReplenish"]!=null && row["AutoReplenish"].ToString()!="")
				{
					model.AutoReplenish=int.Parse(row["AutoReplenish"].ToString());
				}
				if(row["StartDateTime"]!=null && row["StartDateTime"].ToString()!="")
				{
					model.StartDateTime=DateTime.Parse(row["StartDateTime"].ToString());
				}
				if(row["EndDateTime"]!=null && row["EndDateTime"].ToString()!="")
				{
					model.EndDateTime=DateTime.Parse(row["EndDateTime"].ToString());
				}
				if(row["CouponTypeRedeemCount"]!=null && row["CouponTypeRedeemCount"].ToString()!="")
				{
					model.CouponTypeRedeemCount=int.Parse(row["CouponTypeRedeemCount"].ToString());
				}
				if(row["SponsorID"]!=null && row["SponsorID"].ToString()!="")
				{
					model.SponsorID=int.Parse(row["SponsorID"].ToString());
				}
				if(row["SponsoredValue"]!=null && row["SponsoredValue"].ToString()!="")
				{
					model.SponsoredValue=decimal.Parse(row["SponsoredValue"].ToString());
				}
				if(row["SupplierID"]!=null && row["SupplierID"].ToString()!="")
				{
					model.SupplierID=int.Parse(row["SupplierID"].ToString());
				}
				if(row["AllowDeleteCoupon"]!=null && row["AllowDeleteCoupon"].ToString()!="")
				{
					model.AllowDeleteCoupon=int.Parse(row["AllowDeleteCoupon"].ToString());
				}
				if(row["AllowShareCoupon"]!=null && row["AllowShareCoupon"].ToString()!="")
				{
					model.AllowShareCoupon=int.Parse(row["AllowShareCoupon"].ToString());
				}
				if(row["QRCodePrefix"]!=null)
				{
					model.QRCodePrefix=row["QRCodePrefix"].ToString();
				}
				if(row["MaxDownloadCoupons"]!=null && row["MaxDownloadCoupons"].ToString()!="")
				{
					model.MaxDownloadCoupons=int.Parse(row["MaxDownloadCoupons"].ToString());
				}
				if(row["CouponReturnValue"]!=null && row["CouponReturnValue"].ToString()!="")
				{
					model.CouponReturnValue=int.Parse(row["CouponReturnValue"].ToString());
				}
				if(row["TrainingMode"]!=null && row["TrainingMode"].ToString()!="")
				{
					model.TrainingMode=int.Parse(row["TrainingMode"].ToString());
				}
				if(row["UnlimitedUsage"]!=null && row["UnlimitedUsage"].ToString()!="")
				{
					model.UnlimitedUsage=int.Parse(row["UnlimitedUsage"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select CouponTypeID,CouponTypeNatureID,CouponTypeCode,CouponTypeName1,CouponTypeName2,CouponTypeName3,BrandID,CouponTypeNotes,CouponNumMask,CouponNumPattern,CouponValidityDuration,CouponValidityUnit,CouponVerifyMethod,CurrencyID,CouponTypeStartDate,CouponTypeEndDate,CouponTypeAmount,CouponTypePoint,CouponTypeDiscount,Status,CouponTypeLayoutFile,CouponTypePicFile,CouponCheckdigit,CheckDigitModeID,CampaignID,LocateStore,IsMemberBind,ActiveResetExpiryDate,CouponTypeTransfer,PasswordRuleID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponTypeUnvalued,CoupontypeFixedAmount,IsImportCouponNumber,CouponforfeitControl,UIDToCouponNumber,CouponSpecifyExpiryDate,EffectScheduleID,UIDCheckDigit,CouponNumberToUID,IsConsecutiveUID,CouponNatureID,CouponTypeRank,AutoReplenish,StartDateTime,EndDateTime,CouponTypeRedeemCount,SponsorID,SponsoredValue,SupplierID,AllowDeleteCoupon,AllowShareCoupon,QRCodePrefix,MaxDownloadCoupons,CouponReturnValue,TrainingMode,UnlimitedUsage ");
			strSql.Append(" FROM CouponType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" CouponTypeID,CouponTypeNatureID,CouponTypeCode,CouponTypeName1,CouponTypeName2,CouponTypeName3,BrandID,CouponTypeNotes,CouponNumMask,CouponNumPattern,CouponValidityDuration,CouponValidityUnit,CouponVerifyMethod,CurrencyID,CouponTypeStartDate,CouponTypeEndDate,CouponTypeAmount,CouponTypePoint,CouponTypeDiscount,Status,CouponTypeLayoutFile,CouponTypePicFile,CouponCheckdigit,CheckDigitModeID,CampaignID,LocateStore,IsMemberBind,ActiveResetExpiryDate,CouponTypeTransfer,PasswordRuleID,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy,CouponTypeUnvalued,CoupontypeFixedAmount,IsImportCouponNumber,CouponforfeitControl,UIDToCouponNumber,CouponSpecifyExpiryDate,EffectScheduleID,UIDCheckDigit,CouponNumberToUID,IsConsecutiveUID,CouponNatureID,CouponTypeRank,AutoReplenish,StartDateTime,EndDateTime,CouponTypeRedeemCount,SponsorID,SponsoredValue,SupplierID,AllowDeleteCoupon,AllowShareCoupon,QRCodePrefix,MaxDownloadCoupons,CouponReturnValue,TrainingMode,UnlimitedUsage ");
			strSql.Append(" FROM CouponType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM CouponType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CouponTypeID desc");
			}
			strSql.Append(")AS Row, T.*  from CouponType T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            int OrderType = 0;
            string OrderField = filedOrder;
            if (filedOrder.ToLower().EndsWith(" desc"))
            {
                OrderType = 1;
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" desc"));
            }
            else if (filedOrder.ToLower().EndsWith(" asc"))
            {
                OrderField = filedOrder.Substring(0, filedOrder.ToLower().IndexOf(" asc"));
            }
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "CouponType";
            parameters[1].Value = "*";
            parameters[2].Value = OrderField;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = OrderType;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
        }

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from CouponType ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "CouponType";
			parameters[1].Value = "CouponTypeID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

