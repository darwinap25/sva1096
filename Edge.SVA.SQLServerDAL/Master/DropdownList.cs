﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:DropdownList
	/// </summary>
	public partial class DropdownList:IDropdownList
	{
		public DropdownList()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string TypeCode,string Text,string Lan)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from DropdownList");
			strSql.Append(" where TypeCode=@TypeCode and Text=@Text and Lan=@Lan ");
			SqlParameter[] parameters = {
					new SqlParameter("@TypeCode", SqlDbType.NVarChar,50),
					new SqlParameter("@Text", SqlDbType.NVarChar,100),
					new SqlParameter("@Lan", SqlDbType.NVarChar,10)			};
			parameters[0].Value = TypeCode;
			parameters[1].Value = Text;
			parameters[2].Value = Lan;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.DropdownList model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into DropdownList(");
			strSql.Append("TypeCode,Description,Code,Text,OrderID,Lan)");
			strSql.Append(" values (");
			strSql.Append("@TypeCode,@Description,@Code,@Text,@OrderID,@Lan)");
			SqlParameter[] parameters = {
					new SqlParameter("@TypeCode", SqlDbType.NVarChar,50),
					new SqlParameter("@Description", SqlDbType.NVarChar,100),
					new SqlParameter("@Code", SqlDbType.NVarChar,20),
					new SqlParameter("@Text", SqlDbType.NVarChar,100),
					new SqlParameter("@OrderID", SqlDbType.Int,4),
					new SqlParameter("@Lan", SqlDbType.NVarChar,10)};
			parameters[0].Value = model.TypeCode;
			parameters[1].Value = model.Description;
			parameters[2].Value = model.Code;
			parameters[3].Value = model.Text;
			parameters[4].Value = model.OrderID;
			parameters[5].Value = model.Lan;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.DropdownList model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update DropdownList set ");
			strSql.Append("Description=@Description,");
			strSql.Append("Code=@Code,");
			strSql.Append("OrderID=@OrderID");
			strSql.Append(" where TypeCode=@TypeCode and Text=@Text and Lan=@Lan ");
			SqlParameter[] parameters = {
					new SqlParameter("@Description", SqlDbType.NVarChar,100),
					new SqlParameter("@Code", SqlDbType.NVarChar,20),
					new SqlParameter("@OrderID", SqlDbType.Int,4),
					new SqlParameter("@TypeCode", SqlDbType.NVarChar,50),
					new SqlParameter("@Text", SqlDbType.NVarChar,100),
					new SqlParameter("@Lan", SqlDbType.NVarChar,10)};
			parameters[0].Value = model.Description;
			parameters[1].Value = model.Code;
			parameters[2].Value = model.OrderID;
			parameters[3].Value = model.TypeCode;
			parameters[4].Value = model.Text;
			parameters[5].Value = model.Lan;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string TypeCode,string Text,string Lan)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from DropdownList ");
			strSql.Append(" where TypeCode=@TypeCode and Text=@Text and Lan=@Lan ");
			SqlParameter[] parameters = {
					new SqlParameter("@TypeCode", SqlDbType.NVarChar,50),
					new SqlParameter("@Text", SqlDbType.NVarChar,100),
					new SqlParameter("@Lan", SqlDbType.NVarChar,10)			};
			parameters[0].Value = TypeCode;
			parameters[1].Value = Text;
			parameters[2].Value = Lan;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.DropdownList GetModel(string TypeCode,string Text,string Lan)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TypeCode,Description,Code,Text,OrderID,Lan from DropdownList ");
			strSql.Append(" where TypeCode=@TypeCode and Text=@Text and Lan=@Lan ");
			SqlParameter[] parameters = {
					new SqlParameter("@TypeCode", SqlDbType.NVarChar,50),
					new SqlParameter("@Text", SqlDbType.NVarChar,100),
					new SqlParameter("@Lan", SqlDbType.NVarChar,10)			};
			parameters[0].Value = TypeCode;
			parameters[1].Value = Text;
			parameters[2].Value = Lan;

			Edge.SVA.Model.DropdownList model=new Edge.SVA.Model.DropdownList();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["TypeCode"]!=null && ds.Tables[0].Rows[0]["TypeCode"].ToString()!="")
				{
					model.TypeCode=ds.Tables[0].Rows[0]["TypeCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Description"]!=null && ds.Tables[0].Rows[0]["Description"].ToString()!="")
				{
					model.Description=ds.Tables[0].Rows[0]["Description"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Code"]!=null && ds.Tables[0].Rows[0]["Code"].ToString()!="")
				{
					model.Code=ds.Tables[0].Rows[0]["Code"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Text"]!=null && ds.Tables[0].Rows[0]["Text"].ToString()!="")
				{
					model.Text=ds.Tables[0].Rows[0]["Text"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OrderID"]!=null && ds.Tables[0].Rows[0]["OrderID"].ToString()!="")
				{
					model.OrderID=int.Parse(ds.Tables[0].Rows[0]["OrderID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Lan"]!=null && ds.Tables[0].Rows[0]["Lan"].ToString()!="")
				{
					model.Lan=ds.Tables[0].Rows[0]["Lan"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TypeCode,Description,Code,Text,OrderID,Lan ");
			strSql.Append(" FROM DropdownList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TypeCode,Description,Code,Text,OrderID,Lan ");
			strSql.Append(" FROM DropdownList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM DropdownList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Lan desc");
			}
			strSql.Append(")AS Row, T.*  from DropdownList T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "DropdownList";
			parameters[1].Value = "Lan";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

