﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:TENDER
	/// </summary>
	public partial class TENDER:ITENDER
	{
		public TENDER()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("TenderID", "TENDER"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string TenderCode,int TenderID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from TENDER");
			strSql.Append(" where TenderCode=@TenderCode and TenderID=@TenderID ");
			SqlParameter[] parameters = {
					new SqlParameter("@TenderCode", SqlDbType.VarChar,512),
					new SqlParameter("@TenderID", SqlDbType.Int,4)			};
			parameters[0].Value = TenderCode;
			parameters[1].Value = TenderID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Edge.SVA.Model.TENDER model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into TENDER(");
			strSql.Append("TenderCode,TenderType,TenderName1,TenderName2,TenderName3,CashSale,Status,Base,Rate,MinAmount,MaxAmount,CardBegin,CardEnd,CardLen,Additional,BankID,TenderPicFile,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,BrandID)");
			strSql.Append(" values (");
			strSql.Append("@TenderCode,@TenderType,@TenderName1,@TenderName2,@TenderName3,@CashSale,@Status,@Base,@Rate,@MinAmount,@MaxAmount,@CardBegin,@CardEnd,@CardLen,@Additional,@BankID,@TenderPicFile,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@BrandID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TenderCode", SqlDbType.VarChar,512),
					new SqlParameter("@TenderType", SqlDbType.Int,4),
					new SqlParameter("@TenderName1", SqlDbType.NVarChar,512),
					new SqlParameter("@TenderName2", SqlDbType.NVarChar,512),
					new SqlParameter("@TenderName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CashSale", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Base", SqlDbType.Decimal,9),
					new SqlParameter("@Rate", SqlDbType.Decimal,9),
					new SqlParameter("@MinAmount", SqlDbType.Float,8),
					new SqlParameter("@MaxAmount", SqlDbType.Float,8),
					new SqlParameter("@CardBegin", SqlDbType.VarChar,512),
					new SqlParameter("@CardEnd", SqlDbType.VarChar,512),
					new SqlParameter("@CardLen", SqlDbType.Int,4),
					new SqlParameter("@Additional", SqlDbType.VarChar,512),
					new SqlParameter("@BankID", SqlDbType.Int,4),
					new SqlParameter("@TenderPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4)};
			parameters[0].Value = model.TenderCode;
			parameters[1].Value = model.TenderType;
			parameters[2].Value = model.TenderName1;
			parameters[3].Value = model.TenderName2;
			parameters[4].Value = model.TenderName3;
			parameters[5].Value = model.CashSale;
			parameters[6].Value = model.Status;
			parameters[7].Value = model.Base;
			parameters[8].Value = model.Rate;
			parameters[9].Value = model.MinAmount;
			parameters[10].Value = model.MaxAmount;
			parameters[11].Value = model.CardBegin;
			parameters[12].Value = model.CardEnd;
			parameters[13].Value = model.CardLen;
			parameters[14].Value = model.Additional;
			parameters[15].Value = model.BankID;
			parameters[16].Value = model.TenderPicFile;
			parameters[17].Value = model.CreatedOn;
			parameters[18].Value = model.CreatedBy;
			parameters[19].Value = model.UpdatedOn;
			parameters[20].Value = model.UpdatedBy;
			parameters[21].Value = model.BrandID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.TENDER model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update TENDER set ");
			strSql.Append("TenderType=@TenderType,");
			strSql.Append("TenderName1=@TenderName1,");
			strSql.Append("TenderName2=@TenderName2,");
			strSql.Append("TenderName3=@TenderName3,");
			strSql.Append("CashSale=@CashSale,");
			strSql.Append("Status=@Status,");
			strSql.Append("Base=@Base,");
			strSql.Append("Rate=@Rate,");
			strSql.Append("MinAmount=@MinAmount,");
			strSql.Append("MaxAmount=@MaxAmount,");
			strSql.Append("CardBegin=@CardBegin,");
			strSql.Append("CardEnd=@CardEnd,");
			strSql.Append("CardLen=@CardLen,");
			strSql.Append("Additional=@Additional,");
			strSql.Append("BankID=@BankID,");
			strSql.Append("TenderPicFile=@TenderPicFile,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("UpdatedOn=@UpdatedOn,");
			strSql.Append("UpdatedBy=@UpdatedBy,");
			strSql.Append("BrandID=@BrandID");
			strSql.Append(" where TenderID=@TenderID");
			SqlParameter[] parameters = {
					new SqlParameter("@TenderType", SqlDbType.Int,4),
					new SqlParameter("@TenderName1", SqlDbType.NVarChar,512),
					new SqlParameter("@TenderName2", SqlDbType.NVarChar,512),
					new SqlParameter("@TenderName3", SqlDbType.NVarChar,512),
					new SqlParameter("@CashSale", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Int,4),
					new SqlParameter("@Base", SqlDbType.Decimal,9),
					new SqlParameter("@Rate", SqlDbType.Decimal,9),
					new SqlParameter("@MinAmount", SqlDbType.Float,8),
					new SqlParameter("@MaxAmount", SqlDbType.Float,8),
					new SqlParameter("@CardBegin", SqlDbType.VarChar,512),
					new SqlParameter("@CardEnd", SqlDbType.VarChar,512),
					new SqlParameter("@CardLen", SqlDbType.Int,4),
					new SqlParameter("@Additional", SqlDbType.VarChar,512),
					new SqlParameter("@BankID", SqlDbType.Int,4),
					new SqlParameter("@TenderPicFile", SqlDbType.NVarChar,512),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.Int,4),
					new SqlParameter("@UpdatedOn", SqlDbType.DateTime),
					new SqlParameter("@UpdatedBy", SqlDbType.Int,4),
					new SqlParameter("@BrandID", SqlDbType.Int,4),
					new SqlParameter("@TenderID", SqlDbType.Int,4),
					new SqlParameter("@TenderCode", SqlDbType.VarChar,512)};
			parameters[0].Value = model.TenderType;
			parameters[1].Value = model.TenderName1;
			parameters[2].Value = model.TenderName2;
			parameters[3].Value = model.TenderName3;
			parameters[4].Value = model.CashSale;
			parameters[5].Value = model.Status;
			parameters[6].Value = model.Base;
			parameters[7].Value = model.Rate;
			parameters[8].Value = model.MinAmount;
			parameters[9].Value = model.MaxAmount;
			parameters[10].Value = model.CardBegin;
			parameters[11].Value = model.CardEnd;
			parameters[12].Value = model.CardLen;
			parameters[13].Value = model.Additional;
			parameters[14].Value = model.BankID;
			parameters[15].Value = model.TenderPicFile;
			parameters[16].Value = model.CreatedOn;
			parameters[17].Value = model.CreatedBy;
			parameters[18].Value = model.UpdatedOn;
			parameters[19].Value = model.UpdatedBy;
			parameters[20].Value = model.BrandID;
			parameters[21].Value = model.TenderID;
			parameters[22].Value = model.TenderCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int TenderID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from TENDER ");
			strSql.Append(" where TenderID=@TenderID");
			SqlParameter[] parameters = {
					new SqlParameter("@TenderID", SqlDbType.Int,4)
			};
			parameters[0].Value = TenderID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string TenderCode,int TenderID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from TENDER ");
			strSql.Append(" where TenderCode=@TenderCode and TenderID=@TenderID ");
			SqlParameter[] parameters = {
					new SqlParameter("@TenderCode", SqlDbType.VarChar,512),
					new SqlParameter("@TenderID", SqlDbType.Int,4)			};
			parameters[0].Value = TenderCode;
			parameters[1].Value = TenderID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string TenderIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from TENDER ");
			strSql.Append(" where TenderID in ("+TenderIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.TENDER GetModel(int TenderID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TenderID,TenderCode,TenderType,TenderName1,TenderName2,TenderName3,CashSale,Status,Base,Rate,MinAmount,MaxAmount,CardBegin,CardEnd,CardLen,Additional,BankID,TenderPicFile,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,BrandID from TENDER ");
			strSql.Append(" where TenderID=@TenderID");
			SqlParameter[] parameters = {
					new SqlParameter("@TenderID", SqlDbType.Int,4)
			};
			parameters[0].Value = TenderID;

			Edge.SVA.Model.TENDER model=new Edge.SVA.Model.TENDER();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.TENDER DataRowToModel(DataRow row)
		{
			Edge.SVA.Model.TENDER model=new Edge.SVA.Model.TENDER();
			if (row != null)
			{
				if(row["TenderID"]!=null && row["TenderID"].ToString()!="")
				{
					model.TenderID=int.Parse(row["TenderID"].ToString());
				}
				if(row["TenderCode"]!=null)
				{
					model.TenderCode=row["TenderCode"].ToString();
				}
				if(row["TenderType"]!=null && row["TenderType"].ToString()!="")
				{
					model.TenderType=int.Parse(row["TenderType"].ToString());
				}
				if(row["TenderName1"]!=null)
				{
					model.TenderName1=row["TenderName1"].ToString();
				}
				if(row["TenderName2"]!=null)
				{
					model.TenderName2=row["TenderName2"].ToString();
				}
				if(row["TenderName3"]!=null)
				{
					model.TenderName3=row["TenderName3"].ToString();
				}
				if(row["CashSale"]!=null && row["CashSale"].ToString()!="")
				{
					model.CashSale=int.Parse(row["CashSale"].ToString());
				}
				if(row["Status"]!=null && row["Status"].ToString()!="")
				{
					model.Status=int.Parse(row["Status"].ToString());
				}
				if(row["Base"]!=null && row["Base"].ToString()!="")
				{
					model.Base=decimal.Parse(row["Base"].ToString());
				}
				if(row["Rate"]!=null && row["Rate"].ToString()!="")
				{
					model.Rate=decimal.Parse(row["Rate"].ToString());
				}
				if(row["MinAmount"]!=null && row["MinAmount"].ToString()!="")
				{
					model.MinAmount=decimal.Parse(row["MinAmount"].ToString());
				}
				if(row["MaxAmount"]!=null && row["MaxAmount"].ToString()!="")
				{
					model.MaxAmount=decimal.Parse(row["MaxAmount"].ToString());
				}
				if(row["CardBegin"]!=null)
				{
					model.CardBegin=row["CardBegin"].ToString();
				}
				if(row["CardEnd"]!=null)
				{
					model.CardEnd=row["CardEnd"].ToString();
				}
				if(row["CardLen"]!=null && row["CardLen"].ToString()!="")
				{
					model.CardLen=int.Parse(row["CardLen"].ToString());
				}
				if(row["Additional"]!=null)
				{
					model.Additional=row["Additional"].ToString();
				}
				if(row["BankID"]!=null && row["BankID"].ToString()!="")
				{
					model.BankID=int.Parse(row["BankID"].ToString());
				}
				if(row["TenderPicFile"]!=null)
				{
					model.TenderPicFile=row["TenderPicFile"].ToString();
				}
				if(row["CreatedOn"]!=null && row["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(row["CreatedOn"].ToString());
				}
				if(row["CreatedBy"]!=null && row["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=int.Parse(row["CreatedBy"].ToString());
				}
				if(row["UpdatedOn"]!=null && row["UpdatedOn"].ToString()!="")
				{
					model.UpdatedOn=DateTime.Parse(row["UpdatedOn"].ToString());
				}
				if(row["UpdatedBy"]!=null && row["UpdatedBy"].ToString()!="")
				{
					model.UpdatedBy=int.Parse(row["UpdatedBy"].ToString());
				}
				if(row["BrandID"]!=null && row["BrandID"].ToString()!="")
				{
					model.BrandID=int.Parse(row["BrandID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TenderID,TenderCode,TenderType,TenderName1,TenderName2,TenderName3,CashSale,Status,Base,Rate,MinAmount,MaxAmount,CardBegin,CardEnd,CardLen,Additional,BankID,TenderPicFile,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,BrandID ");
			strSql.Append(" FROM TENDER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TenderID,TenderCode,TenderType,TenderName1,TenderName2,TenderName3,CashSale,Status,Base,Rate,MinAmount,MaxAmount,CardBegin,CardEnd,CardLen,Additional,BankID,TenderPicFile,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,BrandID ");
			strSql.Append(" FROM TENDER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM TENDER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.TenderID desc");
			}
			strSql.Append(")AS Row, T.*  from TENDER T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "TENDER";
			parameters[1].Value = "TenderID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

