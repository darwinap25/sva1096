﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Edge.SVA.IDAL;
using Edge.DBUtility;//Please add references
namespace Edge.SVA.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:BatchActive
	/// </summary>
	public partial class BatchActive:IBatchActive
	{
		public BatchActive()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string Number)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from BatchActive");
			strSql.Append(" where Number=@Number ");
			SqlParameter[] parameters = {
					new SqlParameter("@Number", SqlDbType.VarChar,20)};
			parameters[0].Value = Number;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.BatchActive model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into BatchActive(");
			strSql.Append("Number,CardType,BatchID,CardAmount,TotalAmount,CardCount,CreatedOn,CreatedBy,Remark,ServerID,ShopID,POSID,TxnNo,BusDate,TxnDate)");
			strSql.Append(" values (");
			strSql.Append("@Number,@CardType,@BatchID,@CardAmount,@TotalAmount,@CardCount,@CreatedOn,@CreatedBy,@Remark,@ServerID,@ShopID,@POSID,@TxnNo,@BusDate,@TxnDate)");
			SqlParameter[] parameters = {
					new SqlParameter("@Number", SqlDbType.VarChar,20),
					new SqlParameter("@CardType", SqlDbType.NVarChar,20),
					new SqlParameter("@BatchID", SqlDbType.VarChar,30),
					new SqlParameter("@CardAmount", SqlDbType.Money,8),
					new SqlParameter("@TotalAmount", SqlDbType.Money,8),
					new SqlParameter("@CardCount", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,10),
					new SqlParameter("@Remark", SqlDbType.VarChar,100),
					new SqlParameter("@ServerID", SqlDbType.VarChar,30),
					new SqlParameter("@ShopID", SqlDbType.VarChar,8),
					new SqlParameter("@POSID", SqlDbType.VarChar,8),
					new SqlParameter("@TxnNo", SqlDbType.VarChar,30),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@TxnDate", SqlDbType.DateTime)};
			parameters[0].Value = model.Number;
			parameters[1].Value = model.CardType;
			parameters[2].Value = model.BatchID;
			parameters[3].Value = model.CardAmount;
			parameters[4].Value = model.TotalAmount;
			parameters[5].Value = model.CardCount;
			parameters[6].Value = model.CreatedOn;
			parameters[7].Value = model.CreatedBy;
			parameters[8].Value = model.Remark;
			parameters[9].Value = model.ServerID;
			parameters[10].Value = model.ShopID;
			parameters[11].Value = model.POSID;
			parameters[12].Value = model.TxnNo;
			parameters[13].Value = model.BusDate;
			parameters[14].Value = model.TxnDate;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.BatchActive model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update BatchActive set ");
			strSql.Append("CardType=@CardType,");
			strSql.Append("BatchID=@BatchID,");
			strSql.Append("CardAmount=@CardAmount,");
			strSql.Append("TotalAmount=@TotalAmount,");
			strSql.Append("CardCount=@CardCount,");
			strSql.Append("CreatedOn=@CreatedOn,");
			strSql.Append("CreatedBy=@CreatedBy,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("ServerID=@ServerID,");
			strSql.Append("ShopID=@ShopID,");
			strSql.Append("POSID=@POSID,");
			strSql.Append("TxnNo=@TxnNo,");
			strSql.Append("BusDate=@BusDate,");
			strSql.Append("TxnDate=@TxnDate");
			strSql.Append(" where Number=@Number ");
			SqlParameter[] parameters = {
					new SqlParameter("@CardType", SqlDbType.NVarChar,20),
					new SqlParameter("@BatchID", SqlDbType.VarChar,30),
					new SqlParameter("@CardAmount", SqlDbType.Money,8),
					new SqlParameter("@TotalAmount", SqlDbType.Money,8),
					new SqlParameter("@CardCount", SqlDbType.Int,4),
					new SqlParameter("@CreatedOn", SqlDbType.DateTime),
					new SqlParameter("@CreatedBy", SqlDbType.VarChar,10),
					new SqlParameter("@Remark", SqlDbType.VarChar,100),
					new SqlParameter("@ServerID", SqlDbType.VarChar,30),
					new SqlParameter("@ShopID", SqlDbType.VarChar,8),
					new SqlParameter("@POSID", SqlDbType.VarChar,8),
					new SqlParameter("@TxnNo", SqlDbType.VarChar,30),
					new SqlParameter("@BusDate", SqlDbType.DateTime),
					new SqlParameter("@TxnDate", SqlDbType.DateTime),
					new SqlParameter("@Number", SqlDbType.VarChar,20)};
			parameters[0].Value = model.CardType;
			parameters[1].Value = model.BatchID;
			parameters[2].Value = model.CardAmount;
			parameters[3].Value = model.TotalAmount;
			parameters[4].Value = model.CardCount;
			parameters[5].Value = model.CreatedOn;
			parameters[6].Value = model.CreatedBy;
			parameters[7].Value = model.Remark;
			parameters[8].Value = model.ServerID;
			parameters[9].Value = model.ShopID;
			parameters[10].Value = model.POSID;
			parameters[11].Value = model.TxnNo;
			parameters[12].Value = model.BusDate;
			parameters[13].Value = model.TxnDate;
			parameters[14].Value = model.Number;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string Number)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BatchActive ");
			strSql.Append(" where Number=@Number ");
			SqlParameter[] parameters = {
					new SqlParameter("@Number", SqlDbType.VarChar,20)};
			parameters[0].Value = Number;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Numberlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BatchActive ");
			strSql.Append(" where Number in ("+Numberlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.BatchActive GetModel(string Number)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Number,CardType,BatchID,CardAmount,TotalAmount,CardCount,CreatedOn,CreatedBy,Remark,ServerID,ShopID,POSID,TxnNo,BusDate,TxnDate from BatchActive ");
			strSql.Append(" where Number=@Number ");
			SqlParameter[] parameters = {
					new SqlParameter("@Number", SqlDbType.VarChar,20)};
			parameters[0].Value = Number;

			Edge.SVA.Model.BatchActive model=new Edge.SVA.Model.BatchActive();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Number"]!=null && ds.Tables[0].Rows[0]["Number"].ToString()!="")
				{
					model.Number=ds.Tables[0].Rows[0]["Number"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardType"]!=null && ds.Tables[0].Rows[0]["CardType"].ToString()!="")
				{
					model.CardType=ds.Tables[0].Rows[0]["CardType"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BatchID"]!=null && ds.Tables[0].Rows[0]["BatchID"].ToString()!="")
				{
					model.BatchID=ds.Tables[0].Rows[0]["BatchID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CardAmount"]!=null && ds.Tables[0].Rows[0]["CardAmount"].ToString()!="")
				{
					model.CardAmount=decimal.Parse(ds.Tables[0].Rows[0]["CardAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TotalAmount"]!=null && ds.Tables[0].Rows[0]["TotalAmount"].ToString()!="")
				{
					model.TotalAmount=decimal.Parse(ds.Tables[0].Rows[0]["TotalAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CardCount"]!=null && ds.Tables[0].Rows[0]["CardCount"].ToString()!="")
				{
					model.CardCount=int.Parse(ds.Tables[0].Rows[0]["CardCount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedOn"]!=null && ds.Tables[0].Rows[0]["CreatedOn"].ToString()!="")
				{
					model.CreatedOn=DateTime.Parse(ds.Tables[0].Rows[0]["CreatedOn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreatedBy"]!=null && ds.Tables[0].Rows[0]["CreatedBy"].ToString()!="")
				{
					model.CreatedBy=ds.Tables[0].Rows[0]["CreatedBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ServerID"]!=null && ds.Tables[0].Rows[0]["ServerID"].ToString()!="")
				{
					model.ServerID=ds.Tables[0].Rows[0]["ServerID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ShopID"]!=null && ds.Tables[0].Rows[0]["ShopID"].ToString()!="")
				{
					model.ShopID=ds.Tables[0].Rows[0]["ShopID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["POSID"]!=null && ds.Tables[0].Rows[0]["POSID"].ToString()!="")
				{
					model.POSID=ds.Tables[0].Rows[0]["POSID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TxnNo"]!=null && ds.Tables[0].Rows[0]["TxnNo"].ToString()!="")
				{
					model.TxnNo=ds.Tables[0].Rows[0]["TxnNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BusDate"]!=null && ds.Tables[0].Rows[0]["BusDate"].ToString()!="")
				{
					model.BusDate=DateTime.Parse(ds.Tables[0].Rows[0]["BusDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TxnDate"]!=null && ds.Tables[0].Rows[0]["TxnDate"].ToString()!="")
				{
					model.TxnDate=DateTime.Parse(ds.Tables[0].Rows[0]["TxnDate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Number,CardType,BatchID,CardAmount,TotalAmount,CardCount,CreatedOn,CreatedBy,Remark,ServerID,ShopID,POSID,TxnNo,BusDate,TxnDate ");
			strSql.Append(" FROM BatchActive ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Number,CardType,BatchID,CardAmount,TotalAmount,CardCount,CreatedOn,CreatedBy,Remark,ServerID,ShopID,POSID,TxnNo,BusDate,TxnDate ");
			strSql.Append(" FROM BatchActive ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
			parameters[0].Value = "BatchActive";
			parameters[1].Value = "*";
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds");
		}

        /// <summary>
        /// 获取行总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(*) from BatchActive ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());
        }
		#endregion  Method
	}
}

