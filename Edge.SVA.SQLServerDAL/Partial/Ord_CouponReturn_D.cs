﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Ord_CouponReturn_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "Ord_CouponReturn_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "Ord_CouponReturn_D.KeyID";
        }

        public bool DeleteByOrder(string couponReturnNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from Ord_CouponReturn_D where CouponReturnNumber = @CouponReturnNumber");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@CouponReturnNumber",SqlDbType.VarChar,64){ Value = couponReturnNumber }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
