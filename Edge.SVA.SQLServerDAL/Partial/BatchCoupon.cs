﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.IDAL;
using Edge.DBUtility;
using System.Data;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class BatchCoupon : IBatchCoupon
    {
        public Dictionary<int, string> GetBatchID(int top)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("select top {0} BatchCouponID,BatchCouponCode from BatchCoupon order by BatchCouponID desc", top);

            Dictionary<int, string> batchList = new Dictionary<int, string>();

            using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString()))
            {
                int batchCouponID = 0;
                while (reader.Read())
                {
                    if (int.TryParse(reader["BatchCouponID"].ToString(), out batchCouponID))
                    {
                        batchList.Add(batchCouponID, reader["BatchCouponCode"].ToString().Trim());
                    }
                }
            }
            return batchList;

        }

        public Dictionary<int, string> GetBatchID(int top, string partialBatchCode)
        {
            StringBuilder sql = new StringBuilder(150);

            sql.AppendFormat("select  top {0} BatchCouponID,BatchCouponCode from BatchCoupon ", top);
            sql.Append("where BatchCouponCode like '%'+@PartialBatchCode+'%' order by BatchCouponID");


            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PartialBatchCode", partialBatchCode)  };

            Dictionary<int, string> batchList = new Dictionary<int, string>();
            using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString(),parameters))
            {
                int batchCouponID = 0;
                while (reader.Read())
                {
                    if (int.TryParse(reader["BatchCouponID"].ToString(), out batchCouponID))
                    {
                        batchList.Add(batchCouponID, reader["BatchCouponCode"].ToString());
                    }
                }
            }
            return batchList;
        }

        public Dictionary<int,string> GetBatchID(int top, int couponTypeID)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("select top {0} BatchCouponID ,BatchCouponCode from BatchCoupon where CouponTypeID = @CouponTypeID order by BatchCouponID desc", top);

            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@CouponTypeID", couponTypeID) };

            Dictionary<int, string> batchList = new Dictionary<int, string>();

            using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
            {
                int batchCouponID = 0;

                while (reader.Read())
                {
                    if (int.TryParse(reader["BatchCouponID"].ToString(), out batchCouponID))
                    {
                        batchList.Add(batchCouponID, reader["BatchCouponCode"].ToString());
                    }
                }
            }
            return batchList;
        }

        public Dictionary<int, string> GetBatchID(int top, string partialBatchCode, int couponTypeID)
        {

            StringBuilder sql = new StringBuilder(150);

            sql.AppendFormat("select  top {0} BatchCouponID,BatchCouponCode from BatchCoupon ", top);
            sql.Append("where BatchCouponCode like '%'+@PartialBatchCode+'%' ");
            sql.Append("and CouponTypeID = @CouponTypeID ");
            sql.Append("order by BatchCouponID desc");

            SqlParameter[] parameters = new SqlParameter[] { 
                                            new SqlParameter("@PartialBatchCode", partialBatchCode),
                                            new SqlParameter("@CouponTypeID",couponTypeID)
             };

            Dictionary<int, string> batchList = new Dictionary<int, string>();
            using (IDataReader reader = DbHelperSQL.ExecuteReader(sql.ToString(),parameters))
            {
                int batchCouponID = 0;
                while (reader.Read())
                {
                  
                    if (int.TryParse(reader["BatchCouponID"].ToString(), out batchCouponID))
                    {
                        batchList.Add(batchCouponID, reader["BatchCouponCode"].ToString());
                    }
                }
            }
            return batchList;
        }

        public bool ExistBatchCode(string batchCouponCode)
        {
            StringBuilder sql = new StringBuilder(150);

            sql.Append("select count(1) from BatchCoupon ");
            sql.Append("where BatchCouponCode = @BatchCouponCode ");

            return DbHelperSQL.Exists(sql.ToString(), new SqlParameter[] { new SqlParameter("@BatchCouponCode", batchCouponCode) });
        }

        public DataSet GetBatchID()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select BatchCouponID,BatchCouponCode from BatchCoupon order by BatchCouponID desc");
            return DbHelperSQL.Query(sql.ToString()); ;
        }

        public DataSet GetBatchIDByType(int couponTypeID)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("select BatchCouponID ,BatchCouponCode from BatchCoupon where CouponTypeID = @CouponTypeID order by BatchCouponID desc");
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@CouponTypeID", couponTypeID) };
            return DbHelperSQL.Query(sql.ToString(), parameters); ;
        }

    }
}
