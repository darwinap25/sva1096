﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.DBUtility;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Member
    {
        public List<int> GetMembers(string mobileNumber)
        {
            List<int> members = new List<int>();

            StringBuilder sql = new StringBuilder(100);
            sql.Append("select MemberID from Member ");
            sql.Append("where MemberRegisterMobile = @MemberRegisterMobile");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new System.Data.SqlClient.SqlParameter("@MemberRegisterMobile", System.Data.SqlDbType.VarChar) { Value = mobileNumber }
            };
            System.Data.IDataReader reader = null;
            try
            {
                reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters);
                while (reader.Read())
                {
                    members.Add(int.Parse(reader["MemberID"].ToString()));
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return members;
        }
        // Add by Alex 2014-07-28 ++
        public DataSet GetMembersID(string memberMobilePhone, string memberEmail)
        {
            List<int> members = new List<int>();

            StringBuilder sql = new StringBuilder(100);
            sql.Append("select MemberID from Member ");
            sql.Append("where 1=1 ");
            if (memberMobilePhone != "" && memberEmail =="")
            {
                sql.Append("and MemberMobilePhone like  '%"+memberMobilePhone +"%'");
            }
            if (memberEmail != "" && memberMobilePhone =="")
            {
                sql.Append("and MemberEmail like '%" + memberEmail + "%'");
            }
            if (memberEmail != "" && memberMobilePhone != "")
            {
                sql.Append("and MemberMobilePhone like '%" + memberMobilePhone + "%'");
                sql.Append("and MemberEmail like '%" + memberEmail + "%'");
            }
            return DbHelperSQL.Query(sql.ToString());
        }
        // Add by Alex 2014-07-28 --
    }
}
