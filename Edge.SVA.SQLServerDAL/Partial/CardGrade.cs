﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CardGrade
    {
        //Add by Alex 2014-06-09 ++
        public SVA.Model.CardGrade GetImportCardGrade(string cardGradeCode)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select top 1 CardGradeID,IsImportUIDNumber,UIDCheckDigit,CardGradeName1,CardGradeName2,CardGradeName3 from CardGrade ");
            sql.Append(" where CardGradeCode=@CardGradeCode");

            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter("@CardGradeCode", System.Data.SqlDbType.VarChar){Value=cardGradeCode}
            };
            int cardGradID = 0, uidCheckdigit = 0, brandID = 0;
            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
            {
                Edge.SVA.Model.CardGrade gradeType = null;
                if (reader.Read())
                {
                    gradeType = new Model.CardGrade();
                    gradeType.CardGradeCode = cardGradeCode;
                    gradeType.IsImportUIDNumber = reader["IsImportUIDNumber"] != DBNull.Value && reader["IsImportUIDNumber"].ToString() == "1" ? 1 : 0;
                    gradeType.CardGradeID = int.TryParse(reader["CardGradeID"].ToString(), out cardGradID) ? cardGradID : -1;
                    gradeType.UIDCheckDigit = int.TryParse(reader["UIDCheckDigit"].ToString(), out uidCheckdigit) ? uidCheckdigit : -1;
                    //cardType.BrandID = int.TryParse(reader["BrandID"].ToString(), out brandID) ? brandID : -1;
                    gradeType.CardGradeName1 = reader["CardGradeName1"].ToString().Trim();
                    gradeType.CardGradeName2 = reader["CardGradeName2"].ToString().Trim();
                    gradeType.CardGradeName3 = reader["CardGradeName3"].ToString().Trim();
                }
                return gradeType;
            }


        }
        //Add by Alex 2014-06-09 --
    }
}
