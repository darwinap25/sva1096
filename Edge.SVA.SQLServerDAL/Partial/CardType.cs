﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CardType
    {
        public List<int> GetCardTypes(int brandID)
        {
            List<int> cardTypes = new List<int>();

            StringBuilder sql = new StringBuilder(100);
            sql.Append("select CardTypeID from CardType ");
            sql.Append("where BrandID  = @BrandID");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new System.Data.SqlClient.SqlParameter("@BrandID", System.Data.SqlDbType.VarChar) { Value = brandID }
            };
            System.Data.IDataReader reader = null;
            try
            {
                reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters);
                while (reader.Read())
                {
                    cardTypes.Add(int.Parse(reader["CardTypeID"].ToString()));
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return cardTypes;
        }
        //Add by Alex 2014-06-09 ++
        public SVA.Model.CardType GetImportCardType(string cardTypeCode)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select top 1 CardTypeID,IsImportUIDNumber,UIDCheckDigit,BrandID,CardTypeName1,CardTypeName2,CardTypeName3 from CardType ");
            sql.Append(" where CardTypeCode=@CardTypeCode");

            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter("@CardTypeCode", System.Data.SqlDbType.VarChar){Value=cardTypeCode}
            };
            int couponTypeID = 0, uidCheckdigit = 0, brandID = 0;
            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
            {
                Edge.SVA.Model.CardType cardType = null;
                if (reader.Read())
                {
                    cardType = new Model.CardType();
                    cardType.CardTypeCode = cardTypeCode;
                    cardType.IsImportUIDNumber = reader["IsImportUIDNumber"] != DBNull.Value && reader["IsImportUIDNumber"].ToString() == "1" ? 1 : 0;
                    cardType.CardTypeID = int.TryParse(reader["CardTypeID"].ToString(), out couponTypeID) ? couponTypeID : -1;
                    cardType.UIDCheckDigit = int.TryParse(reader["UIDCheckDigit"].ToString(), out uidCheckdigit) ? uidCheckdigit : -1;
                    cardType.BrandID = int.TryParse(reader["BrandID"].ToString(), out brandID) ? brandID : -1;
                    cardType.CardTypeName1 = reader["CardTypeName1"].ToString().Trim();
                    cardType.CardTypeName2 = reader["CardTypeName2"].ToString().Trim();
                    cardType.CardTypeName3 = reader["CardTypeName3"].ToString().Trim();
                }
                return cardType;
            }


        }
        //Add by Alex 2014-06-09 --
    }
}
