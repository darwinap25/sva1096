﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CouponReplenishRule_H : BaseDAL
    {
        protected override string TableName
        {
            get { return "CouponReplenishRule_H"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "CouponReplenishRule_H.CouponReplenishCode";
        }
    }
}
