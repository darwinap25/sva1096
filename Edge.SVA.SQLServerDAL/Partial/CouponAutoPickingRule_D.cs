﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CouponAutoPickingRule_D : BaseDAL
    {
        protected override string TableName
        {
            get { return "CouponAutoPickingRule_D"; }
        }
        protected override void Initialization()
        {
            base.Initialization();
            this.Order = "CouponAutoPickingRule_D.KeyID";
        }

        public bool DeleteByCode(string couponAutoPickingRuleCode)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("delete from CouponAutoPickingRule_D where CouponAutoPickingRuleCode = @CouponAutoPickingRuleCode");

            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]
            {
                new SqlParameter("@couponAutoPickingRuleCode",SqlDbType.VarChar,64){ Value = couponAutoPickingRuleCode }
            };

            return DBUtility.DbHelperSQL.ExecuteSql(sql.ToString(), parameters) > 0;
        }
    }
}
