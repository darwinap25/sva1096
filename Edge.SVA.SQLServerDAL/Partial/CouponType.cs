﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.IDAL;
using System.Data.SqlClient;

namespace Edge.SVA.SQLServerDAL
{
    public partial class CouponType : ICouponType
    {
        public SVA.Model.CouponType GetImportCouponType(string couponTypeCode)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select top 1 CouponTypeID,IsImportCouponNumber,UIDCheckDigit,BrandID,CouponTypeName1,CouponTypeName2,CouponTypeName3 from CouponType ");
            sql.Append(" where CouponTypeCode=@CouponTypeCode");

            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter("@CouponTypeCode", System.Data.SqlDbType.VarChar){Value=couponTypeCode}
            };
            int couponTypeID = 0, uidCheckdigit = 0, brandID = 0;
            using (System.Data.IDataReader reader = DBUtility.DbHelperSQL.ExecuteReader(sql.ToString(), parameters))
            {
                Edge.SVA.Model.CouponType couponType = null;
                if (reader.Read())
                {
                    couponType = new Model.CouponType();
                    couponType.CouponTypeCode = couponTypeCode;
                    couponType.IsImportCouponNumber = reader["IsImportCouponNumber"] != DBNull.Value && reader["IsImportCouponNumber"].ToString() == "1" ? 1 : 0;
                    couponType.CouponTypeID = int.TryParse(reader["CouponTypeID"].ToString(), out couponTypeID) ? couponTypeID : -1;
                    couponType.UIDCheckDigit = int.TryParse(reader["UIDCheckDigit"].ToString(), out uidCheckdigit) ? uidCheckdigit : -1;
                    couponType.BrandID = int.TryParse(reader["BrandID"].ToString(), out brandID) ? brandID : -1;
                    couponType.CouponTypeName1 = reader["CouponTypeName1"].ToString().Trim();
                    couponType.CouponTypeName2 = reader["CouponTypeName2"].ToString().Trim();
                    couponType.CouponTypeName3 = reader["CouponTypeName3"].ToString().Trim();
                }
                return couponType;
            }


        }
    }
}
