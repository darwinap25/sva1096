﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.IDAL;
using Edge.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace Edge.SVA.SQLServerDAL
{
    public partial class Card : ICard
    {
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetListWithBatch(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" Card.CardNumber,Card.CardTypeID,Card.CardGradeID,Card.CardIssueDate,Card.CardExpiryDate,Card.BatchCardID,Card.Status,Card.CardPassword,Card.CardNumber,Card.TotalAmount,Card.CreatedOn,Card.UpdatedOn,Card.CreatedBy,Card.UpdatedBy,BatchCard.BatchCardCode ");
            strSql.Append(" FROM Card left join BatchCard on BatchCard.BatchCardID=Card.BatchCardID ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithBatch(string strWhere)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select count(*) FROM Card left join BatchCard on BatchCard.BatchCardID=Card.BatchCardID ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            return DbHelperSQL.GetCount(sql.ToString());

        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetPageListWithBatch(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            int topNum = pageSize * currentPage;
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select top " + pageSize + " Card.CardNumber,Card.CardTypeID,Card.CardGradeID,Card.CardIssueDate,Card.CardExpiryDate,Card.BatchCardID,Card.Status,Card.CardPassword,Card.CardNumber,Card.TotalAmount,Card.CreatedOn,Card.UpdatedOn,Card.CreatedBy,Card.UpdatedBy,BatchCard.BatchCardCode");
            strSql.Append(" FROM Card left join BatchCard on BatchCard.BatchCardID=Card.BatchCardID ");
            if (currentPage > 0)
            {
                strSql.Append(" where Card.CardNumber not in(select top " + topNum + " Card.CardNumber FROM Card left join BatchCard on BatchCard.BatchCardID=Coupon.BatchCardID");
                if (strWhere.Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                strSql.Append(" order by " + filedOrder + ")");
            }
            if (strWhere.Trim() != "")
            {
                if (currentPage > 0)
                {
                    strSql.Append(" and " + strWhere);
                }
                else
                {
                    strSql.Append(" where " + strWhere);
                }
            }
            strSql.Append(" order by " + filedOrder);

            return DbHelperSQL.Query(strSql.ToString());
        }

        public bool ExsitCard(Model.Ord_ImportCardUID_H model)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select count(1) from CardUIDMap ");
            sql.Append("where CardUID in  ");
            sql.Append("(select Ord_ImportCardUID_D.CardUID from Ord_ImportCardUID_D  where ImportCardNumber = @ImportCardNumber) ");

            SqlParameter[] parameters = { new SqlParameter("@ImportCardNumber", SqlDbType.VarChar, 512) };
            parameters[0].Value = model.ImportCardNumber;

            return DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        public bool ExsitCard(Model.Ord_CardAdjust_H model)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select COUNT(CardNumber) from Card ");
            sql.Append("where CardNumber in ");
            sql.Append("(select CardNumber from Ord_CardAdjust_D where CardAdjustNumber = @CardAdjustNumber) ");

            SqlParameter[] parameters = { new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar, 512) };
            parameters[0].Value = model.CardAdjustNumber;

            return DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        public bool ExsitCard(string cardNumber)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select COUNT(CardNumber) from Card ");
            sql.Append("where CardNumber = @CardNumber ");

            SqlParameter[] parameters = { new SqlParameter("@CardNumber", SqlDbType.VarChar, 512) };
            parameters[0].Value = cardNumber;

            return DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        public bool ValidCardStatus(Model.Ord_CardAdjust_H model, params int[] CardStatus)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select COUNT(CardNumber) from Card ");
            sql.Append("where CardNumber in ");
            sql.Append("(select CardNumber from Ord_CardAdjust_D where CardAdjustNumber = @CardAdjustNumber) ");

            if (CardStatus.Length <= 0) return false;
            if (CardStatus.Length == 1)
            {
                sql.AppendFormat("and  [Status] <> {0}", CardStatus[0]);
            }
            else
            {
                string ids = "";
                for (int i = 0; i < CardStatus.Length; i++)
                {
                    ids += string.Format("{0},", CardStatus[i]);
                }

                sql.AppendFormat("and  [Status] not in ({0})", ids.Substring(0, ids.Length - 1));
            }
            SqlParameter[] parameters = { 
                                            new SqlParameter("@CardAdjustNumber", SqlDbType.VarChar, 512),
                                        };

            parameters[0].Value = model.CardAdjustNumber;


            return !DBUtility.DbHelperSQL.Exists(sql.ToString(), parameters);
        }

        ///// <summary>
        ///// 获得前几行数据Coupon数据for 批量issue,激活等等
        ///// </summary>
        //public DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select ");
        //    if (Top > 0)
        //    {
        //        strSql.Append(" top " + Top.ToString());
        //    }
        //    strSql.Append(" CouponNumber,CouponAmount,CouponExpiryDate,CreatedOn,Status,CouponTypeID,BatchCouponID ");
        //    strSql.Append(" FROM Coupon ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    strSql.Append(" order by " + filedOrder);
        //    return DbHelperSQL.Query(strSql.ToString());
        //}

        /// <summary>
        /// 获得前几行数据Coupon数据for 批量issue,激活等等
        /// </summary>
        public DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" Card.CardNumber,Card.TotalAmount,Card.CardExpiryDate,Card.CreatedOn,Card.Status,Card.CardTypeID,Card.CardGradeID,Card.BatchCardID,BatchCard.BatchCardCode as BatchCode,CardUIDMap.CardUID ");
            strSql.Append(" FROM Card ");
            strSql.Append(" left join CardUIDMap on Card.CardNumber=CardUIDMap.CardNumber ");
            strSql.Append(" left join BatchCard on Card.BatchCardID=BatchCard.BatchCardID ");
            strSql.Append(" left join Member on Card.MemberID = Member.MemberID ");// Add  by Alex 2014-07-28
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListForTotal(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Card";
            parameters[1].Value = fields;
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 1;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds", times);
        }

        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@OrderfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@StatfldName",SqlDbType.VarChar,255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.NText),
					};
            parameters[0].Value = "Card";
            parameters[1].Value = fields;
            parameters[2].Value = filedOrder;
            parameters[3].Value = "";
            parameters[4].Value = PageSize;
            parameters[5].Value = PageIndex;
            parameters[6].Value = 0;
            parameters[7].Value = 0;
            parameters[8].Value = strWhere;
            return DbHelperSQL.RunProcedure("sp_GetRecordByPageOrder", parameters, "ds", times);
        }

        public Edge.SVA.Model.Card GetModelByUID(string cardUID)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select c.Status,c.CardTypeID,c.CardGradeID,c.BatchCardID,c.CardExpiryDate,c.TotalAmount,c.CardNumber ");
            sql.Append("from Card as c inner join CardUIDMap as m on c.CardNumber = m.CardNumber  ");
            sql.Append("where m.CardUID = @CardUID");

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@CardUID",SqlDbType.VarChar) { Value = cardUID }
            };
            System.Data.IDataReader reader = null;
            Edge.SVA.Model.Card card = null;
            try
            {
                reader = DbHelperSQL.ExecuteReader(sql.ToString(), parameters);
                int i = 0;
                if (reader != null && reader.Read())
                {
                    card = new Model.Card();
                    card.Status = int.TryParse(reader["Status"].ToString(), out i) ? i : 0;
                    card.CardGradeID = int.TryParse(reader["CardGradeID"].ToString(), out i) ? i : 0;
                    card.BatchCardID = int.TryParse(reader["BatchCardID"].ToString(), out i) ? i : 0;
                    card.CardNumber = reader["CardNumber"].ToString();

                    if (reader["CardExpiryDate"] != null && !string.IsNullOrEmpty(reader["CardExpiryDate"].ToString()))
                    {
                        card.CardExpiryDate = DateTime.Parse(reader["CardExpiryDate"].ToString());
                    }

                    if (reader["TotalAmount"] != null && !string.IsNullOrEmpty(reader["TotalAmount"].ToString()))
                    {
                        card.TotalAmount = decimal.Parse(reader["TotalAmount"].ToString());
                    }


                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return card;
        }

        public int GetCount(string strWhere, int times)
        {
            StringBuilder sql = new StringBuilder(200);
            sql.Append("select count(1) from Card ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sql.AppendFormat("where {0}", strWhere);
            }

            object result = DbHelperSQL.GetSingle(sql.ToString(), times);

            if (result != null && result is int) return (int)result;

            return -1;
        }
    }
}
