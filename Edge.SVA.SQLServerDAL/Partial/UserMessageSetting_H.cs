﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.SQLServerDAL
{
    public partial class UserMessageSetting_H : BaseDAL
    {
        protected override string TableName
        {
            get { return "UserMessageSetting_H"; }
        }

        protected override void Initialization()
        {
            base.Initialization();

            this.Order = "UserMessageSetting_H.UserMessageCode";
        }
    }
}
