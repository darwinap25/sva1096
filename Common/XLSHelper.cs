﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace Common
{
    public class XLSHelper
    {

        public XLSHelper()
        {

        }

        public static DataTable OpenExcel(string FileName)
        {

            DataSet ds = new DataSet();

            OleDbCommand excelCommand = new OleDbCommand(); OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();

            string excelConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';";  //"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filelocation + "; Extended Properties =Excel 8.0;";

            OleDbConnection excelConn = new OleDbConnection(excelConnStr);

            excelConn.Open();

            DataTable dt = new DataTable();
            DataTable dtSheets = new DataTable();
            dtSheets = excelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);


            string sheetname = dtSheets.Rows[0]["TABLE_NAME"].ToString();

            excelCommand = new OleDbCommand("SELECT * FROM [" + sheetname + "]", excelConn);

            excelDataAdapter.SelectCommand = excelCommand;
            try
            {
                excelDataAdapter.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //"dtPatterns.TableName = Patterns";

            ds.Tables.Add(dt);

            return ds.Tables[0];

        }

    }
}
