﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.Common;
using System.Web;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

namespace Edge.Web.UI
{
    public class ManagePage : Asp.Net.WebFormLib.WebPageBase
    {
        #region for coupon change status
        protected static string GetCouponSearchStrWhere(int top, int batchCouponID, string couponNumber, string couponTypeID, string couponUID, string strWhere)
        {
            if (batchCouponID > 0)
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += " Coupon.BatchCouponID=" + batchCouponID;
                }
                else
                {
                    strWhere += " and Coupon.BatchCouponID =" + batchCouponID;
                }
            }
            if (!string.IsNullOrEmpty(couponTypeID))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += " Coupon.CouponTypeID in (" + couponTypeID + ")";
                }
                else
                {
                    strWhere += " and Coupon.CouponTypeID in (" + couponTypeID + ")";
                }
            }

            if (!string.IsNullOrEmpty(couponUID))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere += string.Format("Coupon.CouponNumber in (select CouponNumber from CouponUIDMap where CouponUID =  '{0}') ", couponUID);

                }
                else
                {
                    strWhere += string.Format("and Coupon.CouponNumber in (select CouponNumber from CouponUIDMap where CouponUID =  '{0}') ", couponUID);
                }
            }


            if (!string.IsNullOrEmpty(couponNumber))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    if (top > 0)
                    {
                        strWhere += " Coupon.CouponNumber >='" + couponNumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                    }
                    else
                    {
                        strWhere += " Coupon.CouponNumber = '" + couponNumber + "'";
                    }
                }
                else
                {
                    if (top > 0)
                    {
                        strWhere += " and Coupon.CouponNumber >='" + couponNumber + "'" + " and Coupon.CouponTypeID = (select top 1 CouponTypeID from Coupon where Coupon.CouponNumber = '" + couponNumber + "') ";
                    }
                    else
                    {
                        strWhere += " and Coupon.CouponNumber = '" + couponNumber + "'"; ;
                    }
                }
            }


            return strWhere;
        }
        #endregion

        protected internal Edge.Security.Model.WebSet webset;

        public ManagePage()
        {
            //this.Load += new EventHandler(ManagePage_Load);
            webset = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Header.DataBind();
        }

        protected override void InitializeCulture()
        {
            if (Session["SiteLanguage"] != null)
            {
                string lan = Session["SiteLanguage"].ToString().ToLower();
                UICulture = lan;
                Culture = lan;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lan);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lan);
            }
            base.InitializeCulture();
        }

        #region Cancel Show Window
        /// <summary>
        /// 打开提示窗口
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        //protected void ShowjAlert(string msg, string title,string url)
        //{
        //    string msbox = "";
        //    msbox += "<script type=\"text/javascript\">\n";
        //    msbox += "parent.jAlert(\"" + msg + "\",\"" + title + "\")\n";
        //    msbox += " var url=\"" + url + "\"; if ( url== \"back\") { parent.sysMain.history.back(-1);} else if (url != \"\") { parent.sysMain.location.href = url;}";
        //    msbox += "</script>\n";
        //    ClientScript.RegisterClientScriptBlock(Page.GetType(), "ShowjAlert", msbox);
        //}


        /// <summary>
        /// 遮罩提示窗口
        /// </summary>
        /// <param name="w">宽度</param>
        /// <param name="h">高度</param>
        /// <param name="msgtitle">窗口标题</param>
        /// <param name="msgbox">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        //protected void JscriptMsg(int w, int h, string msgtitle, string msgbox, string url, string msgcss)
        //{
        //    string msbox = "";
        //    msbox += "<script type=\"text/javascript\">\n";
        //    msbox += "parent.jsmsg(" + w + "," + h + ",\"" + msgtitle + "\",\"" + msgbox + "\",\"" + url + "\",\"" + msgcss + "\")\n";
        //    msbox += "</script>\n";
        //    ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsMsg", msbox);
        //}
        #endregion

        /// <summary>
        /// 添加编辑删除提示(Url = back) 后退
        /// </summary>
        /// <param name="msgtitle">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        protected void JscriptPrint(string msg, string url, string title)
        {
            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("parent.jAlert(\"{0}\",\"{1}\");", msg, title);
            msbox.AppendFormat(" var url=\"{0}\"; ", url);
            msbox.Append("if ( url== \"back\") { parent.sysMain.history.back(-1);}");
            msbox.Append("else if (url != \"\") { parent.sysMain.location.href = url;}");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "ShowjAlert", msbox.ToString());
        }

        /// 添加编辑删除提示并且关闭
        /// </summary>
        /// <param name="msgtitle">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        protected void JscriptPrintAndClose(string msg, string url, string title)
        {

            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("parent.jAlert(\"{0}\",\"{1}\");", msg, title);
            msbox.AppendFormat(" var url=\"{0}\"; ", url);
            msbox.Append("if ( url== \"back\") { parent.sysMain.history.back(-1);}");
            msbox.Append("else if (url != \"\") { parent.sysMain.location.href = url;}");
            msbox.Append("window.top.tb_remove();");
            msbox.Append("window.top.$('#TB_load').remove();");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JscriptPrintAndClose", msbox.ToString());
        }

        /// 添加编辑删除提示并且聚焦控件
        /// </summary>
        /// <param name="msgtitle">提示文字</param>
        /// <param name="url">返回地址</param>
        /// <param name="msgcss">CSS样式</param>
        protected void JscriptPrintAndFocus(string msg, string url, string title, string clientID)
        {

            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("parent.jAlert(\" {0}\",\"{1}\",function(){{", msg, title);                      //Format{->{{   } ->}}
            msbox.AppendFormat("if($(\"#{0}\").length > 0){{$(\"#{0}\").focus();}}}}); ", clientID);             //Format{->{{   } ->}}
            msbox.AppendFormat(" var url=\"{0}\"; ", url);
            msbox.Append("if ( url== \"back\") { parent.sysMain.history.back(-1);}");
            msbox.Append("else if (url != \"\") { parent.sysMain.location.href = url;}");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JscriptPrintAndFocus", msbox.ToString());
        }

        protected void CloseLoading()
        {
            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.Append("window.top.tb_remove();");
            msbox.Append("window.top.$('#TB_load').remove();");
            msbox.Append("</script>");
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "CloseLoading", msbox.ToString());
        }

        protected void AnimateRoll(string id)
        {
            id = id.StartsWith("#") ? id : "#" + id;

            StringBuilder msbox = new StringBuilder(300);
            msbox.Append("<script type=\"text/javascript\">");
            msbox.AppendFormat("if($('{0}').length > 0 ) {{ ", id);
            msbox.AppendFormat("$('html,body').animate({{scrollTop:$('{0}').offset().top}}, 800)}}", id);
            msbox.Append("</script>");

            ClientScript.RegisterStartupScript(Page.GetType(), "AnimateRoll", msbox.ToString());
        }

        ///// <summary>
        ///// 组合URL语句
        ///// </summary>
        ///// <param name="_classId">类别ID</param>
        ///// <param name="_keywords">关健字</param>
        ///// <returns></returns>
        protected string CombUrlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append("keywords=" + HttpContext.Current.Server.UrlEncode(_keywords) + "&");
            }

            return strTemp.ToString();
        }

        /// <summary>
        /// 删除单个文件
        /// </summary>
        /// <param name="_filepath">文件相对路径</param>
        protected void DeleteFile(string _filepath)
        {
            if (string.IsNullOrEmpty(_filepath))
            {
                return;
            }
            string fullpath = Edge.Common.Utils.GetMapPath(_filepath);
            if (File.Exists(fullpath))
            {
                File.Delete(fullpath);
            }
        }

        /// <summary>
        /// 生成缩略图的方法
        /// </summary>
        /// <param name="_filepath">文件相对路径</param>
        /// <returns></returns>
        protected string MakeThumbnail(string _filepath)
        {
            if (!string.IsNullOrEmpty(_filepath) && webset.IsThumbnail == 1)
            {
                string _filename = _filepath.Substring(_filepath.LastIndexOf("/") + 1);
                string _newpath = webset.WebFilePath;
                //检查保存的路径 是否有/开头结尾
                if (_newpath.StartsWith("/") == false)
                {
                    _newpath = "/" + _newpath;
                }
                if (_newpath.EndsWith("/") == false)
                {
                    _newpath = _newpath + "/";
                }
                _newpath = _newpath + "Thumbnail/";

                //检查是否有该路径没有就创建
                if (!Directory.Exists(Edge.Common.Utils.GetMapPath(_newpath)))
                {
                    Directory.CreateDirectory(Edge.Common.Utils.GetMapPath(_newpath));
                }
                //调用生成类方法
                ImageThumbnailMake.MakeThumbnail(_filepath, _newpath + _filename, webset.ProWidth, webset.ProHight, "Cut");

                return _newpath + _filename;
            }

            return _filepath;
        }

        /// <summary>
        /// 刷新整个页面
        /// </summary>
        protected void RefreshParentPage()
        {
            Response.Write("<script language=javascript>self.parent.location.reload();  </script>");
        }

        /// <summary>
        /// 父页面跳转页面
        /// </summary>
        protected void RedirectParentPageTo(string url)
        {
            Response.Write("<script language=javascript>self.parent.location='" + url + "';  </script>");
        }

        /// <summary>
        /// 刷新当前页面
        /// </summary>
        protected void RefreshPage()
        {
            Response.Redirect(Request.Url.ToString());
        }

        /// <summary>
        /// 日志写入方法
        /// </summary>
        /// <param name="str"></param>
        protected void SaveLogs(string str, string Particular)
        {
            if (webset.WebLogStatus == 0)
            {
                return;
            }
            Edge.Security.Manager.SysManage bll = new Edge.Security.Manager.SysManage();
            bll.AddLog(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), str, Particular);
        }

        protected static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' ||
                relativeUrl[0] == '\\') return relativeUrl;

            int idxOfScheme =
              relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        private static Dictionary<string, Edge.Security.Model.SysNode> nodeCache = null;
        public Dictionary<string, Edge.Security.Model.SysNode> NodeCache
        {
            get
            {
                if (nodeCache == null)
                {
                    nodeCache = new Dictionary<string, Edge.Security.Model.SysNode>();
                }
                return nodeCache;
            }
        }

        public string PageName
        {
            get
            {
                string path = System.Web.HttpContext.Current.Request.Path;

                path = path.Remove(0, System.Web.HttpContext.Current.Request.ApplicationPath.Length);
                if (path.StartsWith("/")) path = path.Remove(0, 1);

                string lan = Thread.CurrentThread.CurrentCulture.Name.ToLower();
                string key = string.Format("{0}_{1}", path, lan);

                lock (typeof(ManagePage))
                {
                    if (NodeCache.ContainsKey(key)) return NodeCache[key].Text;

                    Edge.Security.Manager.SysManage manage = new Edge.Security.Manager.SysManage();
                    Edge.Security.Model.SysNode node = manage.GetNodeByUrl(path, lan);
                    if (node == null) return "";

                    NodeCache.Add(key, node);

                    return node.Text;
                }
            }
        }

        #region JS和CS引用方法
        protected string GetJSMultiLanguagePath()
        {
            string lan = Thread.CurrentThread.CurrentUICulture.Name.ToLower();
            string path = "~/js/" + lan + "/messages.js";
            return ResolveUrl(path);
        }

        protected string GetJSFunctionPath()
        {
            //string path = "~/js/function.min.js";
            string path = "~/js/function.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryPath()
        {
            string path = "~/js/jquery-1.4.1.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryUiPath()
        {
            string path = "~/js/jquery-ui-1.8.21.custom.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryUiCssPath()
        {
            string path = "~/js/css/redmond/jquery-ui-1.8.21.custom.css";
            return ResolveUrl(path);
        }

        protected string GetjQueryValidatePath()
        {
            string path = "~/js/jquery.validate.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryFormPath()
        {
            string path = "~/js/jquery.form.js";
            return ResolveUrl(path);
        }

        protected string GetPaginationCssPath()
        {
            string path = "~/Style/pagination.css";
            return ResolveUrl(path);
        }

        protected string GetJSPaginationPath()
        {
            string path = "~/js/jquery.pagination.js";
            return ResolveUrl(path);
        }

        protected string GetJSThickBoxPath()
        {
            string path = "~/js/thickbox.js";
            return ResolveUrl(path);
        }

        protected string GetJSThickBoxCssPath()
        {
            string path = "~/Style/thickbox.css";
            return ResolveUrl(path);
        }

        protected string GetMy97DatePickerPath()
        {
            string path = "~/My97DatePicker/WdatePicker.js";
            return ResolveUrl(path);
        }

        protected string GetSearchBoxJqueryPath()
        {
            string path = "~/js/jquery-1.5.2.min.js";
            return ResolveUrl(path);
        }

        protected string GetSearchBoxCommonJsPath()
        {
            string path = "~/js/CommonJs.js";
            return ResolveUrl(path);
        }

        protected string GetSearchBoxDynamicConJsPath()
        {
            string path = "~/js/dynamicCon.js";
            return ResolveUrl(path);
        }

        protected string GetjQeruyAlertsStylePath()
        {
            string path = "~/js/jQeruyAlerts/jquery.alerts.css";
            return ResolveUrl(path);
        }

        protected string GetjQeruyAlertsPath()
        {
            string path = "~/js/jQeruyAlerts/jquery.alerts.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryLigeruiCSS()
        {
            string path = "~/js/ui/skins/Aqua/css/ligerui-all.css";
            return ResolveUrl(path);
        }

        protected string GetjQueryLigerui()
        {
            string path = "~/js/ui/js/ligerui.min.js";
            return ResolveUrl(path);
        }

        protected string GetjQueryLigeruiBase()
        {
            string path = "~/js/ui/js/core/base.js";
            return ResolveUrl(path);
        }
        #endregion
    }
}
