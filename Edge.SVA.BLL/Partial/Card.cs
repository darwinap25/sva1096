﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Card
    {

        public DataSet GetListWithBatch(int Top, string strWhere, string filedOrder)
        {
            return dal.GetListWithBatch(Top, strWhere, filedOrder);
        }

        public int GetCountWithBatch(string strWhere)
        {
            return dal.GetCountWithBatch(strWhere);
        }

        public DataSet GetPageListWithBatch(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            return dal.GetPageListWithBatch(pageSize, currentPage, strWhere, filedOrder);
        }

        public bool ExsitCard(Model.Ord_ImportCardUID_H model)
        {
            return dal.ExsitCard(model);
        }

        public bool ExsitCard(Model.Ord_CardAdjust_H model)
        {
            return dal.ExsitCard(model);
        }

        public bool ExsitCard(string cardNumber)
        {
            return dal.ExsitCard(cardNumber);
        }
        public bool ValidCardStauts(Model.Ord_CardAdjust_H model, params int[] CardStatus)
        {
            return dal.ValidCardStatus(model, CardStatus);
        }

        public DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder)
        {
            return dal.GetListForBatchOperation(Top, strWhere, filedOrder);
        }

        public DataSet GetListForTotal(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            return dal.GetListForTotal(PageSize, PageIndex, strWhere, filedOrder, fields, times);
        }

        public Edge.SVA.Model.Card GetModelByUID(string cardUID)
        {
            return dal.GetModelByUID(cardUID);
        }

        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, fields, times);
        }

        public int GetCount(string strWhere, int times)
        {
            return dal.GetCount(strWhere, times);
        }
    }
}
