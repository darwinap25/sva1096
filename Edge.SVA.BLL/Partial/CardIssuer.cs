﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// 卡发行方
    /// </summary>
    public partial class CardIssuer
    {
        private string SessionChangeCardIssuersStr(string strWhere)
        {
            //SessionInfo si = new SessionInfo();
            string str = SessionInfo.CardIssuersStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " CardIssuerID in " + str;
                }
                else
                {
                    strWhere = strWhere + " and CardIssuerID in " + str;
                }
            }
            return strWhere;
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllRecordList()
        {
            return dal.GetList(string.Empty);
        }
    }
}

