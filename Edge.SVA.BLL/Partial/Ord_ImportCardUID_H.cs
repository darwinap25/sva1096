﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_ImportCardUID_H : BaseBLL
    {
        public bool ExistCardUID(List<string> couponUIDS)
        {
            return dal.ExistCardUID(couponUIDS);
        }

        public bool ExistCardUID(string beginUID, string endUID, bool isCheckdigit)
        {
            return dal.ExistCardUID(beginUID, endUID, isCheckdigit);
        }

        public bool Update(Model.Ord_ImportCardUID_H model, int times)
        {
            return dal.Update(model, times);
        }

        public string ExportCSV(string importCardNumber, int cardCount)
        {
            return dal.ExportCSV(importCardNumber, cardCount);
        }

        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_ImportCardUID_H() as IDAL.IBaseDAL; }
        }
    }

}
