﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class Ord_CouponReturn_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateOrd_CouponReturn_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByOrder(string couponReturnNumber)
        {
            return dal.DeleteByOrder(couponReturnNumber);
        }
    }
}
