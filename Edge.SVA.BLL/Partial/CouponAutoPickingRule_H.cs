﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class CouponAutoPickingRule_H : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get
            {
                return DALFactory.DataAccess.CreateCouponAutoPickingRule_H() as IDAL.IBaseDAL;
            }
        }
    }
}
