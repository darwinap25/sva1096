﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Edge.DBUtility;

namespace Edge.SVA.BLL
{
    public partial class BatchCard
    {

        public Dictionary<int, string> GetBatchID(int top)
        {
            return dal.GetBatchID(top);

        }

        public Dictionary<int, string> GetBatchID(int top, string partialBatchID)
        {
            return dal.GetBatchID(top, partialBatchID);
        }

        public Dictionary<int, string> GetBatchID(int top, int couponTypeID)
        {
            return dal.GetBatchID(top, couponTypeID);
        }

        public Dictionary<int, string> GetBatchID(int top, string partialBatchID, int couponTypeID)
        {

            return dal.GetBatchID(top, partialBatchID, couponTypeID);
        }

        //Add By Len
        public DataSet GetBatchID()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select BatchCardID,BatchCardCode from BatchCard order by BatchCardID desc");
            return DbHelperSQL.Query(sql.ToString());
        }

        public DataSet GetBatchIDByType(int CardGradeID)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select BatchCardID,BatchCardCode from BatchCard ");
            sql.Append(" where CardGradeID=" + CardGradeID);
            sql.Append(" order by BatchCardID desc");
            return DbHelperSQL.Query(sql.ToString());
        }
        // Add by Alex 2014-06-09 ++
        public bool ExistBatchCode(string batchCardCode)
        {
            return dal.ExistBatchCode(batchCardCode);
        }
      
        // Add by Alex 2014-06-09 -- 
    }

}
