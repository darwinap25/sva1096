﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 优惠劵的店铺条件
	/// </summary>
	public partial class CouponTypeStoreCondition
	{
        /// <summary>
        /// 批量增加数据
        /// </summary>
        public int AddList(List<Edge.SVA.Model.CouponTypeStoreCondition> modelList)
        {
            if (modelList.Count == 0) return 99;
            return dal.AddList(modelList);
        }

        /// <summary>
        /// 批量更新数据
        /// </summary>
        public int UpdateList(List<Edge.SVA.Model.CouponTypeStoreCondition> insertList, List<Edge.SVA.Model.CouponTypeStoreCondition> deleteList)
        {
            return dal.UpdateList(insertList, deleteList);
        }

        /// <summary>
        /// 批量更新数据
        /// </summary>
        public int UpdateList(List<Edge.SVA.Model.CouponTypeStoreCondition> modeList,int id,int userID)
        {
            List<Edge.SVA.Model.CouponTypeStoreCondition> existStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();
            existStoreCheckedList = new Edge.SVA.BLL.CouponTypeStoreCondition().GetModelList("CouponTypeID=" + id);

            List<Edge.SVA.Model.CouponTypeStoreCondition> insertCouponTypeStoreConditionList = GetInsertCouponTypeStoreConditionList(modeList, existStoreCheckedList);
            List<Edge.SVA.Model.CouponTypeStoreCondition> deleteCouponTypeStoreConditionList = GetDeleteCouponTypeStoreConditionList(modeList, existStoreCheckedList);

            foreach (Edge.SVA.Model.CouponTypeStoreCondition newItem in insertCouponTypeStoreConditionList)
            {
                newItem.CreatedBy = userID;
                newItem.UpdatedBy = userID;
                newItem.CreatedOn = System.DateTime.Now;
                newItem.UpdatedOn = System.DateTime.Now;
            }

            if ((insertCouponTypeStoreConditionList.Count == 0) && (deleteCouponTypeStoreConditionList.Count == 0)) return 99;
            return dal.UpdateList(insertCouponTypeStoreConditionList, deleteCouponTypeStoreConditionList);
        }

        private List<Edge.SVA.Model.CouponTypeStoreCondition> GetInsertCouponTypeStoreConditionList(List<Edge.SVA.Model.CouponTypeStoreCondition> selectedStoreCheckedList, List<Edge.SVA.Model.CouponTypeStoreCondition> existedStoreCheckedList)
        {
            List<Edge.SVA.Model.CouponTypeStoreCondition> selectedNoDBStoreCheckedList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();
            foreach (Edge.SVA.Model.CouponTypeStoreCondition selecteditem in selectedStoreCheckedList)
            {
                bool exist = false;
                foreach (Edge.SVA.Model.CouponTypeStoreCondition dbItem in existedStoreCheckedList)
                {
                    if ((dbItem.CouponTypeID == selecteditem.CouponTypeID) && (dbItem.StoreConditionType == selecteditem.StoreConditionType) && (dbItem.ConditionType == selecteditem.ConditionType) && (dbItem.ConditionID == selecteditem.ConditionID))
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    selectedNoDBStoreCheckedList.Add(selecteditem);
                }
            }
            return selectedNoDBStoreCheckedList;
        }

        private List<Edge.SVA.Model.CouponTypeStoreCondition> GetDeleteCouponTypeStoreConditionList(List<Edge.SVA.Model.CouponTypeStoreCondition> selectedStoreCheckedList, List<Edge.SVA.Model.CouponTypeStoreCondition> existedStoreCheckedList)
        {
            List<Edge.SVA.Model.CouponTypeStoreCondition> noSelectedInDBCouponTypeStoreConditionList = new List<Edge.SVA.Model.CouponTypeStoreCondition>();

            foreach (Edge.SVA.Model.CouponTypeStoreCondition dbItem in existedStoreCheckedList)
            {
                bool exist = false;
                foreach (Edge.SVA.Model.CouponTypeStoreCondition selectedItem in selectedStoreCheckedList)
                {
                    if ((dbItem.CouponTypeID == selectedItem.CouponTypeID) && (dbItem.StoreConditionType == selectedItem.StoreConditionType) && (dbItem.ConditionType == selectedItem.ConditionType) && (dbItem.ConditionID == selectedItem.ConditionID))
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    noSelectedInDBCouponTypeStoreConditionList.Add(dbItem);
                }
            }
            return noSelectedInDBCouponTypeStoreConditionList;
        }

	}
}

