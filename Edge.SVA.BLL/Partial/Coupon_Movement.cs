﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Coupon_Movement
    {
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, fields);
        }

        public string GetTxnType(int oprID)
        {
            return dal.GetTxnType(oprID);
        }
    }
}
