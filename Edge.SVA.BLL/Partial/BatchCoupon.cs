﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class BatchCoupon
    {
        public Dictionary<int, string> GetBatchID(int top)
        {
            return dal.GetBatchID(top);

        }

        public Dictionary<int, string> GetBatchID(int top, string partialBatchID)
        {
            return dal.GetBatchID(top, partialBatchID);
        }

        public Dictionary<int, string> GetBatchID(int top, int couponTypeID)
        {
            return dal.GetBatchID(top, couponTypeID);
        }

        public Dictionary<int, string> GetBatchID(int top, string partialBatchID, int couponTypeID)
        {

            return dal.GetBatchID(top, partialBatchID, couponTypeID);
        }

        public bool ExistBatchCode(string batchCouponCode)
        {
            return dal.ExistBatchCode(batchCouponCode);
        }


        public DataSet GetBatchID()
        {
            return dal.GetBatchID();
        }

        public DataSet GetBatchIDByType(int couponTypeID)
        {
            return dal.GetBatchIDByType(couponTypeID);
        }
    }
}
