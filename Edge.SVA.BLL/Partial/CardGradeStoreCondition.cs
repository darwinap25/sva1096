﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 卡级别的店铺条件
	/// </summary>
	public partial class CardGradeStoreCondition
	{

        /// <summary>
        /// 批量增加数据
        /// </summary>
        public int AddList(List<Edge.SVA.Model.CardGradeStoreCondition> modelList)
        {
            if (modelList.Count == 0) return 99;
            return dal.AddList(modelList);
        }

        /// <summary>
        /// 批量更新数据
        /// </summary>
        public int UpdateList(List<Edge.SVA.Model.CardGradeStoreCondition> insertList, List<Edge.SVA.Model.CardGradeStoreCondition> deleteList)
        {
            return dal.UpdateList(insertList, deleteList);
        }

        /// <summary>
        /// 批量更新数据
        /// </summary>
        public int UpdateList(List<Edge.SVA.Model.CardGradeStoreCondition> modeList, int id, int userID)
        {
            List<Edge.SVA.Model.CardGradeStoreCondition> existStoreCheckedList = new List<Edge.SVA.Model.CardGradeStoreCondition>();
            existStoreCheckedList = new Edge.SVA.BLL.CardGradeStoreCondition().GetModelList("CardGradeID=" + id);

            List<Edge.SVA.Model.CardGradeStoreCondition> insertStoreConditionList = GetInsertStoreConditionList(modeList, existStoreCheckedList);
            List<Edge.SVA.Model.CardGradeStoreCondition> deleteStoreConditionList = GetDeleteStoreConditionList(modeList, existStoreCheckedList);

            foreach (Edge.SVA.Model.CardGradeStoreCondition newItem in insertStoreConditionList)
            {
                newItem.CreatedBy = userID;
                newItem.UpdatedBy = userID;
                newItem.CreatedOn = System.DateTime.Now;
                newItem.UpdatedOn = System.DateTime.Now;
            }

            if ((insertStoreConditionList.Count == 0) && (deleteStoreConditionList.Count == 0)) return 99;
            return dal.UpdateList(insertStoreConditionList, deleteStoreConditionList);
        }

        private List<Edge.SVA.Model.CardGradeStoreCondition> GetInsertStoreConditionList(List<Edge.SVA.Model.CardGradeStoreCondition> selectedStoreCheckedList, List<Edge.SVA.Model.CardGradeStoreCondition> existedStoreCheckedList)
        {
            List<Edge.SVA.Model.CardGradeStoreCondition> selectedNoDBStoreCheckedList = new List<Edge.SVA.Model.CardGradeStoreCondition>();
            foreach (Edge.SVA.Model.CardGradeStoreCondition selecteditem in selectedStoreCheckedList)
            {
                bool exist = false;
                foreach (Edge.SVA.Model.CardGradeStoreCondition dbItem in existedStoreCheckedList)
                {
                    if ((dbItem.CardGradeID == selecteditem.CardGradeID) && (dbItem.StoreConditionType == selecteditem.StoreConditionType) && (dbItem.ConditionType == selecteditem.ConditionType) && (dbItem.ConditionID == selecteditem.ConditionID))
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    selectedNoDBStoreCheckedList.Add(selecteditem);
                }
            }
            return selectedNoDBStoreCheckedList;
        }

        private List<Edge.SVA.Model.CardGradeStoreCondition> GetDeleteStoreConditionList(List<Edge.SVA.Model.CardGradeStoreCondition> selectedStoreCheckedList, List<Edge.SVA.Model.CardGradeStoreCondition> existedStoreCheckedList)
        {
            List<Edge.SVA.Model.CardGradeStoreCondition> noSelectedInDBCouponTypeStoreConditionList = new List<Edge.SVA.Model.CardGradeStoreCondition>();

            foreach (Edge.SVA.Model.CardGradeStoreCondition dbItem in existedStoreCheckedList)
            {
                bool exist = false;
                foreach (Edge.SVA.Model.CardGradeStoreCondition selectedItem in selectedStoreCheckedList)
                {
                    if ((dbItem.CardGradeID == selectedItem.CardGradeID) && (dbItem.StoreConditionType == selectedItem.StoreConditionType) && (dbItem.ConditionType == selectedItem.ConditionType) && (dbItem.ConditionID == selectedItem.ConditionID))
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    noSelectedInDBCouponTypeStoreConditionList.Add(dbItem);
                }
            }
            return noSelectedInDBCouponTypeStoreConditionList;
        }
	}
}

