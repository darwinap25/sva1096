﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Member
    {
        public List<int> GetMembers(string mobileNumber)
        {
            return dal.GetMembers(mobileNumber);
        }
        //add by Alex 2014-07-28++
        public DataSet GetMembersID(string memberMobilePhone, string memberEmail)
        {
            return dal.GetMembersID(memberMobilePhone, memberEmail);
        }
        //add by Alex 2014-07-28--
    }
}
