﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL
{
    public partial class Promotion_Hit_PLU
    {
        public void DeleteData(string PromotionCode, int hitSeq)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Promotion_Hit_PLU ");
            strSql.Append(" where PromotionCode=@PromotionCode");
            strSql.Append(" and HitSeq=@HitSeq");
            SqlParameter[] parameters = {
					new SqlParameter("@PromotionCode", SqlDbType.VarChar,64),
					new SqlParameter("@HitSeq", SqlDbType.Int)};
            parameters[0].Value = PromotionCode;
            parameters[1].Value = hitSeq;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }
    }
}
