﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
    public partial class Ord_CardAdjust_D
    {
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetListWithCard(string strWhere)
        {
            return dal.GetListWithCard(strWhere);
        }


        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithCard(string strWhere)
        {
            return dal.GetCountWithCard(strWhere);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetPageListWithCard(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            return dal.GetPageListWithCard(pageSize, currentPage, strWhere, filedOrder);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithCard_Movement(string strWhere)
        {
            return dal.GetCountWithCard_Movement(strWhere);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetPageListWithCard_Movement(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            return dal.GetPageListWithCard_Movement(pageSize, currentPage, strWhere, filedOrder);

        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetPageListWithCard_Movement1(int pageSize, int currentPage, string strWhere, string filedOrder)
        {
            return dal.GetPageListWithCard_Movement1(pageSize, currentPage, strWhere, filedOrder);

        }


        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public int GetCountWithCard_Movement1(string strWhere)
        {
            return dal.GetCountWithCard_Movement1(strWhere);
        }


        public decimal GetAllDenominationWithCard(string strWhere)
        {

            return dal.GetAllDenominationWithCard(strWhere);
        }

        public decimal GetAllDenominationWithOrd_CardAdjust_D(string strWhere)
        {
            return dal.GetAllDenominationWithOrd_CardAdjust_D(strWhere);
        }

        public decimal GetAllDenominationWithCard_Movement(string strWhere)
        {
            return dal.GetAllDenominationWithCard_Movement(strWhere);
        }

        public void GetAllDenominationWithCard_Movement(string strWhere, out decimal openBal, out decimal amount, out decimal closeBal)
        {
            dal.GetAllDenominationWithCard_Movement(strWhere, out openBal, out amount, out closeBal);
        }
    }
}
