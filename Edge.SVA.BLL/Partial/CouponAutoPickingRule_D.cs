﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class CouponAutoPickingRule_D : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateCouponAutoPickingRule_D() as IDAL.IBaseDAL; }
        }

        public bool DeleteByCode(string couponAutoPickingRuleCode)
        {
            return dal.DeleteByCode(couponAutoPickingRuleCode);
        }
    }
}
