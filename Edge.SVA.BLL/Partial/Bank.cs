﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Edge.SVA.BLL
{
   public partial class Bank
    {
        /// <summary>
        /// 根据Bank判断是否存在此Bank,存在则返回BankID
        /// </summary>
        public int ExistsBank(string BankName)
        {
            if (!string.IsNullOrEmpty(BankName))
            {
                Bank bll = new BLL.Bank();
                DataTable dt = bll.GetList("BankName1='" + BankName + "'").Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0]["BankID"]);
                }
            }
            return 0;
        }
    }
}
