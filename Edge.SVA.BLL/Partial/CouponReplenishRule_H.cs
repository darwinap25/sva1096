﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Edge.SVA.BLL
{
    public partial class CouponReplenishRule_H : BaseBLL
    {
        protected override IDAL.IBaseDAL BaseDAL
        {
            get { return DALFactory.DataAccess.CreateCouponReplenishRule_H() as IDAL.IBaseDAL; }
        }
    }
}
