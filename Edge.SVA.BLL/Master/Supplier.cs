﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// 供货商
    /// </summary>
    public partial class Supplier
    {
        private readonly ISupplier dal = DataAccess.CreateSupplier();
        public Supplier()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(string SupplierCode)
		{
            return dal.Exists(SupplierCode);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public int Add(Edge.SVA.Model.Supplier model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.Supplier model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(int SupplierID)
		{

            return dal.Delete(SupplierID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(string SupplierCode)
		{

            return dal.Delete(SupplierCode);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool DeleteList(string SupplierIDlist)
		{
            return dal.DeleteList(SupplierIDlist);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.Supplier GetModel(int SupplierID)
		{

            return dal.GetModel(SupplierID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        public Edge.SVA.Model.Supplier GetModelByCache(int SupplierID)
		{

            string CacheKey = "SupplierModel-" + SupplierID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
                    objModel = dal.GetModel(SupplierID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
            return (Edge.SVA.Model.Supplier)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.Supplier> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.Supplier> DataTableToList(DataTable dt)
		{
            List<Edge.SVA.Model.Supplier> modelList = new List<Edge.SVA.Model.Supplier>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
                Edge.SVA.Model.Supplier model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = new Edge.SVA.Model.Supplier();
                    if (dt.Rows[n]["SupplierID"] != null && dt.Rows[n]["SupplierID"].ToString() != "")
					{
                        model.SupplierID = int.Parse(dt.Rows[n]["SupplierID"].ToString());
					}
                    if (dt.Rows[n]["SupplierCode"] != null && dt.Rows[n]["SupplierCode"].ToString() != "")
					{
                        model.SupplierCode = dt.Rows[n]["SupplierCode"].ToString();
					}
                    if (dt.Rows[n]["SupplierDesc1"] != null && dt.Rows[n]["SupplierDesc1"].ToString() != "")
					{
                        model.SupplierDesc1 = dt.Rows[n]["SupplierDesc1"].ToString();
					}
                    if (dt.Rows[n]["SupplierDesc2"] != null && dt.Rows[n]["SupplierDesc2"].ToString() != "")
					{
                        model.SupplierDesc2 = dt.Rows[n]["SupplierDesc2"].ToString();
					}
                    if (dt.Rows[n]["SupplierDesc3"] != null && dt.Rows[n]["SupplierDesc3"].ToString() != "")
					{
                        model.SupplierDesc3 = dt.Rows[n]["SupplierDesc3"].ToString();
					}
                    if (dt.Rows[n]["SupplierAddress"] != null && dt.Rows[n]["SupplierAddress"].ToString() != "")
                    {
                        model.SupplierAddress = dt.Rows[n]["SupplierAddress"].ToString();
                    }
                    if (dt.Rows[n]["OtherAddress1"] != null && dt.Rows[n]["OtherAddress1"].ToString() != "")
                    {
                        model.OtherAddress1 = dt.Rows[n]["OtherAddress1"].ToString();
                    }
                    if (dt.Rows[n]["OtherAddress2"] != null && dt.Rows[n]["OtherAddress2"].ToString() != "")
                    {
                        model.OtherAddress2 = dt.Rows[n]["OtherAddress2"].ToString();
                    }
                    if (dt.Rows[n]["SupplierEmail"] != null && dt.Rows[n]["SupplierEmail"].ToString() != "")
                    {
                        model.SupplierEmail = dt.Rows[n]["SupplierEmail"].ToString();
                    }
                    if (dt.Rows[n]["Contact"] != null && dt.Rows[n]["Contact"].ToString() != "")
                    {
                        model.Contact = dt.Rows[n]["Contact"].ToString();
                    }
                    if (dt.Rows[n]["ContactPhone"] != null && dt.Rows[n]["ContactPhone"].ToString() != "")
                    {
                        model.ContactPhone = dt.Rows[n]["ContactPhone"].ToString();
                    }
                    if (dt.Rows[n]["ContactEmail"] != null && dt.Rows[n]["ContactEmail"].ToString() != "")
                    {
                        model.ContactEmail = dt.Rows[n]["ContactEmail"].ToString();
                    }
                    if (dt.Rows[n]["ContactMobile"] != null && dt.Rows[n]["ContactMobile"].ToString() != "")
                    {
                        model.ContactMobile = dt.Rows[n]["ContactMobile"].ToString();
                    }
                    if (dt.Rows[n]["Remark"] != null && dt.Rows[n]["Remark"].ToString() != "")
                    {
                        model.Remark = dt.Rows[n]["Remark"].ToString();
                    }
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
    }
}
