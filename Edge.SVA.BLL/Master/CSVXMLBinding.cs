﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 实体卡绑定表
	/// </summary>
	public partial class CSVXMLBinding
	{
		private readonly ICSVXMLBinding dal=DataAccess.CreateCSVXMLBinding();
        public CSVXMLBinding()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(Edge.SVA.Model.CSVXMLBinding model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Edge.SVA.Model.CSVXMLBinding model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete(code);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public Edge.SVA.Model.CSVXMLBinding GetModel(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.GetModel(code);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        public Edge.SVA.Model.CSVXMLBinding GetModelByCache(string code)
		{
			//该表无主键信息，请自定义主键/条件字段
            string CacheKey = "CSVXMLBindingModel-";
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(code);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
            return (Edge.SVA.Model.CSVXMLBinding)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.CSVXMLBinding> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public List<Edge.SVA.Model.CSVXMLBinding> DataTableToList(DataTable dt)
		{
            List<Edge.SVA.Model.CSVXMLBinding> modelList = new List<Edge.SVA.Model.CSVXMLBinding>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
                Edge.SVA.Model.CSVXMLBinding model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = new Edge.SVA.Model.CSVXMLBinding();
                    if (dt.Rows[n]["BindingCode"] != null && dt.Rows[n]["BindingCode"].ToString() != "")
					{
                        model.BindingCode = dt.Rows[n]["BindingCode"].ToString();
					}
                    if (dt.Rows[n]["Description"] != null && dt.Rows[n]["Description"].ToString() != "")
					{
                        model.Description = dt.Rows[n]["Description"].ToString();
					}
                    if (dt.Rows[n]["Func"] != null && dt.Rows[n]["Func"].ToString() != "")
					{
                        model.Func = dt.Rows[n]["Func"].ToString();
					}
                    if (dt.Rows[n]["CSVColumnName"] != null && dt.Rows[n]["CSVColumnName"].ToString() != "")
					{
                        model.CSVColumnName = dt.Rows[n]["CSVColumnName"].ToString();
					}
                    if (dt.Rows[n]["XMLFieldName"] != null && dt.Rows[n]["XMLFieldName"].ToString() != "")
					{
                        model.XMLFieldName = dt.Rows[n]["XMLFieldName"].ToString();
					}
                    if (dt.Rows[n]["DataType"] != null && dt.Rows[n]["DataType"].ToString() != "")
					{
                        model.DataType = int.Parse(dt.Rows[n]["DataType"].ToString());
					}
                    if (dt.Rows[n]["FixedValue"] != null && dt.Rows[n]["FixedValue"].ToString() != "")
					{
                        model.FixedValue = dt.Rows[n]["FixedValue"].ToString();
					}
                    if (dt.Rows[n]["Length"] != null && dt.Rows[n]["Length"].ToString() != "")
					{
                        model.Length = int.Parse(dt.Rows[n]["Length"].ToString());
					}
                    if (dt.Rows[n]["SaveToDB"] != null && dt.Rows[n]["SaveToDB"].ToString() != "")
					{
                        model.SaveToDB = int.Parse(dt.Rows[n]["SaveToDB"].ToString());
					}
                    if (dt.Rows[n]["CreatedOn"] != null && dt.Rows[n]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[n]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedOn"] != null && dt.Rows[n]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedBy"] != null && dt.Rows[n]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

