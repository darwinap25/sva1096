﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// 系统用户消息通知设定
    /// </summary>
    public partial class UserMessageSetting_H
    {
        private readonly IUserMessageSetting_H dal = DataAccess.CreateUserMessageSetting_H();
        public UserMessageSetting_H()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string UserMessageCode)
        {
            return dal.Exists(UserMessageCode);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.UserMessageSetting_H model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.UserMessageSetting_H model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string UserMessageCode)
        {

            return dal.Delete(UserMessageCode);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string UserMessageCodelist)
        {
            return dal.DeleteList(UserMessageCodelist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.UserMessageSetting_H GetModel(string UserMessageCode)
        {

            return dal.GetModel(UserMessageCode);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public Edge.SVA.Model.UserMessageSetting_H GetModelByCache(string UserMessageCode)
        {

            string CacheKey = "UserMessageSetting_HModel-" + UserMessageCode;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(UserMessageCode);
                    if (objModel != null)
                    {
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (Edge.SVA.Model.UserMessageSetting_H)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.UserMessageSetting_H> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.UserMessageSetting_H> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.UserMessageSetting_H> modelList = new List<Edge.SVA.Model.UserMessageSetting_H>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.UserMessageSetting_H model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.UserMessageSetting_H();
                    if (dt.Rows[n]["UserMessageCode"] != null && dt.Rows[n]["UserMessageCode"].ToString() != "")
                    {
                        model.UserMessageCode = dt.Rows[n]["UserMessageCode"].ToString();
                    }
                    if (dt.Rows[n]["UserMessageDesc"] != null && dt.Rows[n]["UserMessageDesc"].ToString() != "")
                    {
                        model.UserMessageDesc = dt.Rows[n]["UserMessageDesc"].ToString();
                    }
                    if (dt.Rows[n]["StartDate"] != null && dt.Rows[n]["StartDate"].ToString() != "")
                    {
                        model.StartDate = DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
                    }
                    if (dt.Rows[n]["EndDate"] != null && dt.Rows[n]["EndDate"].ToString() != "")
                    {
                        model.EndDate = DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = int.Parse(dt.Rows[n]["Status"].ToString());
                    }
                    if (dt.Rows[n]["SendUserID"] != null && dt.Rows[n]["SendUserID"].ToString() != "")
                    {
                        model.SendUserID = int.Parse(dt.Rows[n]["SendUserID"].ToString());
                    }
                    if (dt.Rows[n]["UserMessageType"] != null && dt.Rows[n]["UserMessageType"].ToString() != "")
                    {
                        model.UserMessageType = int.Parse(dt.Rows[n]["UserMessageType"].ToString());
                    }
                    if (dt.Rows[n]["UserMessageTitle"] != null && dt.Rows[n]["UserMessageTitle"].ToString() != "")
                    {
                        model.UserMessageTitle = dt.Rows[n]["UserMessageTitle"].ToString();
                    }
                    if (dt.Rows[n]["UserMessageContent"] != null && dt.Rows[n]["UserMessageContent"].ToString() != "")
                    {
                        model.UserMessageContent = dt.Rows[n]["UserMessageContent"].ToString();
                    }
                    if (dt.Rows[n]["TriggeType"] != null && dt.Rows[n]["TriggeType"].ToString() != "")
                    {
                        model.TriggeType = int.Parse(dt.Rows[n]["TriggeType"].ToString());
                    }
                    if (dt.Rows[n]["CreatedOn"] != null && dt.Rows[n]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[n]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedOn"] != null && dt.Rows[n]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedBy"] != null && dt.Rows[n]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}

