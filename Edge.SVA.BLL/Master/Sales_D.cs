﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// ?úê?μ￥×ó±í
	/// </summary>
	public partial class Sales_D
	{
		private readonly ISales_D dal=DataAccess.CreateSales_D();
		public Sales_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			return dal.Exists(KeyID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Sales_D model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Sales_D model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			return dal.Delete(KeyID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			return dal.DeleteList(KeyIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Sales_D GetModel(int KeyID)
		{
			
			return dal.GetModel(KeyID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Sales_D GetModelByCache(int KeyID)
		{
			
			string CacheKey = "Sales_DModel-" + KeyID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(KeyID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Sales_D)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Sales_D> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Sales_D> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Sales_D> modelList = new List<Edge.SVA.Model.Sales_D>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Sales_D model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Sales_D();
					if(dt.Rows[n]["KeyID"]!=null && dt.Rows[n]["KeyID"].ToString()!="")
					{
						model.KeyID=int.Parse(dt.Rows[n]["KeyID"].ToString());
					}
					if(dt.Rows[n]["TxnNo"]!=null && dt.Rows[n]["TxnNo"].ToString()!="")
					{
					model.TxnNo=dt.Rows[n]["TxnNo"].ToString();
					}
					if(dt.Rows[n]["SeqNo"]!=null && dt.Rows[n]["SeqNo"].ToString()!="")
					{
						model.SeqNo=int.Parse(dt.Rows[n]["SeqNo"].ToString());
					}
					if(dt.Rows[n]["ProdCode"]!=null && dt.Rows[n]["ProdCode"].ToString()!="")
					{
					model.ProdCode=dt.Rows[n]["ProdCode"].ToString();
					}
					if(dt.Rows[n]["ProdDesc"]!=null && dt.Rows[n]["ProdDesc"].ToString()!="")
					{
					model.ProdDesc=dt.Rows[n]["ProdDesc"].ToString();
					}
					if(dt.Rows[n]["Serialno"]!=null && dt.Rows[n]["Serialno"].ToString()!="")
					{
					model.Serialno=dt.Rows[n]["Serialno"].ToString();
					}
					if(dt.Rows[n]["Collected"]!=null && dt.Rows[n]["Collected"].ToString()!="")
					{
						model.Collected=int.Parse(dt.Rows[n]["Collected"].ToString());
					}
					if(dt.Rows[n]["RetailPrice"]!=null && dt.Rows[n]["RetailPrice"].ToString()!="")
					{
						model.RetailPrice=decimal.Parse(dt.Rows[n]["RetailPrice"].ToString());
					}
					if(dt.Rows[n]["NetPrice"]!=null && dt.Rows[n]["NetPrice"].ToString()!="")
					{
						model.NetPrice=decimal.Parse(dt.Rows[n]["NetPrice"].ToString());
					}
					if(dt.Rows[n]["Qty"]!=null && dt.Rows[n]["Qty"].ToString()!="")
					{
						model.Qty=int.Parse(dt.Rows[n]["Qty"].ToString());
					}
					if(dt.Rows[n]["NetAmount"]!=null && dt.Rows[n]["NetAmount"].ToString()!="")
					{
						model.NetAmount=decimal.Parse(dt.Rows[n]["NetAmount"].ToString());
					}
					if(dt.Rows[n]["Additional"]!=null && dt.Rows[n]["Additional"].ToString()!="")
					{
					model.Additional=dt.Rows[n]["Additional"].ToString();
					}
					if(dt.Rows[n]["POPrice"]!=null && dt.Rows[n]["POPrice"].ToString()!="")
					{
						model.POPrice=decimal.Parse(dt.Rows[n]["POPrice"].ToString());
					}
					if(dt.Rows[n]["POReasonID"]!=null && dt.Rows[n]["POReasonID"].ToString()!="")
					{
						model.POReasonID=int.Parse(dt.Rows[n]["POReasonID"].ToString());
					}
					if(dt.Rows[n]["DiscountType"]!=null && dt.Rows[n]["DiscountType"].ToString()!="")
					{
						model.DiscountType=int.Parse(dt.Rows[n]["DiscountType"].ToString());
					}
					if(dt.Rows[n]["DiscountOffValue"]!=null && dt.Rows[n]["DiscountOffValue"].ToString()!="")
					{
						model.DiscountOffValue=decimal.Parse(dt.Rows[n]["DiscountOffValue"].ToString());
					}
					if(dt.Rows[n]["DiscountOffPrice"]!=null && dt.Rows[n]["DiscountOffPrice"].ToString()!="")
					{
						model.DiscountOffPrice=decimal.Parse(dt.Rows[n]["DiscountOffPrice"].ToString());
					}
					if(dt.Rows[n]["DiscountReasonID"]!=null && dt.Rows[n]["DiscountReasonID"].ToString()!="")
					{
						model.DiscountReasonID=int.Parse(dt.Rows[n]["DiscountReasonID"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
					model.CreatedBy=dt.Rows[n]["CreatedBy"].ToString();
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
					model.UpdatedBy=dt.Rows[n]["UpdatedBy"].ToString();
					}
					if(dt.Rows[n]["ReservedDate"]!=null && dt.Rows[n]["ReservedDate"].ToString()!="")
					{
						model.ReservedDate=DateTime.Parse(dt.Rows[n]["ReservedDate"].ToString());
					}
					if(dt.Rows[n]["PickupDate"]!=null && dt.Rows[n]["PickupDate"].ToString()!="")
					{
						model.PickupDate=DateTime.Parse(dt.Rows[n]["PickupDate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

