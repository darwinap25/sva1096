﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 赞助商
	/// </summary>
	public partial class Sponsor
	{
		private readonly ISponsor dal=DataAccess.CreateSponsor();
		public Sponsor()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string SponsorCode)
		{
			return dal.Exists(SponsorCode);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Sponsor model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Sponsor model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SponsorID)
		{
			
			return dal.Delete(SponsorID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string SponsorCode)
		{
			
			return dal.Delete(SponsorCode);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string SponsorIDlist )
		{
			return dal.DeleteList(SponsorIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Sponsor GetModel(int SponsorID)
		{
			
			return dal.GetModel(SponsorID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Sponsor GetModelByCache(int SponsorID)
		{
			
			string CacheKey = "SponsorModel-" + SponsorID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(SponsorID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Sponsor)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Sponsor> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Sponsor> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Sponsor> modelList = new List<Edge.SVA.Model.Sponsor>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Sponsor model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Sponsor();
					if(dt.Rows[n]["SponsorID"]!=null && dt.Rows[n]["SponsorID"].ToString()!="")
					{
						model.SponsorID=int.Parse(dt.Rows[n]["SponsorID"].ToString());
					}
					if(dt.Rows[n]["SponsorCode"]!=null && dt.Rows[n]["SponsorCode"].ToString()!="")
					{
					model.SponsorCode=dt.Rows[n]["SponsorCode"].ToString();
					}
					if(dt.Rows[n]["SponsorName1"]!=null && dt.Rows[n]["SponsorName1"].ToString()!="")
					{
					model.SponsorName1=dt.Rows[n]["SponsorName1"].ToString();
					}
					if(dt.Rows[n]["SponsorName2"]!=null && dt.Rows[n]["SponsorName2"].ToString()!="")
					{
					model.SponsorName2=dt.Rows[n]["SponsorName2"].ToString();
					}
					if(dt.Rows[n]["SponsorName3"]!=null && dt.Rows[n]["SponsorName3"].ToString()!="")
					{
					model.SponsorName3=dt.Rows[n]["SponsorName3"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

