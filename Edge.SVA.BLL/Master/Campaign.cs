﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 活动表
	/// </summary>
	public partial class Campaign
	{
		private readonly ICampaign dal=DataAccess.CreateCampaign();
		public Campaign()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CampaignCode,int CampaignID)
		{
			return dal.Exists(CampaignCode,CampaignID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Campaign model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Campaign model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CampaignID)
		{
			
			return dal.Delete(CampaignID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CampaignCode,int CampaignID)
		{
			
			return dal.Delete(CampaignCode,CampaignID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CampaignIDlist )
		{
			return dal.DeleteList(CampaignIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Campaign GetModel(int CampaignID)
		{
			
			return dal.GetModel(CampaignID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Campaign GetModelByCache(int CampaignID)
		{
			
			string CacheKey = "CampaignModel-" + CampaignID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CampaignID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Campaign)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere);
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere);
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Campaign> GetModelList(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere);
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Campaign> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Campaign> modelList = new List<Edge.SVA.Model.Campaign>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Campaign model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Campaign();
					if(dt.Rows[n]["CampaignID"]!=null && dt.Rows[n]["CampaignID"].ToString()!="")
					{
						model.CampaignID=int.Parse(dt.Rows[n]["CampaignID"].ToString());
					}
					if(dt.Rows[n]["CampaignCode"]!=null && dt.Rows[n]["CampaignCode"].ToString()!="")
					{
					model.CampaignCode=dt.Rows[n]["CampaignCode"].ToString();
					}
					if(dt.Rows[n]["CampaignName1"]!=null && dt.Rows[n]["CampaignName1"].ToString()!="")
					{
					model.CampaignName1=dt.Rows[n]["CampaignName1"].ToString();
					}
					if(dt.Rows[n]["CampaignName2"]!=null && dt.Rows[n]["CampaignName2"].ToString()!="")
					{
					model.CampaignName2=dt.Rows[n]["CampaignName2"].ToString();
					}
					if(dt.Rows[n]["CampaignName3"]!=null && dt.Rows[n]["CampaignName3"].ToString()!="")
					{
					model.CampaignName3=dt.Rows[n]["CampaignName3"].ToString();
					}
					if(dt.Rows[n]["CampaignDetail1"]!=null && dt.Rows[n]["CampaignDetail1"].ToString()!="")
					{
					model.CampaignDetail1=dt.Rows[n]["CampaignDetail1"].ToString();
					}
					if(dt.Rows[n]["CampaignDetail3"]!=null && dt.Rows[n]["CampaignDetail3"].ToString()!="")
					{
					model.CampaignDetail3=dt.Rows[n]["CampaignDetail3"].ToString();
					}
					if(dt.Rows[n]["CampaignDetail2"]!=null && dt.Rows[n]["CampaignDetail2"].ToString()!="")
					{
					model.CampaignDetail2=dt.Rows[n]["CampaignDetail2"].ToString();
					}
					if(dt.Rows[n]["CampaignType"]!=null && dt.Rows[n]["CampaignType"].ToString()!="")
					{
						model.CampaignType=int.Parse(dt.Rows[n]["CampaignType"].ToString());
					}
					if(dt.Rows[n]["CampaignPicFile"]!=null && dt.Rows[n]["CampaignPicFile"].ToString()!="")
					{
					model.CampaignPicFile=dt.Rows[n]["CampaignPicFile"].ToString();
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
						model.Status=int.Parse(dt.Rows[n]["Status"].ToString());
					}
					if(dt.Rows[n]["BrandID"]!=null && dt.Rows[n]["BrandID"].ToString()!="")
					{
						model.BrandID=int.Parse(dt.Rows[n]["BrandID"].ToString());
					}
					if(dt.Rows[n]["StartDate"]!=null && dt.Rows[n]["StartDate"].ToString()!="")
					{
						model.StartDate=DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
					}
					if(dt.Rows[n]["EndDate"]!=null && dt.Rows[n]["EndDate"].ToString()!="")
					{
						model.EndDate=DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere);
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere);
            return dal.GetCount(strWhere);
        }
		#endregion  Method
	}
}

