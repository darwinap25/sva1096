﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 卡有效期延长规则
	///   增值规则阶梯设置：
	///   增值满     延长时间
	///   100           90 天
	///   200           180天
	///   500           360天
	///   匹配时按照金额排序
	/// </summary>
	public partial class CardExtensionRule
	{
		private readonly ICardExtensionRule dal=DataAccess.CreateCardExtensionRule();
		public CardExtensionRule()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ExtensionRuleID)
		{
			return dal.Exists(ExtensionRuleID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CardExtensionRule model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardExtensionRule model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ExtensionRuleID)
		{
			
			return dal.Delete(ExtensionRuleID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string ExtensionRuleIDlist )
		{
			return dal.DeleteList(ExtensionRuleIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardExtensionRule GetModel(int ExtensionRuleID)
		{
			
			return dal.GetModel(ExtensionRuleID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CardExtensionRule GetModelByCache(int ExtensionRuleID)
		{
			
			string CacheKey = "CardExtensionRuleModel-" + ExtensionRuleID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ExtensionRuleID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CardExtensionRule)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardExtensionRule> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardExtensionRule> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CardExtensionRule> modelList = new List<Edge.SVA.Model.CardExtensionRule>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CardExtensionRule model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CardExtensionRule();
					if(dt.Rows[n]["ExtensionRuleID"]!=null && dt.Rows[n]["ExtensionRuleID"].ToString()!="")
					{
						model.ExtensionRuleID=int.Parse(dt.Rows[n]["ExtensionRuleID"].ToString());
					}
					if(dt.Rows[n]["RuleType"]!=null && dt.Rows[n]["RuleType"].ToString()!="")
					{
						model.RuleType=int.Parse(dt.Rows[n]["RuleType"].ToString());
					}
					if(dt.Rows[n]["CardTypeID"]!=null && dt.Rows[n]["CardTypeID"].ToString()!="")
					{
						model.CardTypeID=int.Parse(dt.Rows[n]["CardTypeID"].ToString());
					}
					if(dt.Rows[n]["CardGradeID"]!=null && dt.Rows[n]["CardGradeID"].ToString()!="")
					{
						model.CardGradeID=int.Parse(dt.Rows[n]["CardGradeID"].ToString());
					}
					if(dt.Rows[n]["ExtensionRuleSeqNo"]!=null && dt.Rows[n]["ExtensionRuleSeqNo"].ToString()!="")
					{
						model.ExtensionRuleSeqNo=int.Parse(dt.Rows[n]["ExtensionRuleSeqNo"].ToString());
					}
					if(dt.Rows[n]["MaxLimit"]!=null && dt.Rows[n]["MaxLimit"].ToString()!="")
					{
						model.MaxLimit=int.Parse(dt.Rows[n]["MaxLimit"].ToString());
					}
					if(dt.Rows[n]["RuleAmount"]!=null && dt.Rows[n]["RuleAmount"].ToString()!="")
					{
						model.RuleAmount=decimal.Parse(dt.Rows[n]["RuleAmount"].ToString());
					}
					if(dt.Rows[n]["Extension"]!=null && dt.Rows[n]["Extension"].ToString()!="")
					{
						model.Extension=int.Parse(dt.Rows[n]["Extension"].ToString());
					}
					if(dt.Rows[n]["ExtensionUnit"]!=null && dt.Rows[n]["ExtensionUnit"].ToString()!="")
					{
						model.ExtensionUnit=int.Parse(dt.Rows[n]["ExtensionUnit"].ToString());
					}
					if(dt.Rows[n]["ExtendType"]!=null && dt.Rows[n]["ExtendType"].ToString()!="")
					{
						model.ExtendType=int.Parse(dt.Rows[n]["ExtendType"].ToString());
					}
					if(dt.Rows[n]["StartDate"]!=null && dt.Rows[n]["StartDate"].ToString()!="")
					{
						model.StartDate=DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
					}
					if(dt.Rows[n]["EndDate"]!=null && dt.Rows[n]["EndDate"].ToString()!="")
					{
						model.EndDate=DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
						model.Status=int.Parse(dt.Rows[n]["Status"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

