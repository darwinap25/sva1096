﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 优惠劵大类表。
	///1
	/// </summary>
	public partial class CouponTypeNature
	{
		private readonly ICouponTypeNature dal=DataAccess.CreateCouponTypeNature();
		public CouponTypeNature()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CouponTypeNatureID)
		{
			return dal.Exists(CouponTypeNatureID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.CouponTypeNature model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CouponTypeNature model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CouponTypeNatureID)
		{
			
			return dal.Delete(CouponTypeNatureID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CouponTypeNatureIDlist )
		{
			return dal.DeleteList(CouponTypeNatureIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CouponTypeNature GetModel(int CouponTypeNatureID)
		{
			
			return dal.GetModel(CouponTypeNatureID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CouponTypeNature GetModelByCache(int CouponTypeNatureID)
		{
			
			string CacheKey = "CouponTypeNatureModel-" + CouponTypeNatureID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CouponTypeNatureID);
					if (objModel != null)
					{
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CouponTypeNature)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponTypeNature> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CouponTypeNature> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CouponTypeNature> modelList = new List<Edge.SVA.Model.CouponTypeNature>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CouponTypeNature model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CouponTypeNature();
					if(dt.Rows[n]["CouponTypeNatureID"]!=null && dt.Rows[n]["CouponTypeNatureID"].ToString()!="")
					{
						model.CouponTypeNatureID=int.Parse(dt.Rows[n]["CouponTypeNatureID"].ToString());
					}
					if(dt.Rows[n]["CouponTypeNatureName1"]!=null && dt.Rows[n]["CouponTypeNatureName1"].ToString()!="")
					{
					model.CouponTypeNatureName1=dt.Rows[n]["CouponTypeNatureName1"].ToString();
					}
					if(dt.Rows[n]["CouponTypeNatureName2"]!=null && dt.Rows[n]["CouponTypeNatureName2"].ToString()!="")
					{
					model.CouponTypeNatureName2=dt.Rows[n]["CouponTypeNatureName2"].ToString();
					}
					if(dt.Rows[n]["CouponTypeNatureName3"]!=null && dt.Rows[n]["CouponTypeNatureName3"].ToString()!="")
					{
					model.CouponTypeNatureName3=dt.Rows[n]["CouponTypeNatureName3"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }


		#endregion  Method
	}
}

