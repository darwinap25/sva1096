﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// Address
	/// </summary>
	public partial class Address
	{
		private readonly IAddress dal=DataAccess.CreateAddress();
		public Address()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string AddressID)
		{
			return dal.Exists(AddressID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Address model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Address model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string AddressID)
		{
			
			return dal.Delete(AddressID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string AddressIDlist )
		{
			return dal.DeleteList(AddressIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Address GetModel(string AddressID)
		{
			
			return dal.GetModel(AddressID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Address GetModelByCache(string AddressID)
		{
			
			string CacheKey = "AddressModel-" + AddressID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(AddressID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Address)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Address> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Address> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Address> modelList = new List<Edge.SVA.Model.Address>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Address model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Address();
					if(dt.Rows[n]["AddressID"]!=null && dt.Rows[n]["AddressID"].ToString()!="")
					{
					model.AddressID=dt.Rows[n]["AddressID"].ToString();
					}
					if(dt.Rows[n]["AddressUnit"]!=null && dt.Rows[n]["AddressUnit"].ToString()!="")
					{
					model.AddressUnit=dt.Rows[n]["AddressUnit"].ToString();
					}
					if(dt.Rows[n]["AddressFloor"]!=null && dt.Rows[n]["AddressFloor"].ToString()!="")
					{
						model.AddressFloor=int.Parse(dt.Rows[n]["AddressFloor"].ToString());
					}
					if(dt.Rows[n]["AddressBlock"]!=null && dt.Rows[n]["AddressBlock"].ToString()!="")
					{
					model.AddressBlock=dt.Rows[n]["AddressBlock"].ToString();
					}
					if(dt.Rows[n]["AddressBuilding"]!=null && dt.Rows[n]["AddressBuilding"].ToString()!="")
					{
					model.AddressBuilding=dt.Rows[n]["AddressBuilding"].ToString();
					}
					if(dt.Rows[n]["AddressStreetNo"]!=null && dt.Rows[n]["AddressStreetNo"].ToString()!="")
					{
					model.AddressStreetNo=dt.Rows[n]["AddressStreetNo"].ToString();
					}
					if(dt.Rows[n]["AddressStreet"]!=null && dt.Rows[n]["AddressStreet"].ToString()!="")
					{
					model.AddressStreet=dt.Rows[n]["AddressStreet"].ToString();
					}
					if(dt.Rows[n]["AddressDistrict"]!=null && dt.Rows[n]["AddressDistrict"].ToString()!="")
					{
					model.AddressDistrict=dt.Rows[n]["AddressDistrict"].ToString();
					}
					if(dt.Rows[n]["AddressCity"]!=null && dt.Rows[n]["AddressCity"].ToString()!="")
					{
					model.AddressCity=dt.Rows[n]["AddressCity"].ToString();
					}
					if(dt.Rows[n]["AddressProvince"]!=null && dt.Rows[n]["AddressProvince"].ToString()!="")
					{
					model.AddressProvince=dt.Rows[n]["AddressProvince"].ToString();
					}
					if(dt.Rows[n]["AddressCountry"]!=null && dt.Rows[n]["AddressCountry"].ToString()!="")
					{
					model.AddressCountry=dt.Rows[n]["AddressCountry"].ToString();
					}
					if(dt.Rows[n]["AddressZipCode"]!=null && dt.Rows[n]["AddressZipCode"].ToString()!="")
					{
					model.AddressZipCode=dt.Rows[n]["AddressZipCode"].ToString();
					}
					if(dt.Rows[n]["AddressLanguage"]!=null && dt.Rows[n]["AddressLanguage"].ToString()!="")
					{
						model.AddressLanguage=int.Parse(dt.Rows[n]["AddressLanguage"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
					model.UpdatedBy=dt.Rows[n]["UpdatedBy"].ToString();
					}
					if(dt.Rows[n]["Addr1"]!=null && dt.Rows[n]["Addr1"].ToString()!="")
					{
					model.Addr1=dt.Rows[n]["Addr1"].ToString();
					}
					if(dt.Rows[n]["Addr2"]!=null && dt.Rows[n]["Addr2"].ToString()!="")
					{
					model.Addr2=dt.Rows[n]["Addr2"].ToString();
					}
					if(dt.Rows[n]["Addr3"]!=null && dt.Rows[n]["Addr3"].ToString()!="")
					{
					model.Addr3=dt.Rows[n]["Addr3"].ToString();
					}
					if(dt.Rows[n]["Addr4"]!=null && dt.Rows[n]["Addr4"].ToString()!="")
					{
					model.Addr4=dt.Rows[n]["Addr4"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }

        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }
		#endregion  Method
	}
}

