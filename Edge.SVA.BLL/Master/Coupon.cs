﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
    /// <summary>
    /// 优惠劵表
    /// </summary>
    public partial class Coupon
    {
        private readonly ICoupon dal = DataAccess.CreateCoupon();
        public Coupon()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CouponNumber)
        {
            return dal.Exists(CouponNumber);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Edge.SVA.Model.Coupon model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Edge.SVA.Model.Coupon model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CouponNumber)
        {

            return dal.Delete(CouponNumber);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string CouponNumberlist)
        {
            return dal.DeleteList(CouponNumberlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Edge.SVA.Model.Coupon GetModel(string CouponNumber)
        {

            return dal.GetModel(CouponNumber);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public Edge.SVA.Model.Coupon GetModelByCache(string CouponNumber)
        {

            string CacheKey = "CouponModel-" + CouponNumber;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(CouponNumber);
                    if (objModel != null)
                    {
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (Edge.SVA.Model.Coupon)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.Coupon> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Edge.SVA.Model.Coupon> DataTableToList(DataTable dt)
        {
            List<Edge.SVA.Model.Coupon> modelList = new List<Edge.SVA.Model.Coupon>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.Coupon model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.Coupon();
                    if (dt.Rows[n]["CouponNumber"] != null && dt.Rows[n]["CouponNumber"].ToString() != "")
                    {
                        model.CouponNumber = dt.Rows[n]["CouponNumber"].ToString();
                    }
                    if (dt.Rows[n]["CouponTypeID"] != null && dt.Rows[n]["CouponTypeID"].ToString() != "")
                    {
                        model.CouponTypeID = int.Parse(dt.Rows[n]["CouponTypeID"].ToString());
                    }
                    if (dt.Rows[n]["CouponIssueDate"] != null && dt.Rows[n]["CouponIssueDate"].ToString() != "")
                    {
                        model.CouponIssueDate = DateTime.Parse(dt.Rows[n]["CouponIssueDate"].ToString());
                    }
                    if (dt.Rows[n]["CouponExpiryDate"] != null && dt.Rows[n]["CouponExpiryDate"].ToString() != "")
                    {
                        model.CouponExpiryDate = DateTime.Parse(dt.Rows[n]["CouponExpiryDate"].ToString());
                    }
                    if (dt.Rows[n]["CouponActiveDate"] != null && dt.Rows[n]["CouponActiveDate"].ToString() != "")
                    {
                        model.CouponActiveDate = DateTime.Parse(dt.Rows[n]["CouponActiveDate"].ToString());
                    }
                    if (dt.Rows[n]["StoreID"] != null && dt.Rows[n]["StoreID"].ToString() != "")
                    {
                        model.StoreID = int.Parse(dt.Rows[n]["StoreID"].ToString());
                    }
                    if (dt.Rows[n]["BatchCouponID"] != null && dt.Rows[n]["BatchCouponID"].ToString() != "")
                    {
                        model.BatchCouponID = int.Parse(dt.Rows[n]["BatchCouponID"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = int.Parse(dt.Rows[n]["Status"].ToString());
                    }
                    if (dt.Rows[n]["CouponPassword"] != null && dt.Rows[n]["CouponPassword"].ToString() != "")
                    {
                        model.CouponPassword = dt.Rows[n]["CouponPassword"].ToString();
                    }
                    if (dt.Rows[n]["CardNumber"] != null && dt.Rows[n]["CardNumber"].ToString() != "")
                    {
                        model.CardNumber = dt.Rows[n]["CardNumber"].ToString();
                    }
                    if (dt.Rows[n]["CreatedOn"] != null && dt.Rows[n]["CreatedOn"].ToString() != "")
                    {
                        model.CreatedOn = DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedOn"] != null && dt.Rows[n]["UpdatedOn"].ToString() != "")
                    {
                        model.UpdatedOn = DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
                    }
                    if (dt.Rows[n]["CreatedBy"] != null && dt.Rows[n]["CreatedBy"].ToString() != "")
                    {
                        model.CreatedBy = int.Parse(dt.Rows[n]["CreatedBy"].ToString());
                    }
                    if (dt.Rows[n]["UpdatedBy"] != null && dt.Rows[n]["UpdatedBy"].ToString() != "")
                    {
                        model.UpdatedBy = int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
                    }
                    if (dt.Rows[n]["CouponAmount"] != null && dt.Rows[n]["CouponAmount"].ToString() != "")
                    {
                        model.CouponAmount = decimal.Parse(dt.Rows[n]["CouponAmount"].ToString());
                    }
                    if (dt.Rows[n]["RedeemStoreID"] != null && dt.Rows[n]["RedeemStoreID"].ToString() != "")
                    {
                        model.RedeemStoreID = int.Parse(dt.Rows[n]["RedeemStoreID"].ToString());
                    }
                    if (dt.Rows[n]["PickupFlag"] != null && dt.Rows[n]["PickupFlag"].ToString() != "")
                    {
                        model.PickupFlag = int.Parse(dt.Rows[n]["PickupFlag"].ToString());
                    }
                    if (dt.Rows[n]["LocateStoreID"] != null && dt.Rows[n]["LocateStoreID"].ToString() != "")
                    {
                        model.LocateStoreID = int.Parse(dt.Rows[n]["LocateStoreID"].ToString());
                    }
                    if (dt.Rows[n]["InitCardNumber"] != null && dt.Rows[n]["InitCardNumber"].ToString() != "")
                    {
                        model.InitCardNumber = dt.Rows[n]["InitCardNumber"].ToString();
                    }
                    if (dt.Rows[n]["StockStatus"] != null && dt.Rows[n]["StockStatus"].ToString() != "")
                    {
                        model.StockStatus = int.Parse(dt.Rows[n]["StockStatus"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

