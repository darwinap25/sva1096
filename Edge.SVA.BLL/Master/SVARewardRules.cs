﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 会员推荐新会员奖励规
	/// </summary>
	public partial class SVARewardRules
	{
		private readonly ISVARewardRules dal=DataAccess.CreateSVARewardRules();
		public SVARewardRules()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string SVARewardRulesCode,int SVARewardRulesID)
		{
			return dal.Exists(SVARewardRulesCode,SVARewardRulesID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.SVARewardRules model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.SVARewardRules model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SVARewardRulesID)
		{
			
			return dal.Delete(SVARewardRulesID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string SVARewardRulesCode,int SVARewardRulesID)
		{
			
			return dal.Delete(SVARewardRulesCode,SVARewardRulesID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string SVARewardRulesIDlist )
		{
			return dal.DeleteList(SVARewardRulesIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.SVARewardRules GetModel(int SVARewardRulesID)
		{
			
			return dal.GetModel(SVARewardRulesID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.SVARewardRules GetModelByCache(int SVARewardRulesID)
		{
			
			string CacheKey = "SVARewardRulesModel-" + SVARewardRulesID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(SVARewardRulesID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.SVARewardRules)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.SVARewardRules> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.SVARewardRules> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.SVARewardRules> modelList = new List<Edge.SVA.Model.SVARewardRules>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.SVARewardRules model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.SVARewardRules();
					if(dt.Rows[n]["SVARewardRulesID"]!=null && dt.Rows[n]["SVARewardRulesID"].ToString()!="")
					{
						model.SVARewardRulesID=int.Parse(dt.Rows[n]["SVARewardRulesID"].ToString());
					}
					if(dt.Rows[n]["CardGradeID"]!=null && dt.Rows[n]["CardGradeID"].ToString()!="")
					{
						model.CardGradeID=int.Parse(dt.Rows[n]["CardGradeID"].ToString());
					}
					if(dt.Rows[n]["SVARewardType"]!=null && dt.Rows[n]["SVARewardType"].ToString()!="")
					{
						model.SVARewardType=int.Parse(dt.Rows[n]["SVARewardType"].ToString());
					}
					if(dt.Rows[n]["SVARewardRulesCode"]!=null && dt.Rows[n]["SVARewardRulesCode"].ToString()!="")
					{
					model.SVARewardRulesCode=dt.Rows[n]["SVARewardRulesCode"].ToString();
					}
					if(dt.Rows[n]["RewardAmount"]!=null && dt.Rows[n]["RewardAmount"].ToString()!="")
					{
						model.RewardAmount=decimal.Parse(dt.Rows[n]["RewardAmount"].ToString());
					}
					if(dt.Rows[n]["RewardPoint"]!=null && dt.Rows[n]["RewardPoint"].ToString()!="")
					{
						model.RewardPoint=int.Parse(dt.Rows[n]["RewardPoint"].ToString());
					}
					if(dt.Rows[n]["RewardCouponTypeID"]!=null && dt.Rows[n]["RewardCouponTypeID"].ToString()!="")
					{
						model.RewardCouponTypeID=int.Parse(dt.Rows[n]["RewardCouponTypeID"].ToString());
					}
					if(dt.Rows[n]["RewardCouponCount"]!=null && dt.Rows[n]["RewardCouponCount"].ToString()!="")
					{
						model.RewardCouponCount=int.Parse(dt.Rows[n]["RewardCouponCount"].ToString());
					}
					if(dt.Rows[n]["StartDate"]!=null && dt.Rows[n]["StartDate"].ToString()!="")
					{
						model.StartDate=DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
					}
					if(dt.Rows[n]["EndDate"]!=null && dt.Rows[n]["EndDate"].ToString()!="")
					{
						model.EndDate=DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
						model.Status=int.Parse(dt.Rows[n]["Status"].ToString());
					}
					if(dt.Rows[n]["Note"]!=null && dt.Rows[n]["Note"].ToString()!="")
					{
					model.Note=dt.Rows[n]["Note"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

