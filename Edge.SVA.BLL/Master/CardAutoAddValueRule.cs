﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// Card操作的流水表。
	///通过insert触发器
	/// </summary>
	public partial class CardAutoAddValueRule
	{
		private readonly ICardAutoAddValueRule dal=DataAccess.CreateCardAutoAddValueRule();
		public CardAutoAddValueRule()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid AutoAddRuleID)
		{
			return dal.Exists(AutoAddRuleID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.CardAutoAddValueRule model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.CardAutoAddValueRule model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid AutoAddRuleID)
		{
			
			return dal.Delete(AutoAddRuleID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string AutoAddRuleIDlist )
		{
			return dal.DeleteList(AutoAddRuleIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.CardAutoAddValueRule GetModel(Guid AutoAddRuleID)
		{
			
			return dal.GetModel(AutoAddRuleID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.CardAutoAddValueRule GetModelByCache(Guid AutoAddRuleID)
		{
			
			string CacheKey = "CardAutoAddValueRuleModel-" + AutoAddRuleID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(AutoAddRuleID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.CardAutoAddValueRule)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardAutoAddValueRule> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.CardAutoAddValueRule> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.CardAutoAddValueRule> modelList = new List<Edge.SVA.Model.CardAutoAddValueRule>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.CardAutoAddValueRule model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.CardAutoAddValueRule();
					if(dt.Rows[n]["AutoAddRuleID"]!=null && dt.Rows[n]["AutoAddRuleID"].ToString()!="")
					{
						model.AutoAddRuleID=new Guid(dt.Rows[n]["AutoAddRuleID"].ToString());
					}
					if(dt.Rows[n]["CardTypeID"]!=null && dt.Rows[n]["CardTypeID"].ToString()!="")
					{
					model.CardTypeID=dt.Rows[n]["CardTypeID"].ToString();
					}
					if(dt.Rows[n]["CardGradeID"]!=null && dt.Rows[n]["CardGradeID"].ToString()!="")
					{
					model.CardGradeID=dt.Rows[n]["CardGradeID"].ToString();
					}
					if(dt.Rows[n]["AutoAddRuleSeqNo"]!=null && dt.Rows[n]["AutoAddRuleSeqNo"].ToString()!="")
					{
						model.AutoAddRuleSeqNo=int.Parse(dt.Rows[n]["AutoAddRuleSeqNo"].ToString());
					}
					if(dt.Rows[n]["RuleDesc"]!=null && dt.Rows[n]["RuleDesc"].ToString()!="")
					{
					model.RuleDesc=dt.Rows[n]["RuleDesc"].ToString();
					}
					if(dt.Rows[n]["AddAmount"]!=null && dt.Rows[n]["AddAmount"].ToString()!="")
					{
						model.AddAmount=int.Parse(dt.Rows[n]["AddAmount"].ToString());
					}
					if(dt.Rows[n]["StartDate"]!=null && dt.Rows[n]["StartDate"].ToString()!="")
					{
						model.StartDate=DateTime.Parse(dt.Rows[n]["StartDate"].ToString());
					}
					if(dt.Rows[n]["EndDate"]!=null && dt.Rows[n]["EndDate"].ToString()!="")
					{
						model.EndDate=DateTime.Parse(dt.Rows[n]["EndDate"].ToString());
					}
					if(dt.Rows[n]["Interval"]!=null && dt.Rows[n]["Interval"].ToString()!="")
					{
						model.Interval=int.Parse(dt.Rows[n]["Interval"].ToString());
					}
					if(dt.Rows[n]["IntervalUnit"]!=null && dt.Rows[n]["IntervalUnit"].ToString()!="")
					{
						model.IntervalUnit=int.Parse(dt.Rows[n]["IntervalUnit"].ToString());
					}
					if(dt.Rows[n]["DayOfInterval"]!=null && dt.Rows[n]["DayOfInterval"].ToString()!="")
					{
						model.DayOfInterval=int.Parse(dt.Rows[n]["DayOfInterval"].ToString());
					}
					if(dt.Rows[n]["LastExecDate"]!=null && dt.Rows[n]["LastExecDate"].ToString()!="")
					{
						model.LastExecDate=DateTime.Parse(dt.Rows[n]["LastExecDate"].ToString());
					}
					if(dt.Rows[n]["ExecutedCount"]!=null && dt.Rows[n]["ExecutedCount"].ToString()!="")
					{
						model.ExecutedCount=int.Parse(dt.Rows[n]["ExecutedCount"].ToString());
					}
					if(dt.Rows[n]["JustForBirth"]!=null && dt.Rows[n]["JustForBirth"].ToString()!="")
					{
						model.JustForBirth=int.Parse(dt.Rows[n]["JustForBirth"].ToString());
					}
					if(dt.Rows[n]["UpdatedDate"]!=null && dt.Rows[n]["UpdatedDate"].ToString()!="")
					{
						model.UpdatedDate=DateTime.Parse(dt.Rows[n]["UpdatedDate"].ToString());
					}
					if(dt.Rows[n]["CreatedDate"]!=null && dt.Rows[n]["CreatedDate"].ToString()!="")
					{
						model.CreatedDate=DateTime.Parse(dt.Rows[n]["CreatedDate"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
					model.CreatedBy=dt.Rows[n]["CreatedBy"].ToString();
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
					model.UpdatedBy=dt.Rows[n]["UpdatedBy"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

