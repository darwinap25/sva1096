﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 批次表
	/// </summary>
	public partial class BatchCard
	{
		private readonly IBatchCard dal=DataAccess.CreateBatchCard();
		public BatchCard()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string BatchCardCode,int BatchCardID)
		{
			return dal.Exists(BatchCardCode,BatchCardID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.BatchCard model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.BatchCard model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int BatchCardID)
		{
			
			return dal.Delete(BatchCardID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string BatchCardCode,int BatchCardID)
		{
			
			return dal.Delete(BatchCardCode,BatchCardID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string BatchCardIDlist )
		{
			return dal.DeleteList(BatchCardIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.BatchCard GetModel(int BatchCardID)
		{
			
			return dal.GetModel(BatchCardID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.BatchCard GetModelByCache(int BatchCardID)
		{
			
			string CacheKey = "BatchCardModel-" + BatchCardID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(BatchCardID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.BatchCard)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.BatchCard> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.BatchCard> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.BatchCard> modelList = new List<Edge.SVA.Model.BatchCard>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.BatchCard model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.BatchCard();
					if(dt.Rows[n]["BatchCardID"]!=null && dt.Rows[n]["BatchCardID"].ToString()!="")
					{
						model.BatchCardID=int.Parse(dt.Rows[n]["BatchCardID"].ToString());
					}
					if(dt.Rows[n]["BatchCardCode"]!=null && dt.Rows[n]["BatchCardCode"].ToString()!="")
					{
					model.BatchCardCode=dt.Rows[n]["BatchCardCode"].ToString();
					}
					if(dt.Rows[n]["Qty"]!=null && dt.Rows[n]["Qty"].ToString()!="")
					{
						model.Qty=int.Parse(dt.Rows[n]["Qty"].ToString());
					}
					if(dt.Rows[n]["CardGradeID"]!=null && dt.Rows[n]["CardGradeID"].ToString()!="")
					{
						model.CardGradeID=int.Parse(dt.Rows[n]["CardGradeID"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					if(dt.Rows[n]["SeqFrom"]!=null && dt.Rows[n]["SeqFrom"].ToString()!="")
					{
						model.SeqFrom=long.Parse(dt.Rows[n]["SeqFrom"].ToString());
					}
					if(dt.Rows[n]["SeqTo"]!=null && dt.Rows[n]["SeqTo"].ToString()!="")
					{
						model.SeqTo=long.Parse(dt.Rows[n]["SeqTo"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

