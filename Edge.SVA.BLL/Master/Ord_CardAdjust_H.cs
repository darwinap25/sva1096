﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 卡调整单据主表
	/// </summary>
	public partial class Ord_CardAdjust_H
	{
		private readonly IOrd_CardAdjust_H dal=DataAccess.CreateOrd_CardAdjust_H();

        //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        private string SessionChangeBrandIDsStr(string strWhere)
        {
            string str = SessionInfo.BrandIDsStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " StoreCode in (select storecode from store where brandid in " + str+")";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and StoreCode in (select storecode from store where brandid in " + str + ")";
                    }
                    else
                    {
                        strWhere = strs[0] + " and StoreCode in (select storecode from store where brandid in " + str + ")" + strs[1];
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " 1!=1 ";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and 1!=1 ";
                    }
                    else
                    {
                        strWhere = strs[0] + " and 1!=1 " + strs[1];
                    }
                }
            }
            return strWhere;
        }
        //End

		public Ord_CardAdjust_H()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CardAdjustNumber)
		{
			return dal.Exists(CardAdjustNumber);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Edge.SVA.Model.Ord_CardAdjust_H model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_CardAdjust_H model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CardAdjustNumber)
		{
			
			return dal.Delete(CardAdjustNumber);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CardAdjustNumberlist )
		{
			return dal.DeleteList(CardAdjustNumberlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_CardAdjust_H GetModel(string CardAdjustNumber)
		{
			
			return dal.GetModel(CardAdjustNumber);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Ord_CardAdjust_H GetModelByCache(string CardAdjustNumber)
		{
			
			string CacheKey = "Ord_CardAdjust_HModel-" + CardAdjustNumber;
            object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CardAdjustNumber);
					if (objModel != null)
					{
                        int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Ord_CardAdjust_H)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
            //strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
            //strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CardAdjust_H> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_CardAdjust_H> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Ord_CardAdjust_H> modelList = new List<Edge.SVA.Model.Ord_CardAdjust_H>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Ord_CardAdjust_H model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        ///<summary>
        ///获取分页总数
        ///</summary>
        public int GetCount(string strWhere)
        {
            strWhere = SessionChangeBrandIDsStr(strWhere); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            return dal.GetCount(strWhere);
        }

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

