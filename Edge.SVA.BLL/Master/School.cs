﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 学校表
	/// </summary>
	public partial class School
	{
		private readonly ISchool dal=DataAccess.CreateSchool();
		public School()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SchoolID)
		{
			return dal.Exists(SchoolID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.School model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.School model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SchoolID)
		{
			
			return dal.Delete(SchoolID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string SchoolIDlist )
		{
			return dal.DeleteList(SchoolIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.School GetModel(int SchoolID)
		{
			
			return dal.GetModel(SchoolID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.School GetModelByCache(int SchoolID)
		{
			
			string CacheKey = "SchoolModel-" + SchoolID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(SchoolID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.School)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.School> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.School> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.School> modelList = new List<Edge.SVA.Model.School>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.School model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.School();
					if(dt.Rows[n]["SchoolID"]!=null && dt.Rows[n]["SchoolID"].ToString()!="")
					{
						model.SchoolID=int.Parse(dt.Rows[n]["SchoolID"].ToString());
					}
					if(dt.Rows[n]["SchoolCode"]!=null && dt.Rows[n]["SchoolCode"].ToString()!="")
					{
					model.SchoolCode=dt.Rows[n]["SchoolCode"].ToString();
					}
					if(dt.Rows[n]["SchoolName1"]!=null && dt.Rows[n]["SchoolName1"].ToString()!="")
					{
					model.SchoolName1=dt.Rows[n]["SchoolName1"].ToString();
					}
					if(dt.Rows[n]["SchoolName2"]!=null && dt.Rows[n]["SchoolName2"].ToString()!="")
					{
					model.SchoolName2=dt.Rows[n]["SchoolName2"].ToString();
					}
					if(dt.Rows[n]["SchoolName3"]!=null && dt.Rows[n]["SchoolName3"].ToString()!="")
					{
					model.SchoolName3=dt.Rows[n]["SchoolName3"].ToString();
					}
					if(dt.Rows[n]["SchoolArea1"]!=null && dt.Rows[n]["SchoolArea1"].ToString()!="")
					{
					model.SchoolArea1=dt.Rows[n]["SchoolArea1"].ToString();
					}
					if(dt.Rows[n]["SchoolArea2"]!=null && dt.Rows[n]["SchoolArea2"].ToString()!="")
					{
					model.SchoolArea2=dt.Rows[n]["SchoolArea2"].ToString();
					}
					if(dt.Rows[n]["SchoolArea3"]!=null && dt.Rows[n]["SchoolArea3"].ToString()!="")
					{
					model.SchoolArea3=dt.Rows[n]["SchoolArea3"].ToString();
					}
					if(dt.Rows[n]["SchoolDistrict1"]!=null && dt.Rows[n]["SchoolDistrict1"].ToString()!="")
					{
					model.SchoolDistrict1=dt.Rows[n]["SchoolDistrict1"].ToString();
					}
					if(dt.Rows[n]["SchoolDistrict2"]!=null && dt.Rows[n]["SchoolDistrict2"].ToString()!="")
					{
					model.SchoolDistrict2=dt.Rows[n]["SchoolDistrict2"].ToString();
					}
					if(dt.Rows[n]["SchoolDistrict3"]!=null && dt.Rows[n]["SchoolDistrict3"].ToString()!="")
					{
					model.SchoolDistrict3=dt.Rows[n]["SchoolDistrict3"].ToString();
					}
					if(dt.Rows[n]["SchoolNote"]!=null && dt.Rows[n]["SchoolNote"].ToString()!="")
					{
					model.SchoolNote=dt.Rows[n]["SchoolNote"].ToString();
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

