﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// 批次表
	/// </summary>
	public partial class Batch
	{
		private readonly IBatch dal=DataAccess.CreateBatch();
		public Batch()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string BatchCode,int BatchID)
		{
			return dal.Exists(BatchCode,BatchID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Batch model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Batch model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int BatchID)
		{
			
			return dal.Delete(BatchID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string BatchCode,int BatchID)
		{
			
			return dal.Delete(BatchCode,BatchID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string BatchIDlist )
		{
			return dal.DeleteList(BatchIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Batch GetModel(int BatchID)
		{
			
			return dal.GetModel(BatchID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Batch GetModelByCache(int BatchID)
		{
			
			string CacheKey = "BatchModel-" + BatchID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(BatchID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Batch)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Batch> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Batch> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Batch> modelList = new List<Edge.SVA.Model.Batch>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Batch model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Batch();
					if(dt.Rows[n]["BatchID"]!=null && dt.Rows[n]["BatchID"].ToString()!="")
					{
						model.BatchID=int.Parse(dt.Rows[n]["BatchID"].ToString());
					}
					if(dt.Rows[n]["BatchCode"]!=null && dt.Rows[n]["BatchCode"].ToString()!="")
					{
					model.BatchCode=dt.Rows[n]["BatchCode"].ToString();
					}
					if(dt.Rows[n]["SeqFrom"]!=null && dt.Rows[n]["SeqFrom"].ToString()!="")
					{
						model.SeqFrom=int.Parse(dt.Rows[n]["SeqFrom"].ToString());
					}
					if(dt.Rows[n]["SeqTo"]!=null && dt.Rows[n]["SeqTo"].ToString()!="")
					{
						model.SeqTo=int.Parse(dt.Rows[n]["SeqTo"].ToString());
					}
					if(dt.Rows[n]["InitAmount"]!=null && dt.Rows[n]["InitAmount"].ToString()!="")
					{
						model.InitAmount=decimal.Parse(dt.Rows[n]["InitAmount"].ToString());
					}
					if(dt.Rows[n]["Qty"]!=null && dt.Rows[n]["Qty"].ToString()!="")
					{
						model.Qty=int.Parse(dt.Rows[n]["Qty"].ToString());
					}
					if(dt.Rows[n]["BatchType"]!=null && dt.Rows[n]["BatchType"].ToString()!="")
					{
						model.BatchType=int.Parse(dt.Rows[n]["BatchType"].ToString());
					}
					if(dt.Rows[n]["TypeID"]!=null && dt.Rows[n]["TypeID"].ToString()!="")
					{
					model.TypeID=dt.Rows[n]["TypeID"].ToString();
					}
					if(dt.Rows[n]["Active"]!=null && dt.Rows[n]["Active"].ToString()!="")
					{
						model.Active=int.Parse(dt.Rows[n]["Active"].ToString());
					}
					if(dt.Rows[n]["Redeem"]!=null && dt.Rows[n]["Redeem"].ToString()!="")
					{
						model.Redeem=int.Parse(dt.Rows[n]["Redeem"].ToString());
					}
					if(dt.Rows[n]["Expiry"]!=null && dt.Rows[n]["Expiry"].ToString()!="")
					{
						model.Expiry=int.Parse(dt.Rows[n]["Expiry"].ToString());
					}
					if(dt.Rows[n]["Void"]!=null && dt.Rows[n]["Void"].ToString()!="")
					{
						model.Void=int.Parse(dt.Rows[n]["Void"].ToString());
					}
					if(dt.Rows[n]["Abnormal"]!=null && dt.Rows[n]["Abnormal"].ToString()!="")
					{
						model.Abnormal=int.Parse(dt.Rows[n]["Abnormal"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

