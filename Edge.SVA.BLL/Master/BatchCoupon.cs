﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// Coupon批次表
	/// </summary>
	public partial class BatchCoupon
	{
		private readonly IBatchCoupon dal=DataAccess.CreateBatchCoupon();
		public BatchCoupon()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string BatchCouponCode,int BatchCouponID)
		{
			return dal.Exists(BatchCouponCode,BatchCouponID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.BatchCoupon model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.BatchCoupon model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int BatchCouponID)
		{
			
			return dal.Delete(BatchCouponID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string BatchCouponCode,int BatchCouponID)
		{
			
			return dal.Delete(BatchCouponCode,BatchCouponID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string BatchCouponIDlist )
		{
			return dal.DeleteList(BatchCouponIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.BatchCoupon GetModel(int BatchCouponID)
		{
			
			return dal.GetModel(BatchCouponID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.BatchCoupon GetModelByCache(int BatchCouponID)
		{
			
			string CacheKey = "BatchCouponModel-" + BatchCouponID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(BatchCouponID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.BatchCoupon)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.BatchCoupon> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.BatchCoupon> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.BatchCoupon> modelList = new List<Edge.SVA.Model.BatchCoupon>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.BatchCoupon model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.BatchCoupon();
					if(dt.Rows[n]["BatchCouponID"]!=null && dt.Rows[n]["BatchCouponID"].ToString()!="")
					{
						model.BatchCouponID=int.Parse(dt.Rows[n]["BatchCouponID"].ToString());
					}
					if(dt.Rows[n]["BatchCouponCode"]!=null && dt.Rows[n]["BatchCouponCode"].ToString()!="")
					{
					model.BatchCouponCode=dt.Rows[n]["BatchCouponCode"].ToString();
					}
					if(dt.Rows[n]["SeqFrom"]!=null && dt.Rows[n]["SeqFrom"].ToString()!="")
					{
						model.SeqFrom=int.Parse(dt.Rows[n]["SeqFrom"].ToString());
					}
					if(dt.Rows[n]["SeqTo"]!=null && dt.Rows[n]["SeqTo"].ToString()!="")
					{
						model.SeqTo=int.Parse(dt.Rows[n]["SeqTo"].ToString());
					}
					if(dt.Rows[n]["Qty"]!=null && dt.Rows[n]["Qty"].ToString()!="")
					{
						model.Qty=int.Parse(dt.Rows[n]["Qty"].ToString());
					}
					if(dt.Rows[n]["CouponTypeID"]!=null && dt.Rows[n]["CouponTypeID"].ToString()!="")
					{
						model.CouponTypeID=int.Parse(dt.Rows[n]["CouponTypeID"].ToString());
					}
					if(dt.Rows[n]["CreatedOn"]!=null && dt.Rows[n]["CreatedOn"].ToString()!="")
					{
						model.CreatedOn=DateTime.Parse(dt.Rows[n]["CreatedOn"].ToString());
					}
					if(dt.Rows[n]["UpdatedOn"]!=null && dt.Rows[n]["UpdatedOn"].ToString()!="")
					{
						model.UpdatedOn=DateTime.Parse(dt.Rows[n]["UpdatedOn"].ToString());
					}
					if(dt.Rows[n]["CreatedBy"]!=null && dt.Rows[n]["CreatedBy"].ToString()!="")
					{
						model.CreatedBy=int.Parse(dt.Rows[n]["CreatedBy"].ToString());
					}
					if(dt.Rows[n]["UpdatedBy"]!=null && dt.Rows[n]["UpdatedBy"].ToString()!="")
					{
						model.UpdatedBy=int.Parse(dt.Rows[n]["UpdatedBy"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder);
        }
        /// <summary>
        /// 获取分页总数
        /// </summary>
        public int GetCount(string strWhere)
        {
            return dal.GetCount(strWhere);
        }

		#endregion  Method
	}
}

