﻿using System;
using System.Data;
using System.Collections.Generic;
using Edge.Common;
using Edge.SVA.Model;
using Edge.SVA.DALFactory;
using Edge.SVA.IDAL;
namespace Edge.SVA.BLL
{
	/// <summary>
	/// Ord_ImportMember_H的子表。 当导入Member数据时（MII）
	/// </summary>
	public partial class Ord_ImportMember_D
	{
		private readonly IOrd_ImportMember_D dal=DataAccess.CreateOrd_ImportMember_D();
		public Ord_ImportMember_D()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int KeyID)
		{
			return dal.Exists(KeyID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(Edge.SVA.Model.Ord_ImportMember_D model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Edge.SVA.Model.Ord_ImportMember_D model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int KeyID)
		{
			
			return dal.Delete(KeyID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string KeyIDlist )
		{
			return dal.DeleteList(KeyIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Edge.SVA.Model.Ord_ImportMember_D GetModel(int KeyID)
		{
			
			return dal.GetModel(KeyID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public Edge.SVA.Model.Ord_ImportMember_D GetModelByCache(int KeyID)
		{
			
			string CacheKey = "Ord_ImportMember_DModel-" + KeyID;
			object objModel = Edge.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(KeyID);
					if (objModel != null)
					{
						int ModelCache = Edge.Common.ConfigHelper.GetConfigInt("ModelCache");
						Edge.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (Edge.SVA.Model.Ord_ImportMember_D)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_ImportMember_D> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<Edge.SVA.Model.Ord_ImportMember_D> DataTableToList(DataTable dt)
		{
			List<Edge.SVA.Model.Ord_ImportMember_D> modelList = new List<Edge.SVA.Model.Ord_ImportMember_D>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				Edge.SVA.Model.Ord_ImportMember_D model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new Edge.SVA.Model.Ord_ImportMember_D();
					if(dt.Rows[n]["KeyID"]!=null && dt.Rows[n]["KeyID"].ToString()!="")
					{
						model.KeyID=int.Parse(dt.Rows[n]["KeyID"].ToString());
					}
					if(dt.Rows[n]["ImportMemberNumber"]!=null && dt.Rows[n]["ImportMemberNumber"].ToString()!="")
					{
					model.ImportMemberNumber=dt.Rows[n]["ImportMemberNumber"].ToString();
					}
					if(dt.Rows[n]["RRCCardNumber"]!=null && dt.Rows[n]["RRCCardNumber"].ToString()!="")
					{
					model.RRCCardNumber=dt.Rows[n]["RRCCardNumber"].ToString();
					}
					if(dt.Rows[n]["OLDCardNumber"]!=null && dt.Rows[n]["OLDCardNumber"].ToString()!="")
					{
					model.OLDCardNumber=dt.Rows[n]["OLDCardNumber"].ToString();
					}
					if(dt.Rows[n]["OLDRRCCardNumber"]!=null && dt.Rows[n]["OLDRRCCardNumber"].ToString()!="")
					{
					model.OLDRRCCardNumber=dt.Rows[n]["OLDRRCCardNumber"].ToString();
					}
					if(dt.Rows[n]["LName"]!=null && dt.Rows[n]["LName"].ToString()!="")
					{
					model.LName=dt.Rows[n]["LName"].ToString();
					}
					if(dt.Rows[n]["FName"]!=null && dt.Rows[n]["FName"].ToString()!="")
					{
					model.FName=dt.Rows[n]["FName"].ToString();
					}
					if(dt.Rows[n]["VPPTS"]!=null && dt.Rows[n]["VPPTS"].ToString()!="")
					{
					model.VPPTS=dt.Rows[n]["VPPTS"].ToString();
					}
					if(dt.Rows[n]["RetailID"]!=null && dt.Rows[n]["RetailID"].ToString()!="")
					{
					model.RetailID=dt.Rows[n]["RetailID"].ToString();
					}
					if(dt.Rows[n]["StoreID"]!=null && dt.Rows[n]["StoreID"].ToString()!="")
					{
					model.StoreID=dt.Rows[n]["StoreID"].ToString();
					}
					if(dt.Rows[n]["CardStatus"]!=null && dt.Rows[n]["CardStatus"].ToString()!="")
					{
						model.CardStatus=int.Parse(dt.Rows[n]["CardStatus"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

