﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.SVA.BLL.Domain.ViewModelBLL
{
    public class CardGradeViewModelBLL
    {
        private static BrandRepostory brandRepostory = BrandRepostory.Singleton;
        private static CardTypeRepostory cardTypeRepostory = CardTypeRepostory.Singleton;
        private static CardGradeRepostory cardGradeRepostory = CardGradeRepostory.Singleton;
        private static CouponTypeNatureRepostory couponTypeNatureRepostory = CouponTypeNatureRepostory.Singleton;
        private static CouponTypeRepostory couponTypeRepostory = CouponTypeRepostory.Singleton;

        public CardGradeViewModel GetModel(int cardGradeId, string lan)
        {
            CardGradeViewModel rtn = new CardGradeViewModel();
            Edge.SVA.BLL.CardGrade bll = new SVA.BLL.CardGrade();
            Edge.SVA.Model.CardGrade model = cardGradeRepostory.GetModelByID(cardGradeId);
            rtn.MainTable = model;

            Edge.SVA.BLL.CardGradeHoldCouponRule bll1 = new SVA.BLL.CardGradeHoldCouponRule();

            List<Edge.SVA.Model.CardGradeHoldCouponRule> list = bll1.GetModelList(" RuleType=2 and CardGradeID="+rtn.MainTable.CardGradeID.ToString());
            SetDataCoupponGradeList(lan, rtn.CanHoldCouponGradeList, list);
            list = bll1.GetModelList(" RuleType=1 and CardGradeID=" + rtn.MainTable.CardGradeID.ToString());
            SetDataCoupponGradeList(lan, rtn.GetCouponGradeList, list);
            return rtn;
        }

        private void SetDataCoupponGradeList(string lan, List<CardGradeHoldCouponRuleViewModel> cardGradeHoldCouponRuleViewModelList, List<Edge.SVA.Model.CardGradeHoldCouponRule> cardGradeHoldCouponRuleList)
        {
            foreach (var item in cardGradeHoldCouponRuleList)
            {
                Edge.SVA.Model.Domain.File.BasicViewModel.CardGradeHoldCouponRuleViewModel cardGradeHoldCouponRuleViewModel = new SVA.Model.Domain.File.BasicViewModel.CardGradeHoldCouponRuleViewModel();
                cardGradeHoldCouponRuleViewModel.MainTable = item;
                SetCardGradeHoldCouponRuleViewModelDescription(lan, cardGradeHoldCouponRuleViewModel);
                cardGradeHoldCouponRuleViewModelList.Add(cardGradeHoldCouponRuleViewModel);
            }
        }

        public void SetCardGradeHoldCouponRuleViewModelDescription(string lan, Edge.SVA.Model.Domain.File.BasicViewModel.CardGradeHoldCouponRuleViewModel cardGradeHoldCouponRuleViewModel)
        {
            StringBuilder sb = new StringBuilder();
            Edge.SVA.Model.CouponType couponType = couponTypeRepostory.GetModelByID(cardGradeHoldCouponRuleViewModel.MainTable.CouponTypeID);
            Edge.SVA.Model.Brand brand = brandRepostory.GetModelByID(couponType.BrandID);
            switch (lan)
            {
                case LanguageFlag.ENUS:
                    sb.Append(couponType.CouponTypeCode);
                    sb.Append("-");
                    sb.Append(couponType.CouponTypeName1);
                    cardGradeHoldCouponRuleViewModel.CouponTypeDescription = sb.ToString();
                    sb.Remove(0, sb.Length);
                    sb.Append(brand.BrandCode);
                    sb.Append("-");
                    sb.Append(brand.BrandName1);
                    cardGradeHoldCouponRuleViewModel.BrandDescription = sb.ToString();
                    break;
                case LanguageFlag.ZHCN:
                    sb.Remove(0, sb.Length);
                    sb.Append(couponType.CouponTypeCode);
                    sb.Append("-");
                    sb.Append(couponType.CouponTypeName2);
                    cardGradeHoldCouponRuleViewModel.CouponTypeDescription = sb.ToString();
                    sb.Remove(0, sb.Length);
                    sb.Append(brand.BrandCode);
                    sb.Append("-");
                    sb.Append(brand.BrandName2);
                    cardGradeHoldCouponRuleViewModel.BrandDescription = sb.ToString();
                    break;
                case LanguageFlag.ZHHK:
                    sb.Remove(0, sb.Length);
                    sb.Append(couponType.CouponTypeCode);
                    sb.Append("-");
                    sb.Append(couponType.CouponTypeName3);
                    cardGradeHoldCouponRuleViewModel.CouponTypeDescription = sb.ToString();
                    sb.Remove(0, sb.Length);
                    sb.Append(brand.BrandCode);
                    sb.Append("-");
                    sb.Append(brand.BrandName3);
                    cardGradeHoldCouponRuleViewModel.BrandDescription = sb.ToString();
                    break;
                default:
                    break;
            }
        }
    }
}
