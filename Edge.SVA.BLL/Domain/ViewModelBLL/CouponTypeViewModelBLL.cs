﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.File;

namespace Edge.SVA.BLL.Domain.ViewModelBLL
{
    public class CouponTypeViewModelBLL
    {
        public CouponTypeViewModel GetModel(int id,string lan)
        {
            CouponTypeViewModel rtn=new CouponTypeViewModel();
            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.Model.CouponType mainTable = bll.GetModel(id);

            rtn.MainTable = mainTable;

            Edge.SVA.BLL.CouponReplenishDaily bll1 = new Edge.SVA.BLL.CouponReplenishDaily();
            List<Model.CouponReplenishDaily> list=bll1.GetModelList(" CouponTypeID=" + rtn.MainTable.CouponTypeID.ToString() + " order by WeekDayNum");
            foreach (var item in list)
            {
                Model.Domain.File.BasicViewModel.CouponReplenishDailyViewModel v = new Model.Domain.File.BasicViewModel.CouponReplenishDailyViewModel();
                v.MainTable = item;
                rtn.CouponReplenishDailyViewModelList.Add(v);
            }
            return rtn;
        }
    }
}
