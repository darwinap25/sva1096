﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class CampaignCouponType
    {
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(Edge.SVA.Model.CampaignCouponType model, SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CampaignCouponType(");
            strSql.Append("CampaignID,CouponTypeID)");
            strSql.Append(" values (");
            strSql.Append("@CampaignID,@CouponTypeID)");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignID;
            parameters[1].Value = model.CouponTypeID;

            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public void Update(Edge.SVA.Model.CampaignCouponType model, SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CampaignCouponType set ");

            strSql.Append("CampaignID=@CampaignID,");
            strSql.Append("CouponTypeID=@CouponTypeID");
            strSql.Append(" where CampaignID=@CampaignID and CouponTypeID=@CouponTypeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignID;
            parameters[1].Value = model.CouponTypeID;

            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public void Delete(int CampaignID, int CouponTypeID, SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CampaignCouponType ");

            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CouponTypeID", SqlDbType.Int,4)};

            if (CouponTypeID == 0)
            {
                strSql.Append(" where CampaignID=@CampaignID ");
                parameters[0].Value = CampaignID;
            }
            else
            {
                strSql.Append(" where CampaignID=@CampaignID and CouponTypeID=@CouponTypeID ");
                parameters[0].Value = CampaignID;
                parameters[1].Value = CouponTypeID;
            }
            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }
            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }
    }
}
