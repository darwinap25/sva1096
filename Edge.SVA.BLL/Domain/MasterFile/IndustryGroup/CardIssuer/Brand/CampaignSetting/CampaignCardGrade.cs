﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class CampaignCardGrade
    {
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(Edge.SVA.Model.CampaignCardGrade model,SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CampaignCardGrade(");
            strSql.Append("CampaignID,CardGradeID)");
            strSql.Append(" values (");
            strSql.Append("@CampaignID,@CardGradeID)");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignID;
            parameters[1].Value = model.CardGradeID;
            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public void Update(Edge.SVA.Model.CampaignCardGrade model,SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CampaignCardGrade set ");

            strSql.Append("CampaignID=@CampaignID,");
            strSql.Append("CardGradeID=@CardGradeID");
            strSql.Append(" where CampaignID=@CampaignID and CardGradeID=@CardGradeID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            parameters[0].Value = model.CampaignID;
            parameters[1].Value = model.CardGradeID;
            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }

            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public void Delete(int CampaignID, int CardGradeID,SqlTransaction trans)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CampaignCardGrade ");
            SqlParameter[] parameters = {
					new SqlParameter("@CampaignID", SqlDbType.Int,4),
					new SqlParameter("@CardGradeID", SqlDbType.Int,4)};
            if (CardGradeID == 0)
            {
                strSql.Append(" where CampaignID=@CampaignID ");
                parameters[0].Value = CampaignID;
            }
            else
            {
                strSql.Append(" where CampaignID=@CampaignID and CardGradeID=@CardGradeID ");
                parameters[0].Value = CampaignID;
                parameters[1].Value = CardGradeID;
            }
            //将空值赋值为默认值
            foreach (SqlParameter parameter in parameters)
            {
                if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                {
                    parameter.Value = DBNull.Value;
                }
            }
            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strSql.ToString(), parameters);//执行多个不同的DAL
        }
    }
}
