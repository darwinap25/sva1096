﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class CardTypeRepostory
    {
        private static readonly object syncObj = new object();
        private static readonly CardTypeRepostory instance = new CardTypeRepostory();
        private CardTypeRepostory()
        {
            LoadDataFromDatabase();
        }
        
        private void LoadDataFromDatabase()
        {
            CardType bll = new CardType();
            List<Edge.SVA.Model.CardType> list = bll.GetModelList(string.Empty);
            Dictionary<int, Edge.SVA.Model.CardType> dic = new Dictionary<int, Edge.SVA.Model.CardType>();
            Dictionary<string, Edge.SVA.Model.CardType> dic1 = new Dictionary<string, Edge.SVA.Model.CardType>();
            foreach (var item in list)
            {
                dic.Add(item.CardTypeID, item);
                dic1.Add(item.CardTypeCode, item);
            }
            idDic = dic;
            codeDic = dic1;
        }
        private Dictionary<int, Edge.SVA.Model.CardType> idDic = new Dictionary<int, Edge.SVA.Model.CardType>();
        private Dictionary<string, Edge.SVA.Model.CardType> codeDic = new Dictionary<string, Edge.SVA.Model.CardType>();
        public static CardTypeRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        public void Refresh()
        {
            //Thread t = new Thread(new ThreadStart(RefreshAsync));
            //t.Start();
            RefreshAsync();
        }
        private void RefreshAsync()
        {
            lock (syncObj)
            {
                LoadDataFromDatabase();
            }
        }
        public Edge.SVA.Model.CardType GetModelByID(int id)
        {
            if (!idDic.ContainsKey(id))
            {
                Refresh();
            }
            if (idDic.ContainsKey(id))
            {
                return idDic[id];
            }
            return new Edge.SVA.Model.CardType();
        }
        public Edge.SVA.Model.CardType GetModelByCode(string code)
        {
            if (!codeDic.ContainsKey(code))
            {
                Refresh();
            }
            if (codeDic.ContainsKey(code))
            {
                return codeDic[code];
            }
            return new Edge.SVA.Model.CardType();
        }
    }
}
