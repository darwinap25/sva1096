﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class CardRepostory
    {
        private static readonly object syncObj = new object();
        private static readonly CardRepostory instance = new CardRepostory();
        private CardRepostory()
        {
            LoadDataFromDatabase();
        }

        private void LoadDataFromDatabase()
        {
            Card bll = new Card();
            List<Edge.SVA.Model.Card> list = bll.GetModelList(string.Empty);
            Dictionary<int, Edge.SVA.Model.Card> dic = new Dictionary<int, Edge.SVA.Model.Card>();
            Dictionary<string, Edge.SVA.Model.Card> dic1 = new Dictionary<string, Edge.SVA.Model.Card>();
            foreach (var item in list)
            {
                dic.Add(item.BatchCardID.GetValueOrDefault(), item);
                dic1.Add(item.CardNumber, item);
            }
            idDic = dic;
            codeDic = dic1;
        }
        private Dictionary<int, Edge.SVA.Model.Card> idDic = new Dictionary<int, Edge.SVA.Model.Card>();
        private Dictionary<string, Edge.SVA.Model.Card> codeDic = new Dictionary<string, Edge.SVA.Model.Card>();
        public static CardRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        public void Refresh()
        {
            //Thread t = new Thread(new ThreadStart(RefreshAsync));
            //t.Start();
            RefreshAsync();
        }
        private void RefreshAsync()
        {
            lock (syncObj)
            {
                LoadDataFromDatabase();
            }
        }
        public Edge.SVA.Model.Card GetModelByID(int id)
        {
            if (!idDic.ContainsKey(id))
            {
                Refresh();
            }
            if (idDic.ContainsKey(id))
            {
                return idDic[id];
            }
            return new Edge.SVA.Model.Card();
        }
        public Edge.SVA.Model.Card GetModelByCode(string code)
        {
            if (!codeDic.ContainsKey(code))
            {
                Refresh();
            }
            if (codeDic.ContainsKey(code))
            {
                return codeDic[code];
            }
            return new Edge.SVA.Model.Card();
        }
    }
}
