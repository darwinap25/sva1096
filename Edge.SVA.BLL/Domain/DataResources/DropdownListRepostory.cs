﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class DropdownListRepostory
    {
        private static readonly object syncObj = new object();
        private static readonly DropdownListRepostory instance = new DropdownListRepostory();
        private DropdownListRepostory()
        {
            LoadDataFromDatabase();
        }
        public static DropdownListRepostory Singleton { get { return instance; } }

        private List<Edge.SVA.Model.DropdownList> list;

        private void LoadDataFromDatabase()
        {
            DropdownList bll = new DropdownList();
            list = bll.GetModelList(" 1=1 ORDER BY TypeCode, OrderID");
        }
        public List<IKeyValue> GetDropdownList(string typecode, string lan)
        {
            if (string.IsNullOrEmpty(lan))
            {
                lan = "zh-cn";
            }
            List<Edge.SVA.Model.DropdownList> list1 = list.FindAll(mm => mm.TypeCode == typecode && mm.Lan == lan);
            List<IKeyValue> rtn = new List<IKeyValue>();
            foreach (var item in list1)
            {
                rtn.Add(new KeyValue(item.Code, item.Text));
            }
            return rtn;
        }
        public string GetDropdownListText(string typecode, string lan, string key)
        {
            if (string.IsNullOrEmpty(lan))
            {
                lan = "zh-cn";
            }
            Edge.SVA.Model.DropdownList m = list.Find(mm => mm.TypeCode == typecode && mm.Lan == lan && mm.Code == key);
            if (m != null)
            {
                return m.Text;
            }
            return string.Empty;
        }
    }
}
