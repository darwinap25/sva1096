﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class ConstInfosRepostory
    {
        public enum InfoType
        {
            MessageServiceType,
            ValidType,
            MemberRange,
            ComputTypePoint,
            ComputTypeCoupon,
            YesNo,
            Status,
            TransType,
            ExchangeType, //Add By Len
            ExchangeConsumeRuleOper,
            SVARewardType,
            MemberSex,
            MemberMarital
        }
        private static readonly object syncObj = new object();
        private static readonly ConstInfosRepostory instance = new ConstInfosRepostory();
        public static ConstInfosRepostory Singleton
        {
            get
            {
                return instance;
            }
        }
        private Dictionary<InfoType, List<KeyValue>> dic_enus = new Dictionary<InfoType, List<KeyValue>>();
        private Dictionary<InfoType, List<KeyValue>> dic_zhcn = new Dictionary<InfoType, List<KeyValue>>();
        private Dictionary<InfoType, List<KeyValue>> dic_zhhk = new Dictionary<InfoType, List<KeyValue>>();

        #region MessageServiceType
        private void LoadTransactionType()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "Notification"));
            enus.Add(new KeyValue("1", "Email"));
            enus.Add(new KeyValue("2", "SMS"));
            enus.Add(new KeyValue("3", "MMS"));
            enus.Add(new KeyValue("4", "MSN"));
            enus.Add(new KeyValue("5", "QQ"));
            enus.Add(new KeyValue("6", "Whatsapp"));
            enus.Add(new KeyValue("7", "FACEBOOK"));
            enus.Add(new KeyValue("8", "SINAWEIBO"));
            enus.Add(new KeyValue("9", "iOS"));
            enus.Add(new KeyValue("10", "Android"));
            zhcn.Add(new KeyValue("0", "内部消息"));
            zhcn.Add(new KeyValue("1", "邮件"));
            zhcn.Add(new KeyValue("2", "短信"));
            zhcn.Add(new KeyValue("3", "彩信"));
            zhcn.Add(new KeyValue("4", "MSN"));
            zhcn.Add(new KeyValue("5", "QQ"));
            zhcn.Add(new KeyValue("6", "Whatsapp"));
            zhcn.Add(new KeyValue("7", "FACEBOOK"));
            zhcn.Add(new KeyValue("8", "新浪微博"));
            enus.Add(new KeyValue("9", "iOS"));
            enus.Add(new KeyValue("10", "安卓"));
            zhhk.Add(new KeyValue("0", "內部消息"));
            zhhk.Add(new KeyValue("1", "郵件"));
            zhhk.Add(new KeyValue("2", "短信"));
            zhhk.Add(new KeyValue("3", "彩信"));
            zhhk.Add(new KeyValue("4", "MSN"));
            zhhk.Add(new KeyValue("5", "QQ"));
            zhhk.Add(new KeyValue("6", "Whatsapp"));
            zhhk.Add(new KeyValue("7", "FACEBOOK"));
            zhhk.Add(new KeyValue("8", "新浪微博"));
            zhhk.Add(new KeyValue("9", "iOS"));
            zhhk.Add(new KeyValue("10", "安卓"));

            dic_enus.Add(InfoType.MessageServiceType, enus);
            dic_zhcn.Add(InfoType.MessageServiceType, zhcn);
            dic_zhhk.Add(InfoType.MessageServiceType, zhhk);
        }   

        #endregion
        #region ValidType
        private void LoadValidDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "Close"));
            enus.Add(new KeyValue("1", "Open"));
            zhcn.Add(new KeyValue("0", "启动"));
            zhcn.Add(new KeyValue("1", "关闭"));
            zhhk.Add(new KeyValue("0", "啟動"));
            zhhk.Add(new KeyValue("1", "關閉"));

            dic_enus.Add(InfoType.ValidType, enus);
            dic_zhcn.Add(InfoType.ValidType, zhcn);
            dic_zhhk.Add(InfoType.ValidType, zhhk);
        }
        #endregion
        #region MemberRange Desc
        private void LoadMemberRangeDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "All Members"));
            enus.Add(new KeyValue("3", "Birthday Member"));
            enus.Add(new KeyValue("1", "Birthday Month Member"));
            zhcn.Add(new KeyValue("0", "所有会员"));
            zhcn.Add(new KeyValue("3", "当天生日的会员"));
            zhcn.Add(new KeyValue("1", "当月生日的会员"));
            zhhk.Add(new KeyValue("0", "所有會員"));
            zhhk.Add(new KeyValue("3", "當天生日的會員"));
            zhhk.Add(new KeyValue("1", "當月生日的會員"));

            dic_enus.Add(InfoType.MemberRange, enus);
            dic_zhcn.Add(InfoType.MemberRange, zhcn);
            dic_zhhk.Add(InfoType.MemberRange, zhhk);
        }
        #endregion
        #region ComputTypePointDesc
        private void LoadComputTypePointDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "each full the setting amount,can get the setting point"));
            enus.Add(new KeyValue("1", "more than or equal to the setting amount,can get the setting point"));
            enus.Add(new KeyValue("2", "less than or equal to the setting amount,can get the setting point"));
            zhcn.Add(new KeyValue("0", "每满设定的金额，可获取设定的积分"));
            zhcn.Add(new KeyValue("1", "大于等于设定的金额，可获取设定的积分"));
            zhcn.Add(new KeyValue("2", "少于等于设定的金额，可获取设定的积分"));
            zhhk.Add(new KeyValue("0", "每滿設定的金額，可獲取設定的積分"));
            zhhk.Add(new KeyValue("1", "大於等於設定的金額，可獲取設定的積分"));
            zhhk.Add(new KeyValue("2", "少於等於設定的金額，可獲取設定的積分"));

            dic_enus.Add(InfoType.ComputTypePoint, enus);
            dic_zhcn.Add(InfoType.ComputTypePoint, zhcn);
            dic_zhhk.Add(InfoType.ComputTypePoint, zhhk);
        }
        #endregion
        #region LoadComputTypeCouponDesc
        private void LoadComputTypeCouponDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "each full the setting amount,can get the setting quantity of coupon"));
            enus.Add(new KeyValue("1", "more than or equal to the setting amount,can get the setting quantity of coupon"));
            enus.Add(new KeyValue("2", "less than or equal to the setting amount,can get the setting quantity of coupon"));
            zhcn.Add(new KeyValue("0", "每满设定的金额，可获取设定数量的优惠券"));
            zhcn.Add(new KeyValue("1", "大于等于设定的金额，可获取设定数量的优惠券"));
            zhcn.Add(new KeyValue("2", "少于等于设定的金额，可获取设定数量的优惠券"));
            zhhk.Add(new KeyValue("0", "每滿設定的金額，可獲取設定數量的優惠券"));
            zhhk.Add(new KeyValue("1", "大於等於設定的金額，可獲取設定數量的優惠券"));
            zhhk.Add(new KeyValue("2", "少於等於設定的金額，可獲取設定數量的優惠券"));

            dic_enus.Add(InfoType.ComputTypeCoupon, enus);
            dic_zhcn.Add(InfoType.ComputTypeCoupon, zhcn);
            dic_zhhk.Add(InfoType.ComputTypeCoupon, zhhk);
        }
        #endregion
        #region YesNo Desc
        private void LoadYesNoDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "No"));
            enus.Add(new KeyValue("1", "Yes"));
            zhcn.Add(new KeyValue("0", "否"));
            zhcn.Add(new KeyValue("1", "是"));
            zhhk.Add(new KeyValue("0", "否"));
            zhhk.Add(new KeyValue("1", "是"));

            dic_enus.Add(InfoType.YesNo, enus);
            dic_zhcn.Add(InfoType.YesNo, zhcn);
            dic_zhhk.Add(InfoType.YesNo, zhhk);
        }
        #endregion
        #region Status Desc
        private void LoadStatusDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "Invalid"));
            enus.Add(new KeyValue("1", "Valid"));
            zhcn.Add(new KeyValue("0", "无效"));
            zhcn.Add(new KeyValue("1", "有效"));
            zhhk.Add(new KeyValue("0", "無效"));
            zhhk.Add(new KeyValue("1", "有效"));

            dic_enus.Add(InfoType.Status, enus);
            dic_zhcn.Add(InfoType.Status, zhcn);
            dic_zhhk.Add(InfoType.Status, zhhk);
        }
        #endregion
        #region Trans Type Desc
        private void LoadTransTypeDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "Consume"));
            enus.Add(new KeyValue("2", "Recharge"));
            zhcn.Add(new KeyValue("0", "消费"));
            zhcn.Add(new KeyValue("2", "充值"));
            zhhk.Add(new KeyValue("0", "消費"));
            zhhk.Add(new KeyValue("2", "充值"));

            dic_enus.Add(InfoType.TransType, enus);
            dic_zhcn.Add(InfoType.TransType, zhcn);
            dic_zhhk.Add(InfoType.TransType, zhhk);
        }
        #endregion
        #region Exchange Type Desc //Add By Len
        private void LoadExchangeTypeDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("7", "Free Exchange"));
            enus.Add(new KeyValue("5", "Redemption Exchange"));
            enus.Add(new KeyValue("1", "Cash Exchange"));
            enus.Add(new KeyValue("2", "Point Exchange"));
            enus.Add(new KeyValue("4", "Coupon Exchange"));
            enus.Add(new KeyValue("3", "Combined Exchange"));

            zhcn.Add(new KeyValue("7", "免费兑换"));
            zhcn.Add(new KeyValue("5", "消费金额兑换"));
            zhcn.Add(new KeyValue("1", "金额兑换"));
            zhcn.Add(new KeyValue("2", "积分兑换"));
            zhcn.Add(new KeyValue("4", "优惠券兑换"));
            zhcn.Add(new KeyValue("3", "组合兑换"));

            zhhk.Add(new KeyValue("7", "免費兌換"));
            zhhk.Add(new KeyValue("5", "消費金額兌換"));
            zhhk.Add(new KeyValue("1", "金額兌換"));
            zhhk.Add(new KeyValue("2", "積分兌換"));
            zhhk.Add(new KeyValue("4", "優惠券兌換"));
            zhhk.Add(new KeyValue("3", "組合兌換"));

            dic_enus.Add(InfoType.ExchangeType, enus);
            dic_zhcn.Add(InfoType.ExchangeType, zhcn);
            dic_zhhk.Add(InfoType.ExchangeType, zhhk);
        }
        #endregion
        #region Exchange ConsumeRuleOper Desc //Add By Len
        private void LoadExchangeConsumeRuleOperDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "each full the require amount"));
            enus.Add(new KeyValue("1", "more than or equal to the require amount"));
            enus.Add(new KeyValue("2", "less than or equal to the require amount"));

            zhcn.Add(new KeyValue("0", "每满所需的金额"));
            zhcn.Add(new KeyValue("1", "大于等于所需的金额"));
            zhcn.Add(new KeyValue("2", "少于等于所需的金额"));

            zhhk.Add(new KeyValue("0", "每滿所需的金額"));
            zhhk.Add(new KeyValue("1", "大于等于所需的金額"));
            zhhk.Add(new KeyValue("2", "少于等于所需的金額"));

            dic_enus.Add(InfoType.ExchangeConsumeRuleOper, enus);
            dic_zhcn.Add(InfoType.ExchangeConsumeRuleOper, zhcn);
            dic_zhhk.Add(InfoType.ExchangeConsumeRuleOper, zhhk);
        }
        #endregion
        #region SVAReward Type Desc //Add By Len
        private void LoadSVARewardTypeDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("1", "Member Get Member"));
            enus.Add(new KeyValue("2", "New Member"));
            enus.Add(new KeyValue("3", "Facebook Share"));
            enus.Add(new KeyValue("4", "The day of member's birthday"));

            zhcn.Add(new KeyValue("1", "会员推荐新会员"));
            zhcn.Add(new KeyValue("2", "新注册会员"));
            zhcn.Add(new KeyValue("3", "Facebook分享"));
            zhcn.Add(new KeyValue("4", "生日当天"));

            zhhk.Add(new KeyValue("1", "會員推薦新會員"));
            zhhk.Add(new KeyValue("2", "新注冊會員"));
            zhhk.Add(new KeyValue("3", "Facebook分享"));
            zhhk.Add(new KeyValue("4", "生日當天"));

            dic_enus.Add(InfoType.SVARewardType, enus);
            dic_zhcn.Add(InfoType.SVARewardType, zhcn);
            dic_zhhk.Add(InfoType.SVARewardType, zhhk);
        }
        #endregion
        #region Member Sex Desc //Add By Len
        private void LoadMemberSexDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "Private"));
            enus.Add(new KeyValue("1", "Male"));
            enus.Add(new KeyValue("2", "Female"));

            zhcn.Add(new KeyValue("0", "保密"));
            zhcn.Add(new KeyValue("1", "男性"));
            zhcn.Add(new KeyValue("2", "女性"));

            zhhk.Add(new KeyValue("0", "保密"));
            zhhk.Add(new KeyValue("1", "男性"));
            zhhk.Add(new KeyValue("2", "女性"));

            dic_enus.Add(InfoType.MemberSex, enus);
            dic_zhcn.Add(InfoType.MemberSex, zhcn);
            dic_zhhk.Add(InfoType.MemberSex, zhhk);
        }
        #endregion
        #region MemberMarital Desc //Add By Len
        private void LoadMemberMaritalDesc()
        {
            List<KeyValue> enus = new List<KeyValue>();
            List<KeyValue> zhcn = new List<KeyValue>();
            List<KeyValue> zhhk = new List<KeyValue>();

            enus.Add(new KeyValue("0", "Private"));
            enus.Add(new KeyValue("1", "Single"));
            enus.Add(new KeyValue("2", "Married"));

            zhcn.Add(new KeyValue("0", "保密"));
            zhcn.Add(new KeyValue("1", "未婚"));
            zhcn.Add(new KeyValue("2", "已婚"));

            zhhk.Add(new KeyValue("0", "保密"));
            zhhk.Add(new KeyValue("1", "未婚"));
            zhhk.Add(new KeyValue("2", "已婚"));

            dic_enus.Add(InfoType.MemberMarital, enus);
            dic_zhcn.Add(InfoType.MemberMarital, zhcn);
            dic_zhhk.Add(InfoType.MemberMarital, zhhk);
        }
        #endregion

        private ConstInfosRepostory()
        {
            LoadTransactionType();
            LoadValidDesc();
            LoadMemberRangeDesc();
            LoadComputTypePointDesc();
            LoadYesNoDesc();
            LoadStatusDesc();
            LoadTransTypeDesc();
            LoadComputTypeCouponDesc();

            LoadExchangeTypeDesc();//Add By Len
            LoadExchangeConsumeRuleOperDesc();
            LoadSVARewardTypeDesc();
            LoadMemberSexDesc();
            LoadMemberMaritalDesc();
            
        }

        #region Public Methonds
        public List<KeyValue> GetKeyValueList(string lan, InfoType type)
        {
            switch (lan)
            {
                case LanguageFlag.ZHCN:
                    return dic_zhcn[type];
                case LanguageFlag.ZHHK:
                    return dic_zhhk[type];
                case LanguageFlag.ENUS:
                default:
                    return dic_enus[type];
            }
        }
        public string GetKeyValueDesc(string id, string lan, InfoType type)
        {
            string rtn = string.Empty;
            KeyValue kv = GetKeyValueList(lan, type).Find(mm => mm.Key.Equals(id));
            if (kv != null)
            {
                rtn = kv.Value;
            }
            return rtn;
        }
        #endregion

    }
}
