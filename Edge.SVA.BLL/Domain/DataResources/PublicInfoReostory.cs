﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.SVA;
using System.Data;
using Edge.SVA.Model.Domain;

namespace Edge.SVA.BLL.Domain.DataResources
{
    public class PublicInfoReostory
    {
        private static readonly object syncObj = new object();
        private static readonly PublicInfoReostory instance = new PublicInfoReostory();
        private PublicInfoReostory()
        {
            //LoadDataFromDB();
        }
        public static PublicInfoReostory Singleton
        {
            get
            {
                return instance;
            }
        }
        private List<BrandInfo> brandList_en_us = new List<BrandInfo>();
        private List<BrandInfo> brandList_zh_cn = new List<BrandInfo>();
        private List<BrandInfo> brandList_zh_hk = new List<BrandInfo>();

        private void LoadDataFromDB()
        {
            Edge.SVA.BLL.Brand brandBll = new Edge.SVA.BLL.Brand();
            Edge.SVA.BLL.CardType cardTypeBll = new Edge.SVA.BLL.CardType();
            Edge.SVA.BLL.CardGrade cardGradeBll = new Edge.SVA.BLL.CardGrade();
            Edge.SVA.BLL.CouponType couponTypeBll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.BLL.CouponTypeNature couponTypeNatureBll = new Edge.SVA.BLL.CouponTypeNature();
            Edge.SVA.BLL.Store storeBll = new Edge.SVA.BLL.Store();
            DataSet ds = brandBll.GetAllRecordList();
            List<Edge.SVA.Model.Brand> brandList = brandBll.DataTableToList(ds.Tables[0]);
            //英文
            List<BrandInfo> brandInfoList_en_us = new List<BrandInfo>();
            foreach (var brandItem in brandList)
            {
                BrandInfo bi = new BrandInfo();
                bi.Key = brandItem.BrandID.ToString();
                bi.Value = brandItem.BrandCode + "-" + brandItem.BrandName1;

                List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(brandItem.BrandID).Tables[0]);
                foreach (var cardTypeItem in cardTypeList)
                {
                    CardTypeInfo ct = new CardTypeInfo();
                    ct.Key = cardTypeItem.CardTypeID.ToString();
                    ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName1;

                    List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                    foreach (var cardGradeItem in cardGradList)
                    {
                        ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName1 });
                    }
                    bi.CardTypeInfos.Add(ct);
                }

                List<Edge.SVA.Model.CouponType> couponTypeList = couponTypeBll.DataTableToList(couponTypeBll.GetCouponTypes(brandItem.BrandID).Tables[0]);
                foreach (var couponTypeitem in couponTypeList)
                {
                    CouponTypeInfo cti = new CouponTypeInfo();
                    cti.Key = couponTypeitem.CouponTypeID.ToString();
                    cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName1;
                    bi.CouponTypeInfos.Add(cti);
                }

                List<Edge.SVA.Model.Store> storeList = storeBll.DataTableToList(storeBll.GetStores(brandItem.BrandID).Tables[0]);
                foreach (var storeitem in storeList)
                {
                    StoreInfo st = new StoreInfo();
                    st.Key = storeitem.StoreID.ToString();
                    st.Value = storeitem.StoreCode + "-" + storeitem.StoreName1;
                    bi.StoreInfos.Add(st);
                }
                brandInfoList_en_us.Add(bi);
            }
            //中文简体
            List<BrandInfo> brandInfoList_zh_cn = new List<BrandInfo>();
            foreach (var brandItem in brandList)
            {
                BrandInfo bi = new BrandInfo();
                bi.Key = brandItem.BrandID.ToString();
                bi.Value = brandItem.BrandCode + "-" + brandItem.BrandName2;

                List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(brandItem.BrandID).Tables[0]);
                foreach (var cardTypeItem in cardTypeList)
                {
                    CardTypeInfo ct = new CardTypeInfo();
                    ct.Key = cardTypeItem.CardTypeID.ToString();
                    ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName2;

                    List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                    foreach (var cardGradeItem in cardGradList)
                    {
                        ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName2 });
                    }
                    bi.CardTypeInfos.Add(ct);
                }

                List<Edge.SVA.Model.CouponType> couponTypeList = couponTypeBll.DataTableToList(couponTypeBll.GetCouponTypes(brandItem.BrandID).Tables[0]);
                foreach (var couponTypeitem in couponTypeList)
                {
                    CouponTypeInfo cti = new CouponTypeInfo();
                    cti.Key = couponTypeitem.CouponTypeID.ToString();
                    cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName2;
                    bi.CouponTypeInfos.Add(cti);
                }

                List<Edge.SVA.Model.Store> storeList = storeBll.DataTableToList(storeBll.GetStores(brandItem.BrandID).Tables[0]);
                foreach (var storeitem in storeList)
                {
                    StoreInfo st = new StoreInfo();
                    st.Key = storeitem.StoreID.ToString();
                    st.Value = storeitem.StoreCode + "-" + storeitem.StoreName2;
                    bi.StoreInfos.Add(st);
                }
                brandInfoList_zh_cn.Add(bi);
            }
            //中文繁体
            List<BrandInfo> brandInfoList_zh_hk = new List<BrandInfo>();
            foreach (var brandItem in brandList)
            {
                BrandInfo bi = new BrandInfo();
                bi.Key = brandItem.BrandID.ToString();
                bi.Value = brandItem.BrandCode + "-" + brandItem.BrandName3;

                List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(brandItem.BrandID).Tables[0]);
                foreach (var cardTypeItem in cardTypeList)
                {
                    CardTypeInfo ct = new CardTypeInfo();
                    ct.Key = cardTypeItem.CardTypeID.ToString();
                    ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName3;

                    List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                    foreach (var cardGradeItem in cardGradList)
                    {
                        ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName3 });
                    }
                    bi.CardTypeInfos.Add(ct);
                }

                List<Edge.SVA.Model.CouponType> couponTypeList = couponTypeBll.DataTableToList(couponTypeBll.GetCouponTypes(brandItem.BrandID).Tables[0]);
                foreach (var couponTypeitem in couponTypeList)
                {
                    CouponTypeInfo cti = new CouponTypeInfo();
                    cti.Key = couponTypeitem.CouponTypeID.ToString();
                    cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName3;
                    bi.CouponTypeInfos.Add(cti);
                }

                List<Edge.SVA.Model.Store> storeList = storeBll.DataTableToList(storeBll.GetStores(brandItem.BrandID).Tables[0]);
                foreach (var storeitem in storeList)
                {
                    StoreInfo st = new StoreInfo();
                    st.Key = storeitem.StoreID.ToString();
                    st.Value = storeitem.StoreCode + "-" + storeitem.StoreName3;
                    bi.StoreInfos.Add(st);
                }
                brandInfoList_zh_hk.Add(bi);
            }

            brandList_en_us = brandInfoList_en_us;
            brandList_zh_cn = brandInfoList_zh_cn;
            brandList_zh_hk = brandInfoList_zh_hk;

        }

        public List<BrandInfo> GetBrandInfoList(string lan)
        {
            List<BrandInfo> list = new List<BrandInfo>();
            switch (lan.ToLower())
            {
                case "en-us":
                    list = brandList_en_us;
                    break;
                case "zh-cn":
                    list = brandList_zh_cn;
                    break;
                case "zh-hk":
                    list = brandList_zh_hk;
                    break;
                default:
                    list = brandList_en_us;
                    break;
            }
            return list;
        }
        public List<BrandInfo> GetBrandInfoList(string username, string lan)
        {
            List<BrandInfo> brandInfoList = new List<BrandInfo>();
            DataSet ds = DBUtility.DbHelperSQL.Query(@"SELECT     Brand.BrandID, Brand.BrandCode, Brand.BrandName1, Brand.BrandName2, Brand.BrandName3, Store.StoreID, Store.StoreCode, 
                      Store.StoreName1, Store.StoreName2, Store.StoreName3
FROM         Store INNER JOIN
                      RelationUserStore ON Store.StoreID = RelationUserStore.StoreID INNER JOIN
                      Brand ON Store.BrandID = Brand.BrandID where RelationUserStore.UserName='" + username + "' order by Brand.BrandCode asc,Store.StoreCode asc");
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                switch (lan.ToLower())
                {
                    case "zh-cn":
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName2");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName2");
                            bi.StoreInfos.Add(si);
                        }
                        break;
                    case "zh-hk":
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName3");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName3");
                            bi.StoreInfos.Add(si);
                        }
                        break;
                    case "en-us":
                    default:
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName1");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName1");
                            bi.StoreInfos.Add(si);
                        }
                        break;
                }
                FillBrandInfo(brandInfoList, lan); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            }
            return brandInfoList;
        }
        public List<BrandInfo> GetAllBrandInfoList(string lan)
        {
            List<BrandInfo> brandInfoList = new List<BrandInfo>();
            DataSet ds = DBUtility.DbHelperSQL.Query(@"SELECT     TOP (100) PERCENT dbo.Brand.BrandID, dbo.Brand.BrandCode, dbo.Brand.BrandName1, dbo.Brand.BrandName2, dbo.Brand.BrandName3, dbo.Store.StoreID, 
                      dbo.Store.StoreCode, dbo.Store.StoreName1, dbo.Store.StoreName2, dbo.Store.StoreName3
FROM         dbo.Brand LEFT OUTER JOIN
                      dbo.Store ON dbo.Brand.BrandID = dbo.Store.BrandID
ORDER BY dbo.Brand.BrandCode, dbo.Store.StoreCode");
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                switch (lan.ToLower())
                {
                    case "zh-cn":
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName2");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            if (si.Key.Length > 0)
                            {
                                si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName2");
                                bi.StoreInfos.Add(si);
                            }
                        }
                        break;
                    case "zh-hk":
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName3");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            if (si.Key.Length > 0)
                            {
                                si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName3");
                                bi.StoreInfos.Add(si);
                            }

                        }
                        break;
                    case "en-us":
                    default:
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName1");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            if (si.Key.Length > 0)
                            {
                                si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName1");
                                bi.StoreInfos.Add(si);
                            }
                        }
                        break;
                }
                FillBrandInfo(brandInfoList, lan); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
            }
            return brandInfoList;
        }

        public List<BrandInfo> GetBrandInfoListByBrandUserID(int userid, string lan)
        {
            //            List<BrandInfo> brandInfoList = new List<BrandInfo>();
            //            DataSet ds = DBUtility.DbHelperSQL.Query(@"SELECT     TOP (100) PERCENT dbo.Brand.BrandID, dbo.Brand.BrandName1, dbo.Brand.BrandName2, dbo.Brand.BrandName3, dbo.Brand.BrandCode
            //FROM         dbo.Brand INNER JOIN
            //                      dbo.RelationRoleBrand ON dbo.Brand.BrandID = dbo.RelationRoleBrand.BrandID INNER JOIN
            //                      dbo.Accounts_Users ON dbo.RelationRoleBrand.RoleID = dbo.Accounts_Users.UserID
            //WHERE     (dbo.Accounts_Users.UserName = '"+username+@"')
            //ORDER BY dbo.Brand.BrandCode ");

            List<BrandInfo> brandInfoList = new List<BrandInfo>();
            DataSet ds = DBUtility.DbHelperSQL.Query(@"SELECT     TOP (100) PERCENT dbo.Brand.BrandID, dbo.Brand.BrandCode, dbo.Brand.BrandName1, dbo.Brand.BrandName2, dbo.Brand.BrandName3, dbo.Store.StoreID, 
                      dbo.Store.StoreCode, dbo.Store.StoreName1, dbo.Store.StoreName2, dbo.Store.StoreName3
FROM         dbo.Brand INNER JOIN
                      dbo.RelationRoleBrand ON dbo.Brand.BrandID = dbo.RelationRoleBrand.BrandID LEFT OUTER JOIN
                      dbo.Store ON dbo.Brand.BrandID = dbo.Store.BrandID
WHERE     (dbo.RelationRoleBrand.RoleID = " + userid.ToString() + @")
ORDER BY dbo.Brand.BrandCode, dbo.Store.StoreCode");
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                switch (lan.ToLower())
                {
                    case "zh-cn":
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName2");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            if (si.Key.Length > 0)
                            {
                                si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName2");
                                bi.StoreInfos.Add(si);
                            }
                        }
                        break;
                    case "zh-hk":
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName3");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            if (si.Key.Length > 0)
                            {
                                si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName3");
                                bi.StoreInfos.Add(si);
                            }

                        }
                        break;
                    case "en-us":
                    default:
                        foreach (DataRow item in dt.Rows)
                        {
                            string brandid = GetColumnValue(item, "BrandID");

                            BrandInfo bi = brandInfoList.Find(m => m.Key == brandid);
                            if (bi == null)
                            {
                                bi = new BrandInfo();
                                bi.Key = brandid;
                                bi.Value = GetColumnValue(item, "BrandCode") + "-" + GetColumnValue(item, "BrandName1");
                                brandInfoList.Add(bi);
                            }
                            StoreInfo si = new StoreInfo();
                            si.Key = GetColumnValue(item, "StoreID");
                            if (si.Key.Length > 0)
                            {
                                si.Value = GetColumnValue(item, "StoreCode") + "-" + GetColumnValue(item, "StoreName1");
                                bi.StoreInfos.Add(si);
                            }
                        }
                        break;
                }
                FillBrandInfo(brandInfoList,lan); //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券

            }
            return brandInfoList;
        }


        public List<BrandInfo> GetCardTypeListByBrand(string lan)
        {
            Edge.SVA.BLL.Brand brandBll = new Edge.SVA.BLL.Brand();
            Edge.SVA.BLL.CardType cardTypeBll = new Edge.SVA.BLL.CardType();
            Edge.SVA.BLL.CardGrade cardGradeBll = new Edge.SVA.BLL.CardGrade();
            DataSet ds = brandBll.GetAllRecordList();
            List<Edge.SVA.Model.Brand> brandList = brandBll.DataTableToList(ds.Tables[0]);
            List<BrandInfo> brandInfoList = new List<BrandInfo>();

            //            DataSet ds = DBUtility.DbHelperSQL.Query(@"SELECT  Brand.BrandID, Brand.BrandCode, Brand.BrandName1, Brand.BrandName2, Brand.BrandName3,
            //                                                        CardType.CardTypeID,CardTypeCode,CardTypeName1,CardTypeName2,CardTypeName3,
            //                                                        CardGradeID,CardGradeCode,CardGradeName1,CardGradeName2,CardGradeName3
            //                                                        FROM CardType INNER JOIN Brand ON CardType.BrandID = Brand.BrandID 
            //                                                        left join dbo.CardGrade on CardType.CardTypeID=CardGrade.CardTypeID
            //                                                        order by Brand.BrandCode asc,CardType.CardTypeCode asc");
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                switch (lan.ToLower())
                {
                    case "zh-cn":
                        foreach (var brandItem in brandList)
                        {
                            BrandInfo bi = new BrandInfo();
                            bi.Key = brandItem.BrandID.ToString();
                            bi.Value = brandItem.BrandCode + "-" + brandItem.BrandName2;

                            List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(brandItem.BrandID).Tables[0]);
                            foreach (var cardTypeItem in cardTypeList)
                            {
                                CardTypeInfo ct = new CardTypeInfo();
                                ct.Key = cardTypeItem.CardTypeID.ToString();
                                ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName2;

                                List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                                if (cardGradList.Count > 0)
                                {

                                    foreach (var cardGradeItem in cardGradList)
                                    {
                                        ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName2 });
                                    }
                                    bi.CardTypeInfos.Add(ct);
                                }
                            }
                            brandInfoList.Add(bi);
                        }
                        break;
                    case "zh-hk":
                        foreach (var brandItem in brandList)
                        {
                            BrandInfo bi = new BrandInfo();
                            bi.Key = brandItem.BrandID.ToString();
                            bi.Value = brandItem.BrandCode + "-" + brandItem.BrandName3;

                            List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(brandItem.BrandID).Tables[0]);
                            foreach (var cardTypeItem in cardTypeList)
                            {
                                CardTypeInfo ct = new CardTypeInfo();
                                ct.Key = cardTypeItem.CardTypeID.ToString();
                                ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName3;

                                List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                                if (cardGradList.Count > 0)
                                {

                                    foreach (var cardGradeItem in cardGradList)
                                    {
                                        ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName3 });
                                    }
                                    bi.CardTypeInfos.Add(ct);
                                }
                            }
                            brandInfoList.Add(bi);
                        }
                        break;
                    case "en-us":
                    default:
                        foreach (var brandItem in brandList)
                        {
                            BrandInfo bi = new BrandInfo();
                            bi.Key = brandItem.BrandID.ToString();
                            bi.Value = brandItem.BrandCode + "-" + brandItem.BrandName1;

                            List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(brandItem.BrandID).Tables[0]);
                            foreach (var cardTypeItem in cardTypeList)
                            {
                                CardTypeInfo ct = new CardTypeInfo();
                                ct.Key = cardTypeItem.CardTypeID.ToString();
                                ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName1;

                                List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                                if (cardGradList.Count > 0)
                                {
                                    foreach (var cardGradeItem in cardGradList)
                                    {
                                        ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName1 });
                                    }
                                    bi.CardTypeInfos.Add(ct);
                                }
                            }
                            brandInfoList.Add(bi);
                        }
                        break;
                }

            }
            return brandInfoList;
        }

        private static string GetColumnValue(DataRow item, string name)
        {
            if (item[name] == null)
            {
                return string.Empty;
            }
            return item[name].ToString();
        }
        public void Refresh()
        {
            LoadDataFromDB();
        }

        //Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        private void FillBrandInfo(List<BrandInfo> brandList, string lan)
        {
            Edge.SVA.BLL.CardType cardTypeBll = new Edge.SVA.BLL.CardType();
            Edge.SVA.BLL.CardGrade cardGradeBll = new Edge.SVA.BLL.CardGrade();
            Edge.SVA.BLL.CouponType couponTypeBll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.BLL.CouponTypeNature couponTypeNatureBll = new Edge.SVA.BLL.CouponTypeNature();
            List<BrandInfo> brandInfoList_en_us = new List<BrandInfo>();
            foreach (var brandItem in brandList)
            {
                List<Edge.SVA.Model.CardType> cardTypeList = cardTypeBll.DataTableToList(cardTypeBll.GetCardTypeList(Convert.ToInt32(brandItem.Key)).Tables[0]);
                foreach (var cardTypeItem in cardTypeList)
                {
                    CardTypeInfo ct = new CardTypeInfo();
                    ct.Key = cardTypeItem.CardTypeID.ToString();
                    switch (lan.ToLower())
                    {
                        case "en-us":
                            ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName1;
                            break;
                        case "zh-cn":
                            ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName2;
                            break;
                        case "zh-hk":
                            ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName3;
                            break;
                        default:
                            ct.Value = cardTypeItem.CardTypeCode + "-" + cardTypeItem.CardTypeName1;
                            break;
                    }

                    List<Edge.SVA.Model.CardGrade> cardGradList = cardGradeBll.DataTableToList(cardGradeBll.GetCardGrads(cardTypeItem.CardTypeID).Tables[0]);
                    foreach (var cardGradeItem in cardGradList)
                    {
                        CardGradeInfo cg = new CardGradeInfo();
                        cg.Key = cardGradeItem.CardGradeID.ToString();
                        switch (lan.ToLower())
                        {
                            case "en-us":
                                ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName1 });
                                break;
                            case "zh-cn":
                                ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName2 });
                                break;
                            case "zh-hk":
                                ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName3 });
                                break;
                            default:
                                ct.CardGrades.Add(new CardGradeInfo() { Key = cardGradeItem.CardGradeID.ToString(), Value = cardGradeItem.CardGradeCode + "-" + cardGradeItem.CardGradeName1 });
                                break;
                        }
                    }
                    brandItem.CardTypeInfos.Add(ct);
                }

                List<Edge.SVA.Model.CouponType> couponTypeList = couponTypeBll.DataTableToList(couponTypeBll.GetCouponTypes(Convert.ToInt32(brandItem.Key)).Tables[0]);
                foreach (var couponTypeitem in couponTypeList)
                {
                    CouponTypeInfo cti = new CouponTypeInfo();
                    cti.Key = couponTypeitem.CouponTypeID.ToString();
                    switch (lan.ToLower())
                    {
                        case "en-us":
                            cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName1;
                            break;
                        case "zh-cn":
                            cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName2;
                            break;
                        case "zh-hk":
                            cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName3;
                            break;
                        default:
                            cti.Value = couponTypeitem.CouponTypeCode + "-" + couponTypeitem.CouponTypeName1;
                            break;
                    }
                    brandItem.CouponTypeInfos.Add(cti);
                }
            }
        }
        //End
    }
}
