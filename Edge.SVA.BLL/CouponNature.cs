﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model;

namespace Edge.SVA.BLL
{
     public partial class CouponNature
    {
         public bool isHasCouponNatureCode(string code, int couponTypeID)
        {
            if (couponTypeID != 0)
            {
                Edge.SVA.Model.CouponNature model = new Edge.SVA.BLL.CouponNature().GetModel(couponTypeID);

                if (model == null) return false;

                if (!string.IsNullOrEmpty(model.CouponNatureCode))
                {
                    if (code.ToUpper().Trim() == model.CouponNatureCode.ToUpper().Trim())
                        return false;
                }
            }

            int count = new Edge.SVA.BLL.CouponType().GetCount("CouponNatureCode='" + code + "'");
            return count > 0;
        }

        private string SessionChangeBrandIDsStr(string strWhere)
        {
            string str = SessionInfo.BrandIDsStr;
            if (!String.IsNullOrEmpty(str))
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " BrandID in " + str;
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and BrandID in " + str;
                    }
                    else
                    {
                        strWhere = strs[0] + " and BrandID in " + str + strs[1];
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strWhere))
                {
                    strWhere = " 1!=1 ";
                }
                else
                {
                    string[] strs = SqlWhereUtil.SplitStringByOrderBy(strWhere);
                    if (strs.Length <= 1)
                    {
                        strWhere = strWhere + " and 1!=1 ";
                    }
                    else
                    {
                        strWhere = strs[0] + " and 1!=1 " + strs[1];
                    }
                }
            }
            return strWhere;
        }

        /// <summary>
        /// 获取总页数不加权限
        /// </summary>
        public int GetCountUnlimited(string strWhere)
        {
            return dal.GetCount(strWhere);
        }
    }
}
