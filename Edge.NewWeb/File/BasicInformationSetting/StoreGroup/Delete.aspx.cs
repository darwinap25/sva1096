﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.BasicInformationSetting.StoreGroup
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, "", FineUI.MessageBoxIcon.Warning, "location.href='List.aspx'");
                    return;
                }
                logger.WriteOperationLog(this.PageName, " Delete " + ids);
                foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (string.IsNullOrEmpty(id)) continue;
                    string msg = "";
                    if (!Tools.DALTool.isCanDeleteStoreNature(Tools.ConvertTool.ToInt(id.Trim()), ref msg))
                    {
                        FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteIsUsed, "", FineUI.MessageBoxIcon.Warning, "location.href='List.aspx'");
                        return;
                    }
                    Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.StoreGroup>(Tools.ConvertTool.ToInt(id));

                }
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Delete\t Code:" + ids);
                FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Information, "location.href='List.aspx'");
            }
        }
    }
}