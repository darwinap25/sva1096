﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.Departments;
using System.IO;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.Departments
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Department, Edge.SVA.Model.Department>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        DepartmentsController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                ControlTool.BindBrand(BrandID);

                SVASessionInfo.DepartmentsController = null;
            }
            controller = SVASessionInfo.DepartmentsController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.DepartCode);
                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.DepartPicFile;
                    //存在会员相片时不需要验证此字段
                    if (!string.IsNullOrEmpty(controller.ViewModel.MainTable.DepartPicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = controller.ViewModel.MainTable.DepartPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
                    controller.ViewModel.MainTable.DepartNote = clearTool.ClearSource(this.DepartNote.Text).Replace("\r\n", "");
                }
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (ValidateForm())
            {
                if (controller.ViewModel.MainTable != null)
                {
                    if (!string.IsNullOrEmpty(this.DepartPicFile.ShortFileName) && this.FormLoad.Hidden == false)
                    {
                        if (!ValidateImg(this.DepartPicFile.FileName))
                        {
                            return;
                        }
                        controller.ViewModel.MainTable.DepartPicFile = this.DepartPicFile.SaveToServer("Files/Departments");
                    }
                    else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                    {
                        if (!ValidateImg(this.uploadFilePath.Text))
                        {
                            return;
                        }
                        controller.ViewModel.MainTable.DepartPicFile = this.uploadFilePath.Text;
                    }
                }
                ExecResult er = controller.Update();
                if (er.Success)
                {
                    if (this.FormReLoad.Hidden == true && string.IsNullOrEmpty(controller.ViewModel.MainTable.DepartPicFile))
                    {
                        DeleteFile(this.uploadFilePath.Text);
                    }
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Departments Update\t Code:" + controller.ViewModel.MainTable.DepartCode.ToString());
                    CloseAndPostBack();
                }
                else
                {
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Departments Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.DepartCode);
                    ShowUpdateFailed();
                }
            }
        }
        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        public bool ValidateForm()
        {
            if (string.IsNullOrEmpty(this.DepartNote.Text) || this.DepartNote.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.DepartNoteCannotBeEmpty);
                return false;
            }

            return true;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}