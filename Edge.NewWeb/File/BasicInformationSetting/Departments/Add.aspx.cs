﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.Departments;
using System.IO;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.Departments
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Department, Edge.SVA.Model.Department>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        DepartmentsController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                ControlTool.BindBrand(BrandID);

                SVASessionInfo.DepartmentsController = null;
            }
            controller = SVASessionInfo.DepartmentsController;
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            controller.ViewModel.MainTable = this.GetAddObject();
            if (ValidateForm())
            {
                if (controller.ViewModel.MainTable != null)
                {
                    if (!ValidateImg(this.DepartPicFile.FileName))
                    {
                        return;
                    }
                    controller.ViewModel.MainTable.DepartPicFile = this.DepartPicFile.SaveToServer("Files/Departments");

                    HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
                    controller.ViewModel.MainTable.DepartNote = clearTool.ClearSource(this.DepartNote.Text).Replace("\r\n", "");
                }

                ExecResult er = controller.Add();
                if (er.Success)
                {
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add Departments Success Code:" + controller.ViewModel.MainTable.DepartCode);
                    CloseAndRefresh();
                }
                else
                {
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add Departments Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.DepartCode);
                    ShowAddFailed();
                }
            }
        }
        public bool ValidateForm()
        {
            if (string.IsNullOrEmpty(this.DepartNote.Text) || this.DepartNote.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.DepartNoteCannotBeEmpty);
                return false;
            }

            return true;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}