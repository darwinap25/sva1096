﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.Departments;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.Departments
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.Department, Edge.SVA.Model.Department>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        DepartmentsController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);

                SVASessionInfo.DepartmentsController = null;
            }
            controller = SVASessionInfo.DepartmentsController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.DepartCode);
                if (controller.ViewModel.MainTable != null)
                {
                    Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(controller.ViewModel.MainTable.BrandID.GetValueOrDefault());
                    this.BrandID.Text = brand == null ? "" : brand.BrandCode + "-" + DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);

                    this.btnPreview.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.DepartPicFile) ? true : false;//没有照片时不显示查看按钮(Len)

                    this.uploadFilePath.Text = controller.ViewModel.MainTable.DepartPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                }
            }
        }
    }
}