﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.PasswordRulesSetting;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.PasswordRulesSetting
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        PasswordRulesSettingController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.PasswordRulesSettingController = null;
            }
            controller = SVASessionInfo.PasswordRulesSettingController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!hasRight)
            {
                return;
            }
            PWDStructure_SelectedIndexChanged(null, null);
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            if (this.MemberPWDRule.SelectedValue == "1" )
            {
                System.Data.DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("MemberPWDRule = 1");
                if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["PasswordRuleID"].ToString() != Request.Params["id"])
                {
                    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistMemberRule, FineUI.MessageBoxIcon.Warning);
                    return;
                }
            }

            //Edge.SVA.Model.PasswordRule item = this.GetUpdateObject();
            //if (Tools.DALTool.isHasPasswordRuleCode(this.PasswordRuleCode.Text.Trim(), item.PasswordRuleID))
            //{
            //    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistPasswordSettingCode, FineUI.MessageBoxIcon.Warning);
            //    return;
            //}
            //Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            //if (bll.GetCount(string.Format("PWDSecurityLevel = {0} and PasswordRuleID <> {1}", Tools.ConvertTool.ConverType<int>(this.PWDSecurityLevel.Text), Tools.ConvertTool.ConverType<int>(Request.Params["id"]))) > 0)
            //{
            //    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistPasswordSecurity, FineUI.MessageBoxIcon.Warning);
            //    return;
            //}
            //if (item != null)
            //{
            //    item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = DateTime.Now;
            //    item.PasswordRuleCode = item.PasswordRuleCode.ToUpper();
            //    item.PWDMinLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMinLength.GetValueOrDefault();
            //    item.PWDMaxLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMaxLength.GetValueOrDefault();
            //}
            //if (DALTool.Update<Edge.SVA.BLL.PasswordRule>(item))
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Update\t Code:" + item.PasswordRuleCode);
            //    CloseAndPostBack();
            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Update\t Code:" + item == null ? "No Data" : item.PasswordRuleCode);
            //    ShowUpdateFailed();
            //}

            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (Tools.DALTool.isHasPasswordRuleCode(this.PasswordRuleCode.Text.Trim(), controller.ViewModel.MainTable.PasswordRuleID))
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.ExistPasswordSettingCode, FineUI.MessageBoxIcon.Warning);
                return;
            }
            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            if (bll.GetCount(string.Format("PWDSecurityLevel = {0} and PasswordRuleID <> {1}", Tools.ConvertTool.ConverType<int>(this.PWDSecurityLevel.Text), Tools.ConvertTool.ConverType<int>(Request.Params["id"]))) > 0)
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.ExistPasswordSecurity, FineUI.MessageBoxIcon.Warning);
                return;
            }
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.PasswordRuleCode = controller.ViewModel.MainTable.PasswordRuleCode.ToUpper();
                controller.ViewModel.MainTable.PWDMinLength = controller.ViewModel.MainTable.PWDStructure.GetValueOrDefault() == 0 ? 0 : controller.ViewModel.MainTable.PWDMinLength.GetValueOrDefault();
                controller.ViewModel.MainTable.PWDMaxLength = controller.ViewModel.MainTable.PWDStructure.GetValueOrDefault() == 0 ? 0 : controller.ViewModel.MainTable.PWDMaxLength.GetValueOrDefault();
            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Update\t Code:" + controller.ViewModel.MainTable.PasswordRuleCode);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.PasswordRuleCode);
                ShowUpdateFailed();
            }
        }

        protected void PWDStructure_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.PWDStructure.SelectedValue == "0")
            {
                PWDMinLength.Text = "0";
                PWDMaxLength.Text = "0";
                PWDMinLength.Enabled = false;
                PWDMaxLength.Enabled = false;
            }
            else
            {
                PWDMinLength.Enabled =true;
                PWDMaxLength.Enabled = true;          
            }
        }
    }
}
