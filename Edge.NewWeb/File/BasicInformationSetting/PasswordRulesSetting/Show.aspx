﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.PasswordRulesSetting.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                   
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="PasswordRuleCode" runat="server" Label="密码规则编号："  >
                            </ext:Label>
                            <ext:Label ID="Name1" runat="server" Label="描述：" >
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="Name2" runat="server" Label="其他描述1：">
                            </ext:Label>
                            <ext:Label ID="Name3" runat="server" Label="其他描述2：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow3" runat="server">
                        <Items>
                            <ext:Label ID="PWDStructureView" runat="server" Label="密码构成：">
                            </ext:Label>
                            <ext:Label ID="PWDMinLength" runat="server" Label="密码最小长度："  >
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow4" runat="server">
                        <Items>
                            <ext:Label ID="PWDMaxLength" runat="server" Label="密码最大长度：">
                            </ext:Label>
                            <ext:Label ID="PWDSecurityLevel" runat="server" Label="密码安全级别：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow5" runat="server">
                        <Items>
                            <ext:Label ID="PWDValidDays" runat="server" Label="密码有效天数：">
                            </ext:Label>
                            <ext:Label ID="PWDValidDaysUnitView" runat="server" Label="密码有效天数的单位：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow6" runat="server">
                        <Items>
                             <ext:Label ID="MemberPWDRuleView" runat="server" Label="是否会员密码规则：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                   <ext:FormRow ID="FormRow7" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                            </ext:Label>
                            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow8" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
                            </ext:Label>
                            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>

            <ext:DropDownList ID="PWDStructure" runat="server" Label="密码构成：">
                <ext:ListItem Value="0" Text="No Requirement"></ext:ListItem>
                <ext:ListItem Value="1" Text="Numbers"></ext:ListItem>
                <ext:ListItem Value="2" Text="Alphabets"></ext:ListItem>
                <ext:ListItem Value="3" Text="Numbers + Alphabets"></ext:ListItem>
                <ext:ListItem Value="4" Text="Numbers +Alphabets +Symbols"></ext:ListItem>
            </ext:DropDownList>

            <ext:DropDownList ID="PWDValidDaysUnit" runat="server" Label="密码有效天数的单位：">
                <ext:ListItem Text="" Value="" Selected="True"></ext:ListItem>
                <ext:ListItem Text="年" Value="1"></ext:ListItem>
                <ext:ListItem Text="月" Value="2"></ext:ListItem>
                <ext:ListItem Text="星期" Value="3"></ext:ListItem>
                <ext:ListItem Text="天" Value="4"></ext:ListItem>
            </ext:DropDownList>

            <ext:RadioButtonList ID="MemberPWDRule" runat="server" Label="是否会员密码规则：">
                <ext:RadioItem Text="是" Value="1" />
                <ext:RadioItem Text="否" Value="0" Selected="true" />
            </ext:RadioButtonList>

        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
