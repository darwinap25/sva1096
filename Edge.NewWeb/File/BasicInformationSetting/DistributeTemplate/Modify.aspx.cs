﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.DistributeTemplate;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;

namespace Edge.Web.File.BasicInformationSetting.DistributeTemplate
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.DistributeTemplate, Edge.SVA.Model.DistributeTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        DistributeTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.DistributeTemplateController = null;
            }
            controller = SVASessionInfo.DistributeTemplateController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //if (Model != null)
                //{
                //    this.uploadFilePath.Text = Model.TemplateFile;
                //    //存在文件时不需要验证此字段
                //    if (!string.IsNullOrEmpty(Model.TemplateFile))
                //    {
                //        this.FormLoad.Hidden = true;
                //        this.FormReLoad.Hidden = false;
                //        this.btnBack.Hidden = false;
                //    }
                //    else
                //    {
                //        this.FormLoad.Hidden = false;
                //        this.FormReLoad.Hidden = true;
                //        this.btnBack.Hidden = true;
                //    }
                //}
                controller.LoadViewModel(Model.DistributionID);
                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.TemplateFile;
                    //存在文件时不需要验证此字段
                    if (!string.IsNullOrEmpty(controller.ViewModel.MainTable.TemplateFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }
                }
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            controller.ViewModel.MainTable = this.GetUpdateObject();

            string message = controller.ValidataObject(this.DistributionCode.Text.Trim(), controller.ViewModel.MainTable.DistributionID);
            if (message!="")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.DistributionCode = controller.ViewModel.MainTable.DistributionCode.ToUpper();

                if (!string.IsNullOrEmpty(this.TemplateFile.ShortFileName) && this.FormLoad.Hidden == false)
                {
                    if (!ValidateFile(this.TemplateFile.FileName))
                    {
                        return;
                    }
                    controller.ViewModel.MainTable.TemplateFile = this.TemplateFile.SaveToServer("Files/DistributeTemplate");
                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateFile(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    controller.ViewModel.MainTable.TemplateFile = this.uploadFilePath.Text;
                }

            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                if (this.FormReLoad.Hidden == true && string.IsNullOrEmpty(controller.ViewModel.MainTable.TemplateFile))
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Update\t Code:" + controller.ViewModel.MainTable.DistributionCode);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.DistributionCode);
                ShowUpdateFailed();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("MsgTemplate download ", " filename: " + fileName);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteErrorLog("MsgTemplate download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.DistributeTemplateFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.DistributeTemplateFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
