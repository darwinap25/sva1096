﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.DistributeTemplate;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.DistributeTemplate
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.DistributeTemplate, Edge.SVA.Model.DistributeTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        DistributeTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);

                SVASessionInfo.DistributeTemplateController = null;
            }
            controller = SVASessionInfo.DistributeTemplateController;
           
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(this.Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(this.Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);
                //if (Model != null)
                //{
                //    this.uploadFilePath.Text = Model.TemplateFile;

                //    //this.btnExport.OnClientClick = WindowPic.GetShowReference("../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                //    this.btnExport.Hidden = string.IsNullOrEmpty(Model.TemplateFile) ? true : false;//没有文件时不显示查看按钮(Len)
                //}

                controller.LoadViewModel(Model.DistributionID);
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);
                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.TemplateFile;

                    this.btnExport.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.TemplateFile) ? true : false;//没有文件时不显示查看按钮(Len)
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CuponGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("CardCaede Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CuponGrade download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }
    }
}
