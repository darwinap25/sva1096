﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.Sponsor;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.Sponsor
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Sponsor, Edge.SVA.Model.Sponsor>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        SponsorController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.SponsorController = null;
            }
            controller = SVASessionInfo.SponsorController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.SponsorID);
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);

            }
        }
    }
}