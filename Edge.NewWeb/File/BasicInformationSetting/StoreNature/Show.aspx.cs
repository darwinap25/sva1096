﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.StoreNature;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.StoreNature
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreType,Edge.SVA.Model.StoreType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        StoreNatureController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Show");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.StoreNatureController = null;
            }
            controller = SVASessionInfo.StoreNatureController;
          
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                controller.LoadViewModel(Model.StoreTypeID);
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);

            }
        }
        
    }
}
