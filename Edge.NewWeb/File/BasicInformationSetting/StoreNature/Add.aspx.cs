﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.StoreNature;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.StoreNature
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreType,Edge.SVA.Model.StoreType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        StoreNatureController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.StoreNatureController = null;
            }
            controller = SVASessionInfo.StoreNatureController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            //if (Tools.DALTool.isHasStoreTypeCode(this.StoreTypeCode.Text.Trim(), 0))
            //{
            //    this.ShowWarning(Resources.MessageTips.ExistStoreGroupCode);
            //    return;
            //}

            //Edge.SVA.Model.StoreType item = this.GetAddObject();
            //if (item != null)
            //{
            //    item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = System.DateTime.Now;
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.StoreTypeCode = item.StoreTypeCode.ToUpper();
            //}

            //if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.StoreType>(item) > 0)
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + item.StoreTypeCode);
            //    this.CloseAndRefresh();
            //}
            //else
            //{
            //    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + item == null ? "No Data" : item.StoreTypeCode);
            //    this.ShowAddFailed();
            //}

            string message = controller.ValidataObject(this.StoreTypeCode.Text.Trim(), 0);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.StoreTypeCode = controller.ViewModel.MainTable.StoreTypeCode.ToUpper();
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + controller.ViewModel.MainTable.StoreTypeCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.StoreTypeCode);
                ShowAddFailed();
            }
         
        }
    }
}
