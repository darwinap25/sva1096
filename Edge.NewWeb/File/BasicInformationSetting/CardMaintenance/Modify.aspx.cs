﻿using System;
using System.Web.UI;
using System.Data;
using Edge.Web.DAO;
using Edge.SVA.Model.Domain.SVA;

namespace Edge.Web.File.BasicInformationSetting.CardMaintenance
{
    public partial class Modify : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                int id;

                id = int.Parse(Server.UrlDecode(Request.QueryString["ID"]));

                this.FillFormData(id);
            }
        }


        private void FillFormData(int id)
        {
            
            DataSet ds = new DataSet();

            ds = GetCardValidityValues(id);

            if (ds.Tables[0].Rows.Count > 0)
            {
                txtID.Text = ds.Tables[0].Rows[0]["ID"].ToString();
                ddlRRCTypeOfApplication.SelectedValue = ds.Tables[0].Rows[0]["RRCTypeOfApplicationCode"].ToString();
                ddlRCCustomeTier.SelectedValue = ds.Tables[0].Rows[0]["RRCCustomerTierCode"].ToString();
                ddlExpiryDateField.SelectedValue = ds.Tables[0].Rows[0]["ExpiryDateField"].ToString().Substring(0, 1);
                txtCardValidityInYears.Text = ds.Tables[0].Rows[0]["CardValidityInYears"].ToString();
                ddlWithCondition.SelectedValue = ValidateFieldContent(ds.Tables[0].Rows[0]["WithCondition"].ToString());
                ddlConditionField1.SelectedValue = ValidateFieldContent(ds.Tables[0].Rows[0]["ConditionField1"].ToString()).Substring(0,1);
                //ddlConditionField2.SelectedValue = ValidateFieldContent(ds.Tables[0].Rows[0]["ConditionField2"].ToString()).Substring(0, 1);
                txtConditionalExpiryPeriod.Text = ds.Tables[0].Rows[0]["ConditionalExpiryPeriodInMonths"].ToString();
                ddlOperation.SelectedValue = ValidateFieldContent(ds.Tables[0].Rows[0]["Operation"].ToString());
                ddlLogicalOperator.SelectedValue = ValidateFieldContent(ds.Tables[0].Rows[0]["LogicalOperator"].ToString());

                if (ddlWithCondition.SelectedValue != "1")
                {
                    ddlConditionField1.SelectedValue = "";
                    ddlOperation.SelectedValue = "";
                    txtConditionalExpiryPeriod.Text = "0";
                    ddlLogicalOperator.SelectedValue = "";


                    ddlConditionField1.Enabled = false;
                    ddlOperation.Enabled = false;
                    txtConditionalExpiryPeriod.Enabled = false;
                    ddlLogicalOperator.Enabled = false;
                }

                if (ddlRRCTypeOfApplication.SelectedValue == "1")
                {
                    rdEndOfMonth.Enabled = true;
                    rdEndOfMonth.SelectedValue = ds.Tables[0].Rows[0]["EndOfMonth"].ToString();
                }
                else
                {
                    rdEndOfMonth.Enabled = false;
                    rdEndOfMonth.SelectedValue = "0";
                }
            }

        }

        public string ValidateFieldContent(string FieldName)
        {
            string fnReturnValue = "";
            if (string.IsNullOrEmpty(FieldName))
            {
                fnReturnValue = "0";

                //logger.Debug(FieldName + " is empty.");
            }
            else
            {
                fnReturnValue = FieldName;
            }

            return fnReturnValue;

        }

        private DataSet GetCardValidityValues(int id)
        {


            CardValidityMaintenance vo = new CardValidityMaintenance();
            CardMaintenanceDAO dao = new CardMaintenanceDAO();

            vo.Id = id;

            return dao.GetCardValidityValues(vo);

        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndRefresh();
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                CardValidityMaintenance vo = new CardValidityMaintenance();
                CardMaintenanceDAO dao = new CardMaintenanceDAO();

                vo.Id = int.Parse(txtID.Text);
                vo.RRCTypeOfApplicationCode = int.Parse(ddlRRCTypeOfApplication.SelectedValue);
                vo.RRCCustomerTierCode = int.Parse(ddlRCCustomeTier.SelectedValue);
                vo.CardValidityInYears = int.Parse(txtCardValidityInYears.Text);
                vo.ExpiryDateField = ddlExpiryDateField.SelectedText;
                vo.WithCondition = int.Parse(ddlWithCondition.SelectedValue);
                vo.ConditionField1 = ddlConditionField1.SelectedText;
                vo.ConditionField2 = "";// ddlConditionField2.SelectedText;
                vo.Operation = ddlOperation.SelectedValue;
                vo.LogicalOperator = ddlLogicalOperator.SelectedValue;
                vo.ConditionalExpiryPeriodInMonths = int.Parse(txtConditionalExpiryPeriod.Text);

                if (vo.RRCTypeOfApplicationCode == 1)
                {
                    vo.EndOfMonth = int.Parse(rdEndOfMonth.SelectedValue);
                }
                else
                {
                    vo.EndOfMonth = 0;
                }

                string result = dao.UpdateCardValidityInYears(vo);

                if (result == "0") //success
                {
                    //Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Segment Success Code:" + this.ImportMemberSegmentNumber.Text);
                    CloseAndRefresh();
                }
                else
                {
                    //Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Segment Failed Code:" + "No Data:" + this.ImportMemberSegmentNumber.Text);
                    ShowAddFailed();
                }
            }
        }

        protected void ddlRRCTypeOfApplication_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRRCTypeOfApplication.SelectedText == "NEW")
            {
                rdEndOfMonth.Enabled = true;
                rdEndOfMonth.SelectedIndex = 0;
            }
            else
            {
                rdEndOfMonth.Enabled = false;
                rdEndOfMonth.SelectedIndex = 1;
            }
        }

        protected void ddlWithCondition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlWithCondition.SelectedText == "YES")
            {
                ddlConditionField1.Enabled = true;
                //ddlConditionField2.Enabled = true;
                ddlOperation.Enabled = true;
                txtConditionalExpiryPeriod.Enabled = true;
                ddlLogicalOperator.Enabled = true;

            }
            else
            {
                ddlConditionField1.Enabled = false;
                //ddlConditionField2.Enabled = false;
                ddlOperation.Enabled = false;
                txtConditionalExpiryPeriod.Enabled = false;
                ddlLogicalOperator.Enabled = false;

                ddlConditionField1.SelectedValue = "";
                ddlOperation.SelectedValue = "";
                txtConditionalExpiryPeriod.Text = "0";
                ddlLogicalOperator.SelectedValue = "";
            }
        }
        private bool ValidateInput()
        {
            bool fnReturnValue = false;

            if (!IsNumeric(txtCardValidityInYears.Text))
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> Card Validity is not a number",FineUI.MessageBoxIcon.Error);
            }
            else if (string.IsNullOrEmpty(ddlRRCTypeOfApplication.SelectedText))
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> RRC Type of Application cannot be balnk", FineUI.MessageBoxIcon.Error);
            }
            else if (ddlRRCTypeOfApplication.SelectedText == "-------")
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> RRC Type of Application cannot be balnk", FineUI.MessageBoxIcon.Error);
            }
            else if (string.IsNullOrEmpty(ddlRCCustomeTier.SelectedText))
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> RRC Customer Tier cannot be balnk", FineUI.MessageBoxIcon.Error);
            }
            else if (ddlRCCustomeTier.SelectedText == "-------")
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> RRC Customer Tier cannot be balnk", FineUI.MessageBoxIcon.Error);
            }
            else if (ddlExpiryDateField.SelectedText == "-------")
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> Expiry Date cannot be blank",FineUI.MessageBoxIcon.Error);
            }
            else if (string.IsNullOrEmpty(ddlExpiryDateField.SelectedText))
            {
                FineUI.Alert.ShowInTop("<b>Save Failed !</b> Expiry Date cannot be blank", FineUI.MessageBoxIcon.Error);
            }
            else if (ddlWithCondition.SelectedText == "YES")
            {
                if (ddlConditionField1.SelectedText == "-------")
                {
                    FineUI.Alert.ShowInTop("<b>Save Failed !</b> Condition Field 1 cannot be blank", FineUI.MessageBoxIcon.Error);
                }
                //else if (ddlConditionField2.SelectedText == "-------")
                //{
                //    FineUI.Alert.ShowInTop("<b>Save Failed !</b> Condition Field 2 cannot be blank", FineUI.MessageBoxIcon.Error);
                //}
                else if (ddlOperation.SelectedText == "-------")
                {
                    FineUI.Alert.ShowInTop("<b>Save Failed !</b> Operation cannot be blank", FineUI.MessageBoxIcon.Error);
                }
                else if (string.IsNullOrEmpty(txtConditionalExpiryPeriod.Text))
                {
                    FineUI.Alert.ShowInTop("<b>Save Failed !</b> Expiry Period (in Months) cannot be blank", FineUI.MessageBoxIcon.Error);
                }
                else if (!IsNumeric(txtConditionalExpiryPeriod.Text))
                {
                    FineUI.Alert.ShowInTop("<b>Save Failed !</b> Expiry Period (in Months) must be an integer", FineUI.MessageBoxIcon.Error);
                }
                else if (ddlLogicalOperator.SelectedText == "-------")
                {
                    FineUI.Alert.ShowInTop("<b>Save Failed !</b> Logical operator cannot be blank", FineUI.MessageBoxIcon.Error);
                }
                else
                {
                    fnReturnValue = true;
                }
            }
            
            else
            {
                fnReturnValue = true;
            }
            return fnReturnValue;
        }

        public bool IsNumeric(string s)
        {
            int output;
            return int.TryParse(s, out output);
        }
    }
}