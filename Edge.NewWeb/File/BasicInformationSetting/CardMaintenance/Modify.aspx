﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CardMaintenance.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" runat="server"
                        OnClick="btnClose_Click" Text="Close">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="Save / Close">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="txtID" runat="server" Label="ID：" Required="true" ShowRedStar="true" Enabled="false" />           
            <ext:DropDownList ID="ddlRRCTypeOfApplication" runat="server" Label="RRC Type of Application：" Resizable="true" ShowRedStar="true"  AutoPostBack="true" OnSelectedIndexChanged="ddlRRCTypeOfApplication_OnSelectedIndexChanged">
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="NEW" Value = "1"/>
                                <ext:ListItem Text="RENEWAL" Value = "2"/>
                                <ext:ListItem Text="REPLACEMENT" Value = "3"/>
                            </ext:DropDownList>
            <ext:DropDownList ID="ddlRCCustomeTier" runat="server" Label="RRC Customer Tier：" Resizable="true" ShowRedStar="true" >
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="CLASSIC" Value = "1"/>
                                <ext:ListItem Text="ELITE" Value = "2"/>
                                <ext:ListItem Text="BEEP" Value = 3/>                                
                            </ext:DropDownList>
            <ext:TextBox ID="txtCardValidityInYears" runat="server" Label="Card Validity (in years)：" Required="true" ShowRedStar="true" />  
            <ext:DropDownList ID="ddlExpiryDateField" runat="server" Label="Expiry Date Field：" Resizable="true" ShowRedStar="true" >
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="RRC_Application Date" Value = "R"/>
                                <ext:ListItem Text="ExpirationDate" Value = "E"/>               
                            </ext:DropDownList> 
            <ext:RadioButtonList ID="rdEndOfMonth" runat="server" Label="End of Month：" Required="true" ShowRedStar="true">
                    <ext:RadioItem Text="YES" Value="1" Selected="true" />
                    <ext:RadioItem Text="NO" Value="0" />
            </ext:RadioButtonList>
            <ext:DropDownList ID="ddlWithCondition" runat="server" Label="With Condition：" Resizable="true" ShowRedStar="true" AutoPostBack="true" OnSelectedIndexChanged="ddlWithCondition_OnSelectedIndexChanged" >                                
                                <ext:ListItem Text="YES" Value = "1"/>
                                <ext:ListItem Text="NO" Value = "2" Selected="true"/>              
                            </ext:DropDownList> 
            <ext:DropDownList ID="ddlConditionField1" runat="server" Label="Condition Field：" Resizable="true" ShowRedStar="true" >
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="RRC_Application Date" Value = "R"/>
                                <ext:ListItem Text="ExpirationDate" Value = "E"/>               
                            </ext:DropDownList> 
            <ext:DropDownList ID="ddlOperation" runat="server" Label="Operation：" Resizable="true" ShowRedStar="true" >
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="INCREMENT" Value = "+"/>
                                <ext:ListItem Text="DECREMENT" Value = "-"/>
                                </ext:DropDownList> 
            <ext:TextBox ID="txtConditionalExpiryPeriod" runat="server" Label="Expiry Periond (in Months)：" Required="true" ShowRedStar="true" />  
            <ext:DropDownList ID="ddlLogicalOperator" runat="server" Label="Logical Operator：" Resizable="true" ShowRedStar="true" >
                                <ext:ListItem Text="-------" Value=""  Selected="true"/>
                                <ext:ListItem Text="GREATER THAN" Value = ">"/>
                                <ext:ListItem Text="GREATER THAN OR EQUAL TO" Value = "=>"/>
                                <ext:ListItem Text="LESS THAN" Value = "<"/>               
                                <ext:ListItem Text="LESS THAN OR EQUAL TO" Value = "=<"/>               
                                <ext:ListItem Text="EQUAL TO" Value = "="/>   
                                </ext:DropDownList>              
            <ext:Label ID="lblMessage" ></ext:Label>                                           
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>

