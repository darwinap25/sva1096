﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CardMaintenance.List" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
  <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
                    EnableModelValidation="True" CellPadding="4" ForeColor="#333333" 
                    Height="115px" HorizontalAlign="Justify" Width="691px" 
                    OnRowCommand="GridView1_RowCommand" DataKeyNames="ID">
                    <AlternatingRowStyle BackColor="White" ForeColor="Black" />
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID">
                        </asp:BoundField>
                        <asp:BoundField DataField="RRCTypeOfApplication" 
                            HeaderText="RRC Type of Application" />
                        <asp:BoundField DataField="RRCCustomerTier" HeaderText="RRC Customer Tier" />
                        <asp:BoundField DataField="CardValidityInYears" 
                            HeaderText="Card Validity (In Years)">
                        </asp:BoundField>
                        <asp:CommandField ShowCancelButton="False" SelectText="Edit" 
                            ShowSelectButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="Silver" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="Silver" Font-Bold="False" ForeColor="Black" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="GetCardValidityValues" TypeName="Edge.Web.DAO.CardMaintenanceDAO">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="0" Name="id" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>--%>

    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Items>
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="True" DataKeyNames="ID"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true" AllowSorting="true">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete" Text="Delete" Icon="Cross" runat="server" EnablePostBack="true" OnClick="btnDelete_Click">
                                    </ext:Button>                                    
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="Rule ID" SortField="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Type Of Application">
                                <ItemTemplate>
                                    <asp:Label ID="lblRRCTypeOfApplication" runat="server" Text='<%# Eval("RRCTypeOfApplication") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Customer Tier">
                                <ItemTemplate>
                                    <asp:Label ID="lblRRCCustomerTier" runat="server" Text='<%# Eval("RRCCustomerTier") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>    
                            <ext:TemplateField Width="60px" HeaderText="Card Validity (in years)">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardValidityInYears" runat="server" Text='<%# Eval("CardValidityInYears") %>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>    
                            <ext:TemplateField Width="60px" HeaderText="Expiry Date Field">
                                <ItemTemplate>
                                    <asp:Label ID="lblExpiryDateField" runat="server" Text='<%# Eval("ExpiryDateField") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>    
                            <ext:TemplateField Width="60px" HeaderText="End Of Month">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndOfMonth" runat="server" Text='<%# Eval("EndOfMonth") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="With Condition">
                                <ItemTemplate>
                                    <asp:Label ID="lblWithCondition" runat="server" Text='<%# Eval("WithCondition") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Condition Field">
                                <ItemTemplate>
                                    <asp:Label ID="lblConditionField1" runat="server" Text='<%# Eval("ConditionField1") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>
<%--                            <ext:TemplateField Width="60px" HeaderText="Condition Field 2">
                                <ItemTemplate>
                                    <asp:Label ID="lblConditionField2" runat="server" Text='<%# Eval("ConditionField2") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>--%>
                            <ext:TemplateField Width="60px" HeaderText="Operation">
                                <ItemTemplate>
                                    <asp:Label ID="lblOperation" runat="server" Text='<%# Eval("Operation") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Expiry Period (In Months)">
                                <ItemTemplate>  
                                    <asp:Label ID="lblConditionalExpiryPeriodInMonths" runat="server" Text='<%# Eval("ConditionalExpiryPeriodInMonths") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Logical Operator">
                                <ItemTemplate>
                                    <asp:Label ID="lblLogicalOperator" runat="server" Text='<%# Eval("LogicalOperator") %>'></asp:Label>
                                </ItemTemplate> 
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="ID"
                                DataIFrameUrlFormatString="Modify.aspx?id={0}" DataWindowTitleFormatString="编辑"
                                Title="编辑" />
                        </Columns>
                    </ext:Grid>
                    </Items>
            </ext:Panel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="250px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="450px">
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window> 
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
