﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.DAO;
using Edge.SVA.Model.Domain.SVA;

namespace Edge.Web.File.BasicInformationSetting.CardMaintenance
{
    public partial class Delete : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];

                    if (string.IsNullOrEmpty(ids))
                    {
                        this.ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                    string resultMsg = BatchDeleteID(idList);
                                        

                     Alert.ShowInTop(resultMsg, "", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                     
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Card Validity Rules:" + ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }

        private string BatchDeleteID(List<string> idList)
        {
            int success = 0;
            int failed = 0;
            int count = 0;
            count++;


            for (int i = 0; i < idList.Count; i++)
            {
                CardValidityMaintenance vo = new CardValidityMaintenance();
                CardMaintenanceDAO dao = new CardMaintenanceDAO();
                vo.Id = int.Parse(idList[i].ToString());

                string result = dao.DeleteCardValidityValues(vo);

                if (result == "0")
                {
                    success++;
                }
                else
                {
                    failed++;
                }

            }
            return string.Format(Resources.MessageTips.ApproveResult, success, failed);
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            Response.Redirect("List.aspx");
        }
    }
}