﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.DAO;
using Edge.SVA.Model.Domain.SVA;
using System.Text;
using Edge.Messages.Manager;

namespace Edge.Web.File.BasicInformationSetting.CardMaintenance
{
    
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                RptBind("", "ID");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
            }
        }


        private void RptBind(string strWhere, string orderby)
        {

            DataSet ds = new DataSet();
            CardMaintenanceDAO dao = new CardMaintenanceDAO();
            CardValidityMaintenance vo = new CardValidityMaintenance();

            vo.Id = 0;
            ds = dao.GetCardValidityValues(vo);

            if (ds != null)
            {
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.Reset();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteRecord(Grid1, HiddenWindowForm);
          //  RefreshPage();
        }


        protected void DeleteRecord(FineUI.Grid Grid, FineUI.Window Window)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid.SelectedRowIndexArray)
            {
                sb.Append(Grid.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(sb.ToString(), ",");
            idList = (from m in idList orderby m ascending select m).ToList();
            StringBuilder sb1 = new StringBuilder();
            foreach (var item in idList)
            {
                sb1.Append(item);
                sb1.Append(",");
            }
            Window.Title = Resources.MessageTips.Delete;
            string okScript = Window.GetShowReference("Delete.aspx?ids=" + sb1.ToString().TrimEnd(','));
            string cancelScript = "";
            ShowConfirmDialog(MessagesTool.instance.GetMessage("10018") + "\n TXN NO.: \n" + sb1.ToString().Replace(",", ";\n"), Resources.MessageTips.Delete, FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }
        //protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        // if (e.CommandName != "Page")
        //    {

        //        if (e.CommandName == "Select")
        //        {
        //            int rIndex;
        //            string id;

        //            rIndex = Convert.ToInt32(e.CommandArgument.ToString());

        //            id = GridView1.DataKeys[rIndex].Value.ToString();

        //            //string urlpath = Request.Url.GetLeftPart(UriPartial.Authority).ToString();
        //            //Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority).ToString() + "Index.aspx#/File/BasicInformationSetting/CardMaintenance/Modify.aspx");// ?utc=" + id.ToString());

        //            string url = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + "/Index.aspx#/File/BasicInformationSetting/CardMaintenance/Modify.aspx";
        //            string s = "window.open('" + url + "', 'popup_window', 'width=900,height=580');";
        //            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        //        }
        //    }
        //}

        //protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

       

    }
}