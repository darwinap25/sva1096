﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CurrencyExchangeRate;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CurrencyExchangeRate
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Currency, Edge.SVA.Model.Currency>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CurrencyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                SVASessionInfo.CurrencyController = null;

                //从CardIssuer获取本币
                Edge.SVA.BLL.CardIssuer bll = new SVA.BLL.CardIssuer();
                Edge.SVA.Model.CardIssuer cardissuer = bll.GetModelList("").Count == 0 ? null : bll.GetModelList("")[0];
                if (cardissuer != null)
                {
                    this.DomesticCurrencyIDView.Text = DALTool.GetCurrencyCodeName(Convert.ToInt32(cardissuer.DomesticCurrencyID), null);
                }

            }
            controller = SVASessionInfo.CurrencyController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            string Errormessage = controller.ValidataObject(this.CurrencyCode.Text.Trim(), 0);
            if (Errormessage != "")
            {
                FineUI.Alert.ShowInTop(Errormessage, FineUI.MessageBoxIcon.Warning);
                return;
            }
            //if (Tools.DALTool.isHasCurrencyCode(this.CurrencyCode.Text.Trim(), 0))
            //{
            //    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistCurrencyCode, FineUI.MessageBoxIcon.Warning);
            //    return;
            //}

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;

                controller.ViewModel.MainTable.CurrencyCode = controller.ViewModel.MainTable.CurrencyCode.ToUpper();

                controller.ViewModel.MainTable.CurrencyRate = ConvertTool.ToDecimal(this.CurrencyRate.Text);
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add Currency Success Code:" + controller.ViewModel.MainTable.CurrencyCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add Currency Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.CurrencyCode);
                ShowAddFailed();
            }
        }
    }
}
