﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CurrencyExchangeRate;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CurrencyExchangeRate
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Currency,Edge.SVA.Model.Currency>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CurrencyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                SVASessionInfo.CurrencyController = null;
            }
            controller = SVASessionInfo.CurrencyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                controller.LoadViewModel(Model.CurrencyID);
                if (!hasRight)
                {
                    return;
                }
                Edge.SVA.BLL.CardIssuer bll = new SVA.BLL.CardIssuer();
                Edge.SVA.Model.CardIssuer cardissuer = bll.GetModelList("").Count == 0 ? null : bll.GetModelList("")[0];
                if (cardissuer != null)
                {
                    this.DomesticCurrencyIDView.Text = DALTool.GetCurrencyCodeName(Convert.ToInt32(cardissuer.DomesticCurrencyID), null);
                }
                this.CurrencyRate.Text = controller.ViewModel.MainTable.CurrencyRate.ToString().TrimEnd('M');
            }
        }


        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update success ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            controller.ViewModel.MainTable = this.GetUpdateObject();

            //if (Tools.DALTool.isHasCurrencyCode(this.CurrencyCode.Text.Trim(), controller.ViewModel.MainTable.CurrencyID))
            //{
            //    ShowWarning(Resources.MessageTips.ExistCurrencyCode);
            //    return;
            //}

            string Errormessage = controller.ValidataObject(this.CurrencyCode.Text.Trim(), controller.ViewModel.MainTable.CurrencyID);
            if (Errormessage != "")
            {
                FineUI.Alert.ShowInTop(Errormessage, FineUI.MessageBoxIcon.Warning);
                return;
            }

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.CurrencyCode = controller.ViewModel.MainTable.CurrencyCode.ToUpper();
            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, string.Format("Update Success.   Code:{0}", controller.ViewModel.MainTable.CurrencyCode));
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, string.Format("Update Failed.   Code:{0}", controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.CurrencyCode));
                ShowUpdateFailed();
            }
        }
    }
}
