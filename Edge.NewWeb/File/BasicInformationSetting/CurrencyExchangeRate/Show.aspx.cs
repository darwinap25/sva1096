﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CurrencyExchangeRate;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CurrencyExchangeRate
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Currency, Edge.SVA.Model.Currency>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CurrencyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.CurrencyController = null;
            }
            controller = SVASessionInfo.CurrencyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.CurrencyID);
                if (controller.ViewModel.MainTable != null)
                {
                    this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                    this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                    this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                    this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);

                    //从CardIssuer获取本币
                    Edge.SVA.BLL.CardIssuer bll = new SVA.BLL.CardIssuer();
                    Edge.SVA.Model.CardIssuer cardissuer = bll.GetModelList("").Count == 0 ? null : bll.GetModelList("")[0];
                    if (cardissuer != null)
                    {
                        this.lblDomesticCurrencyView.Text = DALTool.GetCurrencyCodeName(Convert.ToInt32(cardissuer.DomesticCurrencyID), null);
                    }
                    this.CurrencyRateView.Text = controller.ViewModel.MainTable.CurrencyRate.ToString().TrimEnd('M');
                }          
            }
        }
    }
}
