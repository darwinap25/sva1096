﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.BasicInformationSetting.Advertisements;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.Advertisements
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PromotionMsg, Edge.SVA.Model.PromotionMsg>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        AdvertisementsModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.AdvertisementsModifyController = null;
                controller = SVASessionInfo.AdvertisementsModifyController;

                InitParintIDList();
            }
            controller = SVASessionInfo.AdvertisementsModifyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.KeyID, SVASessionInfo.SiteLanguage);
                if (controller.ViewModel.MainTable != null)
                {
                    this.ParentID.SelectedValue = controller.GetParentIDListByTypeID(controller.ViewModel.MainTable.KeyID);
                    this.ParentIDName.Text = this.ParentID.SelectedText;
                    InitPromotionMsgTypeList();

                    this.PromotionMsgTypeID.SelectedValue = controller.ViewModel.MainTable.PromotionMsgTypeID.ToString().Trim();
                    this.PromotionMsgTypeIDByName.Text = this.PromotionMsgTypeID.SelectedText;

                    this.Status.SelectedValue = controller.ViewModel.MainTable.Status.ToString().Trim();
                    this.StatusName.Text = string.IsNullOrEmpty(this.Status.SelectedValue) ? "" : this.Status.SelectedItem.Text;

                    this.btnPreview.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.PromotionPicFile) ? true : false;//没有照片时不显示查看按钮(Len)

                    this.uploadFilePath.Text = controller.ViewModel.MainTable.PromotionPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    //加载树结构
                    LoadTree();
                }
            }
        }

        //加载树结构
        protected void LoadTree()
        {
            Edge.SVA.BLL.CardGrade bll = new SVA.BLL.CardGrade();
            List<Edge.SVA.Model.CardGrade> CardGradelist = bll.DataTableToList(bll.GetList(@" CardGradeID in (select CardGradeID from dbo.PromotionCardCondition where PromotionMsgID =" + Model.KeyID + ")").Tables[0]);

            Edge.SVA.BLL.CardType bll1 = new SVA.BLL.CardType();
            List<Edge.SVA.Model.CardType> CardTypelist = bll1.DataTableToList(bll1.GetList(@" CardTypeID in (select CardTypeID from dbo.CardGrade where CardGradeID in (select CardGradeID from dbo.PromotionCardCondition
                                where PromotionMsgID =" + Model.KeyID + "))").Tables[0]);

            //List<Edge.SVA.Model.CardType> newList=new List<SVA.Model.CardType>();

            //foreach (var item in CardTypelist)
            //{
            //    SVA.Model.CardType model = CardTypelist.Find(m => m.CardTypeID == item.CardTypeID);
            //    newList.Add(model);
            //}

            //获取CardType表中的BrandID,但需去除重复项,防止添加多次根节点
            List<int> BrandIDList = new List<int>();
            BrandIDList = ((from p in CardTypelist
                            select p.BrandID).Distinct().ToArray()).ToList();

            //获取CardGrade表中的CardTypeID，但需要去除重复项，防止添加多次根节点
            List<int> newList = new List<int>();
            newList = ((from p in CardGradelist
                            select p.CardTypeID).Distinct().ToArray()).ToList();

            CardGradeTree.AutoWidth = true;
            List<BrandInfo> listBrandInfo = controller.ViewModel.BrandInfo;
            foreach (BrandInfo brandItem in listBrandInfo)
            {
                if (brandItem.CardTypeInfos.Count >= 1)
                {
                    FineUI.TreeNode brandNode = new FineUI.TreeNode();
                    brandNode.NodeID = brandItem.Key;
                    brandNode.Text = brandItem.Value;
                    foreach (var item in BrandIDList)
                    {
                        if (Convert.ToInt32(brandItem.Key) == item)
                        {
                            CardGradeTree.Nodes.Add(brandNode);
                        }
                    }
                    foreach (var cardtypeitem in brandItem.CardTypeInfos)
                    {
                        FineUI.TreeNode cardtypeNode = new FineUI.TreeNode();
                        cardtypeNode.NodeID = brandItem.Key + "_" + cardtypeitem.Key;
                        cardtypeNode.Text = cardtypeitem.Value;
                        foreach (var item in CardTypelist)//(var item in newList)
                        {
                            if (Convert.ToInt32(cardtypeitem.Key) == item.CardTypeID)
                            {
                                brandNode.Nodes.Add(cardtypeNode);
                            }
                        }
                        foreach (var cardgradeitem in cardtypeitem.CardGrades)
                        {
                            FineUI.TreeNode cardgradeNode = new FineUI.TreeNode();
                            cardgradeNode.NodeID = brandItem.Key + "_" + cardtypeitem.Key + "_" + cardgradeitem.Key;
                            cardgradeNode.Text = cardgradeitem.Value;
                            foreach (var item in controller.ViewModel.CardGradeIDList)
                            {
                                if (Convert.ToInt32(cardgradeitem.Key) == item)
                                {
                                    cardtypeNode.Nodes.Add(cardgradeNode);
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void InitParintIDList()
        {
            controller.ViewModel.ParentIDList = controller.GetParentIDList(SVASessionInfo.SiteLanguage);
            ControlTool.BindKeyValueList(this.ParentID, controller.ViewModel.ParentIDList);
        }
        protected void InitPromotionMsgTypeList()
        {
            controller.ViewModel.PromotionMsgTypeList = controller.AddPromotionMsgTypeList(SVASessionInfo.SiteLanguage, Convert.ToInt32(this.ParentID.SelectedValue));
            ControlTool.BindKeyValueList(this.PromotionMsgTypeID, controller.ViewModel.PromotionMsgTypeList);
        }
    }
}