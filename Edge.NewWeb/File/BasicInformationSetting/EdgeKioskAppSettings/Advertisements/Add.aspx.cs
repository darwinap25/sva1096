﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.BasicInformationSetting.Advertisements;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain;
using System.IO;


namespace Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.Advertisements
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PromotionMsg, Edge.SVA.Model.PromotionMsg>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        AdvertisementsAddController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.AdvertisementsAddController = null;
                controller = SVASessionInfo.AdvertisementsAddController;

                //加载树结构
                LoadTree();
                InitParintIDList();
            }
            controller = SVASessionInfo.AdvertisementsAddController;
        }
        protected void btnSaveClose_Click(object sender, System.EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            controller.ViewModel.MainTable = this.GetAddObject();
            if (ValidateForm())
            {
                if (controller.ViewModel.MainTable != null)
                {
                    //校验图片以及文件类型是否正确
                    if (!ValidateImg(this.PromotionPicFile.Text))
                    {
                        return;
                    }
                    if (!ValidateFile(this.AttchFile.Text))
                    {
                        return;
                    }

                    controller.ViewModel.MainTable.CreatedBy = DALTool.GetCurrentUser().UserID;
                    controller.ViewModel.MainTable.CreatedOn = DateTime.Now;

                    HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
                    //保存图片到数据库
                    this.PromotionPicFile.Text = this.PromotionPicFilePath.SaveToServer("Files/Advertisements");
                    controller.ViewModel.MainTable.PromotionPicFile = this.PromotionPicFile.Text;

                    controller.ViewModel.MainTable.PromotionMsgStr1 = clearTool.ClearSource(this.PromotionMsgStr1.Text).Replace("\r\n", "");
                    controller.ViewModel.MainTable.PromotionMsgStr2 = clearTool.ClearSource(this.PromotionMsgStr2.Text).Replace("\r\n", "");
                    controller.ViewModel.MainTable.PromotionMsgStr3 = clearTool.ClearSource(this.PromotionMsgStr3.Text).Replace("\r\n", "");

                    //保存附件到数据库
                    AttachmentFilePath.SaveAttachFileToServer("Files/Advertisements");
                    controller.ViewModel.MainTable.AttachmentFilePath = this.AttchFile.Text;

                    //获取树节点的值，并保存到对象
                    controller.ViewModel.CardGradeIDList.Clear();
                    controller.ViewModel.BrandInfo.Clear();
                    foreach (FineUI.TreeNode brandNode in CardGradeTree.Nodes)
                    {
                        if (brandNode.Checked)
                        {
                            BrandInfo bi = new BrandInfo();
                            bi.Key = brandNode.NodeID;
                            bi.Value = brandNode.Text;
                            foreach (FineUI.TreeNode cardtypeNode in brandNode.Nodes)
                            {
                                if (cardtypeNode.Checked)
                                {
                                    CardTypeInfo ct = new CardTypeInfo();
                                    ct.Key = cardtypeNode.NodeID.Split('_')[1];
                                    ct.Value = cardtypeNode.Text;
                                    foreach (var cardgradeNode in cardtypeNode.Nodes)
                                    {
                                        if (cardgradeNode.Checked)
                                        {
                                            ct.CardGrades.Add(new CardGradeInfo { Key = cardgradeNode.NodeID.ToString(), Value = cardgradeNode.Text });

                                            int CardGradeID = string.IsNullOrEmpty(cardgradeNode.NodeID.Split('_')[2]) ? 0 : Convert.ToInt32(cardgradeNode.NodeID.Split('_')[2]);
                                            controller.ViewModel.CardGradeIDList.Add(CardGradeID);
                                        }
                                    }
                                    bi.CardTypeInfos.Add(ct);
                                }
                                controller.ViewModel.BrandInfo.Add(bi);
                            }
                        }
                    }
                }

                //ExecResult er = controller.Submit();
                //if (er.Success)
                //{
                //    CloseAndRefresh();
                //}
                //else
                //{
                //    logger.WriteErrorLog("PromotionMsg ", " Add error", er.Ex);
                //    ShowAddFailed();
                //}

                ExecResult er = controller.Save();
                if (er.Success)
                {
                    CloseAndRefresh();
                }
                else
                {
                    logger.WriteErrorLog("PromotionMsg ", " Add error", er.Ex);
                    ShowAddFailed();
                }
            }

        }
        protected void CardGradeTree_NodeCheck(object sender, FineUI.TreeCheckEventArgs e)
        {
            if (e.Checked)
            {
                if (e.Node.ParentNode != null)
                {
                    if (e.Node.ParentNode.ParentNode != null && !e.Node.ParentNode.ParentNode.Checked)
                    {
                        e.Node.ParentNode.ParentNode.Checked = true;
                    }
                }
                if (e.Node.ParentNode != null && !e.Node.ParentNode.Checked)
                {
                    e.Node.ParentNode.Checked = true;
                }
                if (e.Node.Nodes.Count >= 1)
                {
                    CardGradeTree.CheckAllNodes(e.Node.Nodes);
                }
            }
            else
            {
                if (e.Node.ParentNode != null)
                {
                    bool needChecked = false;

                    if (e.Node.ParentNode.Checked)
                    {
                        foreach (FineUI.TreeNode item in e.Node.ParentNode.Nodes)
                        {
                            if (item.Checked)
                            {
                                needChecked = true;
                            }
                        }
                        e.Node.ParentNode.Checked = needChecked;
                    }
                    needChecked = false;
                    if (e.Node.ParentNode.ParentNode != null && e.Node.ParentNode.ParentNode.Checked)
                    {
                        foreach (FineUI.TreeNode item in e.Node.ParentNode.ParentNode.Nodes)
                        {
                            if (item.Checked)
                            {
                                needChecked = true;
                            }
                        }
                        e.Node.ParentNode.ParentNode.Checked = needChecked;
                    }
                }
                CardGradeTree.UncheckAllNodes(e.Node.Nodes);
            }
        }
        protected void CheckAll_CheckedChanged(object sender, System.EventArgs e)
        {
            if (this.CheckAll.Checked)
            {
                CardGradeTree.CheckAllNodes(CardGradeTree.Nodes);
            }
            else
            {
                CardGradeTree.UncheckAllNodes(CardGradeTree.Nodes);
            }
        }

        //加载树结构
        protected void LoadTree()
        {
            CardGradeTree.AutoWidth = true;
            List<BrandInfo> listBrandInfo;
            if (SVASessionInfo.CurrentUser.UserName.Equals(ConstParam.SystemAdminName))
            {
                listBrandInfo = PublicInfoReostory.Singleton.GetCardTypeListByBrand(SVASessionInfo.SiteLanguage);
            }
            else
            {
                listBrandInfo = SVASessionInfo.CurrentUser.BrandInfoList;
            }
            foreach (BrandInfo brandItem in listBrandInfo)
            {
                if (brandItem.CardTypeInfos.Count >= 1)
                {
                    FineUI.TreeNode brandNode = new FineUI.TreeNode();
                    brandNode.EnableCheckBox = true;
                    brandNode.AutoPostBack = true;
                    brandNode.NodeID = brandItem.Key;
                    brandNode.Text = brandItem.Value;
                    CardGradeTree.Nodes.Add(brandNode);
                    foreach (var cardtypeitem in brandItem.CardTypeInfos)
                    {
                        FineUI.TreeNode cardtypeNode = new FineUI.TreeNode();
                        cardtypeNode.EnableCheckBox = true;
                        cardtypeNode.AutoPostBack = true;
                        cardtypeNode.NodeID = brandItem.Key + "_" + cardtypeitem.Key;
                        cardtypeNode.Text = cardtypeitem.Value;
                        brandNode.Nodes.Add(cardtypeNode);
                        foreach (var cardgradeitem in cardtypeitem.CardGrades)
                        {
                            FineUI.TreeNode cardgradeNode = new FineUI.TreeNode();
                            cardgradeNode.EnableCheckBox = true;
                            cardgradeNode.AutoPostBack = true;
                            cardgradeNode.NodeID = brandItem.Key + "_" + cardtypeitem.Key + "_" + cardgradeitem.Key;
                            cardgradeNode.Text = cardgradeitem.Value;
                            cardtypeNode.Nodes.Add(cardgradeNode);
                        }
                    }
                }
            }
        }

        public bool ValidateForm()
        {
            if (string.IsNullOrEmpty(this.PromotionMsgStr1.Text) || this.PromotionMsgStr1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.PromotionMsg1CannotBeEmpty);
                return false;
            }

            return true;
        }

        protected void ParentID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PromotionMsgTypeID.Items.Clear();
            InitPromotionMsgTypeList();
        }

        protected void InitParintIDList()
        {
            controller.ViewModel.ParentIDList = controller.GetParentIDList(SVASessionInfo.SiteLanguage);
            ControlTool.BindKeyValueList(this.ParentID, controller.ViewModel.ParentIDList);
        }



        protected void InitPromotionMsgTypeList()
        {
            controller.ViewModel.PromotionMsgTypeList = controller.AddPromotionMsgTypeList(SVASessionInfo.SiteLanguage, Convert.ToInt32(this.ParentID.SelectedValue));
            ControlTool.BindKeyValueList(this.PromotionMsgTypeID, controller.ViewModel.PromotionMsgTypeList);
        }

        protected void AttachmentFilePath_FileSelected(object sender, EventArgs e)
        {
            if (AttachmentFilePath.HasFile)
            {
                string fileName = AttachmentFilePath.ShortFileName;
                fileName = webset.AttchFileServer + webset.WebFilePath + "/Files/Advertisements/" + fileName;
                this.AttchFile.Text = fileName;
            }

        }
        protected void PromotionPicFile_FileSelected(object sender, EventArgs e)
        {                    
            if (PromotionPicFilePath.HasFile)
            {
                string fileName = this.PromotionPicFilePath.FileName; //this.PromotionPicFilePath.SaveToServer("Files/Advertisements");
                this.PromotionPicFile.Text = fileName;
            }

        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.');
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.').ToLower();
                if (!webset.AdvertisementFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.AdvertisementFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}