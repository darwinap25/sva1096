﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using System.IO;

namespace Edge.Web.File.BasicInformationSetting.EdgeKioskAppSettings.NewsPicture
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PromotionMsg_Pic, Edge.SVA.Model.PromotionMsg_Pic>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);


                Edge.Web.Tools.ControlTool.BindPromotionMsgCode(PromotionMsgCode);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (!ValidateForm())
            {
                return;
            }

            //TODO:
            //if (Edge.Web.Tools.DALTool.isHasStoreAttributeCode(this.StoreAttributeCode.Text.Trim(), 0))
            //{
            //    ShowWarning(Resources.MessageTips.ExistStoreAttributeCode);
            //    this.StoreAttributeCode.Focus();
            //    return;
            //}

            //if (!Edge.Web.Tools.DALTool.isHasStoreAttributeCodeWithStore(this.StoreAttributeCode.Text.Trim(), 0,this.StoreCode.Text.Trim()))
            //{
            //    ShowWarning(Resources.MessageTips.ExistStoreAttributeCodeWith);
            //    this.StoreAttributeCode.Focus();
            //    return;
            //}

            Edge.SVA.Model.PromotionMsg_Pic item = this.GetAddObject();

            if (item != null)
            {
                if (!ValidateImg(this.ExtraPic.FileName))
                {
                    return;
                }
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                item.PromotionMsgCode = item.PromotionMsgCode.ToUpper();
                item.PicRemark = item.PicRemark;
                item.ExtraPic = this.ExtraPic.SaveToServer("Images/NewsPicture");
            }

            if (DALTool.Add<Edge.SVA.BLL.PromotionMsg_Pic>(item) > 0)
            {
               CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.ExtraPic.ShortFileName))
            {
                this.uploadFilePath.Text = this.ExtraPic.SaveToServer("Images/NewsPicture");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
        }
        public bool ValidateForm()
        {
            if (this.PromotionMsgCode.SelectedValue == "-1")
            {
                ShowWarning(Resources.MessageTips.PromotionMsgCodeBeEmpty);
                return false;
            }
            return true;
        }
        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}