﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLMointoringRule;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CSVXMLMointoringRule
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CSVXMLMointoringRule, Edge.SVA.Model.CSVXMLMointoringRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CSVXMLMointoringRuleController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.CSVXMLMointoringRuleController = null;
            }
            controller = SVASessionInfo.CSVXMLMointoringRuleController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
               
                this.ActiveTime.Text = DateTime.Parse(Model.ActiveTime.ToString()).ToString("hh:mm");
                controller.LoadViewModel(Model.RuleName);

            }
        }
       
    }
}
