﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CSVXMLMointoringRule.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
        
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="Basic Settings"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Label ID="RuleName" runat="server" Label="Rule Number："/>
                    <ext:Label ID="Description" runat="server" Label="Description："  />
                    <ext:Label ID="StartDate" runat="server" Label="开始日期：" />    
                    <ext:Label ID="EndDate" runat="server" Label="结束日期：" />  
                    <ext:Label ID="Status" runat="server" Label="状态：" />                                          
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="Function Information"
                 AutoHeight="true" AutoWidth="true">
               <Items>
                   <ext:Label ID="Func1" runat="server" Label="Function：" />   
                   <ext:Label ID="DownloadPath" runat="server" Label="Download Path：" />
               </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="Time Setting Message"
                 AutoHeight="true" AutoWidth="true">
               <Items>
                   <ext:Label ID="DayFlagID" runat="server" Label="有效天数设置：" />
                   <ext:Label ID="WeekFlagID" runat="server" Label="有效周设置："/>
                   <ext:Label ID="MonthFlagID" runat="server" Label="有效月设置："/>
                   <ext:Label ID="ActiveTime" runat="server" Label="Set Valid Time："/>   
               </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
