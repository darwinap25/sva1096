﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate;
using Edge.SVA.Model.Domain.File.BasicViewModel;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MessageTemplate, Edge.SVA.Model.MessageTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MsgTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.WindowEdit.Title = "新增";
                RegisterCloseEvent(btnClose);
                string id = Request.Params["id"];
                if (string.IsNullOrEmpty(id)) return;

                SVASessionInfo.MsgTemplateController = null;
                controller = SVASessionInfo.MsgTemplateController;
                controller.LoadViewModel(int.Parse(id),SVASessionInfo.SiteLanguage);
                BindingDataGrid_NotificationTemplate();

                ControlTool.BindBrandInfoList(BrandID, controller.ViewModel.BrandInfoList);
                ControlTool.BindKeyValueList(OprID, controller.ViewModel.TransactionTypeList);

                btnDelete.OnClientClick = Grid_NotificationTemplate.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
            controller = SVASessionInfo.MsgTemplateController;
        }

        protected void btnNew_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(WindowEdit.GetShowReference("MsgTemplateDetail/Add.aspx"));
        }
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_NotificationTemplate.SelectedRowIndexArray;
            int count = this.Grid_NotificationTemplate.PageIndex * this.Grid_NotificationTemplate.PageSize;
            List<MessageTemplateDetailViewModel> list = new List<MessageTemplateDetailViewModel>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.MessageTemplateDetailViewModelList[rowIndexs[i] + count]);
            }
            controller.RemoveMessageTemplateDetailViewModelList(list);
            BindingDataGrid_NotificationTemplate();
        }
        private void BindingDataGrid_NotificationTemplate()
        {
            this.Grid_NotificationTemplate.RecordCount = controller.ViewModel.MessageTemplateDetailViewModelList.Count;
            this.Grid_NotificationTemplate.DataSource = controller.ViewModel.MessageTemplateDetailViewModelList;
            this.Grid_NotificationTemplate.DataBind();
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_NotificationTemplate();
        }


        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //if (Model != null)
                //{
                //    this.BrandID.SelectedValue = Model.BrandID.ToString();
                //}
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            Edge.SVA.Model.MessageTemplate item = this.GetUpdateObject();

            controller.ViewModel.MainTable = item;

            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MessageTemplate Update Success\tCode:" + item.MessageTemplateCode);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MessageTemplate Update Failed\tCode:" + item == null ? "No Data" : item.MessageTemplateCode);
                ShowUpdateFailed();
            }

        }

        //protected void btnExport_Click(object sender, EventArgs e)
        //{
        //    string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
        //    try
        //    {
        //        Tools.ExportTool.ExportFile(fileName);
        //        Tools.Logger.Instance.WriteOperationLog("MsgTemplate download ", " filename: " + fileName);
        //        //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
        //        //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
        //        //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
        //        Tools.Logger.Instance.WriteErrorLog("MsgTemplate download ", " filename: " + fileName, ex);
        //        ShowWarning(ex.Message);
        //    }

        //}

        //protected void btnReUpLoad_Click(object sender, EventArgs e)
        //{
        //    this.FormLoad.Hidden = false;
        //    this.FormReLoad.Hidden = true;
        //}

        //protected void btnBack_Click(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
        //    {
        //        this.FormLoad.Hidden = true;
        //        this.FormReLoad.Hidden = false;
        //    }
        //}
    }
}
