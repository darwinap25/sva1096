﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MsgTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                controller = SVASessionInfo.MsgTemplateController;
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, FineUI.MessageBoxIcon.Warning);
                        return;
                    }
                    logger.WriteOperationLog(this.PageName, "Delete");
                    //foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    //{
                    //    if (string.IsNullOrEmpty(id)) continue;
                    //    Edge.Web.Tools.DALTool.Delete<Edge.SVA.BLL.MessageTemplate>(Tools.ConvertTool.ToInt(id));
                    //}
                    List<int> MessageTemplateIDList = new List<int>();
                    foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (string.IsNullOrEmpty(id)) continue;
                        MessageTemplateIDList.Add(Tools.ConvertTool.ToInt(id));
                    }
                    controller.DeleteData(MessageTemplateIDList);

                    FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                    Tools.Logger.Instance.WriteOperationLog("MessageTemplate Delete", "MessageTemplate Delete\t Code:" + ids);
                }
                catch (System.Exception ex)
                {
                    logger.WriteErrorLog(this.PageName, "Delete", ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
            SVASessionInfo.MsgTemplateController = null;
        }
    }
}
