﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:HiddenField ID="MessageTemplateID" runat="Server">
            </ext:HiddenField>
            <ext:TextBox ID="MessageTemplateCode" runat="server" Label="通知编号：" Required="true"
                MaxLength="20" ShowRedStar="true" Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="MessageTemplateDesc" runat="server" Label="通知描述：" Required="true"
                ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Required="true" ShowRedStar="true"
                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值" Enabled="false">
            </ext:DropDownList>
            <ext:DropDownList ID="OprID" runat="server" Label="交易类型：" Required="true" ShowRedStar="true"
                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值" Enabled="false">
            </ext:DropDownList>
            <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="通知方式模板列表">
                <Items>
                    <ext:Grid ID="Grid_NotificationTemplate" ShowBorder="true" ShowHeader="false" Title="通知方式模板列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="ObjectKey"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="RegisterGrid_OnPageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew" Text="新增" Icon="Add" runat="server" OnClick="btnNew_OnClick">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="MessageServiceTypeDesc" DataField="MessageServiceTypeDesc"
                                HeaderText="通知方式" />
                            <ext:BoundField ColumnID="StatusDesc" DataField="StatusDesc" HeaderText="通知状态" />
                            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="WindowEdit" Icon="PageEdit"
                                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="ObjectKey"
                                DataIFrameUrlFormatString="MsgTemplateDetail/Modify.aspx?id={0}" Title="编辑" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowEdit" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    </form>
</body>
</html>
