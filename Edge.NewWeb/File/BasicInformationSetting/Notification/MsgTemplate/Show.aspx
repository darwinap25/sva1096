﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="MessageTemplateCode" runat="server" Label="通知编号：" Text="">
                            </ext:Label>
                            <ext:Label ID="MessageTemplateDesc" runat="server" Label="通知描述：" Text="">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="BrandIDDesc" runat="server" Label="品牌：" Text="">
                            </ext:Label>
                            <ext:Label ID="OprIDDesc" runat="server" Label="交易类型：" Text="">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>

            <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="通知方式模板列表">
                <Items>
                    <ext:Grid ID="Grid_NotificationTemplate" ShowBorder="true" ShowHeader="false" Title="通知方式模板列表"
                        AutoHeight="true" PageSize="5" runat="server" DataKeyNames="ObjectKey"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="RegisterGrid_OnPageIndexChange">
                        <Columns>
                            <ext:BoundField ColumnID="MessageServiceTypeDesc" DataField="MessageServiceTypeDesc"
                                HeaderText="通知方式" />
                            <ext:BoundField ColumnID="StatusDesc" DataField="StatusDesc" HeaderText="通知状态" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    </form>
</body>
</html>
