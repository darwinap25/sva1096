﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.MessageTemplate, Edge.SVA.Model.MessageTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MsgTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);
                string id = Request.Params["id"];
                if (string.IsNullOrEmpty(id)) return;
                SVASessionInfo.MsgTemplateController = null;
                controller = SVASessionInfo.MsgTemplateController;
                controller.LoadViewModel(int.Parse(id), SVASessionInfo.SiteLanguage);
                BindingDataGrid_NotificationTemplate();
            }

        }
        private void BindingDataGrid_NotificationTemplate()
        {
            this.Grid_NotificationTemplate.RecordCount = controller.ViewModel.MessageTemplateDetailViewModelList.Count;
            this.Grid_NotificationTemplate.DataSource = controller.ViewModel.MessageTemplateDetailViewModelList;
            this.Grid_NotificationTemplate.DataBind();
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    //Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(int.Parse(Model.BrandID.ToString()));
                    //string brandText = brand == null ? "" : DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                    //this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);

                    //this.BrandID.Text=
                    if (controller.ViewModel.MainTable.BrandID.HasValue)
                    {
                        BrandInfo bi = controller.ViewModel.BrandInfoList.Find(mm => mm.Key.Equals(controller.ViewModel.MainTable.BrandID.Value.ToString()));
                        if (bi != null)
                        {
                            this.BrandIDDesc.Text = bi.Value;
                        }
                    }
                    if (controller.ViewModel.MainTable.OprID.HasValue)
                    {
                        KeyValue kv = controller.ViewModel.TransactionTypeList.Find(mm => mm.Key.Equals(controller.ViewModel.MainTable.OprID.Value.ToString()));
                        if (kv != null)
                        {
                            this.OprIDDesc.Text = kv.Value;
                        }
                    }
                }
            }
        }
        //protected void btnExport_Click(object sender, EventArgs e)
        //{
        //    string fileName = Server.MapPath("~"+this.uploadFilePath.Text);
        //    try
        //    {
        //        Tools.ExportTool.ExportFile(fileName);
        //        Tools.Logger.Instance.WriteOperationLog("MsgTemplate download ", " filename: " + fileName);
        //        //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
        //        //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
        //        //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
        //        Tools.Logger.Instance.WriteErrorLog("MsgTemplate download ", " filename: " + fileName,ex);
        //        ShowWarning(ex.Message);
        //    }

        //}
    }
}
