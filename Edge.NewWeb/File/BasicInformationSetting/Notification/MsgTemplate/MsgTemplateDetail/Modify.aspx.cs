﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgTemplate.MsgTemplateDetail
{
    public partial class Modify : BasePage<Edge.SVA.BLL.MessageTemplateDetail, MessageTemplateDetailViewModel>
    {
        MsgTemplateController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            controller = SVASessionInfo.MsgTemplateController;
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                ControlTool.BindKeyValueList(MessageServiceTypeID, controller.ViewModel.MessageSerivceTypeList);
            }
        }
        protected override void SetObject()
        {
            string id = Request.Params["id"];
            if (string.IsNullOrEmpty(id)) return;
            MessageTemplateDetailViewModel mmm = controller.ViewModel.MessageTemplateDetailViewModelList.Find(mm => mm.ObjectKey.Equals(id));
            if (mmm != null)
            {
                this.SetObject(mmm, this.Form.Controls.GetEnumerator());
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteOperationLog(this.PageName, " Add ");
            try
            {

                MessageTemplateDetailViewModel item = this.GetUpdateObject();
                if (item != null)
                {
                    controller.UpdateMessageTemplateDetailViewModel(SVASessionInfo.SiteLanguage, item);

                    CloseAndPostBack();
                }
                else
                {
                    ShowError(Resources.MessageTips.UnKnownSystemError);
                }
            }
            catch (System.Exception ex)
            {
                Logger.Instance.WriteErrorLog("", "", ex);
                ShowError(Resources.MessageTips.UnKnownSystemError);
            }
        }
        protected void MessageServiceTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageTemplateInfo mti = MessageInfoRepostory.Singleton.GetMessageTemplateInfo(controller.TransactionTypeID, this.MessageServiceTypeID.SelectedValue);
            if (mti != null)
            {
                this.TemplateContent1.Text = mti.TemplateContent1;
                this.TemplateContent2.Text = mti.TemplateContent2;
                this.TemplateContent3.Text = mti.TemplateContent3;
            }
            else
            {
                this.TemplateContent1.Text = string.Empty;
                this.TemplateContent2.Text = string.Empty;
                this.TemplateContent3.Text = string.Empty;
            }
        }
    }
}