﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular
{
    public partial class List : PageBase
    {

        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {        

            if (!Page.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "List");

                Grid1.PageSize = webset.ContentPageNum;
                RptBind("", "CreatedOn");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnApprove.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnApprove.ConfirmText = Resources.MessageTips.ConfirmApprove;
            }
        }

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {

            Edge.SVA.BLL.MemberNotice bll = new Edge.SVA.BLL.MemberNotice();

            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);
            if (this.Grid1.RecordCount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, strWhere, orderby);

            //Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            //Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            //Tools.DataTool.AddMessageType(ds, "MessageType");
            //Tools.DataTool.AddMessageServiceType(ds, "MessageServiceTypeID");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(",'");
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append("'");
            }

            string sql = "update MemberNotice set ApproveStatus='A' where NoticeNumber in ("+sb.ToString().Substring(1)+")";
            try
            {
                DBUtility.DbHelperSQL.ExecuteSql(sql);
            }
            catch (System.Exception ex)
            {
            	
            }
            messageNotifyUtil.SendNotifyFlag();

            RptBind("", "CreatedOn");
        }
        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "CreatedOn");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "CreatedOn");
        }
    }
}
