﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;
using AtiveMQ.NetUtil;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberNotice, Edge.SVA.Model.MemberNotice>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }       

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {

                }
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            ////if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            ////{
            ////    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistDistributeTemplateCode, FineUI.MessageBoxIcon.Warning);
            ////    return;
            ////}
            Edge.SVA.Model.MemberNotice item = this.GetUpdateObject();

            if (item != null)
            {
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.DistributionCode = item.DistributionCode.ToUpper();

                //item.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");


            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.MemberNotice>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice Update Success\tCode:" + item.NoticeNumber);

                messageNotifyUtil.UpdateStatusP(item.NoticeNumber);

                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice Update Failed\tCode:" + item == null ? "No Data" : item.NoticeNumber);
                ShowUpdateFailed();
            }
        }
    }
}
