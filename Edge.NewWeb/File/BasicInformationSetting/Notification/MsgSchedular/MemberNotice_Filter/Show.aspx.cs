﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_Filter
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.MemberNotice_Filter, Edge.SVA.Model.MemberNotice_Filter>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(int.Parse(Model.BrandID.ToString()));
                    string brandText = brand == null ? "" : DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                    this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);

                    if (Model.CardTypeID.HasValue)
                    {
                        Edge.SVA.Model.CardType cardType = new Edge.SVA.BLL.CardType().GetModel(Model.CardTypeID.Value);
                        string cardTypeText = cardType == null ? "" : DALTool.GetStringByCulture(cardType.CardTypeName1, cardType.CardTypeName2, cardType.CardTypeName3);
                        this.CardTypeID.Text = cardType == null ? "" : ControlTool.GetDropdownListText(cardTypeText, cardType.CardTypeCode);
                    }

                    if (Model.CardGradeID.HasValue)
                    {
                        Edge.SVA.Model.CardGrade cardGrade = new Edge.SVA.BLL.CardGrade().GetModel(Model.CardGradeID.Value);
                        string cardGradeText = cardGrade == null ? "" : DALTool.GetStringByCulture(cardGrade.CardGradeName1, cardGrade.CardGradeName2, cardGrade.CardGradeName3);
                        this.CardGradeID.Text = cardGrade == null ? "" : ControlTool.GetDropdownListText(cardGradeText, cardGrade.CardGradeCode);
                    }
                    if (Model.CouponTypeID.HasValue)
                    {
                        Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(Model.CouponTypeID.Value);
                        string couponTypeText = couponType == null ? "" : DALTool.GetStringByCulture(couponType.CouponTypeName1, couponType.CouponTypeName2, couponType.CouponTypeName3);
                        this.CouponTypeID.Text = couponType == null ? "" : ControlTool.GetDropdownListText(couponTypeText, couponType.CouponTypeCode);
                    }

                    if (int.Parse(this.OnBirthDay.Text) == 0)
                    {
                        this.OnBirthDay.Text = "不是";
                    }
                    else
                    {
                        this.OnBirthDay.Text = "是";
                    }
                }
            }
        }
    }
}
