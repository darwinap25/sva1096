﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_Filter
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.MemberNotice_Filter, Edge.SVA.Model.MemberNotice_Filter>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        string id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                id = Request.Params["NoticeNumber"];
                this.NoticeNumber.Text = id;

                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                                
            }
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CardGradeID.Items.Clear();
            this.CardGradeID.Items.Add(new FineUI.ListItem());
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(" CardTypeID="+this.CardTypeID.SelectedValue+" order by CardGradeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CardGradeID, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CardTypeID.Items.Clear();
            this.CardGradeID.Items.Clear();
            this.CouponTypeID.Items.Clear();

            this.CardTypeID.Items.Add(new FineUI.ListItem());
            DataSet ds = new Edge.SVA.BLL.CardType().GetList(" BrandID="+this.BrandID.SelectedValue+" order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CardTypeID, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");

            this.CouponTypeID.Items.Add(new FineUI.ListItem());
            ds = new Edge.SVA.BLL.CouponType().GetList(" BrandID=" + this.BrandID.SelectedValue + " order by CouponTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CouponTypeID, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");

        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");
            ////if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            ////{
            ////    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistDistributeTemplateCode, FineUI.MessageBoxIcon.Warning);
            ////    return;
            ////}
            Edge.SVA.Model.MemberNotice_Filter item = this.GetAddObject();

            if (item != null)
            {
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.DistributionCode = item.DistributionCode.ToUpper();

                //item.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");

            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.MemberNotice_Filter>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice_Filter Add Success\tCode:" + item.NoticeNumber+" KeyID "+item.KeyID);
                messageNotifyUtil.UpdateStatusP(item.NoticeNumber);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice_Filter Add Failed\tCode:" + item == null ? "No Data" : item.NoticeNumber+" KeyID "+item.KeyID);
                ShowAddFailed();
            }

        }
    }
}
