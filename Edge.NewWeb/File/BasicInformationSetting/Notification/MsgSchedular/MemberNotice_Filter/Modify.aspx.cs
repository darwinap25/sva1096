﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;

namespace Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_Filter
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberNotice_Filter, Edge.SVA.Model.MemberNotice_Filter>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);


                RegisterCloseEvent(btnClose);
            }
        }
        protected override void DoAfterModelSet()
        {
            base.DoAfterModelSet();

            ModifyInitData();
        }
        private void ModifyInitData()
        {
            this.CardTypeID.Items.Clear();
            this.CardGradeID.Items.Clear();
            this.CouponTypeID.Items.Clear();

            this.CardTypeID.Items.Add(new FineUI.ListItem());
            DataSet ds = new Edge.SVA.BLL.CardType().GetList(" BrandID=" + Model.BrandID.ToString() + " order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CardTypeID, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");

            this.CouponTypeID.Items.Add(new FineUI.ListItem());
            ds = new Edge.SVA.BLL.CouponType().GetList(" BrandID=" + Model.BrandID.ToString() + " order by CouponTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CouponTypeID, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");

            if (Model.CardTypeID != null)
            {
                this.CardGradeID.Items.Add(new FineUI.ListItem());
                ds = new Edge.SVA.BLL.CardGrade().GetList(" CardTypeID=" + Model.CardTypeID + " order by CardGradeCode");
                Edge.Web.Tools.ControlTool.BindDataSet(this.CardGradeID, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");

            }
        }
        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CardGradeID.Items.Clear();
            this.CardGradeID.Items.Add(new FineUI.ListItem());
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(" CardTypeID=" + this.CardTypeID.SelectedValue + " order by CardGradeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CardGradeID, ds, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3", "CardGradeCode");
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CardTypeID.Items.Clear();
            this.CardGradeID.Items.Clear();
            this.CouponTypeID.Items.Clear();

            this.CardTypeID.Items.Add(new FineUI.ListItem());
            DataSet ds = new Edge.SVA.BLL.CardType().GetList(" BrandID=" + this.BrandID.SelectedValue + " order by CardTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CardTypeID, ds, "CardTypeID", "CardTypeName1", "CardTypeName2", "CardTypeName3", "CardTypeCode");

            this.CouponTypeID.Items.Add(new FineUI.ListItem());
            ds = new Edge.SVA.BLL.CouponType().GetList(" BrandID=" + this.BrandID.SelectedValue + " order by CouponTypeCode");
            Edge.Web.Tools.ControlTool.BindDataSet(this.CouponTypeID, ds, "CouponTypeID", "CouponTypeName1", "CouponTypeName2", "CouponTypeName3", "CouponTypeCode");

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                }
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            ////if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            ////{
            ////    FineUI.Alert.ShowInTop(Resources.MessageTips.ExistDistributeTemplateCode, FineUI.MessageBoxIcon.Warning);
            ////    return;
            ////}
            Edge.SVA.Model.MemberNotice_Filter item = this.GetUpdateObject();

            if (item != null)
            {
                //item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.CreatedOn = System.DateTime.Now;
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.DistributionCode = item.DistributionCode.ToUpper();

                //item.TemplateFile = this.TemplateFile.SaveToServer("Files\\DistributeTemplate");

            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.MemberNotice_Filter>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice_Filter Update Success\tCode:" + item.KeyID.ToString());
                messageNotifyUtil.UpdateStatusP(item.NoticeNumber);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "MemberNotice_Filter Update Failed\tCode:" + item == null ? "No Data" : item.KeyID.ToString());
                ShowUpdateFailed();
            }
        }
    }
}
