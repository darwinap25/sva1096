﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Notification.MsgSchedular.MemberNotice_MessageType.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
</head>
<body>
    <form id="form1" runat="server">
     <ext:PageManager ID="PageManager1" AutoSizePanelID="Grid1" runat="server" />
    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
        runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
        ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnDelete" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Columns>
            <ext:TemplateField Width="0px" HeaderText="通知编号" Hidden=true>
                <ItemTemplate>
                    <asp:Label ID="NoticeNumber" runat="server" Text='<%# Eval("NoticeNumber") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="发送模版">
                <ItemTemplate>
                    <asp:Label ID="MessageTemplateCode" runat="server" Text='<%# Eval("MessageTemplateCode") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="是否生效">
                <ItemTemplate>
                    <asp:Label ID="Status" runat="server" Text='<%# Eval("StatusName") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>            
<%--            <ext:TemplateField Width="60px" HeaderText="发送频率单位">
                <ItemTemplate>
                   <asp:Label ID="FrequencyUnitName" runat="server" Text='<%#Eval("FrequencyUnitName")%>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>                      
            <ext:TemplateField Width="60px" HeaderText="发送频率数量">
                <ItemTemplate>
                    <asp:Label ID="FrequencyValue" runat="server" Text='<%# Eval("FrequencyValue") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>--%>
            <ext:TemplateField Width="60px" HeaderText="发送日期">
                <ItemTemplate>
                    <asp:Label ID="FrequencyValue" runat="server" Text='<%# Eval("SendTime","{0:yyyy-MM-dd}") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="发送时间">
                <ItemTemplate>
                    <asp:Label ID="SendTime" runat="server" Text='<%# Eval("SendTime","{0:HH:mm:ss}") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>      
            <ext:WindowField ColumnID="ViewWindowField" Width="60px" WindowID="Window1" Icon="Page"
                Text="查看" ToolTip="查看" DataTextFormatString="{0}" DataIFrameUrlFields="KeyID"
                DataIFrameUrlFormatString="Show.aspx?id={0}" 
                Title="查看" />
            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                Text="编辑" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="KeyID"
                DataIFrameUrlFormatString="Modify.aspx?id={0}" 
                Title="编辑" />
        </Columns>
    </ext:Grid>
    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="740px"
        Height="400px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="true" EnableResize="true" Target="Top" IsModal="True" Width="740px"
        Height="400px">
    </ext:Window> 
    <ext:Label ID="PrivateKey" runat="server" Label="Label" Text="" Hidden=true>
    </ext:Label>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
