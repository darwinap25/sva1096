﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.Company.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="CompanyCode" runat="server" Label="公司编号：" Required="true" ShowRedStar="true" Enabled="false" />
            <ext:TextBox ID="CompanyName" runat="server" Label="描述：" Required="true" ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
<%--            <ext:TextBox ID="SupplierDesc2" runat="server" Label="其他描述1：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:TextBox ID="SupplierDesc3" runat="server" Label="其他描述2：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>--%>
            <ext:TextBox ID="CompanyAddress" runat="server" Label="地址：" Required="true" ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
<%--            <ext:TextBox ID="OtherAddress1" runat="server" Label="其他地址1：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:TextBox ID="OtherAddress2" runat="server" Label="其他地址2：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>--%>
            <ext:TextBox ID="CompanyTelNum" runat="server" Label="联系电话：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:TextBox ID="CompanyFaxNum" runat="server" Label="传真：" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
            <ext:RadioButtonList ID="isDefault" runat="server" Label="是否默认：" AutoPostBack="true" Required="true" ShowRedStar="true">
                <ext:RadioItem Text="是" Value="1" />
                <ext:RadioItem Text="否" Value="0" Selected="true" />
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>


