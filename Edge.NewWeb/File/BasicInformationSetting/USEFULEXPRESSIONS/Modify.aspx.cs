﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;

namespace Edge.Web.File.BasicInformationSetting.USEFULEXPRESSIONS
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.USEFULEXPRESSIONS, Edge.SVA.Model.USEFULEXPRESSIONS>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        USEFULEXPRESSIONSModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                SVASessionInfo.USEFULEXPRESSIONSModifyController = null;
                controller = SVASessionInfo.USEFULEXPRESSIONSModifyController;

            }
            controller = SVASessionInfo.USEFULEXPRESSIONSModifyController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                controller.LoadViewModel(Model.USEFULEXPRESSIONSID, SVASessionInfo.SiteLanguage);
                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = Model.PhrasePicFile;
                    //存在会员相片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.PhrasePicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = controller.ViewModel.MainTable.PhrasePicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    this.PhraseContent1.Text = controller.ViewModel.MainTable.PhraseContent1;
                    this.PhraseContent2.Text = controller.ViewModel.MainTable.PhraseContent2;
                    this.PhraseContent3.Text = controller.ViewModel.MainTable.PhraseContent3;
                }
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (ValidateForm())
            {
                if (controller.ViewModel.MainTable != null)
                {
                    if (!string.IsNullOrEmpty(this.PhrasePicFile.ShortFileName) && this.FormLoad.Hidden == false)
                    {
                        if (!ValidateImg(this.PhrasePicFile.FileName))
                        {
                            return;
                        }
                        controller.ViewModel.MainTable.PhrasePicFile = this.PhrasePicFile.SaveToServer("Files/USEFULEXPRESSIONS");
                    }
                    else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                    {
                        if (!ValidateImg(this.uploadFilePath.Text))
                        {
                            return;
                        }
                        controller.ViewModel.MainTable.PhrasePicFile = this.uploadFilePath.Text;
                    }
                }
                ExecResult er = controller.Submit();
                if (er.Success)
                {
                    if (this.FormReLoad.Hidden == true && string.IsNullOrEmpty(controller.ViewModel.MainTable.PhrasePicFile))
                    {
                        DeleteFile(this.uploadFilePath.Text);
                    }
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "USEFULEXPRESSIONS Update\t Code:" + controller.ViewModel.MainTable.USEFULEXPRESSIONSID.ToString());
                    CloseAndPostBack();
                }
                else
                {
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "USEFULEXPRESSIONS Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.USEFULEXPRESSIONSID.ToString());
                    ShowUpdateFailed();
                }
            }
        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        public bool ValidateForm()
        {
            if (string.IsNullOrEmpty(this.PhraseContent1.Text) || this.PhraseContent1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.PhraseContent1CannotBeEmpty);
                return false;
            }

            return true;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}