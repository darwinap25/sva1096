﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model;
using Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;


namespace Edge.Web.File.BasicInformationSetting.USEFULEXPRESSIONS
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.USEFULEXPRESSIONS, Edge.SVA.Model.USEFULEXPRESSIONS>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        USEFULEXPRESSIONSAddController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                SVASessionInfo.USEFULEXPRESSIONSAddController = null;
            }
            controller = SVASessionInfo.USEFULEXPRESSIONSAddController;
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            controller.ViewModel.MainTable = this.GetAddObject();
            if (ValidateForm())
            {
                if (controller.ViewModel.MainTable != null)
                {
                    if (!ValidateImg(this.PhrasePicFile.FileName))
                    {
                        return;
                    }
                    HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
                    controller.ViewModel.MainTable.PhrasePicFile = clearTool.ClearSource(this.PhrasePicFile.Text).Replace("\r\n", "");
                    controller.ViewModel.MainTable.PhrasePicFile = this.PhrasePicFile.SaveToServer("Files/USEFULEXPRESSIONS");
                }

                ExecResult er = controller.Submit();
                if (er.Success)
                {
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add USEFULEXPRESSIONS Success Code:" + controller.ViewModel.MainTable.Code);
                    CloseAndRefresh();
                }
                else
                {
                    Tools.Logger.Instance.WriteOperationLog(this.PageName, "Add USEFULEXPRESSIONS Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.Code.ToString());
                    ShowAddFailed();
                }

            }
        }

        public bool ValidateForm()
        {
            if (string.IsNullOrEmpty(this.PhraseContent1.Text) || this.PhraseContent1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.PhraseContent1CannotBeEmpty);
                return false;
            }

            return true;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}