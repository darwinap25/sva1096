﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.USEFULEXPRESSIONS
{
    public partial class Show : Tools.BasePage<Edge.SVA.BLL.USEFULEXPRESSIONS, Edge.SVA.Model.USEFULEXPRESSIONS>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        USEFULEXPRESSIONSModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "show");
                RegisterCloseEvent(btnClose);

                SVASessionInfo.USEFULEXPRESSIONSModifyController = null;
                controller = SVASessionInfo.USEFULEXPRESSIONSModifyController;

            }
            controller = SVASessionInfo.USEFULEXPRESSIONSModifyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.USEFULEXPRESSIONSID, SVASessionInfo.SiteLanguage);
                if (controller.ViewModel.MainTable != null)
                {
                    this.btnPreview.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.PhrasePicFile) ? true : false;//没有照片时不显示查看按钮(Len)

                    this.uploadFilePath.Text = controller.ViewModel.MainTable.PhrasePicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("~/TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                }
            }
        }
    }
}