﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CSVXMLBinding.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
        
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="Basic Settings"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Label ID="BindingCode" runat="server" Label="Binding Code：" />
                    <ext:Label ID="Description" runat="server" Label="Description：" />
                    <ext:Label ID="Func" runat="server" Label="Function：" />
              <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
          </Items>
        </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="CSV-XML Information"
                AutoHeight="true" AutoWidth="true">
            <Items>
                    <ext:Label ID="CSVColumnName" runat="server" Label="CSV Column Name：" />
                    <ext:Label ID="XMLFieldName" runat="server" Label="XML Field Name：" />
                    <ext:Label ID="DataType" runat="server" Label="Data Type：" />
                    <ext:Label ID="FixedValue" runat="server" Label="Fixed Value：" />
                    <ext:Label ID="Length" runat="server" Label="Length：" />
                    <ext:Label ID="SaveToDB" runat="server" Label="Save in Database?" />
            </Items>
        </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
