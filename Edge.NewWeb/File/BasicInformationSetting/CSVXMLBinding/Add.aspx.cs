﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLBinding;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CSVXMLBinding
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CSVXMLBinding, Edge.SVA.Model.CSVXMLBinding>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CSVXMLBindingController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.CSVXMLBindingController = null;
            }
            controller = SVASessionInfo.CSVXMLBindingController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            string message = controller.ValidataObject(this.BindingCode.Text);
            if (message != "")
            {
                FineUI.Alert.ShowInTop(message, FineUI.MessageBoxIcon.Warning);
                return;
            }

            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.Func = this.Func1.SelectedValue.ToString();
                controller.ViewModel.MainTable.BindingCode = controller.ViewModel.MainTable.BindingCode.ToUpper();
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "CSVXMLBinding Add\t Code:" + controller.ViewModel.MainTable.BindingCode); 
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "CSVXMLBinding Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.BindingCode);
                ShowAddFailed();
            }
        }
    }
}
