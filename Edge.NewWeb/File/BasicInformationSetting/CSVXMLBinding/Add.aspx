﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.BasicInformationSetting.CSVXMLBinding.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="4px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="Basic Settings"
                AutoHeight="true" AutoWidth="true">
                <Items>
                     <ext:TextBox ID="BindingCode" runat="server" Label="Binding Code：" Required="true" ShowRedStar="true"
                         MaxLength="64" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" />
                     <ext:TextBox ID="Description" runat="server" Label="描述：" Required="true" ShowRedStar="true" ToolTipTitle="描述"
                         OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" />
                     <ext:DropDownList ID="Func1" runat="server" Label="Function：" Resizable="true" Required="true" ShowRedStar="true" >
                       <ext:ListItem Text="Member Import" Value="Member Import"  Selected="true"/>
                       <ext:ListItem Text="Points Adjustment" Value="Points Adjustment" />
                     </ext:DropDownList>        
                     <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="CSV-XML Information"
                AutoHeight="true" AutoWidth="true">
            <Items>
                <ext:TextBox ID="CSVColumnName" runat="server" Label="CSV Column Name：" ToolTipTitle="CSV Column Name" ToolTip="CSV Column Name"
                    OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" Required="true" ShowRedStar="true"/>
                <ext:TextBox ID="XMLFieldName" runat="server" Label="XML Field Name：" ToolTipTitle="XML Field Name" ToolTip="XML Field Name"
                    OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true" Required="true" ShowRedStar="true"/>
                <ext:DropDownList ID="DataType" runat="server" Label="Data Type：" Resizable="true" Required="true" ShowRedStar="true" >
                     <ext:ListItem Text="int" Value="1"  Selected="true"/>
                     <ext:ListItem Text="string" Value="2" />
                     <ext:ListItem Text="datetime" Value="3" />
                </ext:DropDownList>
                <ext:TextBox ID="FixedValue" runat="server" Label="Fixed Value：" ToolTipTitle="Fixed Value" ToolTip="Fixed Value"
                    OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
                <ext:TextBox ID="Length" runat="server" Label="Length：" ToolTipTitle="Length" ToolTip="Length"
                    OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"/>
                <ext:DropDownList ID="SaveToDB" runat="server" Label="Save in Database?" Resizable="true" >
                     <ext:ListItem Text="Yes" Value="1"  Selected="true"/>
                     <ext:ListItem Text="No" Value="0" />
                </ext:DropDownList>
            </Items>
        </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
