﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLBinding;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.BasicInformationSetting.CSVXMLBinding
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CSVXMLBinding, Edge.SVA.Model.CSVXMLBinding>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CSVXMLBindingController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " show");
                RegisterCloseEvent(btnClose);
                SVASessionInfo.CSVXMLBindingController = null;
            }
            controller = SVASessionInfo.CSVXMLBindingController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.BindingCode);


            }
        }
       
    }
}
