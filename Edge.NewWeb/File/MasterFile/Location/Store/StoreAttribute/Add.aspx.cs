﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model.Domain.File;
using Edge.Utils.Tools;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;
using System.Data;

namespace Edge.Web.File.MasterFile.Location.Store.StoreAttribute
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreAttributeList, Edge.SVA.Model.StoreAttributeList>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["RuleType"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.RuleType.Text = Request["RuleType"].ToString();
                }
                if (Request["PageFlag"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.PageFlag.Text = Request["PageFlag"].ToString();
                }
                string StoreID = Request["StoreID"].ToString();

                if (Request["StoreID"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    ViewState["StoreID"] = Request["StoreID"];
                    setStoreCode(StoreID);
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindStoreAttributeCodeByStoreID(SAID, "1 = 1");

                //BrandID_SelectedIndexChanged(null, null);
            }
        }

        private void setStoreCode(string StoreID)
        {
            string _StoreCode = "";
            Edge.SVA.BLL.Store model = new Edge.SVA.BLL.Store();

            Edge.Web.Tools.ControlTool.BindStoreCodeByStoreID(this.StoreID,"");
            this.StoreID.SelectedValue = StoreID;
            this.StoreID.Enabled = false;
            
        }
        //protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int brandID = 0;
        //    brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
        //    ControlTool.BindCouponType(CouponTypeID, string.Format("BrandID = {0} order by CouponTypeCode", brandID));
        //}
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            int _StoreID = StringHelper.ConvertToInt(ViewState["StoreID"].ToString());
            int _StoreAttributeID = StringHelper.ConvertToInt(this.SAID.SelectedValue);
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.IsHasStoreCodeAttributeCode(ViewState["StoreID"].ToString(), _StoreAttributeID))
            {
                ShowWarning(Resources.MessageTips.ExistStoreAttributeCode);
                this.SAID.Focus();
                return;
            }

            Edge.SVA.Model.StoreAttributeList item = this.GetAddObject();

           
            if (item != null)
            {
                item.StoreID = _StoreID;
                item.SAID = _StoreAttributeID;
               
            }

            if (DALTool.Add<Edge.SVA.BLL.StoreAttributeList>(item) > 0)
            {
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

    }
}