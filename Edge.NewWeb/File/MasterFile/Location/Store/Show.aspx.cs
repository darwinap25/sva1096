﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Text;
using System.Data;

namespace Edge.Web.File.MasterFile.Location.Store
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store,Edge.SVA.Model.Store>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        StoreController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Show");
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.StoreController = null;

                ControlTool.BindStoreGroup(this.StoreGroupID);
                BindingDataGrid_CanHoldCouponGradeList();
            }
            controller = SVASessionInfo.StoreController;
        }
        // Add by Alex 2014-07-01 ++
        #region CardGradeHoldCouponType

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in this.Grid_StoreAttributeList.SelectedRowIndexArray)
            {
                sb.Append(Grid_StoreAttributeList.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString());
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("StoreAttribute/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));
            BindingDataGrid_CanHoldCouponGradeList();
        }

        private void BindingDataGrid_CanHoldCouponGradeList()
        {
            ViewState["sotrID"] = Request["id"];
            string sotrID = "";
            if (ViewState["sotrID"] != null && ViewState["sotrID"].ToString() != "")
                sotrID = ViewState["sotrID"].ToString();


            StringBuilder sb = new StringBuilder("");
            //   string code = controller.ViewModel.MainTable.StoreCode;

            //if (!string.IsNullOrEmpty(sotrID))
            //{
            //    string  StoreAttributeID = "";
            //    sb.Append(" StoreAttributeID in (select StoreAttributeID from StoreAttributeList where StoreID in ( ");

            //    sb.Append(sotrID);
            //    sb.Append("))");
            //}

            //strWhere = sb.ToString();

            //ViewState["strWhere"] = strWhere;


            //StoreAttributeListController AttributeController = new StoreAttributeListController();

            //int count = 0;
            //DataSet ds = AttributeController.GetTransactionList(strWhere, this.Grid_StoreAttributeList.PageSize, this.Grid_StoreAttributeList.PageIndex, out count, "");
            //this.Grid_StoreAttributeList.RecordCount = count;
            //if (ds != null)
            //{
            //    this.Grid_StoreAttributeList.DataSource = ds.Tables[0].DefaultView;
            //    this.Grid_StoreAttributeList.DataBind();
            //}
            //else
            //{
            //    this.Grid_StoreAttributeList.Reset();
            //}


            Edge.SVA.BLL.Store store = new SVA.BLL.Store();
            DataSet ds = store.GetStoreAttributeList(sotrID);

            if (ds != null)
            {
                this.Grid_StoreAttributeList.DataSource = ds.Tables[0].DefaultView;
                this.Grid_StoreAttributeList.DataBind();
            }
            else
            {
                this.Grid_StoreAttributeList.Reset();
            }



        }
        protected void Grid_StoreAttribute_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_StoreAttributeList.PageIndex = e.NewPageIndex;
            BindingDataGrid_CanHoldCouponGradeList();

        }



        private void BindingDataGrid_GetCouponGradeList()
        {
            //this.Grid_GetCouponGradeList.RecordCount = controller.ViewModel.GetCouponGradeList.Count;
            //this.Grid_GetCouponGradeList.DataSource = controller.ViewModel.GetCouponGradeList;
            //this.Grid_GetCouponGradeList.DataBind();
        }
        protected void Grid_GetCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            //this.Grid_GetCouponGradeList.PageIndex = e.NewPageIndex;
        }
        #endregion
        // Add by Alex 2014-07-01 --
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                //Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                //if (brand != null)
                //{
                //    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                //    this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                //    this.BrandID.Text = brand.BrandCode + "-" + DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                //}
                //else
                //{
                //    this.lblCardIssuer.Text = "";
                //}

                //Edge.SVA.Model.StoreType storeType = new Edge.SVA.BLL.StoreType().GetModel(Model.StoreTypeID.GetValueOrDefault());
                //string text = storeType == null ? "" : DALTool.GetStringByCulture(storeType.StoreTypeName1, storeType.StoreTypeName2, storeType.StoreTypeName3);
                //this.StoreTypeID.Text = storeType == null ? "" : ControlTool.GetDropdownListText(text, storeType.StoreTypeCode);

                //if (Model != null)
                //{
                //    //地址
                //    Edge.SVA.Model.Country country = new Edge.SVA.BLL.Country().GetModel(Model.StoreCountry);
                //    this.StoreCountry.Text = country == null ? "" : DALTool.GetStringByCulture(country.CountryName1, country.CountryName2, country.CountryName3);
                //    Edge.SVA.Model.Province province = new Edge.SVA.BLL.Province().GetModel(Model.StoreProvince);
                //    this.StoreProvince.Text = province == null ? "" : DALTool.GetStringByCulture(province.ProvinceName1, province.ProvinceName2, province.ProvinceName3);
                //    Edge.SVA.Model.City city = new Edge.SVA.BLL.City().GetModel(Model.StoreCity);
                //    this.StoreCity.Text = city == null ? "" : DALTool.GetStringByCulture(city.CityName1, city.CityName2, city.CityName3);
                //    Edge.SVA.Model.District district = new Edge.SVA.BLL.District().GetModel(Model.StoreDistrict);
                //    this.StoreDistrict.Text = district == null ? "" : DALTool.GetStringByCulture(district.DistrictName1, district.DistrictName2, district.DistrictName3);
                    
                    
                //    this.uploadFilePath.Text = Model.StorePicFile;

                //    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                //    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.StorePicFile) ? true : false;//没有照片时不显示查看按钮(Len)
                //}
                controller.LoadViewModel(Model.StoreID);
                this.CreatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(controller.ViewModel.MainTable.BrandID.GetValueOrDefault());
                if (brand != null)
                {
                    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                    this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                    this.BrandID.Text = brand.BrandCode + "-" + DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                }
                else
                {
                    this.lblCardIssuer.Text = "";
                }

                Edge.SVA.Model.StoreType storeType = new Edge.SVA.BLL.StoreType().GetModel(controller.ViewModel.MainTable.StoreTypeID.GetValueOrDefault());
                string text = storeType == null ? "" : DALTool.GetStringByCulture(storeType.StoreTypeName1, storeType.StoreTypeName2, storeType.StoreTypeName3);
                this.StoreTypeID.Text = storeType == null ? "" : ControlTool.GetDropdownListText(text, storeType.StoreTypeCode);

                //this.StoreGroupID.SelectedValue = Model.StoreGroupID.ToString() ;
                this.StoreGroupView.Text = this.StoreGroupID.SelectedText;
                //this.Status.SelectedValue = Model.Status.ToString();
                this.StatusView.Text = this.Status.SelectedText;

                if (Model != null)
                {
                    //地址
                    Edge.SVA.Model.Country country = new Edge.SVA.BLL.Country().GetModel(controller.ViewModel.MainTable.StoreCountry);
                    this.StoreCountry.Text = country == null ? "" : DALTool.GetStringByCulture(country.CountryName1, country.CountryName2, country.CountryName3);
                    Edge.SVA.Model.Province province = new Edge.SVA.BLL.Province().GetModel(controller.ViewModel.MainTable.StoreProvince);
                    this.StoreProvince.Text = province == null ? "" : DALTool.GetStringByCulture(province.ProvinceName1, province.ProvinceName2, province.ProvinceName3);
                    Edge.SVA.Model.City city = new Edge.SVA.BLL.City().GetModel(controller.ViewModel.MainTable.StoreCity);
                    this.StoreCity.Text = city == null ? "" : DALTool.GetStringByCulture(city.CityName1, city.CityName2, city.CityName3);
                    Edge.SVA.Model.District district = new Edge.SVA.BLL.District().GetModel(controller.ViewModel.MainTable.StoreDistrict);
                    this.StoreDistrict.Text = district == null ? "" : DALTool.GetStringByCulture(district.DistrictName1, district.DistrictName2, district.DistrictName3);

                    this.PickupStoreFlagView.Text = this.PickupStoreFlag.SelectedItem == null ? "" : this.PickupStoreFlag.SelectedItem.Text;

                    this.uploadFilePath.Text = Model.StorePicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    this.btnPreview.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.StorePicFile) ? true : false;//没有照片时不显示查看按钮(Len)
                }
            }
        }
    }
}
