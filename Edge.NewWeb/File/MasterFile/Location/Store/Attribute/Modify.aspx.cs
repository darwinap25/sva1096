﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using System.IO;

namespace Edge.Web.File.MasterFile.Location.Store.Attribute
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store_Attribute, Edge.SVA.Model.Store_Attribute>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //Edge.Web.Tools.ControlTool.BindStoreCode(StoreCode); 
            }
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    //存在会员相片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.SAPic))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = Model.SAPic;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                    
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.Store_Attribute item = this.GetUpdateObject();

            if (item != null)
            {
                //item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                //item.UpdatedOn = System.DateTime.Now;
                //item.StoreID = item.StoreID;
                //item.StoreCode = item.StoreCode.ToUpper();
                item.SACode = item.SACode.ToUpper();
                item.SADesc1 = item.SADesc1;
                item.SADesc2 = item.SADesc2;
                item.SADesc3 = item.SADesc3;
                //item.OrganizationPicFile = this.OrganizationPicFile.SaveToServer("Images/Organization");
                if (!string.IsNullOrEmpty(this.picSFile.ShortFileName) && this.FormLoad.Hidden == false)
                {
                    if (!ValidateImg(this.picSFile.FileName))
                    {
                        return;
                    }
                    item.SAPic = this.picSFile.SaveToServer("Images/StoreAttribute");
                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateImg(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    item.SAPic = this.uploadFilePath.Text;
                }
            }

            if (Edge.Web.Tools.DALTool.isHasStoreAttributeCode(this.SACode.Text.Trim(), item.SAID))
            {
                ShowWarning(Resources.MessageTips.ExistStoreAttributeCode);
                this.SACode.Focus();
                return;
            }

            //if (!Edge.Web.Tools.DALTool.isHasStoreAttributeCodeWithStore(this.StoreAttributeCode.Text.Trim(), item.KeyID, this.StoreCode.Text.Trim()))
            //{
            //    ShowWarning(Resources.MessageTips.ExistStoreAttributeCodeWith);
            //    this.StoreAttributeCode.Focus();
            //    return;
            //}

            if (DALTool.Update<Edge.SVA.BLL.Store_Attribute>(item))
            {
                if (this.FormReLoad.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.picSFile.ShortFileName))
            {
                this.PicturePath.Text = this.picSFile.SaveToServer("Images/StoreAttribute");
                FineUI.PageContext.RegisterStartupScript(WindowPic1.GetShowReference("../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.PicturePath.Text);
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}