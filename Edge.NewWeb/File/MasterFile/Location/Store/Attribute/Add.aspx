﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Store.Attribute.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <%--            <ext:TextBox ID="StoreCode" runat="server" Label="店铺编号：" MaxLength="20" Required="true"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true">
            </ext:TextBox>--%>
<%--            <ext:DropDownList ID="StoreCode" runat="server" Label="店铺编号：" Required="true" ShowRedStar="true"
                AutoPostBack="true" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值">
            </ext:DropDownList>--%>
            <ext:TextBox ID="SACode" runat="server" Label="店铺属性编号：" MaxLength="64"
                Required="true" ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" ToolTip="只能输入字母和数字,不能超過64個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="SADesc1" runat="server" Label="描述1：" MaxLength="512" Required="true"
                ShowRedStar="true" ToolTipTitle="描述" ToolTip="不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="SADesc2" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                ToolTip="对其他描述2,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="SADesc3" runat="server" Label="其他描述3：" MaxLength="512" ToolTipTitle="其他描述3"
                ToolTip="对其他描述3,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:HiddenField ID="uploadFilePath" runat="server">
            </ext:HiddenField>
            <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false"
                runat="server">
                <Rows>
                    <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                        <Items>
                            <ext:FileUpload ID="SAPic" runat="server" Label="店铺属性图片：" ToolTipTitle="图片"
                                ToolTip="Translate__Special_121_Start店铺属性的图片。点击按钮进行上传，上传的文件支持JPG,GIF，PNG，BMP,文件大小不能超过10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnPreview" runat="server" Text="预览" OnClick="ViewPicture" Hidden="true">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Window ID="WindowPic" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
                CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
                EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
                Height="450px">
            </ext:Window>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项" CssStyle="font-size:12px;color:red">
            </ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
