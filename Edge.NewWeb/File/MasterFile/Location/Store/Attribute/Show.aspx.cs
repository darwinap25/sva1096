﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;

namespace Edge.Web.File.MasterFile.Location.Store.Attribute
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store_Attribute, Edge.SVA.Model.Store_Attribute>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                btnClose.OnClientClick = ActiveWindow.GetConfirmHidePostBackReference();
            }
            logger.WriteOperationLog(this.PageName, "Show");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);


                //this.CallInterfaceView.Text = this.CallInterface.SelectedItem == null ? "" : this.CallInterface.SelectedItem.Text;
                //this.CallInterface.Hidden = true;
                if (Model != null)
                {
                    this.uploadFilePath.Text = Model.SAPic;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.SAPic) ? true : false;//没有照片时不显示查看按钮(Len)
                }
            }
        }
    }
}