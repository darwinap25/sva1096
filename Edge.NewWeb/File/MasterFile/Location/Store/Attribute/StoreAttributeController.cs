﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Edge.Web.File.MasterFile.Location.Store.Attribute
{
    public class StoreAttributeController
    {
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Store_Attribute bll = new Edge.SVA.BLL.Store_Attribute();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "StoreAttributeCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            //Tools.DataTool.AddOrganizationName(ds, "OrganizationName", "OrganizationID");

            return ds;
        }
    }
}