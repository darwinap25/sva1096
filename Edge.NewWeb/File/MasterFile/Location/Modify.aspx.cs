﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.MasterFile.Location
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreGroup, Edge.SVA.Model.StoreGroup>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.StoreGroup item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
            }
            
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.StoreGroup>(item))
            {
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }


    }
}
