﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;


namespace Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion
{
    public partial class ModifyEndDate : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Promotion_H, Edge.SVA.Model.Promotion_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        BuyingNewPromotionController controller = new BuyingNewPromotionController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //对详情表的操作
                this.WindowSearch.Title = "搜索";
                //if (this.Grid1.RecordCount == 0)
                //{
                //    this.btnClearAllHitItem.Enabled = false;
                //}
                //else
                //{
                //    this.btnClearAllHitItem.Enabled = true;
                //}
                SVASessionInfo.BuyingNewPromotionController = null;
                InitData();

                controller = SVASessionInfo.BuyingNewPromotionController;
                if (Request.QueryString["iscopy"] != null)
                {
                    controller.IsCopy = true;
                }

            }
            else
            {
                controller = SVASessionInfo.BuyingNewPromotionController;
            }
        }
        
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.PromotionCode, SVASessionInfo.SiteLanguage);


                if (controller.ViewModel.MainTable != null)
                {
                    this.StartTime.Text = controller.ViewModel.MainTable.StartTime.HasValue ? controller.ViewModel.MainTable.StartTime.Value.ToString("HH:mm:ss") : string.Empty;
                    this.EndTime.Text = controller.ViewModel.MainTable.EndTime.HasValue ? controller.ViewModel.MainTable.EndTime.Value.ToString("HH:mm:ss") : string.Empty;
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");

            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (controller.ViewModel.MainTable != null)
            {
                DateTime dt = DateTime.Now;
                controller.ViewModel.MainTable.StartTime = DateTime.Parse(dt.ToString("yyyy-MM-dd ") + this.StartTime.Text);
                controller.ViewModel.MainTable.EndTime = DateTime.Parse(dt.ToString("yyyy-MM-dd ") + this.EndTime.Text);

                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = dt;
            }

            ExecResult er = controller.Update(); //controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Update Promotion Success Code:" + controller.ViewModel.MainTable.PromotionCode);
                if (controller.IsCopy)
                {
                    CloseAndRefresh();
                }
                else 
                {
                    CloseAndPostBack();
                }
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Update Promotion Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.PromotionCode);
                ShowSaveFailed(er.Message);
            }

        }


        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
        }

        protected void InitData()
        {
        }

    }
}