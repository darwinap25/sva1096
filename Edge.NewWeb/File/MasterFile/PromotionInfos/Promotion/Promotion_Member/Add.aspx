﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Member.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                       OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="PromotionCode" runat="server" Label="促销编号：" Required="true" ShowRedStar="true" Enabled="false"/>
            <ext:DropDownList ID="LoyaltyType" runat="server" Label="促销会员范围：" Resizable="true" 
                 OnSelectedIndexChanged="LoyaltyType_SelectedIndexChanged" AutoPostBack="true" ToolTip="请选择促销会员范围"/>
            <ext:DropDownList ID="LoyaltyValue" runat="server" Label="促销会员范围值：" ToolTip="请选择促销会员范围值"/>
            <ext:NumberBox ID="LoyaltyThreshold" runat="server" Label="促销指定的会员忠诚度阀值：" ToolTip="请输入促销指定的会员忠诚度阀值" NoDecimal="true" MaxLength="9"/>
            <ext:DropDownList ID="LoyaltyBirthdayFlag" runat="server" Label="是否销售当日生日促销：" ToolTip="请选择是否销售当日生日促销"/>               
            <ext:DropDownList ID="LoyaltyPromoScope" runat="server" Label="促销指定的会员范围：" ToolTip="请选择促销指定的会员范围"/>      
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
