﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Hit.Promotion_Hit_PLU.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="SimpleForm1" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                       OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
        <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" AutoHeight="true" AutoWidth="true" Title="基础内容">
            <Items>
                <ext:SimpleForm ID="SimpleForm2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server"  LabelAlign="Right">
                    <Items>
                        <ext:NumberBox ID="HitPLUSeq" runat="server" Label="命中条件序号：" Required="true" ShowRedStar="true" ToolTip="请输入命中条件序号，不能超过10个字符"/>
                        <ext:DropDownList ID="PLUHitType" runat="server" Label="货品命中类型："  CompareOperator="GreaterThan" CompareValue="0" Resizable="true" ToolTip="请选择货品命中类型">
                        </ext:DropDownList>
                        <ext:DropDownList ID="HitSign" runat="server" Label="货品过滤条件："  CompareOperator="GreaterThan" CompareValue="0" Resizable="true" ToolTip="请选择货品过滤条件">
                        </ext:DropDownList>
                        <ext:DropDownList ID="EntityType" runat="server" Label="实体类型：" Resizable="true"
                            OnSelectedIndexChanged="EntityType_SelectedIndexChanged" AutoPostBack="true" 
                            ShowRedStar="true" Required="true" CompareOperator="GreaterThan" CompareValue="0"
                            ToolTip="请选择产品分类类别">
                        </ext:DropDownList>
                        <ext:Form ID="SearchForm" ShowBorder="false" BodyPadding="0px" EnableBackgroundColor="true"
                            ShowHeader="False" runat="server" LabelAlign="Right">
                            <Rows>
                                <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="90% 5% 5%">
                                    <Items>
                                        <ext:TextBox ID="EntityNum" runat="server" Label="实体号码：" ShowRedStar="true" Required="true" Enabled="false"></ext:TextBox>
                                        <ext:Button ID="btnSelect" runat="server" Text="..." ValidateForms="SimpleForm2"></ext:Button>
                                        <ext:Label ID="Label3" runat="server" Hidden="true" HideMode="Display"></ext:Label>
                                    </Items>
                                </ext:FormRow>
                            </Rows>
                        </ext:Form>
                    </Items>
                </ext:SimpleForm>            
            </Items>
        </ext:GroupPanel>
        <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" AutoHeight="true" AutoWidth="true" Title="列表">
            <Items>
                <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="10"
                        runat="server" EnableCheckBoxSelect="False" DataKeyNames="EntityNum" AllowPaging="true"
                        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid1_PageIndexChange">
                        <Columns>
                            <ext:TemplateField Width="60px" HeaderText="实体类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("EntityTypeName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="实体号码">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("EntityNum")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="Window2" Title="Search" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
