﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Edge.Web.Tools;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;


namespace Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Hit.Promotion_Hit_PLU
{
    public partial class Add : PageBase
    {
        BuyingNewPromotionController controller = new BuyingNewPromotionController();
        //PromotionHitViewModel viewModel = new PromotionHitViewModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            controller = SVASessionInfo.BuyingNewPromotionController;
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(this.btnClose);

                InitData();

                Window2.Title = "搜索";
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            DataTable dt = ViewState["table"] as DataTable;
            if (this.EntityType.SelectedValue == "0")
            {
                DataRow dr = dt.NewRow();
                dr["HitPLUSeq"] = this.HitPLUSeq.Text;
                dr["PLUHitType"] = this.PLUHitType.SelectedValue;
                dr["HitSign"] = this.HitSign.SelectedValue;
                dr["PLUHitTypeName"] = this.PLUHitType.SelectedText;
                dr["HitSignName"] = this.HitSign.SelectedText;
                dr["EntityType"] = this.EntityType.SelectedValue;
                dr["EntityTypeName"] = this.EntityType.SelectedText;

                dt.Rows.Add(dr);
            }
            if (controller.ViewModel.HitPluTable!=null)
            {
                controller.ViewModel.HitPluTable.Merge(dt);                
            }
            else
            {
                controller.ViewModel.HitPluTable = dt;  
            }

            CloseAndPostBack();
        }

        protected void InitData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("KeyID", typeof(int));
            dt.Columns.Add("PromotionCode", typeof(string));
            dt.Columns.Add("HitSeq", typeof(int));
            dt.Columns.Add("HitPLUSeq", typeof(int));
            dt.Columns.Add("EntityNum", typeof(string));
            dt.Columns.Add("EntityType", typeof(int));
            dt.Columns.Add("HitSign", typeof(int));
            dt.Columns.Add("PLUHitType", typeof(int));
            dt.Columns.Add("EntityTypeName", typeof(string));
            dt.Columns.Add("PLUHitTypeName", typeof(string));
            dt.Columns.Add("HitSignName", typeof(string));

            ViewState["table"] = dt;
            ViewState["count"] = 0;
            controller.BindEntityType(this.EntityType, true);
            controller.BindPLUHitType(this.PLUHitType, true);
            controller.BindHitSign(this.HitSign, true);
        }

        protected void EntityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            this.EntityNum.Text = "";
            if (this.EntityType.SelectedValue == "0")
            {
                btnSelect.OnClientClick =string.Empty;
            }
            else if (this.EntityType.SelectedValue == "1")
            {
                btnSelect.OnClientClick = Window2.GetShowReference("../../../../../BasicInformationSetting/Products/List.aspx?multiplepicker=true");
            }
            else if (this.EntityType.SelectedValue == "2")
            {
                btnSelect.OnClientClick = Window2.GetShowReference("../../../../../BasicInformationSetting/Departments/List.aspx?multiplepicker=true");
            }
            else if (this.EntityType.SelectedValue == "3")
            {
                btnSelect.OnClientClick = Window2.GetShowReference("../../../../../MasterFile/IndustryGroup/CardIssuer/Brand/Tender/List.aspx?multiplepicker=true");
            }
            else if (this.EntityType.SelectedValue == "4")
            {
                btnSelect.OnClientClick = Window2.GetShowReference("../../../../../MasterFile/IndustryGroup/CardIssuer/Brand/CouponType/List.aspx?multiplepicker=true");
            }
            else
            {
                btnSelect.OnClientClick = string.Empty;
            }
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.EntityType.SelectedValue == "1")
            {
                this.EntityNum.Text = SVASessionInfo.BuyingProdCode;
            }
            else if (this.EntityType.SelectedValue == "2")
            {
                this.EntityNum.Text = SVASessionInfo.BuyingDepartCode;
            }
            else if (this.EntityType.SelectedValue == "3")
            {
                this.EntityNum.Text = SVASessionInfo.BuyingTendCode;
            }
            else if (this.EntityType.SelectedValue == "4")
            {
                this.EntityNum.Text = SVASessionInfo.BuyingCouponTypeCode;
            }
            else
            {
                this.EntityNum.Text = string.Empty;
            }
            RptBind();
        }

        protected void RptBind()
        {
            DataTable dt = ViewState["table"] as DataTable;
            if (this.EntityType.SelectedValue != "-1" && this.EntityNum.Text != "")
            {
                string[] entitynumbers = this.EntityNum.Text.Split(',');
                DataTable viewDT = dt.Clone();
                foreach (string item in entitynumbers)
                {
                    DataRow dr = viewDT.NewRow();
                    dr["HitPLUSeq"] = this.HitPLUSeq.Text;
                    dr["PLUHitType"] = this.PLUHitType.SelectedValue;
                    dr["HitSign"] = this.HitSign.SelectedValue;
                    dr["PLUHitTypeName"] = this.PLUHitType.SelectedText;
                    dr["HitSignName"] = this.HitSign.SelectedText;
                    dr["EntityType"] = this.EntityType.SelectedValue;
                    dr["EntityTypeName"] = this.EntityType.SelectedText;
                    dr["EntityNum"] = item;
                    viewDT.Rows.Add(dr);
                }
                //防止多次选取相同记录的处理
                if (dt != null && dt.Rows.Count > 0)
                {
                    string types = "";
                    string ids = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        types += string.Format("{0},", "'" + dt.Rows[i]["EntityType"].ToString() + "'");
                        ids += string.Format("{0},", "'" + dt.Rows[i]["EntityNum"].ToString() + "'");
                    }
                    DataView dvSearch = viewDT.DefaultView;
                    dvSearch.RowFilter = " EntityType not in (" + types + ") or ( EntityType in (" + types + ") and EntityNum not in (" + ids + ") )";
                    DataTable dtSearch = dvSearch.ToTable();
                    dt.Merge(dtSearch);
                }
                else
                {
                    dt.Merge(viewDT);
                }

                ViewState["count"] = dt.Rows.Count;
                ViewState["table"] = dt;

                this.EntityNum.Text = string.Empty;
            }
            this.Grid1.RecordCount = dt.Rows.Count;
            this.Grid1.DataSource = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
            this.Grid1.DataBind();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind();
        }

                        //        <ext:NumberBox ID="KeyID" runat="server" Label="命中条件序号：" Required="true" ShowRedStar="true" ToolTip="请输入命中条件序号，不能超过10个字符"/>
                        //<ext:DropDownList ID="PLUHitType" runat="server" Label="货品命中类型：" Resizable="true" ToolTip="请选择货品命中类型">
                        //</ext:DropDownList>
                        //<ext:DropDownList ID="HitSign" runat="server" Label="货品过滤条件：" Resizable="true" ToolTip="请选择货品过滤条件">
                        //</ext:DropDownList>
    }
}