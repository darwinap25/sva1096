﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;


namespace Edge.Web.WebBuying.MasterFiles.PromotionInfos.Promotion.Promotion_Hit
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Promotion_Hit, PromotionHitViewModel>
    {
        BuyingNewPromotionController controller = new BuyingNewPromotionController();
        protected void Page_Load(object sender, EventArgs e)
        {
            controller = SVASessionInfo.BuyingNewPromotionController;
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(this.btnClose);

                InitData();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteOperationLog(this.PageName, " Update ");
            try
            {

                PromotionHitViewModel item = this.GetUpdateObject();
                if (item != null)
                {
                    item.HitSeq = ConvertTool.ToInt(Request.Params["id"]);
                    item.OprFlag = this.OprFlag.Text;
                    item.HitOPName = this.HitOP.SelectedText;
                    //item.LogicalOprName = DALTool.GetLogicalOprName(this.HitLogicalOpr.SelectedValue);
                    //todo:AAA
                    item.HitTypeName = this.HitType.SelectedText;
                    item.HitItemName = this.HitItem.SelectedText;
                    item.HitPluTable = controller.ViewModel.HitPluTable;
                    controller.UpdatePromotionHitViewModel(SVASessionInfo.SiteLanguage, item);
                    CloseAndPostBack();
                }
                else
                {
                    ShowError(Resources.MessageTips.UnKnownSystemError);
                }
            }
            catch (System.Exception ex)
            {
                Logger.Instance.WriteErrorLog("Update Promotion Hit", "Failed", ex);
                ShowError(Resources.MessageTips.UnKnownSystemError);
            }
        }

        protected void InitData()
        {
            this.PromotionCode.Text = Request.Params["promotioncode"].ToString();
            this.HitSeq.Text = Request.Params["id"].ToString();
            controller.BindHitType(this.HitType, false);
            controller.BindHitOP(this.HitOP, false);
            controller.BindHitItem(this.HitItem, false);
        }

        protected override void SetObject()
        {
            int id = ConvertTool.ToInt(Request.Params["id"]);
            if (id == 0) return;
            PromotionHitViewModel mmm = controller.ViewModel.PromotionHitList.Find(mm => mm.HitSeq.Equals(id));
            if (mmm != null)
            {
                this.SetObject(mmm, this.Form.Controls.GetEnumerator());
                if (mmm.OprFlag == "Add")
                {
                    this.OprFlag.Text = "Add";
                }
                else
                {
                    this.OprFlag.Text = "Update";
                }
                controller.ViewModel.HitPluTable = mmm.HitPluTable;
                BindHitPLUList(mmm.HitPluTable);
                HitItem_SelectedChanged(null, null);
            }         
        }

        #region 操作PLU表
        protected void btnHitPLUAdd_Click(object sender, EventArgs e)
        {
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Promotion_Hit_PLU/Add.aspx?PromotionCode={0}", this.PromotionCode.Text)));
        }
        protected void btnClearAllHitPLUItem_Click(object sender, EventArgs e)
        {
            ClearGird(this.Grid1);
            controller.ViewModel.HitPluTable = null;
        }
        protected void btnDeleteHitPLUItem_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.HitPluTable != null)
            {
                DataTable addDT = controller.ViewModel.HitPluTable;

                List<DataRow> list = new List<DataRow>();
                int pageno = Grid1.PageIndex;
                int pagesize = Grid1.PageSize;
                foreach (int row in Grid1.SelectedRowIndexArray)
                {
                    list.Add(addDT.Rows[pageno * pagesize + row]);
                }
                foreach (var item in list)
                {
                    addDT.Rows.Remove(item);
                }
                controller.ViewModel.HitPluTable = addDT;
                BindHitPLUList(controller.ViewModel.HitPluTable);
            }
        }
        private void BindHitPLUList(DataTable dt)
        {
            if (dt != null)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;

                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();

            }
            else
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.PageIndex = 0;
                this.Grid1.RecordCount = 0;
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }
        }
        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            BindHitPLUList(controller.ViewModel.HitPluTable);
        }
        #endregion
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindHitPLUList(controller.ViewModel.HitPluTable);
        }
        protected void HitItem_SelectedChanged(object sender, EventArgs e)
        {
            string item = this.HitItem.SelectedValue;
            switch (item)
            {
                case "-1":
                case "0":
                    this.Grid1.Enabled = false;
                    ClearGird(Grid1);
                    break;
                default:
                    this.Grid1.Enabled = true;
                    break;
            }
        }

    }
}