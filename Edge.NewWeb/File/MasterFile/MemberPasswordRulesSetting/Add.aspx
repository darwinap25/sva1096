﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.MemberPasswordRulesSetting.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                密码最小长度：
            </td>
            <td width="75%">
                <asp:TextBox ID="PWDMinLength" TabIndex="1" runat="server" CssClass="input required digits"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码最大长度：
            </td>
            <td>
                <asp:TextBox ID="PWDMaxLength" TabIndex="2" runat="server" CssClass="input required digits"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码安全级别：
            </td>
            <td>
                <asp:TextBox ID="PWDSecurityLevel" TabIndex="3" runat="server" CssClass="input required digits"
                    HintTitle="密码安全级别" HintInfo="Translate__Special_121_Start0级最低。依次提高Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码构成：
            </td>
            <td>
                <asp:TextBox ID="PWDStructure" TabIndex="4" runat="server" CssClass="input required digits"
                    HintTitle="密码构成" HintInfo="Translate__Special_121_Start0：没有要求。1.只能為數字 2.只能為字母 3.必須數字+字母 4.特殊符号+数字+字母Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码有效天数：
            </td>
            <td>
                <asp:TextBox ID="PWDValidDays" TabIndex="5" runat="server" CssClass="input required digits"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                密码有效天数的单位：
            </td>
            <td>
                <asp:TextBox ID="PWDValidDaysUnit" TabIndex="6" runat="server" CssClass="input required digits"
                    HintTitle="密码有效天数的单位" HintInfo="Translate__Special_121_Start0：永久。 1：年。 2：月。 3：星期。 4：天。Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否会员密码规则：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberPWDRule" runat="server" RepeatDirection="Horizontal"
                    TabIndex="7" Enabled="false">
                    <asp:ListItem Text="是" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="否" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
