﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.File.MasterFile.Customer
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Customer, Edge.SVA.Model.Customer>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "Show");
                RegisterCloseEvent(btnClose);
            }

        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName,"Add");
            if (Tools.DALTool.isHasCustomerCode(this.CustomerCode.Text.Trim(), 0))
            {
                ShowWarning(Resources.MessageTips.ExistCustomerCode);
                this.CustomerCode.Focus();
                return;
            }

            Edge.SVA.Model.Customer item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CustomerCode = item.CustomerCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Customer>(item) > 0)
            {
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }
    }
}
