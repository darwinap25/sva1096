﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.File.MasterFile.Customer
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Customer, Edge.SVA.Model.Customer>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");

            Edge.SVA.Model.Customer item = this.GetUpdateObject();

            if (Tools.DALTool.isHasCustomerCode(this.CustomerCode.Text.Trim(), item.CustomerID))
            {
                ShowWarning(Resources.MessageTips.ExistCustomerCode);
                this.CustomerCode.Focus();
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CustomerCode = item.CustomerCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Customer>(item))
            {
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }

        }
    }
}
