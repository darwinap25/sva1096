﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.Organization.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="OrganizationCode" runat="server" Label="捐赠组织编号：" MaxLength="20"
                Required="true" ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="OrganizationName1" runat="server" Label="捐赠组织描述：" MaxLength="512"
                Required="true" ShowRedStar="true" ToolTipTitle="捐赠组织描述" ToolTip="请输入规范的捐赠组织描述。不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="OrganizationName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1" ToolTip="对捐赠组织的其他描述1,不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="OrganizationName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2" ToolTip="对捐赠组织的另一個描述,不能超過512個字符"
                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="CardNumber" runat="server" Label="关联的卡号：" MaxLength="512" Required="true" ShowRedStar="true">
            </ext:TextBox>
            <ext:TextBox ID="CumulativePoints" runat="server" Label="累计获得的积分：" MaxLength="512"
                Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="CumulativeAmt" runat="server" Label="累计获得的金额：" MaxLength="512" Enabled="false">
            </ext:TextBox>
            <ext:HiddenField runat="server" ID="PicturePath"></ext:HiddenField>
            <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Offsets">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 80% 10%">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:FileUpload ID="picSFile" runat="server" Label="图片：" ToolTipTitle="图片" ToolTip="Translate__Special_121_Start捐赠组织的图片。点击按钮进行上传，上传的文件支持JPG,GIF，PNG，BMP,文件大小不能超过10240KBTranslate__Special_121_End">
                            </ext:FileUpload>
                            <ext:Button ID="btnBack" runat="server" Text="返回" HideMode="Display" CssClass="mleft20"
                                OnClick="btnBack_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" runat="server" HideMode="Display" Hidden="true">
                <Rows>
                    <ext:FormRow ID="FormRow1" ColumnWidths="68% 15% 17%" runat="server">
                        <Items>
                        <ext:Label ID="Label1" runat="server" Label="图片："></ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                            </ext:Button>
                            <ext:Button ID="btnReUpLoad" runat="server" Text="重新上传" HideMode="Display" 
                                OnClick="btnReUpLoad_Click">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:RadioButtonList ID="CallInterface" runat="server" Label="是否调用第三方接口：">
                <ext:RadioItem Text="调用" Value="1"></ext:RadioItem>
                <ext:RadioItem Text="不调用" Value="0" Selected="True"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <ext:Window ID="WindowPic1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px"> 
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
