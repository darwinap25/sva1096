﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using System.IO;

namespace Edge.Web.File.MasterFile.Organization
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Organization, Edge.SVA.Model.Organization>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    //存在会员相片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.OrganizationPicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = Model.OrganizationPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                    
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.Organization item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.OrganizationCode = item.OrganizationCode.ToUpper();
                //item.OrganizationPicFile = this.OrganizationPicFile.SaveToServer("Images/Organization");
                if (!string.IsNullOrEmpty(this.picSFile.ShortFileName) && this.FormLoad.Hidden == false)
                {
                    if (!ValidateImg(this.picSFile.FileName))
                    {
                        return;
                    }
                    item.OrganizationPicFile = this.picSFile.SaveToServer("Images/Organization");
                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateImg(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    item.OrganizationPicFile = this.uploadFilePath.Text;
                }
            }

            if (Edge.Web.Tools.DALTool.isHasOrganizationCode(item.OrganizationCode, item.OrganizationID))
            {
                ShowWarning(Resources.MessageTips.ExistOrganizationCode);
                return;
            }
            if (!Edge.Web.Tools.DALTool.isHasCard(this.CardNumber.Text.Trim()))
            {
                ShowWarning(Resources.MessageTips.NotExistCard);
                this.CardNumber.Focus();
                return;
            }

            if (DALTool.Update<Edge.SVA.BLL.Organization>(item))
            {
                if (this.FormReLoad.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.picSFile.ShortFileName))
            {
                this.PicturePath.Text = this.picSFile.SaveToServer("Images/Organization");
                FineUI.PageContext.RegisterStartupScript(WindowPic1.GetShowReference("../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.PicturePath.Text);
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}