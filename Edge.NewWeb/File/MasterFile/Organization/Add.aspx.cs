﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using System.IO;

namespace Edge.Web.File.MasterFile.Organization
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Organization, Edge.SVA.Model.Organization>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.isHasOrganizationCode(this.OrganizationCode.Text.Trim(), 0))
            {
                ShowWarning(Resources.MessageTips.ExistOrganizationCode);
                this.OrganizationCode.Focus();
                return;
            }

            if (!Edge.Web.Tools.DALTool.isHasCard(this.CardNumber.Text.Trim()))
            {
                ShowWarning(Resources.MessageTips.NotExistCard);
                this.CardNumber.Focus();
                return;
            }

            Edge.SVA.Model.Organization item = this.GetAddObject();

            if (item != null)
            {
                if (!ValidateImg(this.OrganizationPicFile.FileName))
                {
                    return;
                }
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.OrganizationCode = item.OrganizationCode.ToUpper();
                item.OrganizationPicFile = this.OrganizationPicFile.SaveToServer("Images/Organization");
            }

            if (DALTool.Add<Edge.SVA.BLL.Organization>(item) > 0)
            {
               CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.OrganizationPicFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.OrganizationPicFile.SaveToServer("Images/Organization");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}