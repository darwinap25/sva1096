﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.Organization.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:RadioButtonList ID="CallInterface" runat="server" Enabled="false">
                <ext:RadioItem Text="调用" Value="1"></ext:RadioItem>
                <ext:RadioItem Text="不调用" Value="0" Selected="True"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="OrganizationCode" runat="server" Label="捐赠组织编号：">
                            </ext:Label>
                            <ext:Label ID="OrganizationName1" runat="server" Label="捐赠组织描述：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="OrganizationName2" runat="server" Label="其他描述1：">
                            </ext:Label>
                            <ext:Label ID="OrganizationName3" runat="server" Label="其他描述2：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow3" runat="server">
                        <Items>
                            <ext:Label ID="CardNumber" runat="server" Label="关联的卡号：">
                            </ext:Label>
                            <ext:Label ID="CumulativePoints" runat="server" Label="累计获得的积分：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow4" runat="server">
                        <Items>
                            <ext:Label ID="CumulativeAmt" runat="server" Label="累计获得的金额：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="Form4" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ColumnWidths="0% 40% 10%">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:Label ID="Label1" Text="" runat="server" Label="店铺图片：">
                            </ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" Icon="Picture">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Form ID="Form5" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow5" runat="server">
                        <Items>
                            <ext:Label ID="CallInterfaceView" runat="server" Label="是否调用第三方接口：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow6" runat="server">
                        <Items>
                            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                            </ext:Label>
                            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow7" runat="server">
                        <Items>
                            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
                            </ext:Label>
                            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
