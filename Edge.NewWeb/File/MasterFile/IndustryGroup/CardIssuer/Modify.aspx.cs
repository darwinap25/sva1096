﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.CardIssuer, Edge.SVA.Model.CardIssuer>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                Edge.Web.Tools.ControlTool.BindCurrency(this.DomesticCurrencyID);
            }
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (this.DomesticCurrencyID.SelectedValue != "-1")
                {
                    this.DomesticCurrencyID.Enabled = false;
                }
                else
                {
                    this.DomesticCurrencyID.Enabled = true;
                }
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.CardIssuer item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardIssuer>(item))
            {
                this.CloseAndPostBack();
            }
            else
            {
                this.ShowUpdateFailed();
            }
        }
    }
}