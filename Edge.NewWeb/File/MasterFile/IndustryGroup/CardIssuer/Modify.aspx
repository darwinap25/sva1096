﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Modify" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="HEAD1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="CardIssuerName1" runat="server" Label="描述：" Required="true" ShowRedStar="true" Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="CardIssuerName2" runat="server" Label="其他描述1：" Enabled="false">
            </ext:TextBox>
            <ext:TextBox ID="CardIssuerName3" runat="server" Label="其他描述2：" Enabled="false">
            </ext:TextBox>
            <ext:DropDownList ID="DomesticCurrencyID" runat="server" Label="本币：" Required="true" ShowRedStar="true"
                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:HtmlEditor ID="CardIssuerDesc1" runat="server" Height="100" Label="说明1："></ext:HtmlEditor>
            <ext:HtmlEditor ID="CardIssuerDesc2" runat="server" Height="100" Label="说明2："></ext:HtmlEditor>
            <ext:HtmlEditor ID="CardIssuerDesc3" runat="server" Height="100" Label="说明3："></ext:HtmlEditor>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
