﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Modify.aspx.cs"
    Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="TabStrip1" runat="server" />
    <ext:TabStrip ID="TabStrip1" ShowBorder="true" ActiveTabIndex="0" runat="server">
        <Tabs>
            <ext:Tab ID="tabBase" runat="server" Title="基本信息" EnableBackgroundColor="true" AutoScroll="true"
                BodyPadding="10px">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                                Text="关闭">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                            </ext:ToolbarSeparator>
                           <ext:Button ID="btnSaveClose" ValidateForms="sform1,sform2,sform3,sform4,sform5,sform6,sform7,sform8,sform9,sf10"
                                Icon="SystemSaveClose" OnClick="btnUpdate_Click" runat="server" Text="保存后关闭">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本资料">
                        <Items>
                            <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:TextBox ID="CardGradeCode" runat="server" Label="卡级别编号：" MaxLength="20" Required="true"
                                        ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" AutoPostBack="True"
                                        OnTextChanged="ConvertTextboxToUpperText" ToolTipTitle="卡级别编号" ToolTip="Translate__Special_121_StartKey identifier of Card Grade. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"
                                        Enabled="false">
                                    </ext:TextBox>
                                    <ext:TextBox ID="CardGradeName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                                        ShowRedStar="true" ToolTipTitle="描述" ToolTip="请输入规范的卡級別名称。不能超過512個字符" 
                                        OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:TextBox ID="CardGradeName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1："
                                        ToolTip="對卡級別的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:TextBox ID="CardGradeName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2："
                                        ToolTip="對卡級別的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" Required="true" ShowRedStar="true"
                                        OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged" AutoPostBack="true"
                                        Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                        CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                    <%--                                    <ext:TextArea ID="CardGradeNotes" runat="server" Height="100px" Label="条款条例：" Text=""
                                        MaxLength="512" Required="true" ShowRedStar="true" ToolTipTitle="条款条例" ToolTip="请输入条框条例的描述，不能超过512位">
                                    </ext:TextArea>--%>
                                    <ext:HtmlEditor ID="MemberClauseDesc1" runat="server" Height="200" Label="条款条例1："
                                        ShowRedStar="true">
                                    </ext:HtmlEditor>
                                    <ext:HtmlEditor ID="MemberClauseDesc2" runat="server" Height="200" Label="条款条例2：">
                                    </ext:HtmlEditor>
                                    <ext:HtmlEditor ID="MemberClauseDesc3" runat="server" Height="200" Label="条款条例3：">
                                    </ext:HtmlEditor>
                                    <%--<ext:FileUpload ID="CardGradePicFile" runat="server" Label="：" Required="true"
                                ShowRedStar="true">
                            </ext:FileUpload>
                            <ext:FileUpload ID="CardGradeLayoutFile" runat="server" Label="：" Required="true"
                                ShowRedStar="true">
                            </ext:FileUpload>
                            <ext:FileUpload ID="CardGradeStatementFile" runat="server" Label="：" Required="true"
                                ShowRedStar="true">
                            </ext:FileUpload>--%>
                                    <ext:HiddenField ID="PicturePath" runat="server">
                                    </ext:HiddenField>
                                    <ext:Form ID="FormLoad" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Offsets">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 80% 10%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:FileUpload ID="CardGradePicFile" runat="server" Label="卡图片：" ToolTipTitle="卡图片"
                                                        ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                                                    </ext:FileUpload>
                                                    <ext:Button ID="btnBack" runat="server" Text="返回" HideMode="Display" Hidden="true"
                                                        CssClass="mleft20" OnClick="btnBack_Click">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="FormReLoad" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                                        runat="server" HideMode="Display" Hidden="true">
                                        <Rows>
                                            <ext:FormRow ID="FormRow1" ColumnWidths="68% 15% 17%" runat="server">
                                                <Items>
                                                    <ext:Label ID="Label5" runat="server" Label="卡图片：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnPreview" runat="server" Text="查看" HideMode="Display" Icon="Picture">
                                                    </ext:Button>
                                                    <ext:Button ID="btnReUpLoad" runat="server" Text="重新上传" HideMode="Display" OnClick="btnReUpLoad_Click">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="FormLoad1" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Offsets">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 80% 10%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath1" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:FileUpload ID="CardGradeLayoutFile" runat="server" Label="卡设计模板：" ToolTipTitle="卡设计模板"
                                                        ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End">
                                                    </ext:FileUpload>
                                                    <ext:Button ID="btnBack1" runat="server" Text="返回" HideMode="Display" Hidden="true"
                                                        OnClick="btnBack1_Click" CssClass="mleft20">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="FormReLoad1" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="true">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="68% 15% 15%">
                                                <Items>
                                                    <ext:Label ID="Label6" runat="server" Label="卡设计模板：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnExport" runat="server" Text="下载" OnClick="btnExport_Click" Icon="PageExcel"
                                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                                    </ext:Button>
                                                    <ext:Button ID="btnReLoad1" runat="server" Text="重新上传" OnClick="btnReUpLoad1_Click">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="FormLoad2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Offsets">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 80% 10%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath2" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:FileUpload ID="CardGradeStatementFile" runat="server" Label="卡结算单模板：" ToolTipTitle="卡结算单模板"
                                                        ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End">
                                                    </ext:FileUpload>
                                                    <ext:Button ID="btnBack2" runat="server" Text="返回" HideMode="Display" Hidden="true"
                                                        OnClick="btnBack2_Click" CssClass="mleft20">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="FormReLoad2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="true">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="68% 15% 15%">
                                                <Items>
                                                    <ext:Label ID="Label7" runat="server" Label="卡结算单模板：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnExport1" runat="server" Text="下载" OnClick="btnExport1_Click" Icon="PageExcel"
                                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                                    </ext:Button>
                                                    <ext:Button ID="btnReUpLoad2" runat="server" Text="重新上传" OnClick="btnReUpLoad2_Click">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="PasswordRuleID" runat="server" Label="密码规则：" Required="true"
                                        ShowRedStar="true" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                        CompareMessage="请选择有效值">
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CampaignID" runat="server" Label="活动：" Hidden="true" Resizable="true">
                                    </ext:DropDownList>
                                    <%--<ext:TextBox ID="" runat="server" Label="：" MaxLength="2" Required="true"
                                ShowRedStar="true" ToolTipTitle="卡級別序號" ToolTip="">
                            </ext:TextBox>--%>
                                    <ext:NumberBox ID="CardGradeRank" runat="server" Label="卡级别等级：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="卡級別序號" ToolTip="作为卡之间级别高低的定义。数字越大，级别越高。"
                                        MaxValue="99" NoDecimal="true">
                                    </ext:NumberBox>
                               <%--add by Alex 2014-06-30 --%>
                              <ext:RadioButtonList ID="TrainingMode" runat="server" Label="Translate__Special_121_Start是否训练卡（是/否）：Translate__Special_121_End"
                                Width="200px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                             <%--add by Alex 2014-06-30 --%>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="卡号规则">
                        <Items>
                            <ext:SimpleForm ID="sform2" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:RadioButtonList ID="IsImportUIDNumber" runat="server" Label="卡号码是否导入：" OnSelectedIndexChanged="IsImportUIDNumber_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100px">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="true" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="ManualRoles" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                        <Items>
                            <ext:SimpleForm ID="sform3" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                  <%--add by Alex 2014-06-30--%>
                            <ext:RadioButtonList ID="AllowOfflineQRCode" runat="server" Label="Translate__Special_121_Start是否允许离线的QR码（是/否）：Translate__Special_121_End" AutoPostBack="True"
                                OnSelectedIndexChanged="CardCheckdigit_SelectedIndexChanged" Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True"/>
                                <ext:RadioItem Text="否" Value="0"/>
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="QRCodePrefix" runat="server" Label="Translate__Special_121_StartQR码前缀号码：Translate__Special_121_End" Resizable="true">
                                <ext:ListItem Value="" Text="-------" />
                              <ext:ListItem Value="Q7FM" Text="Q7FM" />
                            </ext:DropDownList>
                        <%--add by Alex 2014-06-30--%>
                                    <ext:TextBox ID="CardNumMask" runat="server" Label="卡号编码规则：" Regex="(^A*9+$)|(^9+$)"
                                        RegexMessage="编码规则输入不正确" ShowRedStar="true" AutoPostBack="True" OnTextChanged="CardNumMask_TextChanged"
                                        ToolTipTitle="卡号编码规则" ToolTip="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End">
                                    </ext:TextBox>
                                    <ext:TextBox ID="CardNumPattern" runat="server" Label="卡号前缀号码：" ShowRedStar="true"
                                        RegexPattern="NUMBER" RegexMessage="编码规则输入不正确" AutoPostBack="True" OnTextChanged="CardNumPattern_TextChanged"
                                        ToolTipTitle="卡号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                                    </ext:TextBox>
                                    <ext:RadioButtonList ID="CardCheckdigit" runat="server" Label="卡号是否添加校验位：" AutoPostBack="True"
                                        OnSelectedIndexChanged="CardCheckdigit_SelectedIndexChanged" Width="100px">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="True" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px"
                                        Resizable="true">
                                        <ext:ListItem Value="1" Text="EAN13" />
                                        <ext:ListItem Value="2" Text="MOD10" />
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CardNumberToUID" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End"
                                        AutoPostBack="true" OnSelectedIndexChanged="CardNumberToUID_SelectedIndexChanged"
                                        Resizable="true">
                                        <ext:ListItem Value="1" Text="全部复制" />
                                        <ext:ListItem Value="0" Text="绑定" />
                                        <ext:ListItem Value="2" Text="复制去掉校验位" />
                                        <ext:ListItem Value="3" Text="复制加上校验位" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="ImportRoles" runat="server" EnableCollapse="True" Title="导入物理编号规则"
                        Hidden="true">
                        <Items>
                            <ext:SimpleForm ID="sform4" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:TextBox ID="CardNumMaskImport" runat="server" Label="卡号编码规则：" Regex="(^A*9+$)|(^9+$)"
                                        RegexMessage="编码规则输入不正确" ShowRedStar="true" AutoPostBack="True" OnTextChanged="CardNumMaskImport_TextChanged"
                                        ToolTipTitle="卡号编码规则" ToolTip="Translate__Special_121_Start1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999Translate__Special_121_End">
                                    </ext:TextBox>
                                    <ext:TextBox ID="CardNumPatternImport" runat="server" Label="卡号前缀号码：" RegexPattern="NUMBER"
                                        RegexMessage="编码规则输入不正确" ShowRedStar="true" AutoPostBack="True" OnTextChanged="CardNumPatternImport_TextChanged"
                                        ToolTipTitle="卡号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                                    </ext:TextBox>
                                    <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End"
                                        Width="100px">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End"
                                        Width="100px" AutoPostBack="true" OnSelectedIndexChanged="UIDCheckDigit_SelectedIndexChanged">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="CheckDigitModeID2" runat="server" Label="校验位计算方法：" Width="100px"
                                        Resizable="true">
                                        <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                        <ext:ListItem Value="2" Text="MOD10" />
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="UIDToCardNumber" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End"
                                        AutoPostBack="true" OnSelectedIndexChanged="UIDToCardNumber_SelectedIndexChanged"
                                        Resizable="true">
                                        <ext:ListItem Value="1" Text="全部复制" />
                                        <ext:ListItem Value="0" Text="绑定" />
                                        <ext:ListItem Value="2" Text="复制去掉校验位" />
                                        <ext:ListItem Value="3" Text="复制加上校验位" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="使用规则">
                        <Items>
                            <ext:SimpleForm ID="sform5" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                     <ext:LinkButton ID="lbPLUMapping" runat="server" Label="销售货号匹配清单：" Text="编辑销售货号" />
                                     <ext:LinkButton ID="lbPLUSpecific" runat="server" Label="指定限购商品：" Text="编辑限购商品" />
                                     <ext:LinkButton ID="lbSpecificDepartment" runat="server" Label="指定限购部门：" Text="编辑限购部门" />
                                      <ext:LinkButton ID="lblTenderMapping" runat="server" Label="投標匹配列表：" Text="投標" />
                                    <ext:LinkButton ID="lbIssueBrand" runat="server" Label="发行品牌：" Text="品牌" />
                                    <ext:LinkButton ID="lbIssueStore" runat="server" Label="发行店铺：" Text="店铺" />
                                    <ext:LinkButton ID="lbUseBrand" runat="server" Label="使用品牌：" Text="品牌" />
                                    <ext:LinkButton ID="lbUseStore" runat="server" Label="使用店铺：" Text="店铺" />
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="使用规则">
                        <Items>
                            <ext:SimpleForm ID="sform6" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:RadioButtonList ID="IsAllowStoreValue" runat="server" Label="是否允许储值：" Width="100px"
                                        OnSelectedIndexChanged="IsAllowStoreValue_OnSelectedIndexChanged" AutoPostBack="true">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="IsAllowConsumptionPoint" runat="server" Label="是否允许积分："
                                        Width="100px" OnSelectedIndexChanged="IsAllowConsumptionPoint_OnSelectedIndexChanged"
                                        AutoPostBack="true">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:TextBox ID="CardTypeInitAmount" runat="server" Label="初始金额：" MaxLength="20"
                                        Required="true" ShowRedStar="true" Text="0.00" ToolTipTitle="初始金额" ToolTip="请输入正數，保留兩位小數"
                                        OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:NumberBox ID="CardTypeInitPoints" runat="server" Label="初始积分：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="初始积分" ToolTip="请输入正整數，不允许输入小数"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:TextBox ID="CardGradeMaxAmount" runat="server" Label="卡最大現金賬套金額：" MaxLength="20"
                                        Text="999999.00" ToolTipTitle="卡最大現金賬套金額" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal"
                                        AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:NumberBox ID="CardGradeMaxPoint" runat="server" Label="卡最大积分帐套积分：" NoNegative="true"
                                        Text="999999" Required="true" ShowRedStar="true" ToolTipTitle="卡最大积分帐套积分" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:TextBox ID="MinAmountPreAdd" runat="server" Label="最低充值现金帐套金额：" MaxLength="20"
                                        Text="0.00" ToolTipTitle="最低充值现金帐套金额" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal"
                                        AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:NumberBox ID="MinPointPreAdd" runat="server" Label="最低充值积分帐套积分：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="最低充值积分帐套积分" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="999999" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:TextBox ID="MaxAmountPreAdd" runat="server" Label="最高充值现金帐套金额：" MaxLength="20"
                                        Text="999999.00" ToolTipTitle="最高充值现金帐套金额" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal"
                                        AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:NumberBox ID="MaxPointPreAdd" runat="server" Label="最高充值积分帐套积分：" NoNegative="true"
                                        Text="999999" Required="true" ShowRedStar="true" ToolTipTitle="最高充值积分帐套积分" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="999999" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:NumberBox ID="CardGradeDiscCeiling" runat="server" Label="折扣最高值：" NoNegative="true"
                                        Text="100" Required="true" ShowRedStar="true" ToolTipTitle="折扣最高值" ToolTip="请输入0~100的整數，不允許輸入小數"
                                        MaxValue="100" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:NumberBox ID="CardConsumeBasePoint" runat="server" Label="最小消费积分（每次）：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="消费积分的最低值" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:NumberBox ID="MinBalancePoint" runat="server" Label="最小剩余积分：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="最小剩余积分" ToolTip="请输入正整數，不允许输入小数"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:TextBox ID="MinConsumeAmount" runat="server" Label="最小消费金额（每次）：" MaxLength="10"
                                        Text="0.00" Required="true" ShowRedStar="true" ToolTipTitle="最小消费金额(每一次)" ToolTip="请输入正數，保留兩位小數"
                                        OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:TextBox ID="MinBalanceAmount" runat="server" Label="最小剩余金额：" MaxLength="10"
                                        Text="0.00" Required="true" ShowRedStar="true" RegexMessage="请输入正數，保留兩位小數" ToolTipTitle="最小剩余金额"
                                        ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:RadioButtonList ID="ForfeitAmountAfterExpired" runat="server" Label="过期后是否清空帐套金额："
                                        Width="200px">
                                        <ext:RadioItem Text="不清零" Value="0" Selected="True" />
                                        <ext:RadioItem Text="清零" Value="1" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="ForfeitPointAfterExpired" runat="server" Label="过期后是否清空帐套积分："
                                        Width="200px">
                                        <ext:RadioItem Text="不清零" Value="0" Selected="True" />
                                        <ext:RadioItem Text="清零" Value="1" />
                                    </ext:RadioButtonList>
                                     <%--Add by Alex 2014-06-30 ++--%>
                            <ext:NumberBox ID="NumberOfTransDisplay" runat="server" Label="交易数据显示数量：" NoNegative="true"
                                Text="0"  ToolTipTitle="交易数据显示数量" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="NumberOfCouponDisplay" runat="server" Label="优惠券显示数量：" NoNegative="true"
                                Text="0"  ToolTipTitle="优惠券显示数量" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="NumberOfNewsDisplay" runat="server" Label="广告显示数量：" NoNegative="true"
                                Text="0"  ToolTipTitle="广告显示数量" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                                 </ext:NumberBox>
                                 <%--Add by Alex 2014-06-30 ++--%>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="升级规则">
                        <Items>
                            <ext:SimpleForm ID="sform7" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:DropDownList ID="CardGradeUpdMethod" runat="server" Label="升级条件：" AutoPostBack="true"
                                        OnSelectedIndexChanged="CardGradeUpdMethod_OnSelectedIndexChanged" Resizable="true">
                                        <ext:ListItem Value="0" Text="无" Selected="true" />
                                        <ext:ListItem Value="3" Text="单笔消费" />
                                        <ext:ListItem Value="1" Text="年累积购买值大于" />
                                        <ext:ListItem Value="2" Text="年累计积分值大于" />
                                        <ext:ListItem Value="4" Text="获得升级需要的优惠券类型指定的优惠券数量达到指定值" />
                                    </ext:DropDownList>
                                    <%--<ext:TextBox ID="" runat="server" Label="：" MaxLength="20"
                                Text="0" RegexPattern="NUMBER" ToolTipTitle="界限值" ToolTip="请输入正數，最大兩位小數">
                            </ext:TextBox>--%>
                                    <ext:NumberBox ID="CardGradeUpdThreshold" runat="server" Label="界限值：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="界限值" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:TextBox ID="CardGradeUpdHitPLU" runat="server" Label="指定商品：">
                                    </ext:TextBox>
                                    <ext:DropDownList ID="CardGradeUpdCouponTypeID" runat="server" Label="升级需要的优惠券类型：" Resizable="true">
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="有效期规则">
                        <Items>
                            <ext:SimpleForm ID="sform8" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:NumberBox ID="CardValidityDuration" runat="server" Label="有效期长度：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="有效期长度" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="999" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:DropDownList ID="CardValidityUnit" runat="server" Label="有效期长度单位：" Required="true"
                                        ShowRedStar="true" Resizable="true">
                                        <ext:ListItem Value="1" Text="年" Selected="true" />
                                        <ext:ListItem Value="2" Text="月" />
                                        <ext:ListItem Value="3" Text="星期" />
                                        <ext:ListItem Value="4" Text="天" />
                                    </ext:DropDownList>
                                    <ext:RadioButtonList ID="ActiveResetExpiryDate" runat="server" Label="激活是否重置有效期："
                                        Width="100px">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:NumberBox ID="GracePeriodValue" runat="server" Label="宽限期值：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="宽限期值" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="999" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:DropDownList ID="GracePeriodUnit" runat="server" Label="宽限期单位：" Required="true"
                                        ShowRedStar="true" Resizable="true">
                                        <ext:ListItem Value="1" Text="年" Selected="true" />
                                        <ext:ListItem Value="2" Text="月" />
                                        <ext:ListItem Value="3" Text="星期" />
                                        <ext:ListItem Value="4" Text="天" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel7" runat="server" EnableCollapse="True" Title="转赠/转换规则">
                        <Items>
                            <ext:SimpleForm ID="sform9" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:NumberBox ID="CardPointToAmountRate" runat="server" Label="积分转换现金规则：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="积分转换现金规则" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:TextBox ID="CardAmountToPointRate" runat="server" Label="现金转换积分规则：" MaxLength="13"
                                        Text="0.00" Required="true" ShowRedStar="true" RegexMessage="请输入正數，保留兩位小數" ToolTipTitle="现金转换积分规则"
                                        ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:DropDownList ID="CardPointTransfer" runat="server" Label="积分转换规则（转出）：" Required="true"
                                        ShowRedStar="true" AutoPostBack="true" OnSelectedIndexChanged="CardPointTransfer_SelectedIndexChanged"
                                        Resizable="true">
                                        <ext:ListItem Value="0" Text="不允许" Selected="true" />
                                        <ext:ListItem Value="1" Text="允许同卡级别" />
                                        <ext:ListItem Value="2" Text="允许同卡类型" />
                                        <ext:ListItem Value="3" Text="允许同品牌" />
                                        <ext:ListItem Value="4" Text="允许同发行商" />
                                    </ext:DropDownList>
                                    <ext:NumberBox ID="MinPointPreTransfer" runat="server" Label="每笔交易最小转换积分：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最小转换积分" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="999999" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:NumberBox ID="MaxPointPreTransfer" runat="server" Label="每笔交易最大转换积分：" NoNegative="true"
                                        Text="0" Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最大转换积分" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="999999" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:NumberBox ID="DayMaxPointTransfer" runat="server" Label="每天最大转赠积分：" NoNegative="true"
                                        Text="999999" Required="true" ShowRedStar="true" ToolTipTitle="每天最大转赠积分" ToolTip="必須输入數字，並且是整數"
                                        MaxValue="100000000" NoDecimal="true">
                                    </ext:NumberBox>
                                    <ext:DropDownList ID="CardAmountTransfer" runat="server" Label="现金转换规则：" Required="true"
                                        ShowRedStar="true" AutoPostBack="true" OnSelectedIndexChanged="CardAmountTransfer_SelectedIndexChanged"
                                        Resizable="true">
                                        <ext:ListItem Value="0" Text="不允许" Selected="true" />
                                        <ext:ListItem Value="1" Text="允许同卡级别" />
                                        <ext:ListItem Value="2" Text="允许同卡类型" />
                                        <ext:ListItem Value="3" Text="允许同品牌" />
                                        <ext:ListItem Value="4" Text="允许同发行商" />
                                    </ext:DropDownList>
                                    <ext:TextBox ID="MinAmountPreTransfer" runat="server" Label="每笔交易最小转换金额：" Text="0.00"
                                        Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最小转换金额" ToolTip="请输入正數，保留兩位小數"
                                        OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:TextBox ID="MaxAmountPreTransfer" runat="server" Label="每笔交易最大转换金额：" Text="0.00"
                                        Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最大转换金额" ToolTip="请输入正數，保留兩位小數"
                                        OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                    <ext:TextBox ID="DayMaxAmountTransfer" runat="server" Label="每天最大转赠金额：" Text="999999.00"
                                        Required="true" ShowRedStar="true" ToolTipTitle="每天最大转赠金额" ToolTip="请输入正數，保留兩位小數"
                                        OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                                    </ext:TextBox>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                      <%--add by Alex 2014-06-30 ++--%>
                <ext:GroupPanel ID="GroupPanel9" runat="server" EnableCollapse="True" Title="卡级别时间规则">
                <Items>
                    <ext:SimpleForm ID="sf10" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:NumberBox ID="MobileProtectPeriodValue" runat="server" Label="手机号码重用时间数值（月）：" NoNegative="true"
                                Text="0"  ToolTipTitle="手机号码重用时间数值（月）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="EmailValidatedPeriodValue" runat="server" Label="邮箱验证时间数值（分钟）：" NoNegative="true"
                                Text="0" ToolTipTitle="邮箱验证时间数值（分钟）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="NonValidatedPeriodValue" runat="server" Label="未验证用户重设时间数值（小时）：" NoNegative="true"
                                Text="0" ToolTipTitle="未验证用户重设时间数值（小时）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="LoginFailureCount" runat="server" Label="登录错误次数：" NoNegative="true"
                                Text="0" ToolTipTitle="登录错误次数" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>

                             <ext:NumberBox ID="SessionTimeoutValue" runat="server" Label="登录超时数值（小时）：" NoNegative="true"
                                Text="0" ToolTipTitle="登录超时数值（小时）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="QRCodePeriodValue" runat="server" Label="Translate__Special_121_StartQR码有效时间（分钟）：Translate__Special_121_End" NoNegative="true"
                                Text="0" ToolTipTitle="Translate__Special_121_StartQR码有效时间（分钟）Translate__Special_121_End" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>

            <%--add by Alex 2014-06-30 ++--%>
                    <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="可获取优惠券类型规则" Hidden="false">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:NumberBox ID="HoldCouponCount" runat="server" Label="可获取累计最大数量：" NoNegative="true"
                                        NoDecimal="true" ToolTipTitle="可获取累计最大数量" ToolTip="请输入正數" AutoPostBack="true"
                                        OnTextChanged="HoldCouponCount_OnTextChanged" MaxValue="100000000" Hidden="true">
                                    </ext:NumberBox>
                                </Items>
                            </ext:SimpleForm>
                            <ext:Grid ID="Grid_CanHoldCouponGradeList" ShowBorder="true" ShowHeader="true" Title="可获取优惠券类型列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="Grid_CanHoldCouponGradeList_OnPageIndexChange">
                                <Toolbars>
                                    <ext:Toolbar ID="Toolbar4" runat="server">
                                        <Items>
                                            <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                            </ext:Button>
                                            <ext:Button ID="btnDelete" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete_OnClick">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField ColumnID="BrandDescription" DataField="BrandDescription" HeaderText="品牌编号" />
                                    <ext:BoundField ColumnID="CouponTypeDescription" DataField="CouponTypeDescription"
                                        HeaderText="优惠券类型" />
                                    <ext:BoundField ColumnID="MainTable_HoldCount" DataField="MainTable.HoldCount" HeaderText="单个获取最大数量" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel38" runat="server" EnableCollapse="True" Title="自动绑定优惠券类型规则">
                        <Items>
                            <ext:Grid ID="Grid_GetCouponGradeList" ShowBorder="true" ShowHeader="true" Title="自动绑定优惠券类型列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="Grid_GetCouponGradeList_OnPageIndexChange">
                                <Toolbars>
                                    <ext:Toolbar ID="Toolbar5" runat="server">
                                        <Items>
                                            <ext:Button ID="btnNew1" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                            </ext:Button>
                                            <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField ColumnID="BrandDescription" DataField="BrandDescription" HeaderText="品牌编号" />
                                    <ext:BoundField ColumnID="CouponTypeDescription" DataField="CouponTypeDescription"
                                        HeaderText="优惠券类型" />
                                    <ext:BoundField ColumnID="MainTable_HoldCount" DataField="MainTable.HoldCount" HeaderText="优惠券数量" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab1" Title="有效期延长规则" EnableBackgroundColor="true" runat="server">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar3" runat="server">
                        <Items>
                            <ext:Button ID="lbtnAdd" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                            </ext:Button>
                            <ext:Button ID="lbtnDel" Text="删除" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                        Title="" BoxFlex="1" Layout="Fit">
                        <Items>
                            <ext:Grid ID="rptExtensionRuleList" runat="server" ShowBorder="false" ShowHeader="false"
                                DataKeyNames="ExtensionRuleID" AutoHeight="true" EnableCheckBoxSelect="True"
                                IsDatabasePaging="true" EnableRowNumber="true" AutoWidth="true" ForceFitAllTime="true"
                                AllowPaging="true" PageSize="10" OnPageIndexChange="rptExtensionRuleList_PageIndexChange">
                                <Columns>
                                    <ext:TemplateField HeaderText="规则类型">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("RuleType")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="规则记录序号">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("ExtensionRuleSeqNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="开始日期">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="结束日期">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="状态">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:WindowField ColumnID="ViewWindowField" WindowID="Window2" Icon="Page" Text="查看"
                                        ToolTip="查看" DataIFrameUrlFields="ExtensionRuleID" DataIFrameUrlFormatString="CardExtensionRule/show.aspx?id={0}"
                                        DataWindowTitleFormatString="查看" Title="查看" />
                                    <ext:WindowField ColumnID="EditWindowField" WindowID="Window2" Icon="PageEdit" Text="编辑"
                                        ToolTip="编辑" DataIFrameUrlFields="ExtensionRuleID" DataIFrameUrlFormatString="CardExtensionRule/modify.aspx?id={0}"
                                        DataWindowTitleFormatString="编辑" Title="编辑" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab2" Title="获取积分规则" EnableBackgroundColor="true" runat="server">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server">
                        <Items>
                            <ext:Button ID="lbtnPointRuleAdd" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                            </ext:Button>
                            <ext:Button ID="lbtnPointRuleDel" Text="删除" Icon="Delete" OnClick="lbtnPointRuleDel_Click"
                                runat="server">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Panel ID="Panel3" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                        Title="" BoxFlex="1" Layout="Fit">
                        <Items>
                            <ext:Grid ID="rptEarnAmountPointList" runat="server" ShowBorder="false" ShowHeader="false"
                                DataKeyNames="PointRuleID,PointRuleCode" AutoHeight="true" EnableCheckBoxSelect="True" IsDatabasePaging="true"
                                EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true" 
                                AllowPaging="true" PageSize="10" OnPageIndexChange="rptEarnAmountPointList_PageIndexChange">
                                <Columns>
                                    <%--<ext:BoundField HeaderText="记录序号" DataField="PointRuleSeqNo" />--%>
                                    <ext:BoundField HeaderText="规则编号" DataField="PointRuleCode" />
                                    <ext:BoundField HeaderText="会员范围" DataField="MemberDateType_Name" />
                                    <ext:BoundField HeaderText="交易类型" DataField="PointRuleType_Name" />
                                    <ext:BoundField HeaderText="计算方式" DataField="PointRuleOper_Name" />
                                    <ext:BoundField HeaderText="设定的金额" DataField="PointRuleAmount" DataFormatString="{0:N2}"/>
                                    <ext:BoundField HeaderText="设定的积分" DataField="PointRulePoints" />
                                    <ext:TemplateField HeaderText="开始日期">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="结束日期">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:BoundField HeaderText="状态" DataField="Status_Name" />
                                    <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                        ToolTip="查看" DataIFrameUrlFields="PointRuleID" DataIFrameUrlFormatString="PointRule/show.aspx?id={0}"
                                        DataWindowTitleFormatString="查看" Title="查看" />
                                    <ext:WindowField ColumnID="EditWindowField" WindowID="Window1" Icon="PageEdit" Text="编辑"
                                        ToolTip="编辑" DataIFrameUrlFields="PointRuleID" DataIFrameUrlFormatString="PointRule/modify.aspx?id={0}"
                                        DataWindowTitleFormatString="编辑" Title="编辑" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Tab>
<%--            <ext:Tab ID="Tab3" Title="获取优惠券规则" EnableBackgroundColor="true" runat="server">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar7" runat="server">
                        <Items>
                            <ext:Button ID="Button1" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                            </ext:Button>
                            <ext:Button ID="Button3" Text="删除" Icon="Delete" OnClick="lbtnPointRuleDel_Click"
                                runat="server">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                        Title="" BoxFlex="1" Layout="Fit">
                        <Items>
                            <ext:Grid ID="Grid2" runat="server" ShowBorder="false" ShowHeader="false" DataKeyNames="PointRuleID"
                                AutoHeight="true" EnableCheckBoxSelect="True" IsDatabasePaging="true" EnableRowNumber="True"
                                AutoWidth="true" ForceFitAllTime="true">
                                <Columns>
                                    <ext:TemplateField HeaderText="规则编号">
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" runat="server" Text='<%#Eval("PointRuleSeqNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="计算方式">
                                        <ItemTemplate>
                                            <asp:Label ID="Label13" runat="server" Text='<%#Eval("PointRuleOper")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="开始日期">
                                        <ItemTemplate>
                                            <asp:Label ID="Label14" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="结束日期">
                                        <ItemTemplate>
                                            <asp:Label ID="Label15" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                        ToolTip="查看" DataIFrameUrlFields="PointRuleID" DataIFrameUrlFormatString="PointRule/show.aspx?id={0}"
                                        DataWindowTitleFormatString="查看" Title="查看" />
                                    <ext:WindowField ColumnID="EditWindowField" WindowID="Window1" Icon="PageEdit" Text="编辑"
                                        ToolTip="编辑" DataIFrameUrlFields="PointRuleID" DataIFrameUrlFormatString="PointRule/modify.aspx?id={0}"
                                        DataWindowTitleFormatString="编辑" Title="编辑" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Tab>--%>
            <ext:Tab ID="Tab4" Title="会员奖励规则" EnableBackgroundColor="true" runat="server">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar6" runat="server">
                        <Items>
                            <ext:Button ID="btnMemberRewardAdd" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                            </ext:Button>
                            <ext:Button ID="btnMemberRewardDelete" Text="删除" Icon="Delete" OnClick="btnMemberRewardDelete_Click"
                                runat="server">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                        Title="" BoxFlex="1" Layout="Fit">
                        <Items>
                            <ext:Grid ID="gridMemberReward" runat="server" ShowBorder="false" ShowHeader="false"
                                DataKeyNames="SVARewardRulesID" AutoHeight="true" EnableCheckBoxSelect="True"
                                IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                AllowPaging="true" PageSize="10" OnPageIndexChange="gridMemberReward_PageIndexChange">
                                <Columns>
                                    <ext:TemplateField HeaderText="规则编号">
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("SVARewardRulesCode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="奖励类型">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("SVARewardTypeName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="奖励积分">
                                        <ItemTemplate>
                                            <asp:Label ID="Label9" runat="server" Text='<%#Eval("RewardPoint")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="奖励金额">
                                        <ItemTemplate>
                                            <asp:Label ID="Label16" runat="server" Text='<%#Eval("RewardAmount","{0:N2}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="奖励优惠券类型">
                                        <ItemTemplate>
                                            <asp:Label ID="Label17" runat="server" Text='<%#Eval("RewardCouponTypeIDName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="优惠券数量">
                                        <ItemTemplate>
                                            <asp:Label ID="Label18" runat="server" Text='<%#Eval("RewardCouponCount")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="开始日期">
                                        <ItemTemplate>
                                            <asp:Label ID="Label10" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="结束日期">
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:TemplateField HeaderText="状态">
                                        <ItemTemplate>
                                            <asp:Label ID="Label19" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </ext:TemplateField>
                                    <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                        ToolTip="查看" DataIFrameUrlFields="SVARewardRulesID" DataIFrameUrlFormatString="MemberReward/show.aspx?id={0}"
                                        DataWindowTitleFormatString="查看" Title="查看" />
                                    <ext:WindowField ColumnID="EditWindowField" WindowID="Window1" Icon="PageEdit" Text="编辑"
                                        ToolTip="编辑" DataIFrameUrlFields="SVARewardRulesID" DataIFrameUrlFormatString="MemberReward/modify.aspx?id={0}"
                                        DataWindowTitleFormatString="编辑" Title="编辑" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Tab>
        </Tabs>
    </ext:TabStrip>
    <%--    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">

    </ext:Panel>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    <%--    <tr>
        <th colspan="2" align="left">
        </th>
    </tr>
    <tr>
        <td align="right">
        </td>
        <td>
            <%-- <asp:TextBox ID="EarnStoreCondition" TabIndex="18" runat="server" Enabled="false"
                    CssClass="input"></asp:TextBox>--%>
    <%-- <a id="store1" class="thickbox btn_bg" href="../../BrandLocationStore.aspx?Url=&id=<%=Request.QueryString["id"].ToString()%>&type=2&storetype=1&height=560&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        修改</a>--
            <a id="issueBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                品牌</a> <a id="issueStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                    店铺</a>
        </td>
    </tr>
    <tr>
        <td align="right">
            使用品牌/店铺：
        </td>
        <td>
            <%-- <asp:TextBox ID="ConsumeStoreCondition" TabIndex="19" runat="server" Enabled="false"
                    CssClass="input"></asp:TextBox>--%>
    <%--      <a id="store2" class="thickbox btn_bg" href="../../BrandLocationStore.aspx?Url=&id=<%=Request.QueryString["id"].ToString()%>&type=2&storetype=2&height=560&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        修改</a>--
            <a id="useBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                品牌</a>
            <%--                  <a id="useLocation" class="thickbox btn_bg" href="Location/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        区域</a>--
            <a id="useStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=2&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                店铺</a>
        </td>
    </tr>--%>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <ext:Window ID="Window2" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:Window ID="HiddenWindowFormSpecial" Title="" Popup="false" EnableIFrame="true"
        runat="server" CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="50px"
        Height="50px" Left="-1000px" Top="-1000px">
    </ext:Window>
    </form>
</body>
</html>
