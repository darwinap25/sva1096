﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="Panel1" Icon="SystemSaveClose" OnClick="btnSaveClose_Click"
                        runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本信息">
                <Items>
                    <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:TextBox ID="CardGradeCode" runat="server" Label="卡级别编号：" MaxLength="20" Required="true"
                                ShowRedStar="true" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true"
                                RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" ToolTipTitle="卡级别编号" ToolTip="Translate__Special_121_StartKey identifier of Card Grade. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CardGradeName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                                ShowRedStar="true" ToolTipTitle="描述" ToolTip="请输入规范的卡級別名称。不能超過512個字符" 
                                OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="CardGradeName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1："
                                ToolTip="對卡級別的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="CardGradeName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2："
                                ToolTip="對卡級別的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" Required="true" ShowRedStar="true"
                                OnSelectedIndexChanged="CardTypeID_SelectedIndexChanged" AutoPostBack="true"
                                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
<%--                            <ext:TextArea ID="CardGradeNotes" runat="server" Height="100px" Label="条款条例：" Text=""
                                MaxLength="512" Required="true" ShowRedStar="true" ToolTipTitle="条款条例" ToolTip="请输入条框条例的描述，不能超过512位">
                            </ext:TextArea>--%>
                            <ext:HtmlEditor ID="MemberClauseDesc1" runat="server" Height="200" Label="条款条例1：" ShowRedStar="true" ></ext:HtmlEditor>
                            <ext:HtmlEditor ID="MemberClauseDesc2" runat="server" Height="200" Label="条款条例2："></ext:HtmlEditor>
                            <ext:HtmlEditor ID="MemberClauseDesc3" runat="server" Height="200" Label="条款条例3："></ext:HtmlEditor>
                            <ext:HiddenField ID="uploadFilePath" runat="server">
                            </ext:HiddenField>
                            <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false"
                                runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                                        <Items>
                                            <ext:FileUpload ID="CardGradePicFile" runat="server" Label="卡图片：" ToolTipTitle="卡图片"
                                                ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                                            </ext:FileUpload>
                                            <ext:Button ID="btnPreview" runat="server" Text="预览" OnClick="ViewPicture" Hidden="true">
                                            </ext:Button>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:Form ID="Form4" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow17" ColumnWidths="85% 15%" runat="server">
                                        <Items>
                                            <ext:FileUpload ID="CardGradeLayoutFile" runat="server" Label="卡设计模板：" ToolTipTitle="卡设计模板"
                                                ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End">
                                            </ext:FileUpload>
                                            <ext:Label ID="label" runat="server" HideMode="Display" Hidden="true"></ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:Form ID="Form3" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                                <Rows>
                                    <ext:FormRow ID="FormRow2" ColumnWidths="85% 15%" runat="server">
                                        <Items>
                                            <ext:FileUpload ID="CardGradeStatementFile" runat="server" Label="卡结算单模板：" ToolTipTitle="卡结算单模板"
                                                ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持XLS,XLSX,DOC,TXT,RAR,文件大小不能超過10240KBTranslate__Special_121_End">
                                            </ext:FileUpload>
                                            <ext:Label ID="label1" runat="server" HideMode="Display" Hidden="true"></ext:Label>
                                        </Items>
                                    </ext:FormRow>
                                </Rows>
                            </ext:Form>
                            <ext:DropDownList ID="PasswordRuleID" runat="server" Label="密码规则：" Required="true"
                                ShowRedStar="true" Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                                CompareMessage="请选择有效值">
                            </ext:DropDownList>
                            <ext:DropDownList ID="CampaignID" runat="server" Label="活动：" Hidden="true" Resizable="true">
                            </ext:DropDownList>
                            <%--<ext:TextBox ID="" runat="server" Label="：" MaxLength="2" Required="true"
                                ShowRedStar="true" ToolTipTitle="卡級別序號" ToolTip="">
                            </ext:TextBox>--%>                                                
                            <ext:NumberBox ID="CardGradeRank" runat="server" Label="卡级别等级：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="卡級別序號" ToolTip="作为卡之间级别高低的定义。数字越大，级别越高。"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                            <%--add by Alex 2014-06-30 --%>
                              <ext:RadioButtonList ID="TrainingMode" runat="server" Label="Translate__Special_121_Start是否训练卡（是/否）：Translate__Special_121_End"
                                Width="200px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                             <%--add by Alex 2014-06-30 --%>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="卡号规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm2" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:RadioButtonList ID="IsImportUIDNumber" runat="server" Label="卡号码是否导入：" OnSelectedIndexChanged="IsImportUIDNumber_SelectedIndexChanged"
                                AutoPostBack="true" Width="100px">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="true" />
                            </ext:RadioButtonList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="ManualRoles" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm3" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                          <%--add by Alex 2014-06-30--%>
                            <ext:RadioButtonList ID="AllowOfflineQRCode" runat="server" Label="Translate__Special_121_Start是否允许离线的QR码（是/否）：Translate__Special_121_End" Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0"  />
                            </ext:RadioButtonList>
                             <ext:DropDownList ID="QRCodePrefix" runat="server" Label="Translate__Special_121_StartQR码前缀号码：Translate__Special_121_End" Resizable="true">
                                <ext:ListItem Value="" Text="-------" />
                              <ext:ListItem Value="Q7FC" Text="Q7FM" />
                            </ext:DropDownList>
                        <%--add by Alex 2014-06-30--%>
                            <ext:TextBox ID="CardNumMask" runat="server" Label="卡号编码规则：" Regex="(^A*9+$)|(^9+$)"
                                RegexMessage="编码规则输入不正确" ShowRedStar="true" AutoPostBack="true" OnTextChanged="CardNumMask_TextChanged"
                                ToolTipTitle="卡号编码规则" ToolTip="Translate__Special_121_Start 1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999，其中自增长的号码不能超过18位。Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CardNumPattern" runat="server" Label="卡号前缀号码：" RegexPattern="NUMBER"
                                RegexMessage="编码规则输入不正确" ShowRedStar="true" AutoPostBack="true" OnTextChanged="CardNumPattern_TextChanged"
                                ToolTipTitle="卡号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="CardCheckdigit" runat="server" Label="卡号是否添加校验位：" AutoPostBack="True"
                                OnSelectedIndexChanged="CardCheckdigit_SelectedIndexChanged" Width="100px">
                                <ext:RadioItem Text="是" Value="1" />
                                <ext:RadioItem Text="否" Value="0" Selected="True" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="EAN13" />
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="CardNumberToUID" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End"
                                AutoPostBack="true" OnSelectedIndexChanged="CardNumberToUID_SelectedIndexChanged"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="全部复制" />
                                <ext:ListItem Value="0" Text="绑定" />
                                <ext:ListItem Value="2" Text="复制去掉校验位" />
                                <ext:ListItem Value="3" Text="复制加上校验位" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="ImportRoles" runat="server" EnableCollapse="True" Title="导入物理编号规则"
                Hidden="true">
                <Items>
                    <ext:SimpleForm ID="SimpleForm4" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                      
                            <ext:TextBox ID="CardNumMaskImport" runat="server" Label="卡号编码规则：" ShowRedStar="true"
                                Regex="(^A*9+$)|(^9+$)" RegexMessage="编码规则输入不正确" AutoPostBack="True" OnTextChanged="CardNumMaskImport_TextChanged"
                                ToolTipTitle="卡号编码规则" ToolTip="Translate__Special_121_Start1~20個字符。必須输入“A”或者“9”的字符。“A”表示前綴，“9”表示自增長的號碼。例如AAAAA999999Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="CardNumPatternImport" runat="server" Label="卡号前缀号码：" ShowRedStar="true"
                                RegexPattern="NUMBER" RegexMessage="编码规则输入不正确" AutoPostBack="true" OnTextChanged="CardNumPatternImport_TextChanged"
                                ToolTipTitle="卡号前缀号码" ToolTip="必須输入數字或者字母，輸入長度必須與前綴長度一致。例如：80901">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End"
                                Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End"
                                Width="100px" OnSelectedIndexChanged="UIDCheckDigit_SelectedIndexChanged" AutoPostBack="true">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:DropDownList ID="CheckDigitModeID2" runat="server" Label="校验位计算方法：" Width="100px"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="EAN13" Selected="true"></ext:ListItem>
                                <ext:ListItem Value="2" Text="MOD10" />
                            </ext:DropDownList>
                            <ext:DropDownList ID="UIDToCardNumber" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End"
                                AutoPostBack="true" OnSelectedIndexChanged="UIDToCardNumber_SelectedIndexChanged"
                                Resizable="true">
                                <ext:ListItem Value="1" Text="全部复制" />
                                <ext:ListItem Value="0" Text="绑定" />
                                <ext:ListItem Value="2" Text="复制去掉校验位" />
                                <ext:ListItem Value="3" Text="复制加上校验位" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="使用规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm5" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:Label ID="label4" runat="server" Label="Translate__Special_121_Start发行品牌/店铺：Translate__Special_121_End" Text="请新增成功卡级别后添加该项。">
                            </ext:Label>
                            <ext:Label ID="label5" runat="server" Label="Translate__Special_121_Start使用品牌/店铺：Translate__Special_121_End" Text="请新增成功卡级别后添加该项。">
                            </ext:Label>
                            <ext:RadioButtonList ID="IsAllowStoreValue" runat="server" Label="是否允许储值：" Width="100px" OnSelectedIndexChanged="IsAllowStoreValue_OnSelectedIndexChanged" AutoPostBack="true">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="IsAllowConsumptionPoint" runat="server" Label="是否允许积分："
                                Width="100px" OnSelectedIndexChanged="IsAllowConsumptionPoint_OnSelectedIndexChanged" AutoPostBack="true">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:TextBox ID="CardTypeInitAmount" runat="server" Label="初始金额：" MaxLength="20"
                                Required="true" ShowRedStar="true" Text="0.00" ToolTipTitle="初始金额" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>                 
                            <ext:NumberBox ID="CardTypeInitPoints" runat="server" Label="初始积分：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="初始积分" ToolTip="请输入正整數，不允许输入小数"
                                MaxValue="100000000" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:TextBox ID="CardGradeMaxAmount" runat="server" Label="卡最大現金賬套金額：" MaxLength="20"
                                Text="999999.00" ToolTipTitle="卡最大現金賬套金額" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>                    
                            <ext:NumberBox ID="CardGradeMaxPoint" runat="server" Label="卡最大积分帐套积分：" NoNegative="true"
                                Text="999999" Required="true" ShowRedStar="true" ToolTipTitle="卡最大积分帐套积分" ToolTip="必須输入數字，並且是整數"
                                MaxValue="100000000" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:TextBox ID="MinAmountPreAdd" runat="server" Label="最低充值现金帐套金额：" MaxLength="20"
                                Text="0.00" ToolTipTitle="最低充值现金帐套金额" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>                       
                            <ext:NumberBox ID="MinPointPreAdd" runat="server" Label="最低充值积分帐套积分：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="最低充值积分帐套积分" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999999" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:TextBox ID="MaxAmountPreAdd" runat="server" Label="最高充值现金帐套金额：" MaxLength="20"
                                Text="999999.00" ToolTipTitle="最高充值现金帐套金额" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>                        
                            <ext:NumberBox ID="MaxPointPreAdd" runat="server" Label="最高充值积分帐套积分：" NoNegative="true"
                                Text="999999" Required="true" ShowRedStar="true" ToolTipTitle="最高充值积分帐套积分" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999999" NoDecimal="true">
                            </ext:NumberBox>                          
                            <ext:NumberBox ID="CardGradeDiscCeiling" runat="server" Label="折扣最高值：" NoNegative="true"
                                Text="100" Required="true" ShowRedStar="true" ToolTipTitle="折扣最高值" ToolTip="请输入0~100的整數，不允許輸入小數"
                                MaxValue="100" NoDecimal="true">
                            </ext:NumberBox>                          
                            <ext:NumberBox ID="CardConsumeBasePoint" runat="server" Label="最小消费积分（每次）：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="消费积分的最低值" ToolTip="必須输入數字，並且是整數"
                                MaxValue="100000000" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:NumberBox ID="MinBalancePoint" runat="server" Label="最小剩余积分：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="最小剩余积分" ToolTip="请输入正整數，不允许输入小数"
                                MaxValue="100000000" NoDecimal="true">
                            </ext:NumberBox>

                            <ext:TextBox ID="MinConsumeAmount" runat="server" Label="最小消费金额（每次）：" MaxLength="10"
                                Text="0.00" Required="true" ShowRedStar="true" ToolTipTitle="最小消费金额(每一次)" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="MinBalanceAmount" runat="server" Label="最小剩余金额：" MaxLength="10"
                                Text="0.00" Required="true" ShowRedStar="true" RegexMessage="请输入正數，保留兩位小數" ToolTipTitle="最小剩余金额"
                                ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:RadioButtonList ID="ForfeitAmountAfterExpired" runat="server" Label="过期后是否清空帐套金额："
                                Width="200px">
                                <ext:RadioItem Text="不清零" Value="0" Selected="True" />
                                <ext:RadioItem Text="清零" Value="1" />
                            </ext:RadioButtonList>
                            <ext:RadioButtonList ID="ForfeitPointAfterExpired" runat="server" Label="过期后是否清空帐套积分："
                                Width="200px">
                                <ext:RadioItem Text="不清零" Value="0" Selected="True" />
                                <ext:RadioItem Text="清零" Value="1" />
                            </ext:RadioButtonList>
                            <%--Add by Alex 2014-06-30 ++--%>
                            <ext:NumberBox ID="NumberOfTransDisplay" runat="server" Label="交易数据显示数量：" NoNegative="true"
                                Text="0"  ToolTipTitle="交易数据显示数量" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="NumberOfCouponDisplay" runat="server" Label="优惠券显示数量： " NoNegative="true"
                                Text="0"  ToolTipTitle="优惠券显示数量" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="NumberOfNewsDisplay" runat="server" Label="广告显示数量：" NoNegative="true"
                                Text="0"  ToolTipTitle="广告显示数量" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                              
                            </ext:NumberBox>
                               <%--Add by Alex 2014-06-30 ++--%>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="升级规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm6" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:DropDownList ID="CardGradeUpdMethod" runat="server" Label="升级条件：" AutoPostBack="true"
                                OnSelectedIndexChanged="CardGradeUpdMethod_OnSelectedIndexChanged" Resizable="true">
                                <ext:ListItem Value="0" Text="无" Selected="true" />
                                <ext:ListItem Value="3" Text="单笔消费" />
                                <ext:ListItem Value="1" Text="年累积购买值大于" />
                                <ext:ListItem Value="2" Text="年累计积分值大于" />
                                <ext:ListItem Value="4" Text="获得升级需要的优惠券类型指定的优惠券数量达到指定值" />
                            </ext:DropDownList>
                            <%--<ext:TextBox ID="" runat="server" Label="：" MaxLength="20"
                                Text="0" RegexPattern="NUMBER" ToolTipTitle="界限值" ToolTip="请输入正數，最大兩位小數">
                            </ext:TextBox>--%>                            
                            <ext:NumberBox ID="CardGradeUpdThreshold" runat="server" Label="界限值：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="界限值" ToolTip="必須输入數字，並且是整數"
                                MaxValue="100000000" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:TextBox ID="CardGradeUpdHitPLU" runat="server" Label="指定商品：">
                            </ext:TextBox>
                            <ext:DropDownList ID="CardGradeUpdCouponTypeID" runat="server" Label="升级需要的优惠券类型：" Resizable="true">
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="有效期规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm7" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:NumberBox ID="CardValidityDuration" runat="server" Label="有效期长度：" NoNegative="true"
                                 Required="true" ShowRedStar="true" ToolTipTitle="有效期长度" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:DropDownList ID="CardValidityUnit" runat="server" Label="有效期长度单位：" Required="true"
                                ShowRedStar="true" Resizable="true">
                                <ext:ListItem Value="1" Text="年" Selected="true" />
                                <ext:ListItem Value="2" Text="月" />
                                <ext:ListItem Value="3" Text="星期" />
                                <ext:ListItem Value="4" Text="天" />
                            </ext:DropDownList>
                            <ext:RadioButtonList ID="ActiveResetExpiryDate" runat="server" Label="激活是否重置有效期："
                                Width="100px">
                                <ext:RadioItem Text="是" Value="1" Selected="True" />
                                <ext:RadioItem Text="否" Value="0" />
                            </ext:RadioButtonList>
                            <ext:NumberBox ID="GracePeriodValue" runat="server" Label="宽限期值：" NoNegative="true"
                                Required="true" ShowRedStar="true" ToolTipTitle="宽限期值" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:DropDownList ID="GracePeriodUnit" runat="server" Label="宽限期单位：" Required="true"
                                ShowRedStar="true" Resizable="true">
                                <ext:ListItem Value="1" Text="年" Selected="true" />
                                <ext:ListItem Value="2" Text="月" />
                                <ext:ListItem Value="3" Text="星期" />
                                <ext:ListItem Value="4" Text="天" />
                            </ext:DropDownList>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel7" runat="server" EnableCollapse="True" Title="转赠/转换规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm8" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:NumberBox ID="CardPointToAmountRate" runat="server" Label="积分转换现金规则：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="积分转换现金规则" ToolTip="必須输入數字，並且是整數"
                                MaxValue="100000000" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:TextBox ID="CardAmountToPointRate" runat="server" Label="现金转换积分规则：" MaxLength="13"
                                Text="0.00"  Required="true" ShowRedStar="true" RegexMessage="请输入正數，保留兩位小數" ToolTipTitle="现金转换积分规则" ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:DropDownList ID="CardPointTransfer" runat="server" Label="积分转换规则（转出）：" Required="true"
                                ShowRedStar="true" AutoPostBack="True" OnSelectedIndexChanged="CardPointTransfer_SelectedIndexChanged"
                                Resizable="true">
                                <ext:ListItem Value="0" Text="不允许" Selected="true" />
                                <ext:ListItem Value="1" Text="允许同卡级别" />
                                <ext:ListItem Value="2" Text="允许同卡类型" />
                                <ext:ListItem Value="3" Text="允许同品牌" />
                                <ext:ListItem Value="4" Text="允许同发行商" />
                            </ext:DropDownList>
                            <ext:NumberBox ID="MinPointPreTransfer" runat="server" Label="每笔交易最小转换积分：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最小转换积分" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999999" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:NumberBox ID="MaxPointPreTransfer" runat="server" Label="每笔交易最大转换积分：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最大转换积分" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999999" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:NumberBox ID="DayMaxPointTransfer" runat="server" Label="每天最大转赠积分：" NoNegative="true"
                                Text="0" Required="true" ShowRedStar="true" ToolTipTitle="每天最大转赠积分" ToolTip="必須输入數字，並且是整數"
                                MaxValue="999999" NoDecimal="true">
                            </ext:NumberBox>
                            <ext:DropDownList ID="CardAmountTransfer" runat="server" Label="现金转换规则：" Required="true"
                                ShowRedStar="true" AutoPostBack="True" OnSelectedIndexChanged="CardAmountTransfer_SelectedIndexChanged"
                                Resizable="true">
                                <ext:ListItem Value="0" Text="不允许" Selected="true" />
                                <ext:ListItem Value="1" Text="允许同卡级别" />
                                <ext:ListItem Value="2" Text="允许同卡类型" />
                                <ext:ListItem Value="3" Text="允许同品牌" />
                                <ext:ListItem Value="4" Text="允许同发行商" />
                            </ext:DropDownList>
                            <ext:TextBox ID="MinAmountPreTransfer" runat="server" Label="每笔交易最小转换金额：" 
                                Text="0.00" Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最小转换金额"
                                ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="MaxAmountPreTransfer" runat="server" Label="每笔交易最大转换金额：" 
                                Text="0.00" Required="true" ShowRedStar="true" ToolTipTitle="每笔交易最大转换金额"
                                ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>
                            <ext:TextBox ID="DayMaxAmountTransfer" runat="server" Label="每天最大转赠金额：" 
                                Text="999999.00" Required="true" ShowRedStar="true" ToolTipTitle="每天最大转赠金额"
                                ToolTip="请输入正數，保留兩位小數" OnTextChanged="ConvertTextboxToDecimal" AutoPostBack="true">
                            </ext:TextBox>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <%--add by Alex 2014-06-30 ++--%>
                <ext:GroupPanel ID="GroupPanel9" runat="server" EnableCollapse="True" Title="卡级别时间规则">
                <Items>
                    <ext:SimpleForm ID="SForm10" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:NumberBox ID="MobileProtectPeriodValue" runat="server" Label="手机号码重用时间数值（月）：" NoNegative="true"
                                Text="0" ToolTipTitle="手机号码重用时间数值（月）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="EmailValidatedPeriodValue" runat="server" Label="邮箱验证时间数值（分钟）：" NoNegative="true"
                                Text="0" ToolTipTitle="邮箱验证时间数值（分钟）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="NonValidatedPeriodValue" runat="server" Label="未验证用户重设时间数值（小时）：" NoNegative="true"
                                Text="0" ToolTipTitle="未验证用户重设时间数值（小时）：" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="LoginFailureCount" runat="server" Label="登录错误次数：" NoNegative="true"
                                Text="0" ToolTipTitle="登录错误次数" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>

                             <ext:NumberBox ID="SessionTimeoutValue" runat="server" Label="登录超时数值（小时）：" NoNegative="true"
                                Text="0" ToolTipTitle="登录超时数值（小时）" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                             <ext:NumberBox ID="QRCodePeriodValue" runat="server" Label="Translate__Special_121_StartQR码有效时间（分钟）：Translate__Special_121_End" NoNegative="true"
                                Text="0" ToolTipTitle="Translate__Special_121_StartQR码有效时间（分钟）Translate__Special_121_End" ToolTip="必須输入數字，並且是整數"
                                MaxValue="99" NoDecimal="true">
                            </ext:NumberBox>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>

            <%--add by Alex 2014-06-30 ++--%>
            <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="可获取优惠券类型规则">
                <Items>
                    <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
                            <ext:NumberBox ID="HoldCouponCount" runat="server" Label="可获取累计最大数量：" NoNegative="true"
                                NoDecimal="true" ToolTipTitle="可获取累计最大数量" ToolTip="请输入正數" AutoPostBack="true"
                                OnTextChanged="HoldCouponCount_OnTextChanged" MaxValue="100000000" Hidden="true">
                            </ext:NumberBox>
                        </Items>
                    </ext:SimpleForm>
                    <ext:Grid ID="Grid_CanHoldCouponGradeList" ShowBorder="true" ShowHeader="true" Title="可获取优惠券类型列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid_CanHoldCouponGradeList_OnPageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="BrandDescription" DataField="BrandDescription" HeaderText="品牌编号" />
                            <ext:BoundField ColumnID="CouponTypeDescription" DataField="CouponTypeDescription"
                                HeaderText="优惠券类型" />
                            <ext:BoundField ColumnID="MainTable_HoldCount" DataField="MainTable.HoldCount" HeaderText="单个获取最大数量" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="自动绑定优惠券类型规则">
                <Items>
                    <ext:Grid ID="Grid_GetCouponGradeList" ShowBorder="true" ShowHeader="true" Title="自动绑定优惠券类型列表"
                        AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="Grid_GetCouponGradeList_OnPageIndexChange">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew1" Text="新增" Icon="Add" EnablePostBack="false" runat="server">
                                    </ext:Button>
                                    <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                        <Columns>
                            <ext:BoundField ColumnID="BrandDescription" DataField="BrandDescription" HeaderText="品牌编号" />
                            <ext:BoundField ColumnID="CouponTypeDescription" DataField="CouponTypeDescription"
                                HeaderText="优惠券类型" />
                            <ext:BoundField ColumnID="MainTable_HoldCount" DataField="MainTable.HoldCount" HeaderText="优惠券数量" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true"
        EnableResize="true" Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>

