﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.EarnCouponRules
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        public const string tab = "2";
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindKeyValueListNoEmpty(this.MemberDateType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
                ControlTool.BindKeyValueList(this.PointRuleType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.TransType),true);
                ControlTool.BindKeyValueList(this.PointRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ComputTypeCoupon),true);
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "IsImportCouponNumber = 0  order by CouponTypeCode");
            }

        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {

                string cardGradeId = Request.Params["CardGradeID"];
                this.CardGradeID.SelectedValue = cardGradeId;
                this.CardTypeID.SelectedValue = new Edge.SVA.BLL.CardGrade().GetModel(int.Parse(cardGradeId)).CardTypeID.ToString();
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            Edge.SVA.Model.EarnCouponRule item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.EarnCouponRule>(item) > 0)
            {
                SVASessionInfo.Tabs = Add.tab;
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + Request.Params["CardGradeID"] + "&tabs="+Add.tab);
        }
    }
}
