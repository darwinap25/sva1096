﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using FineUI;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardExtensionRule,Edge.SVA.Model.CardExtensionRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
            }

        }

        //Add By Robin 2014-09-15
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                //string cardGradeId = Request.Params["CardGradeID"];
                //this.CardGradeID.SelectedValue = cardGradeId;
                //this.CardTypeID.SelectedValue = new Edge.SVA.BLL.CardGrade().GetModel(int.Parse(cardGradeId)).CardTypeID.ToString();
                
                if (this.ExtensionUnit.SelectedValue == "6")
                {
                    this.SpecifyExpiryDate.Hidden = false;
                }
                else
                {
                    this.SpecifyExpiryDate.Hidden = true;
                }
            }
        }
        //End

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.CardExtensionRule item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Update<Edge.SVA.BLL.CardExtensionRule>(item))
            {
                SVASessionInfo.Tabs = Add.tab;
                //CloseAndPostBack();
                CloseAndRefresh();
            }
            else
            {
                ShowUpdateFailed();
            }

        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + this.CardGradeID.SelectedValue + "&tabs=2");
        }


        //Add By Robin 2014-09-15
        protected void ExtensionUnit_Changed(object sender, EventArgs e)
        {
            if (this.ExtensionUnit.SelectedValue == "6")
            {
                this.SpecifyExpiryDate.Hidden = false;
            }
            else
            {
                this.SpecifyExpiryDate.Hidden = true;
            }
        }
        //End
    }
}
