﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.EarnCouponRules.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:NumberBox ID="PointRuleSeqNo" runat="server" Label="规则编号：" ShowRedStar="true"
                Required="true">
            </ext:NumberBox>
            <ext:DropDownList ID="CardTypeID" runat="server" Enabled="false" Label="卡类型：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeID" runat="server" Enabled="false" Label="卡等级：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="MemberDateType" runat="server" Label="会员范围：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="PointRuleType" runat="server" Label="交易类型：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="PointRuleOper" runat="server" Label="计算方式设置：" Resizable="true"
                ShowRedStar="true" Required="true" ToolTipTitle="计算方式设置" ToolTip="Translate__Special_121_Start注意：0：每当消费PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分。  如：PointRuleAmount=100，PointRulePoints=10， 消费2000， 就可以获得 （2000 /100 ）* 10 = 2001：当消费金额大于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分2：当消费金额小于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分Translate__Special_121_End">
            </ext:DropDownList>
            <ext:NumberBox ID="PointRuleAmount" runat="server" Label="设定的积分规则金额：" NoDecimal="false"
                ShowRedStar="true" Required="true" Text="0" MaxValue="99999999" MinValue="0">
            </ext:NumberBox>
            <ext:DropDownList ID="CouponTypeID" runat="server" Label="优惠券类型：" OnSelectedIndexChanged="CouponTypeID_SelectedIndexChanged"
                AutoPostBack="true" Required="true" ShowRedStar="true" Resizable="true" CompareType="String"
                CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:NumberBox ID="PointRulePoints" runat="server" Label="设定的积分规则积分：" NoDecimal="true"
                ShowRedStar="true" Required="true" Text="0" MaxValue="99999999" MinValue="0">
            </ext:NumberBox>
            <ext:DatePicker ID="StartDate" runat="server" Label="开始日期：" DateFormatString="yyyy-MM-dd"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:DatePicker ID="EndDate" runat="server" Label="结束日期：" DateFormatString="yyyy-MM-dd"
                CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="结束日期应该大于等于开始日期"
                ShowRedStar="true" Required="true">
            </ext:DatePicker>
            <ext:RadioButtonList ID="Status" runat="server" Label="状态：" Width="200px">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
