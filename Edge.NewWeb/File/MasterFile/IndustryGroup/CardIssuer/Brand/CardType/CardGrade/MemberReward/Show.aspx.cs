﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.MemberReward
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.SVARewardRules,Edge.SVA.Model.SVARewardRules>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, "Show");
                RegisterCloseEvent(btnClose);

                //Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                //Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                Edge.Web.Tools.ControlTool.BindCouponType(this.RewardCouponTypeID, "IsImportCouponNumber = 0  order by CouponTypeCode");
                ControlTool.BindKeyValueList(this.SVARewardType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.SVARewardType), true);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.RewardCouponTypeIDName.Text = this.RewardCouponTypeID.SelectedText;
                this.RewardCouponTypeID.Hidden = true;
                this.StatusName.Text = this.Status.SelectedItem.Text;
                this.Status.Hidden = true;

                this.SVARewardTypeView.Text = this.SVARewardType.SelectedText;
            }
        }
    }
}
