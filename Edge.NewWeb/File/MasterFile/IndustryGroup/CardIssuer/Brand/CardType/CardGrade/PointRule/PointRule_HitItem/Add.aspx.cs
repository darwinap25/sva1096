﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule.PointRule_HitItem
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PointRule_Item, Edge.SVA.Model.PointRule_Item>
    {
        PointRuleController controller = new PointRuleController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(this.btnClose);

                this.PointRuleCode.Text = Request.Params["PointRuleCode"].ToString();
            }
            controller = SVASessionInfo.PointRuleController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.SubTable.Columns.Count == 0)
            {
                controller.ViewModel.SubTable.Columns.Add("PointRuleCode", typeof(string));
                controller.ViewModel.SubTable.Columns.Add("EntityType", typeof(int));
                controller.ViewModel.SubTable.Columns.Add("EntityNum", typeof(string));
                controller.ViewModel.SubTable.Columns.Add("EntityTypeName", typeof(string));
            }
            DataRow dr = controller.ViewModel.SubTable.NewRow();
            dr["PointRuleCode"] = this.PointRuleCode.Text;
            dr["EntityType"] = Convert.ToInt32(this.EntityType.SelectedValue);
            dr["EntityNum"] = this.EntityNum.Text;

            dr["EntityTypeName"] = this.EntityType.SelectedText;
            controller.ViewModel.SubTable.Rows.Add(dr);
            CloseAndPostBack();
        }
    }
}