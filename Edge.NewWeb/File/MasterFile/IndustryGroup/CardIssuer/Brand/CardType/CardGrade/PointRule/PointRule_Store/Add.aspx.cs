﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule.PointRule_Store
{
    public partial class Add : PageBase
    {
        PointRuleController controller = new PointRuleController();
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ControlTool.BindStoreType(this.StoreType);
                ControlTool.BindBrand(this.Brand);
                ControlTool.BindStoreGroup(this.StoreGroup);

                RegisterCloseEvent(btnClose);
            }
            controller = SVASessionInfo.PointRuleController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (controller.ViewModel.SubTable.Columns.Count == 0)
            {
                controller.ViewModel.SubTable.Columns.Add("PointRuleCode", typeof(string));
                controller.ViewModel.SubTable.Columns.Add("StoreID", typeof(string));
                controller.ViewModel.SubTable.Columns.Add("StoreName", typeof(string));
            }
            bool flag = false;
            DataTable viewDT = controller.ViewModel.SubTable.Clone();
            for (int i = 0; i < ckbStoreID.Items.Count; i++)
            {
                if (ckbStoreID.Items[i].Selected)
                {
                    flag = true;
                    int storeID = Edge.Web.Tools.ConvertTool.ToInt(ckbStoreID.Items[i].Value.ToString());
                    DataRow dr = viewDT.NewRow();
                    dr["PointRuleCode"] = Request.Params["PointRuleCode"].ToString();
                    dr["StoreID"] = storeID;
                    dr["StoreName"] = ckbStoreID.Items[i].Text.ToString();
                    viewDT.Rows.Add(dr);
                }
            }
            if (!flag)
            {
                ShowWarning(Resources.MessageTips.NotSelected);
                return;
            }
            //防止多次选取相同记录的处理
            if (controller.ViewModel.SubTable != null && controller.ViewModel.SubTable.Rows.Count > 0)
            {
                string ids = "";
                for (int i = 0; i < controller.ViewModel.SubTable.Rows.Count; i++)
                {
                    ids += string.Format("{0},", "'" + controller.ViewModel.SubTable.Rows[i]["StoreID"].ToString() + "'");
                }
                DataView dvSearch = viewDT.DefaultView;
                dvSearch.RowFilter = " StoreID not in (" + ids.TrimEnd(',') + ")";
                DataTable dtSearch = dvSearch.ToTable();
                controller.ViewModel.SubTable.Merge(dtSearch);
            }
            else
            {
                controller.ViewModel.SubTable.Merge(viewDT);
            }
            CloseAndPostBack();
        }

        protected void cbAll_CheckChanged(object sender, EventArgs e)
        {
            foreach (FineUI.CheckItem cb in this.ckbStoreID.Items) cb.Selected = cbAll.Checked;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.Brand.SelectedValue), ConvertTool.ToInt(this.StoreType.SelectedValue), ConvertTool.ToInt(this.StoreGroup.SelectedValue), StoreName.Text.Trim());
        }
    }
}