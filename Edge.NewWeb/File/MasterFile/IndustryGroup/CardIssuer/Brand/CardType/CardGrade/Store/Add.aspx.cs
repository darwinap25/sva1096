﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Store
{
    public partial class Add : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ControlTool.BindStoreType(this.StoreTypeID);
                ControlTool.BindBrand(BrandID);

                ViewState["cardGradeID"] = Request.Params["CardGradeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CardGradeID"].ToString());
                ViewState["storeConditionTypeID"] = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());
                ViewState["conditionTypeID"] = 3;
                this.txtCount.Text = webset.ContentPageNum.ToString();

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //  int storeID =Edge.Web.Tools.ConvertTool.ToInt(this.StoreID.SelectedValue);
            logger.WriteOperationLog(this.PageName, "Add");
            for (int i = 0; i < ckbStoreID.Items.Count; i++)
            {
                if (ckbStoreID.Items[i].Selected)
                {
                    int storeID = Edge.Web.Tools.ConvertTool.ToInt(ckbStoreID.Items[i].Value.ToString());

                    Edge.SVA.Model.CardGradeStoreCondition item = new Edge.SVA.Model.CardGradeStoreCondition();
                    item.CardGradeID = Convert.ToInt32(ViewState["cardGradeID"]);
                    item.StoreConditionType = Convert.ToInt32(ViewState["storeConditionTypeID"]);
                    item.ConditionType = Convert.ToInt32(ViewState["conditionTypeID"]);
                    item.ConditionID = storeID;
                    item.CreatedBy = DALTool.GetCurrentUser().UserID;
                    item.CreatedOn = DateTime.Now;


                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.Append(string.Format(" CardGradeID={0} and StoreConditionType={1} and ConditionType={2} and ConditionID={3}", ViewState["cardGradeID"], ViewState["storeConditionTypeID"], 3, storeID));
                    if (new Edge.SVA.BLL.CardGradeStoreCondition().GetCount(sbWhere.ToString()) <= 0)
                    {
                        DALTool.Add<Edge.SVA.BLL.CardGradeStoreCondition>(item);
                    }
                }
            }
            CloseAndRefresh();
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(this.BrandID.SelectedValue))
            //{
            //    Edge.Web.Tools.ControlTool.BindStore(this.StoreID,0, ConvertTool.ToInt(this.BrandID.SelectedValue));
            //    Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, 0, ConvertTool.ToInt(this.BrandID.SelectedValue));

            //}
        }

        protected void StoreTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if ((!string.IsNullOrEmpty(this.BrandID.SelectedValue)) && (!string.IsNullOrEmpty(this.StoreTypeID.SelectedValue)))
            //{
            //    Edge.Web.Tools.ControlTool.BindStore(this.StoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue),ConvertTool.ToInt(this.BrandID.SelectedValue));
            //    Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), ConvertTool.ToInt(this.BrandID.SelectedValue));

            //}
            //else if (!string.IsNullOrEmpty(this.StoreTypeID.SelectedValue))
            //{
            //    Edge.Web.Tools.ControlTool.BindStore(this.StoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue),0);
            //    Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), 0);
            //}
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //if ((!string.IsNullOrEmpty(this.BrandID.SelectedValue)) && (!string.IsNullOrEmpty(this.StoreTypeID.SelectedValue)))
            //{
            //    Edge.Web.Tools.ControlTool.BindStore(this.StoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), ConvertTool.ToInt(this.BrandID.SelectedValue));
            //    Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), ConvertTool.ToInt(this.BrandID.SelectedValue),StoreName.Text.Trim(),Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));

            //}
            //else if (!string.IsNullOrEmpty(this.StoreTypeID.SelectedValue))
            //{
            //    Edge.Web.Tools.ControlTool.BindStore(this.StoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), 0);
            //    Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), 0,StoreName.Text.Trim(),Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));
            //}
            //else
            //{
            //    StoreID.Items.Clear();
            //    ckbStoreID.Items.Clear();
            //}

            Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), ConvertTool.ToInt(this.BrandID.SelectedValue),StoreCode.Text.Trim(), StoreName.Text.Trim(), Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));

        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            //Response.Redirect(string.Format("List.aspx?page=0&CardGradeID={0}&StoreConditionTypeID={1}&type=2", ViewState["cardGradeID"], ViewState["storeConditionTypeID"]));
            FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
        }

        protected void cbAll_CheckChanged(object sender, EventArgs e)
        {
            foreach (FineUI.CheckItem item in this.ckbStoreID.Items)
            {
                if (this.cbAll.Checked)
                {
                    item.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }
    }
}
