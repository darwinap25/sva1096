﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.EarnCouponRules.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <%-- <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：显示积分规则</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                显示积分规则
            </th>
        </tr>
        <tr>
            <td  width="25%" align="right">
                卡类型：
            </td>
            <td width="75%">
                <asp:DropDownList ID="CardTypeID" TabIndex="2" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡等级：
            </td>
            <td>
                <asp:DropDownList ID="CardGradeID" TabIndex="3" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                计算方式设置：
            </td>
            <td>
                <asp:Label ID="PointRuleOper" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                金额：
            </td>
            <td>
                <asp:Label ID="PointRuleAmount" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                设定的积分规则积分：
            </td>
            <td>
                <asp:Label ID="PointRulePoints" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                开始日期：
            </td>
            <td>
                <asp:Label ID="StartDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                结束日期：
            </td>
            <td>
                <asp:Label ID="EndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="history.back(); " class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>--%>
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Label ID="PointRuleSeqNo" runat="server" Label="规则编号：">
            </ext:Label>
            <ext:Label ID="CardTypeID_Name" runat="server" Label="卡类型：">
            </ext:Label>
            <ext:Label ID="CardGradeID_Name" runat="server" Label="卡等级：">
            </ext:Label>
            <ext:Label ID="MemberDateType_Name" runat="server" Label="会员范围：">
            </ext:Label>
            <ext:Label ID="PointRuleType_Name" runat="server" Label="交易类型：">
            </ext:Label>
            <ext:Label ID="PointRuleOper_Name" runat="server" Label="计算方式设置：">
            </ext:Label>
            <ext:DropDownList ID="CardTypeID" runat="server" Enabled="false" Label="卡类型：">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeID" runat="server" Enabled="false" Label="卡等级：">
            </ext:DropDownList>
            <ext:DropDownList ID="MemberDateType" runat="server" Enabled="false"  Label="会员范围：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="PointRuleType" runat="server" Enabled="false"  Label="交易类型：" Resizable="true"
                ShowRedStar="true" Required="true">
            </ext:DropDownList>
            <ext:DropDownList ID="PointRuleOper" runat="server" Enabled="false"  Label="计算方式设置：" Resizable="true"
                ShowRedStar="true" Required="true">
            </ext:DropDownList>
            <ext:Label ID="PointRuleAmount" runat="server" Label="设定的积分规则金额：">
            </ext:Label>
            <ext:Label ID="PointRulePoints" runat="server" Label="设定的积分规则积分：">
            </ext:Label>
            <ext:Label ID="StartDate" runat="server" Label="开始日期：">
            </ext:Label>
            <ext:Label ID="EndDate" runat="server" Label="结束日期：">
            </ext:Label>
            <ext:RadioButtonList ID="Status" runat="server"  Label="状态：" Width="200px">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
            <ext:Label ID="Status_Name" runat="server" Label="状态：">
            </ext:Label>
            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
            </ext:Label>
            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
            </ext:Label>
            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
            </ext:Label>
            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
            </ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
