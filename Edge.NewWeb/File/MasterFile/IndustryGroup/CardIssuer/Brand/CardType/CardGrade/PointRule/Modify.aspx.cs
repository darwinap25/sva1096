﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PointRule,Edge.SVA.Model.PointRule>
    {
        PointRuleController controller;
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindKeyValueListNoEmpty(this.MemberDateType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
                ControlTool.BindKeyValueList(this.PointRuleType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.TransType),true);
                ControlTool.BindKeyValueList(this.PointRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ComputTypePoint),true);
            }
            controller = SVASessionInfo.PointRuleController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            //Edge.SVA.Model.PointRule item = this.GetUpdateObject();

            //if (item != null)
            //{
            //    item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //}

            //if (DALTool.Update<Edge.SVA.BLL.PointRule>(item))
            //{
            //    SVASessionInfo.Tabs = Add.tab;
            //    CloseAndRefresh();
            //}
            //else
            //{
            //    ShowUpdateFailed();
            //}
            controller.ViewModel.MainTable = this.GetUpdateObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                SVASessionInfo.Tabs = Add.tab;
                CloseAndRefresh();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + this.CardGradeID.SelectedValue + "&tabs=" + Add.tab);
        }

        #region 货品
        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = viewDT;
                this.AddResultListGrid.DataBind();
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }
        }

        protected void AddResultListGrid_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            AddResultListGrid.PageIndex = e.NewPageIndex;

            BindResultList(controller.ViewModel.SubTable);
        }
        #endregion

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.PointRuleCode);

                BindStoreList(controller.ViewModel.SubTable);
            }
        }

        #region 时间设置
        protected void btnAddDay_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("DAYFLAG/List.aspx"));
        }

        protected void btnAddWeek_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("WEEKFLAG/List.aspx"));
        }

        protected void btnAddMonth_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("MONTHFLAG/List.aspx"));
        }

        protected void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            this.DayFlagCode.Text = controller.ViewModel.DayCode;
            this.MonthFlagCode.Text = controller.ViewModel.MonthCode;
            this.WeekFlagCode.Text = controller.ViewModel.WeekCode;
        }
        #endregion

        #region 地点设置
        private void BindStoreList(DataTable dt)
        {
            if (dt != null)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.PageIndex = 0;
                this.Grid1.RecordCount = 0;
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            BindStoreList(controller.ViewModel.SubTable);
        }

        protected void btnAddStore_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window2.GetShowReference(string.Format("PointRule_Store/Add.aspx?PointRuleCode={0}", this.PointRuleCode.Text), "新增"));
        }

        protected void btnDeleteStore_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.SubTable != null)
            {
                DataTable addDT = controller.ViewModel.SubTable;

                foreach (int row in Grid1.SelectedRowIndexArray)
                {
                    string StoreID = Grid1.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["StoreID"].ToString().Trim() == StoreID)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.SubTable = addDT;
                BindStoreList(controller.ViewModel.SubTable);

            }
        }

        protected void btnClearStore_Click(object sender, EventArgs e)
        {
            ClearGird(this.Grid1);
            controller.ViewModel.SubTable = null;
        }

        protected void Window2_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            BindStoreList(controller.ViewModel.SubTable);
        }
        #endregion
    }
}
