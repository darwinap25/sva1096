﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Brand.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--  <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>--%>
    <%--  <style type="text/css">
        #ckbBrandID
        {
            border-collapse: collapse;
            line-height: 18px;
        }
        #ckbBrandID td
        {
            width: 300px;
            padding-left: 5px;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSearch" runat="server" Text="搜 索" OnClick="btnSearch_Click" Icon="Find"/>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:TextBox ID="BrandCode" runat="server" Label="品牌编号：" ToolTipTitle="品牌编号" ToolTip="Translate__Special_121_Start輸入Brand的编号Translate__Special_121_End"/>
                    <ext:TextBox ID="BrandName" runat="server" Label="品牌名：" ToolTipTitle="品牌名" ToolTip="Translate__Special_121_Start輸入Brand的名稱Translate__Special_121_End"/>
                    <ext:NumberBox ID="txtCount" runat="server" Label="查询数量：" ToolTipTitle="查询数量" ToolTip="输入查找的数量，只允許輸入數字"/>
                </Items>
            </ext:SimpleForm>
            <ext:Panel ID="panel1" runat="server" Title="搜索结果" AutoScroll="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server">
                        <Items>
                            <ext:CheckBox ID="cbAll" runat="server" Text="全选" AutoPostBack="true" OnCheckedChanged="cbAll_CheckChanged">
                            </ext:CheckBox>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:CheckBoxList ID="ckbBrandID" runat="server" ColumnNumber="3" />
                </Items>
            </ext:Panel>
        </Items>
    </ext:SimpleForm>
    <%-- <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：增加品牌</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th align="left">
                查询品牌
            </th>
            <th align="right">
                <asp:Button ID="btnSearch" runat="server" Text="搜 索" CssClass="submit" OnClick="btnSearch_Click">
                </asp:Button>
            </th>
        </tr>
         <tr>
            <td width="25%" align="right">
                品牌编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="BrandCode" TabIndex="3" runat="server" MaxLength="20" CssClass="input"
                    ToolTipTitle="品牌编号" ToolTip="Translate__Special_121_Start輸入Brand的编号Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                品牌名：
            </td>
            <td width="75%" class="style1">
                <asp:TextBox ID="BrandName" TabIndex="3" runat="server" MaxLength="512" CssClass="input" ToolTipTitle="品牌名" ToolTip="Translate__Special_121_Start輸入Brand的名稱Translate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                查询数量：
            </td>
            <td>
                <asp:TextBox ID="txtCount" TabIndex="3" runat="server" MaxLength="8" CssClass="input svaQty" ToolTipTitle="查询数量" ToolTip="输入查找的数量，只允許輸入數字"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th  align="left">
                可选品牌
            </th>
                        <th align="right">
                全选<asp:CheckBox ID="ckbSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="ckbSelectAll_CheckedChanged" />
            </th>
        </tr>
        <tr>
            <td colspan="2" align="left">
                    <asp:CheckBoxList ID="ckbBrandID" runat="server" RepeatLayout="Table" RepeatColumns="3" RepeatDirection="Horizontal">
                    </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <asp:Button ID="btnReturn" runat="server" Text="返回" CssClass="submit cancel" OnClick="btnReturn_Click">
                    </asp:Button>
                    <%-- <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>--%>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
