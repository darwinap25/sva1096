﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using Edge.SVA.BLL.Domain.ViewModelBLL;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Linq;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardGrade, Edge.SVA.Model.CardGrade>,IForm
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CardGradeModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            //int tabNum = string.IsNullOrEmpty(Request.Params["tabs"]) ? 1 : int.Parse(Request.Params["tabs"]);
            //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "tabs", "<script type='text/javascript'> $(function() {tabs(" + tabNum + "); });</script>");

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                string id=Request.Params["id"].ToString();
                int tabs = 0;
                tabs = int.TryParse(Request.Params["tabs"], out tabs) ? tabs : SVASessionInfo.Tabs == null ? 0 : int.TryParse(SVASessionInfo.Tabs, out tabs) ? tabs : 0;
                this.TabStrip1.ActiveTabIndex = tabs < TabStrip1.Tabs.Count ? tabs : 0;

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);


                //CardExtensionRuleRptBind("CardGradeID=" + Request.Params["id"]);
                //CardPointRuleRptBind("CardGradeID=" + Request.Params["id"]);

                this.lbIssueBrand.OnClientClick = Window1.GetShowReference(string.Format("Brand/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=1", id));
                this.lbIssueStore.OnClientClick = Window1.GetShowReference(string.Format("Store/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=1", id));
                //Add by Alex 2014-06-05 ++
                this.lbPLUMapping.OnClientClick = Window1.GetShowReference(string.Format("PLUMapping/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=1", id));
                this.lbPLUSpecific.OnClientClick = Window1.GetShowReference(string.Format("SpecificPLU/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=1", id));
                this.lbSpecificDepartment.OnClientClick = Window1.GetShowReference(string.Format("SpecificDepartment/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=1", id));
                this.lblTenderMapping.OnClientClick = Window1.GetShowReference(string.Format("TenderMapping/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=1", id));
                //Add by Alex 2014-06-05 --
                this.lbUseBrand.OnClientClick = Window1.GetShowReference(string.Format("Brand/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=2", id));
                this.lbUseStore.OnClientClick = Window1.GetShowReference(string.Format("Store/List.aspx?Url=&CardGradeID={0}&type=2&StoreConditionTypeID=2", id));

                RegisterCloseEvent(btnClose);


                //其他规则初始化
                SetControl(this.lbtnAdd, this.lbtnDel, this.rptExtensionRuleList, "CardExtensionRule/Add.aspx?CardGradeID=" + id);
                SetControl(this.lbtnPointRuleAdd, this.lbtnPointRuleDel, this.rptEarnAmountPointList, "PointRule/Add.aspx?CardGradeID=" + id);
                SetControl(this.btnMemberRewardAdd, this.btnMemberRewardDelete, this.gridMemberReward, "MemberReward/Add.aspx?CardGradeID=" + id);

                CardExtensionRuleRptBind("CardGradeID=" + id);
                CardPointRuleRptBind("CardGradeID=" + id);
                CardSVAMemberRewardDataBind("CardGradeID=" + id);

                //this.Tab1.Hidden = this.Tab2.Hidden = true;

                SVASessionInfo.CardGradeModifyController = null;
                //this.Tab3.Visible = false;

                //新增字段CardGradeUpdCouponTypeID 
                ControlTool.BindCouponType(CardGradeUpdCouponTypeID);
            }
            controller = SVASessionInfo.CardGradeModifyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (Model != null)
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    //存在小图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.CardGradePicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }
                    //存在文件时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.CardGradeLayoutFile))
                    {
                        this.FormLoad1.Hidden = true;
                        this.FormReLoad1.Hidden = false;
                        this.btnBack1.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad1.Hidden = false;
                        this.FormReLoad1.Hidden = true;
                        this.btnBack1.Hidden = true;
                    }
                    //存在文件时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.CardGradeStatementFile))
                    {
                        this.FormLoad2.Hidden = true;
                        this.FormReLoad2.Hidden = false;
                        this.btnBack2.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad2.Hidden = false;
                        this.FormReLoad2.Hidden = true;
                        this.btnBack2.Hidden = true;
                    }

                    this.uploadFilePath.Text = Model.CardGradePicFile;
                    this.uploadFilePath1.Text = Model.CardGradeLayoutFile;
                    this.uploadFilePath2.Text = Model.CardGradeStatementFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    //新增字段CardGradeUpdCouponTypeID 
                    this.CardGradeUpdCouponTypeID.SelectedValue = Model.CardGradeUpdCouponTypeID.ToString();

                    #region SetAmount Value
                    SetAmountControll(CardTypeInitAmount, Model.CardTypeInitAmount);
                    SetAmountControll(CardGradeMaxAmount, Model.CardGradeMaxAmount);
                    SetAmountControll(MinAmountPreAdd, Model.MinAmountPreAdd);
                    SetAmountControll(MaxAmountPreAdd, Model.MaxAmountPreAdd);
                    SetAmountControll(MinConsumeAmount, Model.MinConsumeAmount);
                    SetAmountControll(MinBalanceAmount, Model.MinBalanceAmount);
                    SetAmountControll(CardAmountToPointRate, Model.CardAmountToPointRate);
                    SetAmountControll(MinAmountPreTransfer, Model.MinAmountPreTransfer);
                    SetAmountControll(MaxAmountPreTransfer, Model.MaxAmountPreTransfer);
                    SetAmountControll(DayMaxAmountTransfer, Model.DayMaxAmountTransfer);
                    #endregion

                }

                CheckCardPointTransfer();
                CheckCardAmountTransfer();

                //CardTypeID_SelectedIndexChanged(null, null);
                checkIsImportCardNumber(false);
                //注意  当上面CardTypeID_SelectedIndexChanged 注释的时候，这个方法要恢复
                if (Model != null)
                {
                    IsAllowStoreValue_OnSelectedIndexChanged(null, null);
                    IsAllowConsumptionPoint_OnSelectedIndexChanged(null, null);

                    this.CheckDigitModeID2.SelectedValue = Model.CheckDigitModeID.HasValue ? Model.CheckDigitModeID.Value.ToString() : "1";
                    this.CardPointToAmountRate.Text = ((int)Model.CardPointToAmountRate.GetValueOrDefault()).ToString("N00");       //todo: 数据库类型不正确
                    this.CardGradeDiscCeiling.Text = ((int)(Model.CardGradeDiscCeiling.GetValueOrDefault() * 100)).ToString();
                    this.CardNumMask.Text = Model.CardNumMask;
                    this.CardNumPattern.Text = Model.CardNumPattern;

                    this.CardNumMaskImport.Text = Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0 ? Model.CardNumMask : "";
                    this.CardNumPatternImport.Text = Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0 ? Model.CardNumPattern : "";
                    
                    //Html处理
                    //this.CardGradeNotes.Text = Edge.Common.Utils.ToTxt(Model.CardGradeNotes);

                    string msg = "";
                    if (DALTool.isCardGradeCreatedCard(Model.CardGradeID, ref msg))//已经创建卡
                    {
                        ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
                        tool.AddControl(this.IsImportUIDNumber);
                        tool.AddControl(CardNumMask);
                        tool.AddControl(CardNumPattern);
                        tool.AddControl(CardCheckdigit);
                        tool.AddControl(CardNumberToUID);
                        tool.AddControl(CheckDigitModeID);
                        tool.AddControl(IsConsecutiveUID);
                        tool.AddControl(UIDCheckDigit);
                        tool.AddControl(UIDToCardNumber);
                        tool.AddControl(CardTypeID);
                        tool.AddControl(CardGradeCode);
                        tool.AddControl(IsAllowStoreValue);
                        tool.AddControl(IsAllowConsumptionPoint);
                        tool.AddControl(CardTypeInitAmount);
                        tool.AddControl(CardTypeInitPoints);
                        tool.AddControl(CardGradeMaxAmount);
                        tool.AddControl(CardGradeMaxPoint);
                        tool.AddControl(MinAmountPreAdd);
                        tool.AddControl(MinPointPreAdd);
                        tool.AddControl(MaxAmountPreAdd);
                        tool.AddControl(MaxPointPreAdd);
                        tool.AddControl(CardGradeDiscCeiling);
                        tool.AddControl(CardConsumeBasePoint);
                        tool.AddControl(MinBalancePoint);
                        tool.AddControl(MinConsumeAmount);
                        tool.AddControl(MinBalanceAmount);
                        tool.AddControl(ForfeitAmountAfterExpired);
                        tool.AddControl(ForfeitPointAfterExpired);
                        tool.AddControl(CardGradeUpdMethod);
                        tool.AddControl(CardGradeUpdThreshold);
                        tool.AddControl(CardValidityDuration);
                        tool.AddControl(CardValidityUnit);
                        tool.AddControl(ActiveResetExpiryDate);
                        tool.AddControl(GracePeriodValue);
                        tool.AddControl(GracePeriodUnit);
                        tool.AddControl(CardPointToAmountRate);
                        tool.AddControl(CardAmountToPointRate);
                        tool.AddControl(CardPointTransfer);
                        tool.AddControl(MinPointPreTransfer);
                        tool.AddControl(MaxPointPreTransfer);
                        tool.AddControl(DayMaxPointTransfer);
                        tool.AddControl(CardAmountTransfer);
                        tool.AddControl(MinAmountPreTransfer);
                        tool.AddControl(MaxAmountPreTransfer);
                        tool.AddControl(DayMaxAmountTransfer);
                        
                        tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                    }
                    else//未创建卡
                    {
                    }



                    controller.LoadViewModel(Model.CardGradeID, SVASessionInfo.SiteLanguage);
                    if (controller.IsInheritsOfCardTypeRule)
                    {
                        ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
                        tool.AddControl(this.IsImportUIDNumber);
                        tool.AddControl(this.CardNumMask);
                        tool.AddControl(this.CardNumPattern);
                        tool.AddControl(CardCheckdigit);
                        tool.AddControl(CheckDigitModeID);
                        tool.AddControl(CardNumberToUID);

                        tool.AddControl(this.CardNumMaskImport);
                        tool.AddControl(this.CardNumPatternImport);
                        tool.AddControl(IsConsecutiveUID);
                        tool.AddControl(UIDCheckDigit);
                        tool.AddControl(CheckDigitModeID2);
                        tool.AddControl(UIDToCardNumber);
                        tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);
                    }
                    btnNew.OnClientClick = Window1.GetShowReference("CouponGrade/Add.aspx?RuleType=2&PageFlag=1", "新增");
                    btnDelete.OnClientClick = this.Grid_CanHoldCouponGradeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                    btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                    btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                    btnNew1.OnClientClick = Window1.GetShowReference("CouponGrade/Add.aspx?RuleType=1&PageFlag=1", "新增");
                    btnDelete1.OnClientClick = this.Grid_GetCouponGradeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                    btnDelete1.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                    btnDelete1.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                    BindingDataGrid_CanHoldCouponGradeList();
                    BindingDataGrid_GetCouponGradeList();

                    CardGradeUpdMethod_OnSelectedIndexChanged(null, null);

                    this.MemberClauseDesc1.Text = RemainVal(controller.ViewModel.MemberClause.MemberClauseDesc1);
                    this.MemberClauseDesc2.Text = RemainVal(controller.ViewModel.MemberClause.MemberClauseDesc2);
                    this.MemberClauseDesc3.Text = RemainVal(controller.ViewModel.MemberClause.MemberClauseDesc3);
                }

            }
        }

        private void SetAmountControll(FineUI.TextBox con,decimal? val)
        {
            if (val.HasValue)
            {
                con.Text = String.Format("{0:F}", val.Value);
            }
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            if (!ValidateForm())
            {
                return;
            }

            if (!ValidataUseRule())
            {
                return;
            }

            int page = 0;
            int.TryParse(Request.Params["page"], out page);
            Edge.SVA.Model.CardGrade item = this.GetUpdateObject();
            if (Edge.Web.Tools.DALTool.isHasCardGradeCode(item.CardGradeCode.Trim(), item.CardGradeID))
            {
                ShowWarning(Resources.MessageTips.ExistCardGradeCode);
                return;
            }

            if (Edge.Web.Tools.DALTool.isHasCardGradeRank(this.CardGradeRank.Text.Trim(), item.CardTypeID, item.CardGradeID))
            {
                ShowWarning(Resources.MessageTips.ExistCardGradeRank);
                return;
            }


            if (item.IsImportUIDNumber.HasValue && item.IsImportUIDNumber.Value == 1)
            {
                if (this.CheckDigitModeID2.Hidden == false)
                {
                    item.CheckDigitModeID = Edge.Web.Tools.ConvertTool.ConverType<int>(this.CheckDigitModeID2.SelectedValue);
                }
            }

            if (!ValidObject(item))
            {
                return;
            }

            if (!string.IsNullOrEmpty(this.CardGradePicFile.ShortFileName) && this.FormLoad.Hidden == false)
            {   
                //校验文件类型
                if (!ValidateImg(this.CardGradePicFile.FileName))
                {
                    return;
                }
                item.CardGradePicFile = this.CardGradePicFile.SaveToServer("Brand");
            }
            else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                //校验文件类型
                if (!ValidateImg(this.uploadFilePath.Text))
                {
                    return;
                }
                item.CardGradePicFile = this.uploadFilePath.Text;
            }
            if (!string.IsNullOrEmpty(this.CardGradeLayoutFile.ShortFileName) && this.FormLoad1.Hidden == false)
            {
                if (!ValidateFile(this.CardGradeLayoutFile.FileName))
                {
                    return;
                }
                item.CardGradeLayoutFile = this.CardGradeLayoutFile.SaveToServer("Brand");
            }
            else if (this.FormReLoad1.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath1.Text))
            {
                if (!ValidateFile(this.uploadFilePath1.Text))
                {
                    return;
                }
                item.CardGradeLayoutFile = this.uploadFilePath1.Text;
            }
            if (!string.IsNullOrEmpty(this.CardGradeStatementFile.ShortFileName) && this.FormLoad2.Hidden == false)
            {
                if (!ValidateFile(this.CardGradeStatementFile.FileName))
                {
                    return;
                }
                item.CardGradeStatementFile = this.CardGradeStatementFile.SaveToServer("Brand");
            }
            else if (this.FormReLoad2.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath2.Text))
            {
                if (!ValidateFile(this.uploadFilePath2.Text))
                {
                    return;
                }
                item.CardGradeStatementFile = this.uploadFilePath2.Text;
            }


            //Html处理
            //item.CardGradeNotes = Edge.Common.Utils.ToHtml(this.CardGradeNotes.Text);
            HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
            controller.ViewModel.MemberClause.MemberClauseDesc1 = clearTool.ClearSource(this.MemberClauseDesc1.Text);
            controller.ViewModel.MemberClause.MemberClauseDesc2 = clearTool.ClearSource(this.MemberClauseDesc2.Text);
            controller.ViewModel.MemberClause.MemberClauseDesc3 = clearTool.ClearSource(this.MemberClauseDesc3.Text);

            //将获取到的BrandID传入MemberClause表
            DataSet ds = DBUtility.DbHelperSQL.Query("select BrandID from CardType where CardTypeID = " + Convert.ToInt32(this.CardTypeID.SelectedValue));
            int brandid = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            controller.ViewModel.MemberClause.BrandID = brandid;


            if (Tools.DALTool.Update<Edge.SVA.BLL.CardGrade>(item))
            {
                if (this.FormReLoad.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath.Text);
                }

                //没有选择文件时要删除加载时所赋的值
                if (this.FormReLoad1.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath1.Text);
                }

                //没有选择文件时要删除加载时所赋的值
                if (this.FormReLoad2.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath2.Text);
                }

                ExecResult er= controller.Submit();
                if (er.Success)
                {
                    CardGradeRepostory.Singleton.Refresh();
                    CloseAndPostBack();
                }
                else
                {
                    logger.WriteErrorLog(" cardgrade update ", " cardgrade update ", er.Ex);
                    ShowError(Resources.MessageTips.UnKnownSystemError);
                }
            }
            else
            {
                ShowAddFailed();
            }
        }

        private bool ValidObject(SVA.Model.CardGrade item)
        {
            if (item == null) return false;

            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardGradeDiscCeiling = string.IsNullOrEmpty(this.CardGradeDiscCeiling.Text) ? 0 : decimal.Parse(this.CardGradeDiscCeiling.Text.Trim()) / 100;
            item.CardGradeCode = item.CardGradeCode.ToUpper();
            item.CardNumMask = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.Trim() : item.CardNumMask;
            item.CardNumPattern = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.Trim() : item.CardNumPattern;
            item.CardNumMask = item.CardNumMask.ToUpper();

            try
            {
                if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    //-------------------------------导入-------------------------------
                    item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
                    item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
                    item.CardCheckdigit = null;
                    item.CardNumberToUID = null;

                    if (!this.CardNumMaskImport.Hidden || !this.CardNumPatternImport.Hidden)
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMaskImport, this.CardNumPatternImport))
                        {
                            return false;
                        }
                    }

                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        if (this.CardNumMaskImport.Enabled)
                        {
                            if (!Tools.DALTool.CheckNumberMask(this.CardNumMaskImport.Text.Trim(), this.CardNumPatternImport.Text.Trim(), 1, item.CardTypeID))
                            {
                                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                                return false;
                            }
                        }
                    }

                    if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
                else
                {
                    //-------------------------------手动-------------------------------
                    item.IsConsecutiveUID = null;
                    item.UIDCheckDigit = null;
                    item.UIDToCardNumber = null;

                    if (!this.CardNumMask.Hidden || !this.CardNumPattern.Hidden)
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask, this.CardNumPattern))
                        {
                            return false;
                        }
                    }

                    if (this.CardNumMask.Enabled)
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 1, item.CardGradeID))
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                            return false;
                        }
                    }

                    if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
                return false;
            }

            return true;
        }
        #region  many Events
        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.CardType item = new Edge.SVA.BLL.CardType().GetModel(Tools.ConvertTool.ToInt(this.CardTypeID.SelectedValue));
            if (item != null)
            {
                this.IsImportUIDNumber.SelectedValue = item.IsImportUIDNumber.GetValueOrDefault().ToString();
                this.IsImportUIDNumber.Enabled = false;

                if (this.IsImportUIDNumber.SelectedValue == "1")//导入
                {
                    this.IsConsecutiveUID.SelectedValue = item.IsConsecutiveUID.GetValueOrDefault().ToString();
                    this.UIDCheckDigit.SelectedValue = item.UIDCheckDigit.GetValueOrDefault().ToString();
                    this.UIDToCardNumber.SelectedValue = item.UIDToCardNumber.GetValueOrDefault().ToString();
                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        this.CardNumMaskImport.Text = item.CardNumMask;
                        this.CardNumPatternImport.Text = item.CardNumPattern;
                    }

                    ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
                    tool.AddControl(this.CardNumMaskImport);
                    tool.AddControl(this.CardNumPatternImport);
                    tool.AddControl(this.IsConsecutiveUID);
                    tool.AddControl(this.UIDCheckDigit);
                    tool.AddControl(this.CheckDigitModeID2);
                    tool.AddControl(this.UIDToCardNumber);
                    tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        if (string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) && string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                        {
                            tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
                        }
                    }
                }
                else//手动
                {
                    this.CardNumMask.Text = item.CardNumMask;
                    this.CardNumPattern.Text = item.CardNumPattern;
                    this.CardCheckdigit.SelectedValue = item.CardCheckdigit.GetValueOrDefault().ToString();
                    this.CheckDigitModeID.SelectedValue = item.CheckDigitModeID.GetValueOrDefault().ToString();
                    this.CardNumberToUID.SelectedValue = item.CardNumberToUID.GetValueOrDefault().ToString();

                    ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
                    tool.AddControl(this.CardNumMask);
                    tool.AddControl(this.CardNumPattern);
                    tool.AddControl(this.CardCheckdigit);
                    tool.AddControl(this.CheckDigitModeID);
                    tool.AddControl(this.CardNumberToUID);
                    if ((string.IsNullOrEmpty(this.CardNumMask.Text.Trim())) && (string.IsNullOrEmpty(this.CardNumPattern.Text.Trim())))
                    {
                        tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
                    }
                    else
                    {
                        tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);
                    }
                }
                checkIsImportCardNumber(false);
            }
        }

        protected void IsImportUIDNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkIsImportCardNumber(true);
        }

        protected void CardCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
            CardNumberToUID.SelectedIndex = 0;
        }

        protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            UIDToCardNumber.SelectedIndex = 0;
        }
        #endregion

        protected override SVA.Model.CardGrade GetPageObject(SVA.Model.CardGrade obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.TabStrip1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CardPointTransfer_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckCardPointTransfer();
        }


        protected void CardAmountTransfer_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckCardAmountTransfer();
        }

        

        protected void CardGradeCode_TextChanged(object sender, EventArgs e)
        {
            //ConvertTextboxToUpperText(sender);
        }


        #region CardExtensionRule操作
        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in rptExtensionRuleList.SelectedRowIndexArray)
            {
                sb.Append(rptExtensionRuleList.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("CardExtensionRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString());
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("CardExtensionRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));
        }

        private void CardExtensionRuleRptBind(string strWhere)
        {

            Edge.SVA.BLL.CardExtensionRule bll = new Edge.SVA.BLL.CardExtensionRule();

            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            if (ds.Tables[0].Rows.Count > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }
            Edge.Web.Tools.DataTool.AddStatus(ds, "StatusName", "Status");
            this.rptExtensionRuleList.RecordCount = ds.Tables[0].Rows.Count;
            this.rptExtensionRuleList.DataSource = Tools.ConvertTool.GetPagedTable(ds.Tables[0], this.rptExtensionRuleList.PageIndex + 1, this.rptExtensionRuleList.PageSize); //ds.Tables[0].DefaultView;
            this.rptExtensionRuleList.DataBind();
        }
        #endregion


        #region PointRule 操作
        protected void lbtnPointRuleDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in rptEarnAmountPointList.SelectedRowIndexArray)
            {
                sb.Append(rptEarnAmountPointList.DataKeys[row][1].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString());
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));
        }

        private void CardPointRuleRptBind(string strWhere)
        {

            Edge.SVA.BLL.PointRule bll = new Edge.SVA.BLL.PointRule();


            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);
            ControlTool.AddColumnValue(ds, "MemberDateType", ConstInfosRepostory.InfoType.MemberRange);
            ControlTool.AddColumnValue(ds, "PointRuleType", ConstInfosRepostory.InfoType.TransType);
            ControlTool.AddColumnValue(ds, "PointRuleOper", ConstInfosRepostory.InfoType.ComputTypePoint);
            ControlTool.AddColumnValue(ds, "Status", ConstInfosRepostory.InfoType.Status);

            if (ds.Tables[0].Rows.Count > 0)
            {
                this.lbtnPointRuleDel.Enabled = true;
            }
            else
            {
                this.lbtnPointRuleDel.Enabled = false;
            }
            this.rptEarnAmountPointList.RecordCount = ds.Tables[0].Rows.Count;
            this.rptEarnAmountPointList.DataSource = Tools.ConvertTool.GetPagedTable(ds.Tables[0], this.rptEarnAmountPointList.PageIndex + 1, this.rptEarnAmountPointList.PageSize); //ds.Tables[0].DefaultView;
            this.rptEarnAmountPointList.DataBind();
        }
        #endregion

        #region MemberRewards 
        protected void btnMemberRewardDelete_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in this.gridMemberReward.SelectedRowIndexArray)
            {
                sb.Append(gridMemberReward.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //FineUI.PageContext.Redirect("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString());
            ExecuteJS(HiddenWindowFormSpecial.GetShowReference("MemberReward/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + sb.ToString()));
        }
        private void CardSVAMemberRewardDataBind(string strWhere)
        {

            Edge.SVA.BLL.SVARewardRules bll = new Edge.SVA.BLL.SVARewardRules();

            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);
            Edge.Web.Tools.DataTool.AddCouponTypeNameByID(ds, "RewardCouponTypeIDName", "RewardCouponTypeID");
            Edge.Web.Tools.DataTool.AddSVARewardTypeName(ds, "SVARewardTypeName", "SVARewardType");
            Edge.Web.Tools.DataTool.AddStatus(ds, "StatusName", "Status");

            if (ds.Tables[0].Rows.Count > 0)
            {
                this.btnMemberRewardDelete.Enabled = true;
            }
            else
            {
                this.btnMemberRewardDelete.Enabled = false;
            }
            this.gridMemberReward.RecordCount = ds.Tables[0].Rows.Count;
            this.gridMemberReward.DataSource = Tools.ConvertTool.GetPagedTable(ds.Tables[0], this.gridMemberReward.PageIndex + 1, this.gridMemberReward.PageSize); //ds.Tables[0].DefaultView;
            this.gridMemberReward.DataBind();
        }
        #endregion

        private void SetControl(FineUI.Button add, FineUI.Button delete, FineUI.Grid grid, string addpath)
        {
            add.OnClientClick = Window2.GetShowReference(addpath, "新增");

            delete.OnClientClick = grid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
            delete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
            delete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
        }

        protected void CardNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (CardCheckdigit.SelectedValue == "0")
            //{
            //    if (this.CardNumberToUID.SelectedValue == "3")
            //    {
            //        this.CheckDigitModeID.Hidden = false;
            //    }
            //    else
            //    {
            //        this.CheckDigitModeID.Hidden = true;
            //    }
            //}
            Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
        }
        protected void UIDToCardNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (this.UIDCheckDigit.SelectedValue == "0")
            //{
            //    if (this.UIDToCardNumber.SelectedValue == "3")
            //    {
            //        this.CheckDigitModeID2.Hidden = false;
            //    }
            //    else
            //    {
            //        this.CheckDigitModeID2.Hidden = true;
            //    }
            //}
            Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            this.CardNumMaskImport.Text = "";
            this.CardNumPatternImport.Text = "";
        }
        protected void CardNumMask_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMask.Text = this.CardNumMask.Text.ToUpper();
        }

        protected void CardNumPattern_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMask.Text) && string.IsNullOrEmpty(this.CardNumPattern.Text)) return;


            if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask.Text, this.CardNumPattern.Text, false))
            {
                CardNumPattern.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CardNumPattern.Text));
            }
        }

        protected void CardNumMaskImport_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMaskImport.Text = this.CardNumMaskImport.Text.ToUpper();
        }

        protected void CardNumPatternImport_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMaskImport.Text) && string.IsNullOrEmpty(this.CardNumPatternImport.Text)) return;

            if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMaskImport.Text, this.CardNumPatternImport.Text, false))
            {
                CardNumPatternImport.MarkInvalid(String.Format("'{0}'" + Resources.MessageTips.InvalidInput, CardNumPatternImport.Text));
            }
        }

        protected void CardGradeUpdMethod_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.CardGradeUpdThreshold.Enabled = false;
            this.CardGradeUpdHitPLU.Enabled = false;
            switch (this.CardGradeUpdMethod.SelectedValue)
            {
                case "0":
                    break;
                case "1":
                case "2":
                case "3":
                    this.CardGradeUpdThreshold.Enabled = true;
                    break;
                default:
                    this.CardGradeUpdHitPLU.Enabled = true;
                    break;
            }
        }

        private void checkIsImportCardNumber(bool clearText)
        {
            if (IsImportUIDNumber.SelectedValue == "1")
            {
                Add.checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);

                this.ManualRoles.Hidden = true;
                this.ImportRoles.Hidden = false;
            }
            else
            {
                Add.checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
                this.ManualRoles.Hidden = false;
                this.ImportRoles.Hidden = true;
            }

            if (clearText)
            {
                this.CardNumPattern.Text = "";
                this.CardNumMask.Text = "";
                this.CardNumPatternImport.Text = "";
                this.CardNumMaskImport.Text = "";
            }
        }


        #region 根据dropdownlist判断
        private void CheckCardPointTransfer()
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(MinPointPreTransfer);
            tool.AddControl(MaxPointPreTransfer);
            tool.AddControl(DayMaxPointTransfer);
            if (this.CardPointTransfer.SelectedIndex == 0)
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxPointPreTransfer.Text = "999999";
                this.DayMaxPointTransfer.Text = "999999";
            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
        }
        private void CheckCardAmountTransfer()
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(MinAmountPreTransfer);
            tool.AddControl(MaxAmountPreTransfer);
            tool.AddControl(DayMaxAmountTransfer);
            if (this.CardAmountTransfer.SelectedIndex == 0)
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxAmountPreTransfer.Text = "999999.00";
                this.DayMaxAmountTransfer.Text = "999999.00";
            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
        }
        protected void IsAllowStoreValue_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            //tool.AddControl(CardGradeMaxAmount);
            tool.AddControl(MinAmountPreAdd);
            tool.AddControl(MaxAmountPreAdd);
            if (this.IsAllowStoreValue.SelectedValue == "0")
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxAmountPreAdd.Text = "999999.00";
            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
        }
        protected void IsAllowConsumptionPoint_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(CardGradeMaxPoint);
            tool.AddControl(MinPointPreAdd);
            tool.AddControl(MaxPointPreAdd);
            if (this.IsAllowConsumptionPoint.SelectedValue == "0")
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxPointPreAdd.Text = "999999";
            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
        }
        #endregion

        #region CardGradeHoldCouponType
        protected void HoldCouponCount_OnTextChanged(object sender, EventArgs e)
        {
            controller.ViewModel.MainTable.HoldCouponCount = StringHelper.ConvertToInt(this.HoldCouponCount.Text);
        }
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CanHoldCouponGradeList.SelectedRowIndexArray;
            int count = this.Grid_CanHoldCouponGradeList.PageIndex * this.Grid_CanHoldCouponGradeList.PageSize;
            List<CardGradeHoldCouponRuleViewModel> list = new List<CardGradeHoldCouponRuleViewModel>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CanHoldCouponGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCanHoldCouponGradeListItem(list[i]);
            }
            BindingDataGrid_CanHoldCouponGradeList();
        }
        private void BindingDataGrid_CanHoldCouponGradeList()
        {
            this.Grid_CanHoldCouponGradeList.RecordCount = controller.ViewModel.CanHoldCouponGradeList.Count;
            this.Grid_CanHoldCouponGradeList.DataSource = controller.ViewModel.CanHoldCouponGradeList;
            this.Grid_CanHoldCouponGradeList.DataBind();
        }
        protected void Grid_CanHoldCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_CanHoldCouponGradeList.PageIndex = e.NewPageIndex;

        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_CanHoldCouponGradeList();
            BindingDataGrid_GetCouponGradeList();

            //删除图片
            DeleteFile(this.PicturePath.Text);
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_GetCouponGradeList.SelectedRowIndexArray;
            int count = this.Grid_GetCouponGradeList.PageIndex * this.Grid_GetCouponGradeList.PageSize;
            List<CardGradeHoldCouponRuleViewModel> list = new List<CardGradeHoldCouponRuleViewModel>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.GetCouponGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveGetCouponGradeListItem(list[i]);
            }
            BindingDataGrid_GetCouponGradeList();
        }
        private void BindingDataGrid_GetCouponGradeList()
        {
            this.Grid_GetCouponGradeList.RecordCount = controller.ViewModel.GetCouponGradeList.Count;
            this.Grid_GetCouponGradeList.DataSource = controller.ViewModel.GetCouponGradeList;
            this.Grid_GetCouponGradeList.DataBind();
        }
        protected void Grid_GetCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_GetCouponGradeList.PageIndex = e.NewPageIndex;
        }
        #endregion

        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.CardGradePicFile.ShortFileName))
            {
                this.PicturePath.Text = this.CardGradePicFile.SaveToServer("Brand");
                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("../../../../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }
        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }
        #endregion

        #region 文件处理
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath1.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CardGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CardGrade download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        protected void btnReUpLoad1_Click(object sender, EventArgs e)
        {
            this.FormLoad1.Hidden = false;
            this.FormReLoad1.Hidden = true;
        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad1.Hidden = true;
                this.FormReLoad1.Hidden = false;
            }
        }

        protected void btnExport1_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath2.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("CardGrade download ", " filename: " + fileName);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                //Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Manual", fn, start, records, ex.Message);
                //JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
                Tools.Logger.Instance.WriteErrorLog("CardGrade download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }
        protected void btnReUpLoad2_Click(object sender, EventArgs e)
        {
            this.FormLoad2.Hidden = false;
            this.FormReLoad2.Hidden = true;
        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath2.Text))
            {
                this.FormLoad2.Hidden = true;
                this.FormReLoad2.Hidden = false;
            }
        }
        #endregion

        #region IForm Members
        public bool ValidateForm()
        {
            List<FineUI.TextBox> list = new List<FineUI.TextBox>();
            list.Add(CardTypeInitAmount);
            list.Add(CardGradeMaxAmount);
            list.Add(MinAmountPreAdd);
            list.Add(MaxAmountPreAdd);
            list.Add(MinConsumeAmount);
            list.Add(MinBalanceAmount);
            list.Add(CardAmountToPointRate);
            list.Add(MinAmountPreTransfer);
            list.Add(MaxAmountPreTransfer);
            list.Add(DayMaxAmountTransfer);
            if (!CheckAndConvertTextboxListToDecimal(list))
            {
                return false;
            }

            if (string.IsNullOrEmpty(this.MemberClauseDesc1.Text.Trim()) || this.MemberClauseDesc1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.Term1CannotBeEmpty);
                return false;
            }
            return true;
        }
        #endregion

        #region Use Rule逻辑处理
        protected bool ValidataUseRule()
        {
            double minAmountPreAdd = string.IsNullOrEmpty(this.MinAmountPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MinAmountPreAdd.Text);
            double maxAmountPreAdd = string.IsNullOrEmpty(this.MaxAmountPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MaxAmountPreAdd.Text);
            double minPointPreAdd = string.IsNullOrEmpty(this.MinPointPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MinPointPreAdd.Text);
            double maxPointPreAdd = string.IsNullOrEmpty(this.MaxPointPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MaxPointPreAdd.Text);
            double minPointPreTransfer = string.IsNullOrEmpty(this.MinPointPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MinPointPreTransfer.Text);
            double maxPointPreTransfer = string.IsNullOrEmpty(this.MaxPointPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MaxPointPreTransfer.Text);
            double minAmountPreTransfer = string.IsNullOrEmpty(this.MinAmountPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MinAmountPreTransfer.Text);
            double maxAmountPreTransfer = string.IsNullOrEmpty(this.MaxAmountPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MaxAmountPreTransfer.Text);
            if (this.IsAllowStoreValue.SelectedValue != "0")
            {
                if (minAmountPreAdd >= maxAmountPreAdd)
                {
                    ShowWarning(Resources.MessageTips.MinAmountTooLarge);
                    return false;
                }
            }
            if (this.IsAllowConsumptionPoint.SelectedValue != "0")
            {
                if (minPointPreAdd >= maxPointPreAdd)
                {
                    ShowWarning(Resources.MessageTips.MinPointTooLarge);
                    return false;
                }
            }
            if (this.CardPointTransfer.SelectedValue != "0")
            {
                if (minPointPreTransfer >= maxPointPreTransfer)
                {
                    ShowWarning(Resources.MessageTips.MinTransferPointTooLarge);
                    return false;
                }
            }
            if (this.CardAmountTransfer.SelectedValue != "0")
            {
                if (minAmountPreTransfer >= maxAmountPreTransfer)
                {
                    ShowWarning(Resources.MessageTips.MinTransferCashTooLarge);
                    return false;
                }
            }
            return true;
        }
        #endregion

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        //校验模板文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.').ToLower();
                if (!webset.CardGradeFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CardGradeFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        protected void rptExtensionRuleList_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            rptExtensionRuleList.PageIndex = e.NewPageIndex;

            CardExtensionRuleRptBind("CardGradeID =" + Request.Params["id"]);
        }

        protected void rptEarnAmountPointList_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            rptEarnAmountPointList.PageIndex = e.NewPageIndex;

            CardPointRuleRptBind("CardGradeID =" + Request.Params["id"]);
        }

        protected void gridMemberReward_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            gridMemberReward.PageIndex = e.NewPageIndex;

            CardSVAMemberRewardDataBind("CardGradeID =" + Request.Params["id"]);
        }
    }





    //#region CardExtensionRule操作
    //protected void lbtnDel_Click(object sender, EventArgs e)
    //{
    //    string ids = "";
    //    for (int i = 0; i < rptExtensionRuleList.Items.Count; i++)
    //    {
    //        int id = Convert.ToInt32(((HiddenField)rptExtensionRuleList.Items[i].FindControl("lb_id")).Value);
    //        CheckBox cb = (CheckBox)rptExtensionRuleList.Items[i].FindControl("cb_id");
    //        if (cb.Checked)
    //        {
    //            ids += string.Format("{0};",id.ToString());
    //        }
    //    }
    //    Response.Redirect("CardExtensionRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
    //}

    //protected void lbtnAdd_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("CardExtensionRule/Add.aspx?CardGradeID=" + Request.Params["id"]);
    //}

    //private void CardExtensionRuleRptBind(string strWhere)
    //{

    //    Edge.SVA.BLL.CardExtensionRule bll = new Edge.SVA.BLL.CardExtensionRule();

    //    DataSet ds = new DataSet();
    //    ds = bll.GetList(strWhere);

    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        this.lbtnDel.Enabled = true;
    //    }
    //    else
    //    {
    //        this.lbtnDel.Enabled = false;
    //    }

    //    this.rptExtensionRuleList.DataSource = ds.Tables[0].DefaultView;
    //    this.rptExtensionRuleList.DataBind();
    //}
    //#endregion


    //#region PointRule 操作
    //protected void lbtnPointRuleDel_Click(object sender, EventArgs e)
    //{
    //    string ids = "";
    //    for (int i = 0; i < rptPointRuleList.Items.Count; i++)
    //    {
    //        int id = Convert.ToInt32(((HiddenField)rptPointRuleList.Items[i].FindControl("lb_id")).Value);
    //        CheckBox cb = (CheckBox)rptPointRuleList.Items[i].FindControl("cb_id");
    //        if (cb.Checked)
    //        {
    //            ids += string.Format("{0};", id.ToString());
    //        }
    //    }
    //    Response.Redirect("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);

    //}

    //protected void lbtnPointRuleAdd_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("PointRule/Add.aspx?CardGradeID=" + Request.Params["id"]);
    //}

    //private void CardPointRuleRptBind(string strWhere)
    //{

    //    Edge.SVA.BLL.PointRule bll = new Edge.SVA.BLL.PointRule();


    //    DataSet ds = new DataSet();
    //    ds = bll.GetList(strWhere);

    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        this.lbtnPointRuleDel.Enabled = true;
    //    }
    //    else
    //    {
    //        this.lbtnPointRuleDel.Enabled = false;
    //    }
    //    this.rptPointRuleList.DataSource = ds.Tables[0].DefaultView;
    //    this.rptPointRuleList.DataBind();
    //}
    //#endregion
}
