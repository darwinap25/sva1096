﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule.PointRule_Store.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                     <ext:Button ID="btnSearch" runat="server" Text="搜 索" Icon="Find" OnClick="btnSearch_Click" />
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:DropDownList ID="Brand" runat="server" Label="店铺所属品牌：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="StoreType" runat="server" Label="店铺种类：" Resizable="true">
            </ext:DropDownList>
            <ext:DropDownList ID="StoreGroup" runat="server" Label="店铺分组：" Resizable="true">
            </ext:DropDownList>
            <ext:TextBox ID="StoreName" runat="server" Label="店铺名称：">
            </ext:TextBox>
            <ext:Panel ID="panel1" runat="server" Title="可选店铺" AutoScroll="true">
                <Toolbars>
                    <ext:Toolbar ID="toolbar2" runat="server">
                        <Items>
                            <ext:CheckBox ID="cbAll" runat="server" Text="全选" AutoPostBack="true" OnCheckedChanged="cbAll_CheckChanged">
                            </ext:CheckBox>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:CheckBoxList ID="ckbStoreID" runat="server" ColumnNumber="3" />
                </Items>
            </ext:Panel>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
