﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PointRule,Edge.SVA.Model.PointRule>
    {
        PointRuleController controller;
        public const string tab = "2";
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindKeyValueListNoEmpty(this.MemberDateType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
                ControlTool.BindKeyValueList(this.PointRuleType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.TransType),true);
                ControlTool.BindKeyValueList(this.PointRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ComputTypePoint),true);
            }
            controller = SVASessionInfo.PointRuleController;

        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {

                string cardGradeId = Request.Params["CardGradeID"];
                this.CardGradeID.SelectedValue = cardGradeId;
                this.CardTypeID.SelectedValue = new Edge.SVA.BLL.CardGrade().GetModel(int.Parse(cardGradeId)).CardTypeID.ToString();
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            //Edge.SVA.Model.PointRule item = this.GetAddObject();

            //if (item != null)
            //{
            //    item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = System.DateTime.Now;
            //}

            //if (IsExistsRuleNumber(this.PointRuleSeqNo.Text))
            //{
            //    ShowWarning(Edge.Messages.Manager.MessagesTool.instance.GetMessage("90487"));
            //    return;
            //}

            //if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.PointRule>(item) > 0)
            //{
            //    SVASessionInfo.Tabs = Add.tab;
            //    CloseAndRefresh();
            //}
            //else
            //{
            //    ShowAddFailed();
            //}
            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = System.DateTime.Now;
            }
            //if (IsExistsRuleNumber(this.PointRuleSeqNo.Text))
            //{
            //    ShowWarning(Edge.Messages.Manager.MessagesTool.instance.GetMessage("90487"));
            //    return;
            //}
            ExecResult er = controller.Add();
            if (er.Success)
            {
                SVASessionInfo.Tabs = Add.tab;
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + Request.Params["CardGradeID"] + "&tabs="+Add.tab);
        }

        //protected bool IsExistsRuleNumber(string strPointRuleSeqNo)
        //{
        //    Edge.SVA.BLL.PointRule bll = new SVA.BLL.PointRule();
        //    return bll.GetModelList("PointRuleSeqNo='" + strPointRuleSeqNo + "'").Count > 0 ? true : false;

        //}

        #region 货品
        protected void btnViewSearch_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference(string.Format("PointRule_HitItem/Add.aspx?PointRuleCode={0}", this.PointRuleCode.Text), "新增"));
        }

        //protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        //{
        //    base.WindowEdit_Close(sender, e);
        //    BindResultList(controller.ViewModel.SubTable);
        //}

        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = viewDT;
                this.AddResultListGrid.DataBind();
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }

            this.btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;
        }

        protected void AddResultListGrid_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            AddResultListGrid.PageIndex = e.NewPageIndex;

            BindResultList(controller.ViewModel.SubTable);
        }

        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.SubTable != null)
            {
                DataTable addDT = controller.ViewModel.SubTable;

                foreach (int row in AddResultListGrid.SelectedRowIndexArray)
                {
                    string couponNumber = AddResultListGrid.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["PointRuleCode"].ToString().Trim() == couponNumber)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.SubTable = addDT;
                BindResultList(controller.ViewModel.SubTable);

            }
        }

        protected void btnClearAllResultItem_Click(object sender, EventArgs e)
        {
            ClearGird(this.AddResultListGrid);
            controller.ViewModel.SubTable = null;
        }
        #endregion

        #region 时间设置
        protected void btnAddDay_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("DAYFLAG/List.aspx"));
        }

        protected void btnAddWeek_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("WEEKFLAG/List.aspx"));
        }

        protected void btnAddMonth_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("MONTHFLAG/List.aspx"));
        }

        protected void WindowEdit1_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            this.DayFlagCode.Text = controller.ViewModel.DayCode;
            this.MonthFlagCode.Text = controller.ViewModel.MonthCode;
            this.WeekFlagCode.Text = controller.ViewModel.WeekCode;
        }
        #endregion

        #region
        protected void btnAddStore_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window2.GetShowReference(string.Format("PointRule_Store/Add.aspx?PointRuleCode={0}", this.PointRuleCode.Text), "新增"));
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindStoreList(controller.ViewModel.SubTable);
        }

        private void BindStoreList(DataTable dt)
        {
            if (dt != null)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.PageIndex = 0;
                this.Grid1.RecordCount = 0;
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }

            this.btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = Grid1.RecordCount > 0 ? true : false;
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            BindStoreList(controller.ViewModel.SubTable);
        }

        protected void btnDeleteStore_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.SubTable != null)
            {
                DataTable addDT = controller.ViewModel.SubTable;

                foreach (int row in Grid1.SelectedRowIndexArray)
                {
                    string StoreID = Grid1.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["StoreID"].ToString().Trim() == StoreID)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.SubTable = addDT;
                BindStoreList(controller.ViewModel.SubTable);

            }
        }

        protected void btnClearStore_Click(object sender, EventArgs e)
        {
            ClearGird(this.Grid1);
            controller.ViewModel.SubTable = null;
        }
        #endregion

        protected void PointRuleCode_TextChanged(object sender, EventArgs e)
        {
            if (this.PointRuleCode.Text == string.Empty)
            {
                this.btnAddStore.Enabled = this.btnDeleteStore.Enabled = this.btnClearStore.Enabled = false;
            }
            else
            {
                this.btnAddStore.Enabled = this.btnDeleteStore.Enabled = this.btnClearStore.Enabled = true;
            }
        }
    }
}
