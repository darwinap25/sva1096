﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:DropDownList ID="RuleType" runat="server" Label="规则类型：" Enabled="false">
                <ext:ListItem Text="卡有效期延长规则" Value="0"></ext:ListItem>
                <ext:ListItem Text="金额有效期规则" Value="1"></ext:ListItem>
                <ext:ListItem Text="积分有效期规则" Value="2"></ext:ListItem>
            </ext:DropDownList>
            <ext:DropDownList ID="CardTypeID" runat="server" Enabled="false" Label="卡类型：">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeID" runat="server" Enabled="false" Label="卡等级：">
            </ext:DropDownList>
            <ext:Label ID="ExtensionRuleSeqNo" runat="server" Label="规则记录序号：">
            </ext:Label>
            <ext:Label ID="MaxLimit" runat="server" Label="最大增加数：">
            </ext:Label>
            <ext:Label ID="RuleAmount" runat="server" Label="增值金额：">
            </ext:Label>
            <ext:Label ID="Extension" runat="server" Label="延长日期：">
            </ext:Label>
            <ext:Label ID="ExtensionUnit" runat="server" Label="有效期延长单位：">
            </ext:Label>
            <ext:Label ID="ExtendType" runat="server" Label="有效期延长方式：">
            </ext:Label>
            <ext:Label ID="SpecifyExpiryDate" runat="server" Label="有效期延长方式：">
            </ext:Label>
            <ext:Label ID="StartDate" runat="server" Label="开始日期：">
            </ext:Label>
            <ext:Label ID="EndDate" runat="server" Label="结束日期：">
            </ext:Label>
            <ext:Label ID="StatusView" runat="server" Label="状态：" />
            <ext:RadioButtonList ID="Status" runat="server" Label="状态：">
                <ext:RadioItem Text="有效" Value="1" Selected="true" />
                <ext:RadioItem Text="无效" Value="0" />
            </ext:RadioButtonList>
            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
            </ext:Label>
            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
            </ext:Label>
            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
            </ext:Label>
            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
            </ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
