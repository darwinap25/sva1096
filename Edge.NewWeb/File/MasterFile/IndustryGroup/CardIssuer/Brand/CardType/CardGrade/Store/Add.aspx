﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Store.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>
    <style type="text/css">
        #ckbStoreID
        {
            border-collapse: collapse;
            line-height: 18px;
        }
        #ckbStoreID td
        {
            width: 300px;
            padding-left: 5px;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnAdd_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSearch" runat="server" Text="搜 索" OnClick="btnSearch_Click" Icon="Find">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="查询店铺">
                <Items>
                    <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                        Title="" EnableBackgroundColor="true" LabelAlign="Right">
                        <Items>
<%--                            <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px">
                                <ext:ListItem Value="1" Text="EAN13" />
                            </ext:DropDownList>--%>
                            <ext:DropDownList ID="BrandID" runat="server" Label="店铺所属品牌列表:" OnSelectedIndexChanged="BrandID_SelectedIndexChanged" Resizable="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="StoreTypeID" runat="server" Label="店铺种类:" OnSelectedIndexChanged="StoreTypeID_SelectedIndexChanged" Resizable="true">
                            </ext:DropDownList>
                            <ext:TextBox ID="StoreCode" runat="server" Label="店铺编号:" ToolTipTitle="店铺编号" ToolTip="Translate__Special_121_Start輸入Store的编号Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="StoreName" runat="server" Label="店铺名:" ToolTipTitle="店铺名" ToolTip="Translate__Special_121_Start輸入Store的名稱Translate__Special_121_End">
                            </ext:TextBox>
                            <ext:TextBox ID="txtCount" runat="server" Label="查询数量:" ToolTipTitle="查询数量" ToolTip="输入查找的数量，只允許輸入數字">
                            </ext:TextBox>
                        </Items>
                    </ext:SimpleForm>
                </Items>
            </ext:GroupPanel>
            <ext:Panel ID="panel1" runat="server" Title="可选店铺" AutoScroll="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server">
                        <Items>
                            <ext:CheckBox ID="cbAll" runat="server" Text="全选" AutoPostBack="true" OnCheckedChanged="cbAll_CheckChanged">
                            </ext:CheckBox>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:CheckBoxList ID="ckbStoreID" runat="server" ColumnNumber="3" />
                </Items>
            </ext:Panel>
            <%-- <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
            </ext:Label>--%>
        </Items>
    </ext:SimpleForm>
    </form>
</body>
</html>
