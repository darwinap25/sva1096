﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PointRule,Edge.SVA.Model.PointRule>
    {
        PointRuleController controller;
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                logger.WriteOperationLog(this.PageName, "Show");
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindKeyValueListNoEmpty(this.MemberDateType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
                ControlTool.BindKeyValueList(this.PointRuleType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.TransType), true);
                ControlTool.BindKeyValueList(this.PointRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ComputTypePoint), true);
                
            }
            controller = SVASessionInfo.PointRuleController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());
                this.CardTypeID.Visible = false;
                this.CardTypeID_Name.Text = this.CardTypeID.SelectedText;
                this.CardGradeID.Visible = false;
                this.CardGradeID_Name.Text = this.CardGradeID.SelectedText;
                this.MemberDateType.Visible = false;
                this.MemberDateType_Name.Text = this.MemberDateType.SelectedText;
                this.PointRuleType.Visible = false;
                this.PointRuleType_Name.Text = this.PointRuleType.SelectedText;
                this.PointRuleOper.Visible = false;
                this.PointRuleOper_Name.Text = this.PointRuleOper.SelectedText;
                this.Status.Visible = false;
                this.Status_Name.Text = this.Status.SelectedItem.Text;

                controller.LoadViewModel(Model.PointRuleCode);

                BindStoreList(controller.ViewModel.SubTable);
            }
        }

        #region 货品
        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = viewDT;
                this.AddResultListGrid.DataBind();
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }
        }

        protected void AddResultListGrid_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            AddResultListGrid.PageIndex = e.NewPageIndex;

            BindResultList(controller.ViewModel.SubTable);
        }
        #endregion

        #region
        private void BindStoreList(DataTable dt)
        {
            if (dt != null)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.PageIndex = 0;
                this.Grid1.RecordCount = 0;
                this.Grid1.DataSource = null;
                this.Grid1.DataBind();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            BindStoreList(controller.ViewModel.SubTable);
        }
        #endregion
    }
}
