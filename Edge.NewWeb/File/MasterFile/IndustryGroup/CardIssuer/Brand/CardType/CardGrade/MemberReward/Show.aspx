﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.MemberReward.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
             <ext:Label ID="SVARewardRulesCode" runat="Server" Label="规则编号："/>
            <ext:Label ID="SVARewardTypeView" runat="server" Label="奖励类型：">
            </ext:Label>
            <ext:Label ID="RewardPoint" runat="server" Label="奖励积分：" Text="0">
            </ext:Label>
            <ext:Label ID="RewardAmount" runat="server" Label="增值金额：" >
            </ext:Label>
            <ext:Label ID="RewardCouponTypeIDName" runat="server" Label="奖励优惠券类型：">
            </ext:Label>
            <ext:DropDownList ID="RewardCouponTypeID" runat="server" Label="奖励优惠券类型：" >
            </ext:DropDownList>
            <ext:Label ID="RewardCouponCount" runat="server" Label="优惠券数量：">
            </ext:Label>
            <ext:Label ID="StartDate" runat="server" Label="开始日期：" >
            </ext:Label>
            <ext:Label ID="EndDate" runat="server" Label="结束日期：" >
            </ext:Label>
            <ext:Label ID="StatusName" runat="Server" Label="状态："></ext:Label>
            <ext:DropDownList ID="SVARewardType" runat="server" Label="奖励类型：" Hidden="true">
            </ext:DropDownList>
            <ext:RadioButtonList ID="Status" runat="server"  Label="状态：" Width="200px">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
