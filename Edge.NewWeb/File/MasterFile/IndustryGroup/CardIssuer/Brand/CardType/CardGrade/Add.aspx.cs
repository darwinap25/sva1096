﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using System.Data;
using Edge.Utils.Tools;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Linq;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardGrade, Edge.SVA.Model.CardGrade>, IForm
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CardGradeAddController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

                RegisterCloseEvent(btnClose);

                CheckCardPointTransfer();
                CheckCardAmountTransfer();

                CardTypeID_SelectedIndexChanged(null, null);
                //checkIsImportCardNumber(false);    注意  当上面CardTypeID_SelectedIndexChanged 注释的时候，这个方法要恢复

                SVASessionInfo.CardGradeAddController = null;
                controller = SVASessionInfo.CardGradeAddController;
                btnNew.OnClientClick = Window1.GetShowReference("CouponGrade/Add.aspx?RuleType=2&PageFlag=0", "新增");
                btnDelete.OnClientClick = this.Grid_CanHoldCouponGradeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                btnNew1.OnClientClick = Window1.GetShowReference("CouponGrade/Add.aspx?RuleType=1&PageFlag=0", "新增");
                btnDelete1.OnClientClick = this.Grid_GetCouponGradeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete1.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete1.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                BindingDataGrid_CanHoldCouponGradeList();
                BindingDataGrid_GetCouponGradeList();
                CardGradeUpdMethod_OnSelectedIndexChanged(null, null);

                //新增字段CardGradeUpdCouponTypeID 
                ControlTool.BindCouponType(CardGradeUpdCouponTypeID);
            }
            controller = SVASessionInfo.CardGradeAddController;
            IsAllowStoreValue_OnSelectedIndexChanged(null, null);
            IsAllowConsumptionPoint_OnSelectedIndexChanged(null, null);
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (!ValidateForm())
            {
                return;
            }
            if (!ValidataUseRule()) return;

            if (Edge.Web.Tools.DALTool.isHasCardGradeCode(this.CardGradeCode.Text.Trim(), 0))
            {
                ShowWarning(Resources.MessageTips.ExistCardGradeCode);
                return;
            }
            if (Edge.Web.Tools.DALTool.isHasCardGradeRank(this.CardGradeRank.Text.Trim(), Tools.ConvertTool.ToInt(this.CardTypeID.SelectedValue), 0))
            {
                ShowWarning(Resources.MessageTips.ExistCardGradeRank);
                return;
            }

            Edge.SVA.Model.CardGrade item = this.GetAddObject();

            if (!ValidObject(item)) return;

            //校验文件类型
            if (!ValidateImg(this.CardGradePicFile.FileName))
            {
                return;
            }
            if (!ValidateFile(this.CardGradeLayoutFile.FileName))
            {
                return;
            }
            if (!ValidateFile(this.CardGradeStatementFile.FileName))
            {
                return;
            }

            //Html处理
            //item.CardGradeNotes = Edge.Common.Utils.ToHtml(this.CardGradeNotes.Text);

            item.CardGradePicFile = this.CardGradePicFile.SaveToServer("Images/CardGrade");
            item.CardGradeLayoutFile = this.CardGradeLayoutFile.SaveToServer("Images/CardGrade");
            item.CardGradeStatementFile = this.CardGradeStatementFile.SaveToServer("Images/CardGrade");
            
            HtmlTool.IClearTool clearTool = HtmlTool.Factory.CreateIClearTool();
            controller.ViewModel.MemberClause.MemberClauseDesc1 = clearTool.ClearSource(this.MemberClauseDesc1.Text);
            controller.ViewModel.MemberClause.MemberClauseDesc2 = clearTool.ClearSource(this.MemberClauseDesc2.Text);
            controller.ViewModel.MemberClause.MemberClauseDesc3 = clearTool.ClearSource(this.MemberClauseDesc3.Text);
            //将获取到的BrandID传入MemberClause表
            DataSet ds = DBUtility.DbHelperSQL.Query("select BrandID from CardType where CardTypeID = " + Convert.ToInt32(this.CardTypeID.SelectedValue));
            int brandid = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            controller.ViewModel.MemberClause.BrandID = brandid;

            List<KeyValue> errorList;
            if (controller.Validate(out errorList))
            {
                int cardGradeID = Tools.DALTool.Add<Edge.SVA.BLL.CardGrade>(item);
                if (cardGradeID > 0)
                {
                    controller.ViewModel.MainTable.CardGradeID = cardGradeID;
                    ExecResult er = controller.Submit();
                    if (er.Success)
                    {
                        CardGradeRepostory.Singleton.Refresh();
                        CloseAndRefresh();
                    }
                    else
                    {
                        logger.WriteErrorLog(" cardgrade add ", " cardgrade add ", er.Ex);
                        ShowError(Resources.MessageTips.UnKnownSystemError);
                    }
                }
                else
                {
                    ShowAddFailed();
                }
            }
            else
            {
                ShowAddFailed();
            }
        }

        private bool ValidObject(SVA.Model.CardGrade item)
        {
            if (item == null) return false;
            item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.CreatedOn = System.DateTime.Now;
            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardGradeCode = item.CardGradeCode.ToUpper();
            item.CardGradeDiscCeiling = string.IsNullOrEmpty(this.CardGradeDiscCeiling.Text) ? 0 : decimal.Parse(this.CardGradeDiscCeiling.Text.Trim()) / 100;

            item.CardNumMask = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.Trim() : item.CardNumMask;
            item.CardNumPattern = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.Trim() : item.CardNumPattern;
            item.CardNumMask = item.CardNumMask.ToUpper();


            try
            {
                if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    //-------------------------------导入-------------------------------
                    item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
                    item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
                    item.CardCheckdigit = null;
                    item.CardNumberToUID = null;

                    //if (!string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) || !string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                    //{
                    if (!this.CardNumMaskImport.Hidden && !this.CardNumPatternImport.Hidden)
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMaskImport, this.CardNumPatternImport))
                        {
                            return false;
                        }
                    }
                    //}
                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        if (this.CardNumMaskImport.Enabled)
                        {
                            if (!Tools.DALTool.CheckNumberMask(this.CardNumMaskImport.Text.Trim(), this.CardNumPatternImport.Text.Trim(), 1, 0))
                            {
                                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                                return false;
                            }
                        }
                    }

                    if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
                else
                {
                    //-------------------------------手动-------------------------------
                    item.IsConsecutiveUID = null;
                    item.UIDCheckDigit = null;
                    item.UIDToCardNumber = null;

                    if (!this.CardNumMask.Hidden && !this.CardNumPattern.Hidden)
                    {
                        if (!Tools.CheckTool.IsMatchNumMask(this.CardNumMask, this.CardNumPattern))
                        {
                            return false;
                        }
                    }

                    if (this.CardNumMask.Enabled)
                    {
                        if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 1, 0))
                        {
                            ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90393"));
                            return false;
                        }
                    }

                    if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
                return false;
            }

            return true;
        }

        #region  many Events
        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.CardType item = new Edge.SVA.BLL.CardType().GetModel(Tools.ConvertTool.ToInt(this.CardTypeID.SelectedValue));
            if (item != null)
            {
                this.IsImportUIDNumber.SelectedValue = item.IsImportUIDNumber.GetValueOrDefault().ToString();
                this.IsImportUIDNumber.Enabled = false;

                if (this.IsImportUIDNumber.SelectedValue == "1")//导入
                {
                    this.IsConsecutiveUID.SelectedValue = item.IsConsecutiveUID.GetValueOrDefault().ToString();
                    this.UIDCheckDigit.SelectedValue = item.UIDCheckDigit.GetValueOrDefault().ToString();
                    this.UIDToCardNumber.SelectedValue = item.UIDToCardNumber.GetValueOrDefault().ToString();
                    if (item.UIDCheckDigit.HasValue)
                    {
                        this.CheckDigitModeID2.SelectedValue = item.UIDCheckDigit.GetValueOrDefault().ToString();
                    }

                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        this.CardNumMaskImport.Text = item.CardNumMask;
                        this.CardNumPatternImport.Text = item.CardNumPattern;
                    }
                    ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
                    tool.AddControl(this.CardNumMaskImport);
                    tool.AddControl(this.CardNumPatternImport);
                    tool.AddControl(this.IsConsecutiveUID);
                    tool.AddControl(this.UIDCheckDigit);
                    tool.AddControl(this.CheckDigitModeID2);
                    tool.AddControl(this.UIDToCardNumber);
                    tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);


                    if (item.UIDToCardNumber.GetValueOrDefault() == 0)
                    {
                        if (string.IsNullOrEmpty(this.CardNumMaskImport.Text.Trim()) && string.IsNullOrEmpty(this.CardNumPatternImport.Text.Trim()))
                        {
                            tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
                        }
                    }

                }
                else//手动
                {
                    this.CardNumMask.Text = item.CardNumMask;
                    this.CardNumPattern.Text = item.CardNumPattern;
                    this.CardCheckdigit.SelectedValue = item.CardCheckdigit.GetValueOrDefault().ToString();
                    this.CheckDigitModeID.SelectedValue = item.CheckDigitModeID.GetValueOrDefault().ToString();
                    this.CardNumberToUID.SelectedValue = item.CardNumberToUID.GetValueOrDefault().ToString();
                    ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
                    tool.AddControl(this.CardNumMask);
                    tool.AddControl(this.CardNumPattern);
                    tool.AddControl(this.CardCheckdigit);
                    tool.AddControl(this.CheckDigitModeID);
                    tool.AddControl(this.CardNumberToUID);
                    if ((string.IsNullOrEmpty(this.CardNumMask.Text.Trim())) && (string.IsNullOrEmpty(this.CardNumPattern.Text.Trim())))
                    {
                        tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
                    }
                    else
                    {
                        tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);
                    }
                }
                checkIsImportCardNumber(false);
            }
        }

        protected void IsImportUIDNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkIsImportCardNumber(true);
        }

        private void checkIsImportCardNumber(bool clearText)
        {
            if (IsImportUIDNumber.SelectedValue == "1")
            {
                checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);

                this.ManualRoles.Hidden = true;
                this.ImportRoles.Hidden = false;
            }
            else
            {
                checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);

                this.ManualRoles.Hidden = false;
                this.ImportRoles.Hidden = true;
            }


            if (clearText)
            {
                this.CardNumPattern.Text = "";
                this.CardNumMask.Text = "";
                this.CardNumPatternImport.Text = "";
                this.CardNumMaskImport.Text = "";
            }
        }


        protected void CardCheckdigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //checkCheckDigitMode4Manual();
            checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);
            CardNumberToUID.SelectedIndex = 0;
        }
        protected void UIDCheckDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //checkCheckDigitMode4Import();
            checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            UIDToCardNumber.SelectedIndex = 0;
        }
        #endregion

        protected override SVA.Model.CardGrade GetPageObject(SVA.Model.CardGrade obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CardGradeCode_TextChanged(object sender, EventArgs e)
        {
            //ConvertTextboxToUpperText(sender);
        }

        protected void CardPointTransfer_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckCardPointTransfer();
        }

        protected void CardAmountTransfer_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckCardAmountTransfer();
        }

        protected void CardNumberToUID_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Manual(CardCheckdigit, CardNumberToUID, CheckDigitModeID);

        }
        protected void UIDToCardNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkCheckDigitMode4Import(UIDCheckDigit, UIDToCardNumber, CheckDigitModeID2, CardNumMaskImport, CardNumPatternImport);
            this.CardNumMaskImport.Text = "";
            this.CardNumPatternImport.Text = "";
        }

        protected void CardNumMask_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMask.Text = this.CardNumMask.Text.ToUpper();
            Tools.CheckTool.IsMatchNumMask(CardNumMask, CardNumPattern);
        }
        protected void CardNumPattern_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMask.Text) && string.IsNullOrEmpty(this.CardNumPattern.Text)) return;

            Tools.CheckTool.IsMatchNumMask(CardNumMask, CardNumPattern);
        }
        protected void CardNumMaskImport_TextChanged(object sender, EventArgs e)
        {
            this.CardNumMaskImport.Text = this.CardNumMaskImport.Text.ToUpper();
            Tools.CheckTool.IsMatchNumMask(CardNumMaskImport, CardNumPatternImport);
        }
        protected void CardNumPatternImport_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardNumMaskImport.Text) && string.IsNullOrEmpty(this.CardNumPatternImport.Text)) return;
            Tools.CheckTool.IsMatchNumMask(CardNumMaskImport, CardNumPatternImport);
        }
        protected void CardGradeUpdMethod_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.CardGradeUpdThreshold.Enabled = false;
            this.CardGradeUpdHitPLU.Enabled = false;
            switch (this.CardGradeUpdMethod.SelectedValue)
            {
                case "0":
                    break;
                case "1":
                case "2":
                case "3":
                    this.CardGradeUpdThreshold.Enabled = true;
                    break;
                default:
                    this.CardGradeUpdHitPLU.Enabled = true;
                    break;
            }
        }

        #region Add & Edit public function
        public static void checkCheckDigitMode4Manual(FineUI.RadioButtonList cardCheckdigit, FineUI.DropDownList cardNumberToUID, FineUI.DropDownList checkDigitModeID)
        {
            //Check select CD
            if (cardCheckdigit.SelectedValue == "1")
            {
                foreach (FineUI.ListItem item in cardNumberToUID.Items)
                {
                    if (item.Value == "3")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                checkDigitModeID.Hidden = false;
            }
            else
            {
                foreach (FineUI.ListItem item in cardNumberToUID.Items)
                {
                    if (item.Value == "2")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                if (cardNumberToUID.SelectedValue == "3" && cardCheckdigit.SelectedValue == "0")
                {
                    checkDigitModeID.Hidden = false;
                }
                else
                {
                    checkDigitModeID.Hidden = true;
                }
            }
        }

        public static void checkCheckDigitMode4Import(FineUI.RadioButtonList uIDCheckDigit, FineUI.DropDownList uIDToCardNumber, FineUI.DropDownList checkDigitModeID2, FineUI.TextBox cardNumMaskImport, FineUI.TextBox cardNumPatternImport)
        {
            //Check select CD
            if (uIDCheckDigit.SelectedValue == "1")
            {
                foreach (FineUI.ListItem item in uIDToCardNumber.Items)
                {
                    if (item.Value == "3")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                checkDigitModeID2.Hidden = false;
            }
            else
            {
                foreach (FineUI.ListItem item in uIDToCardNumber.Items)
                {
                    if (item.Value == "2")
                        item.EnableSelect = false;
                    else
                        item.EnableSelect = true;
                }

                //Check Digit Mode
                if (uIDToCardNumber.SelectedValue == "3")
                {
                    checkDigitModeID2.Hidden = false;
                }
                else
                {
                    checkDigitModeID2.Hidden = true;
                }
            }

            //Check CouponNumMask
            if (uIDToCardNumber.SelectedValue == "0")
            {
                cardNumMaskImport.Hidden = false;
                cardNumPatternImport.Hidden = false;
            }
            else
            {
                cardNumMaskImport.Hidden = true;
                cardNumPatternImport.Hidden = true;
            }
        }

        #endregion

        #region 根据dropdownlist判断
        private void CheckCardPointTransfer()
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(MinPointPreTransfer);
            tool.AddControl(MaxPointPreTransfer);
            tool.AddControl(DayMaxPointTransfer);
            if (this.CardPointTransfer.SelectedIndex == 0)
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxPointPreTransfer.Text = "999999";
                this.DayMaxPointTransfer.Text = "999999";

            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
            
        }
        private void CheckCardAmountTransfer()
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(MinAmountPreTransfer);
            tool.AddControl(MaxAmountPreTransfer);
            tool.AddControl(DayMaxAmountTransfer);
            if (this.CardAmountTransfer.SelectedIndex == 0)
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxAmountPreTransfer.Text = "999999.00";
                this.DayMaxAmountTransfer.Text = "999999.00";

            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
            
        }
        protected void IsAllowStoreValue_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            //tool.AddControl(CardGradeMaxAmount);
            tool.AddControl(MinAmountPreAdd);
            tool.AddControl(MaxAmountPreAdd);
            if (this.IsAllowStoreValue.SelectedValue == "0")
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                this.MaxAmountPreAdd.Text = "999999.00";
            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0.00", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
            
        }
        protected void IsAllowConsumptionPoint_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ExtAspNetPropertyTool tool = new ExtAspNetPropertyTool();
            tool.AddControl(CardGradeMaxPoint);
            tool.AddControl(MinPointPreAdd);
            tool.AddControl(MaxPointPreAdd);
            if (this.IsAllowConsumptionPoint.SelectedValue == "0")
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", false);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, false);

                //this.MaxPointPreAdd.Text = "999999";
                this.CardGradeMaxPoint.Text = "999999";

            }
            else
            {
                tool.SetDefaultValue(ExtAspNetPropertyTool.TextPropertyName.Text, "0", true);
                tool.SetState(ExtAspNetPropertyTool.PropertyName.Enalbed, true);
            }
            
        }
        #endregion

        #region CardGradeHoldCouponType
        protected void HoldCouponCount_OnTextChanged(object sender, EventArgs e)
        {
            controller.ViewModel.MainTable.HoldCouponCount = StringHelper.ConvertToInt(this.HoldCouponCount.Text);
        }
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CanHoldCouponGradeList.SelectedRowIndexArray;
            int count = this.Grid_CanHoldCouponGradeList.PageIndex * this.Grid_CanHoldCouponGradeList.PageSize;
            List<CardGradeHoldCouponRuleViewModel> list = new List<CardGradeHoldCouponRuleViewModel>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CanHoldCouponGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCanHoldCouponGradeListItem(list[i]);
            }
            BindingDataGrid_CanHoldCouponGradeList();
        }
        private void BindingDataGrid_CanHoldCouponGradeList()
        {
            this.Grid_CanHoldCouponGradeList.RecordCount = controller.ViewModel.CanHoldCouponGradeList.Count;
            this.Grid_CanHoldCouponGradeList.DataSource = controller.ViewModel.CanHoldCouponGradeList;
            this.Grid_CanHoldCouponGradeList.DataBind();
        }
        protected void Grid_CanHoldCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_CanHoldCouponGradeList.PageIndex = e.NewPageIndex;

        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_CanHoldCouponGradeList();
            BindingDataGrid_GetCouponGradeList();

            //删除图片
            DeleteFile(this.uploadFilePath.Text);
        }

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_GetCouponGradeList.SelectedRowIndexArray;
            int count = this.Grid_GetCouponGradeList.PageIndex * this.Grid_GetCouponGradeList.PageSize;
            List<CardGradeHoldCouponRuleViewModel> list = new List<CardGradeHoldCouponRuleViewModel>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.GetCouponGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveGetCouponGradeListItem(list[i]);
            }
            BindingDataGrid_GetCouponGradeList();
        }
        private void BindingDataGrid_GetCouponGradeList()
        {
            this.Grid_GetCouponGradeList.RecordCount = controller.ViewModel.GetCouponGradeList.Count;
            this.Grid_GetCouponGradeList.DataSource = controller.ViewModel.GetCouponGradeList;
            this.Grid_GetCouponGradeList.DataBind();
        }
        protected void Grid_GetCouponGradeList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            this.Grid_GetCouponGradeList.PageIndex = e.NewPageIndex;
        }
        #endregion


        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.CardGradePicFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.CardGradePicFile.SaveToServer("Brand");
                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("../../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }
        #endregion



        #region IForm Members
        public bool ValidateForm()
        {
            List<FineUI.TextBox> list = new List<FineUI.TextBox>();
            list.Add(CardTypeInitAmount);
            list.Add(CardGradeMaxAmount);
            list.Add(MinAmountPreAdd);
            list.Add(MaxAmountPreAdd);
            list.Add(MinConsumeAmount);
            list.Add(MinBalanceAmount);
            list.Add(CardAmountToPointRate);
            list.Add(MinAmountPreTransfer);
            list.Add(MaxAmountPreTransfer);
            list.Add(DayMaxAmountTransfer);
            if (!CheckAndConvertTextboxListToDecimal(list))
            {
                return false;
            }

            if (string.IsNullOrEmpty(this.MemberClauseDesc1.Text) || this.MemberClauseDesc1.Text == "<br>")
            {
                ShowWarning(Resources.MessageTips.Term1CannotBeEmpty);
                return false;
            }

            return true;
        }
        #endregion

        #region Use Rule逻辑处理
        protected bool ValidataUseRule()
        {
            double minAmountPreAdd = string.IsNullOrEmpty(this.MinAmountPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MinAmountPreAdd.Text);
            double maxAmountPreAdd = string.IsNullOrEmpty(this.MaxAmountPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MaxAmountPreAdd.Text);
            double minPointPreAdd = string.IsNullOrEmpty(this.MinPointPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MinPointPreAdd.Text);
            double maxPointPreAdd = string.IsNullOrEmpty(this.MaxPointPreAdd.Text) ? 0.00 : Convert.ToDouble(this.MaxPointPreAdd.Text);
            double minPointPreTransfer = string.IsNullOrEmpty(this.MinPointPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MinPointPreTransfer.Text);
            double maxPointPreTransfer = string.IsNullOrEmpty(this.MaxPointPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MaxPointPreTransfer.Text);
            double minAmountPreTransfer = string.IsNullOrEmpty(this.MinAmountPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MinAmountPreTransfer.Text);
            double maxAmountPreTransfer = string.IsNullOrEmpty(this.MaxAmountPreTransfer.Text) ? 0.00 : Convert.ToDouble(this.MaxAmountPreTransfer.Text);
            if (this.IsAllowStoreValue.SelectedValue != "0")
            {
                if (minAmountPreAdd >= maxAmountPreAdd)
                {
                    ShowWarning(Resources.MessageTips.MinAmountTooLarge);
                    return false;
                }
            }
            if (this.IsAllowConsumptionPoint.SelectedValue != "0")
            {
                if (minPointPreAdd >= maxPointPreAdd)
                {
                    ShowWarning(Resources.MessageTips.MinPointTooLarge);
                    return false;
                }
            }
            if (this.CardPointTransfer.SelectedValue != "0")
            {
                if (minPointPreTransfer >= maxPointPreTransfer)
                {
                    ShowWarning(Resources.MessageTips.MinTransferPointTooLarge);
                    return false;
                }
            }
            if (this.CardAmountTransfer.SelectedValue != "0")
            {
                if (minAmountPreTransfer >= maxAmountPreTransfer)
                {
                    ShowWarning(Resources.MessageTips.MinTransferCashTooLarge);
                    return false;
                }
            }
            return true;
        }
        #endregion

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        //校验模板文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.').ToLower();
                if (!webset.CardGradeFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CardGradeFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
