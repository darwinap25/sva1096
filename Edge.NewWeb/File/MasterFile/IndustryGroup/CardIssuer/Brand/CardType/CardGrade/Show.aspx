﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Show</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="TabStrip1" runat="server" />
    <ext:TabStrip ID="TabStrip1" ShowBorder="true" ActiveTabIndex="0" runat="server">
        <Tabs>
            <ext:Tab ID="tabBase" runat="server" Title="基本信息" EnableBackgroundColor="true" AutoScroll="true"
                BodyPadding="10px">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                                Text="关闭">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:GroupPanel ID="GroupPanel1" runat="server" EnableCollapse="True" Title="基本信息">
                        <Items>
                            <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form5" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardGradeCode" runat="server" Label="卡级别编号：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardGradeName1" runat="server" Label="描述：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardGradeName2" runat="server" Label="其他描述1：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardGradeName3" runat="server" Label="其他描述2：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow3" runat="server">
                                                <Items>
                                                    <ext:Label ID="CardTypeIDView" runat="server" Label="卡类型：">
                                                    </ext:Label>
                                                    <%--                                                    <ext:Label ID="CardGradeNotes" runat="server" Label="条款条例：" EncodeText="false">
                                                    </ext:Label>--%>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow26" runat="server">
                                                <Items>
                                                    <ext:Label ID="MemberClauseDesc1" runat="server" Label="条款条例1：" EncodeText="false">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow27" runat="server">
                                                <Items>
                                                    <ext:Label ID="MemberClauseDesc2" runat="server" Label="条款条例2：" EncodeText="false">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow28" runat="server">
                                                <Items>
                                                    <ext:Label ID="MemberClauseDesc3" runat="server" Label="条款条例3：" EncodeText="false">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" Hidden="true">
                                    </ext:DropDownList>
                                    <%--                    <ext:Image ID="CardGradePicFile" runat="server" ImageUrl="" Label="卡图片:" Width="100px">
                    </ext:Image>
                    <ext:Image ID="CardGradeLayoutFile" runat="server" ImageUrl="" Label="卡设计模板:" Width="100px">
                    </ext:Image>
                    <ext:Image ID="CardGradeStatementFile" runat="server" ImageUrl="" Label="卡结算单模板:" Width="100px">
                    </ext:Image>--%>
                                    <ext:Form ID="Form2" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 40% 10%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:Label ID="Label1" Text="" runat="server" Label="卡图片：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnPreview" runat="server" Text="查看" Icon="Picture">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 40% 20%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath1" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:Label ID="Label2" Text="" runat="server" Label="卡设计模板：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnExport" runat="server" Text="下载" OnClick="btnExport_Click" Icon="PageExcel"
                                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="Form4" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="0% 40% 20%">
                                                <Items>
                                                    <ext:Label ID="uploadFilePath2" Hidden="true" Text="" runat="server">
                                                    </ext:Label>
                                                    <ext:Label ID="Label3" Text="" runat="server" Label="卡结算单模板：">
                                                    </ext:Label>
                                                    <ext:Button ID="btnExport1" runat="server" Text="下载" OnClick="btnExport1_Click" Icon="PageExcel"
                                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                                    </ext:Button>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Form ID="Form6" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server">
                                        <Rows>
                                            <ext:FormRow ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="PasswordRuleIDView" runat="server" Label="密码规则：">
                                                    </ext:Label>
                                                    <ext:Label ID="CampaignIDView" runat="server" Label="活动：" Hidden="true">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="PasswordRuleID" runat="server" Label="密码规则：" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CampaignID" runat="server" Label="活动：" Hidden="true">
                                    </ext:DropDownList>
                                    <ext:Label ID="CardGradeRank" runat="server" Label="卡级别等级：">
                                    </ext:Label>
                                    <%--add by Alex 2014-06-30 --%>
                                    <ext:Label ID="TrainingModeView" runat="server" Label="Translate__Special_121_Start是否训练卡（是/ 否）：Translate__Special_121_End">
                                    </ext:Label>
                                    <ext:RadioButtonList ID="TrainingMode" runat="server" Label="Translate__Special_121_Start是否训练卡？：Translate__Special_121_End"
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <%--add by Alex 2014-06-30 --%>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="卡号规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm8" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form7" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow13" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="IsImportUIDNumberView" runat="server" Label="卡号码是否导入：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:RadioButtonList ID="IsImportUIDNumber" runat="server" Label="卡号码是否导入：" AutoPostBack="true"
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="true" />
                                    </ext:RadioButtonList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="ManualRoles" runat="server" EnableCollapse="True" Title="手动创建编号规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm2" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form8" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <%--add by Alex 2014-06-30--%>
                                            <ext:FormRow ID="FormRow29" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="AllowOfflineQRCodeView" runat="server" Label="Translate__Special_121_Start是否允许离线的QR码（是/否）：Translate__Special_121_End">
                                                    </ext:Label>
                                                    <ext:Label ID="QRCodePrefixView" runat="server" Label="Translate__Special_121_StartQR码前缀号码：Translate__Special_121_End">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <%--add by Alex 2014-06-30--%>
                                            <ext:FormRow ID="FormRow14" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardNumMask" runat="server" Label="卡号编码规则：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardNumPattern" runat="server" Label="卡号前缀号码：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow15" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardCheckdigitView" runat="server" Label="卡号是否添加校验位：">
                                                    </ext:Label>
                                                    <ext:Label ID="CheckDigitModeIDView" runat="server" Label="校验位计算方法：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:Label ID="CardNumberToUIDView" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End">
                                    </ext:Label>
                                    <%--add by Alex 2014-06-30 ++--%>
                                    <ext:RadioButtonList ID="AllowOfflineQRCode" runat="server" Label="Translate__Special_121_Start是否允许离线的QR码（是 /否）：Translate__Special_121_End"
                                        AutoPostBack="True" Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="True" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="QRCodePrefix" runat="server" Label="Translate__Special_121_StartQR码前缀号码：Translate__Special_121_End"
                                        Resizable="true">
                                        <ext:ListItem Value="" Text="-----------" />
                                        <ext:ListItem Value="Q7FM" Text="Q7FM" />
                                    </ext:DropDownList>
                                    <%--add by Alex 2014-06-30 ++--%>
                                    <ext:RadioButtonList ID="CardCheckdigit" runat="server" Label="卡号是否添加校验位：" AutoPostBack="True"
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="True" />
                                    </ext:RadioButtonList>
                                    <ext:DropDownList ID="CheckDigitModeID" runat="server" Label="校验位计算方法：" Width="100px"
                                        Enabled="false">
                                        <ext:ListItem Value="1" Text="EAN13" />
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CardNumberToUID" runat="server" Label="Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End"
                                        Enabled="false">
                                        <ext:ListItem Value="1" Text="全部复制" />
                                        <ext:ListItem Value="0" Text="绑定" />
                                        <ext:ListItem Value="2" Text="复制去掉校验位" />
                                        <ext:ListItem Value="3" Text="复制加上校验位" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="ImportRoles" runat="server" EnableCollapse="True" Title="导入物理编号规则"
                        Hidden="true">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm3" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form9" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow4" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardNumMaskImport" runat="server" Label="卡号编码规则：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardNumPatternImport" runat="server" Label="卡号前缀号码：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow5" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="IsConsecutiveUIDView" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End">
                                                    </ext:Label>
                                                    <ext:Label ID="UIDCheckDigitView" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:RadioButtonList ID="IsConsecutiveUID" runat="server" Label="Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End"
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="UIDCheckDigit" runat="server" Label="Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End"
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="UIDToCardNumberView" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End">
                                    </ext:Label>
                                    <ext:DropDownList ID="UIDToCardNumber" runat="server" Label="Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End"
                                        Enabled="false">
                                        <ext:ListItem Value="1" Text="全部复制" />
                                        <ext:ListItem Value="0" Text="绑定" />
                                        <ext:ListItem Value="2" Text="复制去掉校验位" />
                                        <ext:ListItem Value="3" Text="复制加上校验位" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel5" runat="server" EnableCollapse="True" Title="使用规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm4" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form10" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow6" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="IsAllowStoreValueView" runat="server" Label="是否允许储值：">
                                                    </ext:Label>
                                                    <ext:Label ID="IsAllowConsumptionPointView" runat="server" Label="是否允许积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow7" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardTypeInitAmount" runat="server" Label="初始金额：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardTypeInitPoints" runat="server" Label="初始积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow8" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardGradeMaxAmount" runat="server" Label="卡最大現金賬套金額：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardGradeMaxPoint" runat="server" Label="卡最大积分帐套积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow9" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MinAmountPreAdd" runat="server" Label="最低充值现金帐套金额：">
                                                    </ext:Label>
                                                    <ext:Label ID="MinPointPreAdd" runat="server" Label="最低充值积分帐套积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow10" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MaxAmountPreAdd" runat="server" Label="最高充值现金帐套金额：">
                                                    </ext:Label>
                                                    <ext:Label ID="MaxPointPreAdd" runat="server" Label="最高充值积分帐套积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow11" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardGradeDiscCeiling" runat="server" Label="折扣最高值：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardConsumeBasePoint" runat="server" Label="最小消费积分（每次）：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow12" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MinBalancePoint" runat="server" Label="最小剩余积分：">
                                                    </ext:Label>
                                                    <ext:Label ID="MinConsumeAmount" runat="server" Label="最小消费金额（每次）：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow16" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MinBalanceAmount" runat="server" Label="最小剩余金额：">
                                                    </ext:Label>
                                                    <ext:Label ID="ForfeitAmountAfterExpiredView" runat="server" Label="过期后是否清空帐套金额：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:RadioButtonList ID="IsAllowStoreValue" runat="server" Label="是否允许储值：" Width="100px"
                                        Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" />
                                        <ext:RadioItem Text="否" Value="0" Selected="True" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="IsAllowConsumptionPoint" runat="server" Label="是否允许积分："
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:RadioButtonList ID="ForfeitAmountAfterExpired" runat="server" Label="过期后是否清空帐套金额："
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="不清零" Value="0" Selected="True" />
                                        <ext:RadioItem Text="清零" Value="1" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="ForfeitPointAfterExpiredView" runat="server" Label="过期后是否清空帐套积分：">
                                    </ext:Label>
                                    <ext:RadioButtonList ID="ForfeitPointAfterExpired" runat="server" Label="过期后是否清空帐套积分："
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="不清零" Value="0" Selected="True" />
                                        <ext:RadioItem Text="清零" Value="1" />
                                    </ext:RadioButtonList>
                                    <%--Add by Alex 2014-06-30 ++--%>
                                    <ext:Form ID="Form14" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow30" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="NumberOfTransDisplay" runat="server" Label="交易数据显示数量： ">
                                                    </ext:Label>
                                                    <ext:Label ID="NumberOfCouponDisplay" runat="server" Label="优惠券显示数量：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow31" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="NumberOfNewsDisplay" runat="server" Label="广告显示数量：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <%--Add by Alex 2014-06-30 ++--%>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel3" runat="server" EnableCollapse="True" Title="升级规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm5" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:DropDownList ID="CardGradeUpdMethod" runat="server" Label="升级条件：" Enabled="false">
                                        <ext:ListItem Value="0" Text="无" Selected="true" />
                                        <ext:ListItem Value="3" Text="单笔消费" />
                                        <ext:ListItem Value="1" Text="年累积购买值大于" />
                                        <ext:ListItem Value="2" Text="年累计积分值大于" />
                                        <ext:ListItem Value="4" Text="获得升级需要的优惠券类型指定的优惠券数量达到指定值" />
                                    </ext:DropDownList>
                                    <ext:Form ID="Form11" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow17" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardGradeUpdMethodView" runat="server" Label="升级条件：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardGradeUpdThreshold" runat="server" Label="界限值：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow25" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardGradeUpdHitPLU" runat="server" Label="指定商品：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardGradeUpdCouponTypeIDView" runat="server" Label="升级需要的优惠券类型：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="CardGradeUpdCouponTypeID" runat="server" Label="升级需要的优惠券类型："
                                        Hidden="true">
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="有效期规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm6" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form12" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow18" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardValidityDuration" runat="server" Label="有效期长度：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardValidityUnitView" runat="server" Label="有效期长度单位：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow19" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="ActiveResetExpiryDateView" runat="server" Label="激活是否重置有效期：">
                                                    </ext:Label>
                                                    <ext:Label ID="GracePeriodValue" runat="server" Label="宽限期值：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="CardValidityUnit" runat="server" Label="有效期长度单位：" Enabled="false">
                                        <ext:ListItem Value="1" Text="年" Selected="true" />
                                        <ext:ListItem Value="2" Text="月" />
                                        <ext:ListItem Value="3" Text="星期" />
                                        <ext:ListItem Value="4" Text="天" />
                                    </ext:DropDownList>
                                    <ext:RadioButtonList ID="ActiveResetExpiryDate" runat="server" Label="激活是否重置有效期："
                                        Width="100px" Enabled="false">
                                        <ext:RadioItem Text="是" Value="1" Selected="True" />
                                        <ext:RadioItem Text="否" Value="0" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="GracePeriodUnitView" runat="server" Label="宽限期单位：">
                                    </ext:Label>
                                    <ext:DropDownList ID="GracePeriodUnit" runat="server" Label="宽限期单位:" Enabled="false">
                                        <ext:ListItem Value="1" Text="年" Selected="true" />
                                        <ext:ListItem Value="2" Text="月" />
                                        <ext:ListItem Value="3" Text="星期" />
                                        <ext:ListItem Value="4" Text="天" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel7" runat="server" EnableCollapse="True" Title="转赠/转换规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm7" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form13" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow20" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardPointToAmountRate" runat="server" Label="积分转换现金规则：">
                                                    </ext:Label>
                                                    <ext:Label ID="CardAmountToPointRate" runat="server" Label="现金转换积分规则：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow21" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardPointTransferView" runat="server" Label="积分转换规则（转出）：">
                                                    </ext:Label>
                                                    <ext:Label ID="MinPointPreTransfer" runat="server" Label="每笔交易最小转换积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow22" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MaxPointPreTransfer" runat="server" Label="每笔交易最大转换积分：">
                                                    </ext:Label>
                                                    <ext:Label ID="DayMaxPointTransfer" runat="server" Label="每天最大转赠积分：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow23" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="CardAmountTransferView" runat="server" Label="现金转换规则：">
                                                    </ext:Label>
                                                    <ext:Label ID="MinAmountPreTransfer" runat="server" Label="每笔交易最小转换金额：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow24" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MaxAmountPreTransfer" runat="server" Label="每笔交易最大转换金额：">
                                                    </ext:Label>
                                                    <ext:Label ID="DayMaxAmountTransfer" runat="server" Label="每天最大转赠金额：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                    <ext:DropDownList ID="CardPointTransfer" runat="server" Label="积分转换规则（转出）：" Enabled="false">
                                        <ext:ListItem Value="0" Text="不允许" Selected="true" />
                                        <ext:ListItem Value="1" Text="允许同卡级别" />
                                        <ext:ListItem Value="2" Text="允许同卡类型" />
                                        <ext:ListItem Value="3" Text="允许同品牌" />
                                        <ext:ListItem Value="4" Text="允许同发行商" />
                                    </ext:DropDownList>
                                    <ext:DropDownList ID="CardAmountTransfer" runat="server" Label="现金转换规则：" Enabled="false">
                                        <ext:ListItem Value="0" Text="不允许" Selected="true" />
                                        <ext:ListItem Value="1" Text="允许同卡级别" />
                                        <ext:ListItem Value="2" Text="允许同卡类型" />
                                        <ext:ListItem Value="3" Text="允许同品牌" />
                                        <ext:ListItem Value="4" Text="允许同发行商" />
                                    </ext:DropDownList>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <%--add by Alex 2014-06-30 ++--%>
                    <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="卡级别时间规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm10" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Form ID="Form15" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                                        ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                                        <Rows>
                                            <ext:FormRow ID="FormRow36" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="MobileProtectPeriodValue" runat="server" Label="手机号码重用时间数值（月）：">
                                                    </ext:Label>
                                                    <ext:Label ID="EmailValidatedPeriodValue" runat="server" Label="邮箱验证时间数值（分钟）：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow32" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="NonValidatedPeriodValue" runat="server" Label="未验证用户重设时间数值（小时）：">
                                                    </ext:Label>
                                                    <ext:Label ID="LoginFailureCount" runat="server" Label="登录错误次数：">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                            <ext:FormRow ID="FormRow33" runat="server" ColumnWidths="50% 50%">
                                                <Items>
                                                    <ext:Label ID="SessionTimeoutValue" runat="server" Label="登录超时数值（小时）：">
                                                    </ext:Label>
                                                    <ext:Label ID="QRCodePeriodValue" runat="server" Label="Translate__Special_121_StartQR码有效时间（分钟）：Translate__Special_121_End">
                                                    </ext:Label>
                                                </Items>
                                            </ext:FormRow>
                                        </Rows>
                                    </ext:Form>
                                </Items>
                            </ext:SimpleForm>
                        </Items>
                    </ext:GroupPanel>
                    <%--add by Alex 2014-06-30 ++--%>
                    <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="可获取优惠券类型规则">
                        <Items>
                            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false"
                                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                                <Items>
                                    <ext:Label ID="HoldCouponCount" runat="server" Label="可获取累计最大数量：">
                                    </ext:Label>
                                </Items>
                            </ext:SimpleForm>
                            <ext:Grid ID="Grid_CanHoldCouponGradeList" ShowBorder="true" ShowHeader="true" Title="可获取优惠券类型列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="Grid_CanHoldCouponGradeList_OnPageIndexChange">
                                <Columns>
                                    <ext:BoundField ColumnID="BrandDescription" DataField="BrandDescription" HeaderText="品牌编号" />
                                    <ext:BoundField ColumnID="CouponTypeDescription" DataField="CouponTypeDescription"
                                        HeaderText="优惠券类型" />
                                    <ext:BoundField ColumnID="MainTable_HoldCount" DataField="MainTable.HoldCount" HeaderText="单个获取最大数量" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel38" runat="server" EnableCollapse="True" Title="自动绑定优惠券类型规则">
                        <Items>
                            <ext:Grid ID="Grid_GetCouponGradeList" ShowBorder="true" ShowHeader="true" Title="自动绑定优惠券类型列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="Grid_GetCouponGradeList_OnPageIndexChange">
                                <Columns>
                                    <ext:BoundField ColumnID="BrandDescription" DataField="BrandDescription" HeaderText="品牌编号" />
                                    <ext:BoundField ColumnID="CouponTypeDescription" DataField="CouponTypeDescription"
                                        HeaderText="优惠券类型" />
                                    <ext:BoundField ColumnID="MainTable_HoldCount" DataField="MainTable.HoldCount" HeaderText="优惠券数量" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab1" Title="有效期延长规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptExtensionRuleList" runat="server" ShowBorder="false" ShowHeader="false"
                        DataKeyNames="ExtensionRuleID" AutoHeight="true" EnableCheckBoxSelect="false"
                        EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="规则类型">
                                <ItemTemplate>
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("RuleType")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="规则记录序号">
                                <ItemTemplate>
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("ExtensionRuleSeqNo")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="开始日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="结束日期">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="ExtensionRuleID" DataIFrameUrlFormatString="CardExtensionRule/show.aspx?id={0}"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
            <ext:Tab ID="Tab2" Title="获取积分规则" EnableBackgroundColor="true" runat="server">
                <Items>
                    <ext:Grid ID="rptEarnAmountPointList" runat="server" ShowBorder="false" ShowHeader="false"
                        DataKeyNames="PointRuleID" AutoHeight="true" EnableCheckBoxSelect="false" EnableRowNumber="True"
                        AutoWidth="true" ForceFitAllTime="true">
                        <Columns>
                            <ext:TemplateField HeaderText="规则编号">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("PointRuleSeqNo")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="计算方式">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("PointRuleOper")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="开始日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField HeaderText="结束日期">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:WindowField ColumnID="ViewWindowField" WindowID="Window1" Icon="Page" Text="查看"
                                ToolTip="查看" DataIFrameUrlFields="ExtensionRuleID" DataIFrameUrlFormatString="PointRule/show.aspx?id={0}"
                                DataWindowTitleFormatString="查看" Title="查看" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Tab>
        </Tabs>
    </ext:TabStrip>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
