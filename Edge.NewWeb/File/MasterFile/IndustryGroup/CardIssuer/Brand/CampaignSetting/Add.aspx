﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:Panel ID="Panel1" runat="server" BodyPadding="5px" EnableBackgroundColor="true"
        ShowBorder="true" ShowHeader="true" Title="Panel">
        <Items>
            <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
                BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
                LabelAlign="Right">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                                Text="关闭">
                            </ext:Button>
                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                            </ext:ToolbarSeparator>
                            <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                                OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:TextBox ID="CampaignCode" runat="server" Label="活动编号：" MaxLength="20" Required="true"
                        ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText"
                        AutoPostBack="true" ToolTipTitle="活动编号" ToolTip="Translate__Special_121_StartKey identifier of Campaign. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
                    </ext:TextBox>
                    <ext:TextBox ID="CampaignName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                        ShowRedStar="true" ToolTipTitle="描述" ToolTip="请输入规范的活動名称。不能超過512個字符"
                        OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                    </ext:TextBox>
                    <ext:TextBox ID="CampaignName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1"
                        ToolTip="對活動的描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                    </ext:TextBox>
                    <ext:TextBox ID="CampaignName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                        ToolTip="對活動的另一個描述,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
                    </ext:TextBox>
                    <ext:TextBox ID="CampaignDetail1" runat="server" Label="详细描述：" MaxLength="512" ToolTipTitle="详细描述"
                        ToolTip="请输入规范的活動詳細內容的名称。不能超過512個字符">
                    </ext:TextBox>
                    <ext:TextBox ID="CampaignDetail2" runat="server" Label="其他详细描述1：" MaxLength="512"
                        ToolTipTitle="其他详细描述1" ToolTip="對活動詳細內容的描述,不能超過512個字符">
                    </ext:TextBox>
                    <ext:TextBox ID="CampaignDetail3" runat="server" Label="其他详细描述2：" MaxLength="512"
                        ToolTipTitle="其他详细描述2" ToolTip="對活動詳細內容的另一個描述,不能超過512個字符">
                    </ext:TextBox>
                    <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Required="true" ShowRedStar="true"
                        EnableEdit="false" AutoPostBack="true" OnSelectedIndexChanged="BrandID_OnSelectedIndexChanged"
                        Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                        CompareMessage="请选择有效值">
                    </ext:DropDownList>

                    <ext:HiddenField ID="uploadFilePath" runat="server"></ext:HiddenField>
                        <ext:Form ID="Form2" ShowHeader="false" EnableBackgroundColor="true" ShowBorder="false" runat="server">
                            <Rows>
                                <ext:FormRow ID="FormRow1" ColumnWidths="85% 15%" runat="server">
                                    <Items>
                                        <ext:FileUpload ID="CampaignPicFile" runat="server" Label="图片：" ToolTipTitle="图片"
                                            ToolTip="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End">
                                        </ext:FileUpload>
                                        <ext:Button ID="btnPreview" runat="server" Text="预览" OnClick="ViewPicture" Hidden="true"></ext:Button>
                                    </Items>
                                </ext:FormRow>
                            </Rows>
                    </ext:Form>

                    <ext:DatePicker ID="StartDate" runat="server" Label="活动启用日期：" DateFormatString="yyyy-MM-dd"
                        ShowRedStar="true" Required="true" ToolTipTitle="活动启用日期" ToolTip="Translate__Special_121_Start日期格式：YYYY-MM-DDTranslate__Special_121_End">
                    </ext:DatePicker>
                    <ext:DatePicker ID="EndDate" runat="server" Label="活动结束日期：" DateFormatString="yyyy-MM-dd"
                        CompareControl="StartDate" CompareOperator="GreaterThanEqual" CompareMessage="活动结束日期应该大于等于活动启用日期"
                        ShowRedStar="true" Required="true" ToolTipTitle="活动结束日期" ToolTip="日期格式：YYYY-MM-DD">
                    </ext:DatePicker>
                    <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
                    <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="自动绑定卡级别规则">
                        <Items>
                            <ext:Grid ID="Grid_CardGradeList" ShowBorder="true" ShowHeader="true" Title="卡级别列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="Key"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="RegisterGrid_OnPageIndexChange">
                                <Toolbars>
                                    <ext:Toolbar ID="Toolbar2" runat="server">
                                        <Items>
                                            <ext:Button ID="btnNew" Text="新增" Icon="Add" runat="server" OnClick="btnNew_OnClick">
                                            </ext:Button>
                                            <ext:Button ID="btnDelete" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete_OnClick">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField ColumnID="Key" DataField="Key" Hidden="true" />
                                    <ext:BoundField ColumnID="Value" DataField="Value" HeaderText="卡级别" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                    <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="自动绑定优惠券类型规则">
                        <Items>
                            <ext:Grid ID="Grid_CouponTypeList" ShowBorder="true" ShowHeader="true" Title="优惠券类型列表"
                                AutoHeight="true" PageSize="5" runat="server" EnableCheckBoxSelect="True" DataKeyNames="KeyID"
                                AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                                OnPageIndexChange="RegisterGrid_OnPageIndexChange">
                                <Toolbars>
                                    <ext:Toolbar ID="Toolbar3" runat="server">
                                        <Items>
                                            <ext:Button ID="btnNew1" Text="新增" Icon="Add" runat="server" OnClick="btnNew1_OnClick">
                                            </ext:Button>
                                            <ext:Button ID="btnDelete1" Text="删除" Icon="Delete" runat="server" OnClick="btnDelete1_OnClick">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField ColumnID="Key" DataField="Key" Hidden="true" />
                                    <ext:BoundField ColumnID="Value" DataField="Value" HeaderText="优惠券类型" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:GroupPanel>
                </Items>
            </ext:SimpleForm>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Popup="false" EnableIFrame="true" runat="server" CloseAction="Hide"
        OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="850px" Height="510px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
