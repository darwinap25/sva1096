﻿using System;

using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Campaign,Edge.SVA.Model.Campaign>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CampaignModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //Tools.ControlTool.BindBrand(BrandID);
                RegisterCloseEvent(btnClose);
                logger.WriteOperationLog(this.PageName, "Show");
                SVASessionInfo.CampaignModifyController = null;
            }
            controller = SVASessionInfo.CampaignModifyController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                string text = brand == null? "" : DALTool.GetStringByCulture(brand.BrandName1,brand.BrandName2,brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(text, brand.BrandCode);

                //switch (Model.Status.GetValueOrDefault())
                //{
                //    case 1: this.Status.Text = "创建"; break;
                //    case 2: this.Status.Text = "已发布"; break;
                //    case 3: this.Status.Text = "过期"; break;
                //    case 4: this.Status.Text = "作废"; break;
                //    default: this.Status.Text = ""; break;
                    
                //}

                if (Model != null)
                {
                    this.uploadFilePath.Text = Model.CampaignPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.CampaignPicFile) ? true : false;//没有照片时不显示查看按钮(Len)

                    controller.LoadViewModel(Model.CampaignID, SVASessionInfo.SiteLanguage);
                    BindingDataGrid_CardGradeList();
                    BindingDataGrid_CouponTypeList();
                }
            }
        }


        #region CardGrade CouponType
        protected void BrandID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            controller.ViewModel.CardGradeList.Clear();
            controller.ViewModel.CouponTypeList.Clear();
            BindingDataGrid_CardGradeList();
            BindingDataGrid_CouponTypeList();
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CardGradeList.SelectedRowIndexArray;
            int count = this.Grid_CardGradeList.PageIndex * this.Grid_CardGradeList.PageSize;
            List<KeyValue> list = new List<KeyValue>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CardGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCardGrade(list[i]);
            }
            BindingDataGrid_CardGradeList();
        }
        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CouponTypeList.SelectedRowIndexArray;
            int count = this.Grid_CouponTypeList.PageIndex * this.Grid_CouponTypeList.PageSize;
            List<KeyValue> list = new List<KeyValue>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CouponTypeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCouponType(list[i]);
            }
            BindingDataGrid_CouponTypeList();
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_CardGradeList();
            BindingDataGrid_CouponTypeList();
        }

        private void BindingDataGrid_CardGradeList()
        {
            this.Grid_CardGradeList.RecordCount = controller.ViewModel.CardGradeList.Count;
            this.Grid_CardGradeList.DataSource = controller.ViewModel.CardGradeList;
            this.Grid_CardGradeList.DataBind();
        }
        private void BindingDataGrid_CouponTypeList()
        {
            this.Grid_CouponTypeList.RecordCount = controller.ViewModel.CouponTypeList.Count;
            this.Grid_CouponTypeList.DataSource = controller.ViewModel.CouponTypeList;
            this.Grid_CouponTypeList.DataBind();
        }
        #endregion
    }
}
