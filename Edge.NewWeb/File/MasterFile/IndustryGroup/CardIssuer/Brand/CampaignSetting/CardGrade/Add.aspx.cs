﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CardGrade
{
    public partial class Add : PageBase
    {

        CampaignController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["BrandID"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.BrandID.Text = Request["BrandID"].ToString();
                }
                if (Request["PageFlag"] == null)
                {
                    ActiveWindow.GetHidePostBackReference();
                    return;
                }
                else
                {
                    this.PageFlag.Text = Request["PageFlag"].ToString();
                }
                RegisterCloseEvent(btnClose);
            }
            if (this.PageFlag.Text == "0")
            {
                controller = SVASessionInfo.CampaignAddController;
            }
            else
            {
                controller = SVASessionInfo.CampaignModifyController;
            }
            if (!IsPostBack)
            {
                InitDate();
                CardType_SelectedIndexChanged(null, null);
            }
        }
        private void InitDate()
        {
            int brandID = ConvertTool.ConverType<int>(this.BrandID.Text);
            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();
            List<Edge.SVA.Model.CardType> list = bll.GetModelList(" brandID=" + brandID + " order by CardTypeCode");
            List<KeyValue> list1 = new List<KeyValue>();
            foreach (var item in list)
            {
                KeyValue kv = new KeyValue();
                kv.Key=item.CardTypeID.ToString();
                switch (SVASessionInfo.SiteLanguage)
                {
                    case LanguageFlag.ZHCN:
                        kv.Value = item.CardTypeCode+"-"+item.CardTypeName2;
                        break;
                    case LanguageFlag.ZHHK:
                        kv.Value = item.CardTypeCode + "-" + item.CardTypeName3;
                        break;
                    case LanguageFlag.ENUS:
                    default:
                        kv.Value = item.CardTypeCode + "-" + item.CardTypeName1;
                        break;
                }
                list1.Add(kv);
            }
            this.CardType.DataSource = list1;
            this.CardType.DataTextField = "Value";
            this.CardType.DataValueField = "Key";
            this.CardType.DataBind();
        }
        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int cardTypeID = ConvertTool.ConverType<int>(this.CardType.SelectedValue);
            if (cardTypeID==0)
            {
                return;
            }
            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();
            List<Edge.SVA.Model.CardGrade> list = bll.GetModelList(" CardTypeID=" + cardTypeID+" order by CardGradeCode");
            List<KeyValue> list1 = new List<KeyValue>();
            foreach (var item in list)
            {
                KeyValue exist = controller.ViewModel.CardGradeList.Find(m => m.Key == item.CardGradeID.ToString());
                if (exist!=null)
                {
                    continue;
                }

                KeyValue kv = new KeyValue();
                kv.Key = item.CardGradeID.ToString();
                switch (SVASessionInfo.SiteLanguage)
                {
                    case LanguageFlag.ZHCN:
                        kv.Value = item.CardGradeCode+"-"+item.CardGradeName2;
                        break;
                    case LanguageFlag.ZHHK:
                        kv.Value = item.CardGradeCode + "-" + item.CardGradeName3;
                        break;
                    case LanguageFlag.ENUS:
                    default:
                        kv.Value = item.CardGradeCode + "-" + item.CardGradeName1;
                        break;
                }
                list1.Add(kv);
            }
            this.Grid_CardGradeList.RecordCount = list1.Count;
            this.Grid_CardGradeList.DataSource = list1;
            this.Grid_CardGradeList.DataBind();

        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();
            List<string> list=GetSelectedRowIndexArrayFromHiddenField();
            controller.AddCardGradeList(list);
            CloseAndPostBack();
        }
        protected override void RegisterGrid_OnPageIndexChange(object sender, GridPageEventArgs e)
        {
            SyncSelectedRowIndexArrayToHiddenField();

            base.RegisterGrid_OnPageIndexChange(sender, e);

            UpdateSelectedRowIndexArray();
        }

        #region Events

        private List<string> GetSelectedRowIndexArrayFromHiddenField()
        {
            JArray idsArray = new JArray();

            string currentIDS = hfSelectedIDS.Text.Trim();
            if (!String.IsNullOrEmpty(currentIDS))
            {
                idsArray = JArray.Parse(currentIDS);
            }
            else
            {
                idsArray = new JArray();
            }

            return new List<string>(idsArray.ToObject<string[]>());
        }

        private void SyncSelectedRowIndexArrayToHiddenField()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();


            if (Grid_CardGradeList.SelectedRowIndexArray != null && Grid_CardGradeList.SelectedRowIndexArray.Length > 0)
            {
                List<int> selectedRows = new List<int>(Grid_CardGradeList.SelectedRowIndexArray);
                int startPageIndex = Grid_CardGradeList.PageIndex * Grid_CardGradeList.PageSize;
                for (int i = startPageIndex, count = Math.Min(startPageIndex + Grid_CardGradeList.PageSize, Grid_CardGradeList.RecordCount); i < count; i++)
                {
                    string id = Grid_CardGradeList.DataKeys[i][0].ToString();
                    if (selectedRows.Contains(i - startPageIndex))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                        }
                    }
                    else
                    {
                        if (ids.Contains(id))
                        {
                            ids.Remove(id);
                        }
                    }
                }
            }

            hfSelectedIDS.Text = new JArray(ids).ToString(Formatting.None);
        }


        private void UpdateSelectedRowIndexArray()
        {
            List<string> ids = GetSelectedRowIndexArrayFromHiddenField();

            List<int> nextSelectedRowIndexArray = new List<int>();
            int nextStartPageIndex = Grid_CardGradeList.PageIndex * Grid_CardGradeList.PageSize;
            for (int i = nextStartPageIndex, count = Math.Min(nextStartPageIndex + Grid_CardGradeList.PageSize, Grid_CardGradeList.RecordCount); i < count; i++)
            {
                string id = Grid_CardGradeList.DataKeys[i][0].ToString();
                if (ids.Contains(id))
                {
                    nextSelectedRowIndexArray.Add(i - nextStartPageIndex);
                }
            }
            Grid_CardGradeList.SelectedRowIndexArray = nextSelectedRowIndexArray.ToArray();
        }

        #endregion
    }
}