﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="CampaignCode" runat="server" Label="活动编号：">
                            </ext:Label>
                            <ext:Label ID="CampaignName1" runat="server" Label="描述：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="CampaignName2" runat="server" Label="其他描述1：">
                            </ext:Label>
                            <ext:Label ID="CampaignName3" runat="server" Label="其他描述2：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow3" runat="server">
                        <Items>
                            <ext:Label ID="CampaignDetail1" runat="server" Label="详细描述：">
                            </ext:Label>
                            <ext:Label ID="CampaignDetail2" runat="server" Label="其他详细描述1：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow4" runat="server">
                        <Items>
                            <ext:Label ID="CampaignDetail3" runat="server" Label="其他详细描述2：">
                            </ext:Label>
                            <ext:Label ID="BrandID" runat="server" Label="品牌：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ColumnWidths="0% 40% 10%">
                        <Items>
                            <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                            </ext:Label>
                            <ext:Label ID="Label1" Text="" runat="server" Label="图片：">
                            </ext:Label>
                            <ext:Button ID="btnPreview" runat="server" Text="查看" Icon="Picture">
                            </ext:Button>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow5" runat="server">
                        <Items>
                            <ext:Label ID="StartDate" runat="server" Label="活动启用日期：">
                            </ext:Label>
                            <ext:Label ID="EndDate" runat="server" Label="活动结束日期：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow6" runat="server">
                        <Items>
                            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
                            </ext:Label>
                            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow7" runat="server">
                        <Items>
                            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
                            </ext:Label>
                            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:GroupPanel ID="GroupPanel8" runat="server" EnableCollapse="True" Title="自动绑定卡级别规则">
                <Items>
                    <ext:Grid ID="Grid_CardGradeList" ShowBorder="true" ShowHeader="true" Title="卡级别列表"
                        AutoHeight="true" PageSize="5" runat="server" DataKeyNames="Key"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="RegisterGrid_OnPageIndexChange">
                        <Columns>
                            <ext:BoundField ColumnID="Key" DataField="Key" Hidden="true" />
                            <ext:BoundField ColumnID="Value" DataField="Value" HeaderText="卡级别" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel6" runat="server" EnableCollapse="True" Title="自动绑定优惠券类型规则">
                <Items>
                    <ext:Grid ID="Grid_CouponTypeList" ShowBorder="true" ShowHeader="true" Title="优惠券类型列表"
                        AutoHeight="true" PageSize="5" runat="server" DataKeyNames="KeyID"
                        AllowPaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true"
                        OnPageIndexChange="RegisterGrid_OnPageIndexChange">
                        <Columns>
                            <ext:BoundField ColumnID="Key" DataField="Key" Hidden="true" />
                            <ext:BoundField ColumnID="Value" DataField="Value" HeaderText="优惠券类型" />
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide"  IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
