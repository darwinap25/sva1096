﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Linq;
using System.IO;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Campaign,Edge.SVA.Model.Campaign>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CampaignModifyController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Tools.ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                this.Window1.Title = "新增";
                SVASessionInfo.CampaignModifyController = null;

                controller = SVASessionInfo.CampaignModifyController;
                btnDelete.OnClientClick = this.Grid_CardGradeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                btnDelete1.OnClientClick = this.Grid_CouponTypeList.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete1.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete1.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
            controller = SVASessionInfo.CampaignModifyController;
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Model != null)
                {
                    //存在图片时不需要验证此字段
                    if (!string.IsNullOrEmpty(Model.CampaignPicFile))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }

                    this.uploadFilePath.Text = Model.CampaignPicFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                    controller.LoadViewModel(Model.CampaignID, SVASessionInfo.SiteLanguage);
                    BindingDataGrid_CardGradeList();
                    BindingDataGrid_CouponTypeList();
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");

            //Edge.SVA.Model.Campaign item = this.GetUpdateObject();

            //if (item == null)
            //{
            //    Alert.ShowInTop(Resources.MessageTips.UpdateFailed, MessageBoxIcon.Error);
            //    return;
            //}

            //if (Tools.DALTool.isHasCampaignCode(this.CampaignCode.Text.Trim(), item.CampaignID))
            //{
            //    Alert.ShowInTop(Resources.MessageTips.ExistCampaignCode, MessageBoxIcon.Warning);
            //    return;
            //}

            //item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            //item.UpdatedOn = System.DateTime.Now;
            //item.CampaignCode = item.CampaignCode.ToUpper();
            //if (!string.IsNullOrEmpty(this.picSFile.ShortFileName) && this.FormLoad.Hidden == false)
            //{
            //    item.CampaignPicFile = this.picSFile.SaveToServer("Campaign");
            //}
            //else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
            //{
            //    item.CampaignPicFile = this.uploadFilePath.Text;
            //}
            //if (DALTool.Update<Edge.SVA.BLL.Campaign>(item))
            //{
            //    if (this.FormReLoad.Hidden == true)
            //    {
            //        DeleteFile(this.uploadFilePath.Text);
            //    }

            //    ExecResult er=controller.Submit();
            //    if (er.Success)
            //    {
            //    // 2. 关闭本窗体，然后刷新父窗体
            //    CloseAndPostBack();
            //    } 
            //    else
            //    {
            //        logger.WriteErrorLog("Campaign ", " Modify error", er.Ex);
            //        ShowAddFailed();
            //    }
            //}
            //else
            //{
            //    ShowUpdateFailed();
            //}
            controller.ViewModel.MainTable = this.GetUpdateObject();

            if (controller.ViewModel.MainTable == null)
            {
                Alert.ShowInTop(Resources.MessageTips.UpdateFailed, MessageBoxIcon.Error);
                return;
            }

            if (Tools.DALTool.isHasCampaignCode(this.CampaignCode.Text.Trim(), controller.ViewModel.MainTable.CampaignID))
            {
                Alert.ShowInTop(Resources.MessageTips.ExistCampaignCode, MessageBoxIcon.Warning);
                return;
            }

            controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
            controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
            controller.ViewModel.MainTable.CampaignCode = controller.ViewModel.MainTable.CampaignCode.ToUpper();

            if (!string.IsNullOrEmpty(this.picSFile.ShortFileName) && this.FormLoad.Hidden == false)
            {
                //校验图片类型
                if (!ValidateImg(this.picSFile.FileName))
                {
                    return;
                }
                controller.ViewModel.MainTable.CampaignPicFile = this.picSFile.SaveToServer("Campaign");
            }
            else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                if (!ValidateImg(this.uploadFilePath.Text))
                {
                    return;
                }
                controller.ViewModel.MainTable.CampaignPicFile = this.uploadFilePath.Text;
            }

            ExecResult er = controller.Save();
            if (er.Success)
            {
                if (this.FormReLoad.Hidden == true)
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                // 2. 关闭本窗体，然后刷新父窗体
                CloseAndPostBack();
            }
            else
            {
                logger.WriteErrorLog("Campaign ", " Modify error", er.Ex);
                ShowAddFailed();
            }
        }

        #region CardGrade CouponType
        protected void BrandID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            controller.ViewModel.CardGradeList.Clear();
            controller.ViewModel.CouponTypeList.Clear();
            BindingDataGrid_CardGradeList();
            BindingDataGrid_CouponTypeList();
        }

        protected void btnNew_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference(string.Format("CardGrade/Add.aspx?BrandID={0}&PageFlag=1", this.BrandID.SelectedValue)));
        }
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CardGradeList.SelectedRowIndexArray;
            int count = this.Grid_CardGradeList.PageIndex * this.Grid_CardGradeList.PageSize;
            List<KeyValue> list = new List<KeyValue>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CardGradeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCardGrade(list[i]);
            }
            BindingDataGrid_CardGradeList();
        }
        protected void btnNew1_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference(string.Format("CouponType/Add.aspx?BrandID={0}&PageFlag=1", this.BrandID.SelectedValue)));
        }
        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid_CouponTypeList.SelectedRowIndexArray;
            int count = this.Grid_CouponTypeList.PageIndex * this.Grid_CouponTypeList.PageSize;
            List<KeyValue> list = new List<KeyValue>();
            for (int i = 0; i <= rowIndexs.Length - 1; i++)
            {
                list.Add(controller.ViewModel.CouponTypeList[rowIndexs[i] + count]);
            }
            int len = list.Count;
            for (int i = 0; i < len; i++)
            {
                controller.RemoveCouponType(list[i]);
            }
            BindingDataGrid_CouponTypeList();
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindingDataGrid_CardGradeList();
            BindingDataGrid_CouponTypeList();
        }

        private void BindingDataGrid_CardGradeList()
        {
            this.Grid_CardGradeList.RecordCount = controller.ViewModel.CardGradeList.Count;
            this.Grid_CardGradeList.DataSource = controller.ViewModel.CardGradeList;
            this.Grid_CardGradeList.DataBind();
        }
        private void BindingDataGrid_CouponTypeList()
        {
            this.Grid_CouponTypeList.RecordCount = controller.ViewModel.CouponTypeList.Count;
            this.Grid_CouponTypeList.DataSource = controller.ViewModel.CouponTypeList;
            this.Grid_CouponTypeList.DataBind();
        }
        #endregion

        #region 图片处理
        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.picSFile.ShortFileName))
            {
                this.PicturePath.Text = this.picSFile.SaveToServer("Campaign");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../../../TempImage.aspx?url=" + this.PicturePath.Text, "图片"));
            }
        }
        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }
        #endregion

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
