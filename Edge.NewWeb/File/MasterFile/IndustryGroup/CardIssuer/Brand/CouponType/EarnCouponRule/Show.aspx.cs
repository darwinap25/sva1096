﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                logger.WriteOperationLog(this.PageName, "Show");

                InitData();

            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CardTypeIDLimitView.Text = this.CardTypeIDLimit.SelectedText;
                this.CardGradeIDLimitView.Text = this.CardGradeIDLimit.SelectedText;
                this.MemberBirthdayLimitView.Text = this.MemberBirthdayLimit.SelectedText;
                this.ExchangeTypeView.Text = this.ExchangeType.SelectedText;
                this.ExchangeConsumeRuleOperView.Text = this.ExchangeConsumeRuleOper.SelectedText;
                this.ExchangeCouponTypeIDView.Text = this.ExchangeCouponTypeID.SelectedText;
                this.StatusView.Text = this.Status.SelectedItem == null ? "" : this.Status.SelectedItem.Text;
            }
        }

        protected void InitData()
        {
            ControlTool.BindCardType(CardTypeIDLimit);
            ControlTool.BindCardGrade(CardGradeIDLimit);
            ControlTool.BindKeyValueListNoEmpty(this.MemberBirthdayLimit, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
            ControlTool.BindKeyValueList(this.ExchangeType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ExchangeType), true);
            ControlTool.BindKeyValueList(this.ExchangeConsumeRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ExchangeConsumeRuleOper), true);
            ControlTool.BindCouponType(this.ExchangeCouponTypeID);

            this.CouponType.Text = DALTool.GetCouponTypeText(Convert.ToInt32(Request.Params["CouponTypeID"]));

            string sql = @"select a.BrandID,b.BrandCode from CouponType a join Brand b on a.BrandID=b.BrandID where CouponTypeID = " + Request.Params["CouponTypeID"];
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    string brandcode = ds.Tables[0].Rows[0]["BrandCode"].ToString();
                    int brandid = Convert.ToInt32(ds.Tables[0].Rows[0]["BrandID"]);
                    this.Brand.Text = brandcode + "-" + DALTool.GetBrandName(brandid, null);
                }
            }
        }
    }
}