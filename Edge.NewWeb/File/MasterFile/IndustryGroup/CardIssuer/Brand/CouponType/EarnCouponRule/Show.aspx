﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Label ID="Brand" runat="server" Label="品牌："/>
            <ext:Label ID="CouponType" runat="server" Label="优惠券类型："></ext:Label>
            <ext:Label ID="CardTypeIDLimitView"  runat="server" Label="卡类型：">
            </ext:Label>
            <ext:Label ID="CardGradeIDLimitView"  runat="server" Label="卡级别："></ext:Label>
            <ext:Label ID="MemberBirthdayLimitView"  runat="server" Label="会员范围："></ext:Label>
            <ext:Label ID="ExchangeTypeView"  runat="server" Label="兑换类型：" >
            </ext:Label>
            <ext:Label ID="ExchangeConsumeRuleOperView"  runat="server" Label="计算方式：" ></ext:Label>
            <ext:Label ID="ExchangeConsumeAmount" runat="server" Label="消费金额："/>
            <ext:Label ID="ExchangeAmount" runat="server" Label="所需金额："/>
            <ext:Label ID="ExchangePoint" runat="server" Label="所需积分："/>
            <ext:Label ID="ExchangeCouponTypeIDView"  runat="server" Label="所需优惠券类型："></ext:Label>
            <ext:Label ID="ExchangeCouponCount" runat="server" Label="所需优惠券数量："/>
            <ext:Label ID="StartDate" runat="server" Label="开始日期：">
            </ext:Label>
            <ext:Label ID="EndDate" runat="server" Label="结束日期：">
            </ext:Label>
            <ext:Label ID="StatusView" runat="server" Label="状态：">
            </ext:Label>
            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：">
            </ext:Label>
            <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
            </ext:Label>
            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：">
            </ext:Label>
            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：">
            </ext:Label>
            <ext:DropDownList ID="CardTypeIDLimit"  runat="server" Label="卡类型：" Resizable="true"
                 Hidden="true">
            </ext:DropDownList>
            <ext:DropDownList ID="CardGradeIDLimit"  runat="server" Label="卡级别：" Resizable="true" Hidden="true"></ext:DropDownList>
            <ext:DropDownList ID="MemberBirthdayLimit"  runat="server" Label="会员范围：" Resizable="true" Hidden="true"></ext:DropDownList>
            <ext:DropDownList ID="ExchangeType"  runat="server" Label="兑换类型：" Resizable="true" Required="true" ShowRedStar="true" 
                Hidden="true">
            </ext:DropDownList>
            <ext:DropDownList ID="ExchangeConsumeRuleOper"  runat="server" Label="计算方式：" Resizable="true" Required="true" ShowRedStar="true" 
                Enabled="false" Hidden="true"></ext:DropDownList>
            <ext:DropDownList ID="ExchangeCouponTypeID"  runat="server" Label="所需优惠券类型：" Resizable="true" Enabled="false"
                Required="true" ShowRedStar="true" Hidden="true"></ext:DropDownList>
            <ext:RadioButtonList ID="Status" runat="server"  Label="状态：" Width="200px" Hidden="true">
                <ext:RadioItem Text="有效" Value="1" Selected="True"></ext:RadioItem>
                <ext:RadioItem Text="无效" Value="0"></ext:RadioItem>
            </ext:RadioButtonList>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
