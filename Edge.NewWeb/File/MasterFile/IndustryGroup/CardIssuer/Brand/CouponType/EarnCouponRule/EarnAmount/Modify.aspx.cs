﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmount
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID);
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                string CouponTypeID = Request.Params["CouponTypeID"];
                this.CouponTypeID.SelectedValue = CouponTypeID;
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.EarnCouponRule item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Update<Edge.SVA.BLL.EarnCouponRule>(item))
            {
                SVASessionInfo.Tabs = Add.tab;
                //FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
                //CloseAndPostBack();
                CloseAndRefresh();
            }
            else
            {
                //FineUI.Alert.ShowInTop(Resources.MessageTips.AddFailed, FineUI.MessageBoxIcon.Error);
                ShowUpdateFailed();
            }

        }

    }
}
