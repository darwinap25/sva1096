﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnPoint
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                btnClose.OnClientClick = FineUI.ActiveWindow.GetConfirmHideReference();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                int id = Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"]);
                string couponType = Tools.DALTool.GetCouponTypeText(id);
                this.CouponTypeID.Items.Add(new FineUI.ListItem() { Text = couponType, Value = id.ToString() });
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.EarnCouponRule item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Update<Edge.SVA.BLL.EarnCouponRule>(item))
            {
                SVASessionInfo.Tabs = Add.tab;
                FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
            }
            else
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.AddFailed, FineUI.MessageBoxIcon.Error);
            }

        }


    }
}
