﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Brand.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                        <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSearch" runat="server" Text="搜 索" Icon="Find" OnClick="btnSearch_Click" />
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="BrandCode" runat="server" Label="品牌编号：" ToolTipTitle="品牌编号" ToolTip="Translate__Special_121_Start輸入Brand的编号Translate__Special_121_End"/>
            <ext:TextBox ID="BrandName" runat="server" Label="品牌名：" ToolTipTitle="品牌名称" ToolTip="Translate__Special_121_Start輸入Brand的名稱Translate__Special_121_End"/>
            <ext:NumberBox ID="txtCount" runat="server" Label="查询数量：" ToolTipTitle="查询数量" ToolTip="输入查找的数量，只允許輸入數字"/>
            <ext:Panel ID="panel1" runat="server" Title="搜索结果" AutoScroll="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar2" runat="server">
                        <Items>
                            <ext:CheckBox ID="cbAll" runat="server" Text="全选" AutoPostBack="true" OnCheckedChanged="cbAll_CheckChanged"></ext:CheckBox>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                      <ext:CheckBoxList ID="ckbBrandID" runat="server" ColumnNumber="3" />
                </Items>
            </ext:Panel>
          
        
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
