﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Brand
{
    public partial class Add : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["couponTypeID"] = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());
                ViewState["storeConditionTypeID"] = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());
                ViewState["conditionTypeID"] = 1;

                this.txtCount.Text = webset.ContentPageNum.ToString();

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            for (int i = 0; i < ckbBrandID.Items.Count; i++)
            {
                if (ckbBrandID.Items[i].Selected)
                {
                    int brandID = Edge.Web.Tools.ConvertTool.ToInt(ckbBrandID.Items[i].Value.ToString());

                    Edge.SVA.Model.CouponTypeStoreCondition item = new Edge.SVA.Model.CouponTypeStoreCondition();
                    item.CouponTypeID = Convert.ToInt32(ViewState["couponTypeID"]);
                    item.StoreConditionType = Convert.ToInt32(ViewState["storeConditionTypeID"]);
                    item.ConditionType = Convert.ToInt32(ViewState["conditionTypeID"]);
                    item.ConditionID = brandID;
                    item.CreatedBy = DALTool.GetCurrentUser().UserID;
                    item.CreatedOn = DateTime.Now;


                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.Append(string.Format(" CouponTypeID={0} and StoreConditionType={1} and ConditionType={2} and ConditionID={3}", ViewState["couponTypeID"], ViewState["storeConditionTypeID"], ViewState["conditionTypeID"], brandID));
                    if (new Edge.SVA.BLL.CouponTypeStoreCondition().GetCount(sbWhere.ToString()) <= 0)
                    {
                        DALTool.Add<Edge.SVA.BLL.CouponTypeStoreCondition>(item);
                    }
                }
            }
          
            //FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
            CloseAndRefresh();
        }

 
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Search " + BrandName.Text.Trim() + " Count " + txtCount.Text);
            Edge.Web.Tools.ControlTool.BindBrand(ckbBrandID, BrandName.Text.Trim(), Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));

        }

        protected void cbAll_CheckChanged(object sender, EventArgs e)
        {
            foreach (FineUI.CheckItem cb in this.ckbBrandID.Items) cb.Selected = cbAll.Checked;
        }
   

    }
}
