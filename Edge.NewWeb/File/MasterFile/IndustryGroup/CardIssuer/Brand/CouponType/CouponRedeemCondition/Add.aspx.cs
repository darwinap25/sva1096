﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.CouponRedeemCondition
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponTypeRedeemCondition, Edge.SVA.Model.CouponTypeRedeemCondition>
    {
        public const string tab = "2";
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
            }

        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            Edge.SVA.Model.CouponTypeRedeemCondition item = this.GetAddObject();

            if (item != null)
            {
                item.CouponTypeID = ConvertTool.ToInt(Request.Params["CouponTypeID"]);
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.CouponTypeRedeemCondition>(item) > 0)
            {
                SVASessionInfo.Tabs = Add.tab;
                CloseAndRefresh();
            }
            else
            {
                ShowAddFailed();
            }
        }

        protected void CardTypeID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.CardTypeID.SelectedValue != "-1")
            {
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
            }
        }
    }
}