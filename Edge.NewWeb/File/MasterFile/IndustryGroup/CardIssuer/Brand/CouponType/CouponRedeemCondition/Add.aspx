﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.CouponRedeemCondition.Add" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="添加优惠劵使用条件" AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:DropDownList ID="CardTypeID" runat="server" Label="优惠劵使用指定的会员卡类型：" Resizable="true"
                OnSelectedIndexChanged="CardTypeID_OnSelectedIndexChanged" AutoPostBack="true" />
            <ext:DropDownList ID="CardGradeID" runat="server" Label="优惠劵使用指定的会员卡等级：" Resizable="true"/>
            <ext:DropDownList ID="RedeemLimitType" runat="server" Label="使用优惠劵的条件类型：" Resizable="true">
                <ext:ListItem Text="----------" Value="0" />
                <ext:ListItem Text="整单交易总金额大于等于使用优惠劵的条件值" Value="1" />
                <ext:ListItem Text="会员卡累计消费金额大于等于使用优惠劵的条件值" Value="2" />
                <ext:ListItem Text="会员卡积分大于等于使用优惠劵的条件值" Value="3" />
                <ext:ListItem Text="交易单中，在优惠券绑定列表中指定的消费货品，总金额大于等于使用优惠劵的条件值" Value="4" />
                <ext:ListItem Text="交易单中，在优惠券绑定列表中指定的消费货品，总数量大于等于使用优惠劵的条件值" Value="5" />
            </ext:DropDownList>
            <ext:NumberBox ID="LimitValue" runat="server" Label="使用优惠劵的条件值："></ext:NumberBox>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
