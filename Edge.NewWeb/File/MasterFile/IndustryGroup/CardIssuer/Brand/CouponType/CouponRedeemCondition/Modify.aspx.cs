﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.CouponRedeemCondition
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponTypeRedeemCondition, Edge.SVA.Model.CouponTypeRedeemCondition>
    {
        public const string tab = "2";
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CardTypeID.SelectedValue = Model.CardTypeID.ToString();
                if (this.CardTypeID.SelectedValue != "-1")
                {
                    Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID, ConvertTool.ToInt(this.CardTypeID.SelectedValue));
                    this.CardGradeID.SelectedValue = Model.CardGradeID.ToString();
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.CouponTypeRedeemCondition item = this.GetUpdateObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CouponTypeRedeemCondition>(item))
            {
                SVASessionInfo.Tabs = Add.tab;
                CloseAndRefresh();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        protected void CardTypeID_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.CardTypeID.SelectedValue != "-1")
            {
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID, ConvertTool.ToInt(this.CardTypeID.SelectedValue));
            }
        }
    }
}