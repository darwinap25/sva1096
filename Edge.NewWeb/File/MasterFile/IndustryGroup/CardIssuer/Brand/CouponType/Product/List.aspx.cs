﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Product
{
    public partial class List : PageBase
    {

        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");

                //btnNew.OnClientClick = Window1.GetShowReference(string.Format("add.aspx?CouponTypeID={0}", this.CouponTypeID), "新增");
                btnNew.OnClientClick = Window1.GetShowReference("../../../../../../BasicInformationSetting/Products/List.aspx?multiplepicker=true&addforCouponType=1", "新增");


                btnClose.OnClientClick = FineUI.ActiveWindow.GetHideReference();

                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;


                if (this.CouponTypeID > 0)
                {
                    RptBind(this.StrWhere, "KeyID");
                }

                if (this.Type == 1)
                {
                    this.btnDelete.Hidden = true;
                    this.btnNew.Hidden = true;
                    this.Grid1.EnableCheckBoxSelect = false;
                }

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference(string.Format("Delete.aspx?CouponTypeID={0}&ids={1}", this.CouponTypeID, sb.ToString())));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(this.StrWhere, "KeyID");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);

            List<Edge.SVA.Model.CouponTypeExchangeBinding> list = Tools.SVASessionInfo.TempObject as List<Edge.SVA.Model.CouponTypeExchangeBinding>;
            if (this.CouponTypeID > 0 && list != null)
            {
                foreach (var item in list)
                {
                    item.BindingType = 2;
                    item.CouponTypeID = this.CouponTypeID;
                    Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();

                    if (bllExchangeBinding.GetCount(string.Format("BindingType=2 and BrandID={0} and ProdCode='{1}' and CouponTypeID={2} ", item.BrandID, item.ProdCode, item.CouponTypeID)) <= 0)
                    {
                        bllExchangeBinding.Add(item);
                    }
                }
                Tools.SVASessionInfo.TempObject = null;
            }

            RptBind(this.StrWhere, "KeyID");
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();


            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);
            if (this.Grid1.RecordCount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);
            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        #region 页面属性

        public int CouponTypeID
        {
            get
            {
                return Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());
            }
        }

        public int Type
        {
            get
            {
                return Request.Params["type"] == null ? 1 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["type"].ToString());
            }
        }

        public string StrWhere
        {
            get
            {
                StringBuilder sbWhere = new StringBuilder();
                sbWhere.Append(string.Format(" CouponTypeID={0}", this.CouponTypeID));
                sbWhere.Append(" and BindingType=2 and DepartCode is null ");

                return sbWhere.ToString();
            }
        }

        #endregion
    }
}
