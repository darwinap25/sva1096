﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Store.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:pagemanager id="PageManager1" runat="server" />
    <ext:simpleform id="SimpleForm1" showborder="false" showheader="false" runat="server"
        bodypadding="10px" enablebackgroundcolor="true" title="SimpleForm" autoscroll="true">
        <toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                       <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                    </ext:ToolbarSeparator>
                     <ext:Button ID="Button1" runat="server" Text="搜 索" Icon="Find" OnClick="btnSearch_Click" />
                </Items>
            </ext:Toolbar>
        </toolbars>
        <items>
            <ext:DropDownList ID="BrandID" runat="server" Label="店铺所属品牌列表："  Resizable="true"/>
            <ext:DropDownList ID="StoreTypeID" runat="server" Label="店铺种类："  Resizable="true"/>
            <ext:TextBox ID="StoreName" runat="server" Label="店铺名："  ToolTipTitle="店铺名"  ToolTip="Translate__Special_121_Start輸入Store的名稱Translate__Special_121_End"/>
            <ext:NumberBox ID="txtCount" runat="server" MaxLength="8" Label="查询数量：" ToolTipTitle="查询数量" ToolTip="输入查找的数量，只允許輸入數字"/>
            
            <ext:Panel ID="panel1" runat="server" Title="可选店铺" AutoScroll="true">
                <Toolbars>
                    <ext:Toolbar ID="toolbar2" runat="server">
                        <Items>
                            <ext:CheckBox ID="cbAll" runat="server" Text="全选" AutoPostBack="true" OnCheckedChanged="cbAll_CheckChanged">
                            </ext:CheckBox>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:CheckBoxList ID="ckbStoreID" runat="server" ColumnNumber="3" />
                </Items>
            </ext:Panel>
           
        </items>
    </ext:simpleform>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
