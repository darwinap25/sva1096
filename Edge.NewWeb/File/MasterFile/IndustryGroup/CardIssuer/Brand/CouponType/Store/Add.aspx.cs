﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Store
{
    public partial class Add : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ControlTool.BindStoreType(this.StoreTypeID);
                ControlTool.BindBrand(BrandID);

                ViewState["couponTypeID"] = Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString());
                ViewState["storeConditionTypeID"] = Request.Params["StoreConditionTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["StoreConditionTypeID"].ToString());
                ViewState["conditionTypeID"] = 3;
                this.txtCount.Text = webset.ContentPageNum.ToString();

                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            //  int storeID =Edge.Web.Tools.ConvertTool.ToInt(this.StoreID.SelectedValue);
            logger.WriteOperationLog(this.PageName, "Add");
            for (int i = 0; i < ckbStoreID.Items.Count; i++)
            {
                if (ckbStoreID.Items[i].Selected)
                {
                    int storeID = Edge.Web.Tools.ConvertTool.ToInt(ckbStoreID.Items[i].Value.ToString());

                    Edge.SVA.Model.CouponTypeStoreCondition item = new Edge.SVA.Model.CouponTypeStoreCondition();
                    item.CouponTypeID = Convert.ToInt32(ViewState["couponTypeID"]);
                    item.StoreConditionType = Convert.ToInt32(ViewState["storeConditionTypeID"]);
                    item.ConditionType = Convert.ToInt32(ViewState["conditionTypeID"]);
                    item.ConditionID = storeID;
                    item.CreatedBy = DALTool.GetCurrentUser().UserID;
                    item.CreatedOn = DateTime.Now;


                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.Append(string.Format(" CouponTypeID={0} and StoreConditionType={1} and ConditionType={2} and ConditionID={3}", ViewState["couponTypeID"], ViewState["storeConditionTypeID"], 3, storeID));
                    if (new Edge.SVA.BLL.CouponTypeStoreCondition().GetCount(sbWhere.ToString()) <= 0)
                    {
                        DALTool.Add<Edge.SVA.BLL.CouponTypeStoreCondition>(item);
                    }
                }
            }
            //FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
            CloseAndRefresh();
        }

 

        protected void cbAll_CheckChanged(object sender, EventArgs e)
        {
            foreach (FineUI.CheckItem cb in this.ckbStoreID.Items) cb.Selected = cbAll.Checked;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
            Edge.Web.Tools.ControlTool.BindStore(this.ckbStoreID, ConvertTool.ToInt(this.StoreTypeID.SelectedValue), ConvertTool.ToInt(this.BrandID.SelectedValue), StoreName.Text.Trim(), Edge.Web.Tools.ConvertTool.ToInt(txtCount.Text));

        }


        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("List.aspx?page=0&CouponTypeID={0}&StoreConditionTypeID={1}&type=2", ViewState["couponTypeID"], ViewState["storeConditionTypeID"]));
        }

    }
}
