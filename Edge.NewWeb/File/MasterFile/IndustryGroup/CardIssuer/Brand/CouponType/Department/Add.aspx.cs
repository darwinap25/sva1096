﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Department
{
    public partial class Add : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                RegisterCloseEvent(btnClose);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {

            logger.WriteOperationLog(this.PageName, "Add couponTypeID " + this.CouponTypeID);
            if (this.CouponTypeID > 0)
            {

                Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                model.BindingType = 2;
                model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                model.DepartCode = DepartCode.Text.Trim();
                model.CouponTypeID = this.CouponTypeID;

                Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();

                if (bllExchangeBinding.GetCount(string.Format("BindingType=2 and BrandID={0} and DepartCode='{1}' and CouponTypeID={2} ", Tools.ConvertTool.ToInt(BrandID.SelectedValue), DepartCode.Text.Trim(), this.CouponTypeID)) > 0)
                {
                    
                    //FineUI.Alert.ShowInTop(Resources.MessageTips.Exists, FineUI.MessageBoxIcon.Warning);
                    ShowWarning(Resources.MessageTips.Exists);
                    return;
                }


                if (bllExchangeBinding.Add(model) > 0)
                {
                    //FineUI.PageContext.RegisterStartupScript(FineUI.ActiveWindow.GetHideRefreshReference());
                    CloseAndRefresh();
                }
                else
                {
       
                    //FineUI.Alert.ShowInTop(Resources.MessageTips.AddFailed, FineUI.MessageBoxIcon.Warning);
                    ShowAddFailed();
                }
            }
            else
            {
                //FineUI.Alert.ShowInTop(Resources.MessageTips.AddFailed, FineUI.MessageBoxIcon.Warning);
                ShowAddFailed();
            }
        }



        protected int CouponTypeID
        {
            get { return Request.Params["CouponTypeID"] == null ? 0 : Edge.Web.Tools.ConvertTool.ToInt(Request.Params["CouponTypeID"].ToString()); }
        }

    }
}
