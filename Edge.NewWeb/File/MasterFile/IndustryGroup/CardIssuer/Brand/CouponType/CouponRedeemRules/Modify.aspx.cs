﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using FineUI;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.CouponRedeemRules
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);

                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindKeyValueListNoEmpty(this.MemberDateType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberRange));
                ControlTool.BindKeyValueList(this.PointRuleType, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.TransType),true);
                ControlTool.BindKeyValueList(this.PointRuleOper, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.ComputTypePoint),true);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.EarnCouponRule item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Update<Edge.SVA.BLL.EarnCouponRule>(item))
            {
                SVASessionInfo.Tabs = Add.tab;
                //CloseAndPostBack();
                CloseAndRefresh();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Modify.aspx?id=" + this.CardGradeID.SelectedValue + "&tabs="+Add.tab);
        }
    }
}
