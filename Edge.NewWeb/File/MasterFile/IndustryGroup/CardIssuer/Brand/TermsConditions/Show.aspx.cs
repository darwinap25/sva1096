﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.TermsConditions
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberClause, Edge.SVA.Model.MemberClause>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                btnClose.OnClientClick = ActiveWindow.GetConfirmHidePostBackReference();
            }
            logger.WriteOperationLog(this.PageName, "Show");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }


                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                string brandText = brand == null ? "" : DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                this.BrandCode.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);

                //this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);


                //this.CallInterfaceView.Text = this.CallInterface.SelectedItem == null ? "" : this.CallInterface.SelectedItem.Text;
                //this.CallInterface.Hidden = true;
                //if (Model != null)
                //{
                //    this.uploadFilePath.Text = Model.StoreAttributePic;

                //    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                //    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.StoreAttributePic) ? true : false;//没有照片时不显示查看按钮(Len)
                //}
            }
        }
    }
}