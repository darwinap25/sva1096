﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.TermsConditions.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:DropDownList ID="BrandID" runat="server" Label="品牌：" Required="true" ShowRedStar="true"
                Resizable="true" CompareType="String" CompareValue="-1" CompareOperator="NotEqual"
                CompareMessage="请选择有效值">
            </ext:DropDownList>
            <ext:TextBox ID="ClauseTypeCode" runat="server" Label="条款和条件编号：" MaxLength="20" Required="true"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="Translate__Special_121_Start 只能输入字母和数字 Translate__Special_121_End" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="ClauseName" runat="server" Label="Translate__Special_121_Start 条款和条件1：Translate__Special_121_End" MaxLength="512" Required="true"
                ShowRedStar="true" ToolTipTitle="Translate__Special_121_Start 条款和条件 Translate__Special_121_End" ToolTip="不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="ClauseName2" runat="server" Label="Translate__Special_121_Start 条款和条件2：Translate__Special_121_End" MaxLength="512" ToolTipTitle="Translate__Special_121_Start 条款和条件2 Translate__Special_121_End"
                ToolTip="对条款和条件2,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="ClauseName3" runat="server" Label="Translate__Special_121_Start 条款和条件3：Translate__Special_121_End" MaxLength="512" ToolTipTitle="Translate__Special_121_Start 条款和条件3 Translate__Special_121_End"
                ToolTip="对条款和条件3,不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:HtmlEditor ID="MemberClauseDesc1" runat="server" Height="200" Label="Translate__Special_121_Start 条款和条件描述 1：Translate__Special_121_End"
                ShowRedStar="true">
            </ext:HtmlEditor>
            <ext:HtmlEditor ID="MemberClauseDesc2" runat="server" Height="200" Label="Translate__Special_121_Start 条款和条件描述 2：Translate__Special_121_End" >
            </ext:HtmlEditor>
            <ext:HtmlEditor ID="MemberClauseDesc3" runat="server" Height="200" Label="Translate__Special_121_Start 条款和条件描述 3：Translate__Special_121_End" >
            </ext:HtmlEditor>
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项" CssStyle="font-size:12px;color:red">
            </ext:Label>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <ext:Window ID="WindowPic1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank"
        EnableMaximize="false" EnableResize="true" Target="Top" IsModal="True" Width="750px"
        Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
