﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" ValidateRequest="false" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.TermsConditions.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Form ID="Form3" EnableBackgroundColor="true" AutoWidth="true" ShowHeader="false"
                ShowBorder="false" runat="server" HideMode="Display" Hidden="false">
                <Rows>
                    <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="BrandCode" runat="server" Label="品牌：">
                            </ext:Label>
                            <ext:Label ID="ClauseTypeCode" runat="server" Label="Translate__Special_121_Start 条款和条件编号：Translate__Special_121_End">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="50% 50%">
                        <Items>
                            <ext:Label ID="ClauseName" runat="server" Label="Translate__Special_121_Start 条款和条件1：Translate__Special_121_End">
                            </ext:Label>
                            <ext:Label ID="ClauseName2" runat="server" Label="Translate__Special_121_Start 条款和条件2： Translate__Special_121_End">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow3" runat="server">
                        <Items>
                            <ext:Label ID="ClauseName3" runat="server" Label="Translate__Special_121_Start 条款和条件3：Translate__Special_121_End">
                            </ext:Label>
                            <ext:Label ID="MemberClauseDesc1" runat="server" Label="Translate__Special_121_Start 条款和条件描述1：Translate__Special_121_End" EncodeText="false">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow ID="FormRow4" runat="server">
                        <Items>
                            <ext:Label ID="MemberClauseDesc2" runat="server" Label="Translate__Special_121_Start 条款和条件描述2：Translate__Special_121_End" EncodeText="false">
                            </ext:Label>
                            <ext:Label ID="MemberClauseDesc3" runat="server" Label="Translate__Special_121_Start 条款和条件描述3：Translate__Special_121_End" EncodeText="false">
                            </ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
        </Items>
    </ext:SimpleForm>
    <ext:Window ID="WindowPic" Title="图片" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="450px">
    </ext:Window>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
