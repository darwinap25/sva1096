﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Brand, Edge.SVA.Model.Brand>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                Edge.Web.Tools.ControlTool.BindCurrency(this.DomesticCurrencyID);

                RegisterCloseEvent(btnClose);
            }
            logger.WriteOperationLog(this.PageName, "Show");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CardIssuerID.Text = DALTool.GetCardIssuerName(Model.CardIssuerID.GetValueOrDefault(), null);
                this.IndustryID.Text =  DALTool.GetIndustryName(Model.IndustryID.GetValueOrDefault(), null);

                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                this.DomesticCurrencyIDView.Text = this.DomesticCurrencyID.SelectedText;

                if (Model != null)
                {
                    this.uploadFilePath.Text = Model.BrandPicSFile;
                    this.uploadFilePath1.Text = Model.BrandPicMFile;
                    this.uploadFilePath2.Text = Model.BrandPicGFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                    this.btnPreview1.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath1.Text, "图片");
                    this.btnPreview2.OnClientClick = WindowPic.GetShowReference("../../../../../TempImage.aspx?url=" + this.uploadFilePath2.Text, "图片");

                    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.BrandPicSFile) ? true : false;//没有照片时不显示查看按钮(Len)
                    this.btnPreview1.Hidden = string.IsNullOrEmpty(Model.BrandPicMFile) ? true : false;//没有照片时不显示查看按钮(Len)
                    this.btnPreview2.Hidden = string.IsNullOrEmpty(Model.BrandPicGFile) ? true : false;//没有照片时不显示查看按钮(Len)

                }
            }
        }
    }
}
