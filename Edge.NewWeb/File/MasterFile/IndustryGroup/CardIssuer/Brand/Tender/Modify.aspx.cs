﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Linq;
using System.IO;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Tender
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.TENDER, Edge.SVA.Model.TENDER>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        TenderController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);
                SVASessionInfo.TenderController = null;
            }
            controller = SVASessionInfo.TenderController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                if (brand != null)
                {
                    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                    //this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                }
                else
                {
                   // this.lblCardIssuer.Text = "";
                }
                //if (Model != null)
                //{
                //    StoreCountry.SelectedValue = Model.StoreCountry == null ? "-1" : Model.StoreCountry.ToString().Trim();
                //    InitProvinceByCountry();
                //    StoreProvince.SelectedValue = Model.StoreProvince == null ? "-1" : Model.StoreProvince.ToString().Trim();
                //    InitCityByProvince();
                //    StoreCity.SelectedValue = Model.StoreCity == null ? "-1" : Model.StoreCity.ToString().Trim();
                //    InitDistrictByCity();
                //    StoreDistrict.SelectedValue = Model.StoreDistrict == null ? "-1" : Model.StoreDistrict.ToString().Trim();

                //    AppendFullAddress();


                //    //存在图片时不需要验证此字段
                //    if (!string.IsNullOrEmpty(Model.StorePicFile))
                //    {
                //        this.FormLoad.Hidden = true;
                //        this.FormReLoad.Hidden = false;
                //        this.btnBack.Hidden = false;
                //    }
                //    else
                //    {
                //        this.FormLoad.Hidden = false;
                //        this.FormReLoad.Hidden = true;
                //        this.btnBack.Hidden = true;
                //    }

                //    this.uploadFilePath.Text = Model.StorePicFile;

                //    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                //}
                controller.LoadViewModel(Model.TenderID);
                if (controller.ViewModel.MainTable != null)
                {
                   //// StoreCountry.SelectedValue = controller.ViewModel.MainTable.StoreCountry == null ? "-1" : controller.ViewModel.MainTable.StoreCountry.ToString().Trim();
                   // InitProvinceByCountry();
                   // StoreProvince.SelectedValue = controller.ViewModel.MainTable.StoreProvince == null ? "-1" : controller.ViewModel.MainTable.StoreProvince.ToString().Trim();
                   // InitCityByProvince();
                    ////StoreCity.SelectedValue = controller.ViewModel.MainTable.StoreCity == null ? "-1" : controller.ViewModel.MainTable.StoreCity.ToString().Trim();
                    //InitDistrictByCity();
                   // StoreDistrict.SelectedValue = controller.ViewModel.MainTable.StoreDistrict == null ? "-1" : controller.ViewModel.MainTable.StoreDistrict.ToString().Trim();

                   // AppendFullAddress();


                    //存在图片时不需要验证此字段
                    //if (!string.IsNullOrEmpty(controller.ViewModel.MainTable.StorePicFile))
                    //{
                    //    this.FormLoad.Hidden = true;
                    //    this.FormReLoad.Hidden = false;
                    //    this.btnBack.Hidden = false;
                    //}
                    //else
                    //{
                    //    this.FormLoad.Hidden = false;
                    //    this.FormReLoad.Hidden = true;
                    //    this.btnBack.Hidden = true;
                    //}

                    //this.uploadFilePath.Text = controller.ViewModel.MainTable.StorePicFile;

                    //this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            controller.ViewModel.MainTable = this.GetUpdateObject();

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.TenderCode = controller.ViewModel.MainTable.TenderCode.ToUpper();
                //if (!string.IsNullOrEmpty(this.StorePicFile.ShortFileName) && this.FormLoad.Hidden == false)
                //{
                //    if (!ValidateImg(this.StorePicFile.FileName))
                //    {
                //        return;
                //    }
                //    controller.ViewModel.MainTable.StorePicFile = this.StorePicFile.SaveToServer("Images/Store");
                //}
                //else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                //{
                //    if (!ValidateImg(this.uploadFilePath.Text))
                //    {
                //        return;
                //    }
                //    controller.ViewModel.MainTable.StorePicFile = this.uploadFilePath.Text;
                //}
            }
            string message = controller.ValidataObject(this.TenderCode.Text.Trim(), Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()));
            if (!string.IsNullOrEmpty(message))
            {
                ShowWarning(message);
                return;
            }
            ExecResult er = controller.Update();
            if (er.Success)
            {
                //if (this.FormReLoad.Hidden == true)
                //{
                //    DeleteFile(this.uploadFilePath.Text);
                //}
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Tender Add\t Code:" + controller.ViewModel.MainTable.TenderCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Tender Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.TenderCode);
                ShowAddFailed();
            }
        }
    }
}
