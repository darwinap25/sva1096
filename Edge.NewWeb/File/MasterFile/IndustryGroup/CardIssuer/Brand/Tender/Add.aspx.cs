﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model.Domain;
using Edge.SVA.BLL.Domain.DataResources;
using System.Text;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;
using System.Linq;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Tender
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.TENDER, Edge.SVA.Model.TENDER>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        TenderController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                //ControlTool.BindStoreType(this.StoreTypeID);
                //Edge.Web.Tools.ControlTool.BindStoreGroup(StoreGroupID);
                //if (SVASessionInfo.CurrentUser.UserName.Equals(ConstParam.SystemAdminName))
                //{
                //    ControlTool.BindBrandOfMasterStore(BrandID);
                //}
                //else
                //{
                    ControlTool.BindBrand(BrandID);
                //}

                //地址
                //ControlTool.BindCountry(StoreCountry);
                //this.StoreProvince.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                //this.StoreCity.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                //this.StoreDistrict.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });

                //this.lblCardIssuer.Text = DALTool.GetCardIssuerName();
                SVASessionInfo.TenderController = null;
            }
            controller = SVASessionInfo.TenderController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            string message = controller.ValidataObject(this.TenderCode.Text.Trim(), Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()));
            if (!string.IsNullOrEmpty(message))
            {
                ShowWarning(message);
                return;
            }
            controller.ViewModel.MainTable = this.GetAddObject();
            if (controller.ViewModel.MainTable != null)
            {
                //if (!ValidateImg(this.StorePicFile.FileName))
                //{
                //    return;
                //}

                controller.ViewModel.MainTable.CreatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainTable.TenderCode = controller.ViewModel.MainTable.TenderCode.ToUpper();
               // controller.ViewModel.MainTable.StorePicFile = this.StorePicFile.SaveToServer("Images/Store");
            }
            ExecResult er = controller.Add();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Tender Add\t Code:" + controller.ViewModel.MainTable.TenderCode);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Tender Add\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.TenderCode);
                ShowAddFailed();
            }
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ControlTool.AddTitle(this.BrandID);

            Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()));
            if (brand != null)
            {
                Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
               // this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
            }
            else
            {
                //this.lblCardIssuer.Text = "";
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
           // DeleteFile(this.uploadFilePath.Text);
        }

    
    }
}
