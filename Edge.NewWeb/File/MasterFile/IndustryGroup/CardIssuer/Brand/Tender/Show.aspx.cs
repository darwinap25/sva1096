﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.File.MasterFile.Location.Store;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Tender
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.TENDER, Edge.SVA.Model.TENDER>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        TenderController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Show");
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.TenderController = null;

               // ControlTool.BindStoreGroup(this.StoreGroupID);
            }
            controller = SVASessionInfo.TenderController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                //Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                //if (brand != null)
                //{
                //    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                //    this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                //    this.BrandID.Text = brand.BrandCode + "-" + DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                //}
                //else
                //{
                //    this.lblCardIssuer.Text = "";
                //}

                //Edge.SVA.Model.StoreType storeType = new Edge.SVA.BLL.StoreType().GetModel(Model.StoreTypeID.GetValueOrDefault());
                //string text = storeType == null ? "" : DALTool.GetStringByCulture(storeType.StoreTypeName1, storeType.StoreTypeName2, storeType.StoreTypeName3);
                //this.StoreTypeID.Text = storeType == null ? "" : ControlTool.GetDropdownListText(text, storeType.StoreTypeCode);

                //if (Model != null)
                //{
                //    //地址
                //    Edge.SVA.Model.Country country = new Edge.SVA.BLL.Country().GetModel(Model.StoreCountry);
                //    this.StoreCountry.Text = country == null ? "" : DALTool.GetStringByCulture(country.CountryName1, country.CountryName2, country.CountryName3);
                //    Edge.SVA.Model.Province province = new Edge.SVA.BLL.Province().GetModel(Model.StoreProvince);
                //    this.StoreProvince.Text = province == null ? "" : DALTool.GetStringByCulture(province.ProvinceName1, province.ProvinceName2, province.ProvinceName3);
                //    Edge.SVA.Model.City city = new Edge.SVA.BLL.City().GetModel(Model.StoreCity);
                //    this.StoreCity.Text = city == null ? "" : DALTool.GetStringByCulture(city.CityName1, city.CityName2, city.CityName3);
                //    Edge.SVA.Model.District district = new Edge.SVA.BLL.District().GetModel(Model.StoreDistrict);
                //    this.StoreDistrict.Text = district == null ? "" : DALTool.GetStringByCulture(district.DistrictName1, district.DistrictName2, district.DistrictName3);
                    
                    
                //    this.uploadFilePath.Text = Model.StorePicFile;

                //    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");

                //    this.btnPreview.Hidden = string.IsNullOrEmpty(Model.StorePicFile) ? true : false;//没有照片时不显示查看按钮(Len)
                //}
                controller.LoadViewModel(Model.TenderID);
                //this.CreatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                //this.UpdatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainTable.UpdatedBy.GetValueOrDefault());

                //this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.CreatedOn);
                //this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainTable.UpdatedOn);

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(controller.ViewModel.MainTable.BrandID.GetValueOrDefault());
                if (brand != null)
                {
                    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                  // this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                    this.BrandID.Text = brand.BrandCode + "-" + DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                }
                else
                {
                   //this.lblCardIssuer.Text = "";
                }
            }
        }
    }
}
