﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Tender.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
        LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:TextBox ID="TenderCode" runat="server" Label="投标编号：" MaxLength="20" Required="true"
                ShowRedStar="true" RegexPattern="ALPHA_NUMERIC" RegexMessage="只能输入字母和数字" OnTextChanged="ConvertTextboxToUpperText"
                AutoPostBack="true" ToolTipTitle="投标编号" ToolTip="Translate__Special_121_Start1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End">
            </ext:TextBox>
            <ext:TextBox ID="TenderName1" runat="server" Label="描述：" MaxLength="512" Required="true"
                ShowRedStar="true" ToolTipTitle="描述" ToolTip="不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="TenderName2" runat="server" Label="其他描述1：" MaxLength="512" ToolTipTitle="其他描述1"
                ToolTip="對描述的一個補充。不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
            <ext:TextBox ID="TenderName3" runat="server" Label="其他描述2：" MaxLength="512" ToolTipTitle="其他描述2"
                ToolTip="對描述的另一個補充。不能超過512個字符" OnTextChanged="ConvertTextboxToUpperText" AutoPostBack="true">
            </ext:TextBox>
          <%--  <ext:TextBox ID="lblCardIssuer" runat="server" Label="发行商：" MaxLength="512" Required="true"
                ShowRedStar="true" Enabled="false">
            </ext:TextBox>--%>
            <ext:DropDownList ID="BrandID" runat="server" Label="投标所属品牌列表：" Required="true" ShowRedStar="true"
                OnSelectedIndexChanged="BrandID_SelectedIndexChanged" Resizable="true" CompareType="String"
                CompareValue="-1" CompareOperator="NotEqual" CompareMessage="请选择有效值">
            </ext:DropDownList>
<%--            <ext:TimePicker ID="StoreOpenTime" runat="server" Label="营业开始时间：" Text="" Increment="120">
            </ext:TimePicker>
            <ext:TimePicker ID="StoreCloseTime" runat="server" Label="营业结束时间：" Text="" Increment="120"
                CompareControl="StoreOpenTime" CompareOperator="GreaterThanEqual" CompareMessage="营业结束时间应该大于等于营业开始时间">
            </ext:TimePicker>--%>
          
            <ext:Label ID="lblDesc" runat="server" Text="*为必填项"  CssStyle="font-size:12px;color:red"></ext:Label>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
