﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Edge.Web.Tools;
using Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand;


namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.Tender
{
    public partial class List : PageBase
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        Tools.Logger logger = Tools.Logger.Instance;
        TenderController controller;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                logger.WriteOperationLog(this.PageName, "List");

                RptBind("TenderID>0", "TenderCode");
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                btnImport.OnClientClick = Window3.GetShowReference("~/PublicForms/ImportForm.aspx?Menu=TENDER", "Import");

                ControlTool.BindBrand(this.Brand);

                SVASessionInfo.ImportTenderPath = null;
                SVASessionInfo.TenderController = null;

                if (Request["multiplepicker"] != null)
                {
                    //this.Grid1.EnableRowDoubleClick = true;
                    this.Grid1.EnableCheckBoxSelect = true;
                    this.btnAddPicker.Hidden = false;
                    this.btnNew.Hidden = true;
                    this.btnDelete.Hidden = true;
                    btnImport.Hidden = true;
                    btnExport.Hidden = true;
                    FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;
                    editWF.Hidden = true;
                }
            }
            controller = SVASessionInfo.TenderController;
        }


        #region Event
        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);
                    int brandid = this.Brand.SelectedValue == "-1" ? -1 : Convert.ToInt32(this.Brand.SelectedValue);
                    string code = this.Code.Text.Trim();
                    string desc = this.Desc.Text.Trim();
                    if (brandid > 0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" BrandID =");
                        sb.Append(brandid);
                        sb.Append("");
                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" TenderCode like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(desc))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "TenderName1";

                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(desc);
                        sb.Append("%'");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                Edge.SVA.BLL.TENDER bll = new Edge.SVA.BLL.TENDER();

                //获得总条数
                this.Grid1.RecordCount = bll.GetRecordCount(strWhere);
                if (this.Grid1.RecordCount > 0)
                {
                    this.btnDelete.Enabled = true;
                }
                else
                {
                    this.btnDelete.Enabled = false;
                }

                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);
                //Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                //Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
                //Tools.DataTool.AddColumn(ds, "CardIssuerName", Tools.DALTool.GetCardIssuerName());
                //this.Grid1.DataSource = ds.Tables[0].DefaultView;
                //this.Grid1.DataBind();

                TenderController controller = new TenderController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("Tender", "Load Filed", ex);
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            TenderController controller = new TenderController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            //ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("TenderID>0", "TenderCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);

            #region 处理导入数据
            if (!string.IsNullOrEmpty(SVASessionInfo.ImportTenderPath))
            {
                DataTable dt = ExcelTool.GetFirstSheet(SVASessionInfo.ImportTenderPath);
                if (controller.CheckData(dt))
                {
                    controller.AnalysisData(dt);
                    controller.Operation();
                }
                SVASessionInfo.MessageHTML = controller.GetHtml(DateTime.Now).ToString();
                FineUI.PageContext.RegisterStartupScript(Window4.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));
                SVASessionInfo.ImportTenderPath = null;
            }
            RptBind("TENDERID>0", "TenderCode");

            #endregion
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("TenderID>0", "TenderCode");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DataTable dt = controller.GetExportData();
            if (dt == null || dt.Rows.Count == 0)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            DateTime start = DateTime.Now;
            string fileName = controller.UpLoadFileToServer(dt);
            int records = 0;

            try
            {
                string exportname = "ExportTenderInfo.xls";

                Tools.ExportTool.ExportFile(fileName, exportname);

                Tools.Logger.Instance.WriteExportLog("Batch Export Tender", exportname, start, records, null);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Logger.Instance.WriteExportLog("Batch Export Tender", fn, start, records, ex.Message);
                ShowWarning(ex.Message);
            }
        }
        protected void btnAddPicker_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][2].ToString());
                sb.Append(",");
            }
            Tools.SVASessionInfo.BuyingTendCode = sb.ToString().TrimEnd(',');
            CloseAndPostBack();
        }
    }
}
