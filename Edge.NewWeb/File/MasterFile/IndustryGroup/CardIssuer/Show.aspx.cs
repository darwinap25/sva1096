﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using FineUI;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardIssuer, Edge.SVA.Model.CardIssuer>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                btnClose.OnClientClick = ActiveWindow.GetConfirmHidePostBackReference();

                Edge.Web.Tools.ControlTool.BindCurrency(this.DomesticCurrencyID);

               // Edge.Web.Tools.ControlTool.BindDataSet(IndustryID, new Edge.SVA.BLL.Industry().GetAllList(), "IndustryID", "IndustryName1", "IndustryName2", "IndustryName3");
            }
            logger.WriteOperationLog(this.PageName, "Show");
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                this.DomesticCurrencyIDView.Text = this.DomesticCurrencyID.SelectedText;
                
            }
        }

    }
}
