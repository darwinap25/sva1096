﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Education
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Education,Edge.SVA.Model.Education>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                logger.WriteOperationLog(this.PageName, " Modify");
                RegisterCloseEvent(btnClose);
            }
        }


        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);
            Edge.SVA.Model.Education item = this.GetUpdateObject();

            if (item == null)
            {
                this.ShowUpdateFailed();
                return;
            }

            if (Tools.DALTool.isHasEducationCode(this.EducationCode.Text.Trim(), item.EducationID))
            {
                this.ShowWarning(Resources.MessageTips.ExistEducationCode);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.EducationCode = item.EducationCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Education>(item))
            {
                this.CloseAndPostBack();
            }
            else
            {
                this.ShowUpdateFailed();
            }
        }
    }
}
