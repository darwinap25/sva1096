﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MemberMasterFile.Profession.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true" LabelAlign="Right">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Label ID="ProfessionCode" runat="server" Label="专业编号：">
            </ext:Label>
            <ext:Label ID="ProfessionName1" runat="server" Label="描述：">
            </ext:Label>
            <ext:Label ID="ProfessionName2" runat="server" Label="其他描述1：">
            </ext:Label>
            <ext:Label ID="ProfessionName3" runat="server" Label="其他描述2：">
            </ext:Label>
            <ext:Label ID="CreatedOn" runat="server" Label="创建时间：" />
            <ext:Label ID="CreatedBy" runat="server" Label="创建人：" />
            <ext:Label ID="UpdatedOn" runat="server" Label="上次修改时间：" />
            <ext:Label ID="UpdatedBy" runat="server" Label="上次修改人：" />
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
