﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Profession
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Profession ,Edge.SVA.Model.Profession>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        static Edge.SVA.Model.Profession curItem = new Edge.SVA.Model.Profession();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }


        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
         
            Edge.SVA.Model.Profession item = this.GetUpdateObject();

            if (Tools.DALTool.isHasProfessionCode(this.ProfessionCode.Text.Trim(), item.ProfessionID))
            {
                this.ShowWarning(Resources.MessageTips.ExistProfessionCode);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.ProfessionCode = item.ProfessionCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Profession>(item))
            {
                this.CloseAndPostBack();
            }
            else
            {
                this.ShowUpdateFailed();
            }
        }
    }
}
