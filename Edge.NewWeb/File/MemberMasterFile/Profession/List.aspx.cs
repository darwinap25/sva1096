﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace Edge.Web.File.MemberMasterFile.Profession
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                RptBind("", "ProfessionCode");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

            }
        }



        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {
            #region for search
            if (SearchFlag.Text == "1")
            {
                StringBuilder sb = new StringBuilder(strWhere);

                string code = this.Code.Text.Trim();
                string desc = this.Desc.Text.Trim();
                if (!string.IsNullOrEmpty(code))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    sb.Append(" ProfessionCode like '%");
                    sb.Append(code);
                    sb.Append("%'");
                }
                if (!string.IsNullOrEmpty(desc))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }
                    string descLan = "ProfessionName1";
                    sb.Append(descLan);
                    sb.Append(" like '%");
                    sb.Append(desc);
                    sb.Append("%'");
                }
                strWhere = sb.ToString();
            }
            #endregion


            Edge.SVA.BLL.Profession bll = new Edge.SVA.BLL.Profession();

            //获得总条数
            Grid1.RecordCount = bll.GetCount(strWhere);
            if (Grid1.RecordCount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);
            Tools.DataTool.AddProfessionName(ds, "ProfessionName", "ProfessionID");
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            ExecuteJS(HiddenWindowForm.GetShowReference("Delete.aspx?ids=" + sb.ToString().TrimEnd(',')));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "ProfessionCode");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "ProfessionCode");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "ProfessionCode");
        }
    }
}
