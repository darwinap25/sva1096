﻿<%@ Page Language="c#" CodeBehind="Add.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Add</title>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />
    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="20px" EnableBackgroundColor="true" Title="SimpleForm" LabelAlign="Right"
        AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:TextBox ID="txtUserName" runat="server" Label="用户名：" Required="true" ShowRedStar="true">
                    </ext:TextBox>
                    <ext:TextBox ID="txtPassword" runat="server" Label="密码：" TextMode="Password" Required="true"
                        ShowRedStar="true">
                    </ext:TextBox>
                    <ext:TextBox ID="txtPassword1" runat="server" Label="密码验证：" TextMode="Password" Required="true"
                        ShowRedStar="true" CompareControl="txtPassword">
                    </ext:TextBox>
                    <ext:TextBox ID="txtTrueName" runat="server" Label="真实姓名：">
                    </ext:TextBox>
                    <ext:TextBox ID="Description" runat="server" Label="描述：">
                    </ext:TextBox>
                    <ext:RadioButtonList ID="rdblSex" runat="server" Label="性别：" Width="200px">
                        <ext:RadioItem Text="男" Value="1" Selected="true" />
                        <ext:RadioItem Text="女" Value="0" />
                    </ext:RadioButtonList>
                    <ext:TextBox ID="txtPhone" runat="server" Label="联系电话：">
                    </ext:TextBox>
                    <ext:TextBox ID="txtEmail" runat="server" Label="电子邮箱：">
                    </ext:TextBox>
                    <ext:CheckBoxList ID="chblRoles" runat="server" Label="角色分配：" ColumnNumber="3">
                    </ext:CheckBoxList>
                    <ext:RadioButtonList ID="Style" runat="server" Label="用户类别：" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="Style_SelectedIndexChanged">
                        <ext:RadioItem Text="品牌用户" Value="1" Selected="true" />
                        <ext:RadioItem Text="店铺用户" Value="0" />
                    </ext:RadioButtonList>
                    <ext:Form ID="FormBrand" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        runat="server" BodyPadding="10px">
                        <Rows>
                            <ext:FormRow ID="FormRow1" ColumnWidths="20% 80%" runat="server">
                                <Items>
                                    <ext:Label ID="Label1" runat="server" Label="" Hidden="true" HideMode="Visibility">
                                    </ext:Label>
                                    <ext:Tree runat="server" ID="BrandTree" Title="品牌列表">
                                        <Toolbars>
                                            <ext:Toolbar ID="Toolbar2" runat="server">
                                                <Items>
                                                    <ext:CheckBox ID="CheakAllBrands" Text="全选" AutoPostBack="true" runat="server" OnCheckedChanged="CheakAllBrands_CheckedChanged">
                                                    </ext:CheckBox>
                                                </Items>
                                            </ext:Toolbar>
                                        </Toolbars>
                                    </ext:Tree>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="FormBrandStore" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        runat="server" BodyPadding="10px" Hidden="true">
                        <Rows>
                            <ext:FormRow ID="FormRow2" ColumnWidths="20% 80%" runat="server">
                                <Items>
                                    <ext:Label ID="Label2" runat="server" Label="" Hidden="true" HideMode="Visibility">
                                    </ext:Label>
                                    <ext:Tree runat="server" ID="StoreTree" OnNodeCheck="StoreTree_NodeCheck" Title="品牌-店铺列表">
                                        <Toolbars>
                                            <ext:Toolbar ID="Toolbar3" runat="server">
                                                <Items>
                                                    <ext:CheckBox ID="CheckAll" Text="全选" AutoPostBack="true" runat="server" OnCheckedChanged="CheckAll_CheckedChanged">
                                                    </ext:CheckBox>
                                                </Items>
                                            </ext:Toolbar>
                                        </Toolbars>
                                    </ext:Tree>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:SimpleForm>
        </Items>
    </ext:SimpleForm>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
