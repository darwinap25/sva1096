﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Controllers;
using System.Data;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.SysManage.CouponAutoPickingRule
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");
                ControlTool.BindBrand(BrandID);
                ControlTool.BindStore(StoreID,"2");
                RptBind("");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;
            }
        }

        #region 数据列表绑定
        private void RptBind(string strWhere)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string couponAutoPickingRuleCode = this.CouponAutoPickingRuleCode.Text.Trim();
                    string description = this.Description.Text.Trim();
                    string brandID = this.BrandID.SelectedValue.Trim();
                    string storeID = this.StoreID.SelectedValue.Trim();
                    if (!string.IsNullOrEmpty(couponAutoPickingRuleCode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CouponAutoPickingRuleCode like '%");
                        sb.Append(couponAutoPickingRuleCode);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(description))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Description";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(description);
                        sb.Append("%'");
                    }
                    if (brandID != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CouponAutoPickingRuleCode in (select CouponAutoPickingRuleCode from CouponAutoPickingRule_D where BrandID = ");
                        sb.Append(brandID);
                        sb.Append(" )");
                    }
                    if (storeID != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CouponAutoPickingRuleCode in (select CouponAutoPickingRuleCode from CouponAutoPickingRule_D where StoreID = ");
                        sb.Append(storeID);
                        sb.Append(" )");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                CouponAutoPickingRuleController controller = new CouponAutoPickingRuleController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("CouponAutoPickingRule", "Load Field", ex);
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CouponAutoPickingRuleController c = new CouponAutoPickingRuleController();
            int count = 0;
            DataSet ds = c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                if (Tools.DALTool.Delete<Edge.SVA.BLL.CouponAutoPickingRule_H>(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString()))
                {
                    Edge.SVA.BLL.CouponAutoPickingRule_D bll = new SVA.BLL.CouponAutoPickingRule_D();
                    bll.DeleteByCode(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString());
                }
            }
            this.RefreshPage();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("");
        }

        protected void Grid1_RowCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Action1")
            {
                RunDoDailyOperCouponOrder(Grid1.DataKeys[e.RowIndex][0].ToString());
            }
        }

        private void RunDoDailyOperCouponOrder(string couponAutoPickingRuleCode)
        {
            CouponAutoPickingRuleController controller = new CouponAutoPickingRuleController();
            controller.RunDoDailyOperCouponOrder(couponAutoPickingRuleCode);
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeID = 0;
            storeID = int.TryParse(this.StoreID.SelectedValue, out storeID) ? storeID : 0;
        }
    }
}