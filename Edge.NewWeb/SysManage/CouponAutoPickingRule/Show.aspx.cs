﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.SysManage.CouponAutoPickingRule
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponAutoPickingRule_H, Edge.SVA.Model.CouponAutoPickingRule_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                RegisterCloseEvent(btnClose);

                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);
            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                CouponAutoPickingRuleCode.Text = Model.CouponAutoPickingRuleCode;

                if (Model.Status == 1)
                {
                    StatusName.Text = "生效";
                }
                else 
                {
                    StatusName.Text = "关闭";
                }

                DayFlagID.SelectedValue = Model.DayFlagID.ToString();
                lblDayFlagCode.Text = DayFlagID.SelectedText;
                WeekFlagID.SelectedValue = Model.WeekFlagID.ToString();
                lblWeekFlagCode.Text = WeekFlagID.SelectedText;
                MonthFlagID.SelectedValue = Model.MonthFlagID.ToString();
                this.lblMonthFlagCode.Text = MonthFlagID.SelectedText;

                CouponAutoPickingRuleController controller = new CouponAutoPickingRuleController();

                DataTable dt = controller.GetDetailList(Model.CouponAutoPickingRuleCode);
                Session["CouponAutoPickingRule_D"] = dt;
            }

            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            DataTable dt = (DataTable)Session["CouponAutoPickingRule_D"];
            this.Grid1.RecordCount = dt.Rows.Count;
            this.Grid1.DataSource = dt;
            this.Grid1.DataBind();
            this.Grid1.SelectedRowIndexArray = rowIndexs;
        }

        protected void Grid1_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindingDataGrid();
        }

        protected override SVA.Model.CouponAutoPickingRule_H GetPageObject(SVA.Model.CouponAutoPickingRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }
    }
}