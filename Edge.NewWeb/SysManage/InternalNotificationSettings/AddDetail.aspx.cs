﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Tools;
using System.Data;
using System.Text;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;

namespace Edge.Web.SysManage.InternalNotificationSettings
{
    public partial class AddDetail : Edge.Web.Tools.BasePage<Edge.SVA.BLL.UserMessageSetting_D, Edge.SVA.Model.UserMessageSetting_D>
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                ControlTool.BindRoleID(this.RoleID);

                RegisterCloseEvent(btnClose);
            }
        }

        protected void SearchButton1_Click(object sender, EventArgs e)
        {
            RptBind1(" order by a.UserID ");
        }

        private void RptBind1(string orderby)
        {
            this.checkBoxList1.Items.Clear();
            try
            {
                string sql = " select a.UserID,a.UserName from Accounts_Users a,Accounts_UserRoles b where a.UserID=b.UserID ";
                string roleid = RoleID.SelectedValue;
                string username = UserName.Text;
                string usertype = UserType.Text;
                if (roleid != "-1")
                {
                    sql += " and b.RoleID = " + roleid + " ";
                }
                if (!string.IsNullOrEmpty(username))
                {
                    sql += " and a.UserName like '%" + username + "%' ";
                }
                if (!string.IsNullOrEmpty(usertype))
                {
                    sql += " and a.usertype like '%" + usertype + "%' ";
                }

                sql += orderby;

                DataSet ds = DBUtility.DbHelperSQL.Query(sql);
                if (ds != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        this.checkBoxList1.Items.Add(Convert.ToString(dr["UserName"]), Convert.ToString(dr["UserID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("User", "Load Filed", ex);
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["UserMessageSetting_D"];
            int[] arrUserID = this.checkBoxList1.SelectedIndexArray;
            if (arrUserID.Length == 0)
            {
                ShowWarning(" UserID must has value!");
                return;
            }
            foreach (int i in arrUserID)
            {
                if (dt.Select("ReceiveUserID = " + this.checkBoxList1.Items[i].Value + " and MessageServiceTypeID = " + this.MessageServiceTypeID.SelectedValue).Length > 0) 
                {
                    continue;
                }
                DataRow dr = dt.NewRow();
                dr["ReceiveUserID"] = this.checkBoxList1.Items[i].Value;
                dr["AccountNumber"] = this.checkBoxList1.Items[i].Text;
                dr["MessageServiceTypeID"] = this.MessageServiceTypeID.SelectedValue;
                dr["MessageServiceTypeName"] = this.MessageServiceTypeID.SelectedItem.Text;
                dt.Rows.Add(dr);
            }

            CloseAndPostBack();
        }
    }
}