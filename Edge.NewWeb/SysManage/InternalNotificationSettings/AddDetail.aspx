﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDetail.aspx.cs" Inherits="Edge.Web.SysManage.InternalNotificationSettings.AddDetail" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" Icon="SystemSaveClose"
                        OnClick="btnSaveClose_Click" runat="server" Text="添加后关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:GroupPanel ID="GroupPanel4" runat="server" EnableCollapse="True" Title="收件用户信息"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm4" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:DropDownList ID="RoleID" runat="server" Label="角色分配：" Resizable="true"></ext:DropDownList>
                                    <ext:TextBox ID="UserName" runat="server" Label="用户名称：">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:TextBox ID="UserType" runat="server" Label="用户类别：">
                                    </ext:TextBox>
                                    <ext:Button ID="SearchButton1" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton1_Click">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                            <ext:FormRow>
                                <Items>
                                    <ext:RadioButtonList ID="MessageServiceTypeID" runat="server" Label="发送通知渠道：">
                                        <ext:RadioItem Text="Email" Value="1" Selected="true" />
                                        <ext:RadioItem Text="SMS" Value="2" />
                                    </ext:RadioButtonList>
                                    <ext:Label ID="Label5" runat="server" Label="">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
            <ext:GroupPanel ID="GroupPanel2" runat="server" EnableCollapse="True" Title="可选用户"
                AutoHeight="true" AutoWidth="true">
                <Items>
                    <ext:Form ID="sForm2" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                        EnableBackgroundColor="true" LabelAlign="Right" LabelWidth="140">
                        <Rows>
                            <ext:FormRow>
                                <Items>
                                    <ext:CheckBoxList ID="checkBoxList1"  runat="server" ColumnNumber="5" ColumnVertical="true"></ext:CheckBoxList>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:GroupPanel>
        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
