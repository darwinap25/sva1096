﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.SysManage.InternalNotificationSettings
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.UserMessageSetting_H, Edge.SVA.Model.UserMessageSetting_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                SendUserName.Text = DALTool.GetCurrentUser().UserName;

                RegisterCloseEvent(btnClose);

            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                UserMessageContent.Text = Model.UserMessageContent;

                Status.SelectedValue = Model.Status.ToString();
                StatusName.Text = Status.SelectedItem.Text;

                UserMessageType.SelectedValue = Model.UserMessageType.ToString();
                UserMessageTypeName.Text = UserMessageType.SelectedItem.Text;

                TriggeType.SelectedValue = Model.TriggeType.ToString();
                TriggeTypeName.Text = TriggeType.SelectedItem.Text;

                UserMessageSettingController controller = new UserMessageSettingController();

                DataTable dt = controller.GetDetailList(Model.UserMessageCode);
                Session["UserMessageSetting_D"] = dt;
            }

            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            DataTable dt = (DataTable)Session["UserMessageSetting_D"];
            this.Grid1.RecordCount = dt.Rows.Count;
            this.Grid1.DataSource = dt;
            this.Grid1.DataBind();
            this.Grid1.SelectedRowIndexArray = rowIndexs;
        }

        protected void Grid1_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindingDataGrid();
        }

        protected override SVA.Model.UserMessageSetting_H GetPageObject(SVA.Model.UserMessageSetting_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }
    }
}