﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;

namespace Edge.Web.SysManage.ReplenishRule
{
    public partial class modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponReplenishRule_H, Edge.SVA.Model.CouponReplenishRule_H>
    {
        CouponReplenishRuleAddController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["id"] == null)
                {
                    return;
                }
                string id = Request["id"].ToString();

                this.CouponReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                btnNew1.OnClientClick = "";

                controller = new CouponReplenishRuleAddController();
                Edge.SVA.Model.CouponReplenishRule_H model = controller.GetModel(id);
                CouponReplenishCode.Text = model.CouponReplenishCode;
                Description.Text = model.Description;
                StartDate.Text = model.StartDate.ToString();
                EndDate.Text = model.EndDate.ToString();
                Status.SelectedValue = model.Status.ToString();
                BrandID.SelectedValue = model.BrandID.ToString();
                ControlTool.BindCouponType(CouponTypeID, (int)model.BrandID);
                CouponTypeID.SelectedValue = model.CouponTypeID.ToString();
                ControlTool.BindDayFlagID(DayFlagID);
                ControlTool.BindWeekFlagID(WeekFlagID);
                ControlTool.BindMonthFlagID(MonthFlagID);

                StoreTypeID.SelectedValue = model.StoreTypeID.ToString();
                if (model.StoreTypeID == 1)
                {
                    GroupPanel6.Title = "总部列表";
                    //btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=1", "新增");
                    string RequestURL = "addDetail.aspx?StoreTypeID=1&CouponTypeID=" + this.CouponTypeID.SelectedItem.Value;
                    btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                }
                else 
                {
                    GroupPanel6.Title = "店铺列表";
                    //btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=2", "新增");
                    string RequestURL = "addDetail.aspx?StoreTypeID=2&CouponTypeID=" + this.CouponTypeID.SelectedItem.Value;
                    btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                }
                controller.GetDetailList(id, model.StoreTypeID);

                SVASessionInfo.CouponReplenishRuleAddController = controller;

                //Add by Robin 20140520
                //this.BindDetail();

            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.CouponReplenishRuleList.SelectedRowIndexArray;
            controller = SVASessionInfo.CouponReplenishRuleAddController;
            this.CouponReplenishRuleList.RecordCount = controller.ViewModel.CouponReplenishRuleViewModelList.Count;
            this.CouponReplenishRuleList.DataSource = controller.ViewModel.CouponReplenishRuleViewModelList;
            this.CouponReplenishRuleList.DataBind();
            this.CouponReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.BrandID.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(CouponTypeID, string.Format("BrandID = {0}  order by CouponTypeCode", brandID));
        }

        //protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (StoreTypeID.SelectedValue == "1")
        //    {
        //        if (CouponTypeID.SelectedValue != "-1")
        //        {
        //            btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=1&CouponTypeID=" + CouponTypeID.SelectedValue, "新增");
        //        }
        //        else
        //        {
        //            btnNew1.OnClientClick = "";
        //        }
        //    }
        //    else
        //    {
        //        btnNew1.OnClientClick = Window1.GetShowReference("addDetail.aspx?StoreTypeID=2", "新增");
        //    }
        //}

        protected void btnDelete1_OnClick(object sender, EventArgs e)
        {
            int[] rowIndexs = this.CouponReplenishRuleList.SelectedRowIndexArray;
            List<CouponReplenishRuleViewModel> modelList = new List<CouponReplenishRuleViewModel>();
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                foreach (CouponReplenishRuleViewModel model in controller.ViewModel.CouponReplenishRuleViewModelList)
                {
                    if (this.CouponReplenishRuleList.Rows[rowIndexs[i]].Values[6] == model.MainTable.StoreID.ToString() && this.CouponReplenishRuleList.Rows[rowIndexs[i]].Values[7] == model.MainTable.OrderTargetID.ToString())
                    {
                        modelList.Add(model);
                        break;
                    }
                }
            }

            foreach (CouponReplenishRuleViewModel model in modelList)
            {
                controller.ViewModel.CouponReplenishRuleViewModelList.Remove(model);
            }
            SVASessionInfo.CouponReplenishRuleAddController = controller;
            this.CouponReplenishRuleList.SelectedRowIndexArray = new int[0];
            BindingDataGrid();
        }

        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            SVASessionInfo.CouponReplenishRuleAddController = null;
            controller = SVASessionInfo.CouponReplenishRuleAddController;
            BindingDataGrid();
        }

        protected void CouponReplenishRuleList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            CouponReplenishRuleList.PageIndex = e.NewPageIndex;

            BindingDataGrid();

        }

        protected void StoreTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            SVASessionInfo.CouponReplenishRuleAddController = null;
            controller = SVASessionInfo.CouponReplenishRuleAddController;
            BindingDataGrid();
            if (StoreTypeID.SelectedValue == "1")
            {
                //if (CouponTypeID.SelectedValue != "-1")
                //{
                string RequestURL = "addDetail.aspx?StoreTypeID=1&CouponTypeID=" + this.CouponTypeID.SelectedItem.Value;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                //}
                //else 
                //{
                //    btnNew1.OnClientClick = "";
                //}
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&CouponTypeID=" + this.CouponTypeID.SelectedItem.Value;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (this.BrandID.SelectedValue == "-1")
            {
                ShowWarning("请选择品牌！");
                return;
            }

            if (this.CouponTypeID.SelectedValue == "-1")
            {
                ShowWarning("请选择优惠券类型！");
                return;
            }

            Edge.SVA.Model.CouponReplenishRule_H item = this.GetUpdateObject();
            
            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponReplenish  {0} No Data", CouponReplenishCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            controller = SVASessionInfo.CouponReplenishRuleAddController;
            if (this.controller.ViewModel.CouponReplenishRuleViewModelList.Count == 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponReplenish Form  {0} Detail No Data", CouponReplenishCode.Text));
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.FAILED_TITLE);
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.CouponReplenishRule_H>(item))
            {
                Edge.SVA.BLL.CouponReplenishRule_D bll = new SVA.BLL.CouponReplenishRule_D();
                bll.DeleteByCouponReplenishCode(this.CouponReplenishCode.Text.Trim());

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("CouponReplenishRule_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (CouponReplenishRuleViewModel model in this.controller.ViewModel.CouponReplenishRuleViewModelList)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CouponReplenishCode"] = item.CouponReplenishCode;
                        row["StoreID"] = model.MainTable.StoreID;
                        row["OrderTargetID"] = model.MainTable.OrderTargetID;
                        row["MinStockQty"] = model.MainTable.MinStockQty;
                        row["RunningStockQty"] = model.MainTable.RunningStockQty;
                        row["OrderRoundUpQty"] = model.MainTable.OrderRoundUpQty;
                        row["Priority"] = model.MainTable.Priority;
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "CouponReplenishRule_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("CouponReplenishRule  {0} Update Success But Detail Failed", CouponReplenishCode.Text), ex);
                    //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                    ShowUpdateFailed();
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponReplenishRule  {0} Update Success", item.CouponReplenishCode));
                // JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("CouponReplenishRule  {0} Update Failed", item.CouponReplenishCode));
                ShowUpdateFailed();
                //JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    CouponReplenishRuleList.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        //protected void Grid1_RowCommand(object sender, FineUI.GridCommandEventArgs e)
        //{
        //    if (e.CommandName == "Delete")
        //    {
        //        object[] keys = CouponReplenishRuleList.DataKeys[e.RowIndex];
        //        string keyID = keys[0].ToString();
        //        DeleteDetail(keyID);
        //        BindDetail();
        //    }
        //}

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("CouponReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinStockQty", typeof(int));
                    dt.Columns.Add("RunningStockQty", typeof(int));
                    dt.Columns.Add("OrderRoundUpQty", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        //private void DeleteDetail(string keyID)
        //{
        //    foreach (System.Data.DataRow row in this.Detail.Rows)
        //    {
        //        if (row["KeyID"].ToString().Equals(keyID.ToString()))
        //        {
        //            row.Delete();
        //            break;
        //        }
        //    }
        //    this.Detail.AcceptChanges();
        //    this.BindDetail();
        //}

        //private void BindDetail()
        //{
        //    if (StoreTypeID.SelectedValue == "1")
        //    {
        //        GroupPanel6.Title = "总部列表";
        //    }
        //    else 
        //    {
        //        GroupPanel6.Title = "店铺列表";
        //    }
        //    this.CouponReplenishRuleList.RecordCount = this.Detail.Rows.Count;
        //    this.CouponReplenishRuleList.DataSource = DataTool.GetPaggingTable(this.CouponReplenishRuleList.PageIndex, this.CouponReplenishRuleList.PageSize, this.Detail);
        //    this.CouponReplenishRuleList.DataBind();
        //}

        //private int DetailIndex
        //{
        //    get
        //    {
        //        if (ViewState["DetailIndex"] == null)
        //        {
        //            ViewState["DetailIndex"] = 0;
        //        }
        //        ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
        //        return (int)ViewState["DetailIndex"];
        //    }
        //}

        protected override SVA.Model.CouponReplenishRule_H GetPageObject(SVA.Model.CouponReplenishRule_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (StoreTypeID.SelectedValue == "1")
            {
                //if (CouponTypeID.SelectedValue != "-1")
                //{
                string RequestURL = "addDetail.aspx?StoreTypeID=1&CouponTypeID=" + this.CouponTypeID.SelectedItem.Value;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
                //}
                //else 
                //{
                //    btnNew1.OnClientClick = "";
                //}
            }
            else
            {
                string RequestURL = "addDetail.aspx?StoreTypeID=2&CouponTypeID=" + this.CouponTypeID.SelectedItem.Value;
                btnNew1.OnClientClick = Window1.GetShowReference(RequestURL, "Add");
            }
        }
    }
}