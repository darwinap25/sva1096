﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.SVA.Model.Domain;

namespace Edge.Web.SysManage.ReplenishRule
{
    public partial class show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponReplenishRule_H, Edge.SVA.Model.CouponReplenishRule_H>
    {
        CouponReplenishRuleAddController controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                if (Request["id"] == null)
                {
                    return;
                }
                string id = Request["id"].ToString();

                this.CouponReplenishRuleList.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(BrandID);

                RegisterCloseEvent(btnClose);

                controller = new CouponReplenishRuleAddController();
                Edge.SVA.Model.CouponReplenishRule_H model = controller.GetModel(id);
                CouponReplenishCode.Text = model.CouponReplenishCode;
                Description.Text = model.Description;
                StartDate.Text = model.StartDate.ToString();
                EndDate.Text = model.EndDate.ToString();
                if (model.Status == 1)
                {
                    StatusName.Text = "生效";
                }
                else 
                {
                    StatusName.Text = "关闭";
                }
                BrandID.SelectedValue = model.BrandID.ToString();
                Brand.Text = BrandID.SelectedText;
                ControlTool.BindCouponType(CouponTypeID, (int)model.BrandID);
                CouponTypeID.SelectedValue = model.CouponTypeID.ToString();
                CouponType.Text = CouponTypeID.SelectedText;
                ControlTool.BindDayFlagID(DayFlagID);
                DayFlagID.SelectedValue = model.DayFlagID.ToString();
                lblDayFlagCode.Text = DayFlagID.SelectedText;
                ControlTool.BindWeekFlagID(WeekFlagID);
                WeekFlagID.SelectedValue = model.WeekFlagID.ToString();
                lblWeekFlagCode.Text = WeekFlagID.SelectedText;
                ControlTool.BindMonthFlagID(MonthFlagID);
                MonthFlagID.SelectedValue = model.MonthFlagID.ToString();
                this.lblMonthFlagCode.Text = MonthFlagID.SelectedText;
                if (model.StoreTypeID == 1)
                {
                    StoreType.Text = "总部";
                    GroupPanel6.Title = "总部列表";
                }
                else
                {
                    StoreType.Text = "店铺";
                    GroupPanel6.Title = "店铺列表";
                }
                controller.GetDetailList(id, model.StoreTypeID);

                SVASessionInfo.CouponReplenishRuleAddController = controller;

                //Add by Robin 20140520
                //this.BindDetail();

            }
            BindingDataGrid();
        }

        private void BindingDataGrid()
        {
            int[] rowIndexs = this.CouponReplenishRuleList.SelectedRowIndexArray;
            controller = SVASessionInfo.CouponReplenishRuleAddController;
            this.CouponReplenishRuleList.RecordCount = controller.ViewModel.CouponReplenishRuleViewModelList.Count;
            this.CouponReplenishRuleList.DataSource = controller.ViewModel.CouponReplenishRuleViewModelList;
            this.CouponReplenishRuleList.DataBind();
            this.CouponReplenishRuleList.SelectedRowIndexArray = rowIndexs;
        }

        protected void CouponReplenishRuleList_OnPageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            CouponReplenishRuleList.PageIndex = e.NewPageIndex;

            BindingDataGrid();

        }

        //protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        //{
        //    CouponReplenishRuleList.PageIndex = e.NewPageIndex;
        //    this.BindDetail();
        //}

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("KeyID", typeof(int));
                    dt.Columns.Add("CouponReplenishCode", typeof(string));
                    dt.Columns.Add("StoreID", typeof(int));
                    dt.Columns.Add("OrderTargetID", typeof(int));
                    dt.Columns.Add("MinStockQty", typeof(int));
                    dt.Columns.Add("RunningStockQty", typeof(int));
                    dt.Columns.Add("OrderRoundUpQty", typeof(int));
                    dt.Columns.Add("Priority", typeof(int));

                    ViewState["DetailResult"] = dt;
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        //private void BindDetail()
        //{
        //    if (StoreTypeID.SelectedValue == "1")
        //    {
        //        GroupPanel6.Title = "总部列表";
        //    }
        //    else 
        //    {
        //        GroupPanel6.Title = "店铺列表";
        //    }
        //    this.CouponReplenishRuleList.RecordCount = this.Detail.Rows.Count;
        //    this.CouponReplenishRuleList.DataSource = DataTool.GetPaggingTable(this.CouponReplenishRuleList.PageIndex, this.CouponReplenishRuleList.PageSize, this.Detail);
        //    this.CouponReplenishRuleList.DataBind();
        //}

        //private int DetailIndex
        //{
        //    get
        //    {
        //        if (ViewState["DetailIndex"] == null)
        //        {
        //            ViewState["DetailIndex"] = 0;
        //        }
        //        ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
        //        return (int)ViewState["DetailIndex"];
        //    }
        //}
    }
}