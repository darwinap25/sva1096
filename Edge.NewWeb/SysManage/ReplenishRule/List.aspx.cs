﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Controllers;
using System.Data;
using Edge.Web.Tools;
using FineUI;

namespace Edge.Web.SysManage.ReplenishRule
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                RptBind("");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                ControlTool.BindBrand(this.Brand);
                ControlTool.BindCouponType(CouponType, -1);
            }
        }

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.Brand.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(CouponType, string.Format("BrandID = {0}  order by CouponTypeCode", brandID));
        }

        #region 数据列表绑定
        private void RptBind(string strWhere)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string couponReplenishCode = this.CouponReplenishCode.Text.Trim();
                    string description = this.Description.Text.Trim();
                    string brandID = this.Brand.SelectedValue.Trim();
                    string couponType = this.CouponType.SelectedValue.Trim();
                    string storeTypeID = this.StoreType.SelectedValue.Trim();
                    if (!string.IsNullOrEmpty(couponReplenishCode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CouponReplenishCode like '%");
                        sb.Append(couponReplenishCode);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(description))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "Description";
                        sb.Append(descLan);
                        sb.Append(" like '%");
                        sb.Append(description);
                        sb.Append("%'");
                    }
                    if (brandID != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("BrandID = ");
                        sb.Append(brandID);
                    }
                    if (couponType != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CouponTypeID = ");
                        sb.Append(couponType);
                    }
                    if (!string.IsNullOrEmpty(storeTypeID))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("StoreTypeID = ");
                        sb.Append(storeTypeID);
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                CouponReplenishRuleController controller = new CouponReplenishRuleController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog("CouponReplenishRule", "Load Field", ex);
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            CouponReplenishRuleController c = new CouponReplenishRuleController();
            int count = 0;
            DataSet ds = c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count);

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                if (Tools.DALTool.Delete<Edge.SVA.BLL.CouponReplenishRule_H>(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString())) 
                {
                    Edge.SVA.BLL.CouponReplenishRule_D bll = new SVA.BLL.CouponReplenishRule_D();
                    bll.DeleteByCouponReplenishCode(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString());      
                }
            }
            this.RefreshPage();
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("");
        }

        protected void Grid1_RowCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Action1")
            {
                RunAutoGenCouponOrder(Grid1.DataKeys[e.RowIndex][0].ToString());
            }
        }

        private void RunAutoGenCouponOrder(string couponReplenishCode) 
        {
            CouponReplenishRuleController controller = new CouponReplenishRuleController();
            controller.RunAutoGenCouponOrder(couponReplenishCode);
        }

        protected void btnExecute_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sb.Append(Grid1.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            RunAutoGenCouponOrder(sb.ToString());
        }
    }
}