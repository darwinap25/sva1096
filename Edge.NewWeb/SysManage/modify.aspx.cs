﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using Edge.Security.Model;
using Edge.Security.Manager;
using Edge.Web.Tools;
using FineUI;
namespace Edge.Web.SysManage
{
	/// <summary>
	/// modify 的摘要说明。
	/// </summary>
    public partial class modify : PageBase
	{
		protected System.Web.UI.WebControls.DropDownList dropModule;
		protected System.Web.UI.WebControls.DropDownList Dropdepart;
		protected System.Web.UI.WebControls.CheckBox chkPublic;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
                btnClose.OnClientClick = ActiveWindow.GetConfirmHidePostBackReference();

				//得到现有菜单
				BindTree();	           
				//得到所有权限
                BindClassTree();

				//BindImages();

				if(Request["id"]!=null)
				{
					string id=Request.Params["id"];
					if(id==null || id.Trim()=="")
					{
                        return;
					}
					else
					{
						ShowInfo(id);						
					}
				}
			}
			
		}

		private void ShowInfo(string id)
		{
			
			//Navigation01.Para_Str="id="+id;
			Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();
			SysNode node=sm.GetNode(int.Parse(id));		
			
			this.hfID.Text=id;
			this.txtOrderId.Text=node.OrderID.ToString();
			this.txtName.Text=node.Text;
			//menu
			if(node.ParentID==0)
			{
				this.ddlParent.SelectedIndex=0;
			}
			else
			{
                for (int m = 0; m < this.ddlParent.Items.Count; m++)
				{
                    if (this.ddlParent.Items[m].Value == node.ParentID.ToString())
                    {
                        this.ddlParent.Items[m].Selected = true;
                    }
                    else
                    {
                        this.ddlParent.Items[m].Selected = false;
                    }
				}
			}
			this.txtUrl.Text=node.Url;
//			this.txtImgUrl.Text=node.ImageUrl;
			this.txtDescription.Text=node.Comment;

			//Permission
            //for(int n=0;n<this.listPermission.Items.Count;n++)
            //{
            //    if((this.listPermission.Items[n].Value==node.PermissionID.ToString())&&(this.listPermission.Items[n].Value!="-1"))
            //    {
            //        this.listPermission.Items[n].Selected=true;
            //    }
            //}

            DataRow row =AccountsTool.GetPermission(node.PermissionID);
            if (row != null)
            {
                this.ddlCategoryList.SelectedValue = row["CategoryID"].ToString().Trim();
                PermissionsDatabind();
                this.ddlPermissionList.SelectedValue = node.PermissionID.ToString().Trim();
            }

//			//module
//			for(int n=0;n<this.dropModule.Items.Count;n++)
//			{
//				if(this.dropModule.Items[n].Value==node.ModuleID.ToString())
//				{
//					this.dropModule.Items[n].Selected=true;
//				}
//			}
//
//			//module
//			for(int n=0;n<this.Dropdepart.Items.Count;n++)
//			{
//				if(this.Dropdepart.Items[n].Value==node.KeShiDM.ToString())
//				{
//					this.Dropdepart.Items[n].Selected=true;
//				}
//			}

			//image
            //for(int n=0;n<this.imgsel.Items.Count;n++)
            //{
            //    if(this.imgsel.Items[n].Value==node.ImageUrl)
            //    {
            //        this.imgsel.Items[n].Selected=true;
            //        this.hideimgurl.Value=node.ImageUrl;
            //    }
            //}
            if (node.KeshiPublic.ToLower() == "true")
            {
                this.ckbKeshi.Checked = true;
            }
            else
            {
                this.ckbKeshi.Checked = false;
            }
//			if(node.KeshiPublic=="true")
//			{
//				this.chkPublic.Checked=true;
//			}
		}

        protected void btnSaveClose_Click(object sender, System.EventArgs e)
        {
            string id = Edge.Common.PageValidate.InputText(this.hfID.Text, 10);
            string orderid = Edge.Common.PageValidate.InputText(this.txtOrderId.Text, 5);
            string name = txtName.Text;
            string url = Edge.Common.PageValidate.InputText(txtUrl.Text, 100);
            //			string imgUrl=Edge.Common.PageValidate.InputText(txtImgUrl.Text,100);
            string imgUrl = "";
            string target = this.ddlParent.SelectedValue;
            int parentid = int.Parse(target);


            if (orderid.Trim() == "")
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.NumberNotEmpty, FineUI.MessageBoxIcon.Warning);
                return;
            }
            try
            {
                int.Parse(orderid);
            }
            catch
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.NumberFormatError, FineUI.MessageBoxIcon.Warning);
                return;
            }
            if (name.Trim() == "")
            {
                FineUI.Alert.ShowInTop(Resources.MessageTips.NameNotEmpty, FineUI.MessageBoxIcon.Warning);
                return;
            }

            int permission_id = -1;
            if (!string.IsNullOrEmpty(this.ddlPermissionList.SelectedValue))
            {
                permission_id = int.Parse(this.ddlPermissionList.SelectedValue);
            }
            int moduleid = -1;
            //			if(this.dropModule.SelectedIndex>0)
            //			{
            //				moduleid=int.Parse(this.dropModule.SelectedValue);
            //			}
            //			int moduledeptid=-1;
            //			if(this.dropModuleDept.SelectedIndex>0)
            //			{
            //				moduledeptid=int.Parse(this.dropModuleDept.SelectedValue);
            //			}
            int keshidm = -1;
            //			if(this.Dropdepart.SelectedIndex>0)
            //			{
            //				keshidm=int.Parse(this.Dropdepart.SelectedValue);
            //			}
            string keshipublic = this.ckbKeshi.Checked ? "true" : "false";
            //			if(this.chkPublic.Checked)
            //			{
            //				keshipublic="true";
            //			}
            string comment = Edge.Common.PageValidate.InputText(txtDescription.Text, 100);

            SysNode node = new SysNode();
            node.NodeID = int.Parse(id);
            node.OrderID = int.Parse(orderid);
            node.Text = name;
            node.ParentID = parentid;
            node.Location = parentid + "." + orderid;
            node.Comment = comment;
            node.Url = url;
            node.PermissionID = permission_id;
            node.ImageUrl = imgUrl;
            node.ModuleID = moduleid;
            node.KeShiDM = keshidm;
            node.KeshiPublic = keshipublic;

            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
            sm.UpdateNode(node);

            Logger.Instance.WriteOperationLog(this.PageName, "Update Tree Menu : " + node.Text);

            PermissionMapper.GetSingleton().Refresh();

            PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
        }

        #region Event

        private void BindTree()
        {
            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
            DataTable dt = sm.GetTreeList("").Tables[0];
            this.ddlParent.Items.Clear();
            //加载树
            this.ddlParent.Items.Add(new FineUI.ListItem("#", "0"));
            DataRow[] drs = dt.Select("ParentID= " + 0);

            foreach (DataRow r in drs)
            {
                string nodeid = r["NodeID"].ToString();
                string text = r["Text"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ddlParent.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, dt, blank);

            }
            this.ddlParent.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["NodeID"].ToString();
                string text = r["Text"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = blank + "『" + text + "』";

                this.ddlParent.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }

        //private void BindImages()
        //{
        //    string dirpath=Server.MapPath("../Images/MenuImg");
        //    DirectoryInfo di = new DirectoryInfo(dirpath);  
        //    FileInfo[] rgFiles = di.GetFiles("*.gif");  
        //    this.ddlImgUrl.Items.Clear();
        //    foreach(FileInfo fi in rgFiles) 
        //    {
        //        FineUI.ListItem item = new FineUI.ListItem(fi.Name, "Images/MenuImg/" + fi.Name);
        //        this.ddlImgUrl.Items.Add(item);
        //    }
        //    FileInfo[] rgFiles2 = di.GetFiles("*.jpg"); 		
        //    foreach(FileInfo fi in rgFiles2) 
        //    {
        //        FineUI.ListItem item = new FineUI.ListItem(fi.Name, "Images/MenuImg/" + fi.Name);
        //        this.ddlImgUrl.Items.Add(item);
        //    }
        //    this.ddlImgUrl.Items.Insert(0, new FineUI.ListItem("Default", ""));
        //    this.ddlImgUrl.DataBind();
        //}

        private void BindClassTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(SVASessionInfo.SiteLanguage.ToString());


            this.ddlCategoryList.Items.Clear();
            //加载树
            this.ddlCategoryList.Items.Add(new FineUI.ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ddlCategoryList.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindClassNode(sonparentid, ds.Tables[0], blank);

            }
            this.ddlCategoryList.DataBind();

        }

        private void BindClassNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.ddlCategoryList.Items.Add(new FineUI.ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindClassNode(sonparentid, dt, blank2);
            }
        }


        protected void ddlCategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PermissionsDatabind();
        }

        private void PermissionsDatabind()
        {
            int CategoryId = int.Parse(this.ddlCategoryList.SelectedValue);
            DataSet PermissionsList = AccountsTool.GetPermissionsByCategory(CategoryId, SVASessionInfo.SiteLanguage.ToString());//todo: 修改成多语言。
            this.ddlPermissionList.DataSource = PermissionsList;
            this.ddlPermissionList.DataTextField = "Description";
            this.ddlPermissionList.DataValueField = "PermissionID";
            this.ddlPermissionList.DataBind();
        }

        #endregion

//        #region 
//        //菜单
//        private void BindTree()
//        {
////			Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();			
////			DataTable dt=sm.GetTreeList("").Tables[0];
////
////
////			this.listTarget.Items.Clear();
////			//加载树
////			this.listTarget.Items.Add(new ListItem("根目录","0"));
////			DataRow [] drs = dt.Select("ParentID= " + 0);			
////			foreach( DataRow r in drs )
////			{
////				string nodeid=r["NodeID"].ToString();				
////				string text=r["Text"].ToString();					
////				string parentid=r["ParentID"].ToString();
////				string permissionid=r["PermissionID"].ToString();
////				text="╋"+text;				
////				this.listTarget.Items.Add(new ListItem(text,nodeid));
////				int sonparentid=int.Parse(nodeid);
////				BindNode( sonparentid, dt);
////
////			}	
////			this.listTarget.DataBind();	
////		


//            Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();			
//            DataTable dt=sm.GetTreeList("").Tables[0];


//            this.listTarget.Items.Clear();
//            //加载树
//            this.listTarget.Items.Add(new ListItem("#","0"));
//            DataRow [] drs = dt.Select("ParentID= " + 0);
//            foreach( DataRow r in drs )
//            {
//                string nodeid=r["NodeID"].ToString();				
//                string text=r["Text"].ToString();					
//                //string parentid=r["ParentID"].ToString();
//                //string permissionid=r["PermissionID"].ToString();
//                text="╋"+text;				
//                this.listTarget.Items.Add(new ListItem(text,nodeid));
//                int sonparentid=int.Parse(nodeid);
//                string blank="├";
				
//                BindNode( sonparentid, dt,blank);

//            }	
//            this.listTarget.DataBind();			


//        }

//        private void BindNode(int parentid,DataTable dt,string blank)
//        {
//            DataRow [] drs = dt.Select("ParentID= " + parentid );
			
//            foreach( DataRow r in drs )
//            {
//                string nodeid=r["NodeID"].ToString();				
//                string text=r["Text"].ToString();					
//                //string permissionid=r["PermissionID"].ToString();
//                text=blank+"『"+text+"』";
				
//                this.listTarget.Items.Add(new ListItem(text,nodeid));
//                int sonparentid=int.Parse(nodeid);
//                string blank2=blank+"─";
				

//                BindNode( sonparentid, dt,blank2);
//            }
//        }
////
////		private void BindNode(int parentid,DataTable dt)
////		{
////			DataRow [] drs = dt.Select("ParentID= " + parentid );			
////			foreach( DataRow r in drs )
////			{
////				string nodeid=r["NodeID"].ToString();				
////				string text=r["Text"].ToString();					
////				string permissionid=r["PermissionID"].ToString();
////				text="  "+"├『"+text+"』";			
////				this.listTarget.Items.Add(new ListItem(text,nodeid));
////				int sonparentid=int.Parse(nodeid);
////				BindNode( sonparentid, dt);
////			}
////		}

//        //权限
//        //private void BiudPermTree()
//        //{
//        //    //				DataSet ds=Edge.Security.Manager.AccountsTool.GetAllPermissions();
//        //    //				this.listPermission.Items.Clear();
//        //    //				this.listPermission.DataSource=ds.Tables[1];			
//        //    //				this.listPermission.DataTextField="Description";
//        //    //				this.listPermission.DataValueField="PermissionID";
//        //    //				this.listPermission.DataBind();
//        //    //				this.listPermission.Items.Insert(0,"--请选择--");


//        //    DataTable tabcategory = Edge.Security.Manager.AccountsTool.GetAllCategories(SVASessionInfo.SiteLanguage.ToString()).Tables[0];	//todo: 修改成多语言。					
//        //    int rc=tabcategory.Rows.Count;
//        //    for(int n=0;n<rc;n++)
//        //    {
//        //        string CategoryID=tabcategory.Rows[n]["CategoryID"].ToString();
//        //        string CategoryName=tabcategory.Rows[n]["Description"].ToString();
//        //        CategoryName="╋"+CategoryName;
//        //        this.listPermission.Items.Add(new ListItem(CategoryName,"-1"));

//        //        DataTable tabforums = Edge.Security.Manager.AccountsTool.GetPermissionsByCategory(int.Parse(CategoryID),SVASessionInfo.SiteLanguage.ToString()).Tables[0];//todo: 修改成多语言。
//        //        int fc=tabforums.Rows.Count;
//        //        for(int m=0;m<fc;m++)
//        //        {
//        //            string ForumID=tabforums.Rows[m]["PermissionID"].ToString();
//        //            string ForumName=tabforums.Rows[m]["Description"].ToString();
//        //            ForumName="  ├『"+ForumName+"』";
//        //            this.listPermission.Items.Add(new ListItem(ForumName,ForumID));
//        //        }
//        //    }
//        //    this.listPermission.DataBind();	
//        //    this.listPermission.Items.Insert(0,"--请选择--");
//        //}


////		private void BindModule()
////		{
////			Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();
////			DataSet ds=sm.GetTypeModules("function");
////			this.dropModule.DataSource=ds;
////			this.dropModule.DataTextField="Description";
////			this.dropModule.DataValueField="ModuleID";
////			this.dropModule.DataBind();
////			this.dropModule.Items.Insert(0,"--请选择--");
////		}

////		private void BindModuledept()
////		{
////			Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();
////			DataSet ds=sm.GetTypeModules("dept");
////			this.dropModuleDept.DataSource=ds;
////			this.dropModuleDept.DataTextField="Description";
////			this.dropModuleDept.DataValueField="ModuleID";
////			this.dropModuleDept.DataBind();
////			this.dropModuleDept.Items.Insert(0,"--请选择--");
////		}

////		private void BindDept()
////		{
////			DataSet ds=Edge.BLL.PubConstant.GetAllKeShi();
////			this.Dropdepart.DataSource=ds.Tables[0].DefaultView;
////			this.Dropdepart.DataTextField="KeShimch";
////			this.Dropdepart.DataValueField="KeShidm";
////			this.Dropdepart.DataBind();
////			this.Dropdepart.Items.Insert(0,"--请选择--");

////		}

//        private void BindImages()
//        {
//            string dirpath=Server.MapPath("../Images/MenuImg");
//            DirectoryInfo di = new DirectoryInfo(dirpath);  
//            FileInfo[] rgFiles = di.GetFiles("*.gif");  
//            this.imgsel.Items.Clear();
//            foreach(FileInfo fi in rgFiles) 
//            { 
//                ListItem item=new ListItem(fi.Name,"Images/MenuImg/"+fi.Name);
//                this.imgsel.Items.Add(item);
//            }
//            FileInfo[] rgFiles2 = di.GetFiles("*.jpg"); 		
//            foreach(FileInfo fi in rgFiles2) 
//            { 
//                ListItem item=new ListItem(fi.Name,"Images/MenuImg/"+fi.Name);
//                this.imgsel.Items.Add(item);
//            }
//            this.imgsel.Items.Insert(0,"默认图标");
//            this.imgsel.DataBind();
//        }


//        #endregion


		#region Web 窗体设计器生成的代码
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

//        protected void btnAdd_Click(object sender, System.EventArgs e)
//        {
			
//            string id=Edge.Common.PageValidate.InputText(this.lblID.Text,10);
//            string orderid=Edge.Common.PageValidate .InputText(this.txtOrderid.Text,5);
//            string name=txtName.Text;
//            string url=Edge.Common.PageValidate.InputText(txtUrl.Text,100);
////			string imgUrl=Edge.Common.PageValidate.InputText(txtImgUrl.Text,100);
//            string imgUrl=this.hideimgurl.Value;
//            string target=this.listTarget.SelectedValue;			
//            int parentid=int.Parse(target);

//            string strErr="";

//            if(orderid.Trim()=="")
//            {
//                strErr += Resources.MessageTips.NumberNotEmpty + "\\n";
				
//            }
//            try
//            {
//                int.Parse(orderid);
//            }
//            catch
//            {
//                strErr += Resources.MessageTips.NumberFormatError + "\\n";
				
//            }
//            if(name.Trim()=="")
//            {
//                strErr += Resources.MessageTips.NameNotEmpty + "\\n";		
//            }

//            //if(this.listPermission.SelectedItem.Text.StartsWith("╋"))
//            //{
//            //    strErr += Resources.MessageTips.NotUsePermission + "\\n";
//            //}

//            if(strErr!="")
//            {
//                Edge.Common.MessageBox.Show(this,strErr);
//                return;
//            }



//            int permission_id=-1;
//            if (!string.IsNullOrEmpty(this.listPermission.SelectedValue))
//            {
//                permission_id = int.Parse(this.listPermission.SelectedValue);
//            }
//            int moduleid=-1;
////			if(this.dropModule.SelectedIndex>0)
////			{
////				moduleid=int.Parse(this.dropModule.SelectedValue);
////			}
////			int moduledeptid=-1;
////			if(this.dropModuleDept.SelectedIndex>0)
////			{
////				moduledeptid=int.Parse(this.dropModuleDept.SelectedValue);
////			}
//            int keshidm=-1;
////			if(this.Dropdepart.SelectedIndex>0)
////			{
////				keshidm=int.Parse(this.Dropdepart.SelectedValue);
////			}
//            string keshipublic = this.ckbKeshi.Checked ? "true" : "false";
////			if(this.chkPublic.Checked)
////			{
////				keshipublic="true";
////			}
//            string comment=Edge.Common.PageValidate.InputText(txtDescription.Text,100);
		
//            SysNode node=new SysNode();
//            node.NodeID=int.Parse(id);
//            node.OrderID=int.Parse(orderid);
//            node.Text=name;
//            node.ParentID=parentid;
//            node.Location=parentid+"."+orderid;	
//            node.Comment=comment;
//            node.Url=url;
//            node.PermissionID=permission_id;
//            node.ImageUrl=imgUrl;
//            node.ModuleID=moduleid;
//            node.KeShiDM=keshidm;
//            node.KeshiPublic=keshipublic;

//            Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();
//            sm.UpdateNode(node);

//            Logger.Instance.WriteOperationLog(this.PageName, "Update Tree Menu : " + node.Text);

//            PermissionMapper.GetSingleton().Refresh();
//            Response.Redirect("show.aspx?id="+id);

//        }


//        protected void btnCancel_Click(object sender, System.EventArgs e)
//        {
//            this.lblID.Text="";
//            txtName.Text="";
//            txtUrl.Text="";
////			txtImgUrl.Text="";
//            txtDescription.Text="";
			
//        }

//        //protected void btnReturn_Click(object sender, EventArgs e)
//        //{
//        //    Response.Redirect("TreeAdmin.aspx");
//        //}


//        protected void ClassList_SelectedIndexChanged(object sender, System.EventArgs e)
//        {
//            PermissionsDatabind();
//        }


//        private void PermissionsDatabind()
//        {

//            int CategoryId = int.Parse(this.CategoryList.SelectedValue);

//            DataSet PermissionsList = AccountsTool.GetPermissionsByCategory(CategoryId, SVASessionInfo.SiteLanguage.ToString());//todo: 修改成多语言。
//            this.listPermission.DataSource = PermissionsList;
//            this.listPermission.DataTextField = "Description";
//            this.listPermission.DataValueField = "PermissionID";
//            this.listPermission.DataBind();
//        }

//        #region 绑定下拉树
//        private void BindClassTree()
//        {
//            DataSet ds;
//            ds = AccountsTool.GetAllCategories(SVASessionInfo.SiteLanguage.ToString());


//            this.CategoryList.Items.Clear();
//            //加载树
//            this.CategoryList.Items.Add(new ListItem("#", "0"));
//            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


//            foreach (DataRow r in drs)
//            {
//                string nodeid = r["CategoryID"].ToString();
//                string text = r["Description"].ToString();
//                //string parentid=r["ParentID"].ToString();
//                //string permissionid=r["PermissionID"].ToString();
//                text = "╋" + text;
//                this.CategoryList.Items.Add(new ListItem(text, nodeid));
//                int sonparentid = int.Parse(nodeid);
//                string blank = "├";

//                BindClassNode(sonparentid, ds.Tables[0], blank);

//            }
//            this.CategoryList.DataBind();

//        }

//        private void BindClassNode(int parentid, DataTable dt, string blank)
//        {
//            DataRow[] drs = dt.Select("ParentID= " + parentid);

//            foreach (DataRow r in drs)
//            {
//                string nodeid = r["CategoryID"].ToString();
//                string text = r["Description"].ToString();
//                text = blank + "『" + text + "』";
//                this.CategoryList.Items.Add(new ListItem(text, nodeid));
//                int sonparentid = int.Parse(nodeid);
//                string blank2 = blank + "─";

//                BindClassNode(sonparentid, dt, blank2);
//            }
//        }
//        #endregion
	}
}
