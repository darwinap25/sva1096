﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace Edge.Web.Tools
{
    public class ExportTool
    {
        public static bool ExportStream(Stream stream, string filename)
        {
            try
            {
                if (stream == null) return false;

                if (stream.Position > 0)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                }

                long fileSize = stream.Length;
                byte[] buffer = new byte[fileSize];
                stream.Read(buffer, 0, buffer.Length);

                HttpContext.Current.Response.ClearContent();

                HttpContext.Current.Response.ContentType = "application/octet-stream";

                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(filename, Encoding.UTF8));

                HttpContext.Current.Response.AddHeader("Content-Length", fileSize.ToString());

                HttpContext.Current.Response.BinaryWrite(buffer);

                HttpContext.Current.Response.Flush();

                HttpContext.Current.Response.Close();

                return true;
            }
            catch (Exception ex)
            {
                Edge.Web.Tools.Logger.Instance.WriteErrorLog(" Export ","",ex);
                return false;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        public static bool ExportFile(string fileName)
        {
            System.IO.FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            //ExportStream(fs, fileName.Substring(fileName.LastIndexOf("\\") + 1));
            ExportStream(fs, Path.GetFileName(fileName));
            return true;
        }
        public static bool ExportFile(string fileName,string exportFileName)
        {
            System.IO.FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            
            ExportStream(fs, exportFileName);
            return true;
        }

        public static string GetFullName(string fileName)
        {
            string path = System.Web.HttpContext.Current.Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath) + "Export\\";

            if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);

            string fullName = path + fileName;

            return fullName;
        }

        /// <summary>
        /// 文件下载
        /// </summary>
        /// <param name="sData">数据</param>
        /// <param name="sFileName">文件名称</param>
        /// <param name="page">Web 窗体页</param>
        /// <param name="iFileType">文件类型</param>
        public static void DownLoadFile(string sData, string sFileName, System.Web.UI.Page page, int iFileType)
        {
            try
            {
                System.Text.Encoding unicode_encoding = System.Text.Encoding.UTF8;
                System.Text.Encoding default_encoding = System.Text.Encoding.Default;
                byte[] unicodeBytes = unicode_encoding.GetBytes(sData);
                byte[] defaultBytes = System.Text.Encoding.Convert(unicode_encoding,
                                                                   default_encoding,
                                                                   unicodeBytes);

                page.Response.Buffer = true;
                page.Response.Charset = "";
                page.Response.Clear();
                page.Response.Expires = 0;

                page.Response.AddHeader("Content-Disposition", "Attachment; FileName="
                                        + HttpUtility.UrlEncode(sFileName, Encoding.UTF8));
                if (iFileType == 1)
                {
                    // xls
                    page.Response.ContentType = "application/ms-excel";
                }
                else if (iFileType == 2)
                {
                    page.Response.ContentType = "application/x-download";
                }
                else if (iFileType == 3)
                {
                    page.Response.ContentType = "application/octet-stream";
                }

                page.Response.ContentEncoding = System.Text.Encoding.Default;

                if (defaultBytes.Length > 0)
                {
                    page.Response.BinaryWrite(defaultBytes);
                }

                page.Response.End();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}