﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Enquiry.CardViewer
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.ReceiveTxn,Edge.SVA.Model.ReceiveTxn>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                int id = 0;
                this.CreatedBy.Text = DALTool.GetUserName(int.TryParse(this.Model.CreatedBy, out id) ? id : 0);
                this.UpdatedBy.Text = DALTool.GetUserName(int.TryParse(this.Model.UpdatedBy, out id) ? id : 0);
                this.Approvedby.Text = DALTool.GetUserName(int.TryParse(this.Model.Approvedby, out id) ? id : 0);
            }
        }

    }
}
