﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Enquiry.CardViewer.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：显示交易信息</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                显示交易
            </th>
        </tr>
        <tr>
            <td align="right">
                ID：
            </td>
            <td>
                <asp:Label ID="KeyID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                店铺编号：
            </td>
            <td width="75%">
                <asp:Label ID="StoreCode" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                服务器编号：
            </td>
            <td>
                <asp:Label ID="ServerCode" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                终端编号：
            </td>
            <td>
                <asp:Label ID="RegisterCode" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                调用方提供：
            </td>
            <td>
                <asp:Label ID="TxnNoSN" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易单号：
            </td>
            <td>
                <asp:Label ID="TxnNo" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易日期：
            </td>
            <td>
                <asp:Label ID="BusDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                交易用卡号：
            </td>
            <td>
                <asp:Label ID="CardNumber" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_Start交易卡类型ID：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="CardTypeID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_Start操作ID：Translate__Special_121_End
            </td>
            <td>
                <asp:Label ID="OprID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                操作金额：
            </td>
            <td>
                <asp:Label ID="Amount" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                操作积分：
            </td>
            <td>
                <asp:Label ID="Points" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                VoidKeyIDID：
            </td>
            <td>
                <asp:Label ID="VoidKeyID" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                VoidTxnNo：
            </td>
            <td>
                <asp:Label ID="VoidTxnNo" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                附加信息：
            </td>
            <td>
                <asp:Label ID="Additional" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                记录状态：
            </td>
            <td>
                <asp:Label ID="ApproveStatus" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                Remarks：
            </td>
            <td>
                <asp:Label ID="Remark" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                签名校验字段：
            </td>
            <td>
                <asp:Label ID="SecurityCode" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:Label ID="CreatedDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                更新日期：
            </td>
            <td>
                <asp:Label ID="UpdatedDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                更新人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核日期：
            </td>
            <td>
                <asp:Label ID="ApprovedDate" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                批核人：
            </td>
            <td>
                <asp:Label ID="Approvedby" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
