﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportForm.aspx.cs" Inherits="Edge.Web.PublicForms.ImportForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server"/>
        <ext:Panel ID="Panel1" runat="server" ShowBorder="false" ShowHeader="false" BodyPadding="0"
            EnableBackgroundColor="true" Layout="Row" >
            <Items>
                <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
                    BodyPadding="10px" EnableBackgroundColor="true" Title="SimpleForm" AutoScroll="true"
                    LabelAlign="Right">
                    <Toolbars>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                                    Text="关闭">
                                </ext:Button>
                                <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                </ext:ToolbarSeparator>
                                <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                                   OnClick="btnSaveClose_Click" runat="server" Text="保存后关闭">
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </Toolbars>
                    <Items>
                        <ext:Label runat="server"></ext:Label>
                        <ext:FileUpload runat="server" ID="ImportFile" Label="导入文件："></ext:FileUpload>
                    </Items>
                </ext:SimpleForm>
            </Items>
        </ext:Panel>
    </form>
</body>
</html>
