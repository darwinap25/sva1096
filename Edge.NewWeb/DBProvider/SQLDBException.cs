﻿using System;
using System.Runtime.Serialization;

namespace Edge.Web.DBProvider
{
    [Serializable()]
    public class SQLDBException : System.ApplicationException
    {
        public SQLDBException()
            : base()
        {
        }

        public SQLDBException(string strMessage, System.Exception excException)
            : base(strMessage, excException)
        {
        }

        public SQLDBException(string strMessage)
            : base(strMessage)
        {
        }

        public SQLDBException(SerializationInfo serialization, StreamingContext stream)
            : base(serialization, stream)
        {
        }
    }
}