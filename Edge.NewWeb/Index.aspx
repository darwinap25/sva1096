﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Edge.Web.Index" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
    <link href="Style/default.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="RegionPanel1" runat="server">
    </ext:PageManager>
    <ext:RegionPanel ID="RegionPanel1" ShowBorder="false" runat="server">
        <Regions>
            <ext:Region ID="Region1" Margins="0 0 0 0" Height="65px" ShowBorder="false" ShowHeader="false"
                Position="Top" Layout="Fit" runat="server" EnableCollapse="true">
                <Toolbars>
                    <ext:Toolbar ID="Toolbar1" Position="Bottom" runat="server">
                        <Items>
                            <ext:ToolbarText ID="lblAdminName" runat="server">
                            </ext:ToolbarText>
                            <ext:ToolbarFill ID="ToolbarFill1" runat="server">
                            </ext:ToolbarFill>
                            <ext:ToolbarText ID="ToolbarText4" Text="Translate__Special_121_Start&nbsp;&nbsp;语言：Translate__Special_121_End" runat="server">
                            </ext:ToolbarText>
                            <ext:DropDownList ID="ddlLanguage" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"
                                runat="server">
                                <ext:ListItem Text="English" Value="en" />
                                <ext:ListItem Text="简体中文" Value="zh_cn" />
                                <ext:ListItem Text="繁體中文" Value="zh_tw" />
                            </ext:DropDownList>
                            <ext:ToolbarText ID="ToolbarText3" Text="Translate__Special_121_Start&nbsp;&nbsp;主题：Translate__Special_121_End" runat="server" Hidden="true">
                            </ext:ToolbarText>
                            <ext:DropDownList ID="ddlTheme" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="ddlTheme_SelectedIndexChanged"
                                runat="server" Hidden="true">
                                <ext:ListItem Text="Blue" Selected="true" Value="blue" />
                                <ext:ListItem Text="Gray" Value="gray" />
                                <ext:ListItem Text="Access" Value="access" />
                            </ext:DropDownList>
                            <ext:ToolbarText ID="ToolbarText1" runat="server">
                            </ext:ToolbarText>
                            <ext:Button ID="btnLogout" runat="server" Text="退出" Icon="ControlPowerBlue" OnClick="logout_click"
                                ConfirmText="确定退出系统？">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </Toolbars>
                <Items>
                    <ext:ContentPanel ShowBorder="false" CssClass="header" ShowHeader="false" EnableBackgroundColor="true"
                        ID="ContentPanel1" runat="server">
                         <div class="logo">

                        </div>
                         <div class="title">
                             SVA System
                        </div>
						<div class="version">
                             Copyright &copy; 2013. VEI. All Rights Reserved.Version: <asp:Label ID="lblVersion" runat="server"></asp:Label>
                        </div>
                    </ext:ContentPanel>
                </Items>
            </ext:Region>
            <ext:Region ID="Region2" Split="true" EnableSplitTip="true" CollapseMode="Mini" Width="200px"
                Margins="0 0 0 0" ShowHeader="true" Title="菜单" EnableLargeHeader="false" Icon="Outline"
                EnableCollapse="true" Layout="Fit" Position="Left" runat="server">
            </ext:Region>
            <ext:Region ID="mainRegion" ShowHeader="false" Layout="Fit" Margins="0 0 0 0" Position="Center"
                runat="server">
                <Items>
                    <ext:TabStrip ID="mainTabStrip" EnableTabCloseMenu="true" ShowBorder="false" runat="server">
                        <Tabs>
                            <ext:Tab ID="Tab1" Title="首页" Layout="Fit" Icon="House" runat="server" IFrameUrl="AdminCenter.aspx"
                                EnableIFrame="true">
                                <Toolbars>
                                    <ext:Toolbar ID="Toolbar2" runat="server">
                                        <Items>
                                            <ext:ToolbarFill ID="ToolbarFill2" runat="server">
                                            </ext:ToolbarFill>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                            </ext:Tab>
                        </Tabs>
                    </ext:TabStrip>
                </Items>
            </ext:Region>
            <%-- <ext:Region ID="footerRegion" ShowHeader="false" Layout="Fit" Margins="0 0 0 0" Position="Bottom"  Height="24px"
                runat="server">
                <Items>
                    <ext:ContentPanel ShowBorder="false" CssClass="bottom" ShowHeader="false"
                        ID="ContentPanel3" runat="server" BodyStyle="background-color:#1C3E7E;">
                            <div class="copyright"> Copyright &copy; 2012. VEI. All Rights Reserved.Version: <asp:Label ID="lblVersion" runat="server"></asp:Label></div>
                    </ext:ContentPanel>
                </Items>
            </ext:Region>--%>
        </Regions>
    </ext:RegionPanel>
    <script src="js/default.js" type="text/javascript"></script>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>

