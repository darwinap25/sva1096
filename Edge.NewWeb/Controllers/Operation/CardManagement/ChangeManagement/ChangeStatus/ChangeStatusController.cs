﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;

namespace Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeStatus
{
    public class CardChangeStatusController
    {
        protected CardAdjustViewModel viewModel = new CardAdjustViewModel();

        public CardAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }
    }
}