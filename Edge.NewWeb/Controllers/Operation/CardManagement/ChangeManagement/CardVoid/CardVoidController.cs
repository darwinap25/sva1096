﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model.Domain.Operation;

namespace Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardVoid
{
    public class CardVoidController
    {
        protected CardAdjustViewModel viewModel = new CardAdjustViewModel();

        public CardAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }
    }
}