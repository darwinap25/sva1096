﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;
using System.Data;
using System.Text;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data.SqlClient;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangePoints
{
    public class CardChangePointsController
    {
        protected CardChangePointsViewModel viewModel = new CardChangePointsViewModel();

        public CardChangePointsViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string TradeManuallyCode)
        {
            Edge.SVA.BLL.Ord_TradeManually_H bll = new Edge.SVA.BLL.Ord_TradeManually_H();
            Edge.SVA.Model.Ord_TradeManually_H model = bll.GetModel(TradeManuallyCode);
            viewModel.MainTable = model;

            Edge.SVA.BLL.Ord_TradeManually_D blld = new Edge.SVA.BLL.Ord_TradeManually_D();
            DataSet ds = blld.GetList("TradeManuallyCode='" + TradeManuallyCode + "'");
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTool.AddStoreName(ds, "StoreName", "StoreID");
                viewModel.SubTable = ds.Tables[0];
            }
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Ord_TradeManually_H bll = new Edge.SVA.BLL.Ord_TradeManually_H();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "TradeManuallyCode desc";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));
            
            if (ds != null)
            {
                Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            }
            return ds;          
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Ord_TradeManually_H bll = new SVA.BLL.Ord_TradeManually_H();
                Edge.SVA.BLL.Ord_TradeManually_D blld = new SVA.BLL.Ord_TradeManually_D();

                bll.Add(viewModel.MainTable);

                if (viewModel.SubTable != null)
                {
                    foreach (DataRow dr in viewModel.SubTable.Rows)
                    {
                        Edge.SVA.Model.Ord_TradeManually_D mode = new SVA.Model.Ord_TradeManually_D();
                        mode.TradeManuallyCode = dr["TradeManuallyCode"].ToString();
                        mode.TraderAmount = ConvertTool.ToDecimal(dr["TraderAmount"].ToString());
                        mode.StoreID = ConvertTool.ToInt(dr["StoreID"].ToString());
                        mode.EarnPoint = ConvertTool.ToInt(dr["EarnPoint"].ToString());

                        blld.Add(mode);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Ord_TradeManually_H bll = new SVA.BLL.Ord_TradeManually_H();
                Edge.SVA.BLL.Ord_TradeManually_D blld = new SVA.BLL.Ord_TradeManually_D();

                bll.Update(viewModel.MainTable);

                if (viewModel.SubTable != null)
                {
                    string sql = "delete Ord_TradeManually_D where TradeManuallyCode='" + viewModel.MainTable.TradeManuallyCode + "'";
                    DBUtility.DbHelperSQL.ExecuteSql(sql);

                    foreach (DataRow dr in viewModel.SubTable.Rows)
                    {
                        Edge.SVA.Model.Ord_TradeManually_D mode = new SVA.Model.Ord_TradeManually_D();
                        mode.TradeManuallyCode = dr["TradeManuallyCode"].ToString();
                        mode.TraderAmount = ConvertTool.ToDecimal(dr["TraderAmount"].ToString());
                        mode.StoreID = ConvertTool.ToInt(dr["StoreID"].ToString());
                        mode.EarnPoint = ConvertTool.ToInt(dr["EarnPoint"].ToString());
                        blld.Add(mode);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataTable GetCardNumberByCase(string CaseType,int BrandID, string text)
        {
            try
            {
                DataTable dt = new DataTable();
                StringBuilder sb = new StringBuilder();
                sb.Append(" select c.CardNumber,d.MemberRegisterMobile,d.MemberMobilePhone from Card c join Member d on c.MemberID=d.MemberID");
                sb.Append(" where c.CardTypeID in (select CardTypeID from CardType where BrandID=" + BrandID + ")");
                switch (CaseType)
                {
                    case "mobile":
                        sb.Append(" and d.MemberMobilePhone='" + text + "'");
                        break;
                    case "cardno":
                        sb.Append(" and c.CardNumber='" + text + "'");
                        break;
                    case "resmobile":
                        sb.Append(" and d.MemberRegisterMobile='" + text + "'");
                        break;
                }
                return DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public int GetPonits(string cardnumber,decimal amount,int storeid)
        {
            int rtn = 0;

            IDataParameter[] parameters = { 
                 new SqlParameter("@CardNumber", SqlDbType.VarChar,512) , 
                 new SqlParameter("@ConsumeAMT", SqlDbType.Money), 
                 new SqlParameter("@EarnPoint", SqlDbType.Int) , 
                 new SqlParameter("@PointRuleType", SqlDbType.Int) , 
                 new SqlParameter("@StoreID ", SqlDbType.Int), 
             };
            parameters[0].Value = cardnumber;
            parameters[1].Value = amount;
            parameters[2].Direction = ParameterDirection.Output;
            parameters[3].Value = 0;
            parameters[4].Value = storeid;
            DBUtility.DbHelperSQL.RunProcedure("CalcEarnPoints", parameters, "ds");

            if (parameters[2].Value != null)
            {
                rtn = Convert.ToInt32(parameters[2].Value);
            }
            return rtn;
        }

        public string ApproveCardForApproveCode(Edge.SVA.Model.Ord_TradeManually_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return Resources.MessageTips.NoData;

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_TradeManually_H bll = new Edge.SVA.BLL.Ord_TradeManually_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_TradeManually_H().GetModel(model.TradeManuallyCode);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
        }

        private bool VoidCard(Edge.SVA.Model.Ord_TradeManually_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_TradeManually_H bll = new Edge.SVA.BLL.Ord_TradeManually_H();

            return bll.Update(model);
        }

        public string BatchVoidCard(List<string> idList)
        {
            int success = 0;
            int count = 0;
            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                Edge.SVA.Model.Ord_TradeManually_H model = new Edge.SVA.BLL.Ord_TradeManually_H().GetModel(id);
                if (this.VoidCard(model)) success++;
            }
            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }
    }
}