﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards
{
    public class BatchCreationOfCardsController
    {
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate();

            DataSet ds = new DataSet();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "CardCreateNumber";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddCouponTypeName(ds, "CardGradeName", "CardGradeID");
            Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
            Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }
    }
}