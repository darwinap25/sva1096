﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using System.Text.RegularExpressions;
using Edge.SVA.Model.Domain.Operation;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data.SqlClient;
using System.Text;

namespace Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos1
{
    public class ImportMemberInfos1Controller
    {
        protected ImportMemberInfos1ViewModel viewModel = new ImportMemberInfos1ViewModel();

        public ImportMemberInfos1ViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string CreateMemberNumber)
        {
            Edge.SVA.BLL.Ord_CreateMember_H bll = new Edge.SVA.BLL.Ord_CreateMember_H();
            Edge.SVA.Model.Ord_CreateMember_H model = bll.GetModel(CreateMemberNumber);
            viewModel.MainTable = model;
        }

        public ImportModelList list = new ImportModelList();

        public DataTable GetData(string path)
        {
            DataTable dt = ExcelTool.GetFirstSheet(path);

            return dt;
        }

        public void CheckData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                List<KeyValue> storelist = new List<KeyValue>();
                this.list.Error.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    if (string.IsNullOrEmpty(dr["CountryCode"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90469").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["MobileNumber"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90470").Replace("{0}", (i + 1).ToString())));
                    }
                    if (!string.IsNullOrEmpty(dr["CountryCode"].ToString()) && !string.IsNullOrEmpty(dr["MobileNumber"].ToString()))
                    {
                        storelist.Add(new KeyValue() { Key = (i + 1).ToString(), Value = (dr["CountryCode"].ToString() + dr["MobileNumber"].ToString()).Trim() });
                        if (storelist.Count > 1)
                        {
                            for (int j = 0; j < storelist.Count - 1; j++)
                            {
                                if (storelist[j].Value == (dr["CountryCode"].ToString() + dr["MobileNumber"].ToString()).Trim())
                                {
                                    if (i > 0)
                                    {
                                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90473").Replace("{0}", (i + 1).ToString())));
                                    }
                                    break;
                                }
                            }
                        }
                        if (ExistMember(dr["CountryCode"].ToString(), dr["MobileNumber"].ToString()))
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90474").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                    if (!string.IsNullOrEmpty(dr["Birthday"].ToString()))
                    {
                        bool IsDate = false;
                        try
                        {
                            DateTime date = Convert.ToDateTime(dr["Birthday"].ToString());
                            IsDate = true;
                        }
                        catch
                        {
                            IsDate = false;
                        }
                        if (!IsDate)
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90475").Replace("{0}", (i + 1).ToString())));
                        }
                    }

                    if (!string.IsNullOrEmpty(dr["Gender"].ToString()))
                    {
                        string gender = dr["Gender"].ToString();
                        if (gender.ToLower() != "male" && gender.ToLower() != "female")
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90476").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                }
            }
        }

        public List<Edge.SVA.Model.Ord_CreateMember_D>  AnalysisData(DataTable dt)
        {
            List<Edge.SVA.Model.Ord_CreateMember_D> modellist = new List<SVA.Model.Ord_CreateMember_D>();

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    Edge.SVA.Model.Ord_CreateMember_D model = new SVA.Model.Ord_CreateMember_D();
                    model.CreateMemberNumber = viewModel.MainTable.CreateMemberNumber;
                    model.CountryCode = dr["CountryCode"].ToString();
                    model.MobileNumber = dr["MobileNumber"].ToString();
                    model.EngFamilyName = dr["EngFamilyName"].ToString();
                    model.EngGivenName = dr["EngGivenName"].ToString();
                    model.ChiFamilyName = dr["ChiFamilyName"].ToString();
                    model.ChiGivenName = dr["ChiGivenName"].ToString();
                    model.Birthday = ConvertTool.ToDateTime(dr["Birthday"].ToString());
                    if (dr["Gender"].ToString().ToLower() == "male")
                    {
                        model.Gender = 1;
                    }
                    else if (dr["Gender"].ToString().ToLower() == "female")
                    {
                        model.Gender = 2;
                    }
                    else
                    {
                        model.Gender = 0;
                    }
                    model.HomeAddress = dr["HomeAddress"].ToString();
                    model.Email = dr["Email"].ToString();
                    model.Facebook = dr["Facebook"].ToString();
                    model.QQ = dr["QQ"].ToString();
                    model.MSN = dr["MSN"].ToString();
                    model.Weibo = dr["Weibo"].ToString();
                    model.OtherContact = dr["OtherContact"].ToString();
                    modellist.Add(model);
                }
            }
            return modellist;
        }

        public ExecResult Add(string path, out bool result)
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Ord_CreateMember_H bll = new SVA.BLL.Ord_CreateMember_H();
                Edge.SVA.BLL.Ord_CreateMember_D blld = new SVA.BLL.Ord_CreateMember_D();
                //获取数据
                DataTable dt = GetData(path);
                CheckData(dt);
                if (this.list.Error.Count == 0)
                {
                    //保存主表
                    bll.Add(viewModel.MainTable);
                    //保存子表
                    List<Edge.SVA.Model.Ord_CreateMember_D> modellist = AnalysisData(dt);
                    foreach (var item in modellist)
                    {
                        blld.Add(item);
                        this.list.Addrecords++;
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            result = this.list.Success;
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Ord_CreateMember_H bll = new SVA.BLL.Ord_CreateMember_H();
                Edge.SVA.BLL.Ord_CreateMember_D blld = new SVA.BLL.Ord_CreateMember_D();

                bll.Update(viewModel.MainTable);
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Ord_CreateMember_H bll = new Edge.SVA.BLL.Ord_CreateMember_H();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageIndex * pageSize + 1, (pageIndex + 1) * pageSize);

            if (ds != null)
            {
                Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            }

            return ds;
        }

        public DataSet GetDetailList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Ord_CreateMember_D bll = new Edge.SVA.BLL.Ord_CreateMember_D();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "CreateMemberNumber";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageIndex * pageSize + 1, (pageIndex + 1) * pageSize);

            if (ds != null)
            {
                Tools.DataTool.AddGender(ds, "GenderName", "Gender");
                ds.Tables[0].Columns.Add(new DataColumn("CardNumber", typeof(string)));
            }

            return ds;
        }

        public DataSet GetApproveDetailList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" Ord_CreateMember_D.*,Card.CardNumber from Ord_CreateMember_D");
            strSql.Append(" left join Member on Ord_CreateMember_D.CountryCode = Member.CountryCode");
            strSql.Append(" and Ord_CreateMember_D.MobileNumber=Member.MemberMobilePhone ");
            strSql.Append(" left join Card on Member.MemberID=Card.MemberID");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);

            DataSet ds = DBUtility.DbHelperSQL.Query(strSql.ToString());

            if (ds != null && ds.Tables.Count > 0)
            {
                Tools.DataTool.AddGender(ds, "GenderName", "Gender");
            }
            return ds;
        }

        public bool ExistMember(string strcountry, string strmobile)
        {
            Edge.SVA.BLL.Member bll = new SVA.BLL.Member();
            return bll.GetModelList("CountryCode='" + strcountry + "' and MemberMobilePhone='" + strmobile + "'").Count > 0 ? true : false;
        }

        public StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");

            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                }
                html.AppendFormat("</table></td></tr>");
            }
            else
            {
                html.AppendFormat("<tr><td align='right'></td><td>Add {0} records {1}.</td></tr>", this.list.Addrecords, "successfully");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "Import Member");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            this.list.Addrecords = 0;
            this.list.Updaterecords = 0;
            this.list.Deletedrecords = 0;
            this.list.Error.Clear();

            return html;
        }

        public DataSet ApproveImportMember(List<string> idList, out bool isSuccess, out int errorcode, out string approvecode)
        {
            approvecode = "";
            DataSet ds = new DataSet();
            errorcode = 0;
            isSuccess = true;
            try
            {
                foreach (var id in idList)
                {
                    IDataParameter[] parameters = { 
                        new SqlParameter("@CreateMemberNumber", SqlDbType.VarChar, 64),
                        new SqlParameter("@MemberRandomPWD", SqlDbType.Int),
                        new SqlParameter("@MemberInitPWD", SqlDbType.VarChar, 64),
                        new SqlParameter("@ReturnApprovalCode", SqlDbType.VarChar, 6),
                        new SqlParameter("@ErrorCode", SqlDbType.Int)};

                    parameters[0].Value = id;
                    parameters[1].Value = 0;
                    parameters[2].Value = "";
                    parameters[3].Direction = ParameterDirection.Output;
                    parameters[4].Direction = ParameterDirection.Output;

                    ds = DBUtility.DbHelperSQL.RunProcedure("DoCreateMember", parameters, "ds");

                    DataTable dt = ds.Tables[0];
                    dt.Columns.Add("ErrorCode", typeof(string));
                    dt.Columns.Add("ErrorCodeDesc", typeof(string));



                    approvecode = parameters[3].Value.ToString();
                    errorcode = Convert.ToInt32(parameters[4].Value);
                    if (errorcode != 0)
                    {
                        isSuccess = false;
                        break;
                    }
                }
            }
            catch
            {
                //return Edge.Messages.Manager.MessagesTool.instance.GetMessage("90167");
                return null;
            }
            return ds;
        }

        public List<DataSet> ApproveImportMember(List<string> idList,out DataTable dsTrans)
        {
            List<DataSet> dsList = new List<DataSet>();
            dsTrans = new DataTable();
            dsTrans.Columns.Add("TxnNo", typeof(string));
            dsTrans.Columns.Add("ApproveCode", typeof(string));
            dsTrans.Columns.Add("ErrorCode", typeof(string));
            dsTrans.Columns.Add("ApprovalMsg", typeof(string));

            try
            {
                foreach (var id in idList)
                {
                    IDataParameter[] parameters = { 
                        new SqlParameter("@CreateMemberNumber", SqlDbType.VarChar, 64),
                        new SqlParameter("@MemberRandomPWD", SqlDbType.Int),
                        new SqlParameter("@MemberInitPWD", SqlDbType.VarChar, 64),
                        new SqlParameter("@ReturnApprovalCode", SqlDbType.VarChar, 6),
                        new SqlParameter("@ErrorCode", SqlDbType.Int)};

                    parameters[0].Value = id;
                    parameters[1].Value = 1;
                    parameters[2].Value = "";
                    parameters[3].Direction = ParameterDirection.Output;
                    parameters[4].Direction = ParameterDirection.Output;

                    DataSet ds = DBUtility.DbHelperSQL.RunProcedure("DoCreateMember", parameters, "ds");

                    string errcode = parameters[4].Value.ToString();
                    DataRow dr = dsTrans.NewRow();
                    dr["TxnNo"] = id;
                    dr["ApproveCode"] = parameters[3].Value.ToString();
                    dr["ErrorCode"] = errcode;
                    if (errcode == "0")
                    {
                        dr["ApprovalMsg"] = "";
                    }
                    else
                    {
                        dr["ApprovalMsg"] = errcode == "-102" ? Messages.Manager.MessagesTool.instance.GetMessage("90477") : Resources.MessageTips.ApproveError;
                        Logger.Instance.WriteOperationLog("Approve ImportMember", "Error" + errcode);
                    }
                    dsTrans.Rows.Add(dr);

                    dsList.Add(ds);
                }
            }
            catch(Exception ex)
            {
                Logger.Instance.WriteOperationLog("Approve ImportMember ", ex.Message);
                return null;
            }
            return dsList;
        }

        private bool VoidImportMember(Edge.SVA.Model.Ord_CreateMember_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CreateMember_H bll = new Edge.SVA.BLL.Ord_CreateMember_H();

            return bll.Update(model);
        }

        public string BatchVoidCard(List<string> idList)
        {
            int success = 0;
            int count = 0;
            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                Edge.SVA.Model.Ord_CreateMember_H model = new Edge.SVA.BLL.Ord_CreateMember_H().GetModel(id);
                if (this.VoidImportMember(model)) success++;
            }
            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }
    }

    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<string> Error { get { return error; } }
        public int Addrecords { get; set; }
        public int Updaterecords { get; set; }
        public int Deletedrecords { get; set; }
        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                if (Addrecords == 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}