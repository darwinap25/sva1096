﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation.MemberManagement;
using System.Data;
using System.Text;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.Controllers.Operation.MemberManagement.ExportMemberInfos
{
    public class ExportMemberInfoController
    {
        protected ExportMemberInfoViewModel viewModel = new ExportMemberInfoViewModel();

        public ExportMemberInfoViewModel ViewModel
        {
            get { return viewModel; }
        }

        public DataSet GetTransList(string strWhere, int startIndex, int endIndex, out int recodeCount, string filedOrder)
        {
            //获取总条数首句
            string sqlcount = "select count(*) from ";
            //获取分页数据首句
            string sqlquery = "select * from ";

            StringBuilder strSql = new StringBuilder();
            if (string.IsNullOrEmpty(filedOrder))
            {
                filedOrder = "Member.MemberID";
            }
            else
            {
                filedOrder = "Member." + filedOrder;
            }
            strSql.AppendFormat("(SELECT ROW_NUMBER() OVER(order by {0}) as Row,", filedOrder);
            strSql.Append("Member.MemberID,Member.MemberEngFamilyName,Member.MemberEngGivenName,");
            strSql.Append("Member.MemberChiFamilyName,Member.MemberChiGivenName,");
            strSql.Append("Member.CountryCode,Member.MemberMobilePhone,Member.CreatedOn,");
            strSql.Append("Card.CardTypeID,Card.CardGradeID,Card.CardNumber,");
            strSql.Append("b1.AccountNumber as MSN,b2.AccountNumber as QQ,");
            strSql.Append("b3.AccountNumber as FaceBook,b4.AccountNumber as Weibo,");
            strSql.Append("b5.AccountNumber as OtherContact,");
            strSql.Append("(Member.MemberEngFamilyName+Member.MemberEngGivenName) as EnglishName,");
            strSql.Append("(Member.MemberChiFamilyName+Member.MemberChiGivenName) as ChineseName,");
            strSql.Append("Member.HomeAddress,Member.MemberRegisterMobile as RegisterMobile,");
            strSql.Append("Member.MemberMobilePhone as MobileNumber,Member.CreatedOn as RegisterDate,");
            strSql.Append("Member.MemberEmail as Email");
            strSql.Append(" from Member");
            strSql.Append(" join Card on Member.MemberID = Card.MemberID");
            strSql.Append(" left join (select * from MemberMessageAccount where MessageServiceTypeID = 4) b1 on Member.MemberID=b1.MemberID");
            strSql.Append(" left join (select * from MemberMessageAccount where MessageServiceTypeID = 5) b2 on Member.MemberID=b2.MemberID");
            strSql.Append(" left join (select * from MemberMessageAccount where MessageServiceTypeID = 7) b3 on Member.MemberID=b3.MemberID");
            strSql.Append(" left join (select * from MemberMessageAccount where MessageServiceTypeID = 8) b4 on Member.MemberID=b4.MemberID");
            strSql.Append(" left join (select * from MemberMessageAccount where MessageServiceTypeID = 99) b5 on Member.MemberID=b5.MemberID");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" ) TT");
            
            //获取总条数
            object obj = DBUtility.DbHelperSQL.GetSingle(sqlcount + strSql.ToString());
            if (obj == null)
            {
                recodeCount = 0;
            }
            else
            {
                recodeCount = Convert.ToInt32(obj);
            }

            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            //获取分页数据
            DataSet ds = DBUtility.DbHelperSQL.Query(sqlquery + strSql.ToString());

            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardType", "CardTypeID");
                Edge.Web.Tools.DataTool.AddCardGradeName(ds, "CardGrade", "CardGradeID");
            }
            return ds;
        }

        public string UpLoadFileToServer(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                string UploadFilePath = string.Empty;

                UploadFilePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/ExportMemberInfos/");

                if (!System.IO.Directory.Exists(UploadFilePath))
                {
                    System.IO.Directory.CreateDirectory(UploadFilePath);
                }

                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/ExportMemberInfos/" + "ExportMemberInfo" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".xls");

                System.IO.FileStream fs = null;
                try
                {
                    StringBuilder text = new StringBuilder(1000);
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                    #region Write To File
                    text.Append("EnglishName\tChineseName\tCountryCode\tMobileNumber\tRegisterMobile");
                    text.Append("\tCardNumber\tHomeAddress\tEmail\tFacebook\tQQ");
                    text.Append("\tMSN\tWeibo\tOtherContact\tRegisterDate");
                    text.Append("\r\n");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        text.AppendFormat("{0}\t", dr["EnglishName"].ToString());
                        text.AppendFormat("{0}\t", dr["ChineseName"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["CountryCode"].ToString()) ? "" : "'" + dr["CountryCode"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["MobileNumber"].ToString()) ? "" : "'" + dr["MobileNumber"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["RegisterMobile"].ToString()) ? "" : "'" + dr["RegisterMobile"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["CardNumber"].ToString()) ? "" : "'" + dr["CardNumber"].ToString());
                        text.AppendFormat("{0}\t", dr["HomeAddress"].ToString());
                        text.AppendFormat("{0}\t", dr["Email"].ToString());
                        text.AppendFormat("{0}\t", dr["Facebook"].ToString());
                        text.AppendFormat("{0}\t", dr["QQ"].ToString());
                        text.AppendFormat("{0}\t", dr["MSN"].ToString());
                        text.AppendFormat("{0}\t", dr["Weibo"].ToString());
                        text.AppendFormat("{0}\t", dr["OtherContact"].ToString());
                        text.AppendFormat("{0}\t", string.IsNullOrEmpty(dr["RegisterDate"].ToString()) ? "" : Convert.ToDateTime(dr["RegisterDate"].ToString()).ToString("yyyy-MM-dd"));
                        text.Append("\r\n");

                        if (text.Length >= 10000)
                        {
                            byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                            fs.Write(buffer, 0, buffer.Length);
                            text.Remove(0, text.Length);
                        }
                    }

                    if (text.Length > 0)
                    {
                        byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return fileName;
            }
            return "";
        }

        #region 下拉框绑定
        public void BindMemberSex(FineUI.DropDownList ddl)
        {
            ControlTool.BindKeyValueList(ddl, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberSex), false);
        }
        public void BindMemberMarital(FineUI.DropDownList ddl)
        {
            ControlTool.BindKeyValueList(ddl, ConstInfosRepostory.Singleton.GetKeyValueList(SVASessionInfo.SiteLanguage, ConstInfosRepostory.InfoType.MemberMarital), false);
        }
        public void BindEducation(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Education().GetList(" 1=1 ");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3", "EducationCode");
        }
        public void BindProfession(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.Profession().GetList(" 1=1 ");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3", "ProfessionCode");
        }
        public void BindSchoolDistrict(FineUI.DropDownList ddl, string lan)
        {
            ddl.Items.Clear();

            DataSet ds = new DataSet();

            string sql = @"select SchoolDistrict1,SchoolDistrict2,SchoolDistrict3 from School
                            group by SchoolDistrict1,SchoolDistrict2,SchoolDistrict3";
            ds = DBUtility.DbHelperSQL.Query(sql);
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    switch (lan)
                    {
                        case "en-us":
                            FineUI.ListItem li1 = new FineUI.ListItem() { Value = dr["SchoolDistrict1"].ToString().Trim(), Text = dr["SchoolDistrict1"].ToString().Trim() };
                            ddl.Items.Add(li1);
                            break;
                        case "zh-cn":
                            FineUI.ListItem li2 = new FineUI.ListItem() { Value = dr["SchoolDistrict2"].ToString().Trim(), Text = dr["SchoolDistrict2"].ToString().Trim() };
                            ddl.Items.Add(li2);
                            break;

                        case "zh-hk":
                            FineUI.ListItem li3 = new FineUI.ListItem() { Value = dr["SchoolDistrict3"].ToString().Trim(), Text = dr["SchoolDistrict3"].ToString().Trim() };
                            ddl.Items.Add(li3);
                            break;
                    }
                }
                ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
            }
        }
        public void BindSchoolName(FineUI.DropDownList ddl, string district, string lan)
        {
            ddl.Items.Clear();
            string strwhere = "";
            switch (lan)
            {
                case "en-us": strwhere = "SchoolDistrict1='" + district + "'"; break;
                case "zh-cn": strwhere = "SchoolDistrict2='" + district + "'"; break;
                case "zh-hk": strwhere = "SchoolDistrict3='" + district + "'"; break;
            }
            DataSet ds = new Edge.SVA.BLL.School().GetList(strwhere);
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "SchoolID", "SchoolName1", "SchoolName2", "SchoolName3", "SchoolCode");
        }
        public void BindStateCode(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = ds = new Edge.SVA.BLL.Nation().GetAllList();
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    FineUI.ListItem li = new FineUI.ListItem() { Value = dr["CountryCode"].ToString().Trim(), Text = dr["CountryCode"].ToString().Trim() };
                    ddl.Items.Add(li);
                }
            }
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }
        public void BindCountry(FineUI.DropDownList ddl)
        {
            ControlTool.BindCountry(ddl);
        }
        public void BindProvince(FineUI.DropDownList ddl,string countrycode)
        {
            ControlTool.BindProvince(ddl, countrycode);
        }
        public void BindCity(FineUI.DropDownList ddl, string provincecode)
        {
            ControlTool.BindCity(ddl, provincecode);
        }
        public void BindDistrict(FineUI.DropDownList ddl, string citycode)
        {
            ControlTool.BindDistrict(ddl, citycode);
        }
        public void BindCardType(FineUI.DropDownList ddl)
        {
            ControlTool.BindCardType(ddl);
        }
        public void BindCardGrade(FineUI.DropDownList ddl, int cardtypeid)
        {
            ControlTool.BindCardGrade(ddl, cardtypeid);
        }
        public void BindCardStatus(FineUI.DropDownList ddl)
        {
            ControlTool.BindCardStatus(ddl);
        }
        #endregion
    }
}