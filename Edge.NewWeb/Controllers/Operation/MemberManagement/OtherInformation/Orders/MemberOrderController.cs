﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Text;
using Edge.Messages.Manager;

namespace Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.Orders
{
    public class MemberOrderController
    {
        protected MemberOrderViewModel viewModel = new MemberOrderViewModel();

        public MemberOrderViewModel ViewModel
        {
            get { return viewModel; }
        }

        public DataSet GetTransactionList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append("* from Sales_H a");
            strSql.Append(" left join Member b on a.MemberID=b.MemberID");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            if (string.IsNullOrEmpty(filedOrder)) filedOrder = "a.TxnNo";
            strSql.Append(" order by " + filedOrder);

            DataSet ds = DBUtility.DbHelperSQL.Query(strSql.ToString());

            if (ds != null && ds.Tables.Count > 0)
            {
                Tools.DataTool.AddSaleStatusName(ds.Tables[0], "StatusName", "Status");
                Tools.DataTool.AddSaleTypeName(ds.Tables[0], "SalesTypeName", "SalesType");
            }
            return ds;
        }

        //public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        //{
        //    Edge.SVA.BLL.Sales_H bll = new Edge.SVA.BLL.Sales_H();

        //    DataSet ds = new DataSet();

        //    //获得总条数
        //    recodeCount = bll.GetRecordCount(strWhere);
        //    //获取排序字段
        //    string orderStr = "";
        //    if (!string.IsNullOrEmpty(sortFieldStr))
        //    {
        //        orderStr = sortFieldStr;
        //    }

        //    ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

        //    if (ds != null && ds.Tables.Count > 0)
        //    {
        //        Tools.DataTool.AddSaleStatusName(ds.Tables[0], "StatusName", "Status");
        //        Tools.DataTool.AddSaleTypeName(ds.Tables[0], "SalesTypeName", "SalesType");
        //    }

        //    return ds;
        //}

        public void LoadView(string TxnNo)
        {
            Edge.SVA.BLL.Sales_H bll = new Edge.SVA.BLL.Sales_H();
            Edge.SVA.Model.Sales_H model = bll.GetModel(TxnNo);
            viewModel.MainTable = model;

            string strsql1 = @"select top 1 a.StoreID,a.StoreCode,a.StoreName1,a.StoreName2,a.StoreName3,a.StoreFullDetail as SalesAddress,
                   a.Contact AS SalesContact,a.StoreTel AS SalesContactPhone,b.Status,b.SalesType,b.* from Store a,Sales_H b where TxnNo='" + TxnNo + "'";
            viewModel.HeadTable = DBUtility.DbHelperSQL.Query(strsql1).Tables[0];
            Tools.DataTool.AddSalesName(viewModel.HeadTable, "SalesName", "StoreCode");
            Tools.DataTool.AddSaleStatusName(viewModel.HeadTable, "StatusName", "Status");
            Tools.DataTool.AddSaleTypeName(viewModel.HeadTable, "SalesTypeName", "SalesType");
            Tools.DataTool.AddLogisticsProviderName(viewModel.HeadTable, "ProviderName", "LogisticsProviderID");

            string strsql2 = @"select a.*,b.TenderName1,b.TenderName2,
                   b.TenderName3 from Sales_T a left join Tender b on a.TenderID=b.TenderID where TxnNo='" + TxnNo + "'";
            viewModel.TendTable = DBUtility.DbHelperSQL.Query(strsql2).Tables[0];
            Tools.DataTool.AddTendName(viewModel.TendTable, "TenderNewName", "TenderID");

            string strsql3 = @"select Sales_D.ProdCode,Sales_D.ProdDesc,Product_Size.ProductSizeName1,
                Product_Size.ProductSizeName2,Product_Size.ProductSizeName3,Color.ColorName1,Color.ColorName2,
                Product.ProductSizeID,Product.ColorID,Color.ColorName3,Sales_D.Qty,Sales_D.NetAmount from Sales_D 
                left join Product on Sales_D.ProdCode=Product.ProdCode
                left join Product_Size on Product.ProductSizeID=Product_Size.ProductSizeID
                left join Color on Product.ColorID=Color.ColorID
                where Sales_D.TxnNo='" + TxnNo + "'";
            viewModel.DetailTable = DBUtility.DbHelperSQL.Query(strsql3).Tables[0];
            Tools.DataTool.AddProdName(viewModel.DetailTable, "ProdName", "ProdCode");
            Tools.DataTool.AddColorName(viewModel.DetailTable, "ColorName", "ColorID");
            Tools.DataTool.AddProdSizeName(viewModel.DetailTable, "ProductSizeName", "ProductSizeID");

            string strsql4 = @"select b.MemberRegisterMobile,CardNumber,MemberAddress,
                        (b.MemberEngGivenName+'.'+b.MemberEngFamilyName) as EnglishName,
                        (b.MemberChiFamilyName+b.MemberChiGivenName) as ChineseName,
	                    ISNULL(a.MemberMobilePhone,b.MemberMobilePhone) as MemberMobilePhone,b.MemberEmail
                        from Sales_H a join Member b on a.MemberID = b.MemberID and a.TxnNo='" + TxnNo + "'";
            viewModel.MemberTable = DBUtility.DbHelperSQL.Query(strsql4).Tables[0];            
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Sales_H bll = new SVA.BLL.Sales_H();
                bll.Update(viewModel.MainTable);

                Edge.SVA.BLL.Sales_T bllt = new SVA.BLL.Sales_T();
                bllt.Delete(viewModel.MainTable.TxnNo);
                if (viewModel.TendTable != null && viewModel.TendTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in viewModel.TendTable.Rows)
                    {
                        Edge.SVA.Model.Sales_T mode = new SVA.Model.Sales_T();
                        mode.TxnNo = viewModel.MainTable.TxnNo;
                        mode.SeqNo = ConvertTool.ToInt(dr["SeqNo"].ToString());
                        mode.TenderID = ConvertTool.ToInt(dr["TenderID"].ToString());
                        mode.TenderCode = dr["TenderCode"].ToString();
                        mode.TenderAmount = ConvertTool.ToDecimal(dr["TenderAmount"].ToString());
                        mode.ExchangeRate = ConvertTool.ToDecimal(dr["ExchangeRate"].ToString());
                        mode.LocalAmount = ConvertTool.ToDecimal(dr["TenderAmount"].ToString()) * ConvertTool.ToDecimal(dr["ExchangeRate"].ToString());
                        bllt.Add(mode);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        //private bool VoidMemberOrder(Edge.SVA.Model.Sales_H model)
        //{
        //    if (model == null) return false;

        //    if (model.Status != 3 && model.Status != 4) return false;
        //    if (model.LogisticsProviderID == null || model.LogisticsProviderID == -1) return false;

        //    model.Status = 6;
        //    model.DeliverStartOn = DateTime.Now;

        //    Edge.SVA.BLL.Sales_H bll = new Edge.SVA.BLL.Sales_H();

        //    return bll.Update(model);
        //}

        //public string BatchVoidCard(List<string> idList)
        //{
        //    int success = 0;
        //    int count = 0;
        //    foreach (string id in idList)
        //    {
        //        if (string.IsNullOrEmpty(id)) continue;
        //        count++;
        //        Edge.SVA.Model.Sales_H model = new Edge.SVA.BLL.Sales_H().GetModel(id);
        //        if (this.VoidMemberOrder(model)) success++;
        //    }
        //    return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        //}

        #region 导入
        public ImportModelList list = new ImportModelList();

        public void CheckData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                List<KeyValue> storelist = new List<KeyValue>();
                this.list.Error.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    if (string.IsNullOrEmpty(dr["OrderNumber"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90479").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["LogisticsProviderCode"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90480").Replace("{0}", (i + 1).ToString())));
                    }
                    if (string.IsNullOrEmpty(dr["DeliveryNumber"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90481").Replace("{0}", (i + 1).ToString())));
                    }
                    if (!string.IsNullOrEmpty(dr["OrderNumber"].ToString()))
                    {
                        storelist.Add(new KeyValue() { Key = (i + 1).ToString(), Value = dr["OrderNumber"].ToString() });
                        if (storelist.Count > 1)
                        {
                            for (int j = 0; j < storelist.Count - 1; j++)
                            {
                                if (storelist[j].Value == dr["OrderNumber"].ToString())
                                {
                                    if (i > 0)
                                    {
                                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90482").Replace("{0}", (i + 1).ToString())));
                                    }
                                    break;
                                }
                            }
                        }
                        if (!ExistsTxNO(dr["OrderNumber"].ToString()))
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90483").Replace("{0}", (i + 1).ToString())));
                        }
                        else
                        {
                            if (!CheckOrderStatus(dr["OrderNumber"].ToString()))
                            {
                                this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90485").Replace("{0}", (i + 1).ToString())));
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(dr["LogisticsProviderCode"].ToString()))
                    {
                        if (!ExistsCompany(dr["LogisticsProviderCode"].ToString()))
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90484").Replace("{0}", (i + 1).ToString())));
                        }
                    }
                }
            }
        }

        public List<string> GetImportModel(DataTable dt)
        {
            List<string> sqllist = new List<string>();
            Edge.SVA.BLL.LogisticsProvider blllog = new SVA.BLL.LogisticsProvider();
            Edge.SVA.Model.LogisticsProvider modellog = new SVA.Model.LogisticsProvider();
            CheckData(dt);
            if (this.list.Error.Count == 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string strsql = "";
                    DataRow dr = dt.Rows[i];
                    modellog = blllog.GetModelList("LogisticsProviderCode='" + dr["LogisticsProviderCode"].ToString() + "'")[0];
                    strsql = @" Update Sales_H set LogisticsProviderID =" + modellog.LogisticsProviderID + ",DeliveryNumber='" + dr["DeliveryNumber"].ToString() + "'"
                        + " where TxnNo='" + dr["OrderNumber"].ToString() + "'";
                    sqllist.Add(strsql);
                }
            }
            return sqllist;
        }

        public ExecResult ImportData(DataTable dt)
        {
            List<string> sqllist = GetImportModel(dt);
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Sales_H bll = new SVA.BLL.Sales_H();
                foreach (var item in sqllist)
                {
                    DBUtility.DbHelperSQL.ExecuteSql(item);
                    this.list.Updaterecords++;
                }
            }
            catch(Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public bool ExistsTxNO(string Txno)
        {
            Edge.SVA.BLL.Sales_H bll = new SVA.BLL.Sales_H();
            return bll.GetRecordCount("TxnNo='" + Txno + "'") > 0 ? true : false;
        }

        public bool CheckOrderStatus(string Txno)
        {
            Edge.SVA.BLL.Sales_H bll = new SVA.BLL.Sales_H();
            Edge.SVA.Model.Sales_H model = new SVA.Model.Sales_H();
            model = bll.GetModel(Txno);
            if (model != null)
            {
                if (model.Status != 3 && model.Status != 4)
                {
                    return false;
                }
            }
            return true;
        }

        public bool ExistsCompany(string LogisticsProviderCode)
        {
            Edge.SVA.BLL.LogisticsProvider bll = new SVA.BLL.LogisticsProvider();

            return bll.GetRecordCount("LogisticsProviderCode='" + LogisticsProviderCode + "'") > 0 ? true : false;
        }

        public StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");

            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                }
                html.AppendFormat("</table></td></tr>");
            }
            else
            {
                html.AppendFormat("<tr><td align='right'></td><td>Update {0} records {1}.</td></tr>", this.list.Updaterecords, "successfully");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "Import Member Order");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            this.list.Updaterecords = 0;
            this.list.Error.Clear();

            return html;
        }

        #endregion

        public string PayOrders(string id)
        {
            Edge.SVA.Model.Sales_H model = new Edge.SVA.BLL.Sales_H().GetModel(id);
            if (model == null) return "No Data!";
            if (model.Status != 3) return MessagesTool.instance.GetMessage("90488");
            if (model.LogisticsProviderID == null || model.LogisticsProviderID == -1) return MessagesTool.instance.GetMessage("90486");

            bool flag = false;
            string sql1 = "select sum(TenderAmount) as TenderAmount from Sales_T where TxnNo='" + id + "'";
            string sql2 = "select sum(NetAmount) as NetAmount from Sales_D where TxnNo='" + id + "'";
            DataTable dt1= DBUtility.DbHelperSQL.Query(sql1).Tables[0];
            DataTable dt2 = DBUtility.DbHelperSQL.Query(sql2).Tables[0];
            if (dt1 != null)
            {
                if (!string.IsNullOrEmpty(dt1.Rows[0]["TenderAmount"].ToString()))
                {
                    if (ConvertTool.ToDecimal(dt1.Rows[0]["TenderAmount"].ToString()) == ConvertTool.ToDecimal(dt2.Rows[0]["NetAmount"].ToString()))
                    {
                        flag = true;
                    }
                }
            }
            if (!flag) return MessagesTool.instance.GetMessage("90489"); 

            model.Status = 4;
            model.PaymentDoneOn = DateTime.Now;
            Edge.SVA.BLL.Sales_H bll = new Edge.SVA.BLL.Sales_H();
            bll.Update(model);
            return "Pay Success!";
        }

        public string ShipOrders(string id)
        {
            Edge.SVA.Model.Sales_H model = new Edge.SVA.BLL.Sales_H().GetModel(id);
            if (model == null) return "No Data!";

            if (model.Status != 3 && model.Status != 4) return MessagesTool.instance.GetMessage("90488");
            if (model.LogisticsProviderID == null || model.LogisticsProviderID == -1) return MessagesTool.instance.GetMessage("90486");

            model.Status = 6;
            model.DeliverStartOn = DateTime.Now;
            Edge.SVA.BLL.Sales_H bll = new Edge.SVA.BLL.Sales_H();
            bll.Update(model);
            return "Pay Success!";
        }
    }
    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<string> Error { get { return error; } }
        public int Addrecords { get; set; }
        public int Updaterecords { get; set; }
        public int Deletedrecords { get; set; }
        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                if (Updaterecords == 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}