﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.Operation.CouponManagement.BatchCreationOfCoupons
{
    public class BatchCreationOfCouponsController
    {
        private const string fields = " [CouponCreateNumber],[CouponTypeID],[ApprovalCode],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate]";
        private const string condition = " EXISTS (SELECT dbo.CouponType.CouponTypeID FROM dbo.CouponType INNER JOIN dbo.Brand ON dbo.CouponType.BrandID = dbo.Brand.BrandID AND dbo.CouponType.CouponTypeID = dbo.Ord_CouponBatchCreate.CouponTypeID AND dbo.CouponType.BrandID IN {0})";
        private const string andCondition = " and EXISTS (SELECT dbo.CouponType.CouponTypeID FROM dbo.CouponType INNER JOIN dbo.Brand ON dbo.CouponType.BrandID = dbo.Brand.BrandID AND dbo.CouponType.CouponTypeID = dbo.Ord_CouponBatchCreate.CouponTypeID AND dbo.CouponType.BrandID IN {0})";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            string brandids = SVASessionInfo.CurrentUser.SqlConditionBrandIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(brandids))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, brandids);
                }
            } 
            else
            {
                strWhere += string.Format(andCondition, brandids);
            }

            string OrderField = "CouponCreateNumber";
            bool OrderType = false;
            if (sortFieldStr.ToLower().EndsWith(" asc"))
            {
                OrderType = true;
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" asc"));
            }
            else if (sortFieldStr.ToLower().EndsWith(" desc"))
            {
                OrderField = sortFieldStr.Substring(0, sortFieldStr.ToLower().IndexOf(" desc"));
            }

            Edge.SVA.BLL.Ord_CouponBatchCreate bll = new Edge.SVA.BLL.Ord_CouponBatchCreate()
            {
                StrWhere = strWhere,
                Order = OrderField,//"CouponCreateNumber",
                Fields = fields,
                Ascending = OrderType//false
            };

            System.Data.DataSet ds = null;
            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

    }
}