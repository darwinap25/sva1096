﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Operation;

namespace Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement.ChangeStatus
{
    public class ChangeStatusController
    {
        protected CouponAdjustViewModel viewModel = new CouponAdjustViewModel();

        public CouponAdjustViewModel ViewModel
        {
            get { return viewModel; }
        }
    }
}