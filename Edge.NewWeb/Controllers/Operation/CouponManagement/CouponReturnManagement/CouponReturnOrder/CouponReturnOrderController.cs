﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Edge.Web.Tools;


namespace Edge.Web.Controllers.Operation.CouponManagement.CouponReturnManagement.CouponReturnOrder
{
    public class CouponReturnOrderController
    {
        private const string fields = "CouponReturnNumber,ApproveStatus,ApprovalCode,FromStoreID,StoreID,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy";
        private const string condition = " StoreID in {0} and FromStoreID in {0} ";
        private const string andCondition = " and StoreID in {0} and FromStoreID in {0} ";
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            string stores = SVASessionInfo.CurrentUser.SqlConditionStoreIDs;
            if (string.IsNullOrEmpty(strWhere))
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = " 1!=1";
                }
                else
                {
                    strWhere = string.Format(condition, stores);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(stores))
                {
                    strWhere = strWhere + " and 1!=1";
                }
                else
                {
                    strWhere += string.Format(andCondition, stores);
                }
            }
            Edge.SVA.BLL.Ord_CouponReturn_H bll = new Edge.SVA.BLL.Ord_CouponReturn_H()
            {
                StrWhere = strWhere,
                Order = "CouponReturnNumber",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;
            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            return ds;
        }

        public DataSet GetDetailList(int type, string couponTypeID, string batchCouponID, string couponQty, string fromStoreID, string fromStoreName, string para1)
        {
            string sql = "";

            string strWhere = "";
            string couponTypeName = DALTool.GetStringByCulture("CouponTypeName1", "CouponTypeName2", "CouponTypeName3");
            if (couponTypeID != "-1") 
            {
                strWhere += " and a.CouponTypeID = " + couponTypeID;
            }
            if (batchCouponID != "")
            {
                strWhere += " and a.BatchCouponID = " + batchCouponID;
            }
            if (type == 0) 
            {
                string topN = "";
                if (!string.IsNullOrEmpty(couponQty)) 
                {
                    topN = "top " + couponQty;
                }
                sql = " select " + topN + " '" + fromStoreName + "' FromStoreName,a.CouponTypeID,b.CouponTypeCode,b." + couponTypeName + " CouponTypeName,c.BatchCouponCode,a.CouponNumber,a.CouponAmount,a.Status,a.StockStatus,CONVERT(varchar(100), a.CouponIssueDate, 111) CouponIssueDate,CONVERT(varchar(100), a.CouponExpiryDate, 111) CouponExpiryDate,d.CouponUID from Coupon a,CouponType b,BatchCoupon c,CouponUIDMap d where a.CouponTypeID = b.CouponTypeID and a.BatchCouponID = c.BatchCouponID and a.CouponNumber = d.CouponNumber and a.CouponTypeID = d.CouponTypeID and a.Status = 1 and a.LocateStoreID = " + fromStoreID + strWhere + " order by a.CouponNumber ";
            }
            else if (type == 1)
            {
                sql = " select top " + couponQty + " '" + fromStoreName + "' FromStoreName,a.CouponTypeID,b.CouponTypeCode,b." + couponTypeName + " CouponTypeName,c.BatchCouponCode,a.CouponNumber,a.CouponAmount,a.Status,a.StockStatus,CONVERT(varchar(100), a.CouponIssueDate, 111) CouponIssueDate,CONVERT(varchar(100), a.CouponExpiryDate, 111) CouponExpiryDate,d.CouponUID from Coupon a,CouponType b,BatchCoupon c,CouponUIDMap d where a.CouponTypeID = b.CouponTypeID and a.BatchCouponID = c.BatchCouponID and a.CouponNumber = d.CouponNumber and a.CouponTypeID = d.CouponTypeID and a.Status = 1 and a.LocateStoreID = " + fromStoreID + strWhere + " and a.CouponNumber >= '" + para1 + "' order by a.CouponNumber ";
            }
            else
            {
                string couponNumber = "99999999";
                Edge.SVA.Model.CouponUIDMap model = new Edge.SVA.BLL.CouponUIDMap().GetModel(para1);
                if (model != null)
                {
                    couponNumber = model.CouponNumber;
                }
                sql = " select top " + couponQty + " '" + fromStoreName + "' FromStoreName,a.CouponTypeID,b.CouponTypeCode,b." + couponTypeName + " CouponTypeName,c.BatchCouponCode,a.CouponNumber,a.CouponAmount,a.Status,a.StockStatus,CONVERT(varchar(100), a.CouponIssueDate, 111) CouponIssueDate,CONVERT(varchar(100), a.CouponExpiryDate, 111) CouponExpiryDate,d.CouponUID from Coupon a,CouponType b,BatchCoupon c,CouponUIDMap d where a.CouponTypeID = b.CouponTypeID and a.BatchCouponID = c.BatchCouponID and a.CouponNumber = d.CouponNumber and a.CouponTypeID = d.CouponTypeID and a.Status = 1 and a.LocateStoreID = " + fromStoreID + strWhere + " and a.CouponNumber >= '" + couponNumber + "' order by a.CouponNumber ";
            }

            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("StockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCouponTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StockStatusName"] = Edge.Web.Tools.DALTool.GetCouponStockStatusName(Convert.ToInt32(dr["StockStatus"])); //Add By Robin 2014-06-20
                /*if (Convert.ToString(dr["Status"]) == "0")
                {
                    dr["StatusName"] = "未被领用";
                }
                else if (Convert.ToString(dr["Status"]) == "1")
                {
                    dr["StatusName"] = "已被领取（激活）";
                }
                else if (Convert.ToString(dr["Status"]) == "2")
                {
                    dr["StatusName"] = "已被使用";
                }
                else if (Convert.ToString(dr["Status"]) == "3")
                {
                    dr["StatusName"] = "过期";
                }
                else
                {
                    dr["StatusName"] = "作废";
                }
                if (Convert.ToString(dr["StockStatus"]) == "1")
                {
                    dr["StockStatusName"] = "总部未确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "2")
                {
                    dr["StockStatusName"] = "总部确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "3")
                {
                    dr["StockStatusName"] = "总部发现有优惠券损坏";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "4")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "5")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单，并发货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "6")
                {
                    dr["StockStatusName"] = "店铺确认收货";
                }
                else
                {
                    dr["StockStatusName"] = "店铺退货给总部";
                }*/
            }

            return ds;
        }

        public DataSet GetDetailList(string couponReturnNumber,string fromStoreName)
        {
            string sql = "";

            string couponTypeName = DALTool.GetStringByCulture("CouponTypeName1", "CouponTypeName2", "CouponTypeName3");

            sql = " select '" + fromStoreName + "' FromStoreName,a.CouponTypeID,b.CouponTypeCode,b." + couponTypeName + " CouponTypeName,a.BatchCouponCode,a.FirstCouponNumber CouponNumber,c.CouponAmount,c.Status,c.StockStatus,CONVERT(varchar(100), c.CouponIssueDate, 111) CouponIssueDate,CONVERT(varchar(100), c.CouponExpiryDate, 111) CouponExpiryDate,d.CouponUID from Ord_CouponReturn_D a,CouponType b,Coupon c,CouponUIDMap d where a.CouponTypeID = b.CouponTypeID and a.FirstCouponNumber = c.CouponNumber and a.CouponTypeID = c.CouponTypeID and a.FirstCouponNumber = d.CouponNumber and a.CouponTypeID = d.CouponTypeID and a.CouponReturnNumber = '" + couponReturnNumber + "' order by a.FirstCouponNumber ";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = ds.Tables[0];
            dt.Columns.Add(new DataColumn("StatusName", typeof(string)));
            dt.Columns.Add(new DataColumn("StockStatusName", typeof(string)));
            foreach (DataRow dr in dt.Rows)
            {
                dr["StatusName"] = Edge.Web.Tools.DALTool.GetCouponTypeStatusName(Convert.ToInt32(dr["Status"])); //Add By Robin 2014-06-20
                dr["StockStatusName"] = Edge.Web.Tools.DALTool.GetCouponStockStatusName(Convert.ToInt32(dr["StockStatus"])); //Add By Robin 2014-06-20
                /*if (Convert.ToString(dr["Status"]) == "0")
                {
                    dr["StatusName"] = "未被领用";
                }
                else if (Convert.ToString(dr["Status"]) == "1")
                {
                    dr["StatusName"] = "已被领取（激活）";
                }
                else if (Convert.ToString(dr["Status"]) == "2")
                {
                    dr["StatusName"] = "已被使用";
                }
                else if (Convert.ToString(dr["Status"]) == "3")
                {
                    dr["StatusName"] = "过期";
                }
                else
                {
                    dr["StatusName"] = "作废";
                }
                if (Convert.ToString(dr["StockStatus"]) == "1")
                {
                    dr["StockStatusName"] = "总部未确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "2")
                {
                    dr["StockStatusName"] = "总部确认收货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "3")
                {
                    dr["StockStatusName"] = "总部发现有优惠券损坏";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "4")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "5")
                {
                    dr["StockStatusName"] = "总部收到了店铺的订单，并发货";
                }
                else if (Convert.ToString(dr["StockStatus"]) == "6")
                {
                    dr["StockStatusName"] = "店铺确认收货";
                }
                else
                {
                    dr["StockStatusName"] = "店铺退货给总部";
                }*/
            }

            return ds;
        }
    }
}