﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Notification
{
    public class NotificationAddController:NotificationController
    {
        public void LoadViewModel(string lan)
        {
            this.viewModel.BrandInfoList = SVASessionInfo.CurrentUser.BrandInfoList;
            this.viewModel.TransactionTypeList = TransactionTypeRepostory.Singleton.GetTransactionTypeList(lan);
        }

        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.MessageTemplate bllmt=new Edge.SVA.BLL.MessageTemplate();
                int messageTemplateID=bllmt.Add(this.ViewModel.MainTable);
                Edge.SVA.BLL.MessageTemplateDetail bllmtd = new Edge.SVA.BLL.MessageTemplateDetail();
                foreach (var item in this.ViewModel.MessageTemplateDetailList)
                {
                    item.MessageTemplateID = messageTemplateID;
                    if (item.KeyID==0)
                    {
                        bllmtd.Add(item);
                    } 
                    else
                    {
                        bllmtd.Update(item);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
    }
}