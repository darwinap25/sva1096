﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Notification
{
    public class NotificationModifyController:NotificationController
    {
        public void LoadViewModel(int messageTemplateID,string lan)
        {
            this.viewModel.BrandInfoList = SVASessionInfo.CurrentUser.BrandInfoList;
            this.viewModel.TransactionTypeList = TransactionTypeRepostory.Singleton.GetTransactionTypeList(lan);
            Edge.SVA.BLL.MessageTemplate bllmt = new Edge.SVA.BLL.MessageTemplate();
            this.viewModel.MainTable = bllmt.GetModel(messageTemplateID);
            Edge.SVA.BLL.MessageTemplateDetail bllmtd=new   Edge.SVA.BLL.MessageTemplateDetail();
            this.viewModel.MessageTemplateDetailList = bllmtd.GetModelList(" MessageTemplateID=" + messageTemplateID + " order by MessageServiceTypeID asc");
        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.MessageTemplate bllmt = new Edge.SVA.BLL.MessageTemplate();
                bllmt.Update(this.viewModel.MainTable);
                int messageTemplateID = this.viewModel.MainTable.MessageTemplateID;
                Edge.SVA.BLL.MessageTemplateDetail bllmtd = new Edge.SVA.BLL.MessageTemplateDetail();
                foreach (var item in this.ViewModel.MessageTemplateDetailList)
                {
                    item.MessageTemplateID = messageTemplateID;
                    if (item.KeyID == 0)
                    {
                        bllmtd.Add(item);
                    }
                    else
                    {
                        bllmtd.Update(item);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
    }
}