﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;
using System.Data.SqlClient;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Notification.MsgTemplate
{
    public class MsgTemplateController
    {
        public string TransactionTypeID { get; set; } 

        protected MessageTemplateViewModel viewModel = new MessageTemplateViewModel();

        public MessageTemplateViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadPublicInfos(string lan)
        {
            this.viewModel.BrandInfoList = SVASessionInfo.CurrentUser.BrandInfoList;
            this.viewModel.TransactionTypeList = TransactionTypeRepostory.Singleton.GetTransactionTypeList(lan);
            this.viewModel.MessageSerivceTypeList = ConstInfosRepostory.Singleton.GetKeyValueList(lan, ConstInfosRepostory.InfoType.MessageServiceType);

        }
        public void LoadViewModel(int messageTemplateID, string lan)
        {
            LoadPublicInfos(lan);

            Edge.SVA.BLL.MessageTemplate bllmt = new Edge.SVA.BLL.MessageTemplate();
            this.viewModel.MainTable = bllmt.GetModel(messageTemplateID);
            Edge.SVA.BLL.MessageTemplateDetail bllmtd = new Edge.SVA.BLL.MessageTemplateDetail();
            this.viewModel.MessageTemplateDetailViewModelList = bllmtd.GetModelList(" MessageTemplateID=" + messageTemplateID + " order by MessageServiceTypeID asc");
            foreach (var item in this.viewModel.MessageTemplateDetailViewModelList)
            {
                SetViewModelViewInfo(lan, item);
            }
        }
        private static void SetViewModelViewInfo(string lan, MessageTemplateDetailViewModel item)
        {
            if (item.MessageServiceTypeID.HasValue)
            {
                item.MessageServiceTypeDesc = ConstInfosRepostory.Singleton.GetKeyValueDesc(item.MessageServiceTypeID.Value.ToString(), lan, ConstInfosRepostory.InfoType.MessageServiceType);
            }
            if (item.status.HasValue)
            {
                item.StatusDesc = ConstInfosRepostory.Singleton.GetKeyValueDesc(item.status.Value.ToString(), lan, ConstInfosRepostory.InfoType.ValidType);
            }
        }
        public bool CanAddMessageTemplateDetailViewModel(MessageTemplateDetailViewModel model)
        {
            return !this.viewModel.MessageTemplateDetailViewModelList.Exists(m => m.MessageServiceTypeID == model.MessageServiceTypeID);
        }
        public void AddMessageTemplateDetailViewModel(string lan, MessageTemplateDetailViewModel model)
        {
            SetViewModelViewInfo(lan, model);
            this.viewModel.MessageTemplateDetailViewModelList.Add(model);
        }
        public void UpdateMessageTemplateDetailViewModel(string lan, MessageTemplateDetailViewModel model)
        {
            MessageTemplateDetailViewModel vm = this.viewModel.MessageTemplateDetailViewModelList.Find(mm => mm.ObjectKey.Equals(model.ObjectKey));
            if (vm != null)
            {
                DataCopyUtil.CopyData(model, vm);
                SetViewModelViewInfo(lan, vm);
            }
        }
        public void RemoveMessageTemplateDetailViewModelList(List<MessageTemplateDetailViewModel> modelList)
        {
            foreach (var item in modelList)
            {
                this.viewModel.MessageTemplateDetailViewModelList.RemoveAll(mm => mm.Equals(item));
                if (item.KeyID != 0)
                {
                    this.viewModel.DeleteTemplateDetailIDList.Add(item.KeyID);
                }
            }
        }
        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.MessageTemplate bllmt = new Edge.SVA.BLL.MessageTemplate();
                int messageTemplateID = bllmt.Add(this.ViewModel.MainTable);
                Edge.SVA.BLL.MessageTemplateDetail bllmtd = new Edge.SVA.BLL.MessageTemplateDetail();
                foreach (var item in this.ViewModel.MessageTemplateDetailViewModelList)
                {
                    item.MessageTemplateID = messageTemplateID;
                    bllmtd.Add(item);
                }
                SVASessionInfo.MsgTemplateController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.MessageTemplate bllmt = new Edge.SVA.BLL.MessageTemplate();
                bllmt.Update(this.viewModel.MainTable);
                int messageTemplateID = this.viewModel.MainTable.MessageTemplateID;
                Edge.SVA.BLL.MessageTemplateDetail bllmtd = new Edge.SVA.BLL.MessageTemplateDetail();
                StringBuilder sb = new StringBuilder();
                foreach (var item in this.viewModel.DeleteTemplateDetailIDList)
                {
                    sb.Append(",");
                    sb.Append(item);
                }
                if (sb.Length >= 1)
                {
                    bllmtd.DeleteList(sb.ToString().Substring(1));
                }

                foreach (var item in this.ViewModel.MessageTemplateDetailViewModelList)
                {
                    item.MessageTemplateID = messageTemplateID;
                    if (item.KeyID == 0)
                    {
                        bllmtd.Add(item);
                    }
                    else
                    {
                        bllmtd.Update(item);
                    }
                }
                SVASessionInfo.MsgTemplateController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.MessageTemplate bll = new Edge.SVA.BLL.MessageTemplate();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "MessageTemplateCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddBrandInfoDesc(ds, "BrandDesc", "BrandID");
            Tools.DataTool.AddTransactionTypeDesc(ds, "TransactionTypeDesc", "OprID");

            //Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            //Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            //Tools.DataTool.AddMessageType(ds, "MessageType");
            //Tools.DataTool.AddMessageServiceType(ds, "MessageServiceTypeID");            

            return ds;
        }

        #region 事务处理
        public ExecResult AddData()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                //主表保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MsgTemplate bll = new SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MsgTemplate();
                int messageTemplateID = bll.Add(viewModel.MainTable, trans);

                //第一个子表的保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MessageTemplateDetail blldetail = new SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MessageTemplateDetail();
                Edge.SVA.Model.MessageTemplateDetail model = new SVA.Model.MessageTemplateDetail();
                foreach (var item in this.ViewModel.MessageTemplateDetailViewModelList)
                {
                    item.MessageTemplateID = messageTemplateID;
                    blldetail.Add(item, trans);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
                SVASessionInfo.MsgTemplateController = null;
            }
            return rtn;
        }

        public ExecResult UpdateData()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                //主表保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MsgTemplate bll = new SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MsgTemplate();
                bll.Update(viewModel.MainTable, trans);

                //主键ID
                int keyid = viewModel.MainTable.MessageTemplateID;

                //第一个子表的保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MessageTemplateDetail blldetail = new SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MessageTemplateDetail();
                blldetail.Delete(keyid, trans);
                foreach (var item in this.ViewModel.MessageTemplateDetailViewModelList)
                {
                    item.MessageServiceTypeID = keyid;
                    blldetail.Add(item, trans);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
                SVASessionInfo.MsgTemplateController = null;
            }
            return rtn;
        }

        public ExecResult DeleteData(List<int> MessageTemplateIDList)
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                foreach (var MessageTemplateID in MessageTemplateIDList)
                {
                    //主表删除
                    Edge.SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MsgTemplate bll = new SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MsgTemplate();
                    bll.Delete(MessageTemplateID, trans);

                    //第一个子表的删除
                    Edge.SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MessageTemplateDetail blldetail = new SVA.BLL.Domain.BasicInformationSetting.Notification.MsgTemplate.MessageTemplateDetail();
                    blldetail.Delete(MessageTemplateID, trans);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
                SVASessionInfo.MsgTemplateController = null;
            }
            return rtn;
        }
        #endregion
    }
}