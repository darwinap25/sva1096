﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.DistributeTemplate
{
    public class DistributeTemplateController
    {
        protected DistributeTemplateViewModel viewModel = new DistributeTemplateViewModel();

        public DistributeTemplateViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int DistributionID)
        {
            Edge.SVA.BLL.DistributeTemplate bll = new Edge.SVA.BLL.DistributeTemplate();
            Edge.SVA.Model.DistributeTemplate model = bll.GetModel(DistributionID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.DistributeTemplate bll = new SVA.BLL.DistributeTemplate();

                //保存
                if (this.IsExists(viewModel.MainTable.DistributionID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.DistributeTemplate bll = new SVA.BLL.DistributeTemplate();

                //保存
                if (this.IsExists( viewModel.MainTable.DistributionID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.DistributeTemplate bll = new Edge.SVA.BLL.DistributeTemplate();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "DistributionCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            DataTool.AddDistributionDesc(ds, "DistributionDesc", "DistributionID");

            return ds;
        }

        protected bool IsExists(int DistributionID)
        {
            Edge.SVA.BLL.DistributeTemplate bllmc = new SVA.BLL.DistributeTemplate();
            string strwhere = " DistributionID =" + DistributionID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string DistributionCode,int DistributionID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasDistributeCode(DistributionCode, DistributionID))
            {
                Errormessage = Resources.MessageTips.ExistDistributeTemplateCode;
            }
            return Errormessage;
        }
    }
}