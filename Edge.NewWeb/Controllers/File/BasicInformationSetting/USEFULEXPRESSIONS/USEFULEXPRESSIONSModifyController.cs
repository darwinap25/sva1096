﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.File.BasicInformationSetting.USEFULEXPRESSIONS
{
    public class USEFULEXPRESSIONSModifyController : USEFULEXPRESSIONSController
    {
        public void LoadViewModel(int USEFULEXPRESSIONSID, string lan)
        {
            Edge.SVA.BLL.USEFULEXPRESSIONS bll = new Edge.SVA.BLL.USEFULEXPRESSIONS();
            Edge.SVA.Model.USEFULEXPRESSIONS model = bll.GetModel(USEFULEXPRESSIONSID);
            viewModel.MainTable = model;
        }

        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.USEFULEXPRESSIONS bll = new SVA.BLL.USEFULEXPRESSIONS();

                //保存
                if (bll.Exists(viewModel.MainTable.USEFULEXPRESSIONSID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
    }
}