﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.DBUtility;
using System.Data.SqlClient;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Advertisements
{
    public class AdvertisementsModifyController : AdvertisementsController
    {
        public void LoadViewModel(int keyID, string lan)
        {
            Edge.SVA.BLL.PromotionMsg bll = new Edge.SVA.BLL.PromotionMsg();
            Edge.SVA.Model.PromotionMsg model = bll.GetModel(keyID);
            viewModel.MainTable = model;

            Edge.SVA.BLL.PromotionCardCondition bll1 = new SVA.BLL.PromotionCardCondition();
            List<Edge.SVA.Model.PromotionCardCondition> list = bll1.GetModelList(" PromotionMsgID = " + keyID);
            foreach (var item in list)
            {
                viewModel.CardGradeIDList.Add(item.CardGradeID);
            }

            //viewModel.BrandInfo = PublicInfoReostory.Singleton.GetCardTypeListByBrand(lan);
            viewModel.BrandInfo = PublicInfoReostory.Singleton.GetBrandInfoListByBrandUserID(Tools.DALTool.GetCurrentUser().UserID, lan);//Add by Robin 2014-08-04 for 过滤用户可操作卡，优惠券
        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PromotionMsg bll = new SVA.BLL.PromotionMsg();
                int objctid = 0;
                //Msg表保存
                if (bll.Exists(viewModel.MainTable.KeyID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    objctid = bll.Add(viewModel.MainTable);
                }
                //CardCondition表保存
                Edge.SVA.BLL.PromotionCardCondition bll1 = new SVA.BLL.PromotionCardCondition();
                Edge.SVA.Model.PromotionCardCondition model = new SVA.Model.PromotionCardCondition();
                bll1.Delete(viewModel.MainTable.KeyID, 0);
                model.PromotionMsgID = viewModel.MainTable.KeyID;
                foreach (var item in viewModel.CardGradeIDList)
                {
                    model.CardGradeID = item;
                    bll1.Add(model);
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Save()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                //主表保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionMsg bll = new SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionMsg();
                bll.Update(viewModel.MainTable, trans);

                //第一个子表的保存
                Edge.SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionCardCondition bllcard = new SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionCardCondition();
                Edge.SVA.Model.PromotionCardCondition model = new SVA.Model.PromotionCardCondition();
                bllcard.Delete(viewModel.MainTable.KeyID, 0, trans);
                model.PromotionMsgID = viewModel.MainTable.KeyID;
                foreach (var item in viewModel.CardGradeIDList)
                {
                    model.CardGradeID = item;
                    bllcard.Add(model, trans);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
            }
            return rtn;
        }

    }
}