﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.Web.Tools;
using Edge.SVA.Model;
using Edge.SVA.Model.Domain;
using System.Data;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Advertisements
{
    public class AdvertisementsController
    {
        protected AdvertisementsViewModel viewModel = new AdvertisementsViewModel();

        public AdvertisementsViewModel ViewModel
        {
            get { return viewModel; }
        }

        public List<KeyValue> AddPromotionMsgTypeList(string lan, int ParentID)
        {
            viewModel.PromotionMsgTypeList = new List<KeyValue>();
            Edge.SVA.BLL.PromotionMsgType bll = new SVA.BLL.PromotionMsgType();
            List<Edge.SVA.Model.PromotionMsgType> list = bll.GetModelList(" ParentID = " + ParentID);
            foreach (var item in list)
            {
                KeyValue kv = new KeyValue();
                kv.Key = item.PromotionMsgTypeID.ToString();
                switch (lan.ToLower())
                {
                    case "zh-cn":
                        kv.Value = item.PromotionMsgTypeName2;
                        break;
                    case "zh-hk":
                        kv.Value = item.PromotionMsgTypeName3;
                        break;
                    case "en-us":
                    default:
                        kv.Value = item.PromotionMsgTypeName1;
                        break;
                }
                viewModel.PromotionMsgTypeList.Add(kv);
            }

            return viewModel.PromotionMsgTypeList;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.PromotionMsg bll = new Edge.SVA.BLL.PromotionMsg();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "";//PromotionMsgCode
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddPromotionTitle(ds, "PromotionTitle", "KeyID");

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");
            return ds;
        }

        public List<KeyValue> GetParentIDList(string lan)
        {
            viewModel.ParentIDList = new List<KeyValue>();
            Edge.SVA.BLL.PromotionMsgType bll = new SVA.BLL.PromotionMsgType();
            List<Edge.SVA.Model.PromotionMsgType> list = bll.GetModelList(" ParentID = 0 or ParentID is null");
            foreach (var item in list)
            {
                KeyValue kv = new KeyValue();
                kv.Key = item.PromotionMsgTypeID.ToString();
                switch (lan.ToLower())
                {
                    case "zh-cn":
                        kv.Value = item.PromotionMsgTypeName2;
                        break;
                    case "zh-hk":
                        kv.Value = item.PromotionMsgTypeName3;
                        break;
                    case "en-us":
                    default:
                        kv.Value = item.PromotionMsgTypeName1;
                        break;
                }
                viewModel.ParentIDList.Add(kv);
            }

            return viewModel.ParentIDList;
        }

        public string GetParentIDListByTypeID(int KeyID)
        {
            string parentid = "";
            Edge.SVA.BLL.PromotionMsgType bll = new SVA.BLL.PromotionMsgType();
            DataSet ds = bll.GetList(@" PromotionMsgTypeID in (select PromotionMsgTypeID from PromotionMsg where KeyID = " + KeyID + ")");
            if (ds != null)
            {
                parentid = ds.Tables[0].Rows[0]["ParentID"].ToString();
            }

            return parentid;
            
        }
    }
}