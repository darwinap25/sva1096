﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using Edge.DBUtility;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Advertisements
{
    public class AdvertisementsDeleteController : AdvertisementsController
    {
        public ExecResult Delete(int keyID)
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PromotionMsg bll = new SVA.BLL.PromotionMsg();
                //Msg表删除
                bll.Delete(keyID);
                //CardCondition表删除
                Edge.SVA.BLL.PromotionCardCondition bll1 = new SVA.BLL.PromotionCardCondition();
                bll1.Delete(keyID, 0);
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult DeleteData(List<int> keyIDList)
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                foreach (int keyID in keyIDList)
                {
                    //主表保存
                    Edge.SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionMsg bll = new SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionMsg();
                    bll.Delete(keyID, trans);

                    //第一个子表的保存
                    Edge.SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionCardCondition bllcard = new SVA.BLL.Domain.BasicInformationSetting.Advertisements.PromotionCardCondition();
                    Edge.SVA.Model.PromotionCardCondition model = new SVA.Model.PromotionCardCondition();
                    bllcard.Delete(keyID, 0, trans);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
            }
            return rtn;
        }
    }
}