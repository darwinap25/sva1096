﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Reason
{
    public class ReasonController
    {
        protected ReasonViewModel viewModel = new ReasonViewModel();

        public ReasonViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int ReasonID)
        {
            Edge.SVA.BLL.Reason bll = new Edge.SVA.BLL.Reason();
            Edge.SVA.Model.Reason model = bll.GetModel(ReasonID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Reason bll = new SVA.BLL.Reason();

                //保存
                if (this.IsExists(viewModel.MainTable.ReasonID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Reason bll = new SVA.BLL.Reason();

                //保存
                if (this.IsExists(viewModel.MainTable.ReasonID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Reason bll = new Edge.SVA.BLL.Reason();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "ReasonCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddReasonDesc(ds, "ReasonDesc", "ReasonID");

            return ds;
        }

        protected bool IsExists(int ReasonID)
        {
            Edge.SVA.BLL.Reason bllmc = new SVA.BLL.Reason();
            string strwhere = " ReasonID =" + ReasonID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string ReasonCode, int ReasonID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasReasonCode(ReasonCode, ReasonID))
            {
                Errormessage = Resources.MessageTips.ExistReasonCode;
            }
            return Errormessage;
        }

    }
}