﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.StoreNature
{
    public class StoreNatureController
    {
        protected StoreNatureViewModel viewModel = new StoreNatureViewModel();

        public StoreNatureViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int StoreTypeID)
        {
            Edge.SVA.BLL.StoreType bll = new Edge.SVA.BLL.StoreType();
            Edge.SVA.Model.StoreType model = bll.GetModel(StoreTypeID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.StoreType bll = new SVA.BLL.StoreType();

                //保存
                if (this.IsExists(viewModel.MainTable.StoreTypeID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.StoreType bll = new SVA.BLL.StoreType();

                //保存
                if (this.IsExists(viewModel.MainTable.StoreTypeID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.StoreType bll = new Edge.SVA.BLL.StoreType();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "StoreTypeCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddStoreTypeName(ds, "StoreTypeName", "StoreTypeID");

            return ds;
        }

        protected bool IsExists(int StoreTypeID)
        {
            Edge.SVA.BLL.StoreType bllmc = new SVA.BLL.StoreType();
            string strwhere = " StoreTypeID =" + StoreTypeID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string StoreTypeCode, int StoreTypeID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasStoreTypeCode(StoreTypeCode, StoreTypeID))
            {
                Errormessage = Resources.MessageTips.ExistStoreGroupCode;
            }
            return Errormessage;
        }
    }
}