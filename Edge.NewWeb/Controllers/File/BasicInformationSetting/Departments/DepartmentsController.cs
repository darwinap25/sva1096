﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Departments
{
    public class DepartmentsController
    {
        protected DepartmentsViewModel viewModel = new DepartmentsViewModel();

        public DepartmentsViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string DepartCode)
        {
            Edge.SVA.BLL.Department bll = new Edge.SVA.BLL.Department();
            Edge.SVA.Model.Department model = bll.GetModel(DepartCode);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Department bll = new SVA.BLL.Department();

                //保存
                if (this.IsExists(viewModel.MainTable.DepartCode))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Department bll = new SVA.BLL.Department();

                //保存
                if (this.IsExists(viewModel.MainTable.DepartCode))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Department bll = new Edge.SVA.BLL.Department();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "DepartCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddDepartmentName(ds, "DepartmentName", "DepartCode");

            return ds;
        }

        public bool IsExists(string DepartCode)
        {
            Edge.SVA.BLL.Department bllmc = new SVA.BLL.Department();
            string strwhere = " DepartCode =" + DepartCode;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }
    }
}