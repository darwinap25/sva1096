﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.Controllers.File.BasicInformationSetting.Products
{
    public class BuyingProductController
    {
        //protected ProductViewModel viewModel = new ProductViewModel();

        //public ProductViewModel ViewModel
        //{
        //    get { return viewModel; }
        //}

        public void LoadViewModel(string ProdCode)
        {
            //Edge.SVA.BLL.PRODUCT bll = new Edge.SVA.BLL.PRODUCT();
            //Edge.SVA.Model.PRODUCT model = bll.GetModel(ProdCode);
            //viewModel.MainTable = model;

            //Edge.SVA.BLL.BARCODE bllbar = new SVA.BLL.BARCODE();
            //viewModel.dtBarCode = bllbar.GetList("ProdCode='" + ProdCode + "'").Tables[0];

            //Edge.SVA.BLL.BUY_RPRICE_M bllrp = new SVA.BLL.BUY_RPRICE_M();
            ////viewModel.dtRprice = bllrp.GetList("ProdCode='" + ProdCode + "'").Tables[0];
            //string sql = @"select top 1* from BUY_RPRICE_M where ProdCode='" + ProdCode + "' order by keyID desc";
            //viewModel.dtRprice = DBUtility.DbHelperSQL.Query(sql).Tables[0];
            //DataTool.AddBuyingRPriceTypeName(viewModel.dtRprice, "RPriceTypeName", "RPriceTypeCode");
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Product bll = new Edge.SVA.BLL.Product();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            //获取排序字段
            string orderStr = "ProdCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            DataTool.AddBrandName(ds, "BrandName", "ProductBrandID");
            DataTool.AddDepartmentName(ds, "DepartName", "DepartCode");

            return ds;
        }

        //public ExecResult Add()
        //{
        //    ExecResult rtn = ExecResult.CreateExecResult();
        //    try
        //    {
        //        Edge.SVA.BLL.Product bll = new SVA.BLL.Product();

        //        //保存
        //        if (ValidateForm() != "")
        //        {
        //            rtn.Message = ValidateForm();
        //        }
        //        if (ValidateObject(viewModel.MainTable.ProdCode) != "")
        //        {
        //            rtn.Message = ValidateObject(viewModel.MainTable.ProdCode);
        //        }
        //        if (rtn.Message == "")
        //        {
        //            bll.Add(viewModel.MainTable);
        //        }

        //    }
        //    catch (System.Exception ex)
        //    {
        //        rtn.Ex = ex;
        //    }
        //    return rtn;
        //}

        //public ExecResult Update()
        //{
        //    ExecResult rtn = ExecResult.CreateExecResult();
        //    try
        //    {
        //        Edge.SVA.BLL.Product bll = new Edge.SVA.BLL.Product();
        //        Edge.SVA.BLL.BUY_BARCODE bllbar = new SVA.BLL.BUY_BARCODE();
        //        Edge.SVA.BLL.BUY_RPRICE_M bllrp = new SVA.BLL.BUY_RPRICE_M();
        //        Edge.SVA.BLL.BUY_CPRICE_M bllcp = new SVA.BLL.BUY_CPRICE_M();

        //        //保存
        //        if (ValidateForm() != "")
        //        {
        //            rtn.Message = ValidateForm();
        //        }
        //        if (rtn.Message == "")
        //        {
        //            bll.Update(viewModel.MainTable);
        //            //BARCODE
        //            bllbar.DeleteData(viewModel.MainTable.ProdCode);
        //            if (viewModel.dtBarCode != null)
        //            {
        //                for (int i = 0; i < viewModel.dtBarCode.Rows.Count; i++)
        //                {
        //                    Edge.SVA.Model.BUY_BARCODE model = new SVA.Model.BUY_BARCODE();
        //                    DataRow dr = viewModel.dtBarCode.Rows[i];
        //                    model.ProdCode = dr["ProdCode"].ToString();
        //                    model.Barcode = dr["Barcode"].ToString();
        //                    model.InternalBarcode = Convert.ToInt32(dr["InternalBarcode"].ToString());
        //                    bllbar.Add(model);
        //                }
        //            }
        //            //RPRICE no need to delete
        //            //bllrp.Delete(viewModel.MainTable.ProdCode);
        //            if (viewModel.dtRprice != null)
        //            {
        //                for (int i = 0; i < viewModel.dtRprice.Rows.Count; i++)
        //                {
        //                    Edge.SVA.Model.BUY_RPRICE_M model = new SVA.Model.BUY_RPRICE_M();
        //                    DataRow dr = viewModel.dtRprice.Rows[i];
        //                    if (ConvertTool.ToInt(dr["KeyID"].ToString()) < 0)
        //                    {
        //                        model.ProdCode = dr["ProdCode"].ToString();
        //                        model.RPriceCode = "";
        //                        model.RPriceTypeCode = dr["RPriceTypeCode"].ToString();
        //                        model.RefPrice = ConvertTool.ToDecimal(dr["RefPrice"].ToString());
        //                        model.Price = ConvertTool.ToDecimal(dr["Price"].ToString());
        //                        model.StartDate = ConvertTool.ToDateTime(dr["StartDate"].ToString());
        //                        model.EndDate = ConvertTool.ToDateTime(dr["EndDate"].ToString());
        //                        model.MemberPrice = ConvertTool.ToDecimal(dr["MemberPrice"].ToString());
        //                        bllrp.Add(model);
        //                    }
        //                }
        //            }
        //            //CPRICE
        //            bllcp.Add(viewModel.dtCprice);
        //        }

        //    }
        //    catch (System.Exception ex)
        //    {
        //        rtn.Ex = ex;
        //    }
        //    return rtn;
        //}

        //public ExecResult Delete(string ProdCode)
        //{
        //    ExecResult rtn = ExecResult.CreateExecResult();
        //    try
        //    {
        //        Edge.SVA.BLL.Product bll = new Edge.SVA.BLL.Product();
        //        Edge.SVA.BLL.BUY_BARCODE bllbar = new SVA.BLL.BUY_BARCODE();
        //        Edge.SVA.BLL.BUY_RPRICE_M bllrp = new SVA.BLL.BUY_RPRICE_M();
        //        Edge.SVA.BLL.BUY_CPRICE_M bllcp = new SVA.BLL.BUY_CPRICE_M();

        //        if (rtn.Message == "")
        //        {
        //            bllbar.DeleteData(ProdCode);
        //            bllcp.Delete(ProdCode);
        //            bllrp.Delete(ProdCode);
        //            bll.Delete(ProdCode);
        //        }

        //    }
        //    catch (System.Exception ex)
        //    {
        //        rtn.Ex = ex;
        //    }
        //    return rtn;
        //}

        public string ValidateForm()
        {
            return "";
        }

        public string ValidateObject(string strWhere)
        {
            Edge.SVA.BLL.Product bll = new SVA.BLL.Product();
            List<Edge.SVA.Model.Product> list = bll.GetModelList("ProdCode = '" + strWhere + "'");
            if (list.Count > 0)
            {
                return Resources.MessageTips.ExistRecord;
            }
            return "";
        }

        //#region 下拉框绑定
        //public void BindBrand(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_BRAND bll = new SVA.BLL.BUY_BRAND();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "BrandCode", "BrandName1", "BrandName2", "BrandName3", "BrandCode");
        //}
        //public void BindProSize(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.ProductSIZE bll = new SVA.BLL.ProductSIZE();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "ProductSizeCode", "ProductSizeName1", "ProductSizeName2", "ProductSizeName3", "ProductSizeCode");
        //}
        //public void BindDepart(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_DEPARTMENT bll = new SVA.BLL.BUY_DEPARTMENT();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "DepartCode", "DepartName1", "DepartName2", "DepartName3", "DepartCode");
        //}
        //public void BindStore(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_STORE bll = new SVA.BLL.BUY_STORE();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "StoreCode", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        //}
        //public void BindProdClass(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.ProductCLASS bll = new SVA.BLL.ProductCLASS();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "ProdClassCode", "ProdClassDesc1", "ProdClassDesc2", "ProdClassDesc3", "ProdClassCode");
        //}
        //public void BindReplenFormula(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_REPLENFORMULA bll = new SVA.BLL.BUY_REPLENFORMULA();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "ReplenFormulaCode", "Description", "Description", "Description", "ReplenFormulaCode");
        //}
        //public void BindFulfillmentHouse(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_FULFILLMENTHOUSE bll = new SVA.BLL.BUY_FULFILLMENTHOUSE();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "FulfillmentHouseCode", "FulfillmentHouseName1", "FulfillmentHouseName2", "FulfillmentHouseName3", "FulfillmentHouseCode");
        //}
        //public void BindColor(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_COLOR bll = new SVA.BLL.BUY_COLOR();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "ColorCode", "ColorName1", "ColorName2", "ColorName3", "ColorCode");
        //}
        //public void BindRPriceType(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_RPRICETYPE bll = new SVA.BLL.BUY_RPRICETYPE();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "RPriceTypeCode", "RPriceTypeName1", "RPriceTypeName2", "RPriceTypeName3", "RPriceTypeCode");
        //}
        //public void BindStoreGroup(FineUI.DropDownList ddl)
        //{
        //    Edge.SVA.BLL.BUY_STOREGROUP bll = new SVA.BLL.BUY_STOREGROUP();
        //    DataSet ds = bll.GetList("");
        //    ControlTool.BindDataSet(ddl, ds, "StoreGroupCode", "StoreGroupName1", "StoreGroupName2", "StoreGroupName3", "StoreGroupCode");
        //}
        //#endregion

    }
}