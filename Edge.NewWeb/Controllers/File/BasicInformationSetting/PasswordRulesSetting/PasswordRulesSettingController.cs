﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.PasswordRulesSetting
{
    public class PasswordRulesSettingController
    {
        protected PasswordRulesSettingViewModel viewModel = new PasswordRulesSettingViewModel();

        public PasswordRulesSettingViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int PasswordRuleID)
        {
            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            Edge.SVA.Model.PasswordRule model = bll.GetModel(PasswordRuleID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PasswordRule bll = new SVA.BLL.PasswordRule();

                //保存
                if (this.IsExists(viewModel.MainTable.PasswordRuleID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PasswordRule bll = new SVA.BLL.PasswordRule();

                //保存
                if (this.IsExists(viewModel.MainTable.PasswordRuleID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "PasswordRuleCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddPasswordRuleName(ds, "Name", "PasswordRuleID");
            Tools.DataTool.AddPasswordFormat(ds, "PassworFormat", "PWDStructure");

            return ds;
        }

        protected bool IsExists(int PasswordRuleID)
        {
            Edge.SVA.BLL.PasswordRule bllmc = new SVA.BLL.PasswordRule();
            string strwhere = " PasswordRuleID =" + PasswordRuleID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string filed)
        {
            string Errormessage = "";
            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            if (bll.GetCount(string.Format("PWDSecurityLevel = {0}", Convert.ToInt32(filed))) > 0)
            {
                Errormessage = Resources.MessageTips.ExistPasswordSecurity;
            }
            return Errormessage;
        }
    }
}