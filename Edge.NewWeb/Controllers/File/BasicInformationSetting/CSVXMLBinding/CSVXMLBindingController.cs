﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.Utils.Tools;
using System.Text;

namespace Edge.Web.Controllers.File.BasicInformationSetting.CSVXMLBinding
{
    public class CSVXMLBindingController
    {
        protected CSVXMLBindingViewModel viewModel = new CSVXMLBindingViewModel();

        public CSVXMLBindingViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string code)
        {
            Edge.SVA.BLL.CSVXMLBinding bll = new Edge.SVA.BLL.CSVXMLBinding();
            Edge.SVA.Model.CSVXMLBinding model = bll.GetModel(code);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.CSVXMLBinding bll = new SVA.BLL.CSVXMLBinding();

                //保存
                if (this.IsExists(viewModel.MainTable.BindingCode))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.CSVXMLBinding bll = new SVA.BLL.CSVXMLBinding();

                //保存
                if (this.IsExists(viewModel.MainTable.BindingCode))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.CSVXMLBinding bll = new Edge.SVA.BLL.CSVXMLBinding();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "BindingCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            //Tools.DataTool.AddReasonDesc(ds, "ReasonDesc", "ReasonID");

            return ds;
        }

        protected bool IsExists(string code)
        {
            Edge.SVA.BLL.CSVXMLBinding bllmc = new SVA.BLL.CSVXMLBinding();
            string strwhere = " BindingCode =" + code;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }

        public string ValidataObject(string Code)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasCSVXMLBindingCode(Code))
            {
                Errormessage = Resources.MessageTips.ExistRecord;
            }
            return Errormessage;
        }

    }
}