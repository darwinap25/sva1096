﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain;
using System.Data;
using Edge.SVA.Model.Domain.WebInterfaces;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule
{
    public class EarnCouponRuleController
    {
        protected EarnCouponRuleViewModel viewModel = new EarnCouponRuleViewModel();

        public EarnCouponRuleViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }

        public void LoadViewModel(int KeyID)
        {
            Edge.SVA.BLL.EarnCouponRule bll = new SVA.BLL.EarnCouponRule();
            viewModel.MainTable = bll.GetModel(KeyID);
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            Edge.SVA.BLL.EarnCouponRule bll = new Edge.SVA.BLL.EarnCouponRule();
            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "KeyID";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddCardTypeName(ds, "CardTypeIDLimitName", "CardTypeIDLimit");
            Tools.DataTool.AddCardGradeName(ds, "CardGradeIDLimitName", "CardGradeIDLimit");
            Tools.DataTool.AddMemberRangeName(ds, "MemberBirthdayLimitName", "MemberBirthdayLimit");
            Tools.DataTool.AddExchangeTypeName(ds, "ExchangeTypeName", "ExchangeType");
            Tools.DataTool.AddConsumeRuleOperName(ds, "ExchangeConsumeRuleOperName", "ExchangeConsumeRuleOper");
            Tools.DataTool.AddCouponTypeName(ds, "ExchangeCouponTypeIDName", "ExchangeCouponTypeID");
            Tools.DataTool.AddStatus(ds,"StatusName","Status");

            return ds;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.EarnCouponRule bll = new SVA.BLL.EarnCouponRule();

                bll.Add(viewModel.MainTable);

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.EarnCouponRule bll = new SVA.BLL.EarnCouponRule();

                bll.Update(viewModel.MainTable);

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
    }
}