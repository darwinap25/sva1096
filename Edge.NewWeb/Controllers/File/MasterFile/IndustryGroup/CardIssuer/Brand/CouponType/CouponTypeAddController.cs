﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public class CouponTypeAddController:CouponTypeController
    {
        public bool Validate(out List<KeyValue> errorList)
        {
            errorList = new List<KeyValue>();
            KeyValue desc;
            //todo: can add validate rule here
            //if (!viewModel.ValidateCanHoldCouponCount())
            //{
            //    desc = new Description();
            //    desc.Code = "HoldCount";
            //    desc.Value = " num set wrong";
            //    errorList.Add(desc);
            //    goto ReturnUnSuccess;
            //}
            return true;

        ReturnUnSuccess: return false;

        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                int couponTypeID = viewModel.MainTable.CouponTypeID;
                Edge.SVA.BLL.CouponReplenishDaily bll = new Edge.SVA.BLL.CouponReplenishDaily();
                foreach (var item in viewModel.CouponReplenishDailyViewModelListAdd)
                {
                    item.MainTable.CouponTypeID = couponTypeID;
                    item.MainTable.CreatedOn = DateTime.Now;
                    item.MainTable.UpdatedOn = DateTime.Now;
                    bll.Add(item.MainTable);
                }

                Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
                if (IsExistsMemberClause(couponTypeID))
                {
                    bllmc.Update(viewModel.MemberClause);
                }
                else
                {
                    viewModel.MemberClause.ClauseTypeCode = "CouponType";
                    viewModel.MemberClause.ClauseSubCode = couponTypeID.ToString();
                    bllmc.Add(viewModel.MemberClause);
                }

                SVASessionInfo.CouponTypeAddController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }
        private bool IsExistsMemberClause(int CouponTypeID)
        {
            Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
            string strwhere = " MemberClause.ClauseTypeCode='CouponType' and MemberClause.ClauseSubCode=" + CouponTypeID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;
        }
    }
}