﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public class CouponTypeController
    {
        protected CouponTypeViewModel viewModel= new CouponTypeViewModel();

        public CouponTypeViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }
        public void SetCouponReplenishDailyViewModelList(List<CouponReplenishDailyViewModel> list)
        {
            viewModel.CouponReplenishDailyViewModelListAdd = list;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();
            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "CouponTypeCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddColumn(ds, "CardIssuerName", Tools.DALTool.GetCardIssuerName());
            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
            return ds;
        } 
    }
}