﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.BLL.Domain.DataResources;
using System.Data.SqlClient;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class CampaignDeleteController
    {
        public ExecResult Delete(List<int> CampaignIDList)
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                foreach (int CampaignID in CampaignIDList)
                {
                    //主表删除
                    Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Campaign bll = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Campaign();
                    bll.Delete(CampaignID, trans);

                    //第一个子表的删除
                    Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCardGrade bllcard = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCardGrade();
                    bllcard.Delete(CampaignID, 0, trans);

                    //第二个子表的删除
                    Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCouponType bllcoupon = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCouponType();
                    bllcoupon.Delete(CampaignID, 0, trans);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
            }
            return rtn;
        }
    }
}