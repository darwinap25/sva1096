﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.Surpport;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain;
using Edge.SVA.Model;
using Edge.SVA.BLL.Domain.DataResources;
using System.Data.SqlClient;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public class CampaignModifyController:CampaignController
    {
        public void LoadViewModel(int campaignID, string lan)
        {
            Edge.SVA.BLL.Campaign bll = new Edge.SVA.BLL.Campaign();
            Edge.SVA.Model.Campaign model = bll.GetModel(campaignID);
            viewModel.MainTable = model;
            Edge.SVA.BLL.CampaignCardGrade bll1 = new Edge.SVA.BLL.CampaignCardGrade();
            List<Edge.SVA.Model.CampaignCardGrade> list1 = bll1.GetModelList(" CampaignID="+campaignID);
            CardGradeRepostory res = CardGradeRepostory.Singleton;
            foreach (var item in list1)
            {
                KeyValue kv = new KeyValue();
                kv.Key = item.CardGradeID.ToString();
                CardGrade cg = res.GetModelByID(item.CardGradeID);
                switch (lan)
                {
                    case LanguageFlag.ZHCN:
                        kv.Value = cg.CardGradeCode + "-" + cg.CardGradeName2;
                        break;
                    case LanguageFlag.ZHHK:
                        kv.Value = cg.CardGradeCode + "-" + cg.CardGradeName3;
                        break;
                    case LanguageFlag.ENUS:
                    default:
                        kv.Value = cg.CardGradeCode + "-" + cg.CardGradeName1;
                        break;
                }
                this.viewModel.CardGradeList.Add(kv);
            }

            Edge.SVA.BLL.CampaignCouponType bll2 = new Edge.SVA.BLL.CampaignCouponType();
            List<Edge.SVA.Model.CampaignCouponType> list2 = bll2.GetModelList(" CampaignID=" + campaignID);
            CouponTypeRepostory ctr = CouponTypeRepostory.Singleton;
            foreach (var item in list2)
            {
                KeyValue kv = new KeyValue();
                kv.Key = item.CouponTypeID.ToString();
                Edge.SVA.Model.CouponType cg = ctr.GetModelByID(item.CouponTypeID);
                switch (lan)
                {
                    case LanguageFlag.ZHCN:
                        kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName2;
                        break;
                    case LanguageFlag.ZHHK:
                        kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName3;
                        break;
                    case LanguageFlag.ENUS:
                    default:
                        kv.Value = cg.CouponTypeCode + "-" + cg.CouponTypeName1;
                        break;
                }
                this.viewModel.CouponTypeList.Add(kv);
            }
        }
        public bool Validate(out List<KeyValue> errorList)
        {
            bool rtn = true;
            errorList = new List<KeyValue>();

            return rtn;
        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                int id = viewModel.MainTable.CampaignID;
                Edge.SVA.BLL.CampaignCardGrade bll1 = new Edge.SVA.BLL.CampaignCardGrade();
                bll1.Delete(id, 0);
                foreach (var item in this.viewModel.CardGradeList)
                {
                    Edge.SVA.Model.CampaignCardGrade model = new Edge.SVA.Model.CampaignCardGrade();
                    model.CampaignID = id;
                    model.CardGradeID = ConvertTool.ConverType<int>(item.Key);
                    bll1.Add(model);
                }

                Edge.SVA.BLL.CampaignCouponType bll2 = new Edge.SVA.BLL.CampaignCouponType();
                bll2.Delete(id, 0);
                foreach (var item in this.viewModel.CouponTypeList)
                {
                    Edge.SVA.Model.CampaignCouponType model = new Edge.SVA.Model.CampaignCouponType();
                    model.CampaignID = id;
                    model.CouponTypeID = ConvertTool.ConverType<int>(item.Key);
                    bll2.Add(model);
                }
                SVASessionInfo.CardGradeAddController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Save()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            DBUtility.Transaction tr = new DBUtility.Transaction();
            tr.OpenConnSVAWithTrans();
            SqlTransaction trans = tr.Trans;

            try
            {
                //主表保存
                Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Campaign bll = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Campaign();
                bll.Update(viewModel.MainTable, trans);
                int keyid = viewModel.MainTable.CampaignID;

                //第一个子表的保存
                Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCardGrade bllcard = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCardGrade();
                Edge.SVA.Model.CampaignCardGrade model = new SVA.Model.CampaignCardGrade();
                bllcard.Delete(keyid, 0, trans);
                model.CampaignID = keyid;
                foreach (var item in this.viewModel.CardGradeList)
                {
                    model.CardGradeID = ConvertTool.ConverType<int>(item.Key);
                    bllcard.Add(model, trans);
                }

                //第二个子表的保存
                Edge.SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCouponType bllcoupon = new SVA.BLL.Domain.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.CampaignCouponType();
                bllcoupon.Delete(keyid, 0, trans);
                Edge.SVA.Model.CampaignCouponType modelcoupon = new Edge.SVA.Model.CampaignCouponType();
                modelcoupon.CampaignID = keyid;
                foreach (var item in this.viewModel.CouponTypeList)
                {
                    modelcoupon.CouponTypeID = ConvertTool.ConverType<int>(item.Key);
                    bllcoupon.Add(modelcoupon, trans);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
                trans.Rollback();
            }
            finally
            {
                tr.CloseConn();
                SVASessionInfo.CardGradeAddController = null;
            }
            return rtn;
        }

    }
}