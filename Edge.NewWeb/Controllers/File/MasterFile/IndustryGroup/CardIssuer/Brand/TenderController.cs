﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using System.Data;
using System.Text;
using Edge.Web.Operation.CardManagement.BatchImportCards;
using Edge.SVA.Model.Domain.Surpport;
using System.Data.SqlClient;
using Edge.Web.Tools;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public class TenderController
    {
        protected TenderViewMode viewModel = new TenderViewMode();

        public TenderViewMode ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(int TenderID)
        {
            Edge.SVA.BLL.TENDER bll = new Edge.SVA.BLL.TENDER();
            Edge.SVA.Model.TENDER model = bll.GetModel(TenderID);
            viewModel.MainTable = model;
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.TENDER bll = new SVA.BLL.TENDER();
                
                //保存
                if (bll.Exists(viewModel.MainTable.TenderCode, viewModel.MainTable.TenderID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.TENDER bll = new SVA.BLL.TENDER();

                //保存
                if (bll.Exists(viewModel.MainTable.TenderCode, viewModel.MainTable.TenderID))
                {
                    bll.Update(viewModel.MainTable);
                }
                else
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.TENDER bll = new Edge.SVA.BLL.TENDER();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            //获取排序字段
            string orderStr = "TenderCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Edge.Web.Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            //Edge.Web.Tools.DataTool.AddCardIssuerName(ds, "CardIssuerName", "CardIssuerID");
            return ds;
        }

        public string ValidataObject(string TenderCode, int TenderID)
        {
            string Errormessage = "";
            if (Tools.DALTool.isHasBrandCode(TenderCode, TenderID))
            {
                Errormessage = Resources.MessageTips.ExistBrandCode;
            }
            return Errormessage;
        }
        #region Import Data

        public ImportModelList list = new ImportModelList();

        Edge.SVA.BLL.TENDER tender = new SVA.BLL.TENDER();
        private DataTable dtTender = new DataTable();
        private List<Edge.SVA.Model.TENDER> addmodellist = new List<SVA.Model.TENDER>();
        public bool CheckData(DataTable dt)
        {
            //移除占用了格式的空行
            //List<DataRow> drlist = new List<System.Data.DataRow>();
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    if (dt.Rows[i]["Action"] == null || string.IsNullOrEmpty(dt.Rows[i]["Action"].ToString()))
            //    {
            //        drlist.Add(dt.Rows[i]);
            //    }
            //}
            //foreach (var item in drlist)
            //{
            //    dt.Rows.Remove(item);
            //}


            //Add by Alex 2014-08-18 ++
            DataTool.ClearEndRow(dt);

            if (dt == null || dt.Rows.Count <= 0)
            {
                this.list.Error.Add(Resources.MessageTips.NoData);
                return false;
            }
            #region check columns
            List<string> columnList = new List<string>();
            columnList.Add("TenderCode");
            columnList.Add("TenderName1");
            columnList.Add("TenderName2");
            columnList.Add("TenderName3");
            columnList.Add("BrandCode");
            StringBuilder sb = new StringBuilder(Resources.MessageTips.Lackofcolumn);
            bool existColumn = true;
            foreach (var item in columnList)
            {
                if (!dt.Columns.Contains(item))
                {
                    sb.Append(item);
                    sb.Append(",");
                    existColumn = false;
                }
            }
            if (!existColumn)
            {
                this.list.Error.Add(sb.ToString().TrimEnd(','));
                return false;
            }
            #endregion
            //Add by Alex 2014-08-18 --

            Edge.SVA.BLL.Brand brandbll = new SVA.BLL.Brand();

            Edge.SVA.BLL.TENDER tenderbll = new SVA.BLL.TENDER();
            if (dt != null && dt.Rows.Count > 0)
            {
                List<KeyValue> storelist = new List<KeyValue>();
                this.list.Error.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    if (string.IsNullOrEmpty(dr["TenderCode"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90505").Replace("{0}", (i + 1).ToString())));
                    }
                    else
                    {
                        if (tenderbll.GetRecordCount(" TenderCode='" + dr["TenderCode"].ToString() +"'") > 0)
                        {
                            this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90508").Replace("{0}", (i + 1).ToString())));
                        }
                    }


                    if (string.IsNullOrEmpty(dr["BrandCode"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90507").Replace("{0}", (i + 1).ToString())));
                    }

                    if (brandbll.GetCountUnlimited(" BrandCode='" + dr["BrandCode"].ToString() + "'") <= 0)
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90509").Replace("{0}", (i + 1).ToString())));
                    }
                   

                    if (string.IsNullOrEmpty(dr["TenderName1"].ToString()))
                    {
                        this.list.Error.Add(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90506").Replace("{0}", (i + 1).ToString())));
                    }
                    
                }
                if (this.list.Error.Count == 0) return true;
            }

            return false;
        }

        public void AnalysisData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                dtTender = dt.Clone();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    dtTender.Rows.Add(dr.ItemArray); 
                }
                addmodellist = DataTableToList(dtTender, "a");
            }
        }

        public List<Edge.SVA.Model.TENDER> DataTableToList(DataTable dt, string type)
        {
            List<Edge.SVA.Model.TENDER> modelList = new List<Edge.SVA.Model.TENDER>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Edge.SVA.Model.TENDER model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Edge.SVA.Model.TENDER();
                    if (dt.Rows[n]["TenderCode"] != null && dt.Rows[n]["TenderCode"].ToString() != "")
                    {
                        model.TenderCode = dt.Rows[n]["TenderCode"].ToString();
                    }
                    if (dt.Rows[n]["TenderName1"] != null && dt.Rows[n]["TenderName1"].ToString() != "")
                    {
                        model.TenderName1 = dt.Rows[n]["TenderName1"].ToString();
                    }
                    if (dt.Rows[n]["TenderName2"] != null && dt.Rows[n]["TenderName2"].ToString() != "")
                    {
                        model.TenderName2 = dt.Rows[n]["TenderName2"].ToString();
                    }
                    if (dt.Rows[n]["TenderName3"] != null && dt.Rows[n]["TenderName3"].ToString() != "")
                    {
                        model.TenderName3 = dt.Rows[n]["TenderName3"].ToString();
                    }

                    if (dt.Rows[n]["BrandCode"] != null && dt.Rows[n]["BrandCode"].ToString() != "")
                    {
                        model.BrandID = Edge.Web.Tools.DALTool.GetBrandIDByBrandCode(dt.Rows[n]["BrandCode"].ToString(), null);
                    }
                   
                    if (type.ToLower() == "a")
                    {
                        model.CreatedOn = DateTime.Now;
                        model.CreatedBy = SVASessionInfo.CurrentUser.UserID;
                    }
                    if (type.ToLower() == "u")
                    {
                        model.UpdatedOn = DateTime.Now;
                        model.UpdatedBy = SVASessionInfo.CurrentUser.UserID;
                    }
               
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        public ExecResult Operation()
        {
            ExecResult rtn = ExecResult.CreateExecResult();

            //DBUtility.Transaction tr = new DBUtility.Transaction();
            //tr.OpenConnSVAWithTrans();
            //SqlTransaction trans = tr.Trans;

            Edge.SVA.BLL.TENDER bll = new SVA.BLL.TENDER();

            this.list.Addrecords = 0;
            this.list.Updaterecords = 0;
            this.list.Deletedrecords = 0;
            try
            {
                //关闭触发器
               // bll.OpenCloseTrigger("disable TRIGGER UpdateStoreCondition_Store ON Store", trans);
                foreach (var item in addmodellist)
                {
                    bll.Add(item);
                    this.list.Addrecords++;
                }
              //  bll.OpenCloseTrigger("enable TRIGGER UpdateStoreCondition_Store ON Store", trans);

               // trans.Commit();
            }
            catch (Exception ex)
            {
                rtn.Ex = ex;
               // trans.Rollback();
            }
            finally
            {
              //  tr.CloseConn();
            }
            return rtn;
        }

        public StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            html.Append("<table class='msgtable' width='100%'  align='center'>");

            html.AppendFormat("<tr><td align='right'>{0}</td><td style='color:{1};font-weight:bold;font-size:x-large;'>{2}</td></tr>", "Import Result:", this.list.Success ? "green" : "red", this.list.Success ? "Success." : " Fail.");
            if (this.list.Error.Count > 0)
            {
                html.AppendFormat("<tr><td align='right' valign='top'>{0}</td>", "Reason:");
                html.AppendFormat("<td><table valign='top'>");
                for (int i = 0; i < this.list.Error.Count; i++)
                {
                    string error = this.list.Error[i].Replace("\r\n", "");
                    html.AppendFormat("<tr><td align='right'></td><td>{0}</td></tr>", error);
                }
                html.AppendFormat("</table></td></tr>");
            }
            else
            {
                html.AppendFormat("<tr><td align='right'></td><td>Add {0} records {1}.</td></tr>", this.list.Addrecords, "successfully");
                html.AppendFormat("<tr><td align='right'></td><td>Update {0} records {1}.</td></tr>", this.list.Updaterecords, "successfully");
                html.AppendFormat("<tr><td align='right'></td><td>Delete {0} records {1}.</td></tr>", this.list.Deletedrecords, "successfully");
            }
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "Start Datetime:", begin.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right'>{0}</td><td>{1}</td></tr>", "End Datetime:", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            html.AppendFormat("<tr><td align='right' nowrap='nowrap'>{0}</td><td>{1}</td></tr>", "Import Function:", "Import Tendes");
            html.Append("</table>");

            SVASessionInfo.MessageHTML = html.ToString();

            this.list.Addrecords = 0;
            this.list.Updaterecords = 0;
            this.list.Deletedrecords = 0;
            this.list.Error.Clear();

            return html;
        }
        #endregion

        #region 导出数据的处理
        public DataTable GetExportData()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * ");
            sb.Append("from TENDER  ");
            //sb.Append("left join Country b on a.StoreCountry=b.CountryCode ");
            //sb.Append("left join Province c on a.StoreProvince=c.ProvinceCode ");
            //sb.Append("left join City d on a.StoreCity=d.ProvinceCode ");
            //sb.Append("left join District e on a.StoreDistrict=e.DistrictCode ");
            //sb.Append("left join Brand f on a.BrandID=f.BrandID ");
            //sb.Append("left join StoreGroup g on a.StoreGroupID=g.StoreGroupID ");
            //sb.Append("left join StoreType h on a.StoreTypeID=h.StoreTypeID ");
            DataSet ds = DBUtility.DbHelperSQL.Query(sb.ToString());
            Edge.Web.Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            if (ds.Tables.Count > 0) return ds.Tables[0];
            else return null;
        }


        public string UpLoadFileToServer(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                string UploadFilePath = string.Empty;

                UploadFilePath = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/Tender/");

                if (!System.IO.Directory.Exists(UploadFilePath))
                {
                    System.IO.Directory.CreateDirectory(UploadFilePath);
                }

                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/UploadFiles/Tender/" + "ExportTender_" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".xls");

                System.IO.FileStream fs = null;
                try
                {
                    StringBuilder text = new StringBuilder(1000);
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                    #region Write To File
                    text.Append("TenderCode\tTenderName1\tTenderName2\tTenderName3\tBrandCode");
                    text.Append("\r\n");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        text.AppendFormat("{0}\t", dr["TenderCode"].ToString());
                        text.AppendFormat("{0}\t", dr["TenderName1"].ToString());
                        text.AppendFormat("{0}\t", dr["TenderName2"].ToString());
                        text.AppendFormat("{0}\t", dr["TenderName3"].ToString());
                        text.AppendFormat("{0}\t", dr["BrandCode"].ToString());
                        text.Append("\r\n");

                        if (text.Length >= 10000)
                        {
                            byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                            fs.Write(buffer, 0, buffer.Length);
                            text.Remove(0, text.Length);
                        }
                    }

                    if (text.Length > 0)
                    {
                        byte[] buffer = System.Text.Encoding.Default.GetBytes(text.ToString());
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return fileName;
            }
            return "";
        }
        #endregion
    }


    public class ImportModelList
    {
        private List<string> error = new List<string>();
        public List<string> Error { get { return error; } }
        public int Addrecords { get; set; }
        public int Updaterecords { get; set; }
        public int Deletedrecords { get; set; }
        public bool Success
        {
            get
            {
                if (this.Error.Count > 0)
                {
                    return false;
                }
                if (Addrecords == 0 && Updaterecords == 0 && Deletedrecords == 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}