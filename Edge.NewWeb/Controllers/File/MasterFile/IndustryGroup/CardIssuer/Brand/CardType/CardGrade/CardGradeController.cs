﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain;
using Edge.SVA.BLL.Domain.DataResources;
using System.Text;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model;
using Edge.SVA.BLL.Domain.ViewModelBLL;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public class CardGradeController
    {
        protected CardGradeViewModel viewModel = new CardGradeViewModel();

        public CardGradeViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }
        #region CanAddCanHoldCouponGradeList
        public bool ValidateIfCanAddCanHoldCouponGradeListItem(CardGradeHoldCouponRuleViewModel model)
        {
            int id = model.MainTable.CouponTypeID;
            CardGradeHoldCouponRuleViewModel val = viewModel.CanHoldCouponGradeList.Find(m => m.MainTable.CouponTypeID == model.MainTable.CouponTypeID);
            if (val != null)
            {
                return false;
            }
            return true;
        }

        public void AddCanHoldCouponGradeListItem(CardGradeHoldCouponRuleViewModel model)
        {
            CardGradeViewModelBLL bll = new CardGradeViewModelBLL();
            bll.SetCardGradeHoldCouponRuleViewModelDescription(SVASessionInfo.SiteLanguage, model);
            viewModel.CanHoldCouponGradeList.Add(model);
            viewModel.CardGradeHoldCouponRuleListAdd.Add(model);
        }
        public void RemoveCanHoldCouponGradeListItem(CardGradeHoldCouponRuleViewModel model)
        {
            if (viewModel.CardGradeHoldCouponRuleListAdd.Contains(model))
            {
                viewModel.CardGradeHoldCouponRuleListAdd.Remove(model);
                viewModel.CanHoldCouponGradeList.Remove(model);
            }
            else
            {
                if (viewModel.CanHoldCouponGradeList.Contains(model))
                {
                    viewModel.CardGradeHoldCouponRuleListDelete.Add(model);
                    viewModel.CanHoldCouponGradeList.Remove(model);
                }
            }
        }
        #endregion

        public bool ValidateIfCanAddGetCouponGradeListItem(CardGradeHoldCouponRuleViewModel model)
        {
            int id = model.MainTable.CouponTypeID;
            CardGradeHoldCouponRuleViewModel val=viewModel.GetCouponGradeList.Find(m => m.MainTable.CouponTypeID == model.MainTable.CouponTypeID);
            if (val!=null)
            {
                return false;
            }
            return true;
        }
        public void AddGetCouponGradeListItem(CardGradeHoldCouponRuleViewModel model)
        {
            CardGradeViewModelBLL bll = new CardGradeViewModelBLL();
            bll.SetCardGradeHoldCouponRuleViewModelDescription(SVASessionInfo.SiteLanguage, model);
            viewModel.GetCouponGradeList.Add(model);
            viewModel.CardGradeHoldCouponRuleListAdd.Add(model);
        }
        public void RemoveGetCouponGradeListItem(CardGradeHoldCouponRuleViewModel model)
        {
            if (viewModel.CardGradeHoldCouponRuleListAdd.Contains(model))
            {
                viewModel.CardGradeHoldCouponRuleListAdd.Remove(model);
                viewModel.GetCouponGradeList.Remove(model);
            }
            else
            {
                if (viewModel.GetCouponGradeList.Contains(model))
                {
                    viewModel.CardGradeHoldCouponRuleListDelete.Add(model);
                    viewModel.GetCouponGradeList.Remove(model);
                }
            }
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();

            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "CardGradeCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);


            Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");

            ds.Tables[0].Columns.Add(new DataColumn("BrandName", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));

            Edge.SVA.BLL.CardType bllCardType = new Edge.SVA.BLL.CardType();
            Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
            Dictionary<int, string> brandCache = new Dictionary<int, string>();
            Dictionary<int, string> cardIssuerCache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Edge.SVA.Model.CardType mCardType = bllCardType.GetModel(int.Parse(row["CardTypeID"].ToString()));
                row["BrandName"] = Edge.Web.Tools.DALTool.GetBrandName(mCardType.BrandID, brandCache);

                Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(mCardType.BrandID);
                row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetCardIssuerName(mBrand.CardIssuerID.GetValueOrDefault(), cardIssuerCache);
            }

            Tools.DataTool.AddBrandCodeByCardType(ds, "BrandCode", "CardTypeID");
            Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
            Tools.DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");

            return ds;
        }
    }
}