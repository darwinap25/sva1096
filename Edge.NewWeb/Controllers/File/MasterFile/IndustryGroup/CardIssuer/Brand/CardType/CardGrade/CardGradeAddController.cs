﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.File;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.File.BasicViewModel;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public class CardGradeAddController:CardGradeController
    {
        public bool Validate(out List<KeyValue> errorList)
        {
            errorList = new List<KeyValue>();
            KeyValue desc;
            if (!viewModel.ValidateCanHoldCouponCount())
            {
                desc = new KeyValue();
                desc.Key = "HoldCount";
                desc.Value = " num set wrong";
                errorList.Add(desc);
                goto ReturnUnSuccess;
            }

            #region use rule Check Amount and point
            if (viewModel.MainTable.IsAllowStoreValue.HasValue&&viewModel.MainTable.IsAllowStoreValue.Value==1)
            {
                decimal maxAmount=viewModel.MainTable.CardGradeMaxAmount.HasValue?viewModel.MainTable.CardGradeMaxAmount.Value:0;
                decimal minAdd=viewModel.MainTable.MinAmountPreAdd.HasValue?viewModel.MainTable.MinAmountPreAdd.Value:0;
                decimal maxAdd=viewModel.MainTable.MaxAmountPreAdd.HasValue?viewModel.MainTable.MaxAmountPreAdd.Value:0;
                if (minAdd>maxAmount)
                {
                    desc=new KeyValue();
                    desc.Key = "MinAmountPreAdd";
                    desc.Value="";
                    errorList.Add(desc);
                    desc = new KeyValue();
                    desc.Key = "CardGradeMaxAmount";
                    desc.Value = "数据不合法";
                    errorList.Add(desc);
                }
                if (minAdd>maxAdd)
                {
                    desc = new KeyValue();
                    desc.Key = "MinAmountPreAdd";
                    desc.Value = "数据不合法";
                    errorList.Add(desc);
                    desc = new KeyValue();
                    desc.Key = "MaxAmountPreAdd";
                    desc.Value = "";
                    errorList.Add(desc);
                }
                if (maxAdd>maxAmount)
                {
                    desc = new KeyValue();
                    desc.Key = "MaxAmountPreAdd";
                    desc.Value = "数据不合法";
                    errorList.Add(desc);
                    desc = new KeyValue();
                    desc.Key = "CardGradeMaxAmount";
                    desc.Value = "";
                    errorList.Add(desc);
                }
            }
            if (viewModel.MainTable.IsAllowConsumptionPoint.HasValue&&viewModel.MainTable.IsAllowConsumptionPoint.Value==1)
            {
                decimal maxAmount = viewModel.MainTable.CardGradeMaxPoint.HasValue ? viewModel.MainTable.CardGradeMaxPoint.Value : 0;
                decimal minAdd = viewModel.MainTable.MinPointPreAdd.HasValue ? viewModel.MainTable.MinPointPreAdd.Value : 0;
                decimal maxAdd = viewModel.MainTable.MaxPointPreAdd.HasValue ? viewModel.MainTable.MaxPointPreAdd.Value : 0;
                if (minAdd > maxAmount)
                {
                    desc = new KeyValue();
                    desc.Key = "MinPointPreAdd";
                    desc.Value = "";
                    errorList.Add(desc);
                    desc = new KeyValue();
                    desc.Key = "CardGradeMaxPoint";
                    desc.Value = "";
                    errorList.Add(desc);
                }
                if (minAdd > maxAdd)
                {
                    desc = new KeyValue();
                    desc.Key = "MinPointPreAdd";
                    desc.Value = "";
                    errorList.Add(desc);
                    desc = new KeyValue();
                    desc.Key = "MaxPointPreAdd";
                    desc.Value = "";
                    errorList.Add(desc);
                }
                if (maxAdd > maxAmount)
                {
                    desc = new KeyValue();
                    desc.Key = "MaxPointPreAdd";
                    desc.Value = "";
                    errorList.Add(desc);
                    desc = new KeyValue();
                    desc.Key = "CardGradeMaxPoint";
                    desc.Value = "";
                    errorList.Add(desc);
                }
            }
            #endregion

            return true;

        ReturnUnSuccess: return false;
        }
        public ExecResult Submit()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                int cardgradeID = viewModel.MainTable.CardGradeID;
                Edge.SVA.BLL.CardGradeHoldCouponRule bll = new Edge.SVA.BLL.CardGradeHoldCouponRule();
                foreach (var item in viewModel.CardGradeHoldCouponRuleListAdd)
                {
                    item.MainTable.CardGradeID =cardgradeID;
                    item.MainTable.CreatedOn = DateTime.Now;
                    item.MainTable.UpdatedOn = DateTime.Now;
                    bll.Add(item.MainTable);
                }

                Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
                if (IsExistsMemberClause(cardgradeID))
                {
                    bllmc.Update(viewModel.MemberClause);
                }
                else
                {
                    viewModel.MemberClause.ClauseTypeCode = "CardGrade";
                    viewModel.MemberClause.ClauseSubCode = cardgradeID.ToString();
                    bllmc.Add(viewModel.MemberClause);
                }

                SVASessionInfo.CardGradeAddController = null;
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        private bool IsExistsMemberClause(int cardGradeID)
        {
            Edge.SVA.BLL.MemberClause bllmc = new SVA.BLL.MemberClause();
            string strwhere = " MemberClause.ClauseTypeCode='CardGrade' and MemberClause.ClauseSubCode=" + cardGradeID;
            return (bllmc.GetModelList(strwhere).Count > 0) ? true : false;                
        }
    }
}