﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.File;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.PointRule
{
    public class PointRuleController
    {
        protected PointRuleViewModel viewModel = new PointRuleViewModel();

        public PointRuleViewModel ViewModel
        {
            get { return viewModel; }
        }

        public void LoadViewModel(string PointRuleCode)
        {
            //Edge.SVA.BLL.PointRule_Item bll = new SVA.BLL.PointRule_Item();
            //viewModel.SubTable = bll.GetList("PointRuleCode='" + PointRuleCode + "'").Tables.Count > 0 ? bll.GetList("PointRuleCode='" + PointRuleCode + "'").Tables[0] : null;
            //Tools.DataTool.AddEntityTypeName(viewModel.SubTable, "EntityTypeName", "EntityType");
            Edge.SVA.BLL.PointRule_Store bll = new SVA.BLL.PointRule_Store();
            DataSet ds = bll.GetList("PointRuleCode='" + PointRuleCode + "'");
            Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");
            viewModel.SubTable = ds.Tables[0];
        }

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PointRule bll = new SVA.BLL.PointRule();
                //Edge.SVA.BLL.PointRule_Item bllitem = new SVA.BLL.PointRule_Item();
                ////保存
                //bll.Add(viewModel.MainTable);
                //if (viewModel.SubTable != null && viewModel.SubTable.Rows.Count > 0)
                //{
                //    foreach (DataRow dr in viewModel.SubTable.Rows)
                //    {
                //        Edge.SVA.Model.PointRule_Item mode = new SVA.Model.PointRule_Item();
                //        mode.PointRuleCode = dr["PointRuleCode"].ToString();
                //        mode.EntityType = Convert.ToInt32(dr["EntityType"].ToString());
                //        mode.EntityNum = dr["EntityNum"].ToString();
                //        bllitem.Add(mode);
                //    }
                //}
                Edge.SVA.BLL.PointRule_Store bllitem = new SVA.BLL.PointRule_Store();
                //保存
                bll.Add(viewModel.MainTable);
                if (viewModel.SubTable != null && viewModel.SubTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in viewModel.SubTable.Rows)
                    {
                        Edge.SVA.Model.PointRule_Store mode = new SVA.Model.PointRule_Store();
                        mode.PointRuleCode = dr["PointRuleCode"].ToString();
                        mode.StoreID = Convert.ToInt32(dr["StoreID"]);
                        bllitem.Add(mode);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.PointRule bll = new SVA.BLL.PointRule();

                //保存
                bll.Update(viewModel.MainTable);

                if (viewModel.SubTable != null && viewModel.SubTable.Rows.Count > 0)
                {
                    Edge.SVA.BLL.PointRule_Store bllitem = new SVA.BLL.PointRule_Store();
                    //先删除再保存
                    string sql = "delete PointRule_Store where PointRuleCode='" + viewModel.MainTable.PointRuleCode + "'";
                    DBUtility.DbHelperSQL.ExecuteSql(sql);

                    foreach (DataRow dr in viewModel.SubTable.Rows)
                    {
                        Edge.SVA.Model.PointRule_Store mode = new SVA.Model.PointRule_Store();
                        mode.PointRuleCode = dr["PointRuleCode"].ToString();
                        mode.StoreID = Convert.ToInt32(dr["StoreID"]);
                        bllitem.Add(mode);
                    }
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Delete(string PointRuleCode)
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                string sql1 = "delete PointRule where PointRuleCode='" + PointRuleCode + "'";
                string sql2 = "delete PointRule_Store where PointRuleCode='" + PointRuleCode + "'";

                DBUtility.DbHelperSQL.ExecuteSql(sql1);
                DBUtility.DbHelperSQL.ExecuteSql(sql2);               
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetDayFlagList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.DAYFLAG bll = new Edge.SVA.BLL.DAYFLAG();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            //获取排序字段
            string orderStr = "DayFlagCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddDayFlagName(ds, "Day1FlagName", "Day1Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day2FlagName", "Day2Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day3FlagName", "Day3Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day4FlagName", "Day4Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day5FlagName", "Day5Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day6FlagName", "Day6Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day7FlagName", "Day7Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day8FlagName", "Day8Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day9FlagName", "Day9Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day10FlagName", "Day10Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day11FlagName", "Day11Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day12FlagName", "Day12Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day13FlagName", "Day13Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day14FlagName", "Day14Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day15FlagName", "Day15Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day16FlagName", "Day16Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day17FlagName", "Day17Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day18FlagName", "Day18Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day19FlagName", "Day19Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day20FlagName", "Day20Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day21FlagName", "Day21Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day22FlagName", "Day22Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day23FlagName", "Day23Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day24FlagName", "Day24Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day25FlagName", "Day25Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day26FlagName", "Day26Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day27FlagName", "Day27Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day28FlagName", "Day28Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day29FlagName", "Day29Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day30FlagName", "Day30Flag");
            Tools.DataTool.AddDayFlagName(ds, "Day31FlagName", "Day31Flag");

            return ds;
        }

        public DataSet GetMonthFlagList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.MONTHFLAG bll = new Edge.SVA.BLL.MONTHFLAG();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            //获取排序字段
            string orderStr = "MonthFlagCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddDayFlagName(ds, "JanuaryFlagName", "JanuaryFlag");
            Tools.DataTool.AddDayFlagName(ds, "FebruaryFlagName", "FebruaryFlag");
            Tools.DataTool.AddDayFlagName(ds, "MarchFlagName", "MarchFlag");
            Tools.DataTool.AddDayFlagName(ds, "AprilFlagName", "AprilFlag");
            Tools.DataTool.AddDayFlagName(ds, "MayFlagName", "MayFlag");
            Tools.DataTool.AddDayFlagName(ds, "JuneFlagName", "JuneFlag");
            Tools.DataTool.AddDayFlagName(ds, "JulyFlagName", "JulyFlag");
            Tools.DataTool.AddDayFlagName(ds, "AugustFlagName", "AugustFlag");
            Tools.DataTool.AddDayFlagName(ds, "SeptemberFlagName", "SeptemberFlag");
            Tools.DataTool.AddDayFlagName(ds, "OctoberFlagName", "OctoberFlag");
            Tools.DataTool.AddDayFlagName(ds, "NovemberFlagName", "NovemberFlag");
            Tools.DataTool.AddDayFlagName(ds, "DecemberFlagName", "DecemberFlag");

            return ds;
        }

        public DataSet GetWeekFlagList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.WEEKFLAG bll = new Edge.SVA.BLL.WEEKFLAG();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);

            //获取排序字段
            string orderStr = "WeekFlagCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddDayFlagName(ds, "SundayFlagName", "SundayFlag");
            Tools.DataTool.AddDayFlagName(ds, "MondayFlagName", "MondayFlag");
            Tools.DataTool.AddDayFlagName(ds, "TuesdayFlagName", "TuesdayFlag");
            Tools.DataTool.AddDayFlagName(ds, "WednesdayFlagName", "WednesdayFlag");
            Tools.DataTool.AddDayFlagName(ds, "ThursdayFlagName", "ThursdayFlag");
            Tools.DataTool.AddDayFlagName(ds, "FridayFlagName", "FridayFlag");
            Tools.DataTool.AddDayFlagName(ds, "SaturdayFlagName", "SaturdayFlag");

            return ds;
        }
    }
}