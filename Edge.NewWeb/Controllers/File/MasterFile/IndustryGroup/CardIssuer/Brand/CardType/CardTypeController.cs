﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Edge.Web.Controllers.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public class CardTypeController
    {
        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();

            //获得总条数
            //获得总条数
            recodeCount = bll.GetCount(strWhere);
            //获取排序字段
            string orderStr = "CardTypeCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(pageSize, pageIndex, strWhere, orderStr);

            Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");

            ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));
            Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
            Edge.SVA.BLL.CardIssuer bllCardIssuer = new Edge.SVA.BLL.CardIssuer();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(int.Parse(row["BrandID"].ToString()));
                if (mBrand != null)
                {
                    Edge.SVA.Model.CardIssuer mCardIssuer = bllCardIssuer.GetModel(mBrand.CardIssuerID.GetValueOrDefault());
                    if (mCardIssuer != null) row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetStringByCulture(mCardIssuer.CardIssuerName1, mCardIssuer.CardIssuerName2, mCardIssuer.CardIssuerName3);
                }

            }
            return ds;
        }
    }
}