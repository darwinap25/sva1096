﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.SVA.Model.Domain.WebBuying;
using System.Data;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions;
using Edge.SVA.Model.Domain.PromotionInfos.Promotions.BasicViewModels;
using Edge.SVA.BLL.Domain.DataResources;
using Edge.SVA.Model.Domain;
using System.Text;

namespace Edge.Web.Controllers.File.MasterFile.PromotionInfos.Promotion
{
    public class BuyingNewPromotionController
    {

        private bool isCopy = false;

        public bool IsCopy
        {
            get { return isCopy; }
            set { isCopy = value; }
        }

        DropdownListRepostory ddlsource = DropdownListRepostory.Singleton;

        protected NewPromotionViewModel viewModel = new NewPromotionViewModel();

        public NewPromotionViewModel ViewModel
        {
            get { return viewModel; }
        }
        private string lan;

        public void LoadViewModel(string PromotionCode, string lan)
        {
            this.lan = lan;
            Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();
            Edge.SVA.Model.Promotion_H model = bll.GetModel(PromotionCode);
            viewModel.MainTable = model;

            Edge.SVA.BLL.Promotion_Member bllmember = new SVA.BLL.Promotion_Member();
            viewModel.PromotionMemberList = bllmember.GetNewModelList("PromotionCode = '" + PromotionCode + "'");
            if (ViewModel.PromotionMemberList.Count > 0)
            {
                foreach (var item in ViewModel.PromotionMemberList)
                {
                    item.OprFlag = "Update";
                    item.LoyaltyTypeName = ddlsource.GetDropdownListText("LoyaltyType", lan, item.LoyaltyType.ToString());
                    item.LoyaltyBirthdayFlagName = ddlsource.GetDropdownListText("LoyaltyBirthdayFlag", lan, item.LoyaltyBirthdayFlag.ToString());
                    item.LoyaltyPromoScopeName = ddlsource.GetDropdownListText("LoyaltyPromoScope", lan, item.LoyaltyPromoScope.ToString());
                    if (lan == "en-us")
                    {
                        switch (item.LoyaltyType)
                        {
                            case 1:
                                Edge.SVA.Model.Brand b = BrandRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (b != null)
                                {
                                    item.LoyaltyValueName = b.BrandName1;
                                }
                                break;
                            case 2:
                                Edge.SVA.Model.CardType c = CardTypeRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (c != null)
                                {
                                    item.LoyaltyValueName = c.CardTypeName1;
                                }
                                break;
                            case 3:
                                Edge.SVA.Model.CardGrade d = CardGradeRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (d != null)
                                {
                                    item.LoyaltyValueName = d.CardGradeName1;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else if (lan == "zh-cn")
                    {
                        switch (item.LoyaltyType)
                        {
                            case 1:
                                Edge.SVA.Model.Brand b = BrandRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (b != null)
                                {
                                    item.LoyaltyValueName = b.BrandName2;
                                }
                                break;
                            case 2:
                                Edge.SVA.Model.CardType c = CardTypeRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (c != null)
                                {
                                    item.LoyaltyValueName = c.CardTypeName2;
                                }
                                break;
                            case 3:
                                Edge.SVA.Model.CardGrade d = CardGradeRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (d != null)
                                {
                                    item.LoyaltyValueName = d.CardGradeName2;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else if (lan == "zh=hk")
                    {
                        switch (item.LoyaltyType)
                        {
                            case 1:
                                Edge.SVA.Model.Brand b = BrandRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (b != null)
                                {
                                    item.LoyaltyValueName = b.BrandName3;
                                }
                                break;
                            case 2:
                                Edge.SVA.Model.CardType c = CardTypeRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (c != null)
                                {
                                    item.LoyaltyValueName = c.CardTypeName3;
                                }
                                break;
                            case 3:
                                Edge.SVA.Model.CardGrade d = CardGradeRepostory.Singleton.GetModelByID(item.LoyaltyValue.Value);
                                if (d != null)
                                {
                                    item.LoyaltyValueName = d.CardGradeName3;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            Edge.SVA.BLL.Promotion_Hit bllhit = new SVA.BLL.Promotion_Hit();
            viewModel.PromotionHitList = bllhit.GetNewModelList("PromotionCode = '" + PromotionCode + "'");
            if (ViewModel.PromotionHitList.Count > 0)
            {
                foreach (var item in ViewModel.PromotionHitList)
                {
                    item.OprFlag = "Update";
                    //item.LogicalOprName = DALTool.GetLogicalOprName(item.HitLogicalOpr.ToString());
                    //item.HitTypeName = DALTool.GetHitTypeName(item.HitType.ToString());
                    //item.HitItemName = DALTool.GetHitItemName(item.HitItem.ToString());
                    //item.HitOPName = DataTool.GetHitOPName(Convert.ToInt32(item.HitOP));
                    item.HitTypeName = ddlsource.GetDropdownListText("HitType", lan, item.HitType.ToString());
                    item.HitItemName = ddlsource.GetDropdownListText("HitItem", lan, item.HitItem.ToString());
                    item.HitOPName = ddlsource.GetDropdownListText("HitOP", lan, item.HitOP.ToString());
                    //DataTool.AddEntityTypeName(item.HitPluTable, "EntityTypeName", "EntityType");

                    item.HitPluTable.Columns.Add(new DataColumn("EntityTypeName", typeof(string)));
                    item.HitPluTable.Columns.Add(new DataColumn("PLUHitTypeName", typeof(string)));
                    item.HitPluTable.Columns.Add(new DataColumn("HitSignName", typeof(string)));
                    if (item.HitPluTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in item.HitPluTable.Rows)
                        {
                            row["EntityTypeName"] = ddlsource.GetDropdownListText("EntityType", lan, row["EntityType"].ToString());
                            row["PLUHitTypeName"] = ddlsource.GetDropdownListText("PLUHitType", lan, row["PLUHitType"].ToString());
                            row["HitSignName"] = ddlsource.GetDropdownListText("HitSign", lan, row["HitSign"].ToString());
                        }
                    }
                }
            }

            Edge.SVA.BLL.Promotion_Gift bllgift = new SVA.BLL.Promotion_Gift();
            viewModel.PromotionGiftList = bllgift.GetNewModelList("PromotionCode = '" + PromotionCode + "'");
            if (ViewModel.PromotionGiftList.Count > 0)
            {
                foreach (var item in ViewModel.PromotionGiftList)
                {
                    item.OprFlag = "Update";
                    //item.PromotionGiftTypeName = DALTool.GetPromotionGiftTypeName(item.PromotionGiftType.ToString());
                    //item.LogicalOprName = DALTool.GetLogicalOprName(item.GiftLogicalOpr.ToString());
                    //item.GiftTypeName = DALTool.GetPromotionGiftTypeName(item.PromotionGiftType.ToString());
                    //item.GiftItemName = DALTool.GetPromotionGiftItemName(item.PromotionGiftItem.ToString());
                    //item.DiscountTypeName = DALTool.GetPromotionDiscountTypeName(item.PromotionDiscountType.ToString());
                    //item.DiscountOnName = DALTool.GetPromotionDiscountOnName(item.PromotionDiscountOn.ToString());
                    //DataTool.AddEntityTypeName(item.GiftPluTable, "EntityTypeName", "EntityType");

                    item.PromotionGiftTypeName = ddlsource.GetDropdownListText("PromotionGiftType", lan, item.PromotionGiftType.ToString());
                    if (item.PromotionGiftType == 3)
                    {
                        Edge.SVA.Model.CouponType c = CouponTypeRepostory.Singleton.GetModelByID((int)item.PromotionValue);
                        if (lan == "en-us")
                        {
                            item.PromotionValueName = string.Format("{0} - {1}", c.CouponTypeCode, c.CouponTypeName1);
                        }
                        else if (lan == "zh-cn")
                        {
                            item.PromotionValueName = string.Format("{0} - {1}", c.CouponTypeCode, c.CouponTypeName2);
                        }
                        else
                        {
                            item.PromotionValueName = string.Format("{0} - {1}", c.CouponTypeCode, c.CouponTypeName3);
                        }
                    }
                    else
                    {
                        if (item.PromotionValue.HasValue)
                        {
                            item.PromotionValueName = item.PromotionValue.Value.ToString("N2");                            
                        }
                    }

                }
            }
        }
        private string GetStringValue(DataRow dr, string name)
        {
            if (dr[name] != null)
            {
                return dr[name].ToString();
            }
            return string.Empty;
        }
        #region 操作Hit
        public void AddPromotionHitViewModel(string lan, PromotionHitViewModel model)
        {
            this.viewModel.PromotionHitList.Add(model);
        }

        public void UpdatePromotionHitViewModel(string lan, PromotionHitViewModel model)
        {
            PromotionHitViewModel vm = this.viewModel.PromotionHitList.Find(mm => mm.HitSeq.Equals(model.HitSeq));
            if (vm != null)
            {
                DataCopyUtil.CopyData(model, vm);
            }
            //else
            //{
            //    vm.OprFlag = "Add";
            //}
        }

        public bool ValidateHitIsExists(PromotionHitViewModel model)
        {
            PromotionHitViewModel vm = this.viewModel.PromotionHitList.Find(mm => mm.HitSeq.Equals(model.HitSeq) && mm.OprFlag != "Delete");
            if (vm == null)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region 操作Gift
        public void AddPromotionGiftViewModel(string lan, PromotionGiftViewModel model)
        {
            this.viewModel.PromotionGiftList.Add(model);
        }

        public void UpdatePromotionGiftViewModel(string lan, PromotionGiftViewModel model)
        {
            PromotionGiftViewModel vm = this.viewModel.PromotionGiftList.Find(mm => mm.GiftSeq.Equals(model.GiftSeq));
            if (vm != null)
            {
                DataCopyUtil.CopyData(model, vm);
            }
            //else
            //{
            //    vm.OprFlag = "Add";
            //}
        }

        public bool ValidateGiftIsExists(PromotionGiftViewModel model)
        {
            PromotionGiftViewModel vm = this.viewModel.PromotionGiftList.Find(mm => mm.GiftSeq.Equals(model.GiftSeq) && mm.OprFlag != "Delete");
            if (vm == null)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region 操作Member
        public void AddPromotionMemberViewModel(string lan, PromotionMemberViewModel model)
        {
            this.viewModel.PromotionMemberList.Add(model);
        }

        public void UpdatePromotionMemberViewModel(string lan, PromotionMemberViewModel model)
        {
            PromotionMemberViewModel vm = this.viewModel.PromotionMemberList.Find(mm => mm.ObjectKey.Equals(model.ObjectKey));
            if (vm != null)
            {
                DataCopyUtil.CopyData(model, vm);
            }
            //else
            //{
            //    vm.OprFlag = "Add";
            //}
        }

        public bool ValidateMemberIsExists(PromotionMemberViewModel model)
        {
            PromotionMemberViewModel vm = this.viewModel.PromotionMemberList.Find(mm => mm.ObjectKey.Equals(model.ObjectKey) && mm.OprFlag != "Delete");
            if (vm == null)
            {
                return false;
            }
            return true;
        }
        #endregion

        public ExecResult Add()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();

                //保存
                if (ValidateForm() != "")
                {
                    rtn.Message = ValidateForm();
                }
                if (ValidateObject(viewModel.MainTable.PromotionCode) != "")
                {
                    rtn.Message = ValidateObject(viewModel.MainTable.PromotionCode);
                }
                if (rtn.Message == "")
                {
                    bll.Add(viewModel.MainTable);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Update()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                if (!IsCopy)
                {
                    Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();
                    Edge.SVA.BLL.Promotion_Hit bllhit = new SVA.BLL.Promotion_Hit();
                    Edge.SVA.BLL.Promotion_Hit_PLU bllhitplu = new SVA.BLL.Promotion_Hit_PLU();

                    Edge.SVA.BLL.Promotion_Gift bllgift = new SVA.BLL.Promotion_Gift();
                    //// Edge.SVA.BLL.Promotion_Gift_PLU bllgiftplu = new SVA.BLL.Promotion_Gift_PLU();

                    Edge.SVA.BLL.Promotion_Member bllmember = new SVA.BLL.Promotion_Member();

                    //保存
                    if (ValidateForm() != "")
                    {
                        rtn.Message = ValidateForm();
                    }
                    if (rtn.Message == "")
                    {
                        bll.Update(viewModel.MainTable);
                        #region 保存Hit
                        if (ViewModel.PromotionHitList.Count > 0)
                        {
                            foreach (var item in ViewModel.PromotionHitList)
                            {
                                if (item.OprFlag == "Add")
                                {
                                    bllhit.Add(item);
                                    //保存HitPLU
                                    foreach (DataRow dr in item.HitPluTable.Rows)
                                    {
                                        Edge.SVA.Model.Promotion_Hit_PLU mode = new SVA.Model.Promotion_Hit_PLU();
                                        mode.HitPLUSeq = int.Parse(dr["HitPLUSeq"].ToString());
                                        mode.PLUHitType = int.Parse(dr["PLUHitType"].ToString());
                                        mode.HitSign = int.Parse(dr["HitSign"].ToString());
                                        mode.PromotionCode = item.PromotionCode;
                                        mode.HitSeq = item.HitSeq;
                                        mode.EntityType = Tools.ConvertTool.ToInt(dr["EntityType"].ToString());
                                        mode.EntityNum = GetStringValue(dr, "EntityNum");
                                        bllhitplu.Add(mode);
                                    }
                                }
                                else if (item.OprFlag == "Update")
                                {
                                    bllhit.Update(item);
                                    //保存HitPLU
                                    bllhitplu.DeleteData(item.PromotionCode, item.HitSeq);
                                    foreach (DataRow dr in item.HitPluTable.Rows)
                                    {
                                        Edge.SVA.Model.Promotion_Hit_PLU mode = new SVA.Model.Promotion_Hit_PLU();
                                        mode.HitPLUSeq = int.Parse(dr["HitPLUSeq"].ToString());
                                        mode.PLUHitType = int.Parse(dr["PLUHitType"].ToString());
                                        mode.HitSign = int.Parse(dr["HitSign"].ToString());
                                        mode.PromotionCode = item.PromotionCode;
                                        mode.HitSeq = item.HitSeq;
                                        mode.EntityType = Tools.ConvertTool.ToInt(dr["EntityType"].ToString());
                                        mode.EntityNum = GetStringValue(dr, "EntityNum");
                                        bllhitplu.Add(mode);
                                    }
                                }
                                else if (item.OprFlag == "Delete")
                                {
                                    StringBuilder sb = new StringBuilder();
                                    foreach (DataRow dr1 in item.HitPluTable.Rows)
                                    {
                                        string keyid = GetStringValue(dr1, "KeyID");
                                        if (!string.IsNullOrEmpty(keyid.Trim()))
                                        {
                                            sb.Append(keyid + ",");
                                        }
                                    }
                                    if (sb.Length >= 1)
                                    {
                                        sb.Remove(sb.Length - 1, 1);
                                        bllhitplu.DeleteList(sb.ToString());
                                    }
                                    bllhit.Delete(item.PromotionCode, item.HitSeq);
                                }
                            }
                        }
                        else
                        {
                            bllhit.DeleteData(viewModel.MainTable.PromotionCode);
                        }
                        #endregion

                        #region 保存Gift
                        if (ViewModel.PromotionGiftList.Count > 0)
                        {
                            foreach (var item in ViewModel.PromotionGiftList)
                            {
                                if (item.OprFlag == "Add")
                                {
                                    bllgift.Add(item);
                                    ////保存HitPLU
                                    //foreach (DataRow dr in item.GiftPluTable.Rows)
                                    //{
                                    //    Edge.SVA.Model.Promotion_Gift_PLU mode = new SVA.Model.Promotion_Gift_PLU();
                                    //    mode.PromotionCode = item.PromotionCode;
                                    //    mode.GiftSeq = item.GiftSeq;
                                    //    mode.EntityType = Tools.ConvertTool.ToInt(dr["EntityType"].ToString());
                                    //    mode.EntityNum = dr["EntityNum"].ToString();
                                    //    bllgiftplu.Add(mode);
                                    //}
                                }
                                else if (item.OprFlag == "Update")
                                {
                                    bllgift.Update(item);
                                }
                                else if (item.OprFlag == "Delete")
                                {
                                    bllgift.Delete(item.PromotionCode, item.GiftSeq);
                                }
                            }
                        }
                        else
                        {
                            bllgift.DeleteData(viewModel.MainTable.PromotionCode);
                        }
                        #endregion

                        #region 保存Member
                        if (ViewModel.PromotionMemberList.Count > 0)
                        {
                            foreach (var item in ViewModel.PromotionMemberList)
                            {
                                if (item.OprFlag == "Add")
                                {
                                    bllmember.Add(item);
                                }
                                else if (item.OprFlag == "Delete")
                                {
                                    bllmember.Delete(item.KeyID);
                                }
                            }
                        }
                        else
                        {
                            bllmember.DeleteData(viewModel.MainTable.PromotionCode);
                        }
                        #endregion
                    }
                }
                else
                {
                    Copy();
                }
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Copy()
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();

                #region 新增
                if (ValidateForm() != "")
                {
                    rtn.Message = ValidateForm();
                }
                if (ValidateObject(viewModel.MainTable.PromotionCode) != "")
                {
                    rtn.Message = ValidateObject(viewModel.MainTable.PromotionCode);
                }
                if (rtn.Message == "")
                {
                    viewModel.MainTable.ApproveStatus = "P";
                    viewModel.MainTable.ApprovalCode = string.Empty;
                    viewModel.MainTable.ApproveBusDate = null;
                    viewModel.MainTable.ApproveBy = null;
                    viewModel.MainTable.ApproveOn = null;
                    bll.Add(viewModel.MainTable);
                }
                #endregion

                string newPromotionCode = viewModel.MainTable.PromotionCode;

                #region 更新Code
                Edge.SVA.BLL.Promotion_Hit bllhit = new SVA.BLL.Promotion_Hit();
                Edge.SVA.BLL.Promotion_Hit_PLU bllhitplu = new SVA.BLL.Promotion_Hit_PLU();

                Edge.SVA.BLL.Promotion_Gift bllgift = new SVA.BLL.Promotion_Gift();
                //// Edge.SVA.BLL.Promotion_Gift_PLU bllgiftplu = new SVA.BLL.Promotion_Gift_PLU();

                Edge.SVA.BLL.Promotion_Member bllmember = new SVA.BLL.Promotion_Member();

                //保存
                if (ValidateForm() != "")
                {
                    rtn.Message = ValidateForm();
                }
                if (rtn.Message == "")
                {
                    //bll.Update(viewModel.MainTable);
                    #region 保存Hit
                    if (ViewModel.PromotionHitList.Count > 0)
                    {
                        foreach (var item in ViewModel.PromotionHitList)
                        {
                            item.PromotionCode = newPromotionCode;
                            if (item.OprFlag == "Delete")
                            {
                                //StringBuilder sb = new StringBuilder();
                                //foreach (DataRow dr1 in item.HitPluTable.Rows)
                                //{
                                //    string keyid = GetStringValue(dr1, "KeyID");
                                //    if (!string.IsNullOrEmpty(keyid.Trim()))
                                //    {
                                //        sb.Append(keyid + ",");
                                //    }
                                //}
                                //if (sb.Length >= 1)
                                //{
                                //    sb.Remove(sb.Length - 1, 1);
                                //    bllhitplu.DeleteList(sb.ToString());
                                //}
                                //bllhit.Delete(item.PromotionCode, item.HitSeq);
                            }
                            else
                            {
                                bllhit.Add(item);
                                //保存HitPLU
                                foreach (DataRow dr in item.HitPluTable.Rows)
                                {
                                    Edge.SVA.Model.Promotion_Hit_PLU mode = new SVA.Model.Promotion_Hit_PLU();
                                    mode.HitPLUSeq = int.Parse(dr["HitPLUSeq"].ToString());
                                    mode.PLUHitType = int.Parse(dr["PLUHitType"].ToString());
                                    mode.HitSign = int.Parse(dr["HitSign"].ToString());
                                    mode.PromotionCode = newPromotionCode;
                                    mode.HitSeq = item.HitSeq;
                                    mode.EntityType = Tools.ConvertTool.ToInt(dr["EntityType"].ToString());
                                    mode.EntityNum = GetStringValue(dr, "EntityNum");
                                    bllhitplu.Add(mode);
                                }
                            }
                        }
                    }
                    else
                    {
                        bllhit.DeleteData(viewModel.MainTable.PromotionCode);
                    }
                    #endregion

                    #region 保存Gift
                    if (ViewModel.PromotionGiftList.Count > 0)
                    {
                        foreach (var item in ViewModel.PromotionGiftList)
                        {
                            item.PromotionCode = newPromotionCode;

                            if (item.OprFlag == "Delete")
                            {
                               // bllgift.Delete(item.PromotionCode, item.GiftSeq);
                            }
                            else
                            {
                                bllgift.Add(item);
                                ////保存HitPLU
                                //foreach (DataRow dr in item.GiftPluTable.Rows)
                                //{
                                //    Edge.SVA.Model.Promotion_Gift_PLU mode = new SVA.Model.Promotion_Gift_PLU();
                                //    mode.PromotionCode = item.PromotionCode;
                                //    mode.GiftSeq = item.GiftSeq;
                                //    mode.EntityType = Tools.ConvertTool.ToInt(dr["EntityType"].ToString());
                                //    mode.EntityNum = dr["EntityNum"].ToString();
                                //    bllgiftplu.Add(mode);
                                //}
                            }
                        }
                    }
                    else
                    {
                        bllgift.DeleteData(viewModel.MainTable.PromotionCode);
                    }
                    #endregion

                    #region 保存Member
                    if (ViewModel.PromotionMemberList.Count > 0)
                    {
                        foreach (var item in ViewModel.PromotionMemberList)
                        {
                            item.PromotionCode = newPromotionCode;

                            if (item.OprFlag == "Delete")
                            {
                               // bllmember.Delete(item.KeyID);
                            }
                            else       
                            {
                                bllmember.Add(item);
                            }
                        }
                    }
                    else
                    {
                        bllmember.DeleteData(viewModel.MainTable.PromotionCode);
                    }
                    #endregion
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public ExecResult Delete(string PromotionCode)
        {
            ExecResult rtn = ExecResult.CreateExecResult();
            try
            {
                Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();
                //Edge.SVA.BLL.BUY_MNM_D blld = new SVA.BLL.BUY_MNM_D();

                if (rtn.Message == "")
                {
                    bll.Delete(PromotionCode);
                    //blld.DeleteData(PromotionCode);
                }

            }
            catch (System.Exception ex)
            {
                rtn.Ex = ex;
            }
            return rtn;
        }

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount, string sortFieldStr)
        {
            DataSet ds;
            Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();

            //获得总条数
            recodeCount = bll.GetRecordCount(strWhere);
            //获取排序字段
            string orderStr = "PromotionCode";
            if (!string.IsNullOrEmpty(sortFieldStr))
            {
                orderStr = sortFieldStr;
            }

            ds = bll.GetListByPage(strWhere, orderStr, pageSize * pageIndex + 1, pageSize * (pageIndex + 1));

            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            Tools.DataTool.AddStoreName(ds, "StoreName", "StoreID");
            Tools.DataTool.AddStoreGroupName(ds, "StoreGroupName", "StoreGroupID");
            Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");

            return ds;
        }

        public string ValidateForm()
        {
            return "";
        }

        public string ValidateObject(string strWhere)
        {
            Edge.SVA.BLL.Promotion_H bll = new SVA.BLL.Promotion_H();
            List<Edge.SVA.Model.Promotion_H> list = bll.GetModelList("PromotionCode = '" + strWhere + "'");
            if (list.Count > 0)
            {
                return Resources.MessageTips.ExistRecord;
            }
            return "";
        }

        public void BindStore(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.Store bll = new SVA.BLL.Store();
            DataSet ds = bll.GetList(" 1=1 order by StoreCode");
            ControlTool.BindDataSet(ddl, ds, "StoreID", "StoreName1", "StoreName2", "StoreName3", "StoreCode");
        }

        public void BindStoreGroup(FineUI.DropDownList ddl)
        {
            Edge.SVA.BLL.StoreGroup bll = new SVA.BLL.StoreGroup();
            DataSet ds = bll.GetList(" 1=1 order by StoreGroupCode");
            ControlTool.BindDataSet(ddl, ds, "StoreGroupID", "StoreGroupName1", "StoreGroupName2", "StoreGroupName3", "StoreGroupCode");
        }

        public string ApprovePromotionForApproveCode(Edge.SVA.Model.Promotion_H model, out bool isSuccess)
        {
            isSuccess = false;
            if (model == null) return "No Data";

            if (model.ApproveStatus != "P") return Resources.MessageTips.TheTransactionStatusNotPending;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            //model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Promotion_H().GetModel(model.PromotionCode);
                isSuccess = true;
                return model.ApprovalCode;
            }
            return "";
        }

        private bool VoidPromotion(Edge.SVA.Model.Promotion_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            //model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();

            return bll.Update(model);
        }

        public string BatchVoid(List<string> idList)
        {
            int success = 0;
            int count = 0;
            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                Edge.SVA.Model.Promotion_H model = new Edge.SVA.BLL.Promotion_H().GetModel(id);
                if (VoidPromotion(model)) success++;
            }
            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }

        //Add By Robin 2014-09-09 for Stop Promotion
        private bool StopPromotion(Edge.SVA.Model.Promotion_H model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "A") return false;

            model.EndDate= DateTime.Today.AddDays(-1);
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            //model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Promotion_H bll = new Edge.SVA.BLL.Promotion_H();

            return bll.Update(model);
        }

        public string BatchStop(List<string> idList)
        {
            int success = 0;
            int count = 0;
            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                Edge.SVA.Model.Promotion_H model = new Edge.SVA.BLL.Promotion_H().GetModel(id);
                if (StopPromotion(model)) success++;
            }
            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }
        //End

        public void BindLoyaltyType(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("LoyaltyType", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindLoyaltyValue(FineUI.DropDownList ddl, string val)
        {
            switch (val)
            {
                case "1":
                    ControlTool.BindBrand(ddl);
                    break;
                case "2":
                    ControlTool.BindCardType(ddl);
                    break;
                case "3":
                    ControlTool.BindCardGrade(ddl);
                    break;
                default:
                    break;
            }
        }
        public void BindLoyaltyBirthdayFlag(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("LoyaltyBirthdayFlag", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindLoyaltyPromoScope(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("LoyaltyPromoScope", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindHitType(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("HitType", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindHitOP(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("HitOP", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindHitItem(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("HitItem", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindPromotionGiftType(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("PromotionGiftType", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindEntityType(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("EntityType", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindPLUHitType(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("PLUHitType", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
        public void BindHitSign(FineUI.DropDownList ddl, bool required)
        {
            List<IKeyValue> list = ddlsource.GetDropdownList("HitSign", lan);
            ControlTool.BindIKeyValueList(ddl, list, required);
        }
    }
}