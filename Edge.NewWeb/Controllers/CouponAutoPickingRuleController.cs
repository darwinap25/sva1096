﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model;

namespace Edge.Web.Controllers
{
    public class CouponAutoPickingRuleController
    {
        private const string fields = "CouponAutoPickingRuleCode,Description,StartDate,EndDate,Status,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy";

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            Edge.SVA.BLL.CouponAutoPickingRule_H bll = new Edge.SVA.BLL.CouponAutoPickingRule_H()
            {
                StrWhere = strWhere,
                Order = "CouponAutoPickingRuleCode",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;

            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddCouponReplenishStatus(ds, "StatusName", "Status");
            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");


            return ds;
        }

        public DataTable GetDetailList(string id)
        {
            string sql = " select KeyID,CouponAutoPickingRuleCode,BrandID,StoreID from CouponAutoPickingRule_D where CouponAutoPickingRuleCode ='" + id + "' order by KeyID";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            DataTool.AddBrandName(ds, "BrandName", "BrandID");
            DataTool.AddStoreName(ds, "StoreName", "StoreID");
            DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            DataTool.AddStoreCode(ds, "StoreCode", "StoreID");
            return ds.Tables[0];
        }

        public void RunDoDailyOperCouponOrder(string couponAutoPickingRuleCode)
        {
            IDataParameter[] iData = new SqlParameter[5];
            iData[0] = new SqlParameter("@CouponAutoPickingRuleCode", SqlDbType.VarChar, 64);
            iData[0].Value = couponAutoPickingRuleCode;
            int result;
            DBUtility.DbHelperSQL.RunProcedure("AutoGenCouponPicking", iData, out result);
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us": FineUI.Alert.ShowInTop("Finish to execute!"); ; break;
                case "zh-cn": FineUI.Alert.ShowInTop("运行完成！"); ; break;
                case "zh-hk": FineUI.Alert.ShowInTop("運行完成！"); break;
            }
        }
    }
}