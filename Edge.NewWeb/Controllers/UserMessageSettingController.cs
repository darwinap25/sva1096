﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model;
namespace Edge.Web.Controllers
{
    public class UserMessageSettingController
    {
        private const string fields = "UserMessageCode,UserMessageDesc,SendUserID,StartDate,EndDate";

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            Edge.SVA.BLL.UserMessageSetting_H bll = new Edge.SVA.BLL.UserMessageSetting_H()
            {
                StrWhere = strWhere,
                Order = "UserMessageCode",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;

            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddUserName(ds, "SendUserName", "SendUserID");


            return ds;
        }

        public DataTable GetDetailList(string id)
        {
            string sql = " select KeyID,UserMessageCode,ReceiveUserID,MessageServiceTypeID,AccountNumber,(case MessageServiceTypeID when 1 then 'Email' else 'SMS' end) MessageServiceTypeName from UserMessageSetting_D where UserMessageCode ='" + id + "' order by KeyID";
            return DBUtility.DbHelperSQL.Query(sql).Tables[0];
        }
    }
}