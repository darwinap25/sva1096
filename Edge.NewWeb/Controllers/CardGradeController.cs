﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using Edge.Messages.Manager;

namespace Edge.Web.Controllers
{
    public class CardGradeController
    {
        //Add by Alex 2014-06-09 ++
        public static SVA.Model.CardGrade GetImportCardTGradeType(string cardGradeCode, Dictionary<string, SVA.Model.CardGrade> cache)
        {
            if (cache != null && cache.ContainsKey(cardGradeCode)) return cache[cardGradeCode];

            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();

            SVA.Model.CardGrade cardType = bll.GetImportCardGrade(cardGradeCode);
            cache.Add(cardGradeCode, cardType);

            return cardType;
        }
        //Add by Alex 2014-06-09 --

      
    }
}
