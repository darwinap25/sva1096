﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain;
using Edge.Utils.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model;

namespace Edge.Web.Controllers
{
    public class CouponReplenishRuleController
    {
        protected CouponReplenishRuleListViewModel viewModel = new CouponReplenishRuleListViewModel();
        public CouponReplenishRuleListViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }

        private const string fields = "CouponReplenishCode,Description,StartDate,EndDate,Status,BrandID,CouponTypeID,StoreTypeID,AutoCreateOrder,AutoApproveOrder,CreatedOn,UpdatedOn,CreatedBy,UpdatedBy";

        public DataSet GetTransactionList(string strWhere, int pageSize, int pageIndex, out int recodeCount)
        {
            Edge.SVA.BLL.CouponReplenishRule_H bll = new Edge.SVA.BLL.CouponReplenishRule_H()
            {
                StrWhere = strWhere,
                Order = "CouponReplenishCode",
                Fields = fields,
                Ascending = false
            };

            System.Data.DataSet ds = null;

            ds = bll.GetList(pageSize, pageIndex, out recodeCount);

            Tools.DataTool.AddBrandName(ds, "Brand", "BrandID");
            Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponReplenishStatus(ds, "StatusName", "Status");
            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "UpdatedByName", "UpdatedBy");


            return ds;
        }

        public bool Exists(CouponReplenishRuleViewModel model) 
        {
            foreach (CouponReplenishRuleViewModel mdl in viewModel.CouponReplenishRuleViewModelList) 
            {
                if (mdl.MainTable.OrderTargetID == model.MainTable.OrderTargetID && mdl.MainTable.StoreID == model.MainTable.StoreID) 
                {
                    return true;
                }
            }
            return false;
        }

        public void Add(CouponReplenishRuleViewModel model) 
        {
            viewModel.CouponReplenishRuleViewModelList.Add(model);
        }

        public SVA.Model.CouponReplenishRule_H GetModel(string id) 
        {
            Edge.SVA.BLL.CouponReplenishRule_H bll = new SVA.BLL.CouponReplenishRule_H();
            return bll.GetModel(id);
        }

        public void GetDetailList(string id, int StoreTypeID)
        {
            string sql = "";
            string descLan = DALTool.GetStringByCulture("1", "2", "3");
            string supplierDesc = "SupplierDesc" + descLan;
            string storeName = "StoreName" + descLan;
            if (StoreTypeID == 1)
            {
                sql = "select a.KeyID,a.CouponReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + supplierDesc + " OrderTargetName,c." + storeName + " StoreName from CouponReplenishRule_D a,Supplier b,Store c where a.OrderTargetID = b.SupplierID and a.StoreID = c.StoreID and a.CouponReplenishCode='" + id + "' order by a.KeyID";
            }
            else
            {
                sql = "select a.KeyID,a.CouponReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + storeName + " OrderTargetName,c." + storeName + " StoreName from CouponReplenishRule_D a,Store b,Store c where a.OrderTargetID = b.StoreID and a.StoreID = c.StoreID and a.CouponReplenishCode='" + id + "' order by a.KeyID";
            }
            DataTable dt = DBUtility.DbHelperSQL.Query(sql).Tables[0];
            foreach (DataRow dr in dt.Rows) 
            {
                CouponReplenishRuleViewModel model = new CouponReplenishRuleViewModel();
                model.MainTable.KeyID = StringHelper.ConvertToInt(Convert.ToString(dr["KeyID"]));
                model.MainTable.CouponReplenishCode = Convert.ToString(dr["CouponReplenishCode"]);
                model.MainTable.StoreID = StringHelper.ConvertToInt(Convert.ToString(dr["StoreID"]));
                model.MainTable.OrderTargetID = StringHelper.ConvertToInt(Convert.ToString(dr["OrderTargetID"]));
                model.MainTable.MinStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["MinStockQty"]));
                model.MainTable.RunningStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["RunningStockQty"]));
                model.MainTable.OrderRoundUpQty = StringHelper.ConvertToInt(Convert.ToString(dr["OrderRoundUpQty"]));
                model.MainTable.Priority = StringHelper.ConvertToInt(Convert.ToString(dr["Priority"]));
                model.StoreName = Convert.ToString(dr["StoreName"]);
                model.OrderTargetName = Convert.ToString(dr["OrderTargetName"]);
                this.viewModel.CouponReplenishRuleViewModelList.Add(model);
            }
        }

        //Add By Robin 2014-08-18 取所有ReplenishRule的店铺 for 过滤已经加过的店铺
        public void GetAllDetailList(int StoreTypeID, int CouponTypeID)
        {
            string sql = "";
            string descLan = DALTool.GetStringByCulture("1", "2", "3");
            string supplierDesc = "SupplierDesc" + descLan;
            string storeName = "StoreName" + descLan;
            if (StoreTypeID == 1)
            {
                sql = "select a.KeyID,a.CouponReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + supplierDesc + " OrderTargetName,c." + storeName + " StoreName from CouponReplenishRule_D a inner join CouponReplenishRule_H a1 on a.CouponReplenishCode=a1.CouponReplenishCode,Supplier b,Store c where a.OrderTargetID = b.SupplierID and a.StoreID = c.StoreID and a1.Status=1 and a1.CouponTypeID="+CouponTypeID+" order by a.KeyID";
            }
            else
            {
                sql = "select a.KeyID,a.CouponReplenishCode,a.StoreID,a.OrderTargetID,a.MinStockQty,a.RunningStockQty,a.OrderRoundUpQty,a.Priority,b." + storeName + " OrderTargetName,c." + storeName + " StoreName from CouponReplenishRule_D a inner join CouponReplenishRule_H a1 on a.CouponReplenishCode=a1.CouponReplenishCode,Store b,Store c where a.OrderTargetID = b.StoreID and a.StoreID = c.StoreID and a1.Status=1 and a1.CouponTypeID=" + CouponTypeID + " order by a.KeyID";
            }
            DataTable dt = DBUtility.DbHelperSQL.Query(sql).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                CouponReplenishRuleViewModel model = new CouponReplenishRuleViewModel();
                model.MainTable.KeyID = StringHelper.ConvertToInt(Convert.ToString(dr["KeyID"]));
                model.MainTable.CouponReplenishCode = Convert.ToString(dr["CouponReplenishCode"]);
                model.MainTable.StoreID = StringHelper.ConvertToInt(Convert.ToString(dr["StoreID"]));
                model.MainTable.OrderTargetID = StringHelper.ConvertToInt(Convert.ToString(dr["OrderTargetID"]));
                model.MainTable.MinStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["MinStockQty"]));
                model.MainTable.RunningStockQty = StringHelper.ConvertToInt(Convert.ToString(dr["RunningStockQty"]));
                model.MainTable.OrderRoundUpQty = StringHelper.ConvertToInt(Convert.ToString(dr["OrderRoundUpQty"]));
                model.MainTable.Priority = StringHelper.ConvertToInt(Convert.ToString(dr["Priority"]));
                model.StoreName = Convert.ToString(dr["StoreName"]);
                model.OrderTargetName = Convert.ToString(dr["OrderTargetName"]);
                this.viewModel.CouponReplenishRuleViewModelList.Add(model);
            }
        }

        public void RunAutoGenCouponOrder(string couponReplenishCode) 
        {
            IDataParameter[] iData = new SqlParameter[5];
            iData[0] = new SqlParameter("@UserID", SqlDbType.Int);
            iData[0].Value = DALTool.GetCurrentUser().UserID;
            iData[1] = new SqlParameter("@CouponReplenishCode", SqlDbType.VarChar,64);
            iData[1].Value = couponReplenishCode;
            iData[2] = new SqlParameter("@ReferenceNo", SqlDbType.VarChar,64);
            iData[2].Value = null;
            iData[3] = new SqlParameter("@BusDate", SqlDbType.DateTime);
            iData[3].Value = null;
            iData[4] = new SqlParameter("@TxnDate", SqlDbType.DateTime);
            iData[4].Value = null;
            int result;
            DBUtility.DbHelperSQL.RunProcedure("AutoGenCouponOrder", iData, out result);
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us": FineUI.Alert.ShowInTop("Finish to execute!"); ; break;
                case "zh-cn": FineUI.Alert.ShowInTop("运行完成！"); ; break;
                case "zh-hk": FineUI.Alert.ShowInTop("運行完成！"); break;
            }
        }
    }
}