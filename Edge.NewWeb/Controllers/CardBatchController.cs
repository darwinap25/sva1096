﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Edge.Web.Controllers
{
    public class CardBatchController
    {
        public static CardBatchController GetInstance()
        {
            return new CardBatchController();
        }

        public Dictionary<int, string> GetBatch(int top)
        {
            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            Dictionary<int, string> list = bll.GetBatchID(top);

            return list;
        }

        public Dictionary<int, string> GetBatch(int top, string partialBatch)
        {
            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            Dictionary<int, string> list = bll.GetBatchID(top, partialBatch);

            return list;
        }

        public Dictionary<int, string> GetBatch(int top, int couponTypeID)
        {
            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            Dictionary<int, string> list = bll.GetBatchID(top, couponTypeID);

            return list;
        }

        public Dictionary<int, string> GetBatch(int top, string partialBatch, int couponTypeID)
        {
            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            Dictionary<int, string> list = bll.GetBatchID(top, partialBatch, couponTypeID);

            return list;
        }
    }
}
