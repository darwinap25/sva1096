﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Edge.Web.Controllers
{
    public class BatchController
    {
        private BatchController() 
        { 
        }

        public static BatchController GetInstance()
        {
            return new BatchController();
        }

        public Dictionary<int, string> GetBatch(int top)
        {
            Edge.SVA.BLL.BatchCoupon bll = new Edge.SVA.BLL.BatchCoupon();
            Dictionary<int, string> list = bll.GetBatchID(top);

            return list;
        }

        public Dictionary<int, string> GetBatch(int top, string partialBatch)
        {
            Edge.SVA.BLL.BatchCoupon bll = new Edge.SVA.BLL.BatchCoupon();
            Dictionary<int, string> list = bll.GetBatchID(top, partialBatch);

            return list;
        }

        public Dictionary<int, string> GetBatch(int top, int couponTypeID)
        {
            Edge.SVA.BLL.BatchCoupon bll = new Edge.SVA.BLL.BatchCoupon();
            Dictionary<int, string> list = bll.GetBatchID(top, couponTypeID);

            return list;
        }

        public Dictionary<int, string> GetBatch(int top, string partialBatch, int couponTypeID)
        {
            Edge.SVA.BLL.BatchCoupon bll = new Edge.SVA.BLL.BatchCoupon();
            Dictionary<int, string> list = bll.GetBatchID(top, partialBatch, couponTypeID);

            return list;
        }

    }
}
