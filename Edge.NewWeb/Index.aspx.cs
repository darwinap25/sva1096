﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using System.Configuration;
using System.Web.Security;
using Newtonsoft.Json.Linq;
using FineUI;
using Edge.Utils.Tools;
using Edge.SVA.Model;
using Edge.Web.Tools;
using Edge.Web.Controllers;

namespace Edge.Web
{
    public partial class Index : PageBase
    {

        AccountsPrincipal user;
        User currentUser;
        public string strWelcome;
        private static string WebVersion = "";

        #region Page_Init

        protected void Page_Init(object sender, EventArgs e)
        {
            //if (Context.User.Identity.Name != string.Empty)
            //{
            //    string menuType = "menu";

            //    // 注册客户端脚本，服务器端控件ID和客户端ID的映射关系
            //    JObject ids = GetClientIDS(mainTabStrip);

            //    Tree treeMenu = InitTreeMenu();
            //    ids.Add("mainMenu", treeMenu.ClientID);
            //    ids.Add("menuType", "menu");

            //    string idsScriptStr = String.Format("window.IDS={0};", ids.ToString(Newtonsoft.Json.Formatting.None));
            //    PageContext.RegisterStartupScript(idsScriptStr);
            //}
        }

        private Tree InitTreeMenu()
        {
            Tree treeMenu = new Tree();
            treeMenu.ID = "treeMenu";
            treeMenu.EnableArrows = true;
            treeMenu.ShowBorder = false;
            treeMenu.ShowHeader = false;
            treeMenu.EnableIcons = false;
            treeMenu.AutoScroll = true;
            Region2.Items.Add(treeMenu);

            //// 重新设置每个节点的图标
            //ResolveTreeNode(treeMenu.Nodes);

            //Modified By Robin 2014-08-19 For 兼容Domain用户登录
            //user = new AccountsPrincipal(Context.User.Identity.Name, SVASessionInfo.SiteLanguage);//todo: 修改成多语言。
            user = new AccountsPrincipal(SVASessionInfo.CurrentUser.UserName, SVASessionInfo.SiteLanguage);//todo: 修改成多语言。
            //End
            currentUser = SVASessionInfo.CurrentUser;
            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
            DataSet ds;
            ds = sm.GetTreeListByLan("", SVASessionInfo.SiteLanguage);

            BindTreeView(treeMenu, ds.Tables[0]);

            ResolveTreeNode(treeMenu.Nodes);

            return treeMenu;
        }

        //邦定根节点
        public void BindTreeView(Tree treeMenu, DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + 0);//　选出所有子节点	

            //菜单状态
            string MenuExpanded = ConfigurationManager.AppSettings.Get("MenuExpanded");
            // bool menuExpand = bool.Parse(MenuExpanded);

            treeMenu.Nodes.Clear(); // 清空树
            foreach (DataRow r in drs)
            {
                if (r["KeshiPublic"].ToString().ToLower() == "true")
                {
                    string nodeid = r["NodeID"].ToString();
                    string text = r["Text"].ToString();
                    string parentid = r["ParentID"].ToString();
                    string location = r["Location"].ToString();
                    string url = r["Url"].ToString();
                    string imageurl = r["ImageUrl"].ToString();
                    int permissionid = int.Parse(r["PermissionID"].ToString().Trim());

                    //权限控制菜单		
                    if ((permissionid == -1) || (user.HasPermissionID(permissionid)))//绑定用户有权限的和没设权限的（即公开的菜单）
                    {
                        FineUI.TreeNode rootnode = new FineUI.TreeNode();
                        rootnode.Text = text;
                        rootnode.NodeID= nodeid;
                        // rootnode.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "javascript:;" : url;
                        rootnode.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "" : url;
                        rootnode.Expanded = true;
                        //rootnode.IconUrl = imageurl;

                        treeMenu.Nodes.Add(rootnode);

                        int sonparentid = int.Parse(nodeid);// or =location
                        CreateNode(sonparentid, rootnode, dt,treeMenu);
                    }

                }
            }
        }

        //邦定任意节点
        public void CreateNode(int parentid, FineUI.TreeNode parentnode, DataTable dt, Tree treeMenu)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);//选出所有子节点			
            foreach (DataRow r in drs)
            {
                if (r["KeshiPublic"].ToString().ToLower() == "true")
                {
                    string nodeid = r["NodeID"].ToString();
                    string text = r["Text"].ToString();
                    string location = r["Location"].ToString();
                    string url = r["Url"].ToString();
                    string imageurl = r["ImageUrl"].ToString();
                    int permissionid = int.Parse(r["PermissionID"].ToString().Trim());

                    //权限控制菜单
                    if ((permissionid == -1) || (user.HasPermissionID(permissionid)))
                    {

                        FineUI.TreeNode node = new FineUI.TreeNode();
                        node.Text = text;
                        node.NodeID = nodeid;
                        //node.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "javascript:;" : url;
                        node.NavigateUrl = string.IsNullOrEmpty(url.Trim()) ? "" : url;
                        //node.IconUrl = imageurl;
                        node.Expanded = false;
                        //node.Expanded=true;
                        int sonparentid = int.Parse(nodeid);// or =location

                        if (parentnode == null)
                        {
                            treeMenu.Nodes.Clear();
                            parentnode = new FineUI.TreeNode();
                            treeMenu.Nodes.Add(parentnode);
                        }
                        parentnode.Nodes.Add(node);
                        CreateNode(sonparentid, node, dt, treeMenu);
                    }//endif
                }

            }//endforeach		

        }


        private JObject GetClientIDS(params ControlBase[] ctrls)
        {
            JObject jo = new JObject();
            foreach (ControlBase ctrl in ctrls)
            {
                jo.Add(ctrl.ID, ctrl.ClientID);
            }

            return jo;
        }

        #endregion

        #region Page_Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {           

                if (Context.User.Identity.Name != string.Empty)
                {
                    // 设置样式和语言下拉列表的选中值
                    string themeValue = PageManager1.Theme.ToString().ToLower();
                    HttpCookie themeCookie = Request.Cookies["Theme"];
                    if (themeCookie != null)
                    {
                        themeValue = themeCookie.Value;
                    }
                    ddlTheme.SelectedValue = themeValue;
                    ddlLanguage.SelectedValue = PageManager1.Language.ToString().ToLower();
                    string lan = LanConvertUtil.ConvertToOldLanFromNewLan(ddlLanguage.SelectedValue);
                    Asp.Net.WebFormLib.Factory.CreateITranslater().LanguageLan = lan;
                    SVASessionInfo.SiteLanguage = lan;

                    AppController app = new AppController();
                    if (!string.IsNullOrEmpty(SVASessionInfo.CurrentUser.UserName))
                    {
                        SVASessionInfo.CurrentUser = app.GetLoginUser(SVASessionInfo.CurrentUser.UserName, lan);
                    }

                    #region 原来page init位置的函数
                    if (Context.User.Identity.Name != string.Empty)
                    {
                        string menuType = "menu";

                        // 注册客户端脚本，服务器端控件ID和客户端ID的映射关系
                        JObject ids = GetClientIDS(mainTabStrip);

                        Tree treeMenu = InitTreeMenu();
                        ids.Add("mainMenu", treeMenu.ClientID);
                        ids.Add("menuType", "menu");

                        string idsScriptStr = String.Format("window.IDS={0};", ids.ToString(Newtonsoft.Json.Formatting.None));
                        PageContext.RegisterStartupScript(idsScriptStr);
                        this.lblAdminName.Text = String.Format("<span style=\"font-weight:bold;color:red;\">{0}!</span> [{1}]","Welcome "+ currentUser.UserName, Request.UserHostAddress);
                    }
                    #endregion

                    if (string.IsNullOrEmpty(WebVersion))
                    {
                        lblVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Clear();
                    Session.Abandon();
                    Response.Clear();

                    string url = null;
                    if (Request.ApplicationPath == "/")
                    {
                        //不存在虚拟目录
                        url = Request.ApplicationPath + "Login.aspx";
                    }
                    else
                    {
                        //存在虚拟目录
                        url = Request.ApplicationPath + "/Login.aspx";
                    }
                    FineUI.Alert.ShowInTop(Resources.MessageTips.Timeout, "", FineUI.MessageBoxIcon.Warning, "top.location='" + url + "';");
                }                
            }
        }

        #endregion

        #region Event

        /// <summary>
        /// 修改样式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            HttpCookie themeCookie = new HttpCookie("Theme", ddlTheme.SelectedValue);
            themeCookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(themeCookie);

            PageContext.Refresh();
        }

        /// <summary>
        /// 修改语言
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            HttpCookie langCookie = new HttpCookie("Language", ddlLanguage.SelectedValue);
            langCookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(langCookie);
            
            string lan = LanConvertUtil.ConvertToOldLanFromNewLan(ddlLanguage.SelectedValue);
            Asp.Net.WebFormLib.Factory.CreateITranslater().LanguageLan = lan;
            SVASessionInfo.SiteLanguage = lan;
            PageContext.Refresh();
        }

        protected void logout_click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            Response.Clear();
            FineUI.PageContext.Redirect("Login.aspx");
        }

        #endregion

        #region Tree

        /// <summary>
        /// 重新设置每个节点的图标
        /// </summary>
        /// <param name="nodes"></param>
        private void ResolveTreeNode(FineUI.TreeNodeCollection nodes)
        {
            foreach (FineUI.TreeNode node in nodes)
            {
                if (node.Nodes.Count == 0)
                {
                    if (!String.IsNullOrEmpty(node.NavigateUrl))
                    {
                        node.IconUrl = GetIconForTreeNode(node.NavigateUrl);
                    }
                }
                else
                {
                    ResolveTreeNode(node.Nodes);
                }
            }
        }

        /// <summary>
        /// 根据链接地址返回相应的图标
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GetIconForTreeNode(string url)
        {
            string iconUrl = "~/images/filetype/vs_unknow.png";
            url = url.ToLower();
            int lastDotIndex = url.LastIndexOf('.');
            string fileType = url.Substring(lastDotIndex + 1);
            if (fileType == "txt")
            {
                iconUrl = "~/images/filetype/vs_txt.png";
            }
            else if (fileType == "aspx")
            {
                iconUrl = "~/images/filetype/vs_aspx.png";
            }
            else if (fileType == "htm" || fileType == "html")
            {
                iconUrl = "~/images/filetype/vs_htm.png";
            }

            return iconUrl;
        }

        #endregion
    }
}
