﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using System.IO;
using Edge.Common;
using System.Data;
using Edge.Web.DAO;
using Edge.SVA.Model.Master;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_ImportMember_H, SVA.Model.Ord_ImportMember_H>
    {
        public static string XMLDownloadPath; //Add By Robin 2015-08-31
        public static string PointsXMLDownloadPath; //Add By Dawin 2016-08-11
        ImportMemberCotroller controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ImportMemberCotroller = null;
                
            }
            controller = SVASessionInfo.ImportMemberCotroller;
            
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.ImportMemberNumber);
                this.CreatedBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.ApproveBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);

                if (controller.ViewModel.MainTable.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.ApproveOn.GetValueOrDefault());
                }
                else if (controller.ViewModel.MainTable.ApproveStatus == "P")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.Description;

                    this.btnExport.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.Description) ? true : false;//没有文件时不显示查看按钮(Len)

                    XMLDownloadPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["XMLDownloadPath"]) + controller.ViewModel.MainTable.ImportMemberNumber.ToString();//Add By Robin 2015-08-31
                    PointsXMLDownloadPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PointsXMLDownloadPath"]) + controller.ViewModel.MainTable.ImportMemberNumber.ToString();//Add By Darwin 2016-08-11

                    string[] downloadlist = GetAllFiles(XMLDownloadPath);
                    string[] pointsdownloadlist = GetAllFiles(PointsXMLDownloadPath);

                    int i=0;
                    foreach (string filename in downloadlist)
                    {
                        i++;
                        FineUI.TreeNode fileNode = new FineUI.TreeNode();
                        fileNode.EnableCheckBox = true;
                        fileNode.AutoPostBack = true;
                        fileNode.NodeID = i.ToString(); ;
                        fileNode.Text = filename;
                        FileTree.Nodes.Add(fileNode);
                    }

                   // i = 0;
                    foreach (string filename in pointsdownloadlist)
                    {
                        i++;
                        FineUI.TreeNode fileNode1 = new FineUI.TreeNode();
                        fileNode1.EnableCheckBox = true;
                        fileNode1.AutoPostBack = true;
                        fileNode1.NodeID = i.ToString(); ;
                        fileNode1.Text = filename;
                        FileTree.Nodes.Add(fileNode1);
                    }

                    if (!string.IsNullOrEmpty(Request.Params["filename"]))
                    {
                        this.Description.Text = Request.Params["filename"];
                    }
                }
                RptBind();
            }

        }

        private void RptBind()
        {
            try
            {
                DataSet ds = new DataSet();
                ImportMemberImportErrorsDAO dao = new ImportMemberImportErrorsDAO();
                Ord_ImportMember_ImportErrors vo = new Ord_ImportMember_ImportErrors();

                int count = 0;

                vo.ImportMemberNumber = Model.ImportMemberNumber;
                ds = dao.GetImportMemberImportErrors(vo);

                //count = int.Parse(ds.Tables[1].Rows[0]["RowCount"].ToString());
                this.Grid1.RecordCount = count;

                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Load ImportMemberImportErrors", "error", ex);
            }
        }

        public string[] GetAllFiles(string DownloadPath)
        {
          
            string dirPath = HttpContext.Current.Server.MapPath(DownloadPath);
            //string dirPath = DownloadPath;
            if (Directory.Exists(dirPath))
            {
                //获得目录信息
                DirectoryInfo dir = new DirectoryInfo(dirPath);
                //获得目录File List
                FileInfo[] files = dir.GetFiles("*.*");
                string[] fileNames = new string[files.Length];
                int i = 0;
                foreach (FileInfo fileInfo in files)
                {
                    fileNames[i] = fileInfo.Name;
                    i++;
                }
                return fileNames;
            }
            else
                return new string[0];
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);

            #region 处理导入数据
            #endregion
        }


        private void ExportFile(string filename)
        {
            string FileName = XMLDownloadPath + "\\" + filename;
            FileInfo file = new FileInfo(Server.MapPath(FileName));
            Response.AddHeader("content-type", "text/xml;");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlPathEncode(filename));
            Response.AddHeader("content-length", file.Length.ToString());
            Response.TransmitFile(FileName);
            Response.End();
        }

        private void ExportFile2(string filename)
        {
            string FileName = PointsXMLDownloadPath + "\\" + filename;
            FileInfo file = new FileInfo(Server.MapPath(FileName));
            Response.AddHeader("content-type", "text/xml;");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlPathEncode(filename));
            Response.AddHeader("content-length", file.Length.ToString());
            Response.TransmitFile(FileName);
            Response.End();
        }

        protected void NodeCheck(object sender, FineUI.TreeCheckEventArgs e)
        {
            foreach (FineUI.TreeNode fileNode in FileTree.GetCheckedNodes())
            {
                if (fileNode.NodeID != e.NodeID)
                {
                    fileNode.Checked = false;
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            foreach (FineUI.TreeNode fileNode in FileTree.GetCheckedNodes())            
            {
                if (fileNode.Text.Substring(0,1) == "C")
                {
                    ExportFile(fileNode.Text);
                }
                else if (fileNode.Text.Substring(0,1) == "R")
                {
                    ExportFile2(fileNode.Text);
                }
            }
            FileTree.UncheckAllNodes();


/*
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                //以XML的形式呈现
                if (controller.ViewModel.MainTable.ApproveStatus == "A")
                {
                    string xmlfilename = "CustomerImport-" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + "-01.xml";
                    Tools.ExportTool.ExportFile(fileName.Replace(".csv", ".xml"), xmlfilename);
                }
                else
                {
                    Tools.ExportTool.ExportFile(fileName);
                }
                Tools.Logger.Instance.WriteOperationLog("ImportMemberInfos download ", " filename: " + fileName);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteErrorLog("ImportMemberInfos download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }
 */ 
        }

    }
}