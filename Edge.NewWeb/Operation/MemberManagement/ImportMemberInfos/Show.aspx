﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.ImportMemberInfos.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>

<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />

<script type="text/javascript">
    function getValue() {
        var x = document.getElementById("myHeader")
        alert(x.innerHTML)
    }
</script>

    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form" EnableCollapse="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                    EnableBackgroundColor="true" LabelAlign="Right" >
                <Items>
                    <ext:Form runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow runat="server">
                                <Items>
                                    <ext:Label ID="ImportMemberNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server">
                                <Items>
                                    <ext:Label ID="ApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form3" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow2" runat="server">
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form4" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow3" runat="server">
                            <Items>
                                <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                </ext:Label>
                                <ext:Label ID="ApproveBy" runat="server" Label="批核人：">
                                </ext:Label>
                            </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow4" runat="server">
                            <Items>
                                <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                                </ext:Label>
                                <ext:Label ID="Note" runat="server" Label="Remarks：">
                                </ext:Label>
                            </Items>                   
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>


                    <ext:Form ID="Form6" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow5" runat="server" ColumnWidths="0% 80% 20%">
                                <Items>
                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                    </ext:Label>
                                    <ext:Label ID="Description" Text="" runat="server" Label="导入文件：">
                                    </ext:Label>
                                    <ext:Button ID="btnExport" runat="server" Text="下载" OnClick="btnExport_Click" Icon="PageExcel"
                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>

                    <ext:Form ID="FormFiles" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        runat="server" BodyPadding="10px" AutoHeight="true">
                        <Rows>
                            <ext:FormRow ID="FormRow6" ColumnWidths="20% 80%" runat="server">
                                <Items>
                                    <ext:Tree runat="server" ID="FileTree" Title="File List" OnNodeCheck="NodeCheck" >
                                    </ext:Tree>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>                   
                </Items>
            </ext:SimpleForm>

            <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="False" DataKeyNames="ImportMemberNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" AutoExpandColumn="true"
                        ForceFitAllTime="true">

                                <Columns>
                            <ext:TemplateField Width="60px" HeaderText="Import Member Number" SortField="ImportMemberNumber">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("ImportMemberNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Card Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardNumber" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Exception Details">
                                <ItemTemplate>
                                    <asp:Label ID="lblExceptionDetails" runat="server" Text='<%#Eval("ExceptionDetails")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField> 
                            <ext:TemplateField Width="60px" HeaderText="External Buying Unit">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("ExternalBuyingUnit")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="External Member Key">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("ExternalMemberKey")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Oldest External Member Key">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("OldestExternalMemberKey")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Surrendered Card External Member Key">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("SurrenderedExternalMemberKey")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Buying Unit Internal Key">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("BuyingUnitInternalKey")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Old Card Expiration Date">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("OldCardExpirationDate")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Customer Tier">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerTierCode" runat="server" Text='<%#Eval("CustomerTierCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Business Owner">
                                <ItemTemplate>
                                    <asp:Label ID="lblBusinessOwner" runat="server" Text='<%#Eval("BusinessOwner")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Email Subscription">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailSubscription" runat="server" Text='<%#Eval("EmailSubscription")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="SMS Subscription">
                                <ItemTemplate>
                                    <asp:Label ID="lblSMSSubscription" runat="server" Text='<%#Eval("SMSSubscription")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>                            
                            <ext:TemplateField Width="60px" HeaderText="RRC Type Of Applicationn">
                                <ItemTemplate>
                                    <asp:Label ID="lblRRCTypeOfApplication" runat="server" Text='<%#Eval("RRCTypeOfApplication")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField> 
                            <ext:TemplateField Width="60px" HeaderText="Retailer ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblRetailerID" runat="server" Text='<%#Eval("RetailerID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField> 
                            <ext:TemplateField Width="60px" HeaderText="Old Card No">
                                <ItemTemplate>
                                    <asp:Label ID="lblOldCardNo" runat="server" Text='<%#Eval("OldCardNo")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField> 
                            <ext:TemplateField Width="60px" HeaderText="Loyalty Card">
                                <ItemTemplate>
                                    <asp:Label ID="lblLoyaltyCard" runat="server" Text='<%#Eval("LoyaltyCard")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField> 
                            
                        </Columns>
                    </ext:Grid>
        </Items>


    </ext:Panel>
<%--    <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="True" DataKeyNames="ImportMemberNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true"
                        ForceFitAllTime="true">

                                <Columns>
                            <ext:TemplateField Width="60px" HeaderText="Import Member Number" SortField="ImportMemberNumber">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("ImportMemberNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Card Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardNumber" runat="server" Text='<%#Eval("RRCCardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Customer Tier">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerTierCode" runat="server" Text='<%#Eval("CustomerTierCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Business Owner">
                                <ItemTemplate>
                                    <asp:Label ID="lblBusinessOwner" runat="server" Text='<%#Eval("BusinessOwner")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="EmailSubscription">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailSubscription" runat="server" Text='<%#Eval("EmailSubscription")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="SMS Subscription">
                                <ItemTemplate>
                                    <asp:Label ID="lblSMSSubscription" runat="server" Text='<%#Eval("SMSSubscription")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>                            
                        </Columns>
                    </ext:Grid>
                    </Items>
            </ext:Panel>--%>

    <uc2:checkright ID="Checkright1" runat="server" />

    </form>
</body>
</html>
