﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.Messages.Manager;
using System.Data;
using FineUI;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImportMemberCotroller controller = new ImportMemberCotroller();
            if (!this.IsPostBack)
            {
                //try
                //{
                    btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    string files = Request.Params["filenames"];//文件名集合
                    if (string.IsNullOrEmpty(ids))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));

                    //传入配置文件中HQDB的相关值
                    controller.connString = webset.HQDBconnectString;
                    controller.sqlString = webset.HQEnqueryString;
                    controller.sqlString2 = webset.HQEnqueryString2;


                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");
                    List<string> fileList = Edge.Utils.Tools.StringHelper.SplitString(files, ",");
                    bool isSuccess = false;

                    for (int i = 0; i < idList.Count; i++)
                    {
                        Edge.SVA.Model.Ord_ImportMember_H mode = new Edge.SVA.BLL.Ord_ImportMember_H().GetModel(idList[i]);

                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = idList[i];
                        if (!string.IsNullOrEmpty(fileList[i]))
                        {
                            
                            string filename = Server.MapPath("~" + fileList[i]);
                            string ImportPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CSVImportPath"]);
                            ImportPath = ImportPath + mode.ImportMemberNumber + "/";
                            ImportPath = Server.MapPath(ImportPath);


                            if (!Directory.Exists(ImportPath))
                            {
                            //    Directory.CreateDirectory(ImportPath);
                                Directory.CreateDirectory(ImportPath);
                            }

                            ImportPath = ImportPath + System.IO.Path.GetFileName(filename);
                            if (System.IO.File.Exists(filename))
                            {
                                System.IO.File.Copy(filename, ImportPath, true);
                                System.IO.File.Delete(filename);

                                mode.FileName = Path.GetFileName(fileList[i].ToString());
                                mode.ImportStatus = "P";
                                
                            }
                        }

                        //批核成功后要将CSV文件转换为XML文件
                        /*   remove convert by gavin @2015-08-26
                        if (!string.IsNullOrEmpty(fileList[i]))
                        {
                            string filename = Server.MapPath("~" + fileList[i]);
                            if (!controller.ExchangeCSVToXML(filename, idList[i], false))
                            {
                                dr["ApproveCode"] = "Conversion failure";//文件转换失败时的提示
                                dt.Rows.Add(dr);
                                continue;
                            }
                        }
                        */
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member" + mode.ImportMemberNumber);
                        string approveCode=Edge.Web.Controllers.CouponController.ApproveCouponForApproveCode(mode, out isSuccess);
                        dr["ApproveCode"] = approveCode;
                        if (isSuccess)
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveCode + " " + approveCode);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                            mode.ApproveBy = Tools.DALTool.GetCurrentUser().UserID; //UserId approved by. 
                            
                        }
                        else
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveError);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                        }
                        dt.Rows.Add(dr);
                    }

                    //foreach (string id in idList)
                    //{
                    //    Edge.SVA.Model.Ord_ImportMember_H mode = new Edge.SVA.BLL.Ord_ImportMember_H().GetModel(id);

                    //    DataRow dr = dt.NewRow();
                    //    dr["TxnNo"] = id;
                    //    dr["ApproveCode"] = Edge.Web.Controllers.CouponController.ApproveCouponForApproveCode(mode, out isSuccess);
                    //    if (isSuccess)
                    //    {
                    //        Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveCode);

                    //        dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                    //    }
                    //    else
                    //    {
                    //        Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveError);

                    //        dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                    //    }
                    //    dt.Rows.Add(dr);
                    //}
                    this.Grid1.DataSource = dt;
                    this.Grid1.DataBind();



               // }
                //catch (Exception ex)
                //{
                //    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                //    //Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                //    Alert.ShowInTop(ex.Message, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                //}
            }
        }



    }
}