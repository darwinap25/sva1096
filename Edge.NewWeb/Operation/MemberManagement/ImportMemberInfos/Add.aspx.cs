﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;
using Common;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.Ord_ImportMember_H, Edge.SVA.Model.Ord_ImportMember_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ImportMemberCotroller controller;

        List<string> valColumns = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //初始化默认值
                InitData();
                SVASessionInfo.ImportMemberCotroller = null;
            }
            controller = SVASessionInfo.ImportMemberCotroller;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            if (!ValidData()) return;

            controller.ViewModel.MainTable = this.GetAddObject();

            if (controller.ViewModel.MainTable != null)
            {
                //校验文件类型是否正确
                if (!ValidateFile(this.Description.FileName))
                {
                    return;
                }

                //保存附件到数据库

                controller.ViewModel.MainTable.Description = Description.SaveAttachFileToServer("ImportMemberInfos");
                controller.ViewModel.MainTable.FileName = Path.GetFileName(controller.ViewModel.MainTable.Description);//this.Description.FileName;
                controller.ViewModel.MainTable.ImportStatus = "P";
                controller.ViewModel.MainTable.CreatedBy = Tools.DALTool.GetCurrentUser().UserID;
            }

            try
            {
                //choosing type of application
                logger.WriteOperationLog(this.PageName, " Choosing type of application: ");

                if (ActionToIndicateAppType(controller.ViewModel.MainTable.Description) == 1 || ActionToIndicateAppType(controller.ViewModel.MainTable.Description) == 2 || ActionToIndicateAppType(controller.ViewModel.MainTable.Description) == 3)
                {
                    logger.WriteOperationLog(this.PageName, " Validating columns for app1-3 ");

                    string columnValidationResult = ValidateColumnsOnetoThree(controller.ViewModel.MainTable.Description);

                    if (!string.IsNullOrEmpty(columnValidationResult))
                    {
                        //delete file from server if validation of columns failed
                        DeleteFile(controller.ViewModel.MainTable.Description);
                        ShowWarning(columnValidationResult);

                    }
                    else
                    {
                        ExecResult er = controller.Submit();
                        if (er.Success)
                        {
                            Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Success Code:" + controller.ViewModel.MainTable.ImportMemberNumber);
                            CloseAndRefresh();
                        }
                        else
                        {
                            Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ImportMemberNumber.ToString());
                            ShowAddFailed();
                        }
                    }
                }
                else
                {
                    logger.WriteOperationLog(this.PageName, " Validating columns for app4 ");
                    string columnValidationRes = ValidateColumnsFour(controller.ViewModel.MainTable.Description);

                    if (!string.IsNullOrEmpty(columnValidationRes))
                    {
                        //delete file from server if validation of columns failed
                        DeleteFile(controller.ViewModel.MainTable.Description);
                        ShowWarning(columnValidationRes);

                    }
                    else
                    {
                        ExecResult er = controller.Submit();
                        if (er.Success)
                        {
                            Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Success Code:" + controller.ViewModel.MainTable.ImportMemberNumber);
                            CloseAndRefresh();
                        }
                        else
                        {
                            Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ImportMemberNumber.ToString());
                            ShowAddFailed();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " Choosing type of application: " + ex.ToString());
            }

        }



        private void InitData()
        {
            this.ImportMemberNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.MemberImportInfo);
            this.CreatedBusDate.Text = DALTool.GetBusinessDate(); 
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
            this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;
        }

        private bool ValidData()
        {
            if (string.IsNullOrEmpty(this.Description.FileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return false;
            }
            return true;
        }



        //Added by Roche #1096 for column validations

        public int ActionToIndicateAppType(string filename)
        {
            try
            {

                CSVFileHelper cf = new CSVFileHelper();


                string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

                string testpath = Server.MapPath(filename);

                DataTable dt = cf.OpenCSV(testUpdloadFile + filename);
                //DataTable dt = cf.OpenCSV(Server.MapPath(filename));

                DataRow dr = dt.Rows[0];


                if (dr["RRC_Type of Application"].ToString() == "1")
                {
                    return 1;
                }
                else if (dr["RRC_Type of Application"].ToString() == "2")
                {
                    return 2;
                }
                else if (dr["RRC_Type of Application"].ToString() == "3")
                {
                    return 3;
                }
                else if (dr["RRC_Type of Application"].ToString() == "4")
                {
                    return 4;
                }
                return 0;

            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " Choosing type of application Exception: "+ ex.ToString());
                return 0;
                actionToVerifyRRCType(ex.Message);
            }
        }


        public void actionToVerifyRRCType(string exceptionmessage)
        {
            string columnValidationResult = ValidateColumnsOnetoThree(controller.ViewModel.MainTable.Description);

            //delete file from server if validation of columns failed
            DeleteFile(controller.ViewModel.MainTable.Description);
            ShowWarning(columnValidationResult);

        }


        private string ValidateColumnsOnetoThree(string filename)
        {
            logger.WriteOperationLog(this.PageName, " Start validation for App3 ");
            string fnReturnValue = "";

            List<string> list = new List<string>(); //

            list.Add("RRC_RetailerID");
            list.Add("RRC_Cardnumber");
            list.Add("RRC_Status");
            list.Add("RRC_Last Name");
            list.Add("RRC First Name");
            list.Add("RRC_Middle Initial");
            list.Add("RRC_Gender");
            list.Add("RRC_Birthdate");
            list.Add("RRC_Marital Status");
            list.Add("RRC_House No.");
            list.Add("RRC_Street Name");
            list.Add("RRC_Brgy/Subd");
            list.Add("RRC_City/Town");
            list.Add("RRC_ZipCode");
            list.Add("RRC_HomePhone");
            list.Add("RRC_WorkPhone");
            list.Add("RRC_Mobile No.");
            list.Add("RRC_Emailadd");
            list.Add("RRC_Type of Application");
            list.Add("RRC_Old Card No.");
            list.Add("RRC_Business Unit");
            list.Add("RRC_Store Code");
            list.Add("RRC_Sales Invoice");
            list.Add("RRC_Application Date");
            list.Add("RRC_Card Expiry Date");
            list.Add("RRC_Batch");
            list.Add("RRC_Loyalty_Card");
            list.Add("RRC_Customer Tier");
            list.Add("RRC_Business Owner");
            list.Add("RRC_Email Subscription");
            list.Add("RRC_SMS Subscription");

            CSVFileHelper cf = new CSVFileHelper();

            string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

            try
            {
                DataTable dtest = cf.OpenCSV(testUpdloadFile + filename);
                //DataTable dtest = cf.OpenCSV(Server.MapPath(filename));
            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " OpenCSV Exception :  " + ex.ToString());
            }

            DataTable dt = cf.OpenCSV(testUpdloadFile + filename);
            //DataTable dt = cf.OpenCSV(Server.MapPath(filename));

            try
            {
                fnReturnValue = CheckColumnsIfExists(dt, list);
            }
            catch (Exception ex)
            {
                fnReturnValue = ex.Message;

                //delete file from server if exception occurs
                DeleteFile(Server.MapPath(filename));
            }

            return fnReturnValue;

        }



        private string ValidateColumnsFour(string filename)
        {
            logger.WriteOperationLog(this.PageName, " Start validation for App4 ");
            string fnReturnValue = "";

            List<string> list = new List<string>(); //

            list.Add("RRC_RetailerID");
            list.Add("RRC_Cardnumber");
            list.Add("RRC_Status");
            list.Add("RRC_Last Name");
            list.Add("RRC First Name");
            list.Add("RRC_Middle Initial");
            list.Add("RRC_Gender");
            list.Add("RRC_Birthdate");
            list.Add("RRC_Marital Status");
            list.Add("RRC_House No.");
            list.Add("RRC_Street Name");
            list.Add("RRC_Brgy/Subd");
            list.Add("RRC_City/Town");
            list.Add("RRC_ZipCode");
            list.Add("RRC_HomePhone");
            list.Add("RRC_WorkPhone");
            list.Add("RRC_Mobile No.");
            list.Add("RRC_Emailadd");
            list.Add("RRC_Type of Application");
            list.Add("RRC_Old Card No.");
            list.Add("RRC_Business Unit");
            list.Add("RRC_Store Code");
            list.Add("RRC_Sales Invoice");
            list.Add("RRC_Application Date");
            list.Add("RRC_Card Expiry Date");
            list.Add("RRC_Batch");
            list.Add("RRC_Loyalty_Card");
            list.Add("RRC_Customer Tier");
            list.Add("RRC_Business Owner");
            list.Add("RRC_Email Subscription");
            list.Add("RRC_SMS Subscription");
            list.Add("RRC_Points Balance");

            CSVFileHelper cf = new CSVFileHelper();

            string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

            DataTable dt = cf.OpenCSV(testUpdloadFile + filename);
            //DataTable dt = cf.OpenCSV(Server.MapPath(filename));

            try
            {
                fnReturnValue = CheckColumnsIfExists(dt, list);
            }
            catch (Exception ex)
            {
                fnReturnValue = ex.Message;

                //delete file from server if exception occurs
                DeleteFile(Server.MapPath(filename));
            }

            return fnReturnValue;

        }


        private string CheckColumnsIfExists(DataTable tableNameToCheck, List<string> columnsNames)
        {
            string fnReturnValue = "";
            string columnsWithErrors = "";
            try
            {
                if (null != tableNameToCheck && tableNameToCheck.Columns != null)
                {
                    foreach (string columnName in columnsNames)
                    {
                        if (!tableNameToCheck.Columns.Contains(columnName))
                        {
                            //fnReturnValue = columnName + " does not exist in file! Verify that column names are in correct format.";
                            //break;
                            valColumns.Add(columnName);
                        }
                    }

                    for (int i = 0; i < valColumns.Count; i++)
                    {
                        columnsWithErrors += valColumns[i] + "; ";

                    }

                    if (!string.IsNullOrEmpty(columnsWithErrors))
                    {
                        fnReturnValue = columnsWithErrors + " does not exist or incorrect in the file! Verify that column names are in correct format.";
                    }
                    else
                    {
                        fnReturnValue = "";
                    }


                }
                else
                {
                    fnReturnValue = "Erroneous file uploaded!";
                }
            }
            catch (Exception ex)
            {
                fnReturnValue = ex.Message;
            }

            return fnReturnValue;
        }

        //END



        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }


        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                //if (!webset.MemberInfoFileType.ToLower().Split('|').Contains(filename))
                //{
                //    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                //    return false;
                //}

                if (!webset.MemberInfoFileTypeCSV.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                    return false;
                }

            }
            return true;
        }
    }
}