﻿<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.PointAdjustmentConfiguration.List" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
</head>
<body>
    <form id="Form1" runat="server" method="post">
    
    <ext:PageManager ID="PageManager1" runat="server" />


    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
        runat="server" EnableCheckBoxSelect="True" DataKeyNames="RuleID" AllowPaging="true"
        IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnNew" Text="New" Icon="Add" EnablePostBack="false" runat="server">
                    </ext:Button>
                    <ext:Button ID="btnDelete" Text="Delete" Icon="Delete" OnClick="lbtnDel_Click" runat="server">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Columns>
            <ext:TemplateField Width="60px" HeaderText="Rule ID">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("RuleID") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            <ext:TemplateField Width="60px" HeaderText="Acc ID">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("AccID") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>            
            <ext:TemplateField Width="60px" HeaderText="Card Prefix">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("CardPrefix") %>'></asp:Label>
                </ItemTemplate>
            </ext:TemplateField>
            
            <ext:WindowField ColumnID="EditWindowField" Width="60px" WindowID="Window2" Icon="PageEdit"
                Text="Edit" ToolTip="编辑" DataTextFormatString="{0}" DataIFrameUrlFields="RuleID"
                DataIFrameUrlFormatString="Modify.aspx?RuleID={0}" DataWindowTitleField="RuleID"
                DataWindowTitleFormatString="编辑" Title="编辑" />

            <%--<ext:WindowField ColumnID="DeleteWindowField" Width="60px" WindowID="Window2" Icon="PageDelete"
                Text="Delete" ToolTip="Delete" DataTextFormatString="{0}" DataIFrameUrlFields="RuleID"
                DataIFrameUrlFormatString="Delete.aspx?RuleID={0}" DataWindowTitleField="RuleID"
                DataWindowTitleFormatString="编辑" Title="编辑" />--%>


        </Columns>
    </ext:Grid>


    <ext:Window ID="Window1" Title="查看" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false" EnableResize="true"
        Target="Top" IsModal="True" Width="750px" Height="200px">
    </ext:Window>


    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="750px" Height="200px">
    </ext:Window>
    <%--OnClose="WindowEdit_Close"--%>

<%--

    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>--%>
    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="3"></uc1:CheckRight>



    </form>
</body>
</html>
