﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers;
using FineUI;
using Edge.Web.DAO;

namespace Edge.Web.Operation.MemberManagement.PointAdjustmentConfiguration
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnClose.OnClientClick = ActiveWindow.GetHideReference();
        }

        protected void btnSaveClose_Click(object sender, System.EventArgs e)
        {
            PointsAdjustmentConf conf = new PointsAdjustmentConf();

            
            if (conf.AddNewPointAdjustmentConfiguration(Convert.ToInt16(this.txtAccID.Text), this.txtCardPrefix.Text) > 0)
            {

                PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
            }
            

        }


    }
}