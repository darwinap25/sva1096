﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.DAO;
using FineUI;

namespace Edge.Web.Operation.MemberManagement.PointAdjustmentConfiguration
{
    public partial class Delete : System.Web.UI.Page
    {

        PointsAdjustmentConf dao = new PointsAdjustmentConf();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                string ruleid = Request.Params["RuleID"];
                if (string.IsNullOrEmpty(ruleid))
                {
                    //JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                    return;
                }

                foreach (string id in ruleid.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (string.IsNullOrEmpty(id)) continue;
                    string msg = "";

                    //actionToDelete(id);

                    Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                    //JscriptPrint(Resources.MessageTips.DeleteSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            
                }
            }
            catch(System.Exception ex)
            {
                Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                throw ex;
           }
        }





        //private void actionToDelete(string ruleid)
        //{
        //    if (dao.actionToRemoveRule(ruleid) == 1)
        //    {
        //        PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
        //    }
        //    else
        //    {
        //        Alert.ShowInTop("Failed to delete", "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
        //    }


        //}

    }
}