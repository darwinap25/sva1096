﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using System.Data;
using FineUI;
using Edge.Web.DAO;

namespace Edge.Web.Operation.MemberManagement.PointAdjustmentConfiguration
{
    public partial class Modify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                btnClose.OnClientClick = ActiveWindow.GetHideReference();

                int ruleid = Convert.ToInt16(Request.QueryString["RuleID"]);

                PointsAdjustmentConf conf = new PointsAdjustmentConf();

                DataTable dt = new DataTable();

                dt = conf.ViewToEditPointAdjustmentConf(ruleid);

                foreach (DataRow dr in dt.Rows)
                {
                    this.txtAccID.Text = dr[0].ToString();
                    this.txtCardPrefix.Text = dr[1].ToString();
                }

            }


        }

        protected void btnSaveClose_Click(object sender, System.EventArgs e)
        {

            PointsAdjustmentConf conf = new PointsAdjustmentConf();

            int aff = conf.actionToModifyPointAdjustmentConf(Convert.ToInt16(this.txtAccID.Text), this.txtCardPrefix.Text, Convert.ToInt16(Request.QueryString["RuleID"]));

            if(aff > 0)
            {
                PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
            }

        }

    }
}