﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.Web.DAO;
using FineUI;
using Edge.Web.Tools;

namespace Edge.Web.Operation.MemberManagement.PointAdjustmentConfiguration
{
    public partial class List : PageBase
    {
        PointsAdjustmentConf dao = new PointsAdjustmentConf();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                btnNew.OnClientClick = Window1.GetShowReference("Add.aspx", "Added");

                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDelete.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDelete.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                RptBind();
            }

        }


        public void RptBind()
        {
            PointsAdjustmentConf conf = new PointsAdjustmentConf();

            this.Grid1.DataSource = conf.getFromPointAdjustmentConf().DefaultView;
            this.Grid1.DataBind();
        }


        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            int[] rowIndexs = this.Grid1.SelectedRowIndexArray;
            for (int i = 0; i < rowIndexs.Length; i++)
            {
                if (dao.actionToRemoveRule(Grid1.Rows[rowIndexs[i]].DataKeys[0].ToString()) > 0)
                {
                    PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
                }
            }
            
            this.RefreshPage();
        }



    }
}