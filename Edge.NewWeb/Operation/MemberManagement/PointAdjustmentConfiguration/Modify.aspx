﻿<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.PointAdjustmentConfiguration.Modify" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
</head>
<body>
    <form id="Form1" runat="server">
    
    <ext:PageManager ID="PageManager1" AutoSizePanelID="SimpleForm1" runat="server" />


    <ext:SimpleForm ID="SimpleForm1" ShowBorder="false" ShowHeader="false" runat="server"
        BodyPadding="20px" EnableBackgroundColor="true" Title="SimpleForm" LabelAlign="Right"
        AutoScroll="true">
        <Toolbars>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="Close">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="SimpleForm1" Icon="SystemSaveClose"
                         OnClick="btnSaveClose_Click" runat="server" Text="Save and Close">
                    </ext:Button>
                    <%--OnClick="btnSaveClose_Click"--%>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false"
                Title="" EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:TextBox ID="txtAccID" runat="server" Label="Acc ID：" Required="true" ShowRedStar="true">
                    </ext:TextBox>
                    <ext:TextBox ID="txtCardPrefix" runat="server" Label="Card Prefix：" Required="true"
                        ShowRedStar="true">
                        </ext:TextBox>
                </Items>
            </ext:SimpleForm>
        </Items>
    </ext:SimpleForm>

    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="3"></uc1:CheckRight>



    </form>
</body>
</html>
