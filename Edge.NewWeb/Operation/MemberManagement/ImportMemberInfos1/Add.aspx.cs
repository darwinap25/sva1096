﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos1;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos1
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CreateMember_H, Edge.SVA.Model.Ord_CreateMember_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ImportMemberInfos1Controller controller = new ImportMemberInfos1Controller();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                InitData();
                SVASessionInfo.ImportMemberInfos1Controller = null;
            }
            controller = SVASessionInfo.ImportMemberInfos1Controller;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            if (ValidateData())
            {
                controller.ViewModel.MainTable = this.GetAddObject();
                controller.ViewModel.MainTable.CreatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.CreatedOn = DateTime.Now;
                controller.ViewModel.MainTable.ImportFile = this.ImportFile.SaveAttachFileToServer("ImportMemberInfos1");
                
                string path = Server.MapPath("~" + controller.ViewModel.MainTable.ImportFile);
                bool result = false;
                ExecResult er = controller.Add(path, out result);

                this.ExcuteReslut.Text = result.ToString();

                //if (er.Success && errorrecords == 0)
                //{
                //    this.ExcuteReslut.Text = "true";
                //}
                //else
                //{
                //    this.ExcuteReslut.Text = "false";
                //}
                SVASessionInfo.MessageHTML = controller.GetHtml(DateTime.Now).ToString();
                FineUI.PageContext.RegisterStartupScript(Window1.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));
            }
        }

        public void InitData()
        {
            this.CreateMemberNumber.Text = DALTool.GetREFNOCode("MEI");
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);

            Tools.ControlTool.BindCardType(this.CardTypeID);
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(this.CardGradeID, Convert.ToInt32(this.CardTypeID.SelectedValue));
            }
        }

        protected void BindCardFlag_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.BindCardFlag.SelectedValue == "1")
            {
                this.CardTypeID.Enabled = true;
                this.CardGradeID.Enabled = true;
                Tools.ControlTool.BindCardType(this.CardTypeID);
            }
            else
            {
                this.CardTypeID.Items.Clear();
                this.CardGradeID.Items.Clear();
                this.CardTypeID.Enabled = false;
                this.CardGradeID.Enabled = false;
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        protected bool ValidateData()
        {
            if (this.BindCardFlag.SelectedValue == "1")
            {
                if (this.CardTypeID.SelectedValue == "-1")
                {
                    ShowWarning(Resources.MessageTips.YouMustSelectType);
                    return false;
                }
                if (this.CardGradeID.SelectedValue == "-1")
                {
                    ShowWarning(Resources.MessageTips.YouMustSelectGrade);
                    return false;
                }
            }
            if (string.IsNullOrEmpty(this.ImportFile.ShortFileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return false;
            }
            string filename = Path.GetExtension(this.ImportFile.ShortFileName).TrimStart('.');
            if (filename.ToLower() != "xls" && filename.ToLower() != "csv")
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90446"));
                return false;
            }
            return true;
        }
    }
}