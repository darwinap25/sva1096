﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos1;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos1
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CreateMember_H, Edge.SVA.Model.Ord_CreateMember_H>
    {
        public string id = null;
        ImportMemberInfos1Controller controller = new ImportMemberInfos1Controller();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Grid1.PageSize = webset.ContentPageNum;
            this.id = Request.Params["id"];

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Tools.ControlTool.BindCardType(this.CardTypeID);
                Tools.ControlTool.BindCardGrade(this.CardGradeID);

                controller = SVASessionInfo.ImportMemberInfos1Controller;
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.CreateMemberNumber);
                if (controller.ViewModel.MainTable != null)
                {
                    CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());
                    this.lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                    
                    ApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.ApproveBy.GetValueOrDefault());
                    ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.ApproveOn.GetValueOrDefault());
                    this.lblApproveBy.Text = this.ApproveBy.Text;

                    this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);
                    this.lblApproveStatus.Text = this.ApproveStatus.Text;

                    if (Model.ApproveStatus != "A")
                    {
                        this.ApproveOn.Text = null;
                        this.ApprovalCode.Text = null;
                    }

                    if (this.BindCardFlag.SelectedValue == "1")
                    {
                        this.CardTypeID.SelectedValue = controller.ViewModel.MainTable.CardTypeID.ToString();
                        this.CardGradeID.SelectedValue = controller.ViewModel.MainTable.CardGradeID.ToString();
                    }
                    else
                    {
                        this.CardTypeID.Items.Clear();
                        this.CardGradeID.Items.Clear();
                    }

                    this.BindCardFlagView.Text = this.BindCardFlag.SelectedItem == null ? "" : this.BindCardFlag.SelectedItem.Text;
                    this.CardTypeIDView.Text = this.CardTypeID.SelectedText;
                    this.CardGradeIDView.Text = this.CardGradeID.SelectedText;

                    ViewState["ApproveStatus"] = Model.ApproveStatus;
                }

                RptBind();

            }
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        private void RptBind()
        {
            string strWhere = "CreateMemberNumber='" + Request["id"].ToString() + "'";

            string status = ViewState["ApproveStatus"].ToString();

            Edge.SVA.BLL.Ord_CardAdjust_D bll = new Edge.SVA.BLL.Ord_CardAdjust_D();

            DataSet ds = new DataSet();

            if (status.ToUpper().Trim() == "A")
            {
                ds = controller.GetApproveDetailList((this.Grid1.PageSize * (this.Grid1.PageIndex + 1)), strWhere, "CreateMemberNumber");
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                int count = 0;
                ds = controller.GetDetailList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, "CreateMemberNumber");
                this.Grid1.RecordCount = count;
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();

            }

        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            RptBind();
        }
    }
}