﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos1;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.ImportMemberInfos1
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CreateMember_H, Edge.SVA.Model.Ord_CreateMember_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ImportMemberInfos1Controller controller = new ImportMemberInfos1Controller();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                Tools.ControlTool.BindCardType(this.CardTypeID);

                SVASessionInfo.ImportMemberInfos1Controller = null;
            }
            controller = SVASessionInfo.ImportMemberInfos1Controller;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                controller.LoadViewModel(Model.CreateMemberNumber);

                if (controller.ViewModel.MainTable != null)
                {
                    lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                    CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());

                    lblApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(controller.ViewModel.MainTable.ApproveBy.GetValueOrDefault());
                    ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.ApproveOn.GetValueOrDefault());

                    this.ImportFile.Text = controller.ViewModel.MainTable.ImportFile;

                    this.ApproveStatus.Text = Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);
                    if (controller.ViewModel.MainTable.ApproveStatus != "A")
                    {
                        this.ApproveOn.Text = null;
                        this.ApprovalCode.Text = null;
                    }

                    if (this.BindCardFlag.SelectedValue == "1")
                    {
                        this.CardTypeID.Enabled = true;
                        this.CardGradeID.Enabled = true;
                        Tools.ControlTool.BindCardType(this.CardTypeID);
                        this.CardTypeID.SelectedValue = controller.ViewModel.MainTable.CardTypeID.ToString();
                        Tools.ControlTool.BindCardGrade(this.CardGradeID, ConvertTool.ToInt(controller.ViewModel.MainTable.CardTypeID.ToString()));
                        this.CardGradeID.SelectedValue = controller.ViewModel.MainTable.CardGradeID.ToString();
                    }
                    else
                    {
                        this.CardTypeID.Items.Clear();
                        this.CardGradeID.Items.Clear();
                        this.CardTypeID.Enabled = false;
                        this.CardGradeID.Enabled = false;
                    }
                }
                RptBind();
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");

            if (ValidateData())
            {
                controller.ViewModel.MainTable = this.GetUpdateObject();

                ExecResult er = controller.Update();

                if (er.Success)
                {
                    CloseAndPostBack();
                }
                else
                {
                    ShowUpdateFailed();
                }
            }
        }

        protected void RptBind()
        {
            ImportMemberInfos1Controller c = new ImportMemberInfos1Controller();
            int count = 0;
            string strwhere = "CreateMemberNumber = '" + Request["id"].ToString() + "'";
            DataSet ds = c.GetDetailList(strwhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, "CreateMemberNumber");
            this.Grid1.RecordCount = count;
            if (ds != null)
            {
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.Reset();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            RptBind();
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.CardTypeID.SelectedValue != "-1")
            {
                Tools.ControlTool.BindCardGrade(this.CardGradeID, Convert.ToInt32(this.CardTypeID.SelectedValue));
            }
        }

        protected void BindCardFlag_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.BindCardFlag.SelectedValue == "1")
            {
                this.CardTypeID.Enabled = true;
                this.CardGradeID.Enabled = true;
                Tools.ControlTool.BindCardType(this.CardTypeID);
            }
            else
            {
                this.CardTypeID.Items.Clear();
                this.CardGradeID.Items.Clear();
                this.CardTypeID.Enabled = false;
                this.CardGradeID.Enabled = false;
            }
        }

        protected bool ValidateData()
        {
            if (this.BindCardFlag.SelectedValue == "1")
            {
                if (this.CardTypeID.SelectedValue == "-1")
                {
                    ShowWarning(Resources.MessageTips.YouMustSelectType);
                    return false;
                }
                if (this.CardGradeID.SelectedValue == "-1")
                {
                    ShowWarning(Resources.MessageTips.YouMustSelectGrade);
                    return false;
                }
            }
            return true;
        }
    }
}