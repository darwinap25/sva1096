﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Messages.Manager;
using System.Data;
using System.Text;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.MemberManagement.ImportBIFile;

namespace Edge.Web.Operation.MemberManagement.ImportBIFile
{
    public partial class List : PageBase
    {
        private const string fields = "CouponDispenseNumber,ApproveStatus,ApprovalCode,CreatedBusDate,ApproveBusDate,CreatedOn,CreatedBy,ApproveOn,ApproveBy";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind("", "CouponDispenseNumber");
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);

                //ControlTool.bind
            }
        }

        #region  Approve Void

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewApproveTxns(Grid1, Window2);
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), HiddenWindowForm.GetShowReference("Void.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewVoidTxns(Grid1, HiddenWindowForm);
        }

        #endregion

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string code = this.Code.Text.Trim();
                    string status = this.Status.SelectedValue.Trim();
                    string CStatrtDate = this.CreateStartDate.Text;
                    string CEndDate = this.CreateEndDate.Text;
                    string AStatrtDate = this.ApproveStartDate.Text;
                    string AEndDate = this.ApproveEndDate.Text;
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CouponDispenseNumber like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveStatus";
                        sb.Append(descLan);
                        sb.Append(" ='");
                        sb.Append(status);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(CStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(CStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(CEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(CEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(AStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(AStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(AEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(AEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                //Edge.SVA.BLL.Ord_ImportCouponDispense_H bll = new Edge.SVA.BLL.Ord_ImportCouponDispense_H();

                ////获得总条数
                //this.Grid1.RecordCount = bll.GetCount(strWhere);

                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

                //Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
                //Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
                //Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

                ImportBIFileController controller = new ImportBIFileController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }

                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog("ImportBIFile", "Load Faild", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            ImportBIFileController controller = new ImportBIFileController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("CouponDispenseNumber<>''", "CouponDispenseNumber");
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("CouponDispenseNumber<>''", "CouponDispenseNumber");
        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            DataRowView drv = e.DataItem as DataRowView;
            if (drv == null) return;

            string approveStatus = drv["ApproveStatus"].ToString().Trim();
            approveStatus = string.IsNullOrEmpty(approveStatus) ? approveStatus : approveStatus.Substring(0, 1).ToUpper().Trim();

            string couponNumber = drv["CouponDispenseNumber"].ToString().Trim();

            DateTime? createTime = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<DateTime>(drv["CreatedOn"].ToString().Trim());
            string strCreateDate = createTime.ToString();

            FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;
            FineUI.WindowField viewWF = Grid1.FindColumn("ViewWindowField") as FineUI.WindowField;

            switch (approveStatus)
            {
                case "A":
                    editWF.Enabled = false;
                    //viewWF.DataIFrameUrlFormatString = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=A", couponNumber, strCreateDate);
                    break;
                case "P":
                    editWF.Enabled = true;
                    //editWF.DataIFrameUrlFormatString = string.Format("Modify.aspx?id={0}&CreatedOn={1}&Status=P", couponNumber, strCreateDate);
                    drv["ApprovalCode"] = "";
                    //viewWF.DataIFrameUrlFields = couponNumber + "$" + strCreateDate;
                    break;
                case "V":
                    editWF.Enabled = false;
                    drv["ApprovalCode"] = "";
                    //viewWF.DataIFrameUrlFormatString = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=V", couponNumber, strCreateDate);
                    break;
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("CouponDispenseNumber<>''", "CouponDispenseNumber");
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }
    }
}