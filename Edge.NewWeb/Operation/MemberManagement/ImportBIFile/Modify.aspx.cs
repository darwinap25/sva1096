﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.ImportBIFile
{
    public partial class Modify : Tools.BasePage<SVA.BLL.Ord_ImportCouponDispense_H, SVA.Model.Ord_ImportCouponDispense_H>
    {
        private const string fields = "CouponDispenseNumber,CampaignCode,CouponTypeCode,MemberRegisterMobile,ExportDatetime,CardNumber";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind(string.Format("CouponDispenseNumber='{0}'", Request.Params["id"]), "CouponDispenseNumber");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveByName.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                }
                else
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("CouponDispenseNumber<>''", "CouponDispenseNumber");
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_ImportCouponDispense_H item = this.GetUpdateObject();

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_ImportCouponDispense_H>(item))
            {
                CloseAndPostBack();
            }
            else
            {
                ShowUpdateFailed();
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.Ord_ImportCouponDispense_D bll = new Edge.SVA.BLL.Ord_ImportCouponDispense_D();

            //获得总条数
            this.Grid1.RecordCount = bll.GetCount(strWhere);

            DataSet ds = new DataSet();
            ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

            Tools.DataTool.AddCouponTypeNameByCode(ds, "CouponTypeIDName", "CouponTypeCode");
            Tools.DataTool.AddColumn(ds, "CreatedDate", Request.Params["date"]);

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();
        }

        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                //this.rptPager.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }
    }
}
