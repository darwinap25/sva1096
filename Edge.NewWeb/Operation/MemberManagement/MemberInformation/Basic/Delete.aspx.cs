﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUI;
using Edge.Web.Controllers.Operation.MemberManagement;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Delete : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MemberDeleteController controller = new MemberDeleteController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    if (string.IsNullOrEmpty(ids))
                    {
                        FineUI.Alert.ShowInTop(Resources.MessageTips.NotSelected, "", FineUI.MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());

                        return;
                    }
                    logger.WriteOperationLog(this.PageName, "Delete " + ids);
                    //foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    //{
                    //    if (string.IsNullOrEmpty(id)) continue;

                    //    string msg = "";
                    //    if (!Tools.DALTool.isCanDeleteIndustry(Tools.ConvertTool.ToInt(id.Trim()), ref msg))
                    //    {

                    //        FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteIsUsed, "", FineUI.MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                    //        return;
                    //    }
                    //    controller.DeleteData(controller.ViewModel.MainObject.MemberID);
                    //}
                    List<int> MemberIDList = new List<int>();
                    foreach (string id in ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (string.IsNullOrEmpty(id)) continue;

                        string msg = "";
                        if (!Tools.DALTool.isCanDeleteIndustry(Tools.ConvertTool.ToInt(id.Trim()), ref msg))
                        {
                            FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteIsUsed, "", FineUI.MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());
                        }
                        MemberIDList.Add(Tools.ConvertTool.ToInt(id));
                    }
                    controller.DeleteData(MemberIDList);
                    FineUI.Alert.ShowInTop(Resources.MessageTips.DeleteSuccess, "", FineUI.MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                }
                catch (System.Exception ex)
                {
                    logger.WriteErrorLog(this.PageName, "Delete", ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
    }
}
