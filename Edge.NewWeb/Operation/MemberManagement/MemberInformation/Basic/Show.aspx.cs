﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.Web.Controllers.Operation.MemberManagement;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Member, Edge.SVA.Model.Member>
    {
        MemberModifyController controller = new MemberModifyController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                DataSet ds = new Edge.SVA.BLL.Education().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(EducationID, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3", "EducationCode");
                EducationID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });

                ds = new Edge.SVA.BLL.Profession().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(ProfessionID, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3", "ProfessionCode");
                ProfessionID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                
                //ds = new Edge.SVA.BLL.Nation().GetAllList();
                //Edge.Web.Tools.ControlTool.BindDataSet(NationID, ds, "NationID", "NationName1", "NationName2", "NationName3", "NationCode");
                //NationID.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });

                RegisterCloseEvent(btnClose);
                //Add by Nathan 20140707++
                Tools.ControlTool.BindLanguageMap(this.MemberDefLanguage);
                //Add by Nathan 20140707--
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(this.Model.MemberID);
                this.UpdatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainObject.UpdatedBy.GetValueOrDefault());
                this.CreatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainObject.CreatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainObject.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(controller.ViewModel.MainObject.UpdatedOn);

                this.MemberSexView.Text = this.MemberSex.SelectedItem == null ? "" : this.MemberSex.SelectedItem.Text;
                this.MemberSex.Hidden = true;

                this.MemberMaritalView.Text = this.MemberMarital.SelectedItem == null ? "" : this.MemberMarital.SelectedItem.Text;
                this.MemberMarital.Hidden = true;

                this.MemberIdentityTypeView.Text = this.MemberIdentityType.SelectedItem == null ? "" : this.MemberIdentityType.SelectedItem.Text;
                this.MemberIdentityType.Hidden = true;

                this.EducationIDView.Text = this.EducationID.SelectedItem == null ? "" : this.EducationID.SelectedItem.Text;
                this.EducationID.Hidden = true;

                this.ProfessionIDView.Text = this.ProfessionID.SelectedItem == null ? "" : this.ProfessionID.SelectedItem.Text;
                this.ProfessionID.Hidden = true;

                //this.NationIDView.Text = this.NationID.SelectedItem == null ? "" : this.NationID.SelectedItem.Text;
                //this.NationID.Hidden = true;

                
                this.lblFaceBook.Text = controller.ViewModel.FacebookNumber;
                this.lblQQ.Text = controller.ViewModel.QQNumber;
                this.lblMSN.Text = controller.ViewModel.MSNNumber;
                this.lblSina.Text = controller.ViewModel.SinaNumber;

                //地址
                if (controller.ViewModel.SubTable != null)
                {
                    Edge.SVA.Model.Country country = new Edge.SVA.BLL.Country().GetModel(controller.ViewModel.SubTable.AddressCountry);
                    this.AddressCountry.Text = country == null ? "" : DALTool.GetStringByCulture(country.CountryName1, country.CountryName2, country.CountryName3);
                    Edge.SVA.Model.Province province = new Edge.SVA.BLL.Province().GetModel(controller.ViewModel.SubTable.AddressProvince);
                    this.AddressProvince.Text = province == null ? "" : DALTool.GetStringByCulture(province.ProvinceName1, province.ProvinceName2, province.ProvinceName3);
                    Edge.SVA.Model.City city = new Edge.SVA.BLL.City().GetModel(controller.ViewModel.SubTable.AddressCity);
                    this.AddressCity.Text = city == null ? "" : DALTool.GetStringByCulture(city.CityName1, city.CityName2, city.CityName3);
                    Edge.SVA.Model.District district = new Edge.SVA.BLL.District().GetModel(controller.ViewModel.SubTable.AddressDistrict);
                    this.AddressDistrict.Text = district == null ? "" : DALTool.GetStringByCulture(district.DistrictName1, district.DistrictName2, district.DistrictName3);
                    this.AddressDetail.Text = controller.ViewModel.SubTable.AddressDetail;
                    this.AddressFullDetail.Text = controller.ViewModel.SubTable.AddressFullDetail;


                    //Add by Nathan 20140707 ++
                    this.ReceiveEmailPromotionMessage.SelectedValue = controller.ViewModel.ReceiveEmailPromotionMessage.ToString();
                    this.ReceiveSMSPromotionMessage.SelectedValue = controller.ViewModel.ReceiveSMSPromotionMessage.ToString();
                    //Add by Nathan 20140707 --
                }
                
                if (controller.ViewModel.MainObject != null)
                {
                    this.btnPreview.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainObject.MemberPictureFile) ? true : false;//没有照片时不显示查看按钮(Len)

                    this.uploadFilePath.Text = controller.ViewModel.MainObject.MemberPictureFile;

                    this.btnPreview.OnClientClick = WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片");
                }

                // Add by Nathan 20140707 ++
                this.MemberDefLanguageView.Text = this.MemberDefLanguage.SelectedItem == null ? "" : this.MemberDefLanguage.SelectedItem.Text;
                this.MemberDefLanguage.Visible = false;

                this.ReceiveAllAdvertisingView.Text = this.ReceiveAllAdvertising.SelectedItem == null ? "" : this.ReceiveAllAdvertising.SelectedItem.Text;
                this.ReceiveAllAdvertising.Visible = false;

                this.ReceiveSMSPromotionMessageView.Text = this.ReceiveSMSPromotionMessage.SelectedItem == null ? "" : this.ReceiveSMSPromotionMessage.SelectedItem.Text;
                this.ReceiveSMSPromotionMessage.Visible = false;

                this.ReceiveEmailPromotionMessageView.Text = this.ReceiveEmailPromotionMessage.SelectedItem == null ? "" : this.ReceiveEmailPromotionMessage.SelectedItem.Text;
                this.ReceiveEmailPromotionMessage.Visible = false;

                this.AcceptPhoneAdvertisingView.Text = this.AcceptPhoneAdvertising.SelectedItem == null ? "" : this.AcceptPhoneAdvertising.SelectedItem.Text;
                this.AcceptPhoneAdvertising.Visible = false;
                // Add by Nathan 20140707 ++

                //卡列表
                DataSet ds = controller.GetCardList(controller.ViewModel.MainObject.MemberID);
                ViewState["table"] = ds;
                BindCardList(ds.Tables[0]);
            }
        }

        public void BindCardList(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            DataSet ds = (DataSet)ViewState["table"];
            BindCardList(ds.Tables[0]);
        }
    }
}
