﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;
using FineUI;
using Edge.SVA.Model;
using Edge.Web.Controllers.Operation.MemberManagement;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Member, Edge.SVA.Model.Member>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MemberAddController controller = new MemberAddController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.SVA.BLL.Domain.MemberManagement.MemberBindSource bll = new SVA.BLL.Domain.MemberManagement.MemberBindSource();

                DataSet ds = new Edge.SVA.BLL.Education().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(EducationID, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3", "EducationCode");
                ds = new Edge.SVA.BLL.Profession().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(ProfessionID, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3", "ProfessionCode");
                //ds = new Edge.SVA.BLL.Nation().GetAllList();
                //Edge.Web.Tools.ControlTool.BindDataSet(NationID, ds, "NationID", "NationName1", "NationName2", "NationName3", "NationCode");
                RegisterCloseEvent(btnClose);

                //地址
                ControlTool.BindCountry(AddressCountry);
                this.AddressProvince.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                this.AddressCity.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                this.AddressDistrict.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
                //Add by Nathan 20140707++
                Tools.ControlTool.BindLanguageMap(this.MemberDefLanguage);
                //Add by Nathan 20140707--
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            controller.ViewModel.MainObject = this.GetAddObject();
            if (controller.ViewModel.MainObject != null)
            {                
                //校验图片类型是否正确
                if (!ValidateImg(this.MemberPictureFile.FileName))
                {
                    return;
                }
                controller.ViewModel.MainObject.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainObject.CreatedOn = System.DateTime.Now;
                controller.ViewModel.MainObject.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainObject.UpdatedOn = System.DateTime.Now;
                controller.ViewModel.MainObject.HomeAddress = this.AddressFullDetail.Text;

                controller.ViewModel.MainObject.MemberPictureFile = this.MemberPictureFile.SaveToServer("Basic");
            }

            //Edge.SVA.Model.Member item = this.GetAddObject();
            //if (item != null)
            //{
            //    item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.CreatedOn = System.DateTime.Now;
            //    item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            //    item.UpdatedOn = System.DateTime.Now;
            //    item.HomeAddress = this.AddressFullDetail.Text;

            //    item.MemberPictureFile = this.MemberPictureFile.SaveToServer("Basic");
            //}

            if (!string.IsNullOrEmpty(this.txtFaceBook.Text))
            {
                controller.ViewModel.FacebookNumber = this.txtFaceBook.Text;
                controller.ViewModel.MemberMessageTypeIDList.Add(4);
            }
            if (!string.IsNullOrEmpty(this.txtQQ.Text))
            {
                controller.ViewModel.QQNumber = this.txtQQ.Text;
                controller.ViewModel.MemberMessageTypeIDList.Add(5);
            }
            if (!string.IsNullOrEmpty(this.txtMSN.Text))
            {
                controller.ViewModel.MSNNumber = this.txtMSN.Text;
                controller.ViewModel.MemberMessageTypeIDList.Add(7);
            }
            if (!string.IsNullOrEmpty(this.txtSina.Text))
            {
                controller.ViewModel.SinaNumber = this.txtSina.Text;
                controller.ViewModel.MemberMessageTypeIDList.Add(8);
            }
            if (!string.IsNullOrEmpty(this.MemberMobilePhone.Text))
            {
                controller.ViewModel.MemberMessageTypeIDList.Add(2);
            }
            // add by Alex 20141016 ++
            if (!string.IsNullOrEmpty(this.MemberMobilePhone.Text))
            {
                controller.ViewModel.MemberMessageTypeIDList.Add(1);
            }
            // add by Alex 20141016 --

            //Add by Nathan 20140707 ++
            controller.ViewModel.ReceiveEmailPromotionMessage = Convert.ToInt32(this.ReceiveEmailPromotionMessage.SelectedValue);
            controller.ViewModel.ReceiveSMSPromotionMessage = Convert.ToInt32(this.ReceiveSMSPromotionMessage.SelectedValue);
            controller.ViewModel.MemberMessageTypeIDList.Add(2);
            controller.ViewModel.MemberMessageTypeIDList.Add(1);
            //Add by Nathan 20140707 --



            //地址保存字段
            controller.ViewModel.SubTable.AddressCountry = this.AddressCountry.SelectedValue == "-1" ? "" : this.AddressCountry.SelectedValue;
            controller.ViewModel.SubTable.AddressProvince = this.AddressProvince.SelectedValue == "-1" ? "" : this.AddressProvince.SelectedValue;
            controller.ViewModel.SubTable.AddressCity = this.AddressCity.SelectedValue == "-1" ? "" : this.AddressCity.SelectedValue;
            controller.ViewModel.SubTable.AddressDistrict = this.AddressDistrict.SelectedValue == "-1" ? "" : this.AddressDistrict.SelectedValue;
            controller.ViewModel.SubTable.AddressDetail =  this.AddressDetail.Text;
            controller.ViewModel.SubTable.AddressFullDetail =  this.AddressFullDetail.Text;

            ExecResult er = controller.Save();
            if (er.Success)
            {
                CloseAndRefresh();
            }
            else
            {
                logger.WriteErrorLog("Member ", " Add error", er.Ex);
                ShowAddFailed();
            }
            //int id = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Member>(item);
            //if (id > 0)
            //{
            //    //增加MemberMessageAccount数据,MemberMessageTypeID有4种（4:msn,5:qq,7:facebook,8:sina）
            //    controller.ViewModel.MainObject.MemberID = id;
            //    ExecResult er = controller.Submit();
            //    if (er.Success)
            //    {
            //        // 2. 关闭本窗体，然后刷新父窗体
            //        CloseAndRefresh();
            //    }
            //    else
            //    {
            //        logger.WriteErrorLog("Member ", " Add error", er.Ex);
            //        ShowAddFailed();
            //    }
            //}
            //else
            //{
            //    ShowAddFailed();
            //}
        }

        protected void ViewPicture(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.MemberPictureFile.ShortFileName))
            {
                this.uploadFilePath.Text = this.MemberPictureFile.SaveToServer("Basic");
                FineUI.PageContext.RegisterStartupScript(WindowPic.GetShowReference("../../../../TempImage.aspx?url=" + this.uploadFilePath.Text, "图片"));
            }
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            DeleteFile(this.uploadFilePath.Text);
        }

        protected void AddressCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddressCountry.SelectedValue != "-1")
            {
                ControlTool.BindProvince(this.AddressProvince, this.AddressCountry.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindProvince(AddressProvince, "-1");
            }
            AddressProvince_SelectedIndexChanged(null, null);
            AddressCity_SelectedIndexChanged(null, null);
            AddressDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void AddressProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddressProvince.SelectedValue != "-1")
            {
                ControlTool.BindCity(this.AddressCity, this.AddressProvince.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindCity(AddressCity, "-1");
            }
            AddressCity_SelectedIndexChanged(null, null);
            AddressDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void AddressCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddressCity.SelectedValue != "-1")
            {
                ControlTool.BindDistrict(this.AddressDistrict, this.AddressCity.SelectedValue);
            }
            else
            {
                Tools.ControlTool.BindDistrict(AddressDistrict, "-1");
            }
            AddressDistrict_SelectedIndexChanged(null, null);
            AppendFullAddress();
        }
        protected void AddressDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void AddressFullDetail_OnTextChanged(object sender, EventArgs e)
        {
            AppendFullAddress();
        }
        protected void AppendFullAddress()
        {
            string country = this.AddressCountry.SelectedValue == "-1" ? "" : this.AddressCountry.SelectedText;
            string province = this.AddressProvince.SelectedValue == "-1" ? "" : this.AddressProvince.SelectedText;
            string city = this.AddressCity.SelectedValue == "-1" ? "" : this.AddressCity.SelectedText;
            string district = this.AddressDistrict.SelectedValue == "-1" ? "" : this.AddressDistrict.SelectedText;
            string detail = this.AddressDetail.Text;
            AddressFullDetail.Text = country + province + city + district + detail;
        }

        //校验图片文件是否为允许类型
        protected bool ValidateImg(string imgname)
        {
            if (!string.IsNullOrEmpty(imgname))
            {
                imgname = Path.GetExtension(imgname).TrimStart('.').ToLower();
                if (!webset.WebImageType.ToLower().Split('|').Contains(imgname))
                {
                    ShowWarning(Resources.MessageTips.ImgUpLoadFaild.Replace("{0}", webset.WebImageType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}
