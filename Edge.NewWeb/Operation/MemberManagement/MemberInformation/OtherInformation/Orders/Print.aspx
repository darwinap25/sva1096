﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders.Print" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Style/default.css" rel="stylesheet" type="text/css" />
</head>
<body style="padding: 10px;">
    <script language="javascript" type="text/javascript">
        function printdiv(printpage) {
            window.focus();
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            location.reload();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" />
    <div class="print_navigation">
        <span class="back"><a href="#"></a></span><b>打印预览</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="div_print">
        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
            <ItemTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtable">
                    <tr>
                        <th colspan="4" align="left">
                            订单信息
                        </th>
                    </tr>
                    <tr>
                        <td align="right" width="25%">
                            订单编号：
                        </td>
                        <td width="25%">
                            <%#Eval("TxnNo")%>
                        </td>
                        <td align="right" width="25%">
                            卡号码：
                        </td>
                        <td width="25%">
                            <%#Eval("CardNumber")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            订单类型：
                        </td>
                        <td>
                            <%#Eval("SalesTypeName")%>
                        </td>
                        <td align="right">
                            订单状态：
                        </td>
                        <td>
                            <%#Eval("StatusName")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            确认时间：
                        </td>
                        <td>
                            <%#Eval("CreatedOn")%>
                        </td>
                        <td align="right">
                            付款时间：
                        </td>
                        <td>
                            <%#Eval("PaymentDoneOn")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            发货时间：
                        </td>
                        <td>
                            <%#Eval("DeliverStartOn")%>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            送货信息
                        </th>
                    </tr>
                    <tr>
                        <td align="right" colspan="1">
                            送货地址：
                        </td>
                        <td>
                            <%#Eval("DeliveryAddress")%>
                        </td>
                        <td align="right">
                            联系人：
                        </td>
                        <td>
                            <%#Eval("Contact")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            联系电话：
                        </td>
                        <td>
                            <%#Eval("ContactPhone")%>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            卖家信息
                        </th>
                    </tr>
                    <tr>
                        <td align="right" colspan="1">
                            卖家：
                        </td>
                        <td>
                            <%#Eval("SalesName")%>
                        </td>
                        <td align="right">
                            地址：
                        </td>
                        <td>
                            <%#Eval("SalesAddress")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            联系电话：
                        </td>
                        <td>
                            <%#Eval("SalesContactPhone")%>
                        </td>
                        <td align="right">
                            联系人：
                        </td>
                        <td>
                            <%#Eval("SalesContact")%>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            物流信息
                        </th>
                    </tr>
                    <tr>
                        <td align="right" colspan="1">
                            物流公司：
                        </td>
                        <td>
                            <%#Eval("ProviderName")%>
                        </td>
                        <td align="right">
                            运单号码：
                        </td>
                        <td>
                            <%#Eval("DeliveryNumber")%>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            支付信息
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptTendList" runat="server" OnItemDataBound="rptTendList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtablelist">
                                        <tr>
                                            <th width="50%">
                                                支付方式
                                            </th>
                                            <th width="50%">
                                                金额
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblTenderCode" runat="server" Text='<%#Eval("TenderName")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblTenderAmount" runat="server" Text='<%#Eval("TenderAmount","{0:N2}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="center">   
                                            总计：
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" align="left">
                            货品明细
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Repeater ID="rptDetailList" runat="server" OnItemDataBound="rptDetailList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_msgtablelist">
                                        <tr>
                                            <th width="20%">
                                                货品编号
                                            </th>
                                            <th width="30%">
                                                货品描述
                                            </th>
                                            <th width="20%">
                                                尺寸
                                            </th>
                                            <th width="20%">
                                                颜色
                                            </th>
                                            <th width="10%">
                                                数量
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblProdCode" runat="server" Text='<%#Eval("ProdCode")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblProdDesc" runat="server" Text='<%#Eval("ProdName")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblProductSizeID" runat="server" Text='<%#Eval("ProductSizeName")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblColorID" runat="server" Text='<%#Eval("ColorName")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblQty" runat="server" Text='<%#Eval("Qty")%>'></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblNetAmount" runat="server" Text='<%#Eval("NetAmount","{0:N2}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td align="center" colspan="2">
                                            总计：
                                        </td>
                                        <td align="center" colspan="2">
                                           数量总计： <asp:Label ID="lblTotalProdQty" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" colspan="2">
                                           金额总计： <asp:Label ID="lblTotalProdAmount" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom: 10px;">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--endprint-->
    </div>
    <div align="center" style="width: 100%; text-align: center;">
        <table align="center">
            <tr align="center">
                <td>
                    <ext:Button ID="btnPrint" runat="server" Icon="Printer" Text="打印" OnClientClick="printdiv('div_print')"></ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnClose" runat="server" Icon="SystemClose" Text="关闭"></ext:Button>
                </td>
            </tr>
        </table>
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
