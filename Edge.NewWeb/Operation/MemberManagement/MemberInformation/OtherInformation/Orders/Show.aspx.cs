﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.Orders;
using System.Data;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Sales_H, Edge.SVA.Model.Sales_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        MemberOrderController controller = new MemberOrderController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RegisterCloseEvent(btnClose);

                BindLogistics(this.LogisticsProviderID);

                this.Window1.Title = "Print";
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadView(Model.TxnNo);

                if (controller.ViewModel.MainTable != null)
                {
                    this.DeliverStartOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.DeliverStartOn.GetValueOrDefault());
                    this.DeliveryAddress.Text = controller.ViewModel.MainTable.DeliveryAddress == null ? "" : controller.ViewModel.MainTable.DeliveryAddress.ToString();
                    this.Contact.Text = controller.ViewModel.MainTable.Contact == null ? "" : controller.ViewModel.MainTable.Contact.ToString();
                    this.ContactPhone.Text = controller.ViewModel.MainTable.ContactPhone == null ? "" : controller.ViewModel.MainTable.ContactPhone.ToString();
                    this.PaymentDoneOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.PaymentDoneOn.GetValueOrDefault());
                    this.CreatedOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());


                    this.StatusView.Text = DALTool.GetSaleStatusName(ConvertTool.ToInt(controller.ViewModel.MainTable.Status.ToString()));
                    this.SalesTypeView.Text = DALTool.GetSaleTypeName(ConvertTool.ToInt(controller.ViewModel.MainTable.SalesType.ToString()));
                }

                if (controller.ViewModel.HeadTable != null && controller.ViewModel.HeadTable.Rows.Count > 0)
                {
                    DataRow dr = controller.ViewModel.HeadTable.Rows[0];
                    this.SalesName.Text = dr["SalesName"].ToString();
                    this.SalesAddress.Text = dr["SalesAddress"].ToString();
                    this.SalesContactPhone.Text = dr["SalesContactPhone"].ToString();
                    this.SalesContact.Text = dr["SalesContact"].ToString();

                    this.LogisticsProviderID.SelectedValue = dr["LogisticsProviderID"].ToString();
                    this.LogisticsProviderIDView.Text = this.LogisticsProviderID.SelectedText;

                    this.PickupType.SelectedValue = dr["PickupType"].ToString();
                    this.PickupTypeView.Text = this.PickupType.SelectedItem == null ? "" : this.PickupType.SelectedItem.Text;
                }

                if (controller.ViewModel.MemberTable != null && controller.ViewModel.MemberTable.Rows.Count > 0)
                {
                    DataRow dr = controller.ViewModel.MemberTable.Rows[0];
                    this.CardNumber.Text = dr["CardNumber"].ToString();
                    this.MemberRegisterMobile.Text = dr["MemberRegisterMobile"].ToString();
                    this.ChineseName.Text = dr["ChineseName"].ToString();
                    this.EnglishName.Text = dr["EnglishName"].ToString();
                    this.MemberMobilePhone.Text = dr["MemberMobilePhone"].ToString();
                    this.MemberEmail.Text = dr["MemberEmail"].ToString();
                    this.MemberAddress.Text = dr["MemberAddress"].ToString();
                }
                BindTend();
                BindDetail();
            }
        }

        public void BindTend()
        {
            if (controller.ViewModel.TendTable != null && controller.ViewModel.TendTable.Rows.Count > 0)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid1.RecordCount = controller.ViewModel.TendTable.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(controller.ViewModel.TendTable, this.Grid1.PageIndex + 1, this.Grid1.PageSize);
                this.Grid1.DataSource = viewDT;
                this.Grid1.DataBind();

                SummaryTender(controller.ViewModel.TendTable);
            }
        }

        public void BindDetail()
        {
            if (controller.ViewModel.DetailTable != null && controller.ViewModel.DetailTable.Rows.Count > 0)
            {
                this.Grid2.PageSize = webset.ContentPageNum;
                this.Grid2.RecordCount = controller.ViewModel.TendTable.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(controller.ViewModel.DetailTable, this.Grid2.PageIndex + 1, this.Grid2.PageSize);
                this.Grid2.DataSource = viewDT;
                this.Grid2.DataBind();

                SummaryDeatail(controller.ViewModel.DetailTable);
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            BindTend();
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid2.PageIndex = e.NewPageIndex;
            BindDetail();
        }

        protected void btnTrack_Click(object sender, EventArgs e)
        {
            //ExecuteJS("window.open('http://www.baidu.com','DisplayWindow','toolbar=no,,menubar=no,location=no,scrollbars=no');");
        }

        public void BindLogistics(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.LogisticsProvider().GetList("1 = 1 order by LogisticsProviderCode");
            Edge.Web.Tools.ControlTool.BindDataSet(ddl, ds, "LogisticsProviderID", "ProviderName1", "ProviderName2", "ProviderName3", "LogisticsProviderCode");
        }

        private void SummaryTender(DataTable table)
        {
            if (table.Rows.Count > 0)
            {
                decimal total = Tools.ConvertTool.ConverType<decimal>(table.Compute(" sum(TenderAmount) ", "").ToString());
                this.lblTotal.Text = total.ToString("N2");
            }
        }

        private void SummaryDeatail(DataTable table)
        {
            if (table.Rows.Count > 0)
            {
                int qty = Tools.ConvertTool.ConverType<int>(table.Compute(" sum(Qty) ", "").ToString());
                this.lblQty.Text = qty.ToString();
                decimal totalamount = Tools.ConvertTool.ConverType<decimal>(table.Compute(" sum(NetAmount) ", "").ToString());
                this.lblTotalAmount.Text = totalamount.ToString("N2");
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("Print.aspx?id=" + this.TxnNo.Text));
        }
    }
}