﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.Orders;
using Edge.Web.Tools;
using Edge.Messages.Manager;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders
{
    public partial class List : PageBase
    {
        protected static string strWhere = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind(strWhere, "TxnNo");

                btnImport.OnClientClick = Window4.GetShowReference("~/PublicForms/ImportForm.aspx?Menu=MemberOrder", "Import");
            }
            string url = this.Request.Url.AbsolutePath.Substring(0, this.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
        }

        #region 数据列表绑定

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnSend.Enabled = true;
                    //this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnSend.Enabled = false;
                    //this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);
                    string code = this.TxnNo.Text.Trim();
                    int status = Convert.ToInt32(this.Status.SelectedValue);
                    string cardnumber = this.CardNumber.Text;
                    int saletype = Convert.ToInt32(this.SalesType.SelectedValue);
                    string CStatrtDate = this.CreateStartDate.Text;
                    string CEndDate = this.CreateEndDate.Text;
                    string PStartDate = this.PaymentStartDate.Text;
                    string PEndDate = this.PaymentEndDate.Text;
                    string DStartDate = this.DeliverStartOn.Text;
                    string DEndDate = this.DeliverEndOn.Text;

                    string RegisterMobile = this.MemberRegisterMobile.Text;
                    string MobileNumber = this.MemberMobilePhone.Text;
                    //string Country = this.CountryCode.SelectedValue;

                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" a.TxnNo like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (status != -1)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.Status";
                        sb.Append(descLan);
                        sb.Append(" =");
                        sb.Append(status);
                    }
                    if (!string.IsNullOrEmpty(cardnumber))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.CardNumber";
                        sb.Append(descLan);
                        sb.Append(" ='");
                        sb.Append(status);
                        sb.Append("'");
                    }
                    if (saletype != -1)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.SalesType";
                        sb.Append(descLan);
                        sb.Append(" =");
                        sb.Append(saletype);
                    }
                    if (!string.IsNullOrEmpty(CStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(CStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(CEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(CEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(PStartDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.PaymentDoneOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(PStartDate);
                        sb.Append("' as DateTime) ");
                    }
                    if (!string.IsNullOrEmpty(PEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.PaymentDoneOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(PEndDate);
                        sb.Append("' as DateTime) + 1 ");
                    }
                    if (!string.IsNullOrEmpty(DStartDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.DeliverStartOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(PStartDate);
                        sb.Append("' as DateTime) ");
                    }
                    if (!string.IsNullOrEmpty(DEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "a.DeliverEndOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(PEndDate);
                        sb.Append("' as DateTime) + 1 ");
                    }
                    if (!string.IsNullOrEmpty(RegisterMobile))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" b.MemberRegisterMobile ");
                        sb.Append(" like '%");
                        sb.Append(RegisterMobile);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(MobileNumber))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" b.MemberMobilePhone ");
                        sb.Append(" like '%");
                        sb.Append(MobileNumber);
                        sb.Append("%'");
                    }
                    //if (Country != "-1")
                    //{
                    //    if (sb.Length > 0)
                    //    {
                    //        sb.Append(" and ");
                    //    }
                    //    sb.Append(" CountryCode ");
                    //    sb.Append("='");
                    //    sb.Append(Country);
                    //    sb.Append("'");
                    //}
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;


                MemberOrderController c = new MemberOrderController();
                //int count = 0;
                DataSet ds = c.GetTransactionList(this.Grid1.PageSize * (this.Grid1.PageIndex + 1), strWhere, this.SortField.Text);//c.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.RecordCount = ds.Tables[0].Rows.Count;//count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Load MemberOrder", "error", ex);
            }
        }
        #endregion

        #region Event
        //protected void btnApprove_Click(object sender, EventArgs e)
        //{
        //    NewApproveTxns(Grid1, Window2);
        //}

        protected void btnSend_Click(object sender, EventArgs e)
        {
            ShippedTxns(Grid1, Window2);
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            PayedTxns(Grid1, Window2);
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(strWhere, "TxnNo");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);

            MemberOrderController controller = new MemberOrderController();
            #region 处理导入数据
            if (!string.IsNullOrEmpty(SVASessionInfo.ImportStorePath))
            {
                DataTable dt = ExcelTool.GetFirstSheet(SVASessionInfo.ImportStorePath);
                controller.ImportData(dt);
                SVASessionInfo.MessageHTML = controller.GetHtml(DateTime.Now).ToString();
                FineUI.PageContext.RegisterStartupScript(Window5.GetShowReference("~/PublicForms/MessageOK.aspx", "Message"));
                SVASessionInfo.ImportStorePath = null;
            }
            #endregion
            RptBind(strWhere, "TxnNo");
        }
        //protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        //{
        //    if (e.DataItem is DataRowView)
        //    {
        //        DataRowView drv = e.DataItem as DataRowView;
        //        string approveStatus = drv["ApproveStatus"].ToString().Trim();
        //        if (approveStatus != "")
        //        {
        //            approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
        //            switch (approveStatus)
        //            {
        //                case "A":
        //                    break;
        //                case "P":
        //                    (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
        //                    break;
        //                case "V":
        //                    (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
        //                    break;
        //            }
        //        }
        //    }
        //}

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["Status"].ToString().Trim();
                FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;

                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    if (approveStatus.ToLower() == "3" || approveStatus.ToLower() == "4")
                    {
                        editWF.Enabled = true;
                    }
                    else
                    {
                        editWF.Enabled = false;
                    }
                }
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            MemberOrderController c = new MemberOrderController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = c.GetTransactionList(this.Grid1.PageSize * (this.Grid1.PageIndex + 1), ViewState["strWhere"].ToString(), this.SortField.Text);//c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.RecordCount = ds.Tables[0].Rows.Count;//count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind(strWhere, "TxnNo");
        }

        protected void ShippedTxns(FineUI.Grid Grid, FineUI.Window Window)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid.SelectedRowIndexArray)
            {
                sb.Append(Grid.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(sb.ToString(), ",");
            idList = (from m in idList orderby m ascending select m).ToList();
            StringBuilder sb1 = new StringBuilder();
            foreach (var item in idList)
            {
                sb1.Append(item);
                sb1.Append(",");
            }
            Window.Title = "Shipped";
            string okScript = Window.GetShowReference("Shipped.aspx?ids=" + sb1.ToString().TrimEnd(','));
            string cancelScript = "";
            ShowConfirmDialog("Do you want to Ship " + "\n TXN NO.: \n" + sb1.ToString().Replace(",", ";\n"), "Shipped", FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }

        protected void PayedTxns(FineUI.Grid Grid, FineUI.Window Window)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int row in Grid.SelectedRowIndexArray)
            {
                sb.Append(Grid.DataKeys[row][0].ToString());
                sb.Append(",");
            }
            List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(sb.ToString(), ",");
            idList = (from m in idList orderby m ascending select m).ToList();
            StringBuilder sb1 = new StringBuilder();
            foreach (var item in idList)
            {
                sb1.Append(item);
                sb1.Append(",");
            }
            Window.Title = "Paid";
            string okScript = Window.GetShowReference("Payment.aspx?ids=" + sb1.ToString().TrimEnd(','));
            string cancelScript = "";
            ShowConfirmDialog("Do you want to Pay " + "\n TXN NO.: \n" + sb1.ToString().Replace(",", ";\n"), "Paid", FineUI.MessageBoxIcon.Question, okScript, cancelScript);
        }
    }
}