﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.Orders;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders
{
    public partial class Print : Tools.BasePage<SVA.BLL.Sales_H, SVA.Model.Sales_H>
    {
        MemberOrderController controller = new MemberOrderController();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                string id = Request.Params["id"];
                if (string.IsNullOrEmpty(id))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "Modify.aspx", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                controller.LoadView(id);

                this.rptOrders.DataSource = controller.ViewModel.HeadTable;
                this.rptOrders.DataBind();
            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater Tendlist = e.Item.FindControl("rptTendList") as Repeater;
                if (Tendlist == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                if (controller.ViewModel.TendTable != null && controller.ViewModel.TendTable.Rows.Count > 0)
                {
                    Tendlist.DataSource = controller.ViewModel.TendTable;
                    Tendlist.DataBind();
                }

                Repeater Detaillist = e.Item.FindControl("rptDetailList") as Repeater;
                if (Detaillist == null) return;

                System.Data.DataRowView drv1 = e.Item.DataItem as System.Data.DataRowView;
                if (drv1 == null) return;

                if (controller.ViewModel.DetailTable != null && controller.ViewModel.DetailTable.Rows.Count > 0)
                {
                    Detaillist.DataSource = controller.ViewModel.DetailTable;
                    Detaillist.DataBind();
                }
            }
        }

        protected void rptTendList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalAmount = e.Item.FindControl("lblTotalAmount") as Label;
                decimal total = Convert.ToDecimal(0);
                if (controller.ViewModel.TendTable != null && controller.ViewModel.TendTable.Rows.Count > 0)
                {
                    total = Tools.ConvertTool.ConverType<decimal>(controller.ViewModel.TendTable.Compute(" sum(TenderAmount) ", "").ToString());
                }
                lblTotalAmount.Text = total.ToString("N2");
            }
        }

        protected void rptDetailList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalProdQty = e.Item.FindControl("lblTotalProdQty") as Label;
                Label lblTotalProdAmount = e.Item.FindControl("lblTotalProdAmount") as Label;
                int qty = 0;
                decimal totalamount = Convert.ToDecimal(0);
                if (controller.ViewModel.DetailTable != null && controller.ViewModel.DetailTable.Rows.Count > 0)
                {
                    qty = Tools.ConvertTool.ConverType<int>(controller.ViewModel.DetailTable.Compute(" sum(Qty) ", "").ToString());
                    totalamount = Tools.ConvertTool.ConverType<decimal>(controller.ViewModel.DetailTable.Compute(" sum(NetAmount) ", "").ToString());
                }
                lblTotalProdQty.Text = qty.ToString();
                lblTotalProdAmount.Text = totalamount.ToString("N2");
            }
        }
    }
}