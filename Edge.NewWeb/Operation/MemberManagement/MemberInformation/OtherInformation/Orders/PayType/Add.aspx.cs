﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.Orders;
using Edge.Web.Tools;
using System.Data;


namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.Orders.PayType
{
    public partial class Add : PageBase
    {
        MemberOrderController controller = new MemberOrderController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                RegisterCloseEvent(this.btnClose);
                InitData();
            }
            controller = SVASessionInfo.MemberOrderController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            int rowindex = controller.ViewModel.TendTable.Rows.Count;
            if (controller.ViewModel.TendTable.Columns.Count == 0)
            {
                controller.ViewModel.TendTable.Columns.Add("SeqNo", typeof(int));
                controller.ViewModel.TendTable.Columns.Add("TenderID", typeof(string));
                controller.ViewModel.TendTable.Columns.Add("TenderAmount", typeof(decimal));
                controller.ViewModel.TendTable.Columns.Add("TenderNewName", typeof(string));

                controller.ViewModel.TendTable.Columns.Add("TenderCode", typeof(string));
                controller.ViewModel.TendTable.Columns.Add("ExchangeRate", typeof(decimal));
            }
            DataRow dr = controller.ViewModel.TendTable.NewRow();
            rowindex++;
            dr["SeqNo"] = rowindex;
            dr["TenderID"] = this.TenderID.SelectedValue;
            dr["TenderNewName"] = this.TenderID.SelectedText;
            dr["TenderAmount"] = ConvertTool.ToDecimal(this.TenderAmount.Text);

            dr["TenderCode"] = ViewState["TenderCode"].ToString();
            dr["ExchangeRate"] = ConvertTool.ToDecimal(ViewState["ExchangeRate"].ToString());
            controller.ViewModel.TendTable.Rows.Add(dr);
            CloseAndPostBack();
        }

        protected void InitData()
        {
            ViewState["TenderCode"] = "";
            ViewState["ExchangeRate"] = 1;

            this.TenderID.Items.Clear();
            DataSet ds = new Edge.SVA.BLL.TENDER().GetList("1 = 1 order by TenderCode");
            Edge.Web.Tools.ControlTool.BindDataSet(TenderID, ds, "TenderID", "TenderName1", "TenderName2", "TenderName3", "TenderCode");
        }

        protected void TenderID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.TenderID.SelectedValue != "-1")
            {
                DataTable dt = new Edge.SVA.BLL.TENDER().GetList("TenderID=" + TenderID.SelectedValue).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    ViewState["TenderCode"] = dr["TenderCode"].ToString();
                    ViewState["ExchangeRate"] = dr["Rate"].ToString();                   
                }
            }
        }
    }
}