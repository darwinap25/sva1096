﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.ResetMemberLoginPassword;
using System.Data;
using Edge.Web.Tools;
using Edge.Messages.Manager;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ResetMemberLoginPassword
{
    public partial class List : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                btnReset.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);

                RptBind("", "MemberMobilePhone");
                InitData();
            }
        }

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    int cardtype = this.CardType.SelectedValue == "-1" ? 0 : Convert.ToInt32(this.CardType.SelectedValue);
                    int cardgrade = this.CardGrade.SelectedValue == "-1" ? 0 : Convert.ToInt32(this.CardGrade.SelectedValue);
                    string cardnumber = this.CardNumber.Text;
                    string countrycode = this.CountryCode.SelectedValue;
                    string mobile = this.txtmobile.Text;
                    string MemberMobilePhone = this.MemberMobilePhone.Text;
                    if (cardtype>0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardTypeID = ");
                        sb.Append(cardtype);
                    }
                    if (cardgrade>0)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CardGradeID = ");
                        sb.Append(cardgrade);
                    }
                    if (!string.IsNullOrEmpty(cardnumber))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CardNumber = '");
                        sb.Append(cardnumber);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(countrycode) && countrycode != "-1")
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("CountryCode = '");
                        sb.Append(countrycode);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(mobile))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("MemberMobilePhone like '%");
                        sb.Append(mobile);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(MemberMobilePhone))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append("MemberRegisterMobile like '%");
                        sb.Append(MemberMobilePhone);
                        sb.Append("%'");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                ResetMemberLoginPasswordController controller = new ResetMemberLoginPasswordController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.Grid1.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("ResetMemberLoginPassword", "Load error", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            ResetMemberLoginPasswordController controller = new ResetMemberLoginPasswordController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.Grid1.RecordCount = count;

            DataTable table = ds.Tables[0];


            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "MemberMobilePhone");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "MemberMobilePhone");
        }

        private void InitData()
        {
            this.BindCardType(this.CardType);
            BindCountry(this.CountryCode);
        }

        public void BindCardType(FineUI.DropDownList ddl)
        {
            Tools.ControlTool.BindCardType(this.CardType);
        }

        public void BindCardGrade(FineUI.DropDownList ddl, int cardTypeID)
        {
            Tools.ControlTool.BindCardGrade(this.CardGrade, cardTypeID);
        }

        protected void CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindCardGrade(this.CardGrade, Convert.ToInt32(this.CardType.SelectedValue));
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            StringBuilder sbMobile = new StringBuilder();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                sbMobile.Append(Grid1.DataKeys[row][3] == null ? "" : Grid1.DataKeys[row][3].ToString());
                //sbMobile.Append(Grid1.DataKeys[row][2].ToString());
                sbMobile.Append(",");
            }

            Window2.Title = Resources.MessageTips.ResetPassword;
            string okScript = Window2.GetShowReference(string.Format("NewResetPassword.aspx?&mobile={0}", sbMobile.ToString().TrimEnd(',').Trim()));
            ShowConfirmDialog(Resources.MessageTips.ConfirmResetPassword, Window2.Title, FineUI.MessageBoxIcon.Question, okScript, "");
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "MemberMobilePhone");
        }

        public void BindCountry(FineUI.DropDownList ddl)
        {
            ddl.Items.Clear();
            DataSet ds = ds = new Edge.SVA.BLL.Nation().GetAllList();
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    FineUI.ListItem li = new FineUI.ListItem() { Value = dr["CountryCode"].ToString().Trim(), Text = dr["CountryCode"].ToString().Trim() };
                    ddl.Items.Add(li);
                }
            }
            ddl.Items.Insert(0, new FineUI.ListItem() { Text = "----------", Value = "-1" });
        }
    }
}