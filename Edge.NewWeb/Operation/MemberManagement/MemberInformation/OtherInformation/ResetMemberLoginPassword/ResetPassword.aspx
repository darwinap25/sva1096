﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ResetMemberLoginPassword.ResetPassword" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="20px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                    </ext:ToolbarSeparator>
                    <ext:Button ID="btnSaveClose" ValidateForms="sform1" Icon="SystemSaveClose"
                        runat="server" Text="保存后关闭" OnClick="btnSaveClose_Click" >
                    </ext:Button>
                    <ext:ToolbarFill ID="ToolbarFill3" runat="server">
                    </ext:ToolbarFill>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:SimpleForm ID="sform1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                    EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:Form ID="Form3" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow2" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label1" runat="server"></ext:Label>   
                                    <ext:Label ID="Label4" runat="server"></ext:Label>  
                                    <ext:TextBox ID="CardTypeID" runat="server" Label="卡类型：" Enabled="false"> 
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form4" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow3" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label5" runat="server"></ext:Label>   
                                    <ext:Label ID="Label6" runat="server"></ext:Label>  
                                    <ext:TextBox ID="CardGradeID" runat="server" Label="卡级别：" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow4" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label7" runat="server"></ext:Label>   
                                    <ext:Label ID="Label8" runat="server"></ext:Label>  
                                    <ext:TextBox ID="CardNumber" runat="server" Label="卡号：" MaxLength="512" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form6" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow5" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label9" runat="server"></ext:Label>   
                                    <ext:Label ID="Label10" runat="server"></ext:Label>  
                                    <ext:TextBox ID="CountryCode" runat="server" Label="国家码：" MaxLength="512" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form7" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow6" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label11" runat="server"></ext:Label>   
                                    <ext:Label ID="Label12" runat="server"></ext:Label>  
                                    <ext:TextBox ID="MemberRegisterMobile" runat="server" Label="手机号码：" MaxLength="512" Enabled="false">
                                    </ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label3" runat="server"></ext:Label>   
                                    <ext:CheckBox ID="ChkEmail" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtEmail" runat="server" Label="邮件："  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label2" runat="server"></ext:Label>
                                    <ext:CheckBox ID="ChkMessage" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtMessage" runat="server" Label="短信："  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form8" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow7" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label13" runat="server"></ext:Label>   
                                    <ext:CheckBox ID="ChkMSN" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtMSN" runat="server" Label="Translate__Special_121_StartMSN：Translate__Special_121_End"  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form9" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow8" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label14" runat="server"></ext:Label>
                                    <ext:CheckBox ID="ChkQQ" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtQQ" runat="server" Label="Translate__Special_121_StartQQ：Translate__Special_121_End"  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form10" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow9" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label15" runat="server"></ext:Label>   
                                    <ext:CheckBox ID="ChkAPP" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtAPP" runat="server" Label="Translate__Special_121_StartWhat is APP：Translate__Special_121_End"  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form11" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow10" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label16" runat="server"></ext:Label>
                                    <ext:CheckBox ID="ChkFacebook" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtFacebook" runat="server" Label="Translate__Special_121_StartFacebook：Translate__Special_121_End"  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form12" runat="server" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true" LabelAlign="Left">
                        <Rows>
                            <ext:FormRow ID="FormRow11" runat="server" ColumnWidths="8% 2% 90%">
                                <Items>
                                    <ext:Label ID="Label17" runat="server"></ext:Label>
                                    <ext:CheckBox ID="ChkSina" runat="server" ShowLabel="false"></ext:CheckBox>
                                    <ext:TextBox ID="txtSina" runat="server" Label="新浪微博："  Height="25px" ></ext:TextBox>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                </Items>
            </ext:SimpleForm>
        </Items>
    </ext:Panel>
    </form>
</body>
</html>

