﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ResetMemberLoginPassword
{
    public partial class NewResetPassword : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                    if (!hasRight)
                    {
                        return;
                    }
                    string mobiles = Request.Params["mobile"];
                    Tools.ResetPasswordUtil rp = new Tools.ResetPasswordUtil();

                    if (string.IsNullOrEmpty(mobiles))
                    {
                        ShowWarning(Resources.MessageTips.MissingPhoneNumber);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("MemberRegisterMobile", typeof(string));
                    dt.Columns.Add("ResetStatus", typeof(string));

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(mobiles, ",");
                    foreach (string id in idList)
                    {
                        DataRow dr = dt.NewRow();
                        dr["MemberRegisterMobile"] = id;
                        if (rp.ResetPassord(SVASessionInfo.CurrentUser.UserID.ToString(), id))
                        {
                            dr["ResetStatus"] = Resources.MessageTips.ResetSuccess;
                        }
                        else
                        {
                            dr["ResetStatus"] = Resources.MessageTips.ResetFailed;
                        }
                        dt.Rows.Add(dr);
                    }

                    this.Grid1.DataSource = dt;
                    this.Grid1.DataBind();

                }
                catch (Exception ex)
                {
                    //Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    //Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
    }
}