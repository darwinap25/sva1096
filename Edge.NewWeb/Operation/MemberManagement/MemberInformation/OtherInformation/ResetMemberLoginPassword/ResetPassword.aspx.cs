﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.OtherInformation.ResetMemberLoginPassword;
using System.Text.RegularExpressions;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ResetMemberLoginPassword
{
    public partial class ResetPassword : PageBase
    {
        ResetMemberLoginPasswordController controller = new ResetMemberLoginPasswordController();
        List<FineUI.CheckBox> chklist = new List<FineUI.CheckBox>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterCloseEvent(btnClose);
                InitData();
            }
        }

        private void InitData()
        {
            //string cardnumber = Request.Params["id"].ToString();
            //controller.LoadViewModel(cardnumber);
            //this.CardTypeID.Text = Request.Params["CardTypeName"].ToString();//controller.ViewModel.CardTypeID.ToString() ;
            //this.CardGradeID.Text = Request.Params["CardGradeName"].ToString();//controller.ViewModel.CardGradeID.ToString();
            //this.CardNumber.Text = controller.ViewModel.CardNumber;
            //this.MemberRegisterMobile.Text = controller.ViewModel.MemberRegisterMobile;
            //this.CountryCode.Text = controller.ViewModel.CountryCode;

            ////消息形式
            //this.txtEmail.Text = controller.ViewModel.Email;
            //this.txtMessage.Text = this.CountryCode.Text + this.MemberRegisterMobile.Text;//controller.ViewModel.Message;
            //this.txtMSN.Text = controller.ViewModel.MSNNumber;
            //this.txtQQ.Text = controller.ViewModel.QQNumber;
            //this.txtAPP.Text = controller.ViewModel.APP;
            //this.txtFacebook.Text = controller.ViewModel.FacebookNumber;
            //this.txtSina.Text = controller.ViewModel.SinaNumber;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (!IsCheckMessageType())
            {
                return;
            }
            if (!ValidataMessage())
            {
                return;
            }
        }

        //校验邮箱格式是否正确
        private bool IsEmail(string emailStr)
        {
            return Regex.IsMatch(emailStr, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
 
        //判定至少选中一种消息形式
        private bool IsCheckMessageType()
        {
            int checkcount = 0;
            List<Control> list = new List<Control>();
            GetControls(this.Form, ref list);
            foreach (Control item in list)
            {
                if (item is FineUI.CheckBox)
                {
                    FineUI.CheckBox cb = item as FineUI.CheckBox;
                    chklist.Add(cb);
                    if (cb.Checked)
                    {
                        checkcount++;
                    }
                }
            }
            if (checkcount == 0)
            {
                ShowWarning("至少选中一种消息形式！");
                return false;
            }
            return true;
        }

        //判定选中的消息形式是否有值
        private bool ValidataMessage()
        {
            foreach (var item in chklist)
            {
                switch (item.ID.ToLower())
                {
                    case "chkemail":
                        if (item.Checked && this.txtEmail.Text == "")
                        {
                            ShowWarning("邮箱不能为空！");
                            return false;
                        }
                        if (item.Checked && !this.IsEmail(this.txtEmail.Text))
                        {
                            ShowWarning("邮箱格式不正确！");
                            return false;
                        }
                        break;
                    case "chkmessage":
                        if (item.Checked && this.txtMessage.Text == "")
                        {
                            ShowWarning("短信不能为空！");
                            return false;
                        }
                        break;
                    case "chkmsn":
                        if (item.Checked && this.txtMSN.Text == "")
                        {
                            ShowWarning("MSN不能为空！");
                            return false;
                        }
                        break;
                    case "chkqq":
                        if (item.Checked && this.txtQQ.Text == "")
                        {
                            ShowWarning("QQ不能为空！");
                            return false;
                        }
                        break;
                    case "chkapp":
                        if (item.Checked && this.txtAPP.Text == "")
                        {
                            ShowWarning("APP不能为空！");
                            return false;
                        }
                        break;
                    case "chkfacebook":
                        if (item.Checked && this.txtFacebook.Text == "")
                        {
                            ShowWarning("Facebook不能为空！");
                            return false;
                        }
                        break;
                    case "chksina":
                        if (item.Checked && this.txtSina.Text == "")
                        {
                            ShowWarning("Sina不能为空！");
                            return false;
                        }
                        break;
                }
            }
            return true;
        }

        //获取控件列表
        public void GetControls(Control con, ref List<Control> list)
        {
            foreach (Control item in con.Controls)
            {
                if (item.Controls.Count >= 1)
                {
                    GetControls(item, ref list);
                }
                else
                {
                    list.Add(item);
                }
            }
        }
    }
}