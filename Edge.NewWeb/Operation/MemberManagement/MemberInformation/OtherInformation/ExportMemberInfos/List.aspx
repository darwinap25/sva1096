﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.OtherInformation.ExportMemberInfos.List" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
</head>
<body>
    <form id="form2" runat="server">
    <ext:PageManager ID="PageManager1" runat="server" AutoSizePanelID="Panel1" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="3px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="VBox" BoxConfigAlign="Stretch">
        <Toolbars>
            <ext:Toolbar>
                <Items>
                    <ext:Button ID="SearchButton" Text="搜索" Icon="Find" runat="server" OnClick="SearchButton_Click">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>
        <Items>
            <ext:Form ID="SearchForm" ShowBorder="True" BodyPadding="5px" EnableBackgroundColor="true"
                ShowHeader="False" runat="server" LabelAlign="Right" LabelWidth="100">
                <Rows>
                    <ext:FormRow>
                        <Items>
                            <ext:TextBox ID="EngLishName" runat="server" Label="英文名称：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="ChineseName" runat="server" Label="中文名称：" MaxLength="512">
                            </ext:TextBox>
                            <ext:DropDownList ID="MemberSex" runat="server" Label="性别：" Resizable="true"></ext:DropDownList>
                            <ext:DropDownList ID="MemberMarital" runat="server" Label="婚姻情况：" Resizable="true"></ext:DropDownList>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:DropDownList ID="EducationID" runat="server" Label="学历：" Resizable="true"></ext:DropDownList>
                            <ext:DropDownList ID="ProfessionID" runat="server" Label="专业：" Resizable="true"></ext:DropDownList>
                            <ext:DropDownList ID="OfficeAddress" runat="server" Label="学校地区：" Resizable="true"
                                OnSelectedIndexChanged="OfficeAddress_SelectedChanged" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="CompanyDesc" runat="server" Label="学校名称：" Resizable="true"></ext:DropDownList>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:DatePicker ID="MemberDateOfBirthStart" runat="server" Label="生日日期从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="MemberDateOfBirthEnd" runat="server" Label="生日日期到：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:Label ID="Label20" runat="server" Label="" HideMode="Offsets" Height="20"></ext:Label>
                            <ext:Label ID="Label6" runat="server" Label="" HideMode="Offsets" Height="20"></ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:DropDownList ID="StateCode" runat="server" Label="国家码：" Resizable="true"></ext:DropDownList>
                            <ext:TextBox ID="MemberMobilePhone" runat="server" Label="手机号码：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="Email" runat="server" Label="邮箱地址：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="FaceBook" runat="server" Label="FaceBook：" MaxLength="512">
                            </ext:TextBox>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:TextBox ID="QQ" runat="server" Label="QQ：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="MSN" runat="server" Label="MSN：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="Sina" runat="server" Label="新浪微博：" MaxLength="512">
                            </ext:TextBox>
                            <ext:TextBox ID="OtherContact" runat="server" Label="其他联系方式：" MaxLength="512">
                            </ext:TextBox>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:DropDownList ID="CountryCode" runat="server" Label="国家编码：" Resizable="true"
                                OnSelectedIndexChanged="CountryCode_SelectedChanged" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="ProvinceCode" runat="server" Label="省（州）编码：" Resizable="true"
                                OnSelectedIndexChanged="ProvinceCode_SelectedChanged" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="CityCode" runat="server" Label="城市编码：" Resizable="true"
                                OnSelectedIndexChanged="CityCode_SelectedChanged" AutoPostBack="true"></ext:DropDownList>
                            <ext:DropDownList ID="DistrictCode" runat="server" Label="区县编码：" Resizable="true"></ext:DropDownList>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:TextBox ID="AddressFullDetail" runat="server" Label="详细地址：" MaxLength="512">
                            </ext:TextBox>
                            <ext:Label ID="Label7" runat="server" Label="" HideMode="Offsets" Height="20"></ext:Label>
                            <ext:Label ID="Label8" runat="server" Label="" HideMode="Offsets" Height="20"></ext:Label>
                            <ext:Label ID="Label9" runat="server" Label="" HideMode="Offsets" Height="20"></ext:Label>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:DropDownList ID="CardTypeID" runat="server" Label="卡类型：" Resizable="true"
                                OnSelectedIndexChanged="CardTypeID_SelectedChanged" AutoPostBack="true">
                            </ext:DropDownList>
                            <ext:DropDownList ID="CardGradeID" runat="server" Label="卡级别：" Resizable="true"></ext:DropDownList>
                            <ext:TextBox ID="CardNumber" runat="server" Label="卡号码：" MaxLength="512"></ext:TextBox>
                            <ext:DropDownList ID="CardStatus" runat="server" Label="卡状态：" Resizable="true"></ext:DropDownList>
                        </Items>
                    </ext:FormRow>
                    <ext:FormRow>
                        <Items>
                            <ext:DatePicker ID="CreatedOnStart" runat="server" Label="注册日期从：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:DatePicker ID="CreatedOnEnd" runat="server" Label="注册日期到：" DateFormatString="yyyy-MM-dd">
                            </ext:DatePicker>
                            <ext:Label ID="Label10" runat="server" HideMode="Offsets" Hidden="true"></ext:Label>
                            <ext:Label ID="Label11" runat="server" HideMode="Offsets" Hidden="true"></ext:Label>
                        </Items>
                    </ext:FormRow>
                </Rows>
            </ext:Form>
            <ext:Panel ID="Panel2" ShowBorder="false" ShowHeader="false" runat="server" EnableBackgroundColor="true"
                Title="" BoxFlex="1" Layout="Fit">
                    <Toolbars>
                        <ext:Toolbar>
                            <Items>
                                <ext:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" Icon="PageExcel"
                                    EnableAjax="false" DisableControlBeforePostBack="false">
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </Toolbars>
                <Items>
                    <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" DataKeyNames="MemberID" AllowPaging="true" IsDatabasePaging="true" 
                        EnableRowNumber="True" AutoWidth="true" ForceFitAllTime="true" OnPageIndexChange="Grid1_PageIndexChange"
                        OnSort="Grid1_Sort" AllowSorting="true">
                        <Toolbars>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Label ID="lblResult" runat="server" Text="搜索结果列表"></ext:Label>
                                </Items>
                            </ext:Toolbar>
                        </Toolbars>
                                <Columns>
                            <ext:TemplateField Width="60px" HeaderText="英文名称" SortField="MemberEngGivenName">
                                <ItemTemplate>
                                    <asp:Label ID="lblEngName1" runat="server" Text='<%# Eval("MemberEngGivenName") %>'></asp:Label>.<asp:Label
                                        ID="lblEngName2" runat="server" Text='<%#Eval("MemberEngFamilyName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="中文名称">
                                <ItemTemplate>
                                    <asp:Label ID="lblChiName" runat="server" Text='<%#Eval("MemberChiFamilyName")%>'></asp:Label>.<asp:Label
                                        ID="lblChiName2" runat="server" Text='<%#Eval("MemberChiGivenName")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="国家码">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("CountryCode")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="手机号码">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("MemberMobilePhone")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡类型">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("CardType")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡级别">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("CardGrade")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="卡号码">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("CardNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="注册日期" SortField="CreatedOn">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                        </Columns>
                    </ext:Grid>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Panel>
    <ext:Window ID="Window1" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="850px" Height="550px">
    </ext:Window>
    <ext:Window ID="Window2" Title="编辑" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="HidePostBack" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="true" EnableResize="true"
        Target="Top" IsModal="True" Width="900px" Height="550px"> 
    </ext:Window>
    <ext:Window ID="HiddenWindowForm" Title="" Popup="false" EnableIFrame="true" runat="server"
        CloseAction="Hide" OnClose="WindowEdit_Close" IFrameUrl="about:blank" EnableMaximize="false"
        EnableResize="true" Target="Top" IsModal="True" Width="50px" Height="50px" Left="-1000px"
        Top="-1000px">
    </ext:Window>
    <ext:HiddenField ID="SearchFlag" Text="0" runat="server"></ext:HiddenField>
    <ext:HiddenField ID="SortField" Text="" runat="server"></ext:HiddenField>
    </form>
</body>
</html>
