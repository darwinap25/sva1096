﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Others
{
    public partial class List : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindLanguageMap(MemberDefLanguage);
                 Edge.SVA.Model.Member item =new Edge.SVA.BLL.Member().GetModel(Tools.ConvertTool.ConverType<int>(Request.Params["id"]));
                 if (item != null)
                 {
                     MemberDefLanguage.SelectedValue = item.MemberDefLanguage != null ? item.MemberDefLanguage.Value.ToString() : "0";
                 }
            }
        }

        protected void btnOnlineShippingAddress_Click(object sender, EventArgs e)
        {
            Response.Redirect("OnlineShippingAddress/List.aspx?id=" +Request.Params["id"]);
        }

        protected void btnMemberFriends_Click(object sender, EventArgs e)
        {
            Response.Redirect("MemberFriends/List.aspx?id=" + Request.Params["id"]);
        }

        protected void btnMemberOrderManagement_Click(object sender, EventArgs e)
        {
            Response.Redirect("MemberOrder/List.aspx?id=" + Request.Params["id"]);
        }

        protected void btnMemberStatement_Click(object sender, EventArgs e)
        {
            Response.Redirect("MemberStatement/List.aspx?id=" + Request.Params["id"]);
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {

        }

        protected void btnSaveLan_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Member item =new Edge.SVA.BLL.Member().GetModel(Edge.Web.Tools.ConvertTool.ToInt(Request.Params["id"]));
            if (item != null)
            {
                item.MemberDefLanguage = Edge.Web.Tools.ConvertTool.ToInt(this.MemberDefLanguage.SelectedValue);
                Edge.SVA.BLL.Member bll = new Edge.SVA.BLL.Member();
                if (bll.Update(item))
                {
                    JscriptPrint(Resources.MessageTips.UpdateSuccess, "#", Resources.MessageTips.SUCESS_TITLE);
                }
                else
                {
                    JscriptPrint(Resources.MessageTips.UpdateFailed, "#", Resources.MessageTips.FAILED_TITLE);
                }
            }
        }

        protected void btnMemberContact_Click(object sender, EventArgs e)
        {
            Response.Redirect("MemberContact/List.aspx?id=" + Request.Params["id"]);
        }
    }
}
