﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Others.MemberFriends
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberFriend, Edge.SVA.Model.MemberFriend>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!Page.IsPostBack)
            {
                this.SNSTypeID.Text = DALTool.GetSNSTypeName(Model.SNSTypeID.Value, null);
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());
            }
        }
    }
}
