﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Others.OnlineShippingAddress.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：查看会员送货地址</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                查看会员送货地址
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                国家：
            </td>
            <td width="75%">
                <asp:Label ID="AddressCountry" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                省（州）：
            </td>
            <td>
                <asp:Label ID="AddressProvince" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                城市：
            </td>
            <td>
                <asp:Label ID="AddressCity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                详细地址：
            </td>
            <td>
                <asp:Label ID="AddressDetail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮编：
            </td>
            <td>
                <asp:Label ID="AddressZipCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:Label ID="Contact" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:Label ID="ContactPhone" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否会员默认地址：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberFirstAddr" TabIndex="8" runat="server" Enabled="false"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="0">否</asp:ListItem>
                    <asp:ListItem Value="1">是</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
