﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Others.List" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                会员登录密码
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                修改登录密码：
            </td>
            <td width="75%">
                <asp:TextBox ID="MemberAppellation" TabIndex="1" runat="server" MaxLength="512"></asp:TextBox>
                <asp:Button ID="btnResetPassword" runat="server" Text="提交" CssClass="submit" OnClick="btnResetPassword_Click">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                登录默认语言
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                修改默认语言：
            </td>
            <td width="75%">
                <asp:DropDownList ID="MemberDefLanguage" runat="server" CssClass="dropdownlist">
                </asp:DropDownList>
                <asp:Button ID="btnSaveLan" runat="server" Text="提交" CssClass="submit" OnClick="btnSaveLan_Click">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                其他信息
            </th>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnMemberContact" runat="server" Text="其他联系方式" CssClass="submit"
                    OnClick="btnMemberContact_Click"></asp:Button>
                <asp:Button ID="btnOnlineShippingAddress" runat="server" Text="送货地址管理" CssClass="submit"
                    OnClick="btnOnlineShippingAddress_Click"></asp:Button>
                <asp:Button ID="btnMemberFriends" runat="server" Text="查看好友" CssClass="submit" OnClick="btnMemberFriends_Click">
                </asp:Button>
                <asp:Button ID="btnMemberOrderManagement" runat="server" Text="查看订单" CssClass="submit"
                    OnClick="btnMemberOrderManagement_Click"></asp:Button>
                <asp:Button ID="btnMemberStatement" runat="server" Text="查看结算单" CssClass="submit"
                    OnClick="btnMemberStatement_Click"></asp:Button>
                <input type="button" name="button1" value="返 回" onclick="location.href= '../Basic/List.aspx' "
                    class="submit" />
            </td>
        </tr>
    </table>
    <div class="spClear">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
