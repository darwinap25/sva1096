﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.IO;
using System.Data;
using Edge.Web.DAO;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.Model.Master;
using Edge.Common;

namespace Edge.Web.Operation.MemberManagement.ImportMemberSegment
{
    public partial class Show : PageBase
    {
        public static string XMLDownloadPath;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!hasRight)
            {
                return;
            }
            RegisterCloseEvent(btnClose);
        }

        private string ApproveStatusStringUpdate(string ApproveStatus)
        {

            string ApproveResult="";

            if (ApproveStatus == "A")
            {
                ApproveResult = "Approved";
            }
            else if (ApproveStatus == "P")
            {
                ApproveResult = "Pending";
            }
            else if (ApproveStatus == "F")
            {
                ApproveResult = "Failed";
            }
            else if (ApproveStatus == "S")
            {
                ApproveResult = "Success";
            }
            else if (ApproveStatus == "V")
            {
                ApproveResult = "Void";
            }

            return ApproveResult;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }


                ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();
                Ord_ImportMemberSegment_H vo = new Ord_ImportMemberSegment_H();
                DataSet ds = new DataSet();
                string id = Server.UrlDecode(Request.QueryString["id"]);

                vo.ImportMemberSegmentNumber = id;

                ds = dao.ViewImportMemberSegments(vo);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.ImportMemberSegmentNumber.Text = ds.Tables[0].Rows[0]["ImportMemberSegmentNumber"].ToString();
                    this.ApprovalCode.Text = ds.Tables[0].Rows[0]["ApprovalCode"].ToString();
                    this.ApproveStatus.Text = ApproveStatusStringUpdate(ds.Tables[0].Rows[0]["ApproveStatus"].ToString());
                    this.ApproveBusDate.Text = ds.Tables[0].Rows[0]["ApproveBusDate"].ToString();
                    this.CreatedBusDate.Text = ds.Tables[0].Rows[0]["CreatedBusDate"].ToString();
                    this.ApproveOn.Text = ds.Tables[0].Rows[0]["ApproveOn"].ToString();
                    this.CreatedOn.Text = ds.Tables[0].Rows[0]["CreatedOn"].ToString();
                    //this.ApproveBy.Text = ds.Tables[0].Rows[0]["ApproveBy"].ToString();
                    this.ApproveBy.Text = Tools.DALTool.GetUserName(Convert.ToInt32(ds.Tables[0].Rows[0]["ApproveBy"]));
                    //this.CreatedBy.Text = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                    this.CreatedBy.Text = Tools.DALTool.GetUserName(Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"]));
                    this.Note.Text = ds.Tables[0].Rows[0]["Note"].ToString();
                }



                this.uploadFilePath.Text = ds.Tables[0].Rows[0]["Description"].ToString();//controller.ViewModel.MainTable.Description;
               // Path.GetFileName()
                this.btnExport.Hidden = string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Description"].ToString()) ? true : false;   //string.IsNullOrEmpty(controller.ViewModel.MainTable.Description) ? true : false;//没有文件时不显示查看按钮(Len)

                XMLDownloadPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["XMLDownloadPath"]) + this.ImportMemberSegmentNumber.Text; //controller.ViewModel.MainTable.ImportMemberNumber.ToString();//Add By Robin 2015-08-31

                string[] downloadlist = GetAllFiles(XMLDownloadPath);

                int i = 0;
                foreach (string filename in downloadlist)
                {
                    i++;
                    FineUI.TreeNode fileNode = new FineUI.TreeNode();
                    fileNode.EnableCheckBox = true;
                    fileNode.AutoPostBack = true;
                    fileNode.NodeID = i.ToString(); ;
                    fileNode.Text = filename;
                    FileTree.Nodes.Add(fileNode);
                }

                if (!string.IsNullOrEmpty(Request.Params["filename"]))
                {
                    this.Description.Text = Path.GetFileName(Request.Params["filename"].ToString()); // updated by henry 20170925
                }

                this.RptBind();
            }
        }
        private void RptBind()
        {
            try
            {
                DataSet ds = new DataSet();
                ImportMemberSegmentImportErrorsDAO dao = new ImportMemberSegmentImportErrorsDAO();
                Ord_ImportMemberSegment_ImportErrors vo = new Ord_ImportMemberSegment_ImportErrors();

                int count = 0;

                vo.ImportMemberSegmentNumber = this.ImportMemberSegmentNumber.Text;
                ds = dao.GetImportMemberSegmentImportErrors(vo);

                //count = int.Parse(ds.Tables[1].Rows[0]["RowCount"].ToString());
                this.Grid1.RecordCount = count;

                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Load ImportMemberSegmentImportErrors", "error", ex);
            }
        }

        public string[] GetAllFiles(string DownloadPath)
        {
            string dirPath = HttpContext.Current.Server.MapPath(DownloadPath);
            //string dirPath = DownloadPath;
            if (Directory.Exists(dirPath))
            {
                //获得目录信息
                DirectoryInfo dir = new DirectoryInfo(dirPath);
                //获得目录File List
                FileInfo[] files = dir.GetFiles("*.*");
                string[] fileNames = new string[files.Length];
                int i = 0;
                foreach (FileInfo fileInfo in files)
                {
                    fileNames[i] = fileInfo.Name;
                    i++;
                }
                return fileNames;
            }
            else
                return new string[0];
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);

            #region 处理导入数据
            #endregion
        }


        private void ExportFile(string filename)
        {
            string FileName = XMLDownloadPath + "\\" + filename;
            FileInfo file = new FileInfo(Server.MapPath(FileName));
            Response.AddHeader("content-type", "text/xml;");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlPathEncode(filename));
            Response.AddHeader("content-length", file.Length.ToString());
            Response.TransmitFile(FileName);
            Response.End();
        }

        protected void NodeCheck(object sender, FineUI.TreeCheckEventArgs e)
        {
            foreach (FineUI.TreeNode fileNode in FileTree.GetCheckedNodes())
            {
                if (fileNode.NodeID != e.NodeID)
                {
                    fileNode.Checked = false;
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            foreach (FineUI.TreeNode fileNode in FileTree.GetCheckedNodes())
            {
                ExportFile(fileNode.Text);
            }
            FileTree.UncheckAllNodes();


            /*
                        string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
                        try
                        {
                            //以XML的形式呈现
                            if (controller.ViewModel.MainTable.ApproveStatus == "A")
                            {
                                string xmlfilename = "CustomerImport-" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + "-01.xml";
                                Tools.ExportTool.ExportFile(fileName.Replace(".csv", ".xml"), xmlfilename);
                            }
                            else
                            {
                                Tools.ExportTool.ExportFile(fileName);
                            }
                            Tools.Logger.Instance.WriteOperationLog("ImportMemberInfos download ", " filename: " + fileName);
                        }
                        catch (Exception ex)
                        {
                            string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                            Tools.Logger.Instance.WriteErrorLog("ImportMemberInfos download ", " filename: " + fileName, ex);
                            ShowWarning(ex.Message);
                        }
             */
        }

    }
}