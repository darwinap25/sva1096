﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.Messages.Manager;
using System.Data;
using FineUI;
using Edge.SVA.Model.Master;
using Edge.Web.DAO;

namespace Edge.Web.Operation.MemberManagement.ImportMemberSegment
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                
                btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();


                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    string files = Request.Params["filenames"];//文件名集合
                    if (string.IsNullOrEmpty(ids))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));

                    //传入配置文件中HQDB的相关值
                    //controller.connString = webset.HQDBconnectString;
                    //controller.sqlString = webset.HQEnqueryString;
                    //controller.sqlString2 = webset.HQEnqueryString2;

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");
                    List<string> fileList = Edge.Utils.Tools.StringHelper.SplitString(files, ",");
                    bool isSuccess = false;

                    for (int i = 0; i < idList.Count; i++)
                    {
                        

                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = idList[i];


                        if (!string.IsNullOrEmpty(fileList[i]))
                        {
                            string filename = Server.MapPath("~" + fileList[i]);
                            string ImportPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CSVImportPath"]);
                            ImportPath = ImportPath + idList[i].ToString() + "/";
                            ImportPath = Server.MapPath(ImportPath);

                            if (!Directory.Exists(ImportPath))
                            {
                                Directory.CreateDirectory(ImportPath);
                            }

                            ImportPath = ImportPath + System.IO.Path.GetFileName(filename);


                            if (System.IO.File.Exists(filename))
                            {
                                System.IO.File.Copy(filename, ImportPath, true);
                                System.IO.File.Delete(filename);
                            }
                        }

                        //批核成功后要将CSV文件转换为XML文件
                        /*   remove convert by gavin @2015-08-26
                        if (!string.IsNullOrEmpty(fileList[i]))
                        {
                            string filename = Server.MapPath("~" + fileList[i]);
                            if (!controller.ExchangeCSVToXML(filename, idList[i], false))
                            {
                                dr["ApproveCode"] = "Conversion failure";//文件转换失败时的提示
                                dt.Rows.Add(dr);
                                continue;
                            }
                        }
                        */
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member" + idList[i].ToString());

                        Ord_ImportMemberSegment_H vo = new Ord_ImportMemberSegment_H();
                        ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();


                       
                        vo.ImportMemberSegmentNumber = idList[i].ToString();
                        vo.ApproveBy = Tools.DALTool.GetCurrentUser().UserName;
                        vo.ApproveBusDate = DateTime.Parse(DALTool.GetBusinessDate());

                        string approveCode = dao.ApproveImportMemberSegmentHeader(vo).Trim(); // Edge.Web.Controllers.CouponController.ApproveCouponForApproveCode(vo, out isSuccess);
                        dr["ApproveCode"] = approveCode;
                        if (approveCode.Length == 6) isSuccess = true;

                        if (isSuccess)
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + idList[i].ToString() + " " + Resources.MessageTips.ApproveCode + " " + approveCode);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                            string apprvemsg = Resources.MessageTips.ApproveCode;
                        }
                        else
                        {

                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + idList[i].ToString() + " " + Resources.MessageTips.ApproveCode + " " + approveCode);

                            if (approveCode == "1")
                            {
                                dr["ApprovalMsg"] = "The transaction status is NOT Pending";//"Approve Failed: Record is already approved"; 
                            
                            }
                            else if (approveCode == "2")
                            {
                                dr["ApprovalMsg"] = "Approve Failed: Database returned an error";

                            }
                        }
                        dt.Rows.Add(dr);
                    }

                    //foreach (string id in idList)
                    //{
                    //    Edge.SVA.Model.Ord_ImportMember_H mode = new Edge.SVA.BLL.Ord_ImportMember_H().GetModel(id);

                    //    DataRow dr = dt.NewRow();
                    //    dr["TxnNo"] = id;
                    //    dr["ApproveCode"] = Edge.Web.Controllers.CouponController.ApproveCouponForApproveCode(mode, out isSuccess);
                    //    if (isSuccess)
                    //    {
                    //        Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveCode);

                    //        dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;

                    //    }
                    //    else
                    //    {
                    //        Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveError);

                    //        dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                    //    }
                    //    dt.Rows.Add(dr);
                    //}
                    this.Grid1.DataSource = dt;
                    this.Grid1.DataBind();
                   // RefreshParentPage();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    //Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                    Alert.ShowInTop(ex.Message, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndRefresh();
        }
    }
}