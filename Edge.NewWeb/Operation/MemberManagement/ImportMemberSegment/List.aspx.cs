﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.DAO;
using Edge.SVA.Model.Domain.SVA;
using Edge.SVA.Model.Domain.Surpport;
using System.Text;
using Edge.Messages.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Operation.MemberManagement.ImportMemberSegment
{
    public partial class List : PageBase
    {
        
        
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                //RptBind("", "ImportMemberSegmentNumber");
                RptBind(1);
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);

            }
        }

        #region  Approve Void

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            List<KeyValue> list = new List<KeyValue>();
            
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                list.Add(new KeyValue(Grid1.DataKeys[row][0].ToString(), Grid1.DataKeys[row][1] == null ? "" : Grid1.DataKeys[row][1].ToString()));
            
            }
            list = (from m in list orderby m.Key ascending select m).ToList();

            StringBuilder sb = new StringBuilder();
            StringBuilder sbMsg = new StringBuilder();
            StringBuilder sbfile = new StringBuilder();
            foreach (var item in list)
            {
                sb.Append(item.Key);
                sb.Append(",");
                sbMsg.Append(item.Key);
                sbMsg.Append(";\n");
                sbfile.Append(item.Value);
                sbfile.Append(",");
            }
            Window2.Title = Resources.MessageTips.Approve;
            string url = string.Format("Approve.aspx?ids={0}&filenames={1}" , sb.ToString().TrimEnd(','), sbfile.ToString().TrimEnd(','));
            ApproveTxns(sbMsg.ToString(), Window2.GetShowReference(url), "");

            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //StringBuilder sbfile = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //    sbfile.Append(Grid1.DataKeys[row][1].ToString());
            //    sbfile.Append(",");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //string url = string.Format("Approve.aspx?ids={0}&filenames={1}", sb.ToString().TrimEnd(','), sbfile.ToString().TrimEnd(','));
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference(url), "");
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), HiddenWindowForm.GetShowReference("Void.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewVoidTxns(Grid1, HiddenWindowForm);

            List<KeyValue> list = new List<KeyValue>();

            foreach (int row in Grid1.SelectedRowIndexArray)
            {

                list.Add(new KeyValue(Grid1.DataKeys[row][0].ToString(), Grid1.DataKeys[row][1] == null ? "" : Grid1.DataKeys[row][1].ToString()));
            }
            list = (from m in list orderby m.Key ascending select m).ToList();

            foreach (var item in list)
            {

                ControlExtentions.DeleteFileFromServer(item.Value, "MemberImportSegment");

            }

        }


        
        #endregion

        #region 数据列表绑定

        private void RptBind(int pageno)
        {
            try
            {

                DataSet ds = new DataSet();
                ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();


                int count = 0;
                

                ds = dao.GetImportMemberSegments(pageno, 10, this.IStatus.SelectedValue);

                count = int.Parse(ds.Tables[1].Rows[0]["RowCount"].ToString());
                this.Grid1.RecordCount = count;

                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Load ImportMemberSegment", "error", ex);
            }
        }
        //排序
        //private void BindGridWithSort(string sortField, string sortDirection)
        //{
        //    ImportMemberCotroller controller = new ImportMemberCotroller();
        //    int count = 0;
        //    string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
        //    this.SortField.Text = sortFieldStr;
        //    DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
        //    this.RecordCount = count;

        //    DataTable table = ds.Tables[0];

        //    DataView view1 = table.DefaultView;
        //    view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

        //    Grid1.DataSource = view1;
        //    Grid1.DataBind();
        //}
        //protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        //{
        //    BindGridWithSort(e.SortField, e.SortDirection);
        //}
        #endregion

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
          
            //RptBind("ImportMemberSegmentNumber<>''", "ImportMemberSegmentNumber");
            RptBind(Grid1.PageIndex + 1);
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            //RptBind("ImportMemberSegmentNumber<>''", "ImportMemberSegmentNumber");

            
        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            DataRowView drv = e.DataItem as DataRowView;
            if (drv == null) return;

            string approveStatus = drv["ApproveStatusName"].ToString().Trim();
            approveStatus = string.IsNullOrEmpty(approveStatus) ? approveStatus : approveStatus.Substring(0, 1).ToUpper().Trim();

            string couponNumber = drv["ImportMemberSegmentNumber"].ToString().Trim();

            DateTime? createTime = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<DateTime>(drv["CreatedOn"].ToString().Trim());
            string strCreateDate = createTime.ToString();

            FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;
            FineUI.WindowField viewWF = Grid1.FindColumn("ViewWindowField") as FineUI.WindowField;

            switch (approveStatus)
            {
                case "A":
                    editWF.Enabled = false;
                    break;
                case "P":
                    editWF.Enabled = true;
                    drv["ApprovalCode"] = "";
                    break;
                case "V":
                    editWF.Enabled = false;
                    drv["ApprovalCode"] = "";
                    break;
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind(1);
        }

        private void BindGridWithSort(string sortField, string sortDirection)
        {
           
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();
            DataSet ds = dao.GetImportMemberSegments(1, 10, this.IStatus.SelectedValue);//controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);


            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }


        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

    }
}