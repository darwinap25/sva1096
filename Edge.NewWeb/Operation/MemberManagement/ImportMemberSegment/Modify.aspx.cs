﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using System.IO;
using Edge.SVA.Model.Master;
using Edge.Web.DAO;

namespace Edge.Web.Operation.MemberManagement.ImportMemberSegment
{
    public partial class Modify : PageBase
    {
        
        Tools.Logger logger = Tools.Logger.Instance;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ImportMemberCotroller = null;
            }
        
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                string id = Server.UrlDecode(Request.QueryString["id"]);
                string createdon = Server.UrlDecode(Request.QueryString["date"]);
                string statusname = Server.UrlDecode(Request.QueryString["statusname"]);
                string busdate = Server.UrlDecode(Request.QueryString["busdate"]);
                string filename = Server.UrlDecode(Request.QueryString["filename"]);

                this.ImportMemberSegmentNumber.Text = id;
                this.ApproveStatus.Text = statusname;
                this.CreatedOn.Text = createdon;
                this.CreatedBusDate.Text = busdate;


                this.uploadFilePath.Text = filename;
                //    //存在文件时不需要验证此字段
                if (!string.IsNullOrEmpty(filename))
                {
                    this.FormLoad.Hidden = true;
                    this.FormReLoad.Hidden = false;
                    this.btnBack.Hidden = false;
                }
                else
                {
                    this.FormLoad.Hidden = false;
                    this.FormReLoad.Hidden = true;
                    this.btnBack.Hidden = true;
                }
            }
        }

    

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            if (string.IsNullOrEmpty(this.Description.FileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            Ord_ImportMemberSegment_H vo = new Ord_ImportMemberSegment_H();
            ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();

            vo.ImportMemberSegmentNumber = this.ImportMemberSegmentNumber.Text;            
            vo.Note = this.Note.Text;
            vo.FileName = this.Description.FileName;
            vo.UpdatedOn = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            vo.UpdatedBy = Tools.DALTool.GetCurrentUser().UserName;
            vo.Action = this.SegmentAction.SelectedValue;
   
            //vo.CreatedBusDate = DateTime.Parse(this.CreatedBusDate.Text);

          
            if (!string.IsNullOrEmpty(this.Description.ShortFileName) && this.FormLoad.Hidden == false)
            {
                if (!ValidateFile(this.Description.FileName))
                {
                    return;
                }
                vo.Description = Description.SaveAttachFileToServer("MemberImportSegment");
            }
            else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                if (!ValidateFile(this.uploadFilePath.Text))
                {
                    return;
                }
               
                vo.Description = this.uploadFilePath.Text;
            }

            if (!ValidData()) return;

            string result = dao.UpdateImportMemberSegmentHeader(vo);

            if (result == "0")
     
            {
                if (this.FormReLoad.Hidden == true && string.IsNullOrEmpty(vo.Description)) //(controller.ViewModel.MainTable.Description))
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Update\t Code:" + vo.ImportMemberSegmentNumber);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Update\t Code:" + vo == null ? "No Data" : vo.ImportMemberSegmentNumber);
                ShowUpdateFailed();
            }

        }

        private bool ValidData()
        {
            return true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("ImportMember download ", " filename: " + fileName);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteErrorLog("ImportMember download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.MemberInfoFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
    }
}