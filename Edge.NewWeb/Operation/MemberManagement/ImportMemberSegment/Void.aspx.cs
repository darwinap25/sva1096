﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using Edge.SVA.Model.Master;
using Edge.Web.DAO;


namespace Edge.Web.Operation.MemberManagement.ImportMemberSegment
{
    public partial class Void : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                try
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];

                    if (string.IsNullOrEmpty(ids))
                    {
                        this.ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");
                    
                    string resultMsg = BatchVoidCoupon(idList);     
                                        

                    Logger.Instance.WriteOperationLog(this.PageName, "Batch Void Member Segment:" + resultMsg);

                    Alert.ShowInTop(resultMsg, "", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                    
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Batch Void Member Segment:" + ex);
                    Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }
            }
        }

        private string BatchVoidCoupon(List<string> idList)
        {
            int success = 0;
            int count = 0;
            count++;


            for (int i = 0; i < idList.Count; i++)
            {
                Ord_ImportMemberSegment_H vo = new Ord_ImportMemberSegment_H();
                ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();

                vo.ImportMemberSegmentNumber = idList[i];
                vo.ApproveBy = Tools.DALTool.GetCurrentUser().UserName;
                vo.ApproveBusDate = DateTime.Parse(DALTool.GetBusinessDate());

                string result = dao.VoidImportMemberSegmentHeader(vo);

                if (result == "0")
                {
                    success++;
                }
            }
            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }


        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            Response.Redirect("List.aspx");
        }
    }
}