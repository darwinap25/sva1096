﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;
using Edge.Web.DAO;
using Edge.SVA.Model.Master;

namespace Edge.Web.Operation.MemberManagement.ImportMemberSegment
{
    public partial class Add : PageBase
    {
        //Tools.Logger logger = Tools.Logger.Instance;
        //ImportMemberCotroller controller;
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //初始化默认值
                InitData();
         
            }
         
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            if (!ValidData()) return;
            if (!ValidateFile(this.Description.FileName))
            {
                return;
            }
            

            Ord_ImportMemberSegment_H vo = new Ord_ImportMemberSegment_H();
            ImportMemberSegmentDAO dao = new ImportMemberSegmentDAO();

            vo.ImportMemberSegmentNumber = this.ImportMemberSegmentNumber.Text;
            vo.Description = Description.SaveAttachFileToServer("MemberImportSegment");
            vo.FileName = Path.GetFileName(vo.Description); //this.Description.FileName;
            vo.Note = this.Note.Text;
            vo.ApproveStatus = this.ApproveStatus.Text.Substring(0,1);
            vo.CreatedOn = DateTime.Parse(this.CreatedOn.Text);
            vo.CreatedBy = this.CreatedByName.Text;
            vo.CreatedBusDate = DateTime.Parse(this.CreatedBusDate.Text);
            vo.Action = this.SegmentAction.SelectedValue;           

            string result = dao.AddImportMemberSegmentHeader(vo).ToString();
            if (result == "0") //success
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Segment Success Code:" + this.ImportMemberSegmentNumber.Text);
                CloseAndRefresh();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Segment Failed Code:" +  "No Data:" +  this.ImportMemberSegmentNumber.Text);
                ShowAddFailed();
            }
        }

        private bool ValidData()
        {
            if (string.IsNullOrEmpty(this.Description.FileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return false;
            }
            return true;
        }

        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                //if (!webset.MemberInfoFileType.ToLower().Split('|').Contains(filename))
                //{
                //    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                //    return false;
                //}
                
                if(!webset.MemberInfoFileTypeCSV.ToLower().Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                    return false;
                }

            }
            return true;
        }

        private void InitData()
        {
            this.ImportMemberSegmentNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.MemberImportSegment);
            this.CreatedBusDate.Text = DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
            this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;
        }

        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }
    }
}