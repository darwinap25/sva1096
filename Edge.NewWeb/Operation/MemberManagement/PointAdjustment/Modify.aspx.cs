﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.Web.Tools;
using System.IO;
using Edge.Web.DAO;
using FineUI;

namespace Edge.Web.Operation.MemberManagement.PointAdjustment
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.Ord_ImportMember_H, Edge.SVA.Model.Ord_ImportMember_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ImportMemberCotroller controller;

        string uploadPath = @"\UploadFiles\PointsAdjustment\";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ImportMemberCotroller = null;
            }
            controller = SVASessionInfo.ImportMemberCotroller;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.ImportMemberNumber);
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);
                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.Description;
                    //存在文件时不需要验证此字段
                    if (!string.IsNullOrEmpty(controller.ViewModel.MainTable.Description))
                    {
                        this.FormLoad.Hidden = true;
                        this.FormReLoad.Hidden = false;
                        this.btnBack.Hidden = false;
                    }
                    else
                    {
                        this.FormLoad.Hidden = false;
                        this.FormReLoad.Hidden = true;
                        this.btnBack.Hidden = true;
                    }
                }
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");

            PointsAdjustmentConf conf = new PointsAdjustmentConf();

            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            if (string.IsNullOrEmpty(this.Description.FileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }

            controller.ViewModel.MainTable = this.GetUpdateObject();

            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedBy = DALTool.GetCurrentUser().UserID;
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;


                if (!string.IsNullOrEmpty(this.Description.ShortFileName) && this.FormLoad.Hidden == false)
                {
                    if (!ValidateFile(this.Description.FileName))
                    {
                        return;
                    }
                    //controller.ViewModel.MainTable.Description = this.Description.SaveToServer("PointsAdjustment"); 

                    string test_fromRoot = @"C:\Users\roche.santiago\Desktop\1096_SVA\Edge.NewWeb";
                    string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);
                    string uploadPath_server = @"UploadFiles\PointsAdjustment\";
                    string existingFile = Request.Params["filename"];

                    
                    string fullpath_local = test_fromRoot + uploadPath + existingFile;
                    string fullpath_server = testUpdloadFile + uploadPath_server + existingFile;


                    if (System.IO.File.Exists(fullpath_local))
                    {
                        DeleteFilePA(fullpath_local);
                        controller.ViewModel.MainTable.Description = this.Description.SaveAttachFileToServer("PointsAdjustment");
                    }
                    else
                    {
                        controller.ViewModel.MainTable.Description = this.Description.SaveAttachFileToServer("PointsAdjustment");
                    }


                }
                else if (this.FormReLoad.Hidden == false && !string.IsNullOrEmpty(this.uploadFilePath.Text))
                {
                    if (!ValidateFile(this.uploadFilePath.Text))
                    {
                        return;
                    }
                    controller.ViewModel.MainTable.Description = this.uploadFilePath.Text;
                }
            }
            if (!ValidData()) return;

            ExecResult er = controller.Submit();
            if (er.Success)
            {
                string description = controller.ViewModel.MainTable.Description;
                string realFileName = Path.GetFileName(controller.ViewModel.MainTable.Description);

                conf.UpdateFileStatusToReset(description, realFileName);

                if (this.FormReLoad.Hidden == true && string.IsNullOrEmpty(controller.ViewModel.MainTable.Description))
                {
                    DeleteFile(this.uploadFilePath.Text);
                }
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Update\t Code:" + controller.ViewModel.MainTable.ImportMemberNumber);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Update\t Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ImportMemberNumber);
                ShowUpdateFailed();
            }

        }


        public void actionToCancel()
        {
            Alert.ShowInTop("Import file doesnt exist in the folder. Please reupload.", MessageBoxIcon.Warning);
        }


        private bool ValidData()
        {
            return true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("ImportMember download ", " filename: " + fileName);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteErrorLog("ImportMember download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }

        protected void btnReUpLoad_Click(object sender, EventArgs e)
        {
            this.FormLoad.Hidden = false;
            this.FormReLoad.Hidden = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.uploadFilePath.Text))
            {
                this.FormLoad.Hidden = true;
                this.FormReLoad.Hidden = false;
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.MemberInfoFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }

        private void DeleteFilePA(string _filepath)
        {
            if (string.IsNullOrEmpty(_filepath))
            {
                return;
            }
            //string fullpath = Edge.Common.Utils.GetMapPath(_filepath);
            string fullpath = _filepath;

            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);

            }
        }

    }
}