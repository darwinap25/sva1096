﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.PointAdjustment.Show" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server" />
    <ext:Panel ID="Panel1" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px"
        EnableBackgroundColor="true" Title="" AutoScroll="true" Layout="Form">
        <Toolbars>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="btnClose" Icon="SystemClose" EnablePostBack="false" runat="server"
                        Text="关闭">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </Toolbars>


        <Items>
            <ext:SimpleForm ID="SimpleForm1" runat="server" ShowBorder="false" ShowHeader="false" Title=""
                    EnableBackgroundColor="true" LabelAlign="Right">
                <Items>
                    <ext:Form ID="Form13" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow1" runat="server">
                                <Items>
                                    <ext:Label ID="ImportMemberNumber" runat="server" Label="交易编号：">
                                    </ext:Label>
                                    <ext:Label ID="ApprovalCode" runat="server" Label="授权号：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form2" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow2" runat="server">
                                <Items>
                                    <ext:Label ID="ApproveStatus" runat="server" Label="交易状态：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveBusDate" runat="server" Label="交易批核工作日期：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form3" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow3" runat="server">
                                <Items>
                                    <ext:Label ID="CreatedBusDate" runat="server" Label="交易创建工作日期：">
                                    </ext:Label>
                                    <ext:Label ID="ApproveOn" runat="server" Label="批核时间：">
                                    </ext:Label>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form4" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow4" runat="server">
                            <Items>
                                <ext:Label ID="CreatedOn" runat="server" Label="交易创建时间：">
                                </ext:Label>
                                <ext:Label ID="ApproveBy" runat="server" Label="批核人：">
                                </ext:Label>
                            </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form5" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow5" runat="server">
                            <Items>
                                <ext:Label ID="CreatedBy" runat="server" Label="创建人：">
                                </ext:Label>
                                <ext:Label ID="Note" runat="server" Label="Remarks：">
                                </ext:Label>
                            </Items>                   
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>
                    <ext:Form ID="Form6" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow6" runat="server" ColumnWidths="0% 80% 20%">
                                <Items>
                                    <ext:Label ID="uploadFilePath" Hidden="true" Text="" runat="server">
                                    </ext:Label>
                                    <ext:Label ID="Description" Text="" runat="server" Label="导入文件：">
                                    </ext:Label>
                                    <ext:Button ID="btnExportCSV" runat="server" Text="Download CSV" OnClick="btnExportCSV_Click" Icon="PageExcel"
                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                    </ext:Button>
                                    
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>

                    <ext:Form ID="Form7" runat="server" ShowBorder="false" ShowHeader="false" EnableBackgroundColor="true">
                        <Rows>
                            <ext:FormRow ID="FormRow8" runat="server" ColumnWidths="50% 50%">
                                <Items>
                                    <ext:Label ID="dummy" Text="Export XML files : " runat="server">
                                    </ext:Label>
                                    <ext:Button ID="btnExport" runat="server" Text="Download XMLs" OnClick="btnExport_Click" Icon="PageExcel"
                                        EnableAjax="false" DisableControlBeforePostBack="false">
                                    </ext:Button>
                                </Items>
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>



                    <ext:Form ID="FormFiles" ShowBorder="false" ShowHeader="false" Title="" EnableBackgroundColor="true"
                        runat="server" BodyPadding="10px" AutoHeight="true">
                        <Rows>
                            <ext:FormRow ID="FormRow7" ColumnWidths="20% 80%" runat="server">
                                <Items>
                                    <ext:Tree runat="server" ID="FileTree" Title="File List" OnNodeCheck="NodeCheck" >
                                    </ext:Tree>
                                </Items>
                                
                            </ext:FormRow>
                        </Rows>
                    </ext:Form>


                    
                                
                </Items>
            </ext:SimpleForm>


            
            <ext:Grid ID="Grid1" ShowBorder="false" ShowHeader="false" AutoHeight="true" PageSize="3"
                        runat="server" EnableCheckBoxSelect="False" DataKeyNames="ImportMemberNumber"
                        AllowPaging="true" IsDatabasePaging="true" EnableRowNumber="True" AutoWidth="true" AutoExpandColumn="true"
                        ForceFitAllTime="true">

                                <Columns>
                            <ext:TemplateField Width="60px" HeaderText="Retailer ID" SortField="ImportMemberNumber">
                                <ItemTemplate>
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("RetailerID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Store ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCardNumber" runat="server" Text='<%#Eval("StoreID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Card ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblExceptionDetails" runat="server" Text='<%#Eval("CardID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField> 
                            <ext:TemplateField Width="60px" HeaderText="Business Date">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("BusinessDate")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Trans ID">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("TransID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            <ext:TemplateField Width="60px" HeaderText="Acc ID">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("AccID")%>'></asp:Label>
                                </ItemTemplate>
                            </ext:TemplateField>
                            
                            
                        </Columns>
                    </ext:Grid>


        </Items>
    </ext:Panel>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
