﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using System.IO;
using Edge.Web.DAO;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.PointAdjustment
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_ImportMember_H, SVA.Model.Ord_ImportMember_H>
    {
        public static string XMLDownloadPath; //Add By Roche
        public static string PointsXMLDownloadPath; //Add By Roche

        PointsAdjustmentConf pc = new PointsAdjustmentConf();

        ImportMemberCotroller controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                SVASessionInfo.ImportMemberCotroller = null;
            }
            controller = SVASessionInfo.ImportMemberCotroller;

            RptBind(Request.Params["id"].ToString());

            string testID = Request.Params["id"].ToString();



            bool checkImportStatus = pc.CheckTxnStatus(testID);

            if (checkImportStatus)
            {
                btnExportCSV.Enabled = false;
            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {

            //changed by Roche

            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                controller.LoadViewModel(Model.ImportMemberNumber);
                this.CreatedBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                this.ApproveBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);

                if (controller.ViewModel.MainTable.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.ApproveOn.GetValueOrDefault());
                }
                else if (controller.ViewModel.MainTable.ApproveStatus == "P")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                if (controller.ViewModel.MainTable != null)
                {
                    this.uploadFilePath.Text = controller.ViewModel.MainTable.Description;

                    this.btnExport.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.Description) ? true : false;//没有文件时不显示查看按钮(Len)

                    XMLDownloadPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["XMLDownloadPath"]) + controller.ViewModel.MainTable.ImportMemberNumber.ToString();//Add By Robin 2015-08-31
                    PointsXMLDownloadPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PointsXMLDownloadPath"]) + controller.ViewModel.MainTable.ImportMemberNumber.ToString();//Add By Darwin 2016-08-11

                    string[] downloadlist = GetAllFiles(XMLDownloadPath);
                    string[] pointsdownloadlist = GetAllFiles(PointsXMLDownloadPath);

                    int i = 0;
                    foreach (string filename in downloadlist)
                    {
                        i++;
                        FineUI.TreeNode fileNode = new FineUI.TreeNode();
                        fileNode.EnableCheckBox = true;
                        fileNode.AutoPostBack = true;
                        fileNode.NodeID = i.ToString(); ;
                        fileNode.Text = filename;
                        FileTree.Nodes.Add(fileNode);
                    }

                    // i = 0;
                    foreach (string filename in pointsdownloadlist)
                    {
                        i++;
                        FineUI.TreeNode fileNode1 = new FineUI.TreeNode();
                        fileNode1.EnableCheckBox = true;
                        fileNode1.AutoPostBack = true;
                        fileNode1.NodeID = i.ToString(); ;
                        fileNode1.Text = filename;
                        FileTree.Nodes.Add(fileNode1);
                    }

                    if (!string.IsNullOrEmpty(Request.Params["filename"]))
                    {
                        this.Description.Text = Request.Params["filename"];
                    }
                }
                RptBind(Request.Params["id"].ToString());
            }

            //END



            //base.OnLoadComplete(e);

            //if (!this.IsPostBack)
            //{
            //    if (!hasRight)
            //    {
            //        return;
            //    }
            //    controller.LoadViewModel(Model.ImportMemberNumber);
            //    this.CreatedBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
            //    this.ApproveBy.Text = Tools.DALTool.GetUserName(controller.ViewModel.MainTable.ApproveBy.GetValueOrDefault());
            //    this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.CreatedOn.GetValueOrDefault());
            //    this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);

            //    if (controller.ViewModel.MainTable.ApproveStatus == "A")
            //    {
            //        this.ApproveOn.Text = ConvertTool.ToStringDateTime(controller.ViewModel.MainTable.ApproveOn.GetValueOrDefault());
            //    }
            //    else if (controller.ViewModel.MainTable.ApproveStatus == "P")
            //    {
            //        this.ApproveOn.Text = null;
            //        this.ApprovalCode.Text = null;
            //    }

            //    if (controller.ViewModel.MainTable != null)
            //    {
            //        this.uploadFilePath.Text = controller.ViewModel.MainTable.Description;

            //        this.btnExport.Hidden = string.IsNullOrEmpty(controller.ViewModel.MainTable.Description) ? true : false;//没有文件时不显示查看按钮(Len)

            //        if (!string.IsNullOrEmpty(Request.Params["filename"]))
            //        {
            //            this.Description.Text = Request.Params["filename"];
            //        }
            //    }
            //}

        }

        public void RptBind(string transactionnumber)
        {
            PointsAdjustmentImportErrorsDAO dao = new PointsAdjustmentImportErrorsDAO();

            Grid1.DataSource = dao.getPointsAdjustmentImportErrors(transactionnumber);
            Grid1.DataBind();

        }


        protected void NodeCheck(object sender, FineUI.TreeCheckEventArgs e)
        {
            foreach (FineUI.TreeNode fileNode in FileTree.GetCheckedNodes())
            {
                if (fileNode.NodeID != e.NodeID)
                {
                    fileNode.Checked = false;
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            foreach (FineUI.TreeNode fileNode in FileTree.GetCheckedNodes())
            {
                if (fileNode.Text.Substring(0, 1) == "C")
                {
                    ExportFile(fileNode.Text);
                }
                else if (fileNode.Text.Substring(0, 1) == "R")
                {
                    ExportFile2(fileNode.Text);
                }
            }
            FileTree.UncheckAllNodes();

            //string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            //try
            //{
            //    //以XML的形式呈现
            //    if (controller.ViewModel.MainTable.ApproveStatus == "A")
            //    {
            //        string xmlfilename = "CustomerImport-" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + "-01.xml";
            //        Tools.ExportTool.ExportFile(fileName.Replace(".csv", ".xml"), xmlfilename);
            //    }
            //    else
            //    {
            //        Tools.ExportTool.ExportFile(fileName);
            //    }
            //    Tools.Logger.Instance.WriteOperationLog("ImportMemberInfos download ", " filename: " + fileName);
            //}
            //catch (Exception ex)
            //{
            //    string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
            //    Tools.Logger.Instance.WriteErrorLog("ImportMemberInfos download ", " filename: " + fileName, ex);
            //    ShowWarning(ex.Message);
            //}
        }



        private void ExportFile(string filename)
        {
            string FileName = XMLDownloadPath + "\\" + filename;
            FileInfo file = new FileInfo(Server.MapPath(FileName));
            Response.AddHeader("content-type", "text/xml;");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlPathEncode(filename));
            Response.AddHeader("content-length", file.Length.ToString());
            Response.TransmitFile(FileName);
            Response.End();
        }

        private void ExportFile2(string filename)
        {
            string FileName = PointsXMLDownloadPath + "\\" + filename;
            FileInfo file = new FileInfo(Server.MapPath(FileName));
            Response.AddHeader("content-type", "text/xml;");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlPathEncode(filename));
            Response.AddHeader("content-length", file.Length.ToString());
            Response.TransmitFile(FileName);
            Response.End();
        }

        public string[] GetAllFiles(string DownloadPath)
        {
            string dirPath = HttpContext.Current.Server.MapPath(DownloadPath); // for server
            //string dirPath = DownloadPath; // for test local

            if (Directory.Exists(dirPath))
            {
                //获得目录信息
                DirectoryInfo dir = new DirectoryInfo(dirPath);
                //获得目录File List
                FileInfo[] files = dir.GetFiles("*.*");
                string[] fileNames = new string[files.Length];
                int i = 0;
                foreach (FileInfo fileInfo in files)
                {
                    fileNames[i] = fileInfo.Name;
                    i++;
                }
                return fileNames;
            }
            else
                return new string[0];
        }





        public void btnExportCSV_Click(object sender, EventArgs e)
        {
            //ExportCSV
            string fileName = Server.MapPath("~" + this.uploadFilePath.Text);
            try
            {
                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteOperationLog("ImportMember download ", " filename: " + fileName);
            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteErrorLog("ImportMember download ", " filename: " + fileName, ex);
                ShowWarning(ex.Message);
            }

        }


    }
}