﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Text;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.IO;
using System.Data;
using System.Data.Common;
using FineUI;
using System.Data.OleDb;
using Edge.Common;
using Common;

namespace Edge.Web.Operation.MemberManagement.PointAdjustment
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.Ord_ImportMember_H, Edge.SVA.Model.Ord_ImportMember_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        ImportMemberCotroller controller;

        List<string> list = new List<string>(); //
        List<string> valColumns = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                //初始化默认值
                InitData();
                SVASessionInfo.ImportMemberCotroller = null;
            }
            controller = SVASessionInfo.ImportMemberCotroller;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Add ");

            if (!ValidData()) return;

            controller.ViewModel.MainTable = this.GetAddObject();

            if (controller.ViewModel.MainTable != null)
            {
                //校验文件类型是否正确
                if (!ValidateFile(this.Description.FileName))
                {
                    return;
                }

                logger.WriteOperationLog(this.PageName, " Getting file details... ");

                //保存附件到数据库
                controller.ViewModel.MainTable.Description = Description.SaveAttachFileToServer("PointsAdjustment"); //("ImportMemberInfos");
                controller.ViewModel.MainTable.FileName = Path.GetFileName(controller.ViewModel.MainTable.Description);
                controller.ViewModel.MainTable.ImportStatus = "P";
                controller.ViewModel.MainTable.CreatedBy = Tools.DALTool.GetCurrentUser().UserID;

                

                //added by Darwin as per #1096 SA&D 2.3.1.2

                //string columnValidationResult = ValidateColumns(controller.ViewModel.MainTable.Description);
                //if (!string.IsNullOrEmpty(columnValidationResult))
                //{
                //    //delete file from server if validation of columns failed
                //    DeleteFile(controller.ViewModel.MainTable.Description);
                //    ShowWarning(columnValidationResult);

                //}
                //else
                //{
                //    ExecResult er = controller.Submit();
                //    if (er.Success)
                //    {
                //        Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Success Code:" + controller.ViewModel.MainTable.ImportMemberNumber);
                //        CloseAndRefresh();
                //    }
                //    else
                //    {
                //        Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ImportMemberNumber.ToString());

                //        //delete file from server if add record failed
                //        DeleteFile(controller.ViewModel.MainTable.Description);
                //        ShowAddFailed();
                //    }
                //}


                //Modified by Roche as per #1096 SA&D v1.02
                logger.WriteOperationLog(this.PageName, " Start of validations... ");

                string columnValidationResult = ValidateFileName(controller.ViewModel.MainTable.FileName);

                logger.WriteOperationLog(this.PageName, " Evaluate if all validations are success... ");

                logger.WriteOperationLog(this.PageName, " Column validation result: " + columnValidationResult);

                try
                {
                    if (!string.IsNullOrEmpty(columnValidationResult))
                    {
                        //delete file from server if validation of columns failed
                        string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);
                        string filename = controller.ViewModel.MainTable.Description;

                        if (Path.GetExtension(filename) == ".csv")
                        {
                            DeleteFilePA(controller.ViewModel.MainTable.Description);
                            logger.WriteOperationLog(this.PageName, " Show warning: " + columnValidationResult);
                            Alert.ShowInTop(columnValidationResult, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            logger.WriteOperationLog(this.PageName, " Show warning: " + columnValidationResult);
                            Alert.ShowInTop(columnValidationResult, MessageBoxIcon.Warning);
                        }

                        //logger.WriteOperationLog(this.PageName, " Show warning: " + columnValidationResult);
                        //Alert.ShowInTop(columnValidationResult, MessageBoxIcon.Warning);
                        ////ShowWarning(columnValidationResult);
                        
                    }
                    else
                    {
                        ExecResult er = controller.Submit();
                        if (er.Success)
                        {

                            Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Success Code:" + controller.ViewModel.MainTable.ImportMemberNumber);

                            CloseAndRefresh();
                        }
                        else
                        {
                            Tools.Logger.Instance.WriteOperationLog(this.PageName, "Import Member Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.ImportMemberNumber.ToString());

                            string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);
                            string filename = controller.ViewModel.MainTable.Description;

                            //DeleteFile(testUpdloadFile + filename);
                            //delete file from server if add record failed
                            DeleteFilePA(controller.ViewModel.MainTable.Description);
                            ShowAddFailed();
                        }
                    }

                }
                catch (Exception ex)
                {
                    logger.WriteOperationLog(this.PageName, " Show warning: " + ex.ToString());
                }


            }


        }

        private void InitData()
        {
            this.ImportMemberNumber.Text = DALTool.GetREFNOCode("PA");
            this.CreatedBusDate.Text = DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);
            this.CreatedOn.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.CreatedByName.Text = Tools.DALTool.GetCurrentUser().UserName;
        }

        private bool ValidData()
        {
            if (string.IsNullOrEmpty(this.Description.FileName))
            {
                ShowWarning(Resources.MessageTips.NoData);
                return false;
            }
            return true;
        }

        private StringBuilder GetHtml(DateTime begin)
        {
            StringBuilder html = new StringBuilder(200);

            SVASessionInfo.MessageHTML = html.ToString();

            return html;
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            if (this.ExcuteReslut.Text.ToLower() == "true")
            {
                CloseAndPostBack();
            }
        }

        //校验文件是否为允许类型
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.MemberInfoFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning("Error: Invalid file type, please change the file type to CSV, XLS, XLSX");
                    //ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.MemberInfoFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }


        //added by Roche as per #1096 SA&D v1.02

        public string ValidateFileName(string filename)
        {
            logger.WriteOperationLog(this.PageName, " Validating file name ");

            string columnValidationResult;
            if (!string.IsNullOrEmpty(filename))
            {
                return columnValidationResult = ValidateColumns(controller.ViewModel.MainTable.Description);
                
            }
            else
            {
                return "No existing filename";
            }
        }


        //added by Darwin as per #1096 SA&D 2.3.1.2
        //Check for existence of columns in the file

        //added by Roche 1096

        private string ValidateColumns(string filename)
        {
            logger.WriteOperationLog(this.PageName, " Validating columns... ");

            string test = filename.Substring(filename.LastIndexOf("/") + 1);

            string fname = test.Substring(0, 2);
            if (fname == "R_")
            {
                return ValidateColumnsWithR_(filename);
            }
            else
            {
                return ValidateColumnsWithoutR_(filename);
            }

        }


        private string ValidateColumnsWithoutR_(string filename)
        {
            logger.WriteOperationLog(this.PageName, " Validating columns... ");

            try
            {
                string fnReturnValue = "";

                //List<string> list = new List<string>(); //

                list.Add("RetailerId");
                list.Add("MsgType");
                list.Add("LPEVer");
                list.Add("StoreID");
                list.Add("PosID");
                list.Add("CashierID");
                list.Add("TicketTotal");
                list.Add("HomeStore");
                list.Add("FileSource");
                list.Add("BusinessDate");
                list.Add("StartDateTime");
                list.Add("TransID");
                list.Add("CardID");
                list.Add("ServerDate");
                list.Add("Acc ID");
                list.Add("EarnValue");


                logger.WriteOperationLog(this.PageName, " Getting file path:  " + Path.GetExtension(filename));

                string testFilename_filetype = Path.GetExtension(filename);

                DataTable dt = new DataTable();
                if (testFilename_filetype == ".csv")
                {
                    CSVFileHelper cf = new CSVFileHelper();

                    logger.WriteOperationLog(this.PageName, " From this path:  " + filename);

                    string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

                    string testpath = Server.MapPath(filename);

                    logger.WriteOperationLog(this.PageName, " From this path concatenated" + testUpdloadFile + filename);

                    try
                    {
                        dt = cf.OpenCSV(testUpdloadFile + filename);
                    }
                    catch (Exception ex)
                    {
                        dt = cf.OpenCSV(Server.MapPath(filename));
                    }
                }
                else if (testFilename_filetype == ".xlsx" || testFilename_filetype == ".xls")
                {
                    try
                    {
                        string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

                        try
                        {
                            dt = XLSHelper.OpenExcel(testUpdloadFile + filename);
                        }
                        catch (Exception ex)
                        {
                            dt = XLSHelper.OpenExcel(Server.MapPath(filename));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.WriteOperationLog(this.PageName, " Importing XLS or XLSX file:  " + ex.ToString());
                        throw ex;
                    }
                }

                List<string> validatedColumns = new List<string>();

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName.ToString().Contains("\"")) //check if contains qoute
                    {
                        validatedColumns.Add(dt.Columns[i].ColumnName.ToString().Replace("\"", ""));
                    }
                    else
                    {
                        validatedColumns.Add(dt.Columns[i].ColumnName.ToString());
                    }

                }


                logger.WriteOperationLog(this.PageName, " Starting of exception... ");
                try
                {
                    logger.WriteOperationLog(this.PageName, " Checking if columns in CSV are exist in the required fields ");
                    fnReturnValue = CheckColumnIfExistWithoutR_(validatedColumns, list);

                }
                catch (Exception ex)
                {
                    logger.WriteOperationLog(this.PageName, " Ending exception: " + ex.ToString());

                    fnReturnValue = ex.Message;

                    //delete file from server if exception occurs
                    DeleteFilePA(controller.ViewModel.MainTable.Description);
                }

                return fnReturnValue;

            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " ValidateColumns :  " + ex.ToString());
                return ex.ToString();
            }
        }

        private string ValidateColumnsWithR_(string filename)
        {
            logger.WriteOperationLog(this.PageName, " Validating columns... ");

            try
            {
                string fnReturnValue = "";

                //List<string> list = new List<string>(); //

                list.Add("RetailerId");
                list.Add("MsgType");
                list.Add("LPEVer");
                list.Add("StoreID");
                list.Add("PosID");
                list.Add("CashierID");
                list.Add("TicketTotal");
                list.Add("HomeStore");
                list.Add("FileSource");
                list.Add("BusinessDate");
                list.Add("StartDateTime");
                list.Add("TransID");
                list.Add("CardID");
                list.Add("ServerDate");
                list.Add("Acc ID");
                list.Add("EarnValue");
                list.Add("RdmValue");
                //string ff = filename.Substring(19);
                //string test = ff.Substring(0, 2);

                string test = filename.Substring(filename.LastIndexOf("/") + 1);


                logger.WriteOperationLog(this.PageName, " Getting file path:  " + Path.GetExtension(filename));

                string testFilename_filetype = Path.GetExtension(filename);

                DataTable dt = new DataTable();
                if (testFilename_filetype == ".csv")
                {
                    CSVFileHelper cf = new CSVFileHelper();

                    logger.WriteOperationLog(this.PageName, " From this path:  " + filename);

                    string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

                    string testpath = Server.MapPath(filename);

                    logger.WriteOperationLog(this.PageName, " From this path concatenated" + testUpdloadFile + filename);

                    try
                    {
                        dt = cf.OpenCSV(testUpdloadFile + filename);
                    }
                    catch (Exception ex)
                    {
                        dt = cf.OpenCSV(Server.MapPath(filename));
                    }
                }
                else if (testFilename_filetype == ".xlsx" || testFilename_filetype == ".xls")
                {
                    try
                    {
                        string testUpdloadFile = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["UploadFiles"]);

                        try
                        {
                            dt = XLSHelper.OpenExcel(testUpdloadFile + filename);
                        }
                        catch (Exception ex)
                        {
                            dt = XLSHelper.OpenExcel(Server.MapPath(filename));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.WriteOperationLog(this.PageName, " Importing XLS or XLSX file:  " + ex.ToString());
                        throw ex;
                    }
                }

                List<string> validatedColumns = new List<string>();

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName.ToString().Contains("\"")) //check if contains qoute
                    {
                        validatedColumns.Add(dt.Columns[i].ColumnName.ToString().Replace("\"", ""));
                    }
                    else
                    {
                        validatedColumns.Add(dt.Columns[i].ColumnName.ToString());
                    }

                }


                logger.WriteOperationLog(this.PageName, " Starting of exception... ");
                try
                {
                    logger.WriteOperationLog(this.PageName, " Checking if columns in CSV are exist in the required fields ");
                    fnReturnValue = CheckColumnsIfExistsWithR_(validatedColumns, list);


                }
                catch (Exception ex)
                {
                    logger.WriteOperationLog(this.PageName, " Ending exception: " + ex.ToString());

                    fnReturnValue = ex.Message;
                    //delete file from server if exception occurs
                    DeleteFilePA(controller.ViewModel.MainTable.Description);
                }

                return fnReturnValue;

            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " ValidateColumns :  " + ex.ToString());
                return ex.ToString();
            }
        }




        private string CheckColumnsIfExistsWithR_(List<string> validatedColumns, List<string> columnsNames)
        {

            string fnReturnValue = "";
            string columnsWithErrors = "";
            logger.WriteOperationLog(this.PageName, " Start of Checking if columns exist");
            try
            {
                if (null != validatedColumns)
                {
                    foreach (string columnName in columnsNames)
                    {
                        if (!validatedColumns.Contains(columnName))
                        {
                            //fnReturnValue = columnName + " does not exist in file! Verify that column names are in correct format.";
                            
                            valColumns.Add(columnName);
                            //break;
                        }
                    }


                    for (int i = 0; i < valColumns.Count; i++)
                    {
                        columnsWithErrors += valColumns[i] + "; ";

                    }


                    if (!string.IsNullOrEmpty(columnsWithErrors))
                    {
                        //fnReturnValue = columnsWithErrors + " does not exist or incorrect in the file! Verify that column names are in correct format.";
                        fnReturnValue = "Error: Invalid File Name or RdmValue column does not exist";
                    }
                    else
                    {
                        fnReturnValue = "";
                    }

                }
                else
                {
                    fnReturnValue = "Erroneous file uploaded!";
                }
            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " Check if column exist: " + ex);
                fnReturnValue = ex.Message;
            }

            return fnReturnValue;
        }



        private string CheckColumnIfExistWithoutR_(List<string> validatedColumns, List<string> columnsNames)
        {
            string fnReturnValue = "";
            string columnsWithErrors = "";
            logger.WriteOperationLog(this.PageName, " Start of Checking if columns exist");
            try
            {
                if (null != validatedColumns)
                {
                    foreach (string columnName in columnsNames)
                    {
                        if (!validatedColumns.Contains(columnName))
                        {
                            //fnReturnValue = columnName + " does not exist in file! Verify that column names are in correct format.";
                            valColumns.Add(columnName);
                            //break;
                        }
                        
                    }
                    if (validatedColumns.Contains("RdmValue"))
                    {
                        valColumns.Add("RdmValue");
                    }

                    for (int i = 0; i < valColumns.Count; i++)
                    {
                        columnsWithErrors += valColumns[i] + "; ";

                    }

                    if (!string.IsNullOrEmpty(columnsWithErrors))
                    {
                        if (valColumns.Contains("RdmValue"))
                        {
                            //fnReturnValue = columnsWithErrors + "must not exist in the file! Verify that column names are in correct format.";
                            fnReturnValue = "Error: Invalid File Name or RdmValue column does not exist";
                        }
                        else
                        {
                            //fnReturnValue = columnsWithErrors + " does not exist or incorrect in the file! Verify that column names are in correct format.";
                            fnReturnValue = "Error: Invalid File Name or RdmValue column does not exist";
                        }
                    }
                    else
                    {
                        fnReturnValue = "";
                    }

                }
                else
                {
                    fnReturnValue = "Erroneous file uploaded!";
                }
            }
            catch (Exception ex)
            {
                logger.WriteOperationLog(this.PageName, " Check if column exist: " + ex);
                fnReturnValue = ex.Message;
            }

            return fnReturnValue;
        }



        private void DeleteFile(string filename)
        {
            string FileToDelete;
            string testingpath = Server.MapPath(filename);
            logger.WriteOperationLog(this.PageName, " Testing delete path: " + testingpath);
            // Set full path to file 
            FileToDelete = Server.MapPath(filename);
            // Delete a file
            System.IO.File.Delete(FileToDelete);
        }


        private void DeleteFilePA(string _filepath)
        {
            if (string.IsNullOrEmpty(_filepath))
            {
                return;
            }
            string fullpath = Edge.Common.Utils.GetMapPath(_filepath);
            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);
            }
        }



    }
}