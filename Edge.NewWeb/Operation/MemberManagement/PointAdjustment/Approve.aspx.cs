﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.Messages.Manager;
using System.Data;
using FineUI;
using Edge.Web.Controllers.Operation.MemberManagement.ImportMemberInfos;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using Edge.Web.DAO;


namespace Edge.Web.Operation.MemberManagement.PointAdjustment 
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //importMemberCotroller controller = new ImportMemberCotroller();
            //PointsAdjustmentConf conf = new PointsAdjustmentConf();


            if (!this.IsPostBack)
            {

                try
                {
                    //updated by henry 20170927
                   btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                   // btnClose.OnClientClick = FineUI.ActiveWindow.GetHideRefreshReference(); //FineUI.ActiveWindow.GetHidePostBackReference();
                    if (!hasRight)
                    {
                        return;
                    }
                    string ids = Request.Params["ids"];
                    string files = Request.Params["filenames"];//?????

                    // string exactfilename = Request.Params["exactfilename"];
                    string exactfilename = Path.GetFileName(files);

                    if (string.IsNullOrEmpty(ids))
                    {
                        ShowWarning(Resources.MessageTips.NotSelected);
                        return;
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TxnNo", typeof(string));
                    dt.Columns.Add("ApproveCode", typeof(string));
                    dt.Columns.Add("ApprovalMsg", typeof(string));


                    //???????HQDB????
                    //commented out

                    //controller.connString = webset.HQDBconnectString;
                    //controller.sqlString = webset.HQEnqueryString;
                    //controller.sqlString2 = webset.HQEnqueryString2;

                    List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");
                    List<string> fileList = Edge.Utils.Tools.StringHelper.SplitString(files, ",");
                    bool isSuccess = false;


                    for (int i = 0; i < idList.Count; i++)
                    {
                        Edge.SVA.Model.Ord_ImportMember_H mode = new Edge.SVA.BLL.Ord_ImportMember_H().GetModel(idList[i]);

                        DataRow dr = dt.NewRow();
                        dr["TxnNo"] = idList[i];


                        //added by Roche
                        if (!string.IsNullOrEmpty(fileList[i]))
                        {
                            string filename = Server.MapPath("~" + fileList[i]);
                            string ImportPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CSVImportPath"]);
                            ImportPath = ImportPath + idList[i].ToString() + "/";
                            ImportPath = Server.MapPath(ImportPath);

                            if (string.IsNullOrEmpty(mode.FileName))
                            {
                                string testFileName = fileList[i].Substring(fileList[i].LastIndexOf('/') + 1);
                                mode.FileName = fileList[i].Substring(fileList[i].LastIndexOf('/') + 1);
                            }

                            if (!Directory.Exists(ImportPath))
                            {
                                Directory.CreateDirectory(ImportPath);
                            }

                            ImportPath = ImportPath + System.IO.Path.GetFileName(filename);


                            if (System.IO.File.Exists(filename))
                            {
                                System.IO.File.Copy(filename, ImportPath, true);
                                System.IO.File.Delete(filename);

                                mode.FileName = Path.GetFileName(fileList[i].ToString());

                                // mode.ImportStatus = "P";
                            }
                        }


                        ////???????CSV?????XML??
                        //cmmented out

                        //if (!string.IsNullOrEmpty(fileList[i]))
                        //{
                        //    string filename = Server.MapPath("~" + fileList[i]);
                        //   if (!controller.ExchangeCSVToXML(filename, idList[i], true))
                        //    {
                        //        dr["ApproveCode"] = "Conversion failure";//??????????
                        //        dt.Rows.Add(dr);
                        //        continue;
                        //    }
                        //}

                        //Logger.Instance.WriteOperationLog(this.PageName, "Approve Points Adjustment" + mode.ImportMemberNumber);

                        string approveCode = Edge.Web.Controllers.CouponController.ApproveCouponForApproveCode(mode, out isSuccess);
                        dr["ApproveCode"] = approveCode;
                        if (isSuccess)
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveCode + " " + approveCode);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                            //conf.updateFileName(ids, exactfilename);
                            mode.ApproveBy = Tools.DALTool.GetCurrentUser().UserID; //UserId approved by. 
                            mode.ApproveStatus = "P";
                        }
                        else
                        {
                            Logger.Instance.WriteOperationLog(this.PageName, "Approve Import Member " + mode.ImportMemberNumber + " " + Resources.MessageTips.ApproveError);

                            dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                        }
                        dt.Rows.Add(dr);
                    }

                    this.Grid1.DataSource = dt;
                    this.Grid1.DataBind();

                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Approve " + ex);
                    //Alert.ShowInTop(Resources.MessageTips.SystemError, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                    Alert.ShowInTop(ex.Message, "", MessageBoxIcon.Error, ActiveWindow.GetHidePostBackReference());
                }




            }

            // SVASessionInfo.CurrentUser.UserName = NameUser;
        }

        //protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        //{
        //    base.WindowEdit_Close(sender, e);
        //}
        //protected void btnClose_Click(object sender, EventArgs e)
        //{
        //    //CloseAndPostBack();
        //    CloseCurrForm();
        //}

    }
}