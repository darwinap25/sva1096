﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Edge.Web.Operation.Constants
{
    [Serializable()]
    public class CheckDigitMode
    {
        public const int EAN13 = 1;
        public const int MOD10 = 2;
    }

    [Serializable()]
    public class IsImportCouponNumber
    {
        public const int YES = 1;
        public const int NO = 0;
    }

    [Serializable()]
    public class IsConsecutiveUID
    {
        public const int YES = 1;
        public const int NO = 0;
        public const int ISNULL = 2;
        
    }

    [Serializable()]
    public class CouponNumbertoUID         
    {
        public const int COPY_ALL = 0;
        public const int BINDING = 1;
        public const int COPY_AND_DELETE_CHECK_DIGIT = 2;
        public const int COPY_AND_ADD_CHECK_DIGIT = 3;

    }

    [Serializable()]
    public class AutoReplenish
    {
        public const int YES = 1;
        public const int NO = 0;
    }


}