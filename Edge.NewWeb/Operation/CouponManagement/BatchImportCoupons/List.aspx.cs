﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Messages.Manager;
using System.Text;
using Edge.Web.Controllers.Operation.CouponManagement.BatchImportCoupons;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class List : PageBase
    {
        private const string fields = " [ImportCouponNumber],[ApprovalCode],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate]";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind("", "ImportCouponNumber");

                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
            }
        }

        #region Approve Voide DataBinding

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ////System.Text.StringBuilder sb = new System.Text.StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            ////Window2.Title = "Approve";
            ////FineUI.PageContext.RegisterStartupScript(Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')));
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewApproveTxns(Grid1, Window2);
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            ////System.Text.StringBuilder sb = new System.Text.StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            ////FineUI.PageContext.Redirect("Void.aspx?ids=" + sb.ToString().TrimEnd(','));
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), HiddenWindowForm.GetShowReference("Void.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            NewVoidTxns(Grid1, HiddenWindowForm);
        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            DataRowView drv = e.DataItem as DataRowView;
            if (drv == null) return;

            string approveStatus = drv["ApproveStatus"].ToString().Trim();
            approveStatus = string.IsNullOrEmpty(approveStatus) ? approveStatus : approveStatus.Substring(0, 1).ToUpper().Trim();

            FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;

            switch (approveStatus)
            {
                case "A":
                    editWF.Enabled = false;
                    break;
                case "P":
                    editWF.Enabled = true;
                    drv["ApprovalCode"] =  "";
                    break;
                case "V":
                    editWF.Enabled = false;
                    drv["ApprovalCode"] = "";
                    break;
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "ImportCouponNumber");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "ImportCouponNumber");
        }
        #endregion

        #region 数据列表绑定

        //private int RecordCount
        //{
        //    get
        //    {
        //        if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
        //        int count = 0;
        //        return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
        //    }
        //    set
        //    {
        //        if (value < 0) return;
        //        if (value > 0)
        //        {
        //            this.btnApprove.Enabled = true;
        //            this.btnVoid.Enabled = true;
        //        }
        //        else
        //        {
        //            this.btnApprove.Enabled = false;
        //            this.btnVoid.Enabled = false;
        //        }
        //        this.Grid1.RecordCount = value;
        //        ViewState["RecordCount"] = value;
        //    }
        //}

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string code = this.Code.Text.Trim();
                    string status = this.Status.SelectedValue.Trim();
                    string CStatrtDate = this.CreateStartDate.Text;
                    string CEndDate = this.CreateEndDate.Text;
                    string AStatrtDate = this.ApproveStartDate.Text;
                    string AEndDate = this.ApproveEndDate.Text;
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" ImportCouponNumber like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveStatus";
                        sb.Append(descLan);
                        sb.Append(" ='");
                        sb.Append(status);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(CStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(CStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(CEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(CEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(AStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(AStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(AEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(AEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                //Edge.SVA.BLL.Ord_ImportCouponUID_H bll = new Edge.SVA.BLL.Ord_ImportCouponUID_H()
                //{
                //    StrWhere = strWhere,
                //    Order = orderby,
                //    Fields = fields,
                //    Ascending = false
                //};
                ////获得总条数
                //this.Grid1.RecordCount = bll.GetCount(strWhere);
                //DataSet ds = new DataSet();
                //ds = bll.GetList(Grid1.PageSize, Grid1.PageIndex, strWhere, orderby);

                //Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
                //Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");
                //Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

                //this.Grid1.DataSource = ds.Tables[0].DefaultView;
                //this.Grid1.DataBind();

                BatchImportCouponsController controller = new BatchImportCouponsController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }

                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Ord_CouponBatchCreate", "Load error", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            BatchImportCouponsController controller = new BatchImportCouponsController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "ImportCouponNumber");
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

    }
}
