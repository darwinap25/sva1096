﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponDelivery
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_CouponDelivery_H, SVA.Model.Ord_CouponDelivery_H>
    {
        private const string fields = "[KeyID],[CouponDeliveryNumber],[CouponTypeID],[Description],[OrderQTY],[PickQTY],[ActualQTY],[FirstCouponNumber],[EndCouponNumber],[BatchCouponCode]";


        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;
                this.Grid1.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);
                RptBind(string.Format("CouponDeliveryNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;

                this.CouponDeliveryNumber.Text = Model.CouponDeliveryNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());

                this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                this.CustomerType.Visible = false;

                this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                this.SendMethod.Visible = false;

                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID.GetValueOrDefault());
                this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                store = new Edge.SVA.BLL.Store().GetModel(Model.FromStoreID == null ? 0 : (int)Model.FromStoreID);
                this.FromStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                this.NeedActiveName.Text = this.NeedActive.SelectedItem.Text;

                string status = Model.ApproveStatus;
                if (status == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                    this.btnPrint.Visible = false;
                   // this.NeedActive.Enabled = false;
                    //this.btnSave.Visible = false;
                }
                else
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                    this.btnPrint.Visible = false;
                    //this.NeedActive.Enabled = true;
                   // this.btnSave.Visible = true;

                    //if (webset.CouponShipmentConfirmationSwitch == 1)
                    //{
                    //    this.NeedActive.Enabled = true;
                    //    this.btnSave.Visible = true;                    
                    //}
                    //else
                    //{
                    //    this.NeedActive.Enabled = false;
                    //    this.btnSave.Visible = false;
                    //}
                }
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            ViewState["CouponTypeCode"] = null;
            ViewState["CouponType"] = null;
            ViewState["OrderQTY"] = null;

            this.Grid1.PageIndex = e.NewPageIndex;

            RptBind(string.Format("CouponDeliveryNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);

        }

        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            //string CouponTypeCode = "";
            //string CouponType = "";
            //string OrderQTY = "";

            if (e.DataItem is DataRowView)
            {
                //显示格式
                Label lblCouponTypeCode = Grid1.Rows[e.RowIndex].FindControl("lblCouponTypeCode") as Label;
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)Grid1.Rows[e.RowIndex].FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblOrderQTY");
                    Label lblSeq = Grid1.Rows[e.RowIndex].FindControl("lblSeq") as Label;
                    HiddenField hfCouponTypeID = Grid1.Rows[e.RowIndex].FindControl("hfCouponTypeID") as HiddenField;
                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                        if (lblSeq != null) { lblSeq.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null) { ViewState["CouponType"] = lblCouponType.Text.Trim(); }
                        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                        if (lblSeq != null) { lblSeq.Text = (this.CouponTypeIndex[int.Parse(hfCouponTypeID.Value)]).ToString(); }
                    }
                }
            }

        }

        //protected void rptListPager_PageChanged(object sender, EventArgs e)
        //{
        //    ViewState["CouponTypeCode"] = null;
        //    ViewState["CouponType"] = null;
        //    ViewState["OrderQTY"] = null;
        //    RptBind(string.Format("CouponDeliveryNumber='{0}'", Request.Params["id"]), "CouponTypeID", fields);
        //}

        //protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    //string CouponTypeCode = "";
        //    //string CouponType = "";
        //    //string OrderQTY = "";


        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        //显示格式
        //        Label lblCouponTypeCode = (Label)e.Item.FindControl("lblCouponTypeCode");
        //        if (lblCouponTypeCode != null)
        //        {
        //            Label lblCouponType = (Label)e.Item.FindControl("lblCouponType");
        //            Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");
        //            Label lblSeq = e.Item.FindControl("lblSeq") as Label;
        //            HiddenField hfCouponTypeID = e.Item.FindControl("hfCouponTypeID") as HiddenField;
        //            //重复
        //            if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
        //            {
        //                lblCouponTypeCode.Visible = false;
        //                if (lblCouponType != null) { lblCouponType.Visible = false; }
        //                if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
        //                if (lblSeq != null) { lblSeq.Visible = false; }
        //            }
        //            else//不重复
        //            {
        //                ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
        //                if (lblCouponType != null) { ViewState["CouponType"] = lblCouponType.Text.Trim(); }
        //                if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
        //                if (lblSeq != null) { lblSeq.Text = (this.CouponTypeIndex[int.Parse(hfCouponTypeID.Value)]).ToString(); }
        //            }
        //        }
               
        //    }
        //}

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Print.aspx?id={0}", Request.Params["id"]));
        }

        #endregion


        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby, string fields)
        {
            Edge.SVA.BLL.Ord_CouponDelivery_D bll = new Edge.SVA.BLL.Ord_CouponDelivery_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex);
            }


            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            DataTool.AddCouponUIDByCouponNumber(ds, "FirstCouponUID", "FirstCouponNumber");
            DataTool.AddCouponUIDByCouponNumber(ds, "EndCouponUID", "EndCouponNumber");

            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();


            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApproveDeliveryTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            lblTotalPickQTY.Text = totalPickQTY.ToString();
        }

        //private void RptBind(string strWhere, string orderby, string fields)
        //{
        //    Edge.SVA.BLL.Ord_CouponDelivery_D bll = new Edge.SVA.BLL.Ord_CouponDelivery_D()
        //    {
        //        StrWhere = strWhere,
        //        Order = orderby,
        //        Fields = fields,
        //        Timeout = 60
        //    };

        //    System.Data.DataSet ds = null;
        //    if (this.RecordCount < 0)
        //    {
        //        int count = 0;
        //        ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
        //        this.RecordCount = count;

        //    }
        //    else
        //    {
        //        ds = bll.GetList(this.rptPager.PageSize, currentPage);
        //    }

        //    Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
        //    Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");

        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();

        //    //统计
        //    long totalOrderQTY = 0;
        //    long totalPickQTY = 0;
        //    Controllers.CouponOrderController.GetApproveDeliveryTotal(Request.Params["id"].Trim(), out totalOrderQTY, out totalPickQTY);
        //    lblTotalOrderQTY.Text = totalOrderQTY.ToString();
        //    lblTotalPickQTY.Text = totalPickQTY.ToString();
        //    //this.lblOrdersCount.Text = totalOrderQTY.ToString();
        //    this.lblOrdersCount.Text = Controllers.CouponOrderController.GetDeliveryOrderTotalQty(Request.Params["id"].Trim()).ToString();
        //}

        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private Dictionary<int, int> CouponTypeIndex
        {
            get
            {
                if (ViewState["CouponTypeIndex"] == null)
                {
                    ViewState["CouponTypeIndex"] = new SVA.BLL.Ord_CouponDelivery_D().GetCouponTypeIndex(Request.Params["id"]);
                }
                return ViewState["CouponTypeIndex"] as Dictionary<int, int>;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SVA.BLL.Ord_CouponDelivery_H bll = new SVA.BLL.Ord_CouponDelivery_H();
            Edge.SVA.Model.Ord_CouponDelivery_H item = bll.GetModel(Request["id"].ToString().Trim());

            if (item != null)
            {
                item.NeedActive = Tools.ConvertTool.ToInt(this.NeedActive.SelectedValue);
                if (bll.Update(item))
                {
                    CloseAndRefresh();
                   // JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
                }
                else
                {
                    ShowUpdateFailed();
                   // JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
                }

            }
            else
            {
                ShowUpdateFailed();
                //JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

    }
}