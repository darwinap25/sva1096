﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class Print : PageBase
    {
        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;

                //RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponPickingNumber");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;

                //string ids = Request.Params["id"];

                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                DataTable orders = new DataTable();
                orders.Columns.Add("CouponPickingNumber", typeof(string));
                orders.Columns.Add("PrintDateTime", typeof(string));
                orders.Columns.Add("ReferenceNo", typeof(string));
                orders.Columns.Add("ApproveStatus", typeof(string));
                orders.Columns.Add("PickingDate", typeof(string));
                orders.Columns.Add("PickedBy", typeof(string));


                //if (Model.ApproveStatus.ToUpper().Trim()!="A")
                //{
                //    JscriptPrint(Resources.MessageTips.YouNotApprove, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                //    return;
                //}

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");
                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_CouponPicking_H mode = new Edge.SVA.BLL.Ord_CouponPicking_H().GetModel(id);
                    if (mode == null) continue;
                    DataRow order = orders.NewRow();
                    order["CouponPickingNumber"] = mode.CouponPickingNumber;
                    order["PrintDateTime"] = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                    order["ReferenceNo"] = mode.ReferenceNo;
                    order["ApproveStatus"] = Tools.DALTool.GetOrderPickingApproveStatusString(mode.ApproveStatus);
                    order["PickingDate"] = Tools.ConvertTool.ToStringDate(mode.ApproveOn.GetValueOrDefault());
                    order["PickedBy"] = Tools.DALTool.GetUserName(mode.ApproveBy.GetValueOrDefault());

                    orders.Rows.Add(order);

                    this.rptOrders.DataSource = orders;
                    this.rptOrders.DataBind();
                }
                //this.lblCouponPickingNumber.Text = this.Model.CouponPickingNumber;
                //this.lblApproveStatus.Text = Tools.DALTool.GetOrderPickingApproveStatusString(Model.ApproveStatus);
                //this.lblPickedBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                //this.lblPickingDate.Text = Tools.ConvertTool.ToStringDate(Model.ApproveOn.GetValueOrDefault());
                //this.lblPrintDateTime.Text = Tools.ConvertTool.ToStringDateTime(System.DateTime.Now);
                //this.lblReferenceNo.Text = Model.ReferenceNo;
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
           // RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponPickingNumber");
        }
        private int seq = 0;
        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCouponTypeCode = (Label)e.Item.FindControl("lblCouponTypeCode");
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)e.Item.FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");

                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null)
                        {
                            ViewState["CouponType"] = lblCouponType.Text.Trim();                         
                        }
                        if (lblOrderQTY != null)
                        {
                            ViewState["OrderQTY"] = lblOrderQTY.Text.Trim();                             
                            //统计数量
                            ViewState["TotalOrderQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderQTY.Text.Trim());
                        }
                        ((Label)e.Item.FindControl("lblSeq")).Text = (++seq).ToString();
                    }
                }
               
                Label lblPickQTY = (Label)e.Item.FindControl("lblPickQTY");
                if (lblPickQTY != null)
                {
                    //统计数量
                    ViewState["TotalPickQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblPickQTY.Text.Trim());
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                if (lblTotalOrderQTY != null)
                {
                    lblTotalOrderQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()).ToString();
                }
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                if (lblTotalPickQTY != null)
                {
                    lblTotalPickQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()).ToString();
                }

            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;

                System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CouponPicking_D().GetList(string.Format("CouponPickingNumber = '{0}'", drv["CouponPickingNumber"].ToString()) + " order by CouponTypeID,KeyID");

                Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");

                list.DataSource = ds.Tables[0];
                list.DataBind();
            }
        }

        #endregion


        #region 数据列表绑定
        //private void RptBind(string strWhere, string orderby)
        //{
        //    ViewState["CouponTypeCode"] = null;
        //    ViewState["CouponType"] = null;
        //    ViewState["OrderQTY"] = null;

        //    Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D();

        //    System.Data.DataSet ds = null;

        //    ds = bll.GetList(strWhere);

        //    Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
        //    Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");

        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();

        //    //统计
        //    long totalOrderQTY = 0;
        //    long totalPickQTY = 0;
        //    Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
        //    lblTotalOrderQTY.Text = totalOrderQTY.ToString();
        //    lblTotalPickQTY.Text = totalPickQTY.ToString();
        //}

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseAndPostBack();
        }
    }
}