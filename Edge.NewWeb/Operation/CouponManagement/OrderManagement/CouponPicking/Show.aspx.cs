﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponPicking
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_CouponPicking_H, SVA.Model.Ord_CouponPicking_H>
    {

        private const string fields = "[KeyID],[CouponPickingNumber],[CouponTypeID],[Description],[OrderQTY],[PickQTY],[ActualQTY],[FirstCouponNumber],[EndCouponNumber],[BatchCouponCode],[PickupDateTime]";
        private static string ApproveStatusValue; //Add By Robin 2014-11-25 for static stock status

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid2.PageSize = webset.ContentPageNum;
                RegisterCloseEvent(btnClose);
                //RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields); //Moved to OnLoadComplete() Robin 2014-11-26
                RptTotalBind();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;

                this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                this.CustomerType.Hidden = true;

                this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                this.SendMethod.Hidden = true;

                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID);
                this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                store = new Edge.SVA.BLL.Store().GetModel(Model.FromStoreID);
                this.FromStoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetOrderPickingApproveStatusString(this.Model.ApproveStatus);
                ApproveStatusValue = this.Model.ApproveStatus; //Add By Robin 2014-11-25 for static stock status
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    lblOrderType.Text = "手动";
                }
                else
                {
                    lblOrderType.Text = "自动";
                }

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                    this.btnPrint.Visible = false;
                }
                else
                {
                    this.btnPrint.Visible = false;
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }
                RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields); //Moved from Page_Load() Robin 2014-11-26
            }
        }

        #endregion

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.Grid2.PageSize, this.Grid2.PageIndex);
            }

            Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            //Tools.DataTool.AddCouponStockStatusByID(ds, "StockStatus", "FirstCouponNumber");
            //Add By Robin 2014-11-25 for static stock status
            int StockStatus=0;
            if (ApproveStatusValue == "R" || ApproveStatusValue == "V") { StockStatus = 2; }
            if (ApproveStatusValue == "P") { StockStatus = 4; }
            if (ApproveStatusValue == "A") { StockStatus = 5; }
            ds.Tables[0].Columns.Add(new DataColumn("StockStatus", typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row["StockStatus"] = StockStatus;
            }
            //End
            DataTool.AddCouponStockStatus(ds, "StockStatusName", "StockStatus");
            DataTool.AddCouponUIDByCouponNumber(ds, "FirstCouponUID", "FirstCouponNumber");
            DataTool.AddCouponUIDByCouponNumber(ds, "EndCouponUID", "EndCouponNumber");

            this.Grid2.DataSource = ds.Tables[0].DefaultView;
            this.Grid2.DataBind();


            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            lblTotalPickQTY.Text = totalPickQTY.ToString();
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.Grid2.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            ViewState["CouponTypeCode"] = null;
            ViewState["CouponType"] = null;
            ViewState["OrderQTY"] = null;

            this.Grid2.PageIndex = e.NewPageIndex;

            RptBind(string.Format("CouponPickingNumber='{0}'", Request.Params["id"]), "CouponTypeID,KeyID", fields);

        }

        protected void Grid2_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            //string CouponTypeCode = "";
            //string CouponType = "";
            //string OrderQTY = "";

            if (e.DataItem is DataRowView)
            {
                //显示格式
                Label lblCouponTypeCode = Grid2.Rows[e.RowIndex].FindControl("lblCouponTypeCode") as Label;
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)Grid2.Rows[e.RowIndex].FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)Grid2.Rows[e.RowIndex].FindControl("lblOrderQTY");
                    Label lblSeq = Grid2.Rows[e.RowIndex].FindControl("lblSeq") as Label;
                    HiddenField hfCouponTypeID = Grid2.Rows[e.RowIndex].FindControl("hfCouponTypeID") as HiddenField;
                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                        if (lblSeq != null) { lblSeq.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null) { ViewState["CouponType"] = lblCouponType.Text.Trim(); }
                        if (lblOrderQTY != null) { ViewState["OrderQTY"] = lblOrderQTY.Text.Trim(); }
                        if (lblSeq != null) { lblSeq.Text = (this.CouponTypeIndex[int.Parse(hfCouponTypeID.Value)]).ToString(); }
                    }
                }
            }

        }

        private Dictionary<int, int> CouponTypeIndex
        {
            get
            {
                if (ViewState["CouponTypeIndex"] == null)
                {
                    ViewState["CouponTypeIndex"] = new SVA.BLL.Ord_CouponPicking_D().GetCouponTypeIndex(Request.Params["id"]);
                }
                return ViewState["CouponTypeIndex"] as Dictionary<int, int>;
            }
        }

        #endregion

        #region 捡回汇总列表
        private void RptTotalBind()
        {
            Edge.SVA.BLL.Ord_CouponPicking_D bll = new Edge.SVA.BLL.Ord_CouponPicking_D();

            System.Data.DataSet ds = bll.GetListGroupByCouponType(string.Format("CouponPickingNumber='{0}'", Request.Params["id"].Trim()));

            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddID(ds, "ID", this.Grid2.PageSize, this.Grid2.PageIndex);
            this.Grid1.DataSource = ds.Tables[0].DefaultView;
            this.Grid1.DataBind();

            //统计
            long totalOrderQTY = 0;
            long totalPickQTY = 0;
            Controllers.CouponOrderController.GetApprovePickedTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);

            lblGrid1TotalOrderQTY.Text = totalOrderQTY.ToString();
            lblGrid1TotalPickQTY.Text = totalPickQTY.ToString();

            if (!Controllers.CouponOrderController.IsMeetPickingByType(totalOrderQTY, totalPickQTY))
            {
                lblGrid1TotalPickQTY.CssStyle = "color:red;font-weight:bold;";
            }
        }

        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                Label orderQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblOrderQTY1");
                Label pickQTY = (Label)Grid1.Rows[e.RowIndex].FindControl("lblPickQTY1");

                long longOrderQTY = Tools.ConvertTool.ConverType<long>(orderQTY.Text);
                long longPickQTY = Tools.ConvertTool.ConverType<long>(pickQTY.Text);

                if (!Controllers.CouponOrderController.IsMeetPickingByType(longOrderQTY, longPickQTY))
                {
                    pickQTY.ForeColor = System.Drawing.Color.Red;
                    pickQTY.Font.Bold=true;
                }
            }
        }
        #endregion

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Print.aspx?id={0}", Request.Params["id"]));
        }

        protected void btnPrintAR_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("PrintAR.aspx?id={0}", Request.Params["id"]));
        }   
    }
}