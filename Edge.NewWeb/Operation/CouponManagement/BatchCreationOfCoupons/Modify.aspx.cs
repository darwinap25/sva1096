﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponBatchCreate, Edge.SVA.Model.Ord_CouponBatchCreate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "IsImportCouponNumber = 0  order by CouponTypeCode");
                //Edge.Web.Tools.ControlTool.BindCampaign(CampaignID);
                this.txtCouponStatus.Text = Tools.DALTool.GetCouponTypeStatusName((int)Controllers.CouponController.CouponStatus.Dormant);
            }
        }

        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(CouponTypeID.SelectedValue))
                {
                    if (!hasRight)
                    {
                        return;
                    }
                    int couponTypeID = int.Parse(CouponTypeID.SelectedValue);
                    Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

                    Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(model.CampaignID.GetValueOrDefault());
                    CampaignID.Text = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                   
                    GetCreatedInfo(couponTypeID);
                }
                
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                this.lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);

                Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(Model.CouponTypeID);

                this.InitStatusView.Text = this.InitStatus.SelectedItem.Text;
                //if (couponType != null)
                //{
                //    bool isFixedAmount = couponType.CoupontypeFixedAmount.HasValue ? couponType.CoupontypeFixedAmount.Value == 0 : false;
                //    this.InitAmount.Enabled = isFixedAmount;
                //}
            }

        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Logger.Instance.WriteOperationLog(this.PageName, "Update");

            Edge.SVA.Model.Ord_CouponBatchCreate item = null;
            Edge.SVA.Model.Ord_CouponBatchCreate dataItem = this.GetDataObject();

            //Check model
            if (dataItem == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                return;
            }
            //Check the transaction whether pending
            if (dataItem.ApproveStatus.ToUpper().Trim() != "P")
            {
                ShowWarningAndClose(Resources.MessageTips.TheTransactionStatusNotPending);
                return;
            }
            //Update model
            item = this.GetPageObject(dataItem);

            if (item != null)
            {
                string msg = "";
                long remainCoupons = 0;
                string lastCreatedCoupon = "";

                int checkResult = Controllers.CouponController.GetCreatedCouponInfo(item.CouponTypeID, item.CouponCount, ref remainCoupons, ref lastCreatedCoupon, ref msg);
                if (checkResult == -1)
                {
                   // Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.NumberDuplicate);
                    ShowWarning(Resources.MessageTips.NumberDuplicate + "  " + msg);
                    return;
                }
                else if (checkResult == -2)
                {
                    //Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.ExceededNumberOfRange);
                    ShowWarning(Resources.MessageTips.ExceededNumberOfRange);
                    return;
                }

                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponBatchCreate>(item))
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.UpdateSuccess);

                CloseAndPostBack();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.UpdateFailed);
                ShowUpdateFailed();
            }
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CouponTypeID.SelectedValue))
                return;
            int couponTypeID = int.Parse(CouponTypeID.SelectedValue);
            Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

            if (model != null)
            {
                InitAmount.Text = model.CouponTypeAmount.ToString();
                ExpiryDate.Text = Edge.Web.Tools.DALTool.GetCouponTypeExpiryDate(model);
            }

           // Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(ConvertTool.ToInt(CouponTypeID.SelectedValue));
            //if (couponType != null)
            //{
            //    bool isFixedAmount = couponType.CoupontypeFixedAmount.HasValue ? couponType.CoupontypeFixedAmount.Value == 0 : false;
            //    this.InitAmount.Enabled = isFixedAmount;
            //}

            GetCreatedInfo(couponTypeID);
        }

        private void GetCreatedInfo(int couponTypeID)
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            this.CreatedCoupons.Text = bll.GetCount(string.Format("CouponTypeID = {0}", couponTypeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCoupons = 0;
            string lastCreatedCoupon = "";
            Controllers.CouponController.GetCreatedCouponInfo(couponTypeID, 0, ref remainCoupons, ref lastCreatedCoupon, ref msg);
            this.RemainCoupons.Text = remainCoupons.ToString("N00");
            this.LastCreatedCoupons.Text = lastCreatedCoupon;
        }

        protected override SVA.Model.Ord_CouponBatchCreate GetPageObject(SVA.Model.Ord_CouponBatchCreate obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }
    }
}
