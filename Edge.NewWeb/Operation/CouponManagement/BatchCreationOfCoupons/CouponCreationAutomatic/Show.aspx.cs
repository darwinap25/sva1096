﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;
using System.Data;
using Edge.Web.Controllers.Operation.CouponManagement.BatchCreationOfCoupons.CouponCreationAutomatic;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons.CouponCreationAutomatic
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_OrderToSupplier_H, Edge.SVA.Model.Ord_OrderToSupplier_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.Grid1.PageSize = webset.ContentPageNum;
                this.Grid2.PageSize = webset.ContentPageNum;

                ControlTool.BindAllSupplier(SupplierID);
                ControlTool.BindStore(StoreID);

                //ControlTool.BindCouponType(CouponType, -1);
                ControlTool.BindCouponType(CouponTypeID);
                ControlTool.BindCompanyID(CompanyID);

                RegisterCloseEvent(btnClose);
                
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.OrderSupplierNumber.Text = Model.OrderSupplierNumber;
                //Remark by Robin 20140622
                //btnExport.OnClientClick = Window1.GetShowReference("Export.aspx?OrderSupplierNumber=" + Model.OrderSupplierNumber, "导出");
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.lblApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                if (Model.OrderType.GetValueOrDefault() == 0)
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Manually"; break;
                        case "zh-cn": lblOrderType.Text = "手动"; break;
                        case "zh-hk": lblOrderType.Text = "手動"; break;
                    }
                }
                else
                {
                    switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                    {
                        case "en-us": lblOrderType.Text = "Auto"; break;
                        case "zh-cn": lblOrderType.Text = "自动"; break;
                        case "zh-hk": lblOrderType.Text = "自動"; break;
                    }
                }
                this.SupplierID.SelectedValue = Model.SupplierID.ToString();
                this.lblSupplierID.Text = this.SupplierID.SelectedItem.Text;
                this.lblSupplierAddress.Text = Model.SupplierAddress;
                this.lblSuppliertContactName.Text = Model.SuppliertContactName;
                this.lblSupplierPhone.Text = Model.SupplierPhone;

                //ControlTool.BindCouponTypeBySupplierID(this.CouponTypeID, Convert.ToInt32(SupplierID.SelectedValue));

                this.StoreID.SelectedValue = Model.StoreID.ToString();
                this.lblStoreID.Text = this.StoreID.SelectedItem.Text;
                this.lblSendAddress.Text = Model.SendAddress;
                this.lblStoreContactName.Text = Model.StoreContactName;
                this.lblStorePhone.Text = Model.StorePhone;

                //this.CouponTypeID.SelectedValue = Model.CouponTypeID.ToString();
                //this.lblCouponTypeID.Text = this.CouponTypeID.SelectedItem.Text;
                //this.lblCouponQty.Text = Model.CouponQty.ToString();

                if (Model.IsProvideNumber.GetValueOrDefault() == 0)
                {
                    this.lblIsProvideNumber.Text = "否";
                }
                else 
                {
                    this.lblIsProvideNumber.Text = "是";
                }
                this.CompanyID.SelectedValue = Model.CompanyID.ToString();
                this.CompanyName.Text = this.CompanyID.SelectedItem.Text;

                this.BindDetail();
                this.RptTotalBind();

            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }

        private System.Data.DataTable Detail
        {
            get
            {
                //if (ViewState["DetailResult"] == null || this.SearchFlag.Text == "1") //Removed By Robin 2014-10-13
                {
                    string strWhere="";
                    if (this.SearchFlag.Text == "1" && Tools.ConvertTool.ToInt(this.CouponTypeID.SelectedValue) >= 0)
                    {
                        strWhere = " and CouponTypeID=" + Tools.ConvertTool.ToInt(this.CouponTypeID.SelectedValue);
                    }
                    //Modified By Robin 2014-10-13
                    //System.Data.DataSet ds = new Edge.SVA.BLL.Ord_OrderToSupplier_D().GetList(string.Format("OrderSupplierNumber = '{0}'", Request.Params["id"]) + strWhere);
                    int pagesize = this.Grid1.PageSize;
                    int pageindex = this.Grid1.PageIndex;
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_OrderToSupplier_D().GetListByPage(string.Format("OrderSupplierNumber = '{0}'", Request.Params["id"]) + strWhere, "", pagesize * pageindex + 1, pagesize * (pageindex + 1));
                    //End

                    if (ds == null || ds.Tables.Count <= 0) return null;

                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    Tools.DataTool.AddCouponUIDByCouponNumber(ds, "CouponUID", "FirstCouponNumber");
                    Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                    Tools.DataTool.AddCouponTypeNameByID(ds, "CouponTypeName", "CouponTypeID");

                    ViewState["DetailResult"] = ds.Tables[0];
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void BindDetail()
        {

            //获得总条数 Modified By Robin 2014-10-13
            string strWhere = "";
            if (this.SearchFlag.Text == "1" && Tools.ConvertTool.ToInt(this.CouponTypeID.SelectedValue) >= 0)
            {
                strWhere = " and CouponTypeID=" + Tools.ConvertTool.ToInt(this.CouponTypeID.SelectedValue);
            }
            this.Grid1.RecordCount = new Edge.SVA.BLL.Ord_OrderToSupplier_D().GetRecordCount(string.Format("OrderSupplierNumber = '{0}'", Request.Params["id"]) + strWhere);
            this.Grid1.DataSource = DataTool.GetPaggingTable(0, this.Grid1.PageSize, this.Detail);
            //this.Grid1.RecordCount = this.Detail.Rows.Count;
            //this.Grid1.DataSource = DataTool.GetPaggingTable(this.Grid1.PageIndex, this.Grid1.PageSize, this.Detail);
            //End
            this.Grid1.DataBind();
        }

        protected override SVA.Model.Ord_OrderToSupplier_H GetPageObject(SVA.Model.Ord_OrderToSupplier_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        //Add by Robin 20140622
        protected void btnExport_Click(object sender, EventArgs e)
        {
            CouponCreationAutomaticController controller = new CouponCreationAutomaticController();


            DataTable dt = controller.GetExportList(OrderSupplierNumber.Text);
            string fileName = controller.UpLoadFileToServer(dt);

            try
            {
                string exportname = "GC Delivery List.xls";
                Tools.ExportTool.ExportFile(fileName, exportname);
            }
            catch (Exception ex)
            {
                ShowWarning(ex.Message);
            }
        }

        protected void btnPrintPR_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("PrintPR.aspx?id={0}", Request.Params["id"]));
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            this.BindDetail();
        }

        #region 订单汇总列表
        private void RptTotalBind()
        {
            Edge.SVA.BLL.Ord_OrderToSupplier_D bll = new Edge.SVA.BLL.Ord_OrderToSupplier_D();

            System.Data.DataSet ds = bll.GetListGroupByCouponType(string.Format("OrderSupplierNumber='{0}'", Request.Params["id"].Trim()));

            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddCouponTypeNameByID(ds, "CouponTypeName", "CouponTypeID");
            Tools.DataTool.AddID(ds, "ID", this.Grid2.PageSize, this.Grid2.PageIndex);
            this.Grid2.DataSource = ds.Tables[0].DefaultView;
            this.Grid2.DataBind();
        }

        protected void Grid2_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid2.PageIndex = e.NewPageIndex;
            this.BindDetail();
        }
        #endregion
    }
}