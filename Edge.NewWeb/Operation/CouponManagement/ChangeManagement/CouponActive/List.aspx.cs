﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers;
using System.Data;
using Edge.Messages.Manager;
using System.Text;
using Edge.Web.Controllers.Operation.CouponManagement.ChangeManagement;
using Edge.Web.Tools;
using System.IO;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.CouponActive
{
    public partial class List : PageBase
    {

        private const string fields = "[CouponAdjustNumber],[TxnDate],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate],[ApprovalCode]";
        protected static string strWhere = " OprID =" + ((int)CouponController.OprID.Active).ToString();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind(strWhere, "CouponAdjustNumber");
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);

                //added by Rochelle 20151005++
                btnImport.OnClientClick = Window3.GetShowReference("~/PublicForms/ImportForm.aspx?Menu=CouponActive", "Import");
                //added by Rochelle 20151005--

                ControlTool.BindBrand(this.Brand);
                InitStoreByBrand();
            }
            string url = this.Request.Url.AbsolutePath.Substring(0, this.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
        }

        #region 数据列表绑定

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private void RptBind(string strWhere, string orderby)
        {

            //Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H()
            //{
            //    StrWhere = strWhere,
            //    Order = orderby,
            //    Fields = fields,
            //    Ascending = false
            //};

            //System.Data.DataSet ds = null;
            //if (this.RecordCount < 0)
            //{
            //    int count = 0;
            //    ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, out count);
            //    this.RecordCount = count;

            //}
            //else
            //{
            //    ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex);
            //}

            //Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
            //Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
            //Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");
            //this.Grid1.DataSource = ds.Tables[0].DefaultView;
            //this.Grid1.DataBind();

            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string brandcode = Tools.DALTool.GetBrandCode(Tools.ConvertTool.ToInt(this.Brand.SelectedValue), null);
                    string storecode = Tools.DALTool.GetStoreCode(Tools.ConvertTool.ToInt(this.Store.SelectedValue), null);
                    string code = this.Code.Text.Trim();
                    string status = this.Status.SelectedValue.Trim();
                    string CStatrtDate = this.CreateStartDate.Text;
                    string CEndDate = this.CreateEndDate.Text;
                    string AStatrtDate = this.ApproveStartDate.Text;
                    string AEndDate = this.ApproveEndDate.Text;
                    if (!string.IsNullOrEmpty(brandcode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" BrandCode ='");
                        sb.Append(brandcode);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(storecode))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" StoreCode ='");
                        sb.Append(storecode);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CouponAdjustNumber like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveStatus";
                        sb.Append(descLan);
                        sb.Append(" ='");
                        sb.Append(status);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(CStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(CStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(CEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(CEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(AStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(AStatrtDate);
                        sb.Append("' as DateTime) ");
                    }
                    if (!string.IsNullOrEmpty(AEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(AEndDate);
                        sb.Append("' as DateTime) + 1 ");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;

                ChangeManagementController c = new ChangeManagementController();
                int count = 0;
                //DataSet ds = c.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                DataSet ds = c.GetTransactionList_NoStore(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                
                this.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }
            }
            catch (Exception ex)
            {
                Tools.Logger.Instance.WriteErrorLog("Load CouponActive", "error", ex);
            }
        }
        /// <summary>
        /// Import Batch File with single couponUID or range of couponUID.
        /// Added by Rochelle 20151005
        /// </summary>
        /// <param name="importfilename"></param>
        /// <returns></returns>
        protected void ImportFile(string importfilename)
        {
            if (importfilename != "")
            {

                if (ValidateFile(importfilename))
                {
                    DataTable dt = ExcelTool.GetFirstSheet(importfilename);
                    string CouponAdjustNumber = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.OrderCouponActive);
                    int OprID = Convert.ToInt32(Enum.Parse(typeof(CouponController.OprID), CouponController.OprID.Active.ToString()));
                    int detailcount = 0;
                    float detailsum = 0;
                    int storeID = 0;
                    
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);

                    DataTable Detaildt = database.GetTableSchema("Ord_CouponAdjust_D");
                    Detaildt.PrimaryKey = new DataColumn[] { Detaildt.Columns["CouponNumber"] };

                    Edge.SVA.BLL.Coupon importcoupon = new Edge.SVA.BLL.Coupon();
                    foreach (DataRow row in dt.Rows)
                    {
                        #region CouponUIDRage
                        if (row["Coupon UID By Range (Beginning)"].ToString() != "")
                        {
                            string couponUIDEnd = row["Coupon UID By Range (Ending)"].ToString() !="" ? row["Coupon UID By Range (Ending)"].ToString() : (row["Coupon UID By Range (Beginning)"].ToString());

                            DataTable coupondt = (importcoupon.GetListByCouponUIDRange(row["Coupon UID By Range (Beginning)"].ToString(), couponUIDEnd)).Tables[0];
                            if (coupondt.Rows.Count != 0)
                            {
                                int status, locateStoreID;
                                int i = 0;
                                
                                foreach (DataRow couponrow in coupondt.Rows)
                                {
                                    status = int.TryParse(couponrow["Status"].ToString(), out i) ? i : 0;
                                    locateStoreID = int.TryParse(couponrow["LocateStoreID"].ToString(), out i) ? i : 0;
                                    if (status != Convert.ToInt32(Enum.Parse(typeof(CouponController.CouponStatus), CouponController.CouponStatus.Issued.ToString())))
                                    {
                                        ShowSaveFailed(couponrow["CouponNumber"].ToString() + ": Coupon status is not " + CouponController.CouponStatus.Issued.ToString().ToUpper() + "!");
                                        return;
                                    }

                                    if ((locateStoreID != storeID && storeID != 0) || (locateStoreID == 0))
                                    { 
                                        ShowSaveFailed("Coupon Number:" + couponrow["CouponNumber"].ToString() + ", has different StoreCode!");
                                        return;
                                    }
                                    try
                                    {
                                        DataRow dr = Detaildt.NewRow();
                                        dr["CouponAdjustNumber"] = CouponAdjustNumber;
                                        storeID = locateStoreID;
                                        dr["CouponNumber"] = couponrow["CouponNumber"].ToString();
                                        dr["CouponAmount"] = couponrow["CouponAmount"].ToString() == "" ? 0 : decimal.Parse(couponrow["CouponAmount"].ToString());
                                        detailcount = detailcount + 1;
                                        detailsum = detailsum + float.Parse(dr["CouponAmount"].ToString() == "" ? "0" : dr["CouponAmount"].ToString());
                                        Detaildt.Rows.Add(dr);
                                    }
                                    catch (Exception)
                                    {
                                        ShowSaveFailed("Excel duplicate record, Coupon Number:" + couponrow["CouponNumber"].ToString() +
                                                " in CouponUID Range: " + row["Coupon UID By Range (Beginning)"].ToString() +
                                                " and " + couponUIDEnd);
                                        return;
                                    }
                                }
                                                            
                            }
                            else
                            {
                                ShowSaveFailed("Coupon UID By Range (Beginning):" + row["Coupon UID By Range (Beginning)"].ToString() +
                                             "and Coupon UID By Range (Ending)" + row["Coupon UID By Range (Ending)"].ToString() +
                                             " has no valid Coupon Number.");
                                return;
                            }
                        }
                        #endregion
                        else if (row["Coupon UID By Range (Ending)"].ToString() != "")
                        {
                            ShowSaveFailed("Coupon UID By Range (Beginning) is empty but Coupon UID By Range (Ending) has value.");
                            return;
                        }
                        #region CouponUID Single
                        else if (row["Coupon UID (One by One)"].ToString() != "")
                        {
                            DataRow dr = Detaildt.NewRow();
                            dr["CouponAdjustNumber"] = CouponAdjustNumber;
                            
                            Edge.SVA.Model.Coupon cm = importcoupon.GetModelByUID(row["Coupon UID (One by One)"].ToString());
                            if (cm == null)
                            {
                                ShowSaveFailed("Coupon Number:" + row["Coupon UID (One by One)"].ToString() + ", is not valid!");
                                return;
                            }
                            
                            if (cm.Status != Convert.ToInt32(Enum.Parse(typeof(CouponController.CouponStatus), CouponController.CouponStatus.Issued.ToString())))
                            {
                                ShowSaveFailed(cm.CouponNumber + ": Coupon status is not " + CouponController.CouponStatus.Issued.ToString().ToUpper() + "!");
                                return;
                            } 

                            if ((cm.LocateStoreID != storeID  && storeID != 0) || (cm.LocateStoreID == 0))
                            {
                                ShowSaveFailed("Coupon Number:" + cm.CouponNumber + ", has different StoreCode!");
                                return;
                            }
                            try
                            {
                                storeID = cm.LocateStoreID.Value;
                                dr["CouponNumber"] = cm.CouponNumber;
                                dr["CouponAmount"] = cm.CouponAmount;
                                detailcount = detailcount + 1;
                                detailsum = detailsum + float.Parse(dr["CouponAmount"].ToString() == "" ? "0" : dr["CouponAmount"].ToString());
                                Detaildt.Rows.Add(dr);
                            }
                            catch (Exception)
                            {
                                ShowSaveFailed("Excel duplicate record, Coupon Number:" + cm.CouponNumber);
                                return;
                            }
                        }
                        #endregion
                    }

                    DataTable Headdt = database.GetTableSchema("Ord_CouponAdjust_H");
                    DataRow ddr = Headdt.NewRow();
                    ddr["CouponAdjustNumber"] = CouponAdjustNumber;
                    ddr["OprID"] = OprID;
                    ddr["BrandCode"] = DALTool.GetBrandCode(DALTool.GetBrandIDByStoreID(storeID,null),null);
                    ddr["StoreCode"] = DALTool.GetStoreCode(storeID, null);
                    ddr["Note"] = "Import Coupon Offline Activation";
                    ddr["ActAmount"] = detailsum;
                    ddr["CouponCount"] = detailcount;
                    ddr["ApproveStatus"] = "P";
                    ddr["CreatedBy"] = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                    ddr["CreatedOn"] = Edge.Web.Tools.DALTool.GetSystemDateTime();
                    ddr["CreatedBusdate"] = Edge.Web.Tools.DALTool.GetBusinessDate();
                    ddr["TxnDate"] = Edge.Web.Tools.DALTool.GetSystemDateTime();
                    Headdt.Rows.Add(ddr);

                    DatabaseUtil.Interface.IExecStatus hes = database.InsertBigData(Headdt, "Ord_CouponAdjust_H");
                    if (!hes.Success)
                    {
                        ShowAddFailed();
                        return;
                    }
                    DatabaseUtil.Interface.IExecStatus des = database.InsertBigData(Detaildt, "Ord_CouponAdjust_D");
                    if (!des.Success)
                    {
                        ShowAddFailed();
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// Checking if the file has correct file extension .
        /// Added by Rochelle 20151005
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        protected bool ValidateFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                filename = Path.GetExtension(filename).TrimStart('.');
                if (!webset.CardCreateFileType.ToLower().Split('|').Contains(filename))
                {
                    ShowWarning(Resources.MessageTips.FileUpLoadFailed.Replace("{0}", webset.CardCreateFileType.Replace("|", ",")));
                    return false;
                }
            }
            return true;
        }
        //Added by Rochelle 20151005 --
        #endregion

        #region Event
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            ////FineUI.PageContext.RegisterStartupScript(Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')));
            ////FineUI.PageContext.Redirect("Approve.aspx?ids=" + sb.ToString().TrimEnd(','));
            NewApproveTxns(Grid1, Window2);
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), HiddenWindowForm.GetShowReference("Void.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            ////FineUI.PageContext.Redirect("Void.aspx?ids=" + sb.ToString().TrimEnd(','));
            NewVoidTxns(Grid1, HiddenWindowForm);
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind(strWhere, "CouponAdjustNumber");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            
            base.WindowEdit_Close(sender, e);
            #region Importing Data Processing
            if (!string.IsNullOrEmpty(SVASessionInfo.ImportCouponRedeemPath))
            {
                this.ImportFile(SVASessionInfo.ImportCouponRedeemPath);
                SVASessionInfo.ImportCouponRedeemPath = null;
            }
            #endregion
            RptBind(strWhere, "CouponAdjustNumber");
        }
        protected void Grid1_RowDataBound(object sender, FineUI.GridRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["ApproveStatus"].ToString().Trim();
                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    switch (approveStatus)
                    {
                        case "A":
                            break;
                        case "P":
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                        case "V":
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                    }
                }
            }
        }

        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            if (e.DataItem is DataRowView)
            {
                DataRowView drv = e.DataItem as DataRowView;
                string approveStatus = drv["ApproveStatus"].ToString().Trim();
                FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;

                if (approveStatus != "")
                {
                    approveStatus = approveStatus.Substring(0, 1).ToUpper().Trim();
                    switch (approveStatus)
                    {
                        case "A":
                            editWF.Enabled = false;
                            break;
                        case "P":
                            editWF.Enabled = true;
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                        case "V":
                            editWF.Enabled = false;
                            (Grid1.Rows[e.RowIndex].FindControl("lblApproveCode") as Label).Text = "";
                            break;
                    }
                }
            }
        }

        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            ChangeManagementController c = new ChangeManagementController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = c.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            DataView view1 = table.DefaultView;
            view1.Sort = String.Format("{0} {1}", sortField, sortDirection);

            Grid1.DataSource = view1;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind(strWhere, "CouponAdjustNumber");
        }
        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
        }
        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(this.Store, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }
    }
}
