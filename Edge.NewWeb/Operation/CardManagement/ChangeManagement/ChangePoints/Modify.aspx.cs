﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangePoints;
using Edge.Web.Tools;
using Edge.SVA.Model.Domain.WebInterfaces;
using System.Data;
using FineUI;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.ChangePoints
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_TradeManually_H, Edge.SVA.Model.Ord_TradeManually_H>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        CardChangePointsController controller;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);

                InitData();

                SVASessionInfo.CardChangePointsController = null;
            }
            controller = SVASessionInfo.CardChangePointsController;
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }

                controller.LoadViewModel(Model.TradeManuallyCode);

                if (controller.ViewModel.MainTable != null)
                {
                    this.lblCreatedBy.Text = DALTool.GetUserName(controller.ViewModel.MainTable.CreatedBy.GetValueOrDefault());
                    this.ApproveStatus.Text = DALTool.GetApproveStatusString(controller.ViewModel.MainTable.ApproveStatus);
                    DataTable dt = controller.GetCardNumberByCase("cardno", ConvertTool.ToInt(this.BrandID.SelectedValue), controller.ViewModel.MainTable.CardNumber);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        this.MobileNo.Text = dt.Rows[0]["MemberMobilePhone"].ToString();
                        this.MemberRegisterMobile.Text = dt.Rows[0]["MemberRegisterMobile"].ToString();
                    }
                }

                if (controller.ViewModel.SubTable != null)
                {
                    BindTradeList(controller.ViewModel.SubTable);
                }

                if (this.BrandID.SelectedValue != "-1")
                {
                    this.MobileNo.Enabled = this.CardNumber.Enabled = this.MemberRegisterMobile.Enabled = true;
                }
                else
                {
                    this.MobileNo.Enabled = this.CardNumber.Enabled = this.MemberRegisterMobile.Enabled = false;
                }
                
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update ");

            controller.ViewModel.MainTable = this.GetAddObject();


            if (controller.ViewModel.MainTable != null)
            {
                controller.ViewModel.MainTable.UpdatedOn = DateTime.Now;
                controller.ViewModel.MainTable.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;

            }

            ExecResult er = controller.Update();
            if (er.Success)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Update Trade Success Code:" + controller.ViewModel.MainTable.TradeManuallyCode);
                CloseAndPostBack();
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Update Trade Failed Code:" + controller.ViewModel.MainTable == null ? "No Data" : controller.ViewModel.MainTable.TradeManuallyCode.ToString());
                ShowUpdateFailed();
            }
        }

        protected void InitData()
        {
            ControlTool.BindBrand(this.BrandID);
            ControlTool.BindCurrency(this.TenderID);
        }

        protected void BrandID_SelectedChanged(object sender, EventArgs e)
        {
            if (this.BrandID.SelectedValue != "-1")
            {
                this.MobileNo.Enabled = this.CardNumber.Enabled = this.MemberRegisterMobile.Enabled = true;
            }
            else
            {
                this.MobileNo.Enabled = this.CardNumber.Enabled = this.MemberRegisterMobile.Enabled = false;
            }
            this.MobileNo.Text = this.CardNumber.Text = this.MemberRegisterMobile.Text = string.Empty;
            this.rblStore.Items.Clear();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (this.CardNumber.Text == string.Empty || !CheckCardNumeber())
            {
                ShowWarning("请先填写有效的卡号！");
                return;
            }
            if (this.TraderAmount.Text == string.Empty)
            {
                ShowWarning("请填写交易金额！");
                return;
            }
            if (rblStore.SelectedItem != null)
            {
                ShowConfirmDialog(Resources.MessageTips.AreyouSureSubmit, "", MessageBoxIcon.Question, Window1.GetShowReference("~/PublicForms/Confirm.aspx"), "");
            }
            else
            {
                ShowWarning(Resources.MessageTips.NotSelected);
                return;
            }
        }

        protected bool CheckCardNumeber()
        {
            DataTable dt = controller.GetCardNumberByCase("cardno", ConvertTool.ToInt(this.BrandID.SelectedValue), this.CardNumber.Text);
            if (dt != null && dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        protected void Window1_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            int earnpoint = controller.GetPonits(this.CardNumber.Text, Convert.ToDecimal(this.TraderAmount.Text), ConvertTool.ToInt(rblStore.SelectedValue));
            AddTrade(Convert.ToDecimal(this.TraderAmount.Text), ConvertTool.ToInt(this.rblStore.SelectedValue), earnpoint);
            FineUI.Alert.ShowInTop(Resources.MessageTips.SubmitSuccess + "<br/><span style='color:red;font-weight:bold;font-size:14px;'>" + Resources.MessageTips.Awarding + earnpoint + "</span>", "", FineUI.MessageBoxIcon.Information, "");
            return;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Edge.Web.Tools.ControlTool.BindStore(this.rblStore, 0, 0, 0, StoreName.Text.Trim());
        }

        #region 获取卡信息
        protected void MobileNo_TextChanged(object sender, EventArgs e)
        {
            if (this.MobileNo.Text != string.Empty)
            {
                DataTable dt = controller.GetCardNumberByCase("mobile", Convert.ToInt32(this.BrandID.SelectedValue), this.MobileNo.Text);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.CardNumber.Text = dt.Rows[0]["CardNumber"].ToString();
                    this.MemberRegisterMobile.Text = dt.Rows[0]["MemberRegisterMobile"].ToString();
                }
                else
                {
                    ShowWarning(Resources.MessageTips.MemberMobileNotExist);
                    return;
                }
                this.CardNumber.Enabled = this.MemberRegisterMobile.Enabled = false;
            }
            else
            {
                this.CardNumber.Enabled = this.MemberRegisterMobile.Enabled = true;
            }
        }

        protected void CardNumber_TextChanged(object sender, EventArgs e)
        {
            if (this.CardNumber.Text != string.Empty)
            {
                DataTable dt = controller.GetCardNumberByCase("cardno", Convert.ToInt32(this.BrandID.SelectedValue), this.CardNumber.Text);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.MobileNo.Text = dt.Rows[0]["MemberMobilePhone"].ToString();
                    this.MemberRegisterMobile.Text = dt.Rows[0]["MemberRegisterMobile"].ToString();
                }
                else
                {
                    ShowWarning(Resources.MessageTips.NoData);
                    return;
                }
                this.MobileNo.Enabled = this.MemberRegisterMobile.Enabled = false;
            }
            else
            {
                this.MobileNo.Enabled = this.MemberRegisterMobile.Enabled = true;
            }
        }

        protected void MemberRegisterMobile_TextChanged(object sender, EventArgs e)
        {
            if (this.MemberRegisterMobile.Text != string.Empty)
            {
                DataTable dt = controller.GetCardNumberByCase("resmobile", Convert.ToInt32(this.BrandID.SelectedValue), this.MemberRegisterMobile.Text);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.MobileNo.Text = dt.Rows[0]["MemberMobilePhone"].ToString();
                    this.CardNumber.Text = dt.Rows[0]["CardNumber"].ToString();
                }
                else
                {
                    ShowWarning(Resources.MessageTips.MemberMobileNotExist);
                    return;
                }
                this.MobileNo.Enabled = this.CardNumber.Enabled = false;
            }
            else
            {
                this.MobileNo.Enabled = this.CardNumber.Enabled = true;
            }
        }
        #endregion

        #region 交易列表
        protected void AddTrade(decimal traderamount, int storeid, int earnpoint)
        {
            int keyid = controller.ViewModel.SubTable.Rows.Count;
            if (controller.ViewModel.SubTable.Columns.Count == 0)
            {
                controller.ViewModel.SubTable.Columns.Add("KeyID", typeof(string));
                //controller.ViewModel.SubTable.Columns.Add("BrandName", typeof(string));
                //controller.ViewModel.SubTable.Columns.Add("TendName", typeof(string));
                //controller.ViewModel.SubTable.Columns.Add("BusDate", typeof(string));
                controller.ViewModel.SubTable.Columns.Add("StoreName", typeof(string));
                controller.ViewModel.SubTable.Columns.Add("TraderAmount", typeof(decimal));
                controller.ViewModel.SubTable.Columns.Add("EarnPoint", typeof(int));
                controller.ViewModel.SubTable.Columns.Add("StoreID", typeof(int));
                controller.ViewModel.SubTable.Columns.Add("TradeManuallyCode", typeof(string));
            }
            if (storeid > 0)
            {
                keyid--;
                DataRow dr = controller.ViewModel.SubTable.NewRow();

                dr["KeyID"] = keyid;
                dr["TradeManuallyCode"] = this.TradeManuallyCode.Text;
                dr["TraderAmount"] = traderamount;
                dr["StoreID"] = storeid;
                dr["EarnPoint"] = earnpoint;

                //dr["BrandName"] = this.BrandID.SelectedText;
                //dr["TendName"] = this.TenderID.SelectedText;
                //dr["BusDate"] = this.Busdate.Text;
                dr["StoreName"] = this.rblStore.SelectedItem == null ? "" : this.rblStore.SelectedItem.Text;

                controller.ViewModel.SubTable.Rows.Add(dr);
            }
            BindTradeList(controller.ViewModel.SubTable);
        }

        protected void BindTradeList(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = viewDT;
                this.AddResultListGrid.DataBind();

                SummaryTrade(dt);

                this.BrandID.Enabled = this.TenderID.Enabled = this.Busdate.Enabled = false;
            }
            else
            {
                ClearGird(AddResultListGrid);
            }
        }

        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            if (controller.ViewModel.SubTable != null)
            {
                DataTable addDT = controller.ViewModel.SubTable;

                foreach (int row in AddResultListGrid.SelectedRowIndexArray)
                {
                    string KeyID = AddResultListGrid.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["KeyID"].ToString().Trim() == KeyID)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.SubTable = addDT;
                BindTradeList(controller.ViewModel.SubTable);
            }
        }

        private void SummaryTrade(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                decimal totalamount = Tools.ConvertTool.ConverType<decimal>(dt.Compute(" sum(TraderAmount) ", "").ToString());
                this.lblTotalAmount.Text = totalamount.ToString("N2");

                int totalpoints = Tools.ConvertTool.ConverType<int>(dt.Compute(" sum(EarnPoint) ", "").ToString());
                this.lblTotalPoints.Text = totalpoints.ToString();
            }
        }

        protected void AddResultListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            AddResultListGrid.PageIndex = e.NewPageIndex;
            BindTradeList(controller.ViewModel.SubTable);
        }
        #endregion
    }
}