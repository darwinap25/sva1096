﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery
{
    public partial class ParentChildList : PageBase
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Grid1.PageSize = webset.ContentPageNum;
                logger.WriteOperationLog(this.PageName, "List");

                RegisterCloseEvent(this.btnClose);

                RptBind("", "ParentCardNumber");

                btnDelete.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);

                Window1.Title = "添加子卡";
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            Edge.SVA.BLL.CardParentChild bll = new SVA.BLL.CardParentChild();
            strWhere = "ParentCardNumber='" + Request.Params["id"] + "'";
            DataSet ds = bll.GetList(strWhere);
            if (ds != null && ds.Tables.Count > 0)
            {
                this.Grid1.RecordCount = ds.Tables[0].Rows.Count;
                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            else
            {
                this.Grid1.Reset();
            }
        }

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            Edge.SVA.BLL.CardParentChild bll = new SVA.BLL.CardParentChild();
            foreach (int row in Grid1.SelectedRowIndexArray)
            {
                bll.Delete(Grid1.DataKeys[row][0].ToString(), Grid1.DataKeys[row][1].ToString());
            }
            RptBind("", "ParentCardNumber");

        }

        protected void btnNew_OnClick(object sender, EventArgs e)
        {
            ExecuteJS(Window1.GetShowReference("AddParentChild.aspx?id=" + Request.Params["id"]));
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "ParentCardNumber");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "ParentCardNumber");
        }
    }
}