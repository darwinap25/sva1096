﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeDenomination;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardIssue.ChangeDenomination
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardAdjust_H, Edge.SVA.Model.Ord_CardAdjust_H>
    {
        ChangeCardDenominationController controller = new ChangeCardDenominationController();
        #region Basic Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.WindowSearch.Title = "搜索";
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);

                btnDeleteResultItem.OnClientClick = this.AddResultListGrid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDeleteResultItem.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDeleteResultItem.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                InitData();
                SVASessionInfo.ChangeCardDenominationController = null;
            }
            controller = SVASessionInfo.ChangeCardDenominationController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardAdjust_H item = this.GetAddObject();

            if (item != null)
            {
                if (item.CardAdjustNumber.Equals(string.Empty))
                {
                    ShowAddFailed();
                    return;
                }

                if (this.OprID.SelectedValue == "23" && string.IsNullOrEmpty(this.OrderPoints.Text))
                {
                    ShowWarning(Resources.MessageTips.YouMustInputPoints);
                    return;
                }
                if (this.OprID.SelectedValue == "22" && string.IsNullOrEmpty(this.OrderAmount.Text))
                {
                    ShowWarning(Resources.MessageTips.YouMustInputAmount);
                    return;
                }

                item.OprID = Convert.ToInt32(this.OprID.SelectedValue); //Convert.ToInt32(Enum.Parse(typeof(Edge.Web.Controllers.CardController.OprID), Edge.Web.Controllers.CardController.OprID.CardChangeDenomination.ToString()));
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = null;
                item.UpdatedBy = null;
                item.ApproveOn = null;
            }
            if (this.AddResultListGrid.RecordCount == 0)
            {
                ShowWarning(Resources.MessageTips.SelectCards);
                return;
            }
            if (!ValidateCardAdjust())
            {
                return;
            }
            int count = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_CardAdjust_H>(item);
            if (count > 0)
            {
                if (controller.ViewModel.CardTable != null)
                {
                    DataTable issuedDT = controller.ViewModel.CardTable;
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(600);

                    DataTable needInsertDt = database.GetTableSchema("Ord_CardAdjust_D");
                    foreach (DataRow row in issuedDT.Rows)
                    {
                        DataRow dr = needInsertDt.NewRow();
                        dr["CardAdjustNumber"] = this.CardAdjustNumber.Text;
                        dr["CardNumber"] = row["CardNumber"];
                        dr["OrderAmount"] = row["OrderAmount"];
                        dr["OrderPoints"] = row["OrderPoints"];
                        needInsertDt.Rows.Add(dr);
                    }
                    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CardAdjust_D");
                    if (!es.Success)
                    {
                        ShowAddFailed();
                        return;
                    }
                }

                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Change Denomination  " + item.CardAdjustNumber + " " + Resources.MessageTips.AddSuccess);

                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Change Denomination  " + item.CardAdjustNumber + " " + Resources.MessageTips.AddFailed);

                ShowAddFailed();
            }
        }

        #endregion

        #region Basic Functions
        private void InitData()
        {
            this.CardAdjustNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderCardChangeDenomination);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);

            if (this.OprID.SelectedValue == "23")
            {
                this.OrderAmount.Enabled = false;
            }
            else
            {
                this.OrderPoints.Enabled = false;
            }
        }

        protected override SVA.Model.Ord_CardAdjust_H GetPageObject(SVA.Model.Ord_CardAdjust_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }
        
        private void SummaryAmounts(DataTable table)
        {
            if (table.Rows.Count > 0)
            {
                //todo
                int totalpoints = Tools.ConvertTool.ConverType<int>(table.Compute(" sum(OrderPoints) ", "").ToString());
                this.lblTotalPoints.Text = totalpoints.ToString();

                decimal totalamount = Tools.ConvertTool.ConverType<decimal>(table.Compute(" sum(OrderAmount) ", "").ToString());
                this.lblTotalAmount.Text = totalamount.ToString("N2");
            }
        }
        #endregion

        #region Search Functions

        private void DeleteItem()
        {
            if (controller.ViewModel.CardTable != null)
            {
                DataTable addDT = controller.ViewModel.CardTable;

                foreach (int row in AddResultListGrid.SelectedRowIndexArray)
                {
                    string CardNumber = AddResultListGrid.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["CardNumber"].ToString().Trim() == CardNumber)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.CardTable = addDT;
                BindResultList(controller.ViewModel.CardTable);

            }
        }

        private void DeleteAllItem()
        {
            if (controller.ViewModel.CardTable != null)
            {
                DataTable addDT = controller.ViewModel.CardTable.Clone();
                controller.ViewModel.CardTable = addDT;
                BindResultList(controller.ViewModel.CardTable);
            }
        }

        private void InitResultList()
        {
            controller.ViewModel.CardTable = null;
            BindResultList(controller.ViewModel.CardTable);
        }

        protected void OprID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.OprID.SelectedValue == "23")
            {
                this.OrderAmount.Enabled = false;
                this.OrderPoints.Enabled = true;
                this.OrderAmount.Text = string.Empty;
            }
            else
            {
                this.OrderPoints.Enabled = false;
                this.OrderAmount.Enabled = true;
                this.OrderPoints.Text = string.Empty;
            }
        }

        #endregion

        #region Search Events

        protected void btnViewSearch_Click(object sender, EventArgs e)
        {
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Card/Add.aspx")));
        }

        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            DeleteItem();
        }

        protected void btnClearAllResultItem_Click(object sender, EventArgs e)
        {
            DeleteAllItem();
        }

        protected void cbSearchAll_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            BindResultList(controller.ViewModel.CardTable);
        }
        #endregion

        //绑定添加结果（新）
        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                if (!string.IsNullOrEmpty(this.OrderPoints.Text))
                {
                    int orderpoints = ConvertTool.ToInt(this.OrderPoints.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["OrderPoints"] = orderpoints;
                        dt.Rows[i]["AdjustPoints"] = ConvertTool.ToInt(dt.Rows[i]["TotalPoints"].ToString()) + orderpoints;
                    }
                }
                if (!string.IsNullOrEmpty(this.OrderAmount.Text))
                {
                    decimal orderamount = ConvertTool.ToDecimal(this.OrderAmount.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["OrderAmount"] = orderamount;
                        dt.Rows[i]["AdjustAmount"] = ConvertTool.ToDecimal(dt.Rows[i]["TotalAmount"].ToString()) + orderamount;
                    }
                }
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.AddResultListGrid.DataBind();

                SummaryAmounts(dt);
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }

            this.btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;
            this.OprID.Enabled = AddResultListGrid.RecordCount > 0 ? false : true;
        }

        protected void OrderPoints_TextChanged(object sender, EventArgs e)
        {
            int orderpoints = ConvertTool.ToInt(this.OrderPoints.Text);
            if (controller.ViewModel.CardTable != null && controller.ViewModel.CardTable.Rows.Count > 0)
            {
                if (orderpoints < -99999999 || orderpoints > 99999999 || this.OrderPoints.Text.Contains('.'))
                {
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90465"));
                    return;
                }
                for (int i = 0; i < controller.ViewModel.CardTable.Rows.Count; i++)
                {
                    DataRow dr = controller.ViewModel.CardTable.Rows[i];
                    if (ConvertTool.ToInt(dr["TotalPoints"].ToString()) + orderpoints < 0)
                    {
                        ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90467"), "line:" + (i + 1)));
                        return;
                    }
                    dr["OrderPoints"] = orderpoints;
                    dr["AdjustPoints"] = ConvertTool.ToInt(dr["TotalPoints"].ToString()) + orderpoints;
                }

                BindResultList(controller.ViewModel.CardTable);
            }
        }

        protected override void ConvertTextboxToDecimal(object sender, EventArgs e)
        {
            base.ConvertTextboxToDecimal(sender, e);
            decimal orderamount = ConvertTool.ToDecimal(this.OrderAmount.Text);
            if (controller.ViewModel.CardTable != null && controller.ViewModel.CardTable.Rows.Count > 0)
            {
                if (orderamount < -99999999 || orderamount > 99999999)
                {                    
                    ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90466"));
                    return;
                }
                for (int i = 0; i < controller.ViewModel.CardTable.Rows.Count; i++)
                {
                    DataRow dr = controller.ViewModel.CardTable.Rows[i];
                    if (ConvertTool.ToDecimal(dr["TotalAmount"].ToString()) + orderamount < 0)
                    {
                        ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90468"), "line:" + (i + 1)));
                        return;
                    }
                    dr["OrderAmount"] = orderamount;
                    dr["AdjustAmount"] = ConvertTool.ToDecimal(dr["TotalAmount"].ToString()) + orderamount;
                }
                BindResultList(controller.ViewModel.CardTable);
            }
        }

        public bool ValidateCardAdjust()
        {
            int orderpoints = ConvertTool.ToInt(this.OrderPoints.Text);
            if (orderpoints < -99999999 || orderpoints > 99999999 || this.OrderPoints.Text.Contains('.'))
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90465"));
                return false;
            }
            for (int i = 0; i < controller.ViewModel.CardTable.Rows.Count; i++)
            {
                DataRow dr = controller.ViewModel.CardTable.Rows[i];
                if (ConvertTool.ToInt(dr["TotalPoints"].ToString()) + orderpoints < 0)
                {
                    ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90467"), "line:" + (i + 1)));
                    return false;
                }
            }
            decimal orderamount = ConvertTool.ToDecimal(this.OrderAmount.Text);
            if (orderamount < -99999999 || orderamount > 99999999)
            {
                ShowWarning(Messages.Manager.MessagesTool.instance.GetMessage("90466"));
                return false;
            }
            for (int i = 0; i < controller.ViewModel.CardTable.Rows.Count; i++)
            {
                DataRow dr = controller.ViewModel.CardTable.Rows[i];
                if (ConvertTool.ToDecimal(dr["TotalAmount"].ToString()) + orderamount < 0)
                {
                    ShowWarning(string.Format(Messages.Manager.MessagesTool.instance.GetMessage("90468"), "line:" + (i + 1)));
                    return false;
                }
            }
            return true;
        }
    }
}