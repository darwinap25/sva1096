﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Common;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.ChangeDenomination;
using System.Data;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardIssue.ChangeDenomination
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardAdjust_H, Edge.SVA.Model.Ord_CardAdjust_H>
    {
        public string id = null;
        ChangeCardDenominationController controller = new ChangeCardDenominationController();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Grid1.PageSize = webset.ContentPageNum;
            this.id = Request.Params["id"];

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                RegisterCloseEvent(btnClose);
                controller = SVASessionInfo.ChangeCardDenominationController;
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());

                ApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());

                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);


                Edge.SVA.Model.Reason reason = new Edge.SVA.BLL.Reason().GetModel(Model.ReasonID.GetValueOrDefault());
                string reasonText = reason == null ? null : DALTool.GetStringByCulture(reason.ReasonDesc1, reason.ReasonDesc2, reason.ReasonDesc3);
                this.ReasonID.Text = reason == null ? "" : ControlTool.GetDropdownListText(reasonText, reason.ReasonCode);

                this.OprIDView.Text = this.OprID.SelectedText;

                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }

                string strWhere = string.Format("Ord_CardAdjust_D.CardAdjustNumber = '{0}'", WebCommon.No_SqlHack(Model.CardAdjustNumber));

                if (Model.ApproveStatus.ToUpper().Trim() == "A") strWhere = string.Format(" Card_Movement.RefTxnNo ='{0}' and Card_Movement.OprID = '{1}' ", WebCommon.No_SqlHack(Model.CardAdjustNumber), WebCommon.No_SqlHack(Model.OprID.ToString()));


                ViewState["StrWhere"] = strWhere;
                ViewState["ApproveStatus"] = Model.ApproveStatus;

                RptBind();

                //汇总金额
                Edge.SVA.BLL.Ord_CardAdjust_D bll = new SVA.BLL.Ord_CardAdjust_D();
                if (Model.ApproveStatus.ToUpper().Trim() == "A")
                {
                    this.lblTotalPoints.Text = controller.GetTotalPonitsWithOrd_CardMovent(strWhere).ToString();
                    this.lblTotalAmount.Text = controller.GetTotalAmountWithOrd_CardMovent(strWhere).ToString("N02");
                }
                else
                {
                    this.lblTotalPoints.Text = controller.GetTotalPonitsWithOrd_CardAdjust_D(strWhere).ToString();
                    this.lblTotalAmount.Text = controller.GetTotalAmountWithOrd_CardAdjust_D(strWhere).ToString("N2");
                }
            }
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }

        private void RptBind()
        {
            if (ViewState["StrWhere"] != null && ViewState["ApproveStatus"] != null)
            {
                string strWhere = ViewState["StrWhere"].ToString();
                string status = ViewState["ApproveStatus"].ToString();

                Edge.SVA.BLL.Ord_CardAdjust_D bll = new Edge.SVA.BLL.Ord_CardAdjust_D();

                DataSet ds = new DataSet();

                if (status.ToUpper().Trim() == "A")
                {
                    this.Grid1.RecordCount = controller.GetCountWithCard_Movement(strWhere);

                    ds = controller.GetPageListWithCard_Movement((this.Grid1.PageSize * (this.Grid1.PageIndex + 1)), strWhere, "Card_Movement.CardNumber");

                    ViewState["ViewTable"] = ds.Tables[0];

                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.RecordCount = controller.GetCountWithCard(strWhere);

                    ds = controller.GetPageListWithCard((this.Grid1.PageSize * (this.Grid1.PageIndex + 1)), strWhere, "Ord_CardAdjust_D.CardNumber");

                    ViewState["ViewTable"] = ds.Tables[0];

                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();

                }

                this.OrderPoints.Text = ds.Tables[0].Rows[0]["OrderPoints"].ToString();
                this.OrderAmount.Text = ds.Tables[0].Rows[0]["OrderAmount"].ToString();
            }
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;
            RptBind();
        }
    }
}