﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using Edge.Web.Controllers;
using Edge.Utils;
using FineUI;
using System.Text;
using Edge.Web.Controllers.Operation.CardManagement.ChangeManagement.CardIssue;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardIssue
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardAdjust_H, Edge.SVA.Model.Ord_CardAdjust_H>
    {
        CardIssueController controller = new CardIssueController();
        #region Basic Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                //this.Window1.Title = "搜索";
                this.WindowSearch.Title = "搜索";
                RegisterCloseEvent(btnClose);

                Edge.Web.Tools.ControlTool.BindReasonType(ReasonID);
                //Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "CouponTypeID =-1");
                Edge.Web.Tools.ControlTool.BindBrand(Brand);

                this.CardStatus.Text = Tools.DALTool.GetCouponTypeStatusName((int)Controllers.CardController.CardStatus.Issued);

                btnDeleteResultItem.OnClientClick = this.AddResultListGrid.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnDeleteResultItem.ConfirmIcon = FineUI.MessageBoxIcon.Question;
                btnDeleteResultItem.ConfirmText = Resources.MessageTips.ConfirmDeleteRecord;

                InitData();
                SVASessionInfo.CardIssueController = null;
            }
            controller = SVASessionInfo.CardIssueController;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardAdjust_H item = this.GetAddObject();

            if (item != null)
            {
                if (item.CardAdjustNumber.Equals(string.Empty))
                {
                    ShowAddFailed();
                    return;
                }
                item.BrandCode = Tools.DALTool.GetBrandCode(Tools.ConvertTool.ToInt(this.Brand.SelectedValue), null);//Add Brand Code
                item.StoreCode = Tools.DALTool.GetStoreCode(Tools.ConvertTool.ToInt(this.StoreID.SelectedValue), null);//Add Store Code
                item.OprID = Convert.ToInt32(Enum.Parse(typeof(Edge.Web.Controllers.CardController.OprID), Edge.Web.Controllers.CardController.OprID.CardIssue.ToString()));
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = null;
                item.UpdatedBy = null;
                item.ApproveOn = null;
            }

            //新界面代码
            if (controller.ViewModel.CardTable == null || controller.ViewModel.CardTable.Rows.Count <= 0)
            {
                ShowWarning(Resources.MessageTips.SelectCards);
                return;
            }

            int count = Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_CardAdjust_H>(item);
            if (count > 0)
            {

                //}
                //新界面代码
                if (controller.ViewModel.CardTable != null)
                {
                    DataTable addDT = controller.ViewModel.CardTable;

                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(600);

                    DataTable needInsertDt = database.GetTableSchema("Ord_CardAdjust_D");
                    foreach (DataRow row in addDT.Rows)
                    {
                        DataRow dr = needInsertDt.NewRow();
                        dr["CardAdjustNumber"] = this.CardAdjustNumber.Text;
                        dr["CardNumber"] = row["CardNumber"];
                        needInsertDt.Rows.Add(dr);
                    }
                    DatabaseUtil.Interface.IExecStatus es = database.InsertBigData(needInsertDt, "Ord_CardAdjust_D");
                    if (!es.Success)
                    {
                        ShowAddFailed();
                        return;
                    }

                }
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Issue  " + item.CardAdjustNumber + " " + Resources.MessageTips.AddSuccess);

                CloseAndRefresh();
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Add Card Issue  " + item.CardAdjustNumber + " " + Resources.MessageTips.AddFailed);

                ShowAddFailed();
            }

        }

        #endregion

        #region Basic Functions
        private void InitData()
        {
            this.CardAdjustNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderCardIssue);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();
            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);

            //btnAddSearchItem.Enabled = SearchListGrid.RecordCount > 0 ? true : false;
            btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;

            //Tools.ControlTool.BindBatchID(BatchCouponID);
            InitStoreByBrand();
        }

        protected override SVA.Model.Ord_CardAdjust_H GetPageObject(SVA.Model.Ord_CardAdjust_H obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }

        private void SummaryAmounts(DataTable table)
        {
            if (table.Rows.Count > 0)
            {
                decimal totalDenomination = Tools.ConvertTool.ConverType<decimal>(table.Compute(" sum(TotalAmount) ", "").ToString());
                this.lblTotalDenomination.Text = totalDenomination.ToString("N2");
            }
            else
            {
                this.lblTotalDenomination.Text = "0.00";
            }
        }
        #endregion

        #region Search Functions

        private void DeleteItem()
        {
            //老界面代码
            //if (ViewState["AddResult"] != null)
            //{
            //    DataTable addDT = (DataTable)ViewState["AddResult"];

            //    foreach (int row in AddResultListGrid.SelectedRowIndexArray)
            //    {
            //        string couponNumber = AddResultListGrid.DataKeys[row][0].ToString();
            //        for (int j = addDT.Rows.Count - 1; j >= 0; j--)
            //        {
            //            if (addDT.Rows[j]["CouponNumber"].ToString().Trim() == couponNumber)
            //            {
            //                addDT.Rows.Remove(addDT.Rows[j]);
            //            }
            //        }
            //        addDT.AcceptChanges();
            //    }

            //    ViewState["AddResult"] = addDT;
            //    BindResultList();
            //}
            //转移界面删除代码
            if (controller.ViewModel.CardTable != null)
            {
                DataTable addDT = controller.ViewModel.CardTable;

                foreach (int row in AddResultListGrid.SelectedRowIndexArray)
                {
                    string cardNumber = AddResultListGrid.DataKeys[row][0].ToString();
                    for (int j = addDT.Rows.Count - 1; j >= 0; j--)
                    {
                        if (addDT.Rows[j]["CardNumber"].ToString().Trim() == cardNumber)
                        {
                            addDT.Rows.Remove(addDT.Rows[j]);
                        }
                    }
                    addDT.AcceptChanges();
                }

                controller.ViewModel.CardTable = addDT;
                BindResultList(controller.ViewModel.CardTable);

            }
        }

        private void DeleteAllItem()
        {
            //老界面代码
            //if (ViewState["AddResult"] != null)
            //{
            //    DataTable addDT = ((DataTable)ViewState["AddResult"]).Clone();
            //    ViewState["AddResult"] = addDT;
            //    BindResultList();
            //}
            //删除界面代码
            if (controller.ViewModel.CardTable != null)
            {
                DataTable addDT = controller.ViewModel.CardTable.Clone();
                controller.ViewModel.CardTable = addDT;
                BindResultList(controller.ViewModel.CardTable);
            }
        }

        //private void InitSearchList()
        //{
        //    ViewState["SearchResult"] = null;
        //    BindSearchList();
        //}

        private void InitResultList()
        {
            //老界面代码
            //ViewState["AddResult"] = null;
            //BindResultList();
            //删除界面代码
            controller.ViewModel.CardTable = null;
            BindResultList(controller.ViewModel.CardTable);
        }

        #endregion

        #region Search Events

        protected void btnViewSearch_Click(object sender, EventArgs e)
        {
            //InitSearchList();
            //this.Window1.Hidden = false;

            //转移界面
            ExecuteJS(WindowSearch.GetShowReference(string.Format("Card/Add.aspx?BrandID={0}&StoreID={1}", this.Brand.SelectedValue, this.StoreID.SelectedValue)));
        }


        protected void btnDeleteResultItem_Click(object sender, EventArgs e)
        {
            DeleteItem();
        }

        protected void btnClearAllResultItem_Click(object sender, EventArgs e)
        {
            DeleteAllItem();
        }


        protected void AddResultListGrid_PageIndexChange(object sender, GridPageEventArgs e)
        {
            //老界面代码
            //AddResultListGrid.PageIndex = e.NewPageIndex;
            //BindResultList();
            //删除界面代码
            AddResultListGrid.PageIndex = e.NewPageIndex;
            BindResultList(controller.ViewModel.CardTable);
        }


        protected void cbSearchAll_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            //老界面
            //base.WindowEdit_Close(sender, e);
            //InitSearchList();

            base.WindowEdit_Close(sender, e);
            BindResultList(controller.ViewModel.CardTable);
            
        }
        #endregion

        protected void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStoreByBrand();
            //InitCouponTypeByStore();
            //InitSearchList();
        }

        //protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    InitCouponTypeByStore();
        //    InitSearchList();
        //}
        private void InitStoreByBrand()
        {
            Edge.Web.Tools.ControlTool.BindStoreWithBrand(StoreID, Edge.Web.Tools.ConvertTool.ToInt(this.Brand.SelectedValue));
        }


        //绑定添加结果（新）
        private void BindResultList(DataTable dt)
        {
            if (dt != null)
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                //DataTable dt = (DataTable)ViewState["AddResult"];
                this.AddResultListGrid.RecordCount = dt.Rows.Count;
                DataTable viewDT = Edge.Web.Tools.ConvertTool.GetPagedTable(dt, this.AddResultListGrid.PageIndex + 1, this.AddResultListGrid.PageSize);
                this.AddResultListGrid.DataSource = Tools.DALTool.GetCardViewDataTable(viewDT);
                this.AddResultListGrid.DataBind();

                SummaryAmounts(dt);
            }
            else
            {
                this.AddResultListGrid.PageSize = webset.ContentPageNum;
                this.AddResultListGrid.PageIndex = 0;
                this.AddResultListGrid.RecordCount = 0;
                this.AddResultListGrid.DataSource = null;
                this.AddResultListGrid.DataBind();
            }

            this.btnDeleteResultItem.Enabled = btnClearAllResultItem.Enabled = AddResultListGrid.RecordCount > 0 ? true : false;
            this.Brand.Enabled = this.StoreID.Enabled = AddResultListGrid.RecordCount > 0 ? false : true;
        }
    }
}
