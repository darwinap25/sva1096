﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;
using FineUI;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardBatchCreate, Edge.SVA.Model.Ord_CardBatchCreate>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.Web.Tools.ControlTool.BindCardTypeForManual(CardTypeID);
                CardTypeID_SelectedIndexChanged(null, null);
                CardGradeID_SelectedIndexChanged(null, null);
                btnClose.OnClientClick = FineUI.ActiveWindow.GetConfirmHidePostBackReference();
                this.txtCardStatus.Text = Tools.DALTool.GetCardStatusName((int)Controllers.CardController.CardStatus.Dormant);
                InitData();
            }
        }

        private void InitData()
        {
           CardCreateNumber.Text= DALTool.GetREFNOCode(Edge.Web.Controllers.CardController.CardRefnoCode.OrderBatchCreationOfCard);
           CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
           lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
           CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();

           this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Text);

           IssuedDate.Text = Edge.Web.Tools.DALTool.GetSystemDate();
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardBatchCreate item = this.GetAddObject();
          
            if (item != null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Card Create Manual" + item.CardCreateNumber);
                string msg="";
                long remainCards = 0;
                string lastCreatedCard = "";

                int checkResult = Controllers.CardController.GetCreatedCardInfo(item.CardGradeID, item.CardCount, ref remainCards, ref lastCreatedCard, ref msg);
                if (checkResult==-1)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.NumberDuplicate);
                    ShowWarning(Resources.MessageTips.NumberDuplicate + "  " + msg);
                    //this.JscriptPrintAndFocus(Resources.MessageTips.NumberDuplicate + "  " + msg, "", Resources.MessageTips.WARNING_TITLE, this.CardCount.ClientID);
                    return;
                }
                else if(checkResult==-2)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.ExceededNumberOfRange);
                    ShowWarning(Resources.MessageTips.ExceededNumberOfRange);
                    // this.JscriptPrintAndFocus(Resources.MessageTips.ExceededNumberOfRange, "", Resources.MessageTips.WARNING_TITLE, this.CardCount.ClientID);
                    return;
                }

                if (item.CardCreateNumber.Equals(string.Empty))
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Card Number is Empty");
                    ShowWarning(Resources.MessageTips.AddFailed);
                    // JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
                    return;
                }
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedOn = null;
                item.UpdatedBy = null;
                item.ApproveOn = null;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_CardBatchCreate>(item) > 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.AddSuccess);
                CloseAndRefresh();
                //JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.AddFailed);
                ShowAddFailed();
                // JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CardTypeID.SelectedValue))
            { 
                CardGradeID.Items.Clear();
                return;
            }
               
            int cardTypeID = int.Parse(CardTypeID.SelectedValue);
            DataSet cardGradeDS = new Edge.SVA.BLL.CardGrade().GetList(" cardTypeID=" + cardTypeID.ToString() + " order by CardGradeCode");
            this.CreatedCards.Text = null;
            Edge.Web.Tools.ControlTool.BindDataSet(CardGradeID, cardGradeDS, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3","CardGradeCode");

        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CardGradeID.SelectedValue))
            {
                this.CreatedCards.Text = null;
                return;
            }
            int cardGradeID = int.Parse(CardGradeID.SelectedValue);
            Edge.SVA.Model.CardGrade model = new Edge.SVA.BLL.CardGrade().GetModel(cardGradeID);
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();

            if (model != null)
            {
                InitAmount.Text = model.CardTypeInitAmount.GetValueOrDefault().ToString();
                InitPoints.Text = model.CardTypeInitPoints.GetValueOrDefault().ToString();
                ExpiryDate.Text = Edge.Web.Tools.DALTool.GetCardTypeExpiryDate(model);
            }

            GetCreatedInfo(cardGradeID);
        }

        private void GetCreatedInfo(int cardGradeID)
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
            this.CreatedCards.Text = bll.GetCount(string.Format("CardGradeID = {0}", cardGradeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCards = 0;
            string lastCreatedCard = "";
            Controllers.CardController.GetCreatedCardInfo(cardGradeID, 0, ref remainCards, ref lastCreatedCard, ref msg);
            this.RemainCards.Text = remainCards.ToString("N00");
            this.LastCreatedCards.Text = lastCreatedCard;
        }

        protected override SVA.Model.Ord_CardBatchCreate GetPageObject(SVA.Model.Ord_CardBatchCreate obj)
        {
            List<System.Web.UI.Control> list = new List<System.Web.UI.Control>();

            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    foreach (System.Web.UI.Control c in con.Controls) list.Add(c);
                }
            }
            return base.GetPageObject(obj, list.GetEnumerator());
        }
    }
}
