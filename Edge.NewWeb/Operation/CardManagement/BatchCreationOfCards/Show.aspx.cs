﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardBatchCreate, Edge.SVA.Model.Ord_CardBatchCreate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                this.btnClose.OnClientClick = FineUI.ActiveWindow.GetConfirmHidePostBackReference();
                this.CardStatus.Text = Tools.DALTool.GetCardStatusName((int)Controllers.CardController.CardStatus.Dormant);
            }
        }

        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                Edge.SVA.Model.CardGrade cardGrade = new Edge.SVA.BLL.CardGrade().GetModel(Model.CardGradeID);
                this.CardGradeID.Text = cardGrade == null ? "" : DALTool.GetStringByCulture(cardGrade.CardGradeName1, cardGrade.CardGradeName2, cardGrade.CardGradeName3);
                this.CardGradeID.Text = cardGrade == null ? "" : ControlTool.GetDropdownListText(this.CardGradeID.Text, cardGrade.CardGradeCode);

                Edge.SVA.Model.CardType cardType = new SVA.BLL.CardType().GetModel(cardGrade == null ? 0 : cardGrade.CardTypeID);
                this.CardTypeID.Text = cardType == null ? "" : DALTool.GetStringByCulture(cardType.CardTypeName1, cardType.CardTypeName2, cardType.CardTypeName3);
                this.CardTypeID.Text = cardType == null ? "" : ControlTool.GetDropdownListText(this.CardTypeID.Text, cardType.CardTypeCode);

                if (Model.ApproveStatus == "A")
                {
                    Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
                    this.BatchCardID.Text = DALTool.GetCardBatchCode(Model.BatchCardID.GetValueOrDefault(), null);
                    this.btnExport.Visible = true;
                }
                else
                {
                    this.BatchCardID.Text = "";
                    this.btnExport.Visible = false;

                }

                this.lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.Value);
                this.CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.Value);
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);

                this.lblApproveBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                this.ApproveBusDate.Text = Edge.Web.Tools.ConvertTool.ToStringDate(Model.ApproveBusDate.GetValueOrDefault());

                GetCreatedInfo(Model.CardGradeID);
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            string fileName = "";
            int records = 0;

            Edge.SVA.BLL.Ord_CardBatchCreate bll = new SVA.BLL.Ord_CardBatchCreate();
            Edge.SVA.Model.Ord_CardBatchCreate model = bll.GetModel(this.CardCreateNumber.Text.Trim());

            if (model == null)
            {
                ShowWarning(Resources.MessageTips.NoData);
                //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }

            try
            {
                fileName = bll.ExportCSV(model.BatchCardID.GetValueOrDefault(), model.CardCount);
                if (string.IsNullOrEmpty(fileName))
                {
                    ShowWarning(Resources.MessageTips.NoData);
                    //JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                records = model == null ? 0 : model.CardCount;
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);

                Tools.ExportTool.ExportFile(fileName);

                Tools.Logger.Instance.WriteExportLog("Batch Creation of Cards - Manual", fn, start, records, null);

            }
            catch (Exception ex)
            {
                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteExportLog("Batch Creation of Cards - Manual", fn, start, records, ex.Message);
                ShowWarning(ex.Message);
                // JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
            }

        }


        private void GetCreatedInfo(int cardGradeID)
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
            this.CreatedCards.Text = bll.GetCount(string.Format("CardGradeID = {0}", cardGradeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCards = 0;
            string lastCreatedCard = "";
            Controllers.CardController.GetCreatedCardInfo(cardGradeID, 0, ref remainCards, ref lastCreatedCard, ref msg);
            this.RemainCards.Text = remainCards.ToString("N00");
            this.LastCreatedCards.Text = lastCreatedCard;
        }

        protected override void SetObject()
        {
            foreach (System.Web.UI.Control con in this.Panel1.Controls)
            {
                if (con is FineUI.GroupPanel)
                {
                    base.SetObject(Model, con.Controls.GetEnumerator());
                }
            }
        }
    }
}
