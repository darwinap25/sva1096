﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Messages.Manager;
using System.Text;
using Edge.Web.Tools;
using Edge.Web.Controllers.Operation.CardManagement.BatchCreationOfCards;


namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards
{
    public partial class List : PageBase
    {
        //public int pcount;                      //总条数
        //public int page;                        //当前页
        //public int pagesize;                    //设置每页显示的大小

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.pagesize = webset.ContentPageNum;

        //    if (!Page.IsPostBack)
        //    {
        //        RptBind("", "CreatedOn");
        //    }
        //    string url = this.Request.Url.AbsolutePath.Substring(0, this.Request.Url.AbsolutePath.LastIndexOf("/") + 1);

        //    this.lbtnApprove.Attributes["onclick"] = string.Format("return checkSelect( '{0}','{1}');", MessagesTool.instance.GetMessage("10017"), url + "Approve.aspx");
        //    this.lbtnVoid.OnClientClick = "return hasSelect( '" + Resources.MessageTips.ConfirmVoid + " ');";
        //}


        //#region 数据列表绑定

        //private void RptBind(string strWhere, string orderby)
        //{
        //    if (!int.TryParse(Request.Params["page"] as string, out this.page))
        //    {
        //        this.page = 0;
        //    }

        //    Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate();

        //    //获得总条数
        //    this.pcount = bll.GetCount(strWhere);
        //    if (this.pcount > 0)
        //    {
        //        this.lbtnApprove.Attributes.Remove("disabled");
        //        this.lbtnVoid.Attributes.Remove("disabled");

        //    }
        //    else
        //    {
        //        this.lbtnApprove.Attributes["disabled"] = "disabled";
        //        this.lbtnVoid.Attributes["disabled"] = "disabled";
        //    }

        //    DataSet ds = new DataSet();
        //    ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
        //    Tools.DataTool.AddCouponTypeName(ds, "CardGradeName", "CardGradeID");
        //    Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
        //    Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
        //    Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}
        //#endregion

        //protected void lbtnAdd_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("add.aspx");
        //}

        //protected void lbtnVoid_Click(object sender, EventArgs e)
        //{
        //    string ids = "";
        //    for (int i = 0; i < this.rptList.Items.Count; i++)
        //    {
        //        System.Web.UI.Control item = rptList.Items[i];
        //        CheckBox cb = item.FindControl("cb_id") as CheckBox;

        //        if (cb != null && cb.Checked == true)
        //        {
        //            string couponNumber = (item.FindControl("lb_id") as Label).Text;
        //            ids += string.Format("{0};", couponNumber);
        //        }
        //    }
        //    Response.Redirect("Void.aspx?ids=" + ids);
        //}

        //protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string couponNumber = (e.Item.FindControl("lb_id") as Label).Text;
        //        string approveStatus = ((Label)e.Item.FindControl("lblApproveStatus")).Text;
        //        (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = "Show.aspx?id=" + couponNumber;

        //        switch (approveStatus.Substring(0,1).ToUpper().Trim())
        //        {
        //            case "A":
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
        //                break;
        //            case "P":
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Attributes["href"] = "Modify.aspx?id=" + couponNumber;
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                break;
        //            case "V":
        //                (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
        //                (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
        //                (e.Item.FindControl("lblApproveCode") as Label).Text = "";
        //                break;

        //        }
        //    }
        //}


        private const string fields = " [CardCreateNumber],[CardGradeID],[ApprovalCode],[ApproveStatus],[ApproveOn],[ApproveBy],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[CreatedBusDate],[ApproveBusDate]";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Grid1.PageSize = webset.ContentPageNum;

                RptBind("", "CardCreateNumber");
                btnNew.OnClientClick = Window2.GetShowReference("Add.aspx", "新增");
                btnApprove.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
                btnVoid.OnClientClick = Grid1.GetNoSelectionAlertReference(Resources.MessageTips.NotSelected);
            }

        }

        #region 数据列表绑定

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                if (value > 0)
                {
                    this.btnApprove.Enabled = true;
                    this.btnVoid.Enabled = true;
                }
                else
                {
                    this.btnApprove.Enabled = false;
                    this.btnVoid.Enabled = false;
                }
                this.Grid1.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }

        private void RptBind(string strWhere, string orderby)
        {
            try
            {
                #region for search
                if (SearchFlag.Text == "1")
                {
                    StringBuilder sb = new StringBuilder(strWhere);

                    string code = this.Code.Text.Trim();
                    string status = this.Status.SelectedValue.Trim();
                    string CStatrtDate = this.CreateStartDate.Text;
                    string CEndDate = this.CreateEndDate.Text;
                    string AStatrtDate = this.ApproveStartDate.Text;
                    string AEndDate = this.ApproveEndDate.Text;
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        sb.Append(" CardCreateNumber like '%");
                        sb.Append(code);
                        sb.Append("%'");
                    }
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveStatus";
                        sb.Append(descLan);
                        sb.Append(" ='");
                        sb.Append(status);
                        sb.Append("'");
                    }
                    if (!string.IsNullOrEmpty(CStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(CStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(CEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "CreatedOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(CEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    if (!string.IsNullOrEmpty(AStatrtDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" >= Cast('");
                        sb.Append(AStatrtDate);
                        sb.Append("' as DateTime)");
                    }
                    if (!string.IsNullOrEmpty(AEndDate))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }
                        string descLan = "ApproveOn";
                        sb.Append(descLan);
                        sb.Append(" < Cast('");
                        sb.Append(AEndDate);
                        sb.Append("' as DateTime) + 1");
                    }
                    strWhere = sb.ToString();
                }
                #endregion
                //记录查询条件用于排序
                ViewState["strWhere"] = strWhere;


                //Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate()
                //{
                //    StrWhere = strWhere,
                //    Order = orderby,
                //    Fields = fields,
                //    Ascending = false
                //};

                //System.Data.DataSet ds = null;
                //int count = 0;
                //ds = bll.GetList(this.Grid1.PageSize, this.Grid1.PageIndex, out count);
                //this.RecordCount = count;

                //Tools.DataTool.AddCouponTypeName(ds, "CardGradeName", "CardGradeID");
                //Tools.DataTool.AddUserName(ds, "CreatedName", "CreatedBy");
                //Tools.DataTool.AddUserName(ds, "ApproveName", "ApproveBy");
                //Tools.DataTool.AddCardApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

                BatchCreationOfCardsController controller = new BatchCreationOfCardsController();
                int count = 0;
                DataSet ds = controller.GetTransactionList(strWhere, this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
                this.RecordCount = count;
                if (ds != null)
                {
                    this.Grid1.DataSource = ds.Tables[0].DefaultView;
                    this.Grid1.DataBind();
                }
                else
                {
                    this.Grid1.Reset();
                }

                this.Grid1.DataSource = ds.Tables[0].DefaultView;
                this.Grid1.DataBind();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteErrorLog("BatchCreationOfCards", "Load Faild", ex);
            }
        }
        //排序
        private void BindGridWithSort(string sortField, string sortDirection)
        {
            BatchCreationOfCardsController controller = new BatchCreationOfCardsController();
            int count = 0;
            string sortFieldStr = String.Format("{0} {1}", sortField, sortDirection);
            this.SortField.Text = sortFieldStr;
            DataSet ds = controller.GetTransactionList(ViewState["strWhere"].ToString(), this.Grid1.PageSize, this.Grid1.PageIndex, out count, this.SortField.Text);
            this.RecordCount = count;

            DataTable table = ds.Tables[0];

            Grid1.DataSource = table;
            Grid1.DataBind();
        }
        protected void Grid1_Sort(object sender, FineUI.GridSortEventArgs e)
        {
            BindGridWithSort(e.SortField, e.SortDirection);
        }
        #endregion

        #region Event
        protected void SearchButton_Click(object sender, EventArgs e)
        {
            this.Grid1.PageIndex = 0;
            this.SearchFlag.Text = "1";
            RptBind("", "CardCreateNumber");
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}

            //Window2.Title = Resources.MessageTips.Approve;
            //ApproveTxns(sbMsg.ToString(), Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')), "");
            ////StringBuilder sb = new StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            ////Window2.Title = "Approve";
            ////FineUI.PageContext.RegisterStartupScript(Window2.GetShowReference("Approve.aspx?ids=" + sb.ToString().TrimEnd(',')));
            NewApproveTxns(Grid1, Window2);
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //StringBuilder sbMsg = new StringBuilder();
            //foreach (int row in Grid1.SelectedRowIndexArray)
            //{
            //    sb.Append(Grid1.DataKeys[row][0].ToString());
            //    sb.Append(",");
            //    sbMsg.Append(Grid1.DataKeys[row][0].ToString());
            //    sbMsg.Append(";\n");
            //}
            //VoidTxns(sbMsg.ToString(), "location.href='Void.aspx?ids=" + sb.ToString().TrimEnd(',') + "'", ""); 
            ////StringBuilder sb = new StringBuilder();
            ////foreach (int row in Grid1.SelectedRowIndexArray)
            ////{
            ////    sb.Append(Grid1.DataKeys[row][0].ToString());
            ////    sb.Append(",");
            ////}
            ////FineUI.PageContext.Redirect("Void.aspx?ids=" + sb.ToString().TrimEnd(','));
            NewVoidTxns(Grid1, HiddenWindowForm);
        }

        protected void Grid1_PageIndexChange(object sender, FineUI.GridPageEventArgs e)
        {
            Grid1.PageIndex = e.NewPageIndex;

            RptBind("", "CardCreateNumber");
        }
        protected override void WindowEdit_Close(object sender, FineUI.WindowCloseEventArgs e)
        {
            base.WindowEdit_Close(sender, e);
            RptBind("", "CardCreateNumber");
        }
        protected void Grid1_PreRowDataBound(object sender, FineUI.GridPreRowEventArgs e)
        {
            DataRowView drv = e.DataItem as DataRowView;
            if (drv == null) return;

            string approveStatus = drv["ApproveStatus"].ToString().Trim();
            approveStatus = string.IsNullOrEmpty(approveStatus) ? approveStatus : approveStatus.Substring(0, 1).ToUpper().Trim();

            FineUI.WindowField editWF = Grid1.FindColumn("EditWindowField") as FineUI.WindowField;

            switch (approveStatus)
            {
                case "A":
                    editWF.Enabled = false;
                    break;
                case "P":
                    editWF.Enabled = true;
                    drv["ApprovalCode"] = "";
                    break;
                case "V":
                    editWF.Enabled = false;
                    drv["ApprovalCode"] = "";
                    break;
            }
        }

        #endregion
    }
}
