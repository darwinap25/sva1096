﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using System.Data;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards
{
    public partial class Approve : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                btnClose.OnClientClick = FineUI.ActiveWindow.GetHidePostBackReference();
                if (!hasRight)
                {
                    return;
                }
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    ShowWarning(Resources.MessageTips.NotSelected);
                   // JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("TxnNo", typeof(string));
                dt.Columns.Add("ApproveCode", typeof(string));
                dt.Columns.Add("ApprovalMsg", typeof(string));

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                bool isSuccess = false;
                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_CardBatchCreate mode = new Edge.SVA.BLL.Ord_CardBatchCreate().GetModel(id);

                    DataRow dr = dt.NewRow();
                    dr["TxnNo"] = id;
                    dr["ApproveCode"] = CardController.ApproveCardForApproveCode(mode, out isSuccess);
                    if (isSuccess)
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Success :" + id);
                     
                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                    }
                    else
                    {
                        Logger.Instance.WriteOperationLog(this.PageName, "Approve Failed :" + id);

                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                    }


                    dt.Rows.Add(dr);
                }
                this.Grid1.DataSource = dt;
                this.Grid1.DataBind();

            }
        }
    }
}
