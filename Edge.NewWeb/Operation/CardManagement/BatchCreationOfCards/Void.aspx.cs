﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using FineUI;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards
{
    public partial class Void : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!hasRight)
                {
                    return;
                }
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, ActiveWindow.GetHidePostBackReference());

                    //Alert.ShowInTop(Resources.MessageTips.NotSelected, "", MessageBoxIcon.Warning, "location.href='List.aspx'");
                    //JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                 List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ",");

                string resultMsg = CardController.BatchVoidCard(idList, CardController.ApproveType.BatchCreate);
                Logger.Instance.WriteOperationLog(this.PageName, "Batch Void Card:" + ids);
                Alert.ShowInTop(resultMsg, "", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());
                //Alert.ShowInTop(resultMsg, "", MessageBoxIcon.Information, "location.href='List.aspx'");
                // JscriptPrint(resultMsg, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
        }
    }
}
