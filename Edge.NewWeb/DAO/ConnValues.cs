﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Web;
using System.Text;
using System.Data.SqlClient;

namespace Edge.Web.DAO
{

    public class ConnValues
    {

        public string conn()
        {

            string connstring = null;

            connstring =  ConfigurationSettings.AppSettings["ConnectionString"]; 
            //ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            return connstring;

        }

    }
}