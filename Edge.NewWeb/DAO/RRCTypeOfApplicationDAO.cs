﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.DBProvider;
using System.Data.SqlClient;
using System.Data;
using Edge.SVA.Model.Domain.SVA;
namespace Edge.Web.DAO
{
    public class RRCTypeOfApplicationDAO
    {
        public RRCTypeOfApplicationDAO()
        { }

        public string AddRRCTypeOfApplication(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            RRCTypeOfApplicationMaintenance vo = (RRCTypeOfApplicationMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[3];

            @params[0] = new SqlParameter("@rrctypeofapplicationcode", SqlDbType.Int);
            @params[0].Value = vo.RRCTypeOfApplicationCode;

            @params[1] = new SqlParameter("@rrctypeofapplication", SqlDbType.VarChar);
            @params[1].Value = vo.RRCTypeOfApplication;

            @params[2] = new SqlParameter("@result", SqlDbType.Char);
            @params[2].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("AddRRCTypeOfApplication", CommandType.StoredProcedure, @params);

                return @params[2].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string UpdateRRCTypeOfApplication(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            RRCTypeOfApplicationMaintenance vo = (RRCTypeOfApplicationMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[3];

            @params[0] = new SqlParameter("@rrctypeofapplicationcode", SqlDbType.Int);
            @params[0].Value = vo.RRCTypeOfApplicationCode;

            @params[1] = new SqlParameter("@rrctypeofapplication", SqlDbType.VarChar);
            @params[1].Value = vo.RRCTypeOfApplication;

            @params[2] = new SqlParameter("@result", SqlDbType.Char);
            @params[2].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("UpdateRRCTypeOfApplication", CommandType.StoredProcedure, @params);

                return @params[2].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

    }
}