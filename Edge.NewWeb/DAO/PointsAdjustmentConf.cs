﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Edge.Web.DBProvider;

namespace Edge.Web.DAO
{
    public class PointsAdjustmentConf
    {

        public PointsAdjustmentConf()
        {
        }
        //Added by Roche as per #1096

        public DataTable getFromPointAdjustmentConf()
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand querySP = new SqlCommand("SELECT RuleID, AccID, CardNumPattern FROM CardType", conn);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = querySP;
                    adapter.Fill(dt);

                    return dt;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public int AddNewPointAdjustmentConfiguration(int accid, string cardprefix)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand querySP = new SqlCommand("INSERT INTO PointAdjustmentConf(AccID, CardPrefix) VALUES (@AccID, @CardPrefix)", conn);

                    querySP.Parameters.AddWithValue("@AccID", accid);
                    querySP.Parameters.AddWithValue("@CardPrefix", cardprefix);

                    int aff = querySP.ExecuteNonQuery();

                    return aff;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public DataTable ViewToEditPointAdjustmentConf(int ruleid)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand querySP = new SqlCommand("sel_viewToEditPointAdjustmentConf", conn);
                    querySP.CommandType = CommandType.StoredProcedure;

                    querySP.Parameters.AddWithValue("@RuleID", ruleid);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = querySP;
                    adapter.Fill(dt);

                    return dt;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public int actionToModifyPointAdjustmentConf(int accid, string cardprefix, int ruleid)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand querySP = new SqlCommand("UPDATE PointAdjustmentConf SET AccID = @AccID, CardPrefix = @CardPrefix WHERE RuleID = @RuleID", conn);

                    querySP.Parameters.AddWithValue("@AccID", accid);
                    querySP.Parameters.AddWithValue("@CardPrefix", cardprefix);
                    querySP.Parameters.AddWithValue("@RuleID", ruleid);

                    int aff = querySP.ExecuteNonQuery();

                    return aff;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        //Point Adjustment
        public void updateFileName(string importmembernumber, string filename)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();

                    SqlCommand query = new SqlCommand("UPDATE Ord_ImportMember_H SET FileName = @filename WHERE ImportMemberNumber = @importnumber", conn);
                    query.Parameters.AddWithValue("@importnumber", importmembernumber);
                    query.Parameters.AddWithValue("@filename", filename);

                    int aff = query.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public int actionToRemoveRule(string ruleid)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();

                    SqlCommand query = new SqlCommand("DELETE FROM PointAdjustmentConf WHERE RuleID = @ruleid", conn);
                    query.Parameters.AddWithValue("@ruleid", ruleid);

                    int aff = query.ExecuteNonQuery();

                    return aff;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public void actionToUpdateCardChecking(string checkingcode)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();

                    SqlCommand querySP = new SqlCommand("updateCardChecking", conn);
                    querySP.CommandType = CommandType.StoredProcedure;

                    querySP.Parameters.AddWithValue("@algoID", checkingcode);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = querySP;
                    adapter.Fill(dt);


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public void UpdateFileStatusToReset(string filename, string realFileName)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    string queryString = "Update Ord_ImportMember_H Set FileName = @realFileName, IsProcessed = 0 WHERE Description = @filename";

                    SqlCommand querySP = new SqlCommand(queryString, conn);

                    querySP.Parameters.AddWithValue("@filename", filename);
                    querySP.Parameters.AddWithValue("@realFileName", realFileName);

                    int aff = querySP.ExecuteNonQuery();


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public bool CheckTxnStatus(string id)
        {
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());

            bool defaultValue = false;

            SqlParameter[] @params = new SqlParameter[1];


            try
            {
                DataSet ds = sqldb.FillDataset("SELECT ImportStatus FROM Ord_ImportMember_H WHERE ImportMemberNumber = '"+ id +"'" , CommandType.Text);

                DataTable dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    string status = dr[0].ToString();

                    if (status == "S" || status == "F")
                    {
                        defaultValue = true;
                    }
                }

                return defaultValue;


            }
            catch (System.Exception ex)
            {
                throw ex;

            }
        }



    }
}