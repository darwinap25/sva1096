﻿using System.Data;
using Edge.Web.DBProvider;
using Edge.SVA.Model.Domain.SVA;
using System.Data.SqlClient;
using Edge.SVA.Model.Master;

namespace Edge.Web.DAO
{
    public class ImportMemberSegmentImportErrorsDAO
    {
        public ImportMemberSegmentImportErrorsDAO()
        { }

        public DataSet GetImportMemberSegmentImportErrors(object obj)
        {
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMemberSegment_ImportErrors vo = (Ord_ImportMemberSegment_ImportErrors)obj;

            SqlParameter[] @params = new SqlParameter[1];

            @params[0] = new SqlParameter("@importmembersegmentnumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberSegmentNumber;

            try
            {
                DataSet ds = sqldb.FillDataset("GetImportMemberSegmentImportErrors", CommandType.StoredProcedure, @params);

                return ds;

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }
    }
}