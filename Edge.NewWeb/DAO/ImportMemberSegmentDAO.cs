﻿using System.Data;
using Edge.Web.DBProvider;
using Edge.SVA.Model.Domain.SVA;
using System.Data.SqlClient;
using Edge.SVA.Model.Master;


namespace Edge.Web.DAO
{
    public class ImportMemberSegmentDAO
    {
        public ImportMemberSegmentDAO()
        { }

        public DataSet GetImportMemberSegments(int pageno, int recsperpage, string importstatus)
        {
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            

            SqlParameter[] @params = new SqlParameter[3];

            @params[0] = new SqlParameter("@page", SqlDbType.Int);
            @params[0].Value = pageno;

            @params[1] = new SqlParameter("@recsperpage", SqlDbType.Int);
            @params[1].Value = recsperpage;

            @params[2] = new SqlParameter("@importstatus", SqlDbType.Char,1);
            @params[2].Value = importstatus;

            try
            {
                DataSet ds = sqldb.FillDataset("GetImportMemberSegments", CommandType.StoredProcedure, @params);

                return ds;

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public DataSet ViewImportMemberSegments(object obj)
        {
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMemberSegment_H vo = (Ord_ImportMemberSegment_H)obj;

            SqlParameter[] @params = new SqlParameter[1];

            @params[0] = new SqlParameter("@importmembersegmentnumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberSegmentNumber;


            try
            {
                DataSet ds = sqldb.FillDataset("ViewImportMemberSegment", CommandType.StoredProcedure, @params);

                return ds;

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string AddImportMemberSegmentHeader(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMemberSegment_H vo = (Ord_ImportMemberSegment_H)obj;

            SqlParameter[] @params = new SqlParameter[10];

            @params[0] = new SqlParameter("@importmembersegmentnumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberSegmentNumber;

            @params[1] = new SqlParameter("@description", SqlDbType.NVarChar);
            @params[1].Value = vo.Description;

            @params[2] = new SqlParameter("@filename", SqlDbType.VarChar);
            @params[2].Value = vo.FileName;

            @params[3] = new SqlParameter("@note", SqlDbType.NVarChar);
            @params[3].Value = vo.Note;

            @params[4] = new SqlParameter("@action", SqlDbType.Char);
            @params[4].Value = vo.Action;

            @params[5] = new SqlParameter("@approvestatus", SqlDbType.Char);
            @params[5].Value = vo.ApproveStatus;

            @params[6] = new SqlParameter("@createdon", SqlDbType.DateTime);
            @params[6].Value = vo.CreatedOn;

            @params[7] = new SqlParameter("@createdby", SqlDbType.VarChar);
            @params[7].Value = vo.CreatedBy;

            @params[8] = new SqlParameter("@createdbusdate", SqlDbType.DateTime);
            @params[8].Value = vo.CreatedBusDate;

            @params[9] = new SqlParameter("@result", SqlDbType.Char, 1);
            @params[9].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("AddImportMemberSegmentHeader", CommandType.StoredProcedure, @params);

                return @params[9].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string UpdateImportMemberSegmentHeader(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMemberSegment_H vo = (Ord_ImportMemberSegment_H)obj;

            SqlParameter[] @params = new SqlParameter[8];

            @params[0] = new SqlParameter("@importmembersegmentnumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberSegmentNumber;

            @params[1] = new SqlParameter("@description", SqlDbType.NVarChar);
            @params[1].Value = vo.Description;

            @params[2] = new SqlParameter("@filename", SqlDbType.VarChar);
            @params[2].Value = vo.FileName;

            @params[3] = new SqlParameter("@note", SqlDbType.NVarChar);
            @params[3].Value = vo.Note;

            @params[4] = new SqlParameter("@action", SqlDbType.Char);
            @params[4].Value = vo.Action;

            @params[5] = new SqlParameter("@updatedon", SqlDbType.DateTime);
            @params[5].Value = vo.UpdatedOn;

            @params[6] = new SqlParameter("@updatedby", SqlDbType.VarChar);
            @params[6].Value = vo.UpdatedOn;

            @params[7] = new SqlParameter("@result", SqlDbType.Char, 1);
            @params[7].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("UpdateImportMemberSegmentHeader", CommandType.StoredProcedure, @params);

                return @params[7].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string VoidImportMemberSegmentHeader(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMemberSegment_H vo = (Ord_ImportMemberSegment_H)obj;

            SqlParameter[] @params = new SqlParameter[4];

            @params[0] = new SqlParameter("@importmembersegmentnumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberSegmentNumber;

            @params[1] = new SqlParameter("@approvebusdate", SqlDbType.DateTime);
            @params[1].Value = vo.ApproveBusDate;

            @params[2] = new SqlParameter("@approveby", SqlDbType.VarChar);
            @params[2].Value = vo.ApproveBy;

            @params[3] = new SqlParameter("@result", SqlDbType.Char, 1);
            @params[3].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("VoidImportMemberSegmentHeader", CommandType.StoredProcedure, @params);

                return @params[3].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string ApproveImportMemberSegmentHeader(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMemberSegment_H vo = (Ord_ImportMemberSegment_H)obj;

            SqlParameter[] @params = new SqlParameter[4];

            @params[0] = new SqlParameter("@importmembersegmentnumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberSegmentNumber;

            @params[1] = new SqlParameter("@approvebusdate", SqlDbType.DateTime);
            @params[1].Value = vo.ApproveBusDate;

            @params[2] = new SqlParameter("@approveby", SqlDbType.VarChar);
            @params[2].Value = vo.ApproveBy;

            @params[3] = new SqlParameter("@result", SqlDbType.Char, 6);
            @params[3].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("ApproveImportMemberSegmentHeader", CommandType.StoredProcedure, @params);

                return @params[3].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

    }
}