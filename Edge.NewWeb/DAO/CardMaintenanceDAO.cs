﻿using System.Data;
using Edge.Web.DBProvider;
using Edge.SVA.Model.Domain.SVA;
using System.Data.SqlClient;

namespace Edge.Web.DAO
{
    public class CardMaintenanceDAO
    {
        public CardMaintenanceDAO()
        { }

            public DataSet GetCardValidityValues(object obj)
            {
                ConnValues ConnStr = new ConnValues();
                SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
                CardValidityMaintenance vo = (CardValidityMaintenance)obj;

                SqlParameter[] @params = new SqlParameter[1];

                @params[0] = new SqlParameter("@id", SqlDbType.Int);
                @params[0].Value = vo.Id;

                try
                {
                    DataSet ds = sqldb.FillDataset("GetCardValidityValues", CommandType.StoredProcedure, @params);

                    return ds;

                }
                catch (System.Exception ex)
                {
                    throw ex;

                }

            }

        
            public string UpdateCardValidityInYears(object obj)
            {
                      
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            CardValidityMaintenance vo = (CardValidityMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[13];

            @params[0] = new SqlParameter("@id", SqlDbType.Int);
            @params[0].Value = vo.Id;

            @params[1] = new SqlParameter("@rrctypeofapplication", SqlDbType.Int);
            @params[1].Value = vo.RRCTypeOfApplicationCode;

            @params[2] = new SqlParameter("@rrccustomertiercode", SqlDbType.Int);
            @params[2].Value = vo.RRCCustomerTierCode;

            @params[3] = new SqlParameter("@cardvalidityinyears", SqlDbType.Int);
            @params[3].Value = vo.CardValidityInYears;

            @params[4] = new SqlParameter("@expirydatefield", SqlDbType.VarChar);
            @params[4].Value = vo.ExpiryDateField;

            @params[5] = new SqlParameter("@withcondition", SqlDbType.Int);
            @params[5].Value = vo.WithCondition;

            @params[6] = new SqlParameter("@conditionfield1", SqlDbType.VarChar);
            @params[6].Value = vo.ConditionField1;

            @params[7] = new SqlParameter("@conditionfield2", SqlDbType.VarChar);
            @params[7].Value = vo.ConditionField2;

            @params[8] = new SqlParameter("@conditionalexpiryperiod", SqlDbType.Int);
            @params[8].Value = vo.ConditionalExpiryPeriodInMonths;

            @params[9] = new SqlParameter("@logicaloperator", SqlDbType.VarChar);
            @params[9].Value = vo.LogicalOperator;

            @params[10] = new SqlParameter("@operation", SqlDbType.Char, 1);
            @params[10].Value = vo.Operation;

            @params[11] = new SqlParameter("@endofmonth", SqlDbType.Int);
            @params[11].Value = vo.EndOfMonth;

            @params[12] = new SqlParameter("@result", SqlDbType.Int);
            @params[12].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("UpdateCardValidityInYears", CommandType.StoredProcedure, @params);

                return @params[12].Value.ToString();


            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string AddCardValidityInYears(object obj)
            {
                      
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            CardValidityMaintenance vo = (CardValidityMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[12];

            @params[0] = new SqlParameter("@rrctypeofapplication", SqlDbType.Int);
            @params[0].Value = vo.RRCTypeOfApplicationCode;

            @params[1] = new SqlParameter("@rrccustomertiercode", SqlDbType.Int);
            @params[1].Value = vo.RRCCustomerTierCode;

            @params[2] = new SqlParameter("@cardvalidityinyears", SqlDbType.Int);
            @params[2].Value = vo.CardValidityInYears;

            @params[3] = new SqlParameter("@expirydatefield", SqlDbType.VarChar);
            @params[3].Value = vo.ExpiryDateField;

            @params[4] = new SqlParameter("@withcondition", SqlDbType.Int);
            @params[4].Value = vo.WithCondition;

            @params[5] = new SqlParameter("@conditionfield1", SqlDbType.VarChar);
            @params[5].Value = vo.ConditionField1;

            @params[6] = new SqlParameter("@conditionfield2", SqlDbType.VarChar);
            @params[6].Value = vo.ConditionField2;

            @params[7] = new SqlParameter("@conditionalexpiryperiod", SqlDbType.Int);
            @params[7].Value = vo.ConditionalExpiryPeriodInMonths;

            @params[8] = new SqlParameter("@logicaloperator", SqlDbType.VarChar);
            @params[8].Value = vo.LogicalOperator;

            @params[9] = new SqlParameter("@operation", SqlDbType.Char,1);
            @params[9].Value = vo.Operation;

            @params[10] = new SqlParameter("@endofmonth", SqlDbType.Int);
            @params[10].Value = vo.EndOfMonth;

            @params[11] = new SqlParameter("@result", SqlDbType.Char, 1);
            @params[11].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("AddCardValidityInYears", CommandType.StoredProcedure, @params);

                return @params[11].Value.ToString();


            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string DeleteCardValidityValues(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            CardValidityMaintenance vo = (CardValidityMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[2];

            @params[0] = new SqlParameter("@id", SqlDbType.Int);
            @params[0].Value = vo.Id;

            @params[1] = new SqlParameter("@result", SqlDbType.Char, 1);
            @params[1].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("DeleteCardValidityValues", CommandType.StoredProcedure, @params);

                return @params[1].Value.ToString();


            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }
    }
}


