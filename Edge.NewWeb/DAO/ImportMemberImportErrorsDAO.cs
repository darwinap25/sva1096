﻿using System.Data;
using Edge.Web.DBProvider;
using Edge.SVA.Model.Domain.SVA;
using System.Data.SqlClient;
using Edge.SVA.Model.Master;
namespace Edge.Web.DAO
{
    public class ImportMemberImportErrorsDAO
    {
         public ImportMemberImportErrorsDAO()
        { }

        public DataSet GetImportMemberImportErrors(object obj)
        {
            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            Ord_ImportMember_ImportErrors vo = (Ord_ImportMember_ImportErrors)obj;

            SqlParameter[] @params = new SqlParameter[1];

            @params[0] = new SqlParameter("@importmembernumber", SqlDbType.VarChar);
            @params[0].Value = vo.ImportMemberNumber;

            try
            {
                DataSet ds = sqldb.FillDataset("GetImportMemberImportErrors", CommandType.StoredProcedure, @params);

                return ds;

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

    }
}