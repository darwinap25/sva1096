﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace Edge.Web.DAO
{
    public class PointsAdjustmentImportErrorsDAO
    {
        public PointsAdjustmentImportErrorsDAO()
        {

        }

        public DataTable getPointsAdjustmentImportErrors(string transactionnumber)
        {
            string connectionString = ConfigurationSettings.AppSettings["ConnectionString"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();

                    SqlCommand querySP = new SqlCommand("GetPointsAdjustmentImportErrors", conn);
                    querySP.CommandType = CommandType.StoredProcedure;

                    querySP.Parameters.AddWithValue("@pointsadjustmentnumber", transactionnumber);

                    DataTable dt = new DataTable();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = querySP;
                    adapter.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


    }
}