﻿using System.Data;
using Edge.Web.DBProvider;
using Edge.SVA.Model.Domain.SVA;
using System.Data.SqlClient;

namespace Edge.Web.DAO
{
    public class RRCCustomerTierDAO
    {
        public RRCCustomerTierDAO()
        { }

        public string AddRRCCustomerTier(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            RRCCustomerTierMaintenance vo = (RRCCustomerTierMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[3];

            @params[0] = new SqlParameter("@rrccustomertiercode", SqlDbType.Int);
            @params[0].Value = vo.RRCCustomerTierCode;

            @params[1] = new SqlParameter("@rrccustomertier", SqlDbType.VarChar);
            @params[1].Value = vo.RRCCustomerTier;

            @params[2] = new SqlParameter("@result", SqlDbType.Char);
            @params[2].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("AddRRCCustomerTier", CommandType.StoredProcedure, @params);

                return @params[2].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public string UpdateRRCCustomerTier(object obj)
        {

            ConnValues ConnStr = new ConnValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.conn());
            RRCCustomerTierMaintenance vo = (RRCCustomerTierMaintenance)obj;

            SqlParameter[] @params = new SqlParameter[3];

            @params[0] = new SqlParameter("@rrccustomertiercode", SqlDbType.Int);
            @params[0].Value = vo.RRCCustomerTierCode;

            @params[1] = new SqlParameter("@rrccustomertier", SqlDbType.VarChar);
            @params[1].Value = vo.RRCCustomerTier;

            @params[2] = new SqlParameter("@result", SqlDbType.Char);
            @params[2].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("UpdateRRCCustomerTier", CommandType.StoredProcedure, @params);

                return @params[2].Value.ToString();

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }
    }
}