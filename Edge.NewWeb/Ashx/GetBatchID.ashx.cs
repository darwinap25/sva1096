﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Utils.Tools;

namespace Edge.Web.Ashx
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    public class GetBatchID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string partialBatchCode = context.Request.Params["batchCode"];
            string strCoupontTypeID = context.Request.Params["couponTypeID"];
            string strTop = context.Request.Params["top"];

            if (string.IsNullOrEmpty(partialBatchCode)) return;

            int top = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(strTop);
            int couponTpypID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(strCoupontTypeID);

            if (top == default(int)) top = 10;
            Dictionary<int, string> batchIDS = null;
            if (couponTpypID > 0)
            {
                batchIDS = Edge.Web.Controllers.BatchController.GetInstance().GetBatch(10, partialBatchCode, couponTpypID);
            }
            else
            {
                batchIDS = Edge.Web.Controllers.BatchController.GetInstance().GetBatch(10, partialBatchCode);
            }
            if (batchIDS.Count <= 0) return;

            IList<JSonModel> modelList = new List<JSonModel>();

            foreach(KeyValuePair<int,string> batch in batchIDS)
            {
                modelList.Add(new JSonModel() { BatchCouponCode = batch.Value, BatchCouponID = batch.Key });
            }            

            string list = Edge.Common.JsonHelper.ListToJson<JSonModel>(modelList,"list");

            context.Response.ContentType = "Application/json";
            context.Response.Write(list);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class JSonModel
    {
        public string BatchCouponCode { get; set; }
        public int BatchCouponID { get; set; }
    }
}
