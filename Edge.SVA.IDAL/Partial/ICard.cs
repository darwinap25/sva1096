﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface ICard
    {
        DataSet GetListWithBatch(int Top, string strWhere, string filedOrder);
        DataSet GetListForTotal(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times);
        DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields, int times);
        int GetCount(string strWhere, int times);
        int GetCountWithBatch(string strWhere);
        DataSet GetPageListWithBatch(int pageSize, int currentPage, string strWhere, string filedOrder);
        bool ExsitCard(Model.Ord_ImportCardUID_H model);
        bool ExsitCard(Model.Ord_CardAdjust_H model);
        bool ExsitCard(string cardNumber);
        DataSet GetListForBatchOperation(int Top, string strWhere, string filedOrder);
        bool ValidCardStatus(Model.Ord_CardAdjust_H model, params int[] CardStatus);
        Edge.SVA.Model.Card GetModelByUID(string cardUID);
    }
}
