﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface ICouponAutoPickingRule_D
    {
        bool DeleteByCode(string couponAutoPickingRuleCode);
    }
}
