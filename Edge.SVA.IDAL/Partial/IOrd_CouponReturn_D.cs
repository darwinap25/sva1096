﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_CouponReturn_D
    {
        bool DeleteByOrder(string couponReturnNumber);
    }
}
