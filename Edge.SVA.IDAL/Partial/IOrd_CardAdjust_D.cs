﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_CardAdjust_D
    {
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetListWithCard(string strWhere);

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        int GetCountWithCard(string strWhere);

        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetPageListWithCard(int pageSize, int currentPage, string strWhere, string filedOrder);

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        int GetCountWithCard_Movement(string strWhere);
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetPageListWithCard_Movement(int pageSize, int currentPage, string strWhere, string filedOrder);

        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetPageListWithCard_Movement1(int pageSize, int currentPage, string strWhere, string filedOrder);

        int GetCountWithCard_Movement1(string strWhere);

        decimal GetAllDenominationWithCard(string strWhere);

        decimal GetAllDenominationWithOrd_CardAdjust_D(string strWhere);

        decimal GetAllDenominationWithCard_Movement(string strWhere);

        void GetAllDenominationWithCard_Movement(string strWhere, out decimal openBal, out decimal amount, out decimal closeBal);
    }
}
