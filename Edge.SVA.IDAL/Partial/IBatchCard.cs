﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface IBatchCard
    {

        Dictionary<int, string> GetBatchID(int top);
        Dictionary<int, string> GetBatchID(int top, string partialBatchID);
        Dictionary<int, string> GetBatchID(int top, int couponTypeID);
        Dictionary<int, string> GetBatchID(int top, string partialBatchID, int couponTypeID);
        //Add by Alex 2014-06-09 ++
        bool ExistBatchCode(string batchCardCode);
        //Add by Alex 2014-06-09 --

    }
}
