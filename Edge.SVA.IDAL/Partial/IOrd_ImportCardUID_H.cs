﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface IOrd_ImportCardUID_H
    {

        bool ExistCardUID(List<string> couponUIDS);
        bool ExistCardUID(string beginUID, string endUID, bool isCheckdigit);
        bool Update(Model.Ord_ImportCardUID_H model, int times);
        string ExportCSV(string importCardNumber, int cardCount);

    }
}
