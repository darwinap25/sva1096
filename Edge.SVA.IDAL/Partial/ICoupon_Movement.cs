﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface ICoupon_Movement
    {
        DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, string fields);
        string GetTxnType(int oprID);
    }
}
