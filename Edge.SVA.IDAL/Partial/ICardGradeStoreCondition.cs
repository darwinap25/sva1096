﻿using System;
using System.Data;
using System.Collections.Generic;
namespace Edge.SVA.IDAL
{
	/// <summary>
	/// 接口层卡级别的店铺条件
	/// </summary>
    public partial interface ICardGradeStoreCondition
    {
        int AddList(List<Edge.SVA.Model.CardGradeStoreCondition> modelList);

        int UpdateList(List<Edge.SVA.Model.CardGradeStoreCondition> insertList, List<Edge.SVA.Model.CardGradeStoreCondition> deleteList);
	} 
}
