﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface ICard_Movement
    {
         /// <summary>
         /// 获得总条数.Card_Move_Movement和Card内联查询
         /// </summary>
         /// <param name="strWhere"></param>
         /// <returns></returns>
        int GetCountWithCard(string strWhere);
        /// <summary>
        ///  /// 获得查询分页数据.Card_Move_Movement和Card内联查询
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="currentPage"></param>
        /// <param name="strWhere"></param>
        /// <param name="filedOrder"></param>
        /// <returns></returns>
        DataSet GetListWithCard(int pageSize, int currentPage, string strWhere, string filedOrder);
    }
}
