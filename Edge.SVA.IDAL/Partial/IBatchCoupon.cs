﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.IDAL
{
    public partial interface IBatchCoupon
    {
        Dictionary<int, string> GetBatchID(int top);
        Dictionary<int, string> GetBatchID(int top, string partialBatchID);
        Dictionary<int, string> GetBatchID(int top, int couponTypeID);
        Dictionary<int, string> GetBatchID(int top, string partialBatchID, int couponTypeID);
        bool ExistBatchCode(string batchCouponCode);
        DataSet GetBatchID();
        DataSet GetBatchIDByType(int couponTypeID);
    }
}
