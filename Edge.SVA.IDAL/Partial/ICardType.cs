﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.IDAL
{
    public partial interface ICardType
    {
        List<int> GetCardTypes(int brandID);
        //Add by Alex 2014-06-09 ++
        SVA.Model.CardType GetImportCardType(string cardTypeCode);
        //Add by Alex 2014-06-09 --
    }
}
