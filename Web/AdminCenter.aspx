﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminCenter.aspx.cs" Inherits="Edge.Web.AdminCenter" %>

<%@ Register src="Controls/checkright.ascx" tagname="checkright" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>管理中心首页</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <b>您当前的位置： 管理中心首页</b></div>
    <%--    <span class="add"><a href="AdminConfig.aspx">修改配置信息</a></span>--%>
    <div class="spClear">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                系统基本信息
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                网站名称：
            </td>
            <td width="75%">
                <%=webset.WebName.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                网站域名：
            </td>
            <td>
                <%=webset.WebUrl.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                安装目录：
            </td>
            <td>
                <%=webset.WebPath.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                后台目录：
            </td>
            <td>
                <%=webset.WebManagePath.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <%=webset.WebTel.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                传真号码：
            </td>
            <td>
                <%=webset.WebFax.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                电子邮箱：
            </td>
            <td>
                <%=webset.WebEmail.ToString()%>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                服务器信息
            </th>
        </tr>
        <tr>
            <td align="right">
                服务器名称：
            </td>
            <td>
                <%=Server.MachineName%>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_Start服务器IP：Translate__Special_121_End
            </td>
            <td>
                <%=Request.ServerVariables["LOCAL_ADDR"] %>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_Start NET框架版本：Translate__Special_121_End
            </td>
            <td>
                <%=Environment.Version.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                操作系统：
            </td>
            <td>
                <%=Environment.OSVersion.ToString()%>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_StartIIS环境：Translate__Special_121_End
            </td>
            <td>
                <%=Request.ServerVariables["SERVER_SOFTWARE"]%>
            </td>
        </tr>
        <tr>
            <td align="right">
                服务器端口：
            </td>
            <td>
                <%=Request.ServerVariables["SERVER_PORT"]%>
            </td>
        </tr>
        <tr>
            <td align="right">
                虚拟目录绝对路径：
            </td>
            <td>
                <%=Request.ServerVariables["APPL_PHYSICAL_PATH"]%>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_StartHTTPS支持：Translate__Special_121_End
            </td>
            <td>
                <%=Request.ServerVariables["HTTPS"]%>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_StartSession总数：Translate__Special_121_End
            </td>
            <td>
                <%=Session.Keys.Count.ToString()%>
                </td>
        </tr>
    </table>
    
    </form>
</body>
</html>
