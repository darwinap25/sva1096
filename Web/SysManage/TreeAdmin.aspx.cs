﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.SysManage
{
    public partial class TreeAdmin : Edge.Web.UI.ManagePage
    {
        AccountsPrincipal user;
        User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                if (Session["UserInfo"] == null)
                {
                    return;
                }
                this.TreeView1.ShowCheckBoxes = TreeNodeTypes.All;
                currentUser = (Edge.Security.Manager.User)Session["UserInfo"];
                Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
                DataSet ds;
                ds = sm.GetTreeListByLan("", Session["SiteLanguage"].ToString());
                BindTreeView("sysRight", ds.Tables[0]);

            }
        }
        //邦定根节点
        public void BindTreeView(string TargetFrame, DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + 0);//　选出所有子节点	

            //菜单状态
            //string MenuExpanded = ConfigurationManager.AppSettings.Get("MenuExpanded");
            //bool menuExpand = bool.Parse(MenuExpanded);

            TreeView1.Nodes.Clear(); // 清空树
            foreach (DataRow r in drs)
            {
                string nodeid = r["NodeID"].ToString();
                string text = r["Text"].ToString();
                string parentid = r["ParentID"].ToString();
                string location = r["Location"].ToString();
                string url = string.Format("modify.aspx?id={0}", nodeid);
                string imageurl = string.Format("../{0}",r["ImageUrl"].ToString());
                int permissionid = int.Parse(r["PermissionID"].ToString().Trim());
                string framename = TargetFrame;

                //treeview set
                //this.TreeView1.Font.Name = "宋体";
                this.TreeView1.Font.Size = FontUnit.Parse("9");

                ////权限控制菜单		
                //if ((permissionid == -1) || (user.HasPermissionID(permissionid)))//绑定用户有权限的和没设权限的（即公开的菜单）
                //{
                    // Microsoft.Web.UI.WebControls.TreeNode rootnode = new Microsoft.Web.UI.WebControls.TreeNode();
                    TreeNode rootnode = new TreeNode();
                    rootnode.Text = text;
                    rootnode.Value = nodeid;
                    rootnode.NavigateUrl = url;
                    rootnode.Target = framename;

                    rootnode.Expanded = true;
                    rootnode.ImageUrl = imageurl;

                    TreeView1.Nodes.Add(rootnode);

                    int sonparentid = int.Parse(nodeid);// or =location
                    CreateNode(framename, sonparentid, rootnode, dt);
               // }


            }


            //treeview set
            this.TreeView1.ShowLines = true;
            this.TreeView1.ShowExpandCollapse = true;//是否显示前面的加减号

        }

        //邦定任意节点
        public void CreateNode(string TargetFrame, int parentid, TreeNode parentnode, DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);//选出所有子节点			
            foreach (DataRow r in drs)
            {
                string nodeid = r["NodeID"].ToString();
                string text = r["Text"].ToString();
                string location = r["Location"].ToString();
                string url = string.Format("modify.aspx?id={0}", nodeid);
                string imageurl = string.Format("../{0}", r["ImageUrl"].ToString());
                int permissionid = int.Parse(r["PermissionID"].ToString().Trim());
                string framename = TargetFrame;

                //权限控制菜单
                //if ((permissionid == -1) || (user.HasPermissionID(permissionid)))
                //{

                    TreeNode node = new TreeNode();
                    node.Text = text;
                    node.Value = nodeid;
                    node.NavigateUrl = url;
                    node.Target = TargetFrame;
                    node.ImageUrl = imageurl;
                    node.Expanded = false;
                    //node.Expanded=true;
                    int sonparentid = int.Parse(nodeid);// or =location

                    if (parentnode == null)
                    {
                        TreeView1.Nodes.Clear();
                        parentnode = new TreeNode();
                        TreeView1.Nodes.Add(parentnode);
                    }
                    parentnode.ChildNodes.Add(node);
                    CreateNode(framename, sonparentid, node, dt);
                //}//endif

            }//endforeach		

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (TreeNode tn in this.TreeView1.CheckedNodes)
            {
              
                Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
                //保存日志
                // SaveLogs("");
                Logger.Instance.WriteOperationLog(this.PageName, "Delete Tree Menu : "+tn.Text);

                sm.DelTreeNode(int.Parse(tn.Value));
            }
            string msg = string.Format(Resources.MessageTips.DeleteMenu, this.TreeView1.CheckedNodes.Count);
            JscriptPrint(msg, "TreeAdmin.aspx", Resources.MessageTips.SUCESS_TITLE);
          
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }
    }
}
