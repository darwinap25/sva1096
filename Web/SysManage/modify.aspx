﻿<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<%@ Page language="c#"  ValidateRequest="false" Codebehind="modify.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.SysManage.modify" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<HTML>
	<head id="Head1" runat="server">
		<title>Index</title>
	</head>
	<body style="padding:10px;">
    <form id="Form1" method="post" runat="server">		
    <script language="javascript" type="text/javascript">
        function imgchang() {
            if (document.Form1.imgsel.selectedIndex != 0) {
                document.Form1.imgview.src = '../' + document.Form1.imgsel.options[document.Form1.imgsel.selectedIndex].value;
                document.Form1.hideimgurl.value = document.Form1.imgsel.options[document.Form1.imgsel.selectedIndex].value;
            }
            else {
                document.Form1.imgview.src = '../Images/MenuImg/folder16.gif';
                document.Form1.hideimgurl.value = 'Images/MenuImg/folder16.gif';
            }
        }
		</script>
	<div class="navigation"><b>您当前的位置：编辑节点</b></div>
    <div class="spClear"></div>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
		<tr>
          <td colspan="2">信息添加，请详细填写下列信息，带有*的必须填写。</td>
        </tr>
		<tr>
			<td width="25%" align="right">
				*编号：
			</td>
			<td width="75%">
				<asp:Label id="lblID" runat="server"></asp:Label></td>
		</tr>
		<tr>
			<td align="right">
				*名称：
			</td>
			<td><asp:textbox id="txtName" runat="server" Width="200px" MaxLength="20"></asp:textbox></td>
		</tr>
		<tr>
			<td align="right">
				*父类：
			</td>
			<td><asp:DropDownList id="listTarget" runat="server" Width="200px">
					<asp:ListItem Value="0" Selected="True" Text="根目录"></asp:ListItem>
				</asp:DropDownList></td>
		</tr>
		<tr>
			<td align="right">
				*排序号：
			</td>
			<td>
				<asp:TextBox id="txtOrderid" runat="server" MaxLength="5" Width="200px"></asp:TextBox></td>
		</tr>
		<tr>
			<td align="right">
				*路径：
			</td>
			<td><asp:textbox id="txtUrl" runat="server" Width="300px" MaxLength="100"></asp:textbox></td>
		</tr>
		<tr>
			<td align="right">
				Translate__Special_121_Start图标(16x16)：Translate__Special_121_End
			</td>
			<td>
				<SELECT id="imgsel" onchange="imgchang()" runat="server" NAME="imgsel">
					<OPTION selected></OPTION>
				</SELECT>
				<IMG id="imgview" src="../Images/MenuImg/folder16.gif" border="0" runat="server">
				<INPUT id="hideimgurl" type="hidden" size="1" runat="server"
					NAME="hideimgurl" value="Images/MenuImg/folder16.gif">
			</td>
		</tr>
		<tr>
			<td align="right" >权限：
			</td>
			<td><asp:DropDownList ID="CategoryList" runat="server" Width="156px" AutoPostBack="True"
                    OnSelectedIndexChanged="ClassList_SelectedIndexChanged">
                </asp:DropDownList><asp:DropDownList id="listPermission" runat="server" Width="300px"></asp:DropDownList></td>
		</tr>
		<tr>
			<td align="right">
				说明：
			</td>
			<td ><asp:textbox id="txtDescription" runat="server" Width="300px"></asp:textbox></td>
		</tr>
		<tr>
					<td align="right">
						是否菜单可见：
					</td>
					<td >
                        <asp:CheckBox ID="ckbKeshi" runat="server" /></td>
				</tr>
		<tr>
            <td colspan="2" align="center">
	       <asp:button id="btnAdd" runat="server" Text="提交" onclick="btnAdd_Click" CssClass="submit"></asp:button>
		      <asp:button id="btnCancel" runat="server" Text="重填" onclick="btnCancel_Click" CssClass="submit"></asp:button>
                 <%--<asp:button id="btnReturn" runat="server" Text="返回"  CssClass="submit" 
                     onclick="btnReturn_Click"></asp:button>--%>
            </td>
        </tr>
			</table>

	<div class="spClear"></div>
	<uc1:CheckRight id="CheckRight1" runat="server" PermissionID=6></uc1:CheckRight>
	</form>
	</body>
</HTML>
