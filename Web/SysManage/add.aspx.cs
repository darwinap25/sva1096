﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Edge.Security.Model;
using System.IO;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.SysManage
{
	/// <summary>
	/// TreeAdd 的摘要说明。
	/// </summary>
    public partial class add : Edge.Web.UI.ManagePage
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				//得到现有菜单
				BindTree();	           
				//得到所有权限
                BindClassTree();
				BindImages();		
			}
			
		}

		#region 
		
		private void BindTree()
		{
			Edge.Security.Manager.SysManage sm=new Edge.Security.Manager.SysManage();			
			DataTable dt=sm.GetTreeList("").Tables[0];


			this.listTarget.Items.Clear();
			//加载树
			this.listTarget.Items.Add(new ListItem("#","0"));
			DataRow [] drs = dt.Select("ParentID= " + 0);	
		

			foreach( DataRow r in drs )
			{
				string nodeid=r["NodeID"].ToString();				
				string text=r["Text"].ToString();					
				//string parentid=r["ParentID"].ToString();
				//string permissionid=r["PermissionID"].ToString();
				text="╋"+text;				
				this.listTarget.Items.Add(new ListItem(text,nodeid));
				int sonparentid=int.Parse(nodeid);
				string blank="├";
				
				BindNode( sonparentid, dt,blank);

			}	
			this.listTarget.DataBind();			

		}

		private void BindNode(int parentid,DataTable dt,string blank)
		{
			DataRow [] drs = dt.Select("ParentID= " + parentid );
			
			foreach( DataRow r in drs )
			{
				string nodeid=r["NodeID"].ToString();				
				string text=r["Text"].ToString();					
				//string permissionid=r["PermissionID"].ToString();
				text=blank+"『"+text+"』";
				
				this.listTarget.Items.Add(new ListItem(text,nodeid));
				int sonparentid=int.Parse(nodeid);
				string blank2=blank+"─";
				

				BindNode( sonparentid, dt,blank2);
			}
		}

        //private void BiudPermTree()
        //{
        //    DataTable tabcategory = Edge.Security.Manager.AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString()).Tables[0];	//todo: 修改成多语言。				
        //    int rc=tabcategory.Rows.Count;
        //    for(int n=0;n<rc;n++)
        //    {
        //        string CategoryID=tabcategory.Rows[n]["CategoryID"].ToString();
        //        string CategoryName=tabcategory.Rows[n]["Description"].ToString();
        //        CategoryName="╋"+CategoryName;
        //        this.listPermission.Items.Add(new ListItem(CategoryName,CategoryID));

        //        DataTable tabforums = Edge.Security.Manager.AccountsTool.GetPermissionsByCategory(int.Parse(CategoryID), Session["SiteLanguage"].ToString()).Tables[0];//todo: 修改成多语言。	
        //        int fc=tabforums.Rows.Count;
        //        for(int m=0;m<fc;m++)
        //        {
        //            string ForumID=tabforums.Rows[m]["PermissionID"].ToString();
        //            string ForumName=tabforums.Rows[m]["Description"].ToString();
        //            ForumName="  ├『"+ForumName+"』";
        //            this.listPermission.Items.Add(new ListItem(ForumName,ForumID));
        //        }
        //    }
        //    this.listPermission.DataBind();	
        //    this.listPermission.Items.Insert(0,"--请选择--");
        //}

		private void BindImages()
		{
			string dirpath=Server.MapPath("../Images/MenuImg");
			DirectoryInfo di = new DirectoryInfo(dirpath);  
			FileInfo[] rgFiles = di.GetFiles("*.gif");  
			this.imgsel.Items.Clear();
			foreach(FileInfo fi in rgFiles) 
			{ 
				ListItem item=new ListItem(fi.Name,"Images/MenuImg/"+fi.Name);
				this.imgsel.Items.Add(item);
			}
			FileInfo[] rgFiles2 = di.GetFiles("*.jpg"); 		
			foreach(FileInfo fi in rgFiles2) 
			{ 
				ListItem item=new ListItem(fi.Name,"Images/MenuImg/"+fi.Name);
				this.imgsel.Items.Add(item);
			}
			this.imgsel.Items.Insert(0,"Default");
			this.imgsel.DataBind();
		}

		#endregion

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
            string orderid = Edge.Common.PageValidate.InputText(txtId.Text, 10);
            string name = txtName.Text;
            string url = Edge.Common.PageValidate.InputText(txtUrl.Text, 100);
            //string imgUrl=Edge.Common.PageValidate.InputText(txtImgUrl.Text,100);
            string imgUrl = this.hideimgurl.Value;

            string target = this.listTarget.SelectedValue;
            int parentid = int.Parse(target);

            string strErr = "";

            if (orderid.Trim() == "")
            {
                strErr += Resources.MessageTips.NumberNotEmpty + "\\n";
            }
            try
            {
                int.Parse(orderid);
            }
            catch
            {
                strErr += Resources.MessageTips.NumberFormatError + "\\n";

            }
            if (name.Trim() == "")
            {
                strErr += Resources.MessageTips.NameNotEmpty + "\\n";
            }

            //if (this.listPermission.SelectedItem.Text.StartsWith("╋"))
            //{
            //    strErr += Resources.MessageTips.NotUsePermission + "\\n";
                
            //}

            if (strErr != "")
            {
                Edge.Common.MessageBox.Show(this, strErr);
                return;
            }

            int permission_id = -1;
            if (!string.IsNullOrEmpty(this.listPermission.SelectedValue))
            {
                permission_id = int.Parse(this.listPermission.SelectedValue);
            }
            int moduleid = -1;
            int keshidm = -1;
            string keshipublic = this.ckbKeshi.Checked ? "true" : "false";
            string comment = Edge.Common.PageValidate.InputText(txtDescription.Text, 100);

            SysNode node = new SysNode();
            node.Text = name;
            node.ParentID = parentid;
            node.Location = parentid + "." + orderid;
            node.OrderID = int.Parse(orderid);
            node.Comment = comment;
            node.Url = url;
            node.PermissionID = permission_id;
            node.ImageUrl = imgUrl;
            node.ModuleID = moduleid;
            node.KeShiDM = keshidm;
            node.KeshiPublic = keshipublic;
            Edge.Security.Manager.SysManage sm = new Edge.Security.Manager.SysManage();
            //if (CheckBox1.Checked)
            //{
            //    Edge.Security.Manager.Permissions p = new Edge.Security.Manager.Permissions();
            //    string permissionName = node.Text;
            //    int parentID = node.ParentID;
            //    if (parentID == 0)
            //    {
            //        //根目录下不能选择同步创建权限
            //        Edge.Common.MessageBox.Show(this.Page, "根目录不能选择同步创建权限，请您手动创建！");
            //        return;
            //    }
            //    SysNode parentNode = new SysNode();
            //    parentNode = sm.GetNode(parentID);
            //    int catalogID = sm.GetPermissionCatalogID(parentNode.PermissionID);
            //    int permissionID = p.Create(catalogID, permissionName);
            //    node.PermissionID = permissionID;
            //}			
            sm.AddTreeNode(node);
            Logger.Instance.WriteOperationLog(this.PageName, "Add Tree Menu : " + node.Text);
            if(chkAddContinue.Checked)
            {
            Response.Redirect("Add.aspx");
            }
            else
            {
                RedirectParentPageTo("TreeAdmin.aspx");	
                //Response.Redirect("TreeAdmin.aspx");
            }
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			txtId.Text="";
			txtName.Text="";
			txtUrl.Text="";
			txtImgUrl.Text="";
			txtDescription.Text="";
			chkAddContinue.Checked=false;
		}

        //protected void btnReturn_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("TreeAdmin.aspx");
        //}

        protected void ClassList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            PermissionsDatabind();
        }


        private void PermissionsDatabind()
        {

            int CategoryId = int.Parse(this.CategoryList.SelectedValue);

            DataSet PermissionsList = AccountsTool.GetPermissionsByCategory(CategoryId, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            this.listPermission.DataSource = PermissionsList;
            this.listPermission.DataTextField = "Description";
            this.listPermission.DataValueField = "PermissionID";
            this.listPermission.DataBind();
        }

        #region 绑定下拉树
        private void BindClassTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());


            this.CategoryList.Items.Clear();
            //加载树
            this.CategoryList.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.CategoryList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindClassNode(sonparentid, ds.Tables[0], blank);

            }
            this.CategoryList.DataBind();

        }

        private void BindClassNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.CategoryList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindClassNode(sonparentid, dt, blank2);
            }
        }
        #endregion
        
	}
}
