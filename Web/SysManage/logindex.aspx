﻿<%@ Page Language="c#" CodeBehind="LogIndex.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.SysManage.LogIndex" %>

<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">

    <script type="text/javascript">
$(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }
        
        $(function() {
            $(".msgtable tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtable tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

    <div class="navigation">
        <b>您当前的位置：系统日志管理</b></div>
    <div class="spClear">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                <tr>
                    <th width="6%">
                        <input type="checkbox" onclick="checkAll(this);" />选择
                    </th>
                    <th width="6%">
                        编号
                    </th>
                    <th width="6%">
                        日期
                    </th>
                    <th width="10%">
                        时间
                    </th>
                    <th width="13%">
                        信息
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                </td>
                <td align="center">
                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("ID")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("datetime")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblTime" runat="server" Text='<%#Eval("datetime")%>'></asp:Label>
                </td>
                <td align="center">
                    <span class="btn_bg"><a href="logshow.aspx?id=<%#Eval("ID") %>">
                        <%#Eval("loginfo")%></a></span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <%--<asp:datagrid id="grid" runat="server" DataKeyField="ID" Width="100%" AutoGenerateColumns="False"
								AllowPaging="True">
								<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
								<Columns>
									<asp:BoundColumn Visible="False" DataField="ID" ReadOnly="True" HeaderText="序号">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="选择">
										<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="25px"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:CheckBox id="DeleteThis" onclick="javascript:CCA(this);" runat="server"></asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="datetime" ReadOnly="True" HeaderText="日期" DataFormatString="{0:d}">
										<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="datetime" HeaderText="时间" DataFormatString="{0:T}">
										<HeaderStyle Width="43px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="logshow.aspx?id={0}" DataTextField="loginfo"
										HeaderText="信息"></asp:HyperLinkColumn>
								</Columns>
								<PagerStyle Visible="False"></PagerStyle>
							</asp:datagrid>--%>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr">
        </div>
        <div class="left">
            <span class="btn_bg">
                <asp:LinkButton ID="lbtnDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                    OnClick="lbtnDel_Click">删除</asp:LinkButton>
                <asp:LinkButton ID="btnDelAll" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                    OnClick="btnDelAll_Click">清除全部日志</asp:LinkButton>
            </span>
        </div>
    </div>
    <%--<table border="0" cellpadding="0" width="700">
					<tr>
						<td><input type="checkbox" name="allbox" onclick="CA();">全选</td>
						<td><asp:Button Text=" 删 除 " ID="Confirm" runat="server" BorderStyle="Groove" BackColor="Transparent"
								BorderColor="RoyalBlue" Height="20px" BorderWidth="1px" Font-Size="XX-Small" onclick="Confirm_Click" /></td>
						<td><asp:Button Text="清除全部日志" ID="btnDelAll" runat="server" BorderStyle="Groove" BackColor="Transparent"
								BorderColor="RoyalBlue" Height="20px" BorderWidth="1px" Font-Size="XX-Small" onclick="btnDelAll_Click" /></td>
					</tr>
				</table>--%>
    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="5"></uc1:CheckRight>
    </form>
</body>
</html>
