﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TreeAdmin.aspx.cs" Inherits="Edge.Web.SysManage.TreeAdmin" %>

<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(window).resize(function() {
            myWindowResize();
        })

        function myWindowResize() {
            var divheight;
            divheight = $(window).height() - $("#btns").height() - 10;

            $("#mainLeft").height(divheight);
            $("#mainDiv").height(divheight);
        }
    </script>

</head>
<body style="padding: 10px;" onload="myWindowResize()">
    <form id="form1" method="post" runat="server">
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="tree_bgcolor">
        <tr>
            <td align="middle" valign="top" style="background: #FFF;">
                <div id="btns">
                    <a id="addLinkt" href="add.aspx" target="sysRight">
                        <button type="button" class="submit" value="添加" onclick="document.getElementById('addLinkt').click();">
                            添加</button></a>
                    <asp:Button ID="btnDelete" runat="server" CssClass="submit" Text="删除" OnClientClick="return confirm('确定删除选择的菜单');"
                        OnClick="btnDelete_Click" />
                </div>
                <div id="mainLeft" style="text-align: left; width: 210px; overflow: scroll; font-size: 12px;">
                Translate__Remain_121_Start
                    <asp:TreeView ID="TreeView1" runat="server">
                    </asp:TreeView>
                    Translate__Remain_121_End
                </div>
            </td>
            <td style="width: 100%;" valign="top">
                <div id="mainDiv" style="text-align: left; width: 100%; font-size: 12px;">
                    <iframe frameborder="0" id="sysRight" name="sysRight" scrolling="yes" src="modify.aspx"
                        style="height: 100%; visibility: inherit; width: 100%; z-index: 2;"></iframe>
                </div>
            </td>
        </tr>
    </table>
    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="5"></uc1:CheckRight>
    </form>
</body>
</html>
