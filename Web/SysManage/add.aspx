﻿<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>

<%@ Page Language="c#" ValidateRequest="false" CodeBehind="add.aspx.cs" AutoEventWireup="True"
    Inherits="Edge.Web.SysManage.add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>TreeAdd</title>
</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">

    <script language="javascript" type="text/javascript">
        function imgchang() {
            if (document.Form1.imgsel.selectedIndex != 0) {
                document.Form1.imgview.src = '../' + document.Form1.imgsel.options[document.Form1.imgsel.selectedIndex].value;
                document.Form1.hideimgurl.value = document.Form1.imgsel.options[document.Form1.imgsel.selectedIndex].value;
            }
            else {
                document.Form1.imgview.src = '../Images/MenuImg/folder16.gif';
                document.Form1.hideimgurl.value = 'Images/MenuImg/folder16.gif';
            }
        }
    </script>

    <div class="navigation">
        <b>您当前的位置：新增节点</b></div>
    <div class="spClear">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <td colspan="2">
                信息添加，请详细填写下列信息，带有 *的必须填写。
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                *名称：
            </td>
            <td width="75%">
                <asp:TextBox ID="txtName" runat="server" Width="200px" MaxLength="20" ToolTip="菜单名称"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                *父类：
            </td>
            <td>
                <asp:DropDownList ID="listTarget" runat="server" Width="200px">
                    <asp:ListItem Value="0" Selected="True">根目录</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                *排序号：
            </td>
            <td>
                <asp:TextBox ID="txtId" runat="server" MaxLength="10" ToolTip="该父类下子节点的排列顺序号"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                链接路径：
            </td>
            <td>
                <asp:TextBox ID="txtUrl" runat="server" MaxLength="100" ToolTip="该菜单链接的页面路径"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                Translate__Special_121_Start图标(16x16)：Translate__Special_121_End
            </td>
            <td>
                <asp:TextBox ID="txtImgUrl" runat="server" MaxLength="100" Visible="False" Text="Images/folder16.gif"></asp:TextBox>
                <select id="imgsel" onchange="imgchang()" runat="server" name="imgsel">
                    <option selected></option>
                </select>
                <img id="imgview" src="../Images/MenuImg/folder16.gif" border="0" runat="server">
                <input id="hideimgurl" type="hidden" size="1" runat="server" name="hideimgurl" value="Images/MenuImg/folder16.gif">
            </td>
        </tr>
        <tr>
            <td align="right">
                绑定权限：
            </td>
            <td>
                <asp:DropDownList ID="CategoryList" runat="server" Width="156px" AutoPostBack="True"
                    OnSelectedIndexChanged="ClassList_SelectedIndexChanged">
                </asp:DropDownList><asp:DropDownList ID="listPermission" runat="server">
                </asp:DropDownList>
               <%-- <asp:CheckBox ID="CheckBox1" runat="server" Text="是否创建同名权限" ToolTip="选择该选项,将创建同名权限,并自动绑定.注意:如果父类为根目录则不能使用该设置" />--%>
            </td>
        </tr>
        <tr>
            <td align="right">
                说明：
            </td>
            <td>
                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                是否菜单可见：
            </td>
            <td>
                <asp:CheckBox ID="ckbKeshi" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;
            </td>
            <td>
                <asp:CheckBox ID="chkAddContinue" runat="server" Text="连续添加"></asp:CheckBox>添加成功后直接跳回此页进行再次添加
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="重填" OnClick="btnCancel_Click" CssClass="submit">
                </asp:Button>
                <%--<asp:button id="btnReturn" runat="server" Text="返回"  CssClass="submit" 
                     onclick="btnReturn_Click"></asp:button>--%>
            </td>
        </tr>
    </table>
    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="6"></uc1:CheckRight>
    </form>
</body>
</html>
