﻿<%@ Page language="c#" Codebehind="LogShow.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.SysManage.LogShow" %>
<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<HTML>
	<head id="Head1" runat="server">
		<title>LogShow</title>
    </head>
	<body style="padding:10px;">
    <form id="Form1" method="post" runat="server">	
    <div class="navigation"><b>您当前的位置：显示日志</b></div>
    <div class="spClear"></div>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
		<tr>
			<td  align="left" colspan="2">错误的详细信息:</td>
		</tr>
		<tr>
		<td width="25%" align="right">
				出错时间：
		</td>
		<td width="75%"><%=strtime%>
		</td>
		</tr>
		<tr>
			<td align="right">错误信息：
			</td>
			<td><%=errmsg%>
			</td>
		</tr>
		<tr>
			<td align="right">堆栈详细：
			</td>
			<td><%=Particular%>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
                <asp:Button ID="btnReturn" runat="server" Text="返回" CssClass="submit" 
                    onclick="btnReturn_Click" />
			</td>
		</tr>
	</table>		
	 <div class="spClear"></div>				
     <uc1:CheckRight id="CheckRight1" runat="server"></uc1:CheckRight>
     </form>
	</body>
</HTML>
