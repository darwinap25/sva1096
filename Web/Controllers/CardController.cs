﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edge.Web.Tools;
using System.Data;
using System.Data.SqlClient;

namespace Edge.Web.Controllers
{
    public class CardController
    {
        public static string ApproveCardForApproveCode(Edge.SVA.Model.Ord_CardBatchCreate model,out bool isSuccess)
        {
            isSuccess = false;

            if (model == null) return "No　Data";

            if (model.ApproveStatus != "P") return "The Transaction Stataus Not Pending";

            string msg = CanApprove(model,out isSuccess);

            if (!isSuccess) return msg;

            model.ApproveStatus = "A";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = Tools.DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());

            Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate();

            if (bll.Update(model))
            {
                model = new Edge.SVA.BLL.Ord_CardBatchCreate().GetModel(model.CardCreateNumber);
                //need to ask 
                return model.ApprovalCode;
            }
            return "";
        }

        public static class CardRefnoCode
        {
            //todo:need to ask
            public static string OrderBatchCreationOfCard = "CAC";
        }
        public static string CanApprove(Edge.SVA.Model.Ord_CardBatchCreate model, out bool isSusscess)
        {
            isSusscess = false;
            if (model == null) return "No Data";
            long remainCards = 0;
            string lastCreatedCard = null;
            string msg = null;
            Controllers.CardController.GetCreatedCardInfo(model.CardGradeID, 0, ref remainCards, ref lastCreatedCard, ref msg);

            if ((remainCards - model.CardCount) >= 0)
            {
                isSusscess = true;
                return null;
            }

            return Messages.Manager.MessagesTool.instance.GetMessage("90417");
        }

        public static string BatchVoidCard(List<string> idList, ApproveType type)
        {
            int success = 0;
            int count = 0;

            foreach (string id in idList)
            {
                if (string.IsNullOrEmpty(id)) continue;
                count++;
                if (type == ApproveType.BatchCreate)
                {
                    Edge.SVA.Model.Ord_CardBatchCreate model = new Edge.SVA.BLL.Ord_CardBatchCreate().GetModel(id);
                    if (CardController.VoidCard(model)) success++;
                }
                else if (type == ApproveType.ImportCard)
                {
                    //Edge.SVA.Model.Ord_ImportCouponUID_H model = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(id);
                    //if (CardController.VoidCoupon(model)) success++;
                }
                else if (type == ApproveType.CardAdjust)
                {
                    //Edge.SVA.Model.Ord_CouponAdjust_H model = new Edge.SVA.BLL.Ord_CouponAdjust_H().GetModel(id);
                    //if (CardController.VoidCoupon(model)) success++;
                }
            }

            return string.Format(Resources.MessageTips.ApproveResult, success, count - success);
        }
        private static bool VoidCard(Edge.SVA.Model.Ord_CardBatchCreate model)
        {
            if (model == null) return false;

            if (model.ApproveStatus != "P") return false;

            model.ApproveStatus = "V";
            model.ApproveOn = DateTime.Now;
            model.ApproveBy = DALTool.GetCurrentUser().UserID;
            model.ApproveBusDate = ConvertTool.ConverNullable<DateTime>(DALTool.GetBusinessDate());
            model.ApprovalCode = null;


            Edge.SVA.BLL.Ord_CardBatchCreate bll = new Edge.SVA.BLL.Ord_CardBatchCreate();

            return bll.Update(model);
        }
        public enum ApproveType
        {
            BatchCreate,
            ImportCard,
            CardAdjust
        }

        public enum CardStatus
        {
            Dormant = 0,
            Issued = 1,
            Active = 2,
            Redeemed = 3,
            Expired = 4,
            Void = 5
        }

        //public static int IsCanCreateCard(int cardGradeID, int createCount, ref string msg)
        //{
        //    string rtn = string.Empty;

        //    IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int,6) , 
        //                                    new SqlParameter("@TypeID", SqlDbType.Int,50),
        //                                    new SqlParameter("@GenQty", SqlDbType.Int,6), 
        //                                    new SqlParameter("@ReturnStartNumber", SqlDbType.VarChar,64),
        //                                    new SqlParameter("@ReturnEndNumber", SqlDbType.VarChar,64),
        //                                    new SqlParameter("@ReturnDuplicateNumber", SqlDbType.VarChar,64)
        //                                  };
        //    parameters[0].Value = 0;
        //    parameters[1].Value = cardGradeID;
        //    parameters[2].Value = createCount;
        //    parameters[3].Direction = ParameterDirection.Output;
        //    parameters[4].Direction = ParameterDirection.Output;
        //    parameters[5].Direction = ParameterDirection.Output;

        //    int count = 0;
        //    int result = DBUtility.DbHelperSQL.RunProcedure("CheckGenerateNumber", parameters, out count);
        //    if (parameters[5].Value != null)
        //    {
        //        msg = parameters[5].Value.ToString();
        //    }
        //    return result;

        //}


        public static int GetCreatedCardInfo(int cardGradeID, int createCount, ref long remainCards, ref string lastCreatedCard, ref string msg)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int,6) , 
                                            new SqlParameter("@TypeID", SqlDbType.Int,50),
                                            new SqlParameter("@GenQty", SqlDbType.Int,6), 
                                            new SqlParameter("@ReturnStartNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@ReturnEndNumber", SqlDbType.VarChar,64),
                                            new SqlParameter("@ReturnDuplicateNumber", SqlDbType.VarChar,64),
                                             new SqlParameter("@RemainQty", SqlDbType.BigInt),
                                            new SqlParameter("@LastNumber", SqlDbType.VarChar,64)
                                          };
            parameters[0].Value = 0;
            parameters[1].Value = cardGradeID;
            parameters[2].Value = createCount;
            parameters[3].Direction = ParameterDirection.Output;
            parameters[4].Direction = ParameterDirection.Output;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;


            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("CheckGenerateNumber", parameters, out count);
            if (parameters[5].Value != null)
            {
                msg = parameters[5].Value.ToString();
            }
            if (parameters[6].Value != null)
            {
                remainCards = Edge.Web.Tools.ConvertTool.ToLong(parameters[6].Value.ToString());
            }
            if (parameters[7].Value != null)
            {
                lastCreatedCard = parameters[7].Value.ToString();
            }
            return result;

        }
    }
}
