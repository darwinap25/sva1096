﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Edge.Web.Controllers
{
    public class CardOrderController
    {
        public static class RefnoCode
        {
            public static string OrderCardPickup = "CPO";
            public static string OrderCardDelivery = "CDO";
            public static string OrderCardForm = "COF";

        }
    }
}