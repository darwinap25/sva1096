﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

namespace Edge.Web
{
    public partial class AdminConfig : Edge.Web.UI.ManagePage
    {
        private Edge.Security.Manager.WebSet bll = new Edge.Security.Manager.WebSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               // chkLoginLevel("editConfig");
                BindLanList();
                LoadWevSet();
            }
        }

        public void LoadWevSet()
        {
            //赋值给对应的控件
            txtWebName.Text = webset.WebName;
            txtWebUrl.Text = webset.WebUrl;
            txtWebTel.Text = webset.WebTel;
            txtWebFax.Text = webset.WebFax;
            txtWebEmail.Text = webset.WebEmail;
            txtWebCrod.Text = webset.WebCrod;
            txtWebKeywords.Text = webset.WebKeywords.ToString();
            txtWebDescription.Text = webset.WebDescription.ToString();
            txtWebCopyright.Text = webset.WebCopyright;

            txtWebPath.Text = webset.WebPath;
            txtWebManagePath.Text = webset.WebManagePath;
            txtWebFilePath.Text = webset.WebFilePath.ToString();
            txtWebFileType.Text = webset.WebFileType.ToString();
            txtWebFileSize.Text = webset.WebFileSize.ToString();
            rblWebLogStatus.SelectedValue = webset.WebLogStatus.ToString();
            txtWebKillKeywords.Text = webset.WebKillKeywords.ToString();
            //rblIsUrlRewrite.SelectedValue = webset.IsUrlRewrite.ToString();
            //rblIsCheckFeedback.SelectedValue = webset.IsCheckFeedback.ToString();
            //rblIsCheckComment.SelectedValue = webset.IsCheckComment.ToString();

            //txtArticlePageNum.Text = webset.ArticlePageNum.ToString();
            //txtArticlePageNum_Client.Text = webset.ArticlePageNum_Client.ToString();
            //txtPicturePageNum.Text = webset.PicturePageNum.ToString();
            //txtPicturePageNum_Client.Text = webset.PicturePageNum_Client.ToString();
            //txtDownPageNum.Text = webset.DownPageNum.ToString();
            //txtDownPageNum_Client.Text = webset.DownPageNum_Client.ToString();
            txtContentPageNum.Text = webset.ContentPageNum.ToString();
            //txtFeedbackPageNum.Text = webset.FeedbackPageNum.ToString();
            //txtFeedbackPageNum_Client.Text = webset.FeedbackPageNum_Client.ToString();
            //txtLinkPageNum.Text = webset.LinkPageNum.ToString();
            //txtCommentPageNum.Text = webset.CommentPageNum.ToString();
            //txtCommentPageNum_Client.Text = webset.CommentPageNum_Client.ToString();
            //txtAdPageNum.Text = webset.AdPageNum.ToString();
            //txtLogPageNum.Text = webset.LogPageNum.ToString();
            //txtManagePageNum.Text = webset.ManagePageNum.ToString();

            //rblIsThumbnail.SelectedValue = webset.IsThumbnail.ToString();
            //txtProWidth.Text = webset.ProWidth.ToString();
            //txtProHight.Text = webset.ProHight.ToString();
            //rblIsWatermark.SelectedValue = webset.IsWatermark.ToString();
            //rblWatermarkStatus.SelectedValue = webset.WatermarkStatus.ToString();
            //txtImgQuality.Text = webset.ImgQuality.ToString();
            //txtImgWaterPath.Text = webset.ImgWaterPath.ToString();
            //txtImgWaterTransparency.Text = webset.ImgWaterTransparency.ToString();
            //txtWaterText.Text = webset.WaterText.ToString();
            //ddlWaterFont.SelectedValue = webset.WaterFont.ToString();
            //txtFontSize.Text = webset.FontSize.ToString();

            txtMaxShowNum.Text = webset.MaxShowNum.ToString();
            txtMaxSearchNum.Text = webset.MaxSearchNum.ToString();


            //优惠券状态设置
            this.mchkVoidStatus.SelectedValue = webset.CouponVoidStatusEnable;
            this.mchkChangeStatus.SelectedValue = webset.CouponStatusChangeStatusEnable;
            this.mchkExpiredStatus.SelectedValue = webset.CouponExpiryDateStatusEnable;
            this.mchkChangeDenomination.SelectedValue = webset.CouponChangeDenominationEnable;

            //撿貨單設置
            this.ddlCouponOrderPickingAllowSetting.SelectedValue = webset.CouponOrderPickingAllowSetting;

            //是否激活優惠券
            this.rblCouponShipmentConfirmationSwitch.SelectedValue = webset.CouponShipmentConfirmationSwitch.ToString();

            ddlLanList.SelectedIndex = ddlLanList.Items.IndexOf(ddlLanList.Items.FindByValue(webset.SiteLanguage));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //赋值给MODEL
                Edge.Security.Model.WebSet model = new Edge.Security.Model.WebSet();
                model.WebName = txtWebName.Text;
                model.WebUrl = txtWebUrl.Text;
                model.WebTel = txtWebTel.Text;
                model.WebFax = txtWebFax.Text;
                model.WebEmail = txtWebEmail.Text;
                model.WebCrod = txtWebCrod.Text;
                model.WebCopyright = txtWebCopyright.Text;
                model.WebKeywords = txtWebKeywords.Text.Trim();
                model.WebDescription = txtWebDescription.Text.Trim();

                model.WebPath = txtWebPath.Text;
                model.WebManagePath = txtWebManagePath.Text;
                model.WebFilePath = txtWebFilePath.Text;
                model.WebFileType = txtWebFileType.Text;
                model.WebFileSize = int.Parse(txtWebFileSize.Text.Trim());
                model.WebLogStatus = int.Parse(rblWebLogStatus.SelectedValue);
                model.WebKillKeywords = txtWebKillKeywords.Text.Trim();
                //model.IsUrlRewrite = int.Parse(rblIsUrlRewrite.SelectedValue);
                //model.IsCheckFeedback = int.Parse(rblIsCheckFeedback.SelectedValue);
                //model.IsCheckComment = int.Parse(rblIsCheckComment.SelectedValue);

                //model.ArticlePageNum = int.Parse(txtArticlePageNum.Text.Trim());
                //model.ArticlePageNum_Client = int.Parse(txtArticlePageNum_Client.Text.Trim());
                //model.PicturePageNum = int.Parse(txtPicturePageNum.Text.Trim());
                //model.PicturePageNum_Client = int.Parse(txtPicturePageNum_Client.Text.Trim());
                //model.DownPageNum = int.Parse(txtDownPageNum.Text.Trim());
                //model.DownPageNum_Client = int.Parse(txtDownPageNum_Client.Text.Trim());
                model.ContentPageNum = int.Parse(txtContentPageNum.Text.Trim());
                //model.FeedbackPageNum = int.Parse(txtFeedbackPageNum.Text.Trim());
                //model.FeedbackPageNum_Client = int.Parse(txtFeedbackPageNum_Client.Text.Trim());
                //model.LinkPageNum = int.Parse(txtLinkPageNum.Text.Trim());
                //model.CommentPageNum = int.Parse(txtCommentPageNum.Text.Trim());
                //model.CommentPageNum_Client = int.Parse(txtCommentPageNum_Client.Text.Trim());
                //model.AdPageNum = int.Parse(txtAdPageNum.Text.Trim());
                //model.LogPageNum = int.Parse(txtLogPageNum.Text.Trim());
                //model.ManagePageNum = int.Parse(txtManagePageNum.Text.Trim());

                //model.IsThumbnail = int.Parse(rblIsThumbnail.SelectedValue);
                //model.ProWidth = int.Parse(txtProWidth.Text.Trim());
                //model.ProHight = int.Parse(txtProHight.Text.Trim());
                //model.IsWatermark = int.Parse(rblIsWatermark.SelectedValue.Trim());
                //model.WatermarkStatus = int.Parse(rblWatermarkStatus.SelectedValue.Trim());
                //model.ImgQuality = int.Parse(txtImgQuality.Text.Trim());
                //model.ImgWaterPath = txtImgWaterPath.Text.Trim();
                //model.ImgWaterTransparency = int.Parse(txtImgWaterTransparency.Text.Trim());
                //model.WaterText = txtWaterText.Text.Trim();
                //model.WaterFont = ddlWaterFont.SelectedValue;
                //model.FontSize = int.Parse(txtFontSize.Text.Trim());

                model.MaxShowNum = int.Parse(txtMaxShowNum.Text.Trim());
                model.MaxSearchNum = int.Parse(txtMaxSearchNum.Text.Trim());

                //保证当前模板不丢失
                model.TemplateSkin = webset.TemplateSkin;
                model.SiteLanguage = ddlLanList.SelectedItem.Value.Trim();

                //优惠券状态设置
                model.CouponVoidStatusEnable = this.mchkVoidStatus.SelectedValue;
                model.CouponStatusChangeStatusEnable = this.mchkChangeStatus.SelectedValue;
                model.CouponExpiryDateStatusEnable = this.mchkExpiredStatus.SelectedValue;
                model.CouponChangeDenominationEnable = this.mchkChangeDenomination.SelectedValue;

                //撿貨單設置
                model.CouponOrderPickingAllowSetting = this.ddlCouponOrderPickingAllowSetting.SelectedValue;

                //是否激活優惠券
                model.CouponShipmentConfirmationSwitch = Tools.ConvertTool.ConverType<int>(this.rblCouponShipmentConfirmationSwitch.SelectedValue);


                ////修改配置信息
                bll.saveConifg(model, Server.MapPath(ConfigurationManager.AppSettings["Configpath"].ToString()));
                //保存日志
                // SaveLogs("[系统管理]修改系统配置文件");

                RefreshParentPage();

                JscriptPrint("System Setting Success!", "AdminCenter.aspx", Resources.MessageTips.SUCESS_TITLE);
            }
            catch
            {

                JscriptPrint("<b>Save Failed !</b>Please check write permissions, if not, please contact the administrator to open the permission to write to the file!", "AdminCenter.aspx", Resources.MessageTips.WARNING_TITLE);
            }
        }

        private void BindLanList()
        {
            Edge.Security.Manager.Lan_List lanBll = new Edge.Security.Manager.Lan_List();
            DataSet LanList = lanBll.GetAllList();
            this.ddlLanList.DataSource = LanList;
            this.ddlLanList.DataValueField = "Lan";
            this.ddlLanList.DataTextField = "LanDesc";
            this.ddlLanList.DataBind();

        }
    }
}
