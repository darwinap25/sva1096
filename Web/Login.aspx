﻿<%@ Page Language="c#" CodeBehind="Login.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Admin.Login"
    EnableTheming="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>:Login</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

//            $("#txtUsername").focus();
        });
    </script>

</head>
<body>
    <form id="form1" method="post" runat="server">
    <div id="login_body">
        <table width="500px" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <td height="50" class="loginnavigation" colspan="3">
                    <img src="images/veilogo.png" width="98" height="50">
                    <img src="images/head_logo.png" width="233" height="50">
                </td>
            </tr>
            <tr>
                <td width="40%" align="right">
                    用户名：
                </td>
                <td width="60%">
                    <input tabindex="1" maxlength="22" size="21" id="txtUsername" runat="server" class="input required"
                        hinttitle="用户名" hintinfo="请输入您的用户名！" />
                </td>
                <td rowspan="3">
                    <asp:ImageButton ID="btnLogin" runat="server" CssClass="login_btn" ImageUrl="Images/login_btn.gif" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    密 码：
                </td>
                <td>
                    <input type="password" tabindex="2" size="22" maxlength="22" id="txtPass" runat="server"
                        class="input required" hinttitle="密码" hintinfo="请输入您的密码！" />
                </td>
            </tr>
            <tr runat="server" id="CheckCodeTR">
                <td align="right">
                    验证码：
                </td>
                <td>
                    <input id="CheckCode" tabindex="3" maxlength="22" size="11" runat="server" class="input required " />
                    <asp:Image ID="Image1" TabIndex="5" runat="server" ImageUrl="ValidateCode.aspx" ToolTip="Not case-sensitive! Red numbers, black letters!"
                        onclick="window.location.reload();"></asp:Image>
                </td>
            </tr>
            <tr>
                <td align="right">
                    语言：
                </td>
                <td colspan="4">
                    <asp:DropDownList ID="ddlLanList" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlLanList_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td align="center" colspan="5">
                   Edge.Web © 2011 - 2012.Version:<asp:Label ID="lblVersion" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div style="margin-top: 10px; text-align: center;">
            <asp:Label ID="lblMsg" runat="server" BackColor="Transparent" ForeColor="Red"></asp:Label></div>
<%--        <div id="login_footer">
            </div>--%>
    </div>
    </form>
</body>
</html>
