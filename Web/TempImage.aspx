﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TempImage.aspx.cs" Inherits="Edge.Web.TempImage" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th align="left">
                图片预览
            </th>
        </tr>
        <tr>
            <td style="text-align: center;">
                <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
                <asp:Image ID="img" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align:center;">
                <input type="button" value="关 闭" class="submit" onclick="javascript:window.top.tb_remove(); " />
            </td>
        </tr>
    </table>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
