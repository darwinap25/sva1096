﻿using System;
using Edge.Security.Manager;
using System.Web.Security;
using Edge.SVA.Model;
using System.Globalization;
using System.Threading;
using System.Configuration;
using System.IO;

namespace Edge.Web.Admin
{

	/// <summary>
	/// Login 的摘要说明。
	/// </summary>
    public partial class Login : Edge.Web.UI.ManagePage
	{
        private static string WebVersion = "";
        Tools.Logger logger = Tools.Logger.Instance;
        private static bool NoCheckCode = true;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// 在此处放置用户代码以初始化页面
            if (!IsPostBack)
            {                
                Edge.Web.Accounts.LanAdmin.LanTools.BindLanList(this.ddlLanList);
                this.ddlLanList.SelectedValue = webset.SiteLanguage;
                SetLanguage(webset.SiteLanguage);


                NoCheckCode = GetCheckCode();

                if (NoCheckCode)
                {
                    this.CheckCodeTR.Visible = false;
                }
                else
                {
                    this.CheckCodeTR.Visible = true;
                }

                if (string.IsNullOrEmpty(WebVersion))
                {
                    this.lblVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }
            }
		}

		#region Web 窗体设计器生成的代码
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnLogin.Click += new System.Web.UI.ImageClickEventHandler(this.btnLogin_Click);

		}
		#endregion

		private void btnLogin_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
            if ((Session["PassErrorCountAdmin"] != null) && (Session["PassErrorCountAdmin"].ToString() != ""))
            {
                int PassErroeCount = Convert.ToInt32(Session["PassErrorCountAdmin"]);
                if (PassErroeCount > 3)
                {
                    txtUsername.Disabled = true;
                    txtPass.Disabled = true;
                    btnLogin.Enabled = false;
                    //this.lblMsg.Text = "对不起，你错误登录了三次，系统登录锁定！";
                    this.lblMsg.Text = Resources.MessageTips.SystemLocked;
                    return;
                }

            }

            if (!NoCheckCode)
            {
                if ((Session["CheckCode"] != null) && (Session["CheckCode"].ToString() != ""))
                {
                    if (Session["CheckCode"].ToString().ToLower() != this.CheckCode.Value.ToLower())
                    {
                        //this.lblMsg.Text = "所填写的验证码与所给的不符 !";
                        this.lblMsg.Text = Resources.MessageTips.SecurityCodeDifferent;
                        Session["CheckCode"] = null;
                        return;
                    }
                    else
                    {
                        Session["CheckCode"] = null;
                    }
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }

			string userName=Edge.Common.PageValidate.InputText(txtUsername.Value.Trim(),30);
			string Password=Edge.Common.PageValidate.InputText(txtPass.Value.Trim(),30);
            logger.WriteOperationLog("Login", " Login Operation userName:" + userName);
            if (Session["SiteLanguage"] == null)
            {
                this.ddlLanList.SelectedValue = webset.SiteLanguage;
                SetLanguage(webset.SiteLanguage);
            }
            AccountsPrincipal newUser = AccountsPrincipal.ValidateLogin(userName, Password, Session["SiteLanguage"].ToString());//todo: 修改成多语言。	
			if (newUser == null)
			{				
				this.lblMsg.Text = "Login Error： " + userName;
                if ((Session["PassErrorCountAdmin"] != null) && (Session["PassErrorCountAdmin"].ToString() != ""))
                {
                    int PassErroeCount = Convert.ToInt32(Session["PassErrorCountAdmin"]);
                    Session["PassErrorCountAdmin"] = PassErroeCount + 1;
                }
                else
                {
                    Session["PassErrorCountAdmin"] = 1;
                }
			}
			else 
			{				
				User currentUser=new Edge.Security.Manager.User(newUser);
                //if (currentUser.UserType != "AA")
                //{
                //    this.lblMsg.Text = "你非管理员用户，你没有权限登录后台系统！";
                //    return;
                //}
				Context.User = newUser;
				if(((SiteIdentity)User.Identity).TestPassword( Password) == 0)
				{
                    this.lblMsg.Text = Resources.MessageTips.PasswordInvalid;
                    if ((Session["PassErrorCountAdmin"] != null) && (Session["PassErrorCountAdmin"].ToString() != ""))
                    {
                        int PassErroeCount = Convert.ToInt32(Session["PassErrorCountAdmin"]);
                        Session["PassErrorCountAdmin"] = PassErroeCount + 1;
                    }
                    else
                    {
                        Session["PassErrorCountAdmin"] = 1;
                    }
				}
				else
				{					
					FormsAuthentication.SetAuthCookie( userName,false );
                    //日志
                    //UserLog.AddLog(currentUser.UserName, currentUser.UserType, Request.UserHostAddress, Request.Url.AbsoluteUri, "登录成功");
					
					Session["UserInfo"]=currentUser;
					Session["Style"]=currentUser.Style;

                   

                    AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());
                    //Edge.SVA.BLL.RelationRoleIssuer bll=new Edge.SVA.BLL.RelationRoleIssuer();
                    //SessionInfo.CardIssuersStr=bll.GetCardIssuersStr(user.RoleIDList);
                    Edge.SVA.BLL.RelationRoleBrand bll = new Edge.SVA.BLL.RelationRoleBrand();
                    SessionInfo.BrandIDsStr = bll.GetBrandIDsStr(user.RoleIDList);

					if(Session["returnPage"]!=null)
					{
						string returnpage=Session["returnPage"].ToString();
						Session["returnPage"]=null;
						Response.Redirect(returnpage);
					}
					else
					{
						//Response.Redirect("main.htm");
                        Response.Redirect("Index.aspx");
					}
				}
			}		
		}

        protected void ddlLanList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetLanguage(this.ddlLanList.SelectedValue);
        }

        protected bool SetLanguage(string lan)
        {
            //Edge.Messages.Manager.MessagesTool.instance.Refresh("SVA", lan);

            Edge.Messages.Manager.MessagesTool.instance.MessageLan = lan;

            if (!string.IsNullOrEmpty(lan))
            {
                UICulture = lan;
                Culture = lan;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lan);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lan);
                Session["SiteLanguage"] = lan;
                Asp.Net.WebFormLib.Factory.CreateITranslater().LanguageLan = Session["SiteLanguage"].ToString();
                return true;
            }
            else
            {
                Session["SiteLanguage"] = webset.SiteLanguage;
                Asp.Net.WebFormLib.Factory.CreateITranslater().LanguageLan = Session["SiteLanguage"].ToString();
                return false;
            }
        }

        private bool GetCheckCode()
        {
            bool result = false;
            try
            {
               string cfgVal= ConfigurationManager.AppSettings["NoCheckCode"];
                result = bool.Parse(cfgVal); 

            }
            catch
            { }

            return result;
        }

       
	}
}
