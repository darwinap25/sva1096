﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Member, Edge.SVA.Model.Member>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataSet ds = new Edge.SVA.BLL.Education().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(EducationID, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3", "EducationCode");
                ds = new Edge.SVA.BLL.Profession().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(ProfessionID, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3", "ProfessionCode");
                ds = new Edge.SVA.BLL.Nation().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(NationID, ds, "NationID", "NationName1", "NationName2", "NationName3","NationCode");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!IsPostBack)
            {

                if (this.Model.MemberPicture == null)
                {
                    this.Image1.Visible = false;
                }
                else
                {
                    Session["TempImage"] = this.Model.MemberPicture;
                    this.Image1.ImageUrl = "~/TempImage.aspx";
                }

                //RptBindContact(Model.MemberID);

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            Edge.SVA.Model.Member item = this.GetUpdateObject();
            if (item != null)
            {
                item.MemberPicture = Session["TempImage"] as byte[];
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;

            }
           
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Member>(item))
            {
               
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
            Session["TempImage"] = null;
            ////Response.Redirect("List.aspx?page=0");
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            Session["TempImage"] = this.MemberPicture.FileBytes;
            Random r = new Random();
            this.Image1.ImageUrl = "~/TempImage.aspx?s=" + r.Next();
            this.Image1.Visible = true;
        }

        //protected void btnDelContact_Click(object sender, EventArgs e)
        //{
        //    for (int i = 0; i < rptList.Items.Count; i++)
        //    {
        //        int id = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
        //        CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
        //        if (cb.Checked)
        //        {
        //            Edge.SVA.BLL.MemberSNSAccount bll = new Edge.SVA.BLL.MemberSNSAccount();
        //            保存日志
        //             SaveLogs("");
        //            bll.Delete(id);
        //        }
        //    }

        //    RptBindContact(string.IsNullOrEmpty(Request.Params["id"].ToString()) ? 0 : int.Parse(Request.Params["id"].ToString()));

        //    JscriptPrint("删除成功！", "#", "Success");

        //}

        //protected void btnSaveContact_Click(object sender, EventArgs e)
        //{
        //    AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
        //    User currentUser = new Edge.Security.Manager.User(user);

        //    Edge.SVA.Model.MemberSNSAccount item = new Edge.SVA.Model.MemberSNSAccount();

        //    item.MemberID = string.IsNullOrEmpty(Request.Params["id"].ToString()) ? 0 : int.Parse(Request.Params["id"].ToString());
        //    item.Note = contactNote.Text;//TODO: 不确定字段，需要修改。
        //    item.Status = 1;
        //    item.SNSTypeID = int.Parse(SNSTypeID.SelectedValue);
        //    item.AccountNumber = ContactNumber.Text;
        //    item.UpdatedBy = currentUser.UserID;
        //    item.UpdatedOn = System.DateTime.Now;
        //    item.UpdatedBy = currentUser.UserID;
        //    item.UpdatedOn = System.DateTime.Now;

        //    Edge.SVA.BLL.MemberSNSAccount bll = new Edge.SVA.BLL.MemberSNSAccount();

        //    int id = bll.Add(item);
        //    if (id > 0)
        //    {
        //        lblMsg.Text = "新增成功！";
        //        lblMsg.Visible = true;
        //        RptBindContact(item.MemberID);
        //    }
        //    else
        //    {
        //        lblMsg.Text = "新增失败！";
        //        lblMsg.Visible = true;
        //        JscriptPrint("新增失败！", "List.aspx?page=0", "Success");
        //    }
        //}

        //#region 数据列表绑定

        //private void RptBindContact(int id)
        //{
        //    Edge.SVA.BLL.MemberSNSAccount bll = new Edge.SVA.BLL.MemberSNSAccount();

        //    DataSet ds = new DataSet();
        //    ds = bll.GetList("MemberID=" + id);
        //    Tools.DataTool.AddSNSTypeName(ds, "SNSName", "SNSTypeID");
        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}
        //#endregion
    }
}
