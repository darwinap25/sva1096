﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.MemberManagement.MemberInformation.Basic
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Member, Edge.SVA.Model.Member>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataSet ds = new Edge.SVA.BLL.Education().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(EducationID, ds, "EducationID", "EducationName1", "EducationName2", "EducationName3", "EducationCode");
                EducationID.Items.Insert(0, new ListItem() { Text = "----------", Value = "-1" });
                ds = new Edge.SVA.BLL.Profession().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(ProfessionID, ds, "ProfessionID", "ProfessionName1", "ProfessionName2", "ProfessionName3", "ProfessionCode");
                ProfessionID.Items.Insert(0, new ListItem() { Text = "----------", Value = "-1" });
                ds = new Edge.SVA.BLL.Nation().GetAllList();
                Edge.Web.Tools.ControlTool.BindDataSet(NationID, ds, "NationID", "NationName1", "NationName2", "NationName3", "NationCode");
                NationID.Items.Insert(0, new ListItem() { Text = "----------", Value = "-1" });
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!IsPostBack)
            {

                if (this.Model.MemberPicture == null)
                {
                    this.Image1.Visible = false;
                }
                else
                {
                    Session["TempImage"] = this.Model.MemberPicture;
                    this.Image1.ImageUrl = "~/TempImage.aspx";
                }
                this.CreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                //RptBindContact(Model.MemberID);
            }
        }
        //#region 数据列表绑定

        //private void RptBindContact(int id)
        //{
        //    Edge.SVA.BLL.MemberSNSAccount bll = new Edge.SVA.BLL.MemberSNSAccount();

        //    DataSet ds = new DataSet();
        //    ds = bll.GetList("MemberID=" + id);
        //    Tools.DataTool.AddSNSTypeName(ds, "SNSName", "SNSTypeID");
        //    this.rptList.DataSource = ds.Tables[0].DefaultView;
        //    this.rptList.DataBind();
        //}
        //#endregion
    }
}
