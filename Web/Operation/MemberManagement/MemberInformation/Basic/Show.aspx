﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Basic.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                称呼：
            </td>
            <td width="75%">
                <asp:Label ID="MemberAppellation" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                英文名（姓）：
            </td>
            <td>
                <asp:Label ID="MemberEngFamilyName" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                英文名（名）：
            </td>
            <td>
                <asp:Label ID="MemberEngGivenName" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                中文名（姓）：
            </td>
            <td>
                <asp:Label ID="MemberChiFamilyName"  runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                中文名（名）：
            </td>
            <td>
                <asp:Label ID="MemberChiGivenName" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                性别 ：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberSex" runat="server" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Selected="True" Value="0">保密</asp:ListItem>
                    <asp:ListItem Value="1">男性</asp:ListItem>
                    <asp:ListItem Value="2">女性</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                生日 ：
            </td>
            <td>
                <asp:Label ID="MemberDateOfBirth" runat="server" class="Wdate"
                    onClick="WdatePicker()" CssClass="input required"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                手机号码：
            </td>
            <td>
                <asp:Label ID="MemberMobilePhone"  runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                婚姻情况：
            </td>
            <td width="75%">
                <asp:RadioButtonList ID="MemberMarital" runat="server" RepeatDirection="Horizontal"
                    Enabled="false">
                    <asp:ListItem Selected="True" Value="0">保密</asp:ListItem>
                    <asp:ListItem Value="1">未婚</asp:ListItem>
                    <asp:ListItem Value="2">已婚</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                证件类别：
            </td>
            <td>
                <asp:DropDownList ID="MemberIdentityType" runat="server" Enabled="false">
                    <asp:ListItem Value="0">----------</asp:ListItem>
                    <asp:ListItem Value="1">手机</asp:ListItem>
                    <asp:ListItem Value="2">邮箱</asp:ListItem>
                    <asp:ListItem Value="3">身份证</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                证件号码：
            </td>
            <td>
                <asp:Label ID="MemberIdentityRef" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                学历：
            </td>
            <td>
                <asp:DropDownList ID="EducationID" runat="server"  Enabled="false"
                    Height="21px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                专业：
            </td>
            <td>
                <asp:DropDownList ID="ProfessionID"  runat="server"  Enabled="false"
                    Height="21px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                职位：
            </td>
            <td>
                <asp:Label ID="MemberPosition"  runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                国籍：
            </td>
            <td>
                <asp:DropDownList ID="NationID"  runat="server"  Enabled="false"
                    Height="21px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                固定电话：
            </td>
            <td>
                <asp:Label ID="HomeTelNum"  runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮箱：
            </td>
            <td>
                <asp:Label ID="MemberEmail" runat="server" ></asp:Label>
            </td>
        </tr>
          <tr>
            <td align="right">
                其他联系方式：
            </td>
            <td>
              <asp:Label ID="OtherContact" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                兴趣爱好：
            </td>
            <td>
                <asp:Label ID="Hobbies"  runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="SpRemark"  runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                会员照片：
            </td>
            <td>
                <asp:Image ID="Image1" runat="server" Width="100px" />
                <%-- <asp:FileUpload ID="MemberPicture" runat="server" />
                <asp:Button ID="btnUpload" runat="server" CssClass="submit" OnClick="btnUpload_Click"
                    Text="上传" />--%>
            </td>
        </tr>
        <tr>
            <td align="right">创建时间：</td>
	        <td><asp:Label ID="CreatedOn" runat = "server"></asp:Label></td>
        </tr>
        <tr>
            <td align="right">创建人：</td>
	        <td><asp:Label ID="CreatedBy" runat = "server"></asp:Label></td>
        </tr>
         <tr>
            <td align="right">上次修改时间：</td>
	        <td><asp:Label ID="UpdatedOn" runat = "server"></asp:Label></td>
        </tr>
        <tr>
            <td align="right">上次修改人：</td>
	        <td><asp:Label ID="UpdatedBy" runat = "server"></asp:Label></td>
        </tr>
<%--        <tr>
            <th colspan="2" align="left">
                其他联系方式：
            </th>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                            <tr>
                                <th width="6%">
                                    编号
                                </th>
                                <th width="10%">
                                    联系方式类型
                                </th>
                                <th width="10%">
                                    联系方式
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lb_id" runat="server" Text='<%#Eval("SNSAccountID")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblSNSType" runat="server" Text='<%#Eval("SNSName")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="spClear">
                </div>
            </td>
        </tr>--%>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit"></div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
