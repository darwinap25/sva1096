﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.MemberManagement.MemberInformation.Basic.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="HEAD1" runat="server">
    <title>Add</title>
	<script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

	<script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

	<script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

	<script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

	<script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
	   

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#Form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });

        $(document).ready(function() {
            //显示与不显示新增联系方式框
            $("#btnAddContact").click(function() {
                $("#ContactDiv").toggle();
            });

        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                称呼：
            </td>
            <td width="75%">
                <asp:TextBox ID="MemberAppellation" TabIndex="1" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                英文名（姓）：
            </td>
            <td>
                <asp:TextBox ID="MemberEngFamilyName" TabIndex="2" runat="server" MaxLength="512"
                    CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                英文名（名）：
            </td>
            <td>
                <asp:TextBox ID="MemberEngGivenName" TabIndex="3" runat="server" MaxLength="512"
                    CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                中文名（姓）：
            </td>
            <td>
                <asp:TextBox ID="MemberChiFamilyName" TabIndex="4" runat="server" MaxLength="512"
                    CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                中文名（名）：
            </td>
            <td>
                <asp:TextBox ID="MemberChiGivenName" TabIndex="5" runat="server" MaxLength="512"
                    CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                性别 ：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberSex" runat="server" RepeatDirection="Horizontal" TabIndex="6">
                    <asp:ListItem Selected="True" Value="0">保密</asp:ListItem>
                    <asp:ListItem Value="1">男性</asp:ListItem>
                    <asp:ListItem Value="2">女性</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                生日 ：
            </td>
            <td>
                <asp:TextBox ID="MemberDateOfBirth" TabIndex="7" runat="server" MaxLength="512" class="Wdate"
                    onClick="WdatePicker()" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                国家码：
            </td>
            <td>
                <asp:DropDownList ID="NationID" TabIndex="8" runat="server" CssClass="dropdownlist required"
                    Height="21px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                手机号码：
            </td>
            <td>
                <asp:TextBox ID="MemberMobilePhone" TabIndex="9" runat="server" MaxLength="512" CssClass="input required digit"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                婚姻情况：
            </td>
            <td width="75%">
                <asp:RadioButtonList ID="MemberMarital" runat="server" RepeatDirection="Horizontal"
                    TabIndex="10">
                    <asp:ListItem Selected="True" Value="0">保密</asp:ListItem>
                    <asp:ListItem Value="1">未婚</asp:ListItem>
                    <asp:ListItem Value="2">已婚</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                证件类别：
            </td>
            <td>
                <asp:DropDownList ID="MemberIdentityType" runat="server" TabIndex="11" CssClass="dropdownlist">
                    <asp:ListItem Value="0">----------</asp:ListItem>
                    <asp:ListItem Value="1">手机</asp:ListItem>
                    <asp:ListItem Value="2">邮箱</asp:ListItem>
                    <asp:ListItem Value="3">身份证</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                证件号码：
            </td>
            <td>
                <asp:TextBox ID="MemberIdentityRef" TabIndex="12" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                学历：
            </td>
            <td>
                <asp:DropDownList ID="EducationID" TabIndex="13" runat="server" CssClass="dropdownlist required">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                专业：
            </td>
            <td>
                <asp:DropDownList ID="ProfessionID" TabIndex="14" runat="server" CssClass="dropdownlist required">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                职位：
            </td>
            <td>
                <asp:TextBox ID="MemberPosition" TabIndex="15" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                固定电话：
            </td>
            <td>
                <asp:TextBox ID="HomeTelNum" TabIndex="16" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮箱：
            </td>
            <td>
                <asp:TextBox ID="MemberEmail" TabIndex="17" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td align="right">
                其他联系方式：
            </td>
            <td>
              <asp:TextBox ID="OtherContact" TabIndex="18" runat="server"  MaxLength="512" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                兴趣爱好：
            </td>
            <td>
                <asp:TextBox ID="Hobbies" TabIndex="19" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:TextBox ID="SpRemark" TabIndex="20" runat="server" MaxLength="512"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                会员照片：
            </td>
            <td>
                <asp:Image ID="Image1" runat="server" Width="100px" />
                <asp:FileUpload ID="MemberPicture" runat="server" />
                <asp:Button ID="btnUpload" runat="server" CssClass="submit" OnClick="btnUpload_Click"
                    Text="上传" />
            </td>
        </tr>
<%--        <tr>
            <th colspan="2" align="left">
                其他联系方式：
            </th>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                            <tr>
                                <th width="6%">
                                    <input type="checkbox" onclick="checkAll(this);" />选择
                                </th>
                                <th width="6%">
                                    编号
                                </th>
                                <th width="10%">
                                    联系方式类型
                                </th>
                                <th width="10%">
                                    联系方式
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lb_id" runat="server" Text='<%#Eval("SNSAccountID")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblSNSType" runat="server" Text='<%#Eval("SNSName")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div id="ContactDiv" style="display: none;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                        <tr>
                            <td width="25%" align="right">
                                其他联系方式类别：
                            </td>
                            <td width="75%">
                                <asp:DropDownList ID="SNSTypeID" TabIndex="4" runat="server" Width="255px" Height="21px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                其他联系方式：
                            </td>
                            <td>
                                <asp:TextBox ID="ContactNumber" TabIndex="4" runat="server" Width="249px"></asp:TextBox>
                                <asp:Button ID="btnSaveContact" runat="server" CssClass="submit" Text="新增" OnClick="btnSaveContact_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="spClear">
                </div>
                <div style="line-height: 30px; height: 30px;">
                    <div class="left">
                        <asp:Button ID="btnDelContact" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                            Text="删除" OnClick="btnDelContact_Click" CssClass="submit" />
                        <input id="btnAddContact" type="button" value="添加" class="submit" />
                    </div>
                </div>
            </td>
        </tr>--%>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit"></div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
