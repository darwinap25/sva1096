﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;


namespace Edge.Web.Operation.MemberManagement.ImportBIFile
{
    public partial class Modify : Tools.BasePage<SVA.BLL.Ord_ImportCouponDispense_H, SVA.Model.Ord_ImportCouponDispense_H>
    {
        private const string fields = "CouponDispenseNumber,CampaignCode,CouponTypeCode,MemberRegisterMobile,ExportDatetime,CardNumber";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.rptPager.PageSize = webset.ContentPageNum;

                RptBind(string.Format("CouponDispenseNumber='{0}'", Request.Params["id"]), "CouponDispenseNumber", fields);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveByName.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());
                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(this.ApproveStatus.Text);

                if (Model.ApproveStatus == "A")
                {
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                }
                else
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;

                }
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            RptBind(string.Format("CouponDispenseNumber='{0}'", Request.Params["id"]), "CouponDispenseNumber", fields);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_ImportCouponDispense_H item = this.GetUpdateObject();

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_ImportCouponDispense_H>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby,string fields)
        {
            int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Ord_ImportCouponDispense_D bll = new Edge.SVA.BLL.Ord_ImportCouponDispense_D() 
            {
                StrWhere = strWhere,
                Order = orderby,
                Fields = fields,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
                this.RecordCount = count;

            }
            else
            {
                ds = bll.GetList(this.rptPager.PageSize, currentPage);
            }
           
            Tools.DataTool.AddCouponTypeNameByCode(ds, "CouponTypeIDName", "CouponTypeCode");
            Tools.DataTool.AddColumn(ds, "CreatedDate", Request.Params["CreatedOn"]);


            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }

        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.rptPager.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }
    }
}
