﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SOD.aspx.cs" Inherits="Edge.Web.Operation.AppOperation.SOD" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
     <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />

        <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
        
    </div>
    <div style="text-align:center;padding-top:40px;">
    当前工作日：<asp:Label ID="lbNowBusDate" runat="server" Text=""></asp:Label><br /><br />
       
        <asp:TextBox ID="tbEODTime" TabIndex="1" runat="server" MaxLength="6"
                    CssClass="input "  EnableTheming="false" Width="50px" onfocus="WdatePicker({dateFmt:'HHmmss'})"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;格式为：HHmmss
        <br />
        <br />
    <span class="btn_bg">    
        <asp:LinkButton ID="lbSetSODEODTime" runat="server" onclick="lbSetSODEODTime_Click" >设置SOD/EOD</asp:LinkButton>
        </span>
        <br />
        <br />    
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
