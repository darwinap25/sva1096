﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Security.Manager;

namespace Edge.Web.Operation.AppOperation
{
    public partial class SOD : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RefreshSODEOD();
            }
        }
        private void RefreshSODEOD()
        {
            DateTime busDate;
            if (DateTime.TryParse(DALTool.GetBusinessDate(), out busDate))
            {
                this.lbNowBusDate.Text = busDate.ToString("yyyy-MM-dd");
            }
        }

        protected void lbSetSODEODTime_Click(object sender, EventArgs e)
        {
            this.lbSetSODEODTime.Enabled = false;
            this.tbEODTime.ReadOnly = true;

            Logger.Instance.WriteOperationLog(this.PageName, " SetSODEOD " + DateTime.Today.ToString());

            this.CloseLoading();
            DateTime newBusDate;
            AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());
            Edge.Security.Manager.User currentUser = Session["UserInfo"] as Edge.Security.Manager.User;

            if (currentUser != null)
            {
                int rtn = DALTool.ExecSODEOD(currentUser.UserID, out newBusDate);
                if (rtn == 0)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Set SODEOD Success New BusDate " + newBusDate.ToString());
                    JscriptPrint("Set SOD/EOD Success", "EOD.aspx", Resources.MessageTips.SUCESS_TITLE);
                }
                else
                {
                    Logger.Instance.WriteErrorLog(this.PageName, "Set SODEOD Failed " + rtn.ToString());
                    JscriptPrint("Set SOD/EOD Failed ", "EOD.aspx", Resources.MessageTips.FAILED_TITLE);
                }
            }
            else
            {
                Logger.Instance.WriteErrorLog(this.PageName, " user is null !");
            }
            this.lbSetSODEODTime.Enabled = true;
            this.tbEODTime.ReadOnly = false;
        }
    }
}
