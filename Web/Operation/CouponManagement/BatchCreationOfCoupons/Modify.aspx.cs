﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponBatchCreate, Edge.SVA.Model.Ord_CouponBatchCreate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "IsImportCouponNumber = 0");
                //Edge.Web.Tools.ControlTool.BindCampaign(CampaignID);
                this.txtCouponStatus.Text = Tools.DALTool.GetCouponTypeStatusName((int)Controllers.CouponController.CouponStatus.Dormant);
            }
        }

        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(CouponTypeID.SelectedValue))
                {
                    int couponTypeID = int.Parse(CouponTypeID.SelectedValue);
                    Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

                    Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(model.CampaignID.GetValueOrDefault());
                    CampaignID.Text = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                   
                    GetCreatedInfo(couponTypeID);
                }
                
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                this.lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);

                Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(Model.CouponTypeID);
                if (couponType != null)
                {
                    bool isFixedAmount = couponType.CoupontypeFixedAmount.HasValue ? couponType.CoupontypeFixedAmount.Value == 0 : false;
                    this.InitAmount.Enabled = isFixedAmount;
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponBatchCreate item = this.GetUpdateObject();
            if (item != null)
            {
                string msg = "";
                long remainCoupons = 0;
                string lastCreatedCoupon = "";
                Logger.Instance.WriteOperationLog(this.PageName, "Coupon Batch Create :" + item.CouponCreateNumber);
                int checkResult = Controllers.CouponController.GetCreatedCouponInfo(item.CouponTypeID, item.CouponCount, ref remainCoupons, ref lastCreatedCoupon, ref msg);
                if (checkResult == -1)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.NumberDuplicate);
                    this.JscriptPrintAndFocus(Resources.MessageTips.NumberDuplicate + "  " + msg, "", Resources.MessageTips.WARNING_TITLE, this.CouponCount.ClientID);
                    return;
                }
                else if (checkResult == -2)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.ExceededNumberOfRange);
                    this.JscriptPrintAndFocus(Resources.MessageTips.ExceededNumberOfRange, "", Resources.MessageTips.WARNING_TITLE, this.CouponCount.ClientID);
                    return;
                }

                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponBatchCreate>(item))
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.UpdateSuccess);
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.UpdateFailed);
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CouponTypeID.SelectedValue))
                return;
            int couponTypeID = int.Parse(CouponTypeID.SelectedValue);
            Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

            InitAmount.Text = Edge.Web.Tools.ConvertTool.ToStringAmount(model.CouponTypeAmount);
            ExpiryDate.Text = Edge.Web.Tools.DALTool.GetCouponTypeExpiryDate(model);

            Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(ConvertTool.ToInt(CouponTypeID.SelectedValue));
            if (couponType != null)
            {
                bool isFixedAmount = couponType.CoupontypeFixedAmount.HasValue ? couponType.CoupontypeFixedAmount.Value == 0 : false;
                this.InitAmount.Enabled = isFixedAmount;
            }

            GetCreatedInfo(couponTypeID);
        }

        private void GetCreatedInfo(int couponTypeID)
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            this.CreatedCoupons.Text = bll.GetCount(string.Format("CouponTypeID = {0}", couponTypeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCoupons = 0;
            string lastCreatedCoupon = "";
            Controllers.CouponController.GetCreatedCouponInfo(couponTypeID, 0, ref remainCoupons, ref lastCreatedCoupon, ref msg);
            this.RemainCoupons.Text = remainCoupons.ToString("N00");
            this.LastCreatedCoupons.Text = lastCreatedCoupon;
        }
    }
}
