﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons
{
    public partial class Void : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ";");

                Logger.Instance.WriteOperationLog(this.PageName, "Batch Void Coupon:" + ids);
                string resultMsg = CouponController.BatchVoidCoupon(idList, CouponController.ApproveType.BatchCreate);
                Logger.Instance.WriteOperationLog(this.PageName, "Batch Void Coupon:" + resultMsg);
                JscriptPrint(resultMsg, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
        }
    }
}
