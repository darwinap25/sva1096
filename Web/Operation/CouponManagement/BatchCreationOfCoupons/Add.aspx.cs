﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.BatchCreationOfCoupons
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponBatchCreate, Edge.SVA.Model.Ord_CouponBatchCreate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "IsImportCouponNumber = 0");

                InitData();

                this.InitAmount.Enabled = false;
                this.txtCouponStatus.Text = Tools.DALTool.GetCouponTypeStatusName((int)Controllers.CouponController.CouponStatus.Dormant);
            }
        }


        private void InitData()
        {
            CouponCreateNumber.Text = DALTool.GetREFNOCode(Edge.Web.Controllers.CouponController.CouponRefnoCode.OrderBatchCreationOfCoupons);
            CreatedOn.Text = Edge.Web.Tools.DALTool.GetSystemDateTime();
            lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Edge.Web.Tools.DALTool.GetCurrentUser().UserID);
            CreatedBusDate.Text = Edge.Web.Tools.DALTool.GetBusinessDate();

            this.lblApproveStatus.Text = DALTool.GetApproveStatusString(ApproveStatus.Value);

            IssuedDate.Text = Edge.Web.Tools.DALTool.GetSystemDate();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponBatchCreate item = this.GetAddObject();

            if (item != null)
            {
                string msg = "";
                long remainCoupons = 0;
                string lastCreatedCoupon = "";
                Logger.Instance.WriteOperationLog(this.PageName, "Batch Create Coupon:" + item.CouponCreateNumber);
                int checkResult = Controllers.CouponController.GetCreatedCouponInfo(item.CouponTypeID, item.CouponCount,ref remainCoupons,ref lastCreatedCoupon, ref msg);
                if (checkResult == -1)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.NumberDuplicate);
                    this.JscriptPrintAndFocus(Resources.MessageTips.NumberDuplicate + "  " + msg, "", Resources.MessageTips.WARNING_TITLE, this.CouponCount.ClientID);
                    return;
                }
                else if (checkResult == -2)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.ExceededNumberOfRange);
                    this.JscriptPrintAndFocus(Resources.MessageTips.ExceededNumberOfRange, "", Resources.MessageTips.WARNING_TITLE, this.CouponCount.ClientID);
                    return;
                }

                if (item.CouponCreateNumber.Equals(string.Empty))
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.ExceededNumberOfRange);
                    JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
                    return;
                }
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedOn = null;
                item.UpdatedBy = null;
                item.ApproveOn = null;
                item.ApproveBusDate = null;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Ord_CouponBatchCreate>(item) > 0)
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.AddSuccess);
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.AddFailed);
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void CouponTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CouponTypeID.SelectedValue))
                return;
            int couponTypeID = int.Parse(CouponTypeID.SelectedValue);
            Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

            InitAmount.Text = Edge.Web.Tools.ConvertTool.ToStringAmount(model.CouponTypeAmount);
            ExpiryDate.Text = Edge.Web.Tools.DALTool.GetCouponTypeExpiryDate(model);
           
            Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(model.CampaignID.GetValueOrDefault());
            CampaignID.Text = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);

            GetCreatedInfo(couponTypeID);
        }

        private void GetCreatedInfo(int couponTypeID)
        {
            Edge.SVA.BLL.Coupon bll = new Edge.SVA.BLL.Coupon();
            this.CreatedCoupons.Text = bll.GetCount(string.Format("CouponTypeID = {0}", couponTypeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCoupons = 0;
            string lastCreatedCoupon = "";
            Controllers.CouponController.GetCreatedCouponInfo(couponTypeID, 0, ref remainCoupons, ref lastCreatedCoupon, ref msg);
            this.RemainCoupons.Text = remainCoupons.ToString("N00");
            this.LastCreatedCoupons.Text = lastCreatedCoupon;
        }
    }
}
