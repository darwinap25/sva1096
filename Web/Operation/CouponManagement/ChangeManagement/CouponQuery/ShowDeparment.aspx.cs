﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.CouponQuery
{
    public partial class ShowDeparment : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                int couponTypeID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(Request.Params["CouponTypeID"]);

                Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                DataSet ds = bll.GetList(" CouponTypeID=" + couponTypeID + " and BindingType=2 and ProdCode is null ");

                Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");

                this.rptList.DataSource = ds.Tables[0].DefaultView;
                this.rptList.DataBind();
            }
        }
    }
}
