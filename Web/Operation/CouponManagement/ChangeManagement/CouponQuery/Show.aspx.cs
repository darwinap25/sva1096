﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.ChangeManagement.CouponQuery
{
    public partial class Show : Edge.Web.UI.ManagePage
    {

        private const string fields = "ServerCode,StoreID,RegisterCode,OprID,RefTxnNo,BusDate,Txndate,ApprovalCode,CouponTypeID,OpenBal,Amount,CloseBal";
      
        public int couponTypetID;

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!this.IsPostBack)
            {
                this.TXNListPager.PageSize = webset.ContentPageNum;

                SetObject();

                string strWhere = string.Format("CouponNumber = '{0}'",Request.Params["id"]);
                RptBind(strWhere, "KeyID", fields);
            }
        }

        protected void TXNListPager_PageChanged(object sender, EventArgs e)
        {
            string strWhere = string.Format("CouponNumber = '{0}'", Request.Params["id"]);
            RptBind(strWhere, "KeyID", fields);
            this.AnimateRoll("msgtablelist");
            this.PageCount++;

        }

        private void SetObject()
        {
            this.couponTypetID = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<int>(Request.Params["couponTypeID"]);

            this.CouponNumber.Text = Request.Params["id"];
            this.CouponUID.Text = Request.Params["couponUID"];
            this.BatchCouponID.Text = Request.Params["batchCode"];
            this.CouponTypeID.Text = ControlTool.GetDropdownListText(Request.Params["CouponTypeName"], Request.Params["CouponTypeCode"]);
            this.CouponTypeAmount.Text = Request.Params["amount"];
            this.Status.Text = Request.Params["status"];
            this.CouponIssueDate.Text = Request.Params["CouponIssueDate"];
            this.CouponExpiryDate.Text = Request.Params["CouponExpiryDate"];
           
        }

        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby, string fields)
        {
            int currentPage = this.TXNListPager.CurrentPageIndex - 1 < 0 ? 0 : this.TXNListPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Coupon_Movement bll = new Edge.SVA.BLL.Coupon_Movement();

            //获得总条数
            this.TXNListPager.RecordCount = bll.GetCount(strWhere);

            DataSet ds = bll.GetList(this.TXNListPager.PageSize, currentPage, strWhere, orderby, fields);

            Tools.DataTool.AddID(ds, "IDNumber", this.TXNListPager.PageSize, currentPage);
            Tools.DataTool.AddTxnTypeName(ds, "OprIDName", "OprID");
            DataTool.AddBrandCodeByCouponTypeID(ds, "BrandCode", "CouponTypeID");
            Tools.DataTool.AddStoreCode(ds, "StoreCode", "StoreID");

            this.TXNList.DataSource = ds.Tables[0].DefaultView;
            this.TXNList.DataBind();
        }
        #endregion

        public int PageCount
        {
            get
            {
                if (ViewState["PageCount"] is int) return (int)ViewState["PageCount"];

                return 1;
            }
            set
            {
                ViewState["PageCount"] = value;
            }
        }


    }
}
