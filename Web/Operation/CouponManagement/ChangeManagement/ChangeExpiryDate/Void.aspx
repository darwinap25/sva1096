﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Void.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.ChangeManagement.ChangeExpiryDate.Void" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <th colspan="2" align="left">
                    <%=this.PageName %>
                </th>
            </tr>
            <tr>
                <td align="right">
                    交易编号:
                </td>
                <td align="left">
                    <asp:Label ID="TxnNo" runat="server" Text='<%#Eval("TxnNo") %>'></asp:Label>
                </td>
            </tr>
            <tr style="color: Red; font-size: large;">
                <td align="right">
                    授权号:
                </td>
                <td align="left">
                    <asp:Label ID="errorMsg" runat="server" Text='<%#Eval("ApproveCode") %>'></asp:Label>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr>
                <td colspan="2" align="center">
                    <div style="text-align: center;">
                        <input type="button" value="关 闭" class="submit" onclick="javascript:window.top.tb_remove();parent.frames['sysMain'].location.href= 'List.aspx' " />
                    </div>
                </td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
