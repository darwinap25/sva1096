﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.ChangeExpiryDate.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：修改有效期</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                修改优惠劵
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                优惠劵号码：
            </td>
            <td width="75%">
                <asp:TextBox ID="CouponNumber" TabIndex="1" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵类型：
            </td>
            <td>
                <asp:DropDownList ID="CouponTypeID" TabIndex="2" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠券发行日期：
            </td>
            <td>
                <asp:TextBox ID="CouponIssueDate" TabIndex="4" runat="server" onClick="WdatePicker()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠券过期日期：
            </td>
            <td>
                <asp:TextBox ID="CouponExpiryDate" TabIndex="4" runat="server" onClick="WdatePicker()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵激活日期：
            </td>
            <td>
                <asp:TextBox ID="CouponActiveDate" TabIndex="4" runat="server" onClick="WdatePicker()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                领取优惠劵的店铺代码：
            </td>
            <td>
                <asp:TextBox ID="StoreID" TabIndex="4" runat="server" Width="249px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时的批次号：
            </td>
            <td>
                <asp:TextBox ID="BatchCouponID" TabIndex="4" runat="server" Width="249px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵状态：
            </td>
            <td>
                <asp:DropDownList ID="Status" TabIndex="4" runat="server"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                主卡卡号：
            </td>
            <td>
                <asp:TextBox ID="CardNumber" TabIndex="4" runat="server" Width="249px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" /></div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
