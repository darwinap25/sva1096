﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;
using Edge.Messages.Manager;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class Approve : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("TxnNo", typeof(string));
                dt.Columns.Add("ApproveCode", typeof(string));
                dt.Columns.Add("ApprovalMsg",typeof(string));

                
                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids,";");

                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_ImportCouponUID_H mode = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(id);

                    DataRow dr = dt.NewRow();
                    dr["TxnNo"] = id;
                    if (CouponController.CanApprove(mode))
                    {
                        dr["ApproveCode"] = CouponController.ApproveCouponForApproveCode(mode);
                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveCode;
                    }
                    else
                    {
                        dr["ApproveCode"] = Edge.Messages.Manager.MessagesTool.instance.GetMessage("90190");
                        dr["ApprovalMsg"] = Resources.MessageTips.ApproveError;
                    }
                   
                   
                    dt.Rows.Add(dr);
                }
                this.rptList.DataSource = dt;
                this.rptList.DataBind();

            }
        }
    }
}
