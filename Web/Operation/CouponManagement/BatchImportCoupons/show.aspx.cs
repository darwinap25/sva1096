﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class show : Tools.BasePage<Edge.SVA.BLL.Ord_ImportCouponUID_H, Edge.SVA.Model.Ord_ImportCouponUID_H>
    {
        private const string fields = "KeyID,ImportCouponNumber,CouponTypeID,CouponUID,ExpiryDate,BatchID,Denomination";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.rptPager.PageSize = webset.ContentPageNum;
             
                this.RecordCount = -1;
                RptBind(string.Format("ImportCouponNumber = '{0}'", Request.Params["id"]), "KeyID");

                //this.dtTotal.Visible = true;
                //this.lblTotalDenomination.Text = new Edge.SVA.BLL.Ord_ImportCouponUID_D().GetAllDenomination(Request.Params["id"]).ToString("N02");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveBy.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());

                this.CreatedOn.Text = Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
       
                this.ApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(this.ApproveStatus.Text);

                if (Model.ApproveStatus == "P")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                    this.btnExport.Visible = false;
                }
                else if (Model.ApproveStatus == "A")
                {
                    this.btnExport.Visible = true;
                    this.ApproveOn.Text = ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault()); 
                }
                else
                {
                    this.btnExport.Visible = false;
                }


                //if (this.RecordCount > 0)
                //{
                //    Edge.SVA.BLL.Ord_ImportCouponUID_D bll = new SVA.BLL.Ord_ImportCouponUID_D();
                //    decimal totalDenomination = bll.GetAllDenomination(Request.Params["id"].ToString());
                //    this.lblTotalDenomination.Text = totalDenomination.ToString("N2");
                //    this.dtTotal.Visible = true;
                //}
                //else
                //{
                //    this.dtTotal.Visible = false;
                //}
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            RptBind(string.Format("ImportCouponNumber = '{0}'", Request.Params["id"]), "KeyID");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            string fileName = "";
            int records = 0;

            Edge.SVA.BLL.Ord_ImportCouponUID_H bll = new SVA.BLL.Ord_ImportCouponUID_H();
           
            try
            {
                records = new SVA.BLL.Ord_ImportCouponUID_D().GetCount(this.ImportCouponNumber.Text.Trim(),120);
                fileName = bll.ExportCSV(this.ImportCouponNumber.Text, records);
                if (string.IsNullOrEmpty(fileName))
                {
                    JscriptPrint(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }

                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);

                Tools.ExportTool.ExportFile(fileName);
                Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Import", fn, start, records, null);

            }
            catch (Exception ex)
            {

                string fn = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                Tools.Logger.Instance.WriteExportLog("Batch Creation of Coupons - Import", fn, start, records, ex.Message);
                JscriptPrint(ex.Message, "", Resources.MessageTips.FAILED_TITLE);
            }

           
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Ord_ImportCouponUID_D bll = new Edge.SVA.BLL.Ord_ImportCouponUID_D();

            DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetListForTotal(this.rptPager.PageSize, currentPage, strWhere, orderby, fields, 120);
                this.RecordCount = ds != null && ds.Tables.Count > 1 ? int.TryParse(ds.Tables[1].Rows[0]["Total"].ToString(), out count) ? count : 0 : 0;
            }
            else
            {
                ds = bll.GetList(this.rptPager.PageSize, currentPage, strWhere, orderby, fields, 120);
            }

            this.rptPager.RecordCount = this.RecordCount < 0 ? 0 : this.RecordCount;

            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeIDName", "CouponTypeID");
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddColumn(ds, "CreatedDate", Request.Params["CreatedOn"]);
            Tools.DataTool.AddColumn(ds, "CouponNumber", "");

            if (Request.Params["Status"] == "A")
            {
                Tools.DataTool.AddColumn(ds, "Status", "");
                Dictionary<string, Edge.SVA.Model.Coupon> cache = new Dictionary<string, SVA.Model.Coupon>();

                Tools.DataTool.UpdateCouponStatus(ds, "Status", "CouponUID", cache);
                Tools.DataTool.UpdateCouponBatch(ds, "BatchID", "CouponUID",cache);
                Tools.DataTool.UpdateCouponExpiryDate(ds, "ExpiryDate", "CouponUID",cache);
                Tools.DataTool.UpdateCouponDenomination(ds, "Denomination", "CouponUID",cache);
                Tools.DataTool.UpdateCouponNumber(ds, "CouponNumber", "CouponUID",cache);

            }
            else
            {
                Tools.DataTool.AddColumn(ds, "Status", CouponController.CouponStatus.Dormant.ToString());
                Tools.DataTool.UpdateCouponAmout(ds, "Denomination", "CouponTypeID");
            }

            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                ViewState["RecordCount"] = value;
            }
        }
    }
}
