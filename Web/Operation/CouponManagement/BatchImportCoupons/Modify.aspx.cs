﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Controllers;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.Ord_ImportCouponUID_H, Edge.SVA.Model.Ord_ImportCouponUID_H>
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小
        public string id;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;
            this.id = Request.Params["id"];
            if (!this.IsPostBack)
            {
                RptBind(string.Format("ImportCouponNumber = '{0}'", id), "KeyID");
            }
        }
        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.Ord_ImportCouponUID_D bll = new Edge.SVA.BLL.Ord_ImportCouponUID_D();
            this.pcount = bll.GetCount(strWhere);

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);

            Edge.SVA.Model.Ord_ImportCouponUID_H item = new Edge.SVA.BLL.Ord_ImportCouponUID_H().GetModel(id);
            string createDate = item.CreatedOn.HasValue ? item.CreatedOn.Value.ToString("yyyy-MM-dd") : null;

            Tools.DataTool.AddColumn(ds, "Status", CouponController.CouponStatus.Dormant.ToString());          
            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeIDName", "CouponTypeID");         
            Tools.DataTool.AddColumn(ds, "CreatedDate", createDate);
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.UpdateCouponAmout(ds, "Denomination", "CouponTypeID");
            Tools.DataTool.AddColumn(ds, "CouponNumber", "");
          
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.ApproveByName.Text = Tools.DALTool.GetUserName(Model.ApproveBy.GetValueOrDefault());

                this.lblApproveStatus.Text = Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                if (Model.ApproveStatus != "A")
                {
                    this.ApproveOn.Text = null;
                    this.ApprovalCode.Text = null;
                }
                else 
                {
                    this.ApproveOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.ApproveOn.GetValueOrDefault());
                }

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_ImportCouponUID_H item = this.GetUpdateObject();

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Ord_ImportCouponUID_H>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

    
    }
}
