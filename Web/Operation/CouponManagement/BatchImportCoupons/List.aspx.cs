﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Messages.Manager;

namespace Edge.Web.Operation.CouponManagement.BatchImportCoupons
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;
           
            if (!Page.IsPostBack)
            {
                RptBind("", "ImportCouponNumber");
            }
            string url = this.Request.Url.AbsolutePath.Substring(0, this.Request.Url.AbsolutePath.LastIndexOf("/") + 1);

            this.lbtnApprove.Attributes["onclick"] = string.Format("return checkSelect( '{0}','{1}');", MessagesTool.instance.GetMessage("10017"), url + "Approve.aspx");
            this.lbtnVoid.OnClientClick = "return hasSelect( '" + Resources.MessageTips.ConfirmVoid + " ');";
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.Ord_ImportCouponUID_H bll = new Edge.SVA.BLL.Ord_ImportCouponUID_H();

            //获得总条数
            this.pcount = bll.GetCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnApprove.Attributes.Remove("disabled");
                this.lbtnVoid.Attributes.Remove("disabled");

            }
            else
            {
                this.lbtnApprove.Attributes["disabled"] = "disabled";
                this.lbtnVoid.Attributes["disabled"] = "disabled";
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);

            Tools.DataTool.AddUserName(ds, "CreatedByName", "CreatedBy");
            Tools.DataTool.AddUserName(ds, "ApproveByName", "ApproveBy");

            Tools.DataTool.AddCouponApproveStatusName(ds, "ApproveStatusName", "ApproveStatus");

            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }

        protected void lbtnApprove_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < this.rptList.Items.Count; i++)
            {
                System.Web.UI.Control item = rptList.Items[i];
                CheckBox cb = item.FindControl("cb_id") as CheckBox;

                if (cb != null && cb.Checked == true)
                {
                    string couponNumber = (item.FindControl("lb_id") as Label).Text;
                    ids += string.Format("{0};", couponNumber);
                }
            }
            Response.Redirect("Approve.aspx?ids=" + ids);
        }

        protected void lbtnVoid_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < this.rptList.Items.Count; i++)
            {
                System.Web.UI.Control item = rptList.Items[i];
                CheckBox cb = item.FindControl("cb_id") as CheckBox;

                if (cb != null && cb.Checked == true)
                {
                    string couponNumber = (item.FindControl("lb_id") as Label).Text;
                    ids += string.Format("{0};", couponNumber);
                }
            }
            Response.Redirect("Void.aspx?ids=" + ids);
        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string couponNumber = (e.Item.FindControl("lb_id") as Label).Text;
                string approveStatus = ((Label)e.Item.FindControl("lblApproveStatus")).Text;
                DateTime? createTime = Edge.Utils.Tools.ConvertTool.GetInstance().ConverToType<DateTime>(((Label)e.Item.FindControl("lblCreatedOn")).Text);
                string strCreateDate =    Edge.Utils.Tools.StringHelper.GetDateString(createTime);
             

                switch (approveStatus.Substring(0,1).ToUpper().Trim())
                {
                    case "A":
                        (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
                        (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
                        (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=A", couponNumber, strCreateDate);
                        break;
                    case "P":
                        (e.Item.FindControl("lbkEdit") as LinkButton).Attributes["href"] = string.Format("Modify.aspx?id={0}&CreatedOn={1}&Status=P", couponNumber, strCreateDate);
                        (e.Item.FindControl("lblApproveCode") as Label).Text = "";
                        (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=P", couponNumber, strCreateDate);
                        break;
                    case "V":
                        (e.Item.FindControl("lbkEdit") as LinkButton).Enabled = false;
                        (e.Item.FindControl("cb_id") as CheckBox).Enabled = false;
                        (e.Item.FindControl("lblApproveCode") as Label).Text = "";
                        (e.Item.FindControl("lkbView") as LinkButton).Attributes["href"] = string.Format("Show.aspx?id={0}&CreatedOn={1}&Status=V", couponNumber, strCreateDate);
                        break;

                }
            }
        }

    }
}
