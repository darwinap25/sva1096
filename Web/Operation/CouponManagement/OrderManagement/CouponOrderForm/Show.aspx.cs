﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm
{
    public partial class Show : Tools.BasePage<SVA.BLL.Ord_CouponOrderForm_H, SVA.Model.Ord_CouponOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            
            if (!this.IsPostBack)
            {
                this.rptPager.PageSize = webset.ContentPageNum;

                this.CouponOrderFormNumber.Text = Model.CouponOrderFormNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

                this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                this.CustomerType.Visible = false;

                this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                this.SendMethod.Visible = false;

                Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID.GetValueOrDefault());
                this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);
                DataBind(string.Format("CouponOrderFormNumber = '{0}'", Request.Params["id"]), "KeyID");
            }
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
             DataBind(string.Format("CouponOrderFormNumber = '{0}'",Request.Params["id"]), "KeyID");
             this.AnimateRoll("tranctionDetial");
        }

        protected void DataBind(string strWhere, string orderby)
        {
            int currentPage = this.rptPager.CurrentPageIndex < 1 ? 0 : this.rptPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Ord_CouponOrderForm_D bll = new Edge.SVA.BLL.Ord_CouponOrderForm_D()
            {
                StrWhere = strWhere,
                Order = orderby,
                Timeout = 60
            };

            System.Data.DataSet ds = null;
            if (this.RecordCount < 0)
            {
                int count = 0;
                ds = bll.GetList(this.rptPager.PageSize, currentPage, out count);
                this.RecordCount = count;
            }
            else
            {
                ds = bll.GetList(this.rptPager.PageSize, currentPage);
            }
            Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
            Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
            Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
          
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }

        private int RecordCount
        {
            get
            {
                if (ViewState["RecordCount"] == null || string.IsNullOrEmpty(ViewState["RecordCount"].ToString())) return -1;
                int count = 0;
                return int.TryParse(ViewState["RecordCount"].ToString(), out count) ? count : -1;
            }
            set
            {
                if (value < 0) return;
                this.rptPager.RecordCount = value;
                ViewState["RecordCount"] = value;
            }
        }
    }
}