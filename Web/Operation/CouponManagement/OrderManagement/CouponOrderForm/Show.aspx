﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="4" align="left">
                订单信息
            </th>
        </tr>
        <tr>
            <td width="15%" align="right">
                交易编号：
            </td>
            <td width="35%">
                <asp:Label ID="CouponOrderFormNumber" runat="server" MaxLength="512"></asp:Label>
            </td>
            <td width="15%" align="right">
                交易状态：
            </td>
            <td width="35%">
                <asp:Label ID="lblApproveStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="ApproveStatus" runat="server" Value="P" />
            </td>
        </tr>
        <tr>
            <td align="right">
                交易创建工作日期：
            </td>
            <td>
                <asp:Label ID="CreatedBusDate" runat="server"></asp:Label>
            </td>
            <td align="right">
                交易创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td colspan="3">
                <asp:Label ID="CreatedByName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="4" align="left">
                订单内容
            </th>
        </tr>
        <tr>
            <td align="right" colspan="1">
                订单类型：
            </td>
            <td colspan="3">
                <asp:Label ID="CustomerTypeView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="CustomerType" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Text="客户订货" Value="1"></asp:ListItem>
                    <asp:ListItem Text="店铺订货" Value="2" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货品牌：
            </td>
            <td>
                <asp:Label ID="ddlBrand" runat="server"></asp:Label>
            </td>
            <td align="right">
                店铺：
            </td>
            <td>
                <asp:Label ID="StoreID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                订货客户：
            </td>
            <td>
                <asp:Label ID="CustomerID" runat="server"></asp:Label>
            </td>
            <td align="right">
                送货单发送方式：
            </td>
            <td>
                <asp:Label ID="SendMethodView" runat="server"></asp:Label>
                <asp:DropDownList ID="SendMethod" runat="server" CssClass="dropdownlist">
                    <asp:ListItem Text="直接交付（打印）" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="SMS" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Email" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Social Network" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                送货地址：
            </td>
            <td>
                <asp:Label ID="SendAddress" runat="server"></asp:Label>
            </td>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:Label ID="ContactName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                邮件发送：
            </td>
            <td>
                <asp:Label ID="Email" runat="server"></asp:Label>
            </td>
            <td align="right">
                SMS/MMS发送：
            </td>
            <td>
                <asp:Label ID="SMSMMS" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:Label ID="ContactNumber" runat="server"></asp:Label>
            </td>
            <td align="right">
                备注：
            </td>
            <td>
                <asp:Label ID="Remark" runat="server"></asp:Label>
            </td>
        </tr>
        <tr id="tranctionDetial">
            <td colspan="4">
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist" id="msgtablelist">
                            <tr>
                                <th width="10%">
                                    品牌编号
                                </th>
                                <th width="35%">
                                    品牌
                                </th>
                                <th width="10%">
                                    优惠劵类型编号
                                </th>
                                <th width="35%">
                                    优惠劵类型
                                </th>
                                <th width="10%">
                                    订货数量
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#Eval("BrandCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("BrandName")%>
                            </td>
                            <td align="center">
                                <%#Eval("CouponTypeCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("CouponTypeName")%>
                            </td>
                            <td align="center">
                                <%#Eval("CouponQty","{0:N00}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="clear" />
                <div class="right">
                    <webdiyer:AspNetPager ID="rptPager" runat="server" CustomInfoTextAlign="Left" FirstPageText="First"
                        HorizontalAlign="Right" InvalidPageIndexErrorMessage="Page index is not a valid value."
                        LastPageText="Last" NextPageText="Next" PageIndexBoxType="TextBox" PageIndexOutOfRangeErrorMessage="Page index out of range!"
                        PrevPageText="Prev" ShowPageIndexBox="Always" SubmitButtonText="Go" SubmitButtonClass="pagerSubmit"
                        TextBeforePageIndexBox="" OnPageChanged="rptListPager_PageChanged" CssClass="asppager"
                        CurrentPageButtonClass="cpb" CustomInfoClass="asppagercustom" CustomInfoHTML="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        CustomInfoSectionWidth="20%" ShowCustomInfoSection="Left" AlwaysShow="False"
                        LayoutType="Table">
                    </webdiyer:AspNetPager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx'"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
