﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using Edge.Web.Controllers;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponOrderForm
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CouponOrderForm_H, Edge.SVA.Model.Ord_CouponOrderForm_H>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.rptPager.PageSize = webset.ContentPageNum;

                ControlTool.BindBrand(ddlBrand);
                ControlTool.BindBrand(brandDetail);
                ControlTool.BindCustomer(CustomerID);
                ControlTool.BindCouponType(ddlCouponType, -1);

                ControlTool.AddTitle(ddlBrand);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                this.CouponOrderFormNumber.Text = Model.CouponOrderFormNumber;
                this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                this.CreatedByName.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

                Edge.SVA.Model.Store store = new SVA.BLL.Store().GetModel(Model.StoreID.GetValueOrDefault());
                int brandID = store == null ? -1 : store.BrandID.GetValueOrDefault();
                this.ddlBrand.SelectedValue = brandID < 0 ? "" : brandID.ToString();

                ControlTool.BindStore(StoreID, brandID);

                string storeID = Model.StoreID.GetValueOrDefault().ToString();
                this.StoreID.SelectedValue = this.StoreID.Items.FindByValue(storeID) == null ? "" : storeID;

                this.BindDetail();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CouponOrderForm_H item = this.GetUpdateObject();

            if (item == null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} No Data", item.CouponOrderFormNumber));
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
                return;
            }

            item.UpdatedBy = DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;

            if (Tools.DALTool.Update<Edge.SVA.BLL.Ord_CouponOrderForm_H>(item))
            {
                Edge.SVA.BLL.Ord_CouponOrderForm_D bll = new SVA.BLL.Ord_CouponOrderForm_D();
                bll.DeleteByOrder(this.CouponOrderFormNumber.Text.Trim());

                try
                {
                    DatabaseUtil.Factory.SetConnecctionString(DBUtility.PubConstant.ConnectionString);
                    DatabaseUtil.Interface.IDatabase database = DatabaseUtil.Factory.CreateIDatabase();
                    database.SetExecuteTimeout(6000);
                    System.Data.DataTable sourceTable = database.GetTableSchema("Ord_CouponOrderForm_D");
                    DatabaseUtil.Interface.IExecStatus es = null;
                    foreach (System.Data.DataRow detail in this.Detail.Rows)
                    {
                        System.Data.DataRow row = sourceTable.NewRow();
                        row["CouponOrderFormNumber"] = item.CouponOrderFormNumber;
                        row["BrandID"] = detail["BrandID"];
                        row["CouponTypeID"] = detail["CouponTypeID"];
                        row["CouponQty"] = detail["CouponQty"];
                        sourceTable.Rows.Add(row);
                    }
                    es = database.InsertBigData(sourceTable, "Ord_CouponOrderForm_D");
                    if (es.Success)
                    {
                        sourceTable.Rows.Clear();
                    }
                    else
                    {
                        throw es.Ex;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteErrorLog(this.PageName, string.Format("Coupon Order Form  {0} Add Success But Detail Failed", item.CouponOrderFormNumber), ex);
                    JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
                    return;
                }

                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Update Success", item.CouponOrderFormNumber));
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, string.Format("Coupon Order Form  {0} Update Failed", item.CouponOrderFormNumber));
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            this.AddDetail();

            this.AnimateRoll("tranctionDetial");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            if (btn == null) return;

            int couponID = -1;
            couponID = int.TryParse(btn.Attributes["couponid"], out couponID) ? couponID : -1;
            this.DeleteDetail(couponID);

            this.AnimateRoll("tranctionDetial");
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            this.BindDetail();
            this.AnimateRoll("tranctionDetial");
        }

        protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.ddlBrand.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindStore(StoreID, brandID);

            ControlTool.AddTitle(ddlBrand);
            ControlTool.AddTitle(StoreID);
        }

        protected void brandDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            int brandID = 0;
            brandID = int.TryParse(this.brandDetail.SelectedValue, out brandID) ? brandID : 0;
            ControlTool.BindCouponType(ddlCouponType, string.Format("BrandID = {0}", brandID));

            ControlTool.AddTitle(ddlCouponType);
            ControlTool.AddTitle(brandDetail);
        }

        private System.Data.DataTable Detail
        {
            get
            {
                if (ViewState["DetailResult"] == null)
                {
                    System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CouponOrderForm_D().GetList(string.Format("CouponOrderFormNumber = '{0}'",Request.Params["id"]));
                    if (ds == null || ds.Tables.Count <= 0) return null;

                    Tools.DataTool.AddID(ds, "ID", 0, 0);
                    Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");
                    Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                    Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                    Tools.DataTool.AddCouponTypeName(ds, "CouponTypeName", "CouponTypeID");
                    Tools.DataTool.AddColumn(ds, "CouponGradeID", "");
                    Tools.DataTool.AddColumn(ds, "CouponGradeCode", "");
                    Tools.DataTool.AddColumn(ds, "CouponGradeName", "");
                  
                    ViewState["DetailResult"] = ds.Tables[0];
                }
                return ViewState["DetailResult"] as System.Data.DataTable;
            }
        }

        private void AddDetail()
        {
            if (string.IsNullOrEmpty(this.brandDetail.SelectedItem.Value)) throw new Exception("Brand Can't Empty");
            if (string.IsNullOrEmpty(this.ddlCouponType.SelectedItem.Value)) throw new Exception("Coupon Type Can't Empty");
            if (string.IsNullOrEmpty(this.txtOrderCount.Text)) throw new Exception("Coupon Qty Can't Empty");

            foreach (System.Data.DataRow detail in this.Detail.Rows)
            {
                if (detail["CouponTypeID"].ToString().Equals(this.ddlCouponType.SelectedValue))
                {
                    JscriptPrint(Resources.MessageTips.ExistCouponTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
            }

            System.Data.DataRow row = this.Detail.NewRow();
            row["ID"] = this.DetailIndex;
            row["BrandID"] = int.Parse(this.brandDetail.SelectedItem.Value);
            row["BrandCode"] = this.brandDetail.SelectedItem.Text.Substring(0, this.brandDetail.SelectedItem.Text.IndexOf("-")).Trim();
            row["BrandName"] = this.brandDetail.SelectedItem.Text.Substring(this.brandDetail.SelectedItem.Text.IndexOf("-") + 1).Trim();
            row["CouponTypeID"] = int.Parse(this.ddlCouponType.SelectedItem.Value);
            row["CouponTypeCode"] = this.ddlCouponType.SelectedItem.Text.Substring(0, this.ddlCouponType.SelectedItem.Text.IndexOf("-"));
            row["CouponTypeName"] = this.ddlCouponType.SelectedItem.Text.Substring(this.ddlCouponType.SelectedItem.Text.IndexOf("-") + 1);
            row["CouponQty"] = int.Parse(this.txtOrderCount.Text.Replace(",", "").Trim());
            this.Detail.Rows.Add(row);

            this.BindDetail();
        }

        private void DeleteDetail(int couponID)
        {
            foreach (System.Data.DataRow row in this.Detail.Rows)
            {
                if (row["ID"].ToString().Equals(couponID.ToString()))
                {
                    row.Delete();
                    break;
                }
            }
            this.Detail.AcceptChanges();
            this.BindDetail();
        }

        private void BindDetail()
        {
            this.rptPager.RecordCount = this.Detail.Rows.Count;

            int index = this.rptPager.CurrentPageIndex > 0 ? this.rptPager.CurrentPageIndex - 1 : 0;

            this.rptList.DataSource = DataTool.GetPaggingTable(index, this.rptPager.PageSize, this.Detail);
            this.rptList.DataBind();

        }

        private int DetailIndex
        {
            get
            {
                if (ViewState["DetailIndex"] == null)
                {
                    ViewState["DetailIndex"] = 0;
                }
                ViewState["DetailIndex"] = (int)ViewState["DetailIndex"] + 1;
                return (int)ViewState["DetailIndex"];
            }
        }

        protected void StoreID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(Tools.ConvertTool.ConverType<int>(StoreID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.StoreAddressDetail;
                ContactName.Text = model.Contact;
                ContactNumber.Text = model.StoreTel;

            }
            else
            {
                SendAddress.Text = "";
                ContactName.Text = "";
                ContactNumber.Text = "";
            }

        }

        protected void CustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edge.SVA.Model.Customer model = new Edge.SVA.BLL.Customer().GetModel(Tools.ConvertTool.ConverType<int>(CustomerID.SelectedValue.ToString()));
            if (model != null)
            {
                SendAddress.Text = model.CustomerAddress;
                ContactName.Text = model.Contact;
                ContactNumber.Text = model.ContactPhone;

            }
            else
            {
                SendAddress.Text = "";
                ContactName.Text = "";
                ContactNumber.Text = "";
            }

        }
    }
}