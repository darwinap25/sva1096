﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CouponManagement.OrderManagement.CouponDelivery
{
    public partial class Print : Tools.BasePage<SVA.BLL.Ord_CouponDelivery_H, SVA.Model.Ord_CouponDelivery_H>
    {
        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                string ids = Request.Params["ids"];
                if (string.IsNullOrEmpty(ids))
                {
                    JscriptPrint(Resources.MessageTips.NotSelected, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
                DataTable orders = new DataTable();
                orders.Columns.Add("CouponDeliveryNumber", typeof(string));
                orders.Columns.Add("CreatedBusDate", typeof(string));
                orders.Columns.Add("ApprovalCode", typeof(string));
                orders.Columns.Add("ApproveBusDate", typeof(string));
                orders.Columns.Add("ApproveStatus", typeof(string));
                orders.Columns.Add("CreatedOn", typeof(string));
                orders.Columns.Add("CreatedBy", typeof(string));
                orders.Columns.Add("ApproveOn", typeof(string));
                orders.Columns.Add("ApproveBy", typeof(string));
                orders.Columns.Add("CustomerType", typeof(string));
                orders.Columns.Add("SendMethod", typeof(string));
                orders.Columns.Add("NeedActive", typeof(string));
                orders.Columns.Add("StoreID", typeof(string));
                orders.Columns.Add("Brand", typeof(string));
                orders.Columns.Add("CustomerID", typeof(string));
                orders.Columns.Add("Remark", typeof(string));
                orders.Columns.Add("OrdersCount", typeof(string));
                orders.Columns.Add("SendAddress", typeof(string));
                orders.Columns.Add("ContactName", typeof(string));
                orders.Columns.Add("Email", typeof(string));
                orders.Columns.Add("SMSMMS", typeof(string));
                orders.Columns.Add("ContactNumber", typeof(string));

                List<string> idList = Edge.Utils.Tools.StringHelper.SplitString(ids, ";");


                //if (Model.ApproveStatus.ToUpper().Trim() != "A")
                //{
                //    JscriptPrint(Resources.MessageTips.YouNotApprove, "List.aspx?page=0", Resources.MessageTips.WARNING_TITLE);
                //    return;
                //}

                //this.CouponDeliveryNumber.Text = Model.CouponDeliveryNumber;
                //this.CreatedBusDate.Text = ConvertTool.ToStringDate(Model.CreatedBusDate.GetValueOrDefault());
                //this.lblApproveStatus.Text = DALTool.GetApproveStatusString(Model.ApproveStatus);
                //this.CreatedOn.Text = ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
                //this.lblCreatedBy.Text = Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());

                //this.CustomerTypeView.Text = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                //this.CustomerType.Visible = false;

                //this.SendMethodView.Text = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                //this.SendMethod.Visible = false;
                
                //this.NeedActiveView.Text = NeedActive.SelectedItem == null ? "" : NeedActive.SelectedItem.Text;
                //this.NeedActive.Visible = false;

                //Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(Model.StoreID.GetValueOrDefault());
                //this.StoreID.Text = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);

                //Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                //this.ddlBrand.Text = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);

                //Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(Model.CustomerID.GetValueOrDefault());
                //this.CustomerID.Text = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                //RptBind(string.Format("CouponDeliveryNumber='{0}'", Request.Params["id"]));

                foreach (string id in idList)
                {
                    Edge.SVA.Model.Ord_CouponDelivery_H mode = new Edge.SVA.BLL.Ord_CouponDelivery_H().GetModel(id);

                    if (mode == null) continue;

                    DataRow order = orders.NewRow();

                    order["CouponDeliveryNumber"] = mode.CouponDeliveryNumber;
                    order["CreatedBusDate"] = ConvertTool.ToStringDate(mode.CreatedBusDate.GetValueOrDefault());
                    order["ApproveBusDate"] = ConvertTool.ToStringDate(mode.ApproveBusDate.GetValueOrDefault());
                    order["ApprovalCode"] = mode.ApprovalCode;
                    order["ApproveStatus"] = DALTool.GetApproveStatusString(mode.ApproveStatus);
                    order["CreatedOn"] = ConvertTool.ToStringDateTime(mode.CreatedOn.GetValueOrDefault());
                    order["CreatedBy"] = Tools.DALTool.GetUserName(mode.CreatedBy.GetValueOrDefault());
                    order["ApproveOn"] = ConvertTool.ToStringDateTime(mode.ApproveOn.GetValueOrDefault());
                    order["ApproveBy"] = Tools.DALTool.GetUserName(mode.ApproveBy.GetValueOrDefault());
                    CustomerType.SelectedValue = mode.CustomerType.GetValueOrDefault().ToString();
                    order["CustomerType"] = CustomerType.SelectedItem == null ? "" : CustomerType.SelectedItem.Text;
                    this.CustomerType.Visible = false;
                    SendMethod.SelectedValue = mode.SendMethod.GetValueOrDefault().ToString();
                    order["SendMethod"] = SendMethod.SelectedItem == null ? "" : SendMethod.SelectedItem.Text;
                    this.SendMethod.Visible = false;
                    NeedActive.SelectedValue = mode.NeedActive.GetValueOrDefault().ToString();
                    order["NeedActive"] = NeedActive.SelectedItem == null ? "" : NeedActive.SelectedItem.Text;
                    this.NeedActive.Visible = false;
                    Edge.SVA.Model.Store store = new Edge.SVA.BLL.Store().GetModel(mode.StoreID.GetValueOrDefault());
                    order["StoreID"] = store == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(store.StoreName1, store.StoreName2, store.StoreName3), store.StoreCode);
                    Edge.SVA.Model.Brand brand = store == null ? null : new Edge.SVA.BLL.Brand().GetModel(store.BrandID.GetValueOrDefault());
                    order["Brand"] = brand == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3), brand.BrandCode);
                    Edge.SVA.Model.Customer customer = new Edge.SVA.BLL.Customer().GetModel(mode.CustomerID.GetValueOrDefault());
                    order["CustomerID"] = customer == null ? "" : ControlTool.GetDropdownListText(DALTool.GetStringByCulture(customer.CustomerDesc1, customer.CustomerDesc2, customer.CustomerDesc3), customer.CustomerCode);

                    order["Remark"] = mode.Remark;
                    order["OrdersCount"] = Controllers.CouponOrderController.GetDeliveryOrderTotalQty(mode.CouponDeliveryNumber).ToString();
                    order["SendAddress"] = mode.SendAddress;
                    order["ContactName"] = mode.ContactName;
                    order["Email"] = mode.Email;
                    order["SMSMMS"] = mode.SMSMMS;
                    order["ContactNumber"] = mode.ContactNumber;

                    orders.Rows.Add(order);

                    this.rptOrders.DataSource = orders;
                    this.rptOrders.DataBind();
                }


            }
        }
        private int seq = 0;
        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //显示格式
                Label lblCouponTypeCode = (Label)e.Item.FindControl("lblCouponTypeCode");
                if (lblCouponTypeCode != null)
                {
                    Label lblCouponType = (Label)e.Item.FindControl("lblCouponType");
                    Label lblOrderQTY = (Label)e.Item.FindControl("lblOrderQTY");

                    //重复
                    if (ViewState["CouponTypeCode"] != null && ViewState["CouponTypeCode"].ToString().Trim() == lblCouponTypeCode.Text.Trim())
                    {
                        lblCouponTypeCode.Visible = false;
                        if (lblCouponType != null) { lblCouponType.Visible = false; }
                        if (lblOrderQTY != null) { lblOrderQTY.Visible = false; }
                    }
                    else//不重复
                    {
                        ViewState["CouponTypeCode"] = lblCouponTypeCode.Text.Trim();
                        if (lblCouponType != null)
                        {
                            ViewState["CouponType"] = lblCouponType.Text.Trim();
                        }
                        if (lblOrderQTY != null)
                        {
                            ViewState["OrderQTY"] = lblOrderQTY.Text.Trim();
                            //统计数量
                            ViewState["TotalOrderQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblOrderQTY.Text.Trim());
                        }
                        ((Label)e.Item.FindControl("lblSeq")).Text = (++seq).ToString();
                    }
                }

                Label lblPickQTY = (Label)e.Item.FindControl("lblPickQTY");
                if (lblPickQTY != null)
                {
                    //统计数量
                    ViewState["TotalPickQTY"] = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()) + Tools.ConvertTool.ConverType<long>(lblPickQTY.Text.Trim());
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblTotalOrderQTY = (Label)e.Item.FindControl("lblTotalOrderQTY");
                if (lblTotalOrderQTY != null)
                {
                    lblTotalOrderQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalOrderQTY"].ToString()).ToString();
                }
                Label lblTotalPickQTY = (Label)e.Item.FindControl("lblTotalPickQTY");
                if (lblTotalPickQTY != null)
                {
                    lblTotalPickQTY.Text = Tools.ConvertTool.ConverType<long>(ViewState["TotalPickQTY"].ToString()).ToString();
                }

            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater list = e.Item.FindControl("rptOrderList") as Repeater;
                if (list == null) return;

                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                if (drv == null) return;

                ViewState["CouponTypeCode"] = null;
                ViewState["CouponType"] = null;
                ViewState["OrderQTY"] = null;
                ViewState["TotalOrderQTY"] = 0;
                ViewState["TotalPickQTY"] = 0;

                System.Data.DataSet ds = new Edge.SVA.BLL.Ord_CouponDelivery_D().GetList(string.Format("CouponDeliveryNumber = '{0}'", drv["CouponDeliveryNumber"].ToString()) + " order by CouponTypeID");

                Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");
                Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");

                list.DataSource = ds.Tables[0];
                list.DataBind();
            }
        }

        #endregion


        #region 数据列表绑定
        private void RptBind(string strWhere)
        {
            //ViewState["CouponTypeCode"] = null;
            //ViewState["CouponType"] = null;
            //ViewState["OrderQTY"] = null;

            //Edge.SVA.BLL.Ord_CouponDelivery_D bll = new Edge.SVA.BLL.Ord_CouponDelivery_D();

            //System.Data.DataSet ds = null;

            //ds = bll.GetList(strWhere);
            
            //Tools.DataTool.AddCouponTypeNameByID(ds, "CouponType", "CouponTypeID");
            //Tools.DataTool.AddCouponTypeCode(ds, "CouponTypeCode", "CouponTypeID");

            //this.rptList.DataSource = ds.Tables[0].DefaultView;
            //this.rptList.DataBind();


            ////统计
            //long totalOrderQTY = 0;
            //long totalPickQTY = 0;
            //Controllers.CouponOrderController.GetApproveDeliveryTotal(Request.Params["id"], out totalOrderQTY, out totalPickQTY);
            //lblTotalOrderQTY.Text = totalOrderQTY.ToString();
            //lblTotalPickQTY.Text = totalPickQTY.ToString();
            //this.lblOrdersCount.Text = totalOrderQTY.ToString();
        }

        #endregion

    }
}