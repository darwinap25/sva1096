﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.BatchCreationOfCards
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Ord_CardBatchCreate, Edge.SVA.Model.Ord_CardBatchCreate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID, "IsImportCouponNumber = 0");
                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCardGrade(CardGradeID);
                //Edge.Web.Tools.ControlTool.BindCampaign(CampaignID);
            }
        }

        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(CardGradeID.SelectedValue))
                {
                    int cardGradeID = int.Parse(CardGradeID.SelectedValue);
                    Edge.SVA.Model.CardGrade model = new Edge.SVA.BLL.CardGrade().GetModel(cardGradeID);
                    //if (model.CampaignID != null)
                    //{
                    //    try
                    //    {
                    //        Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(model.CampaignID.GetValueOrDefault());

                    //        CampaignID.Text = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                    //    }
                    //    catch { }
                    //}

                    GetCreatedInfo(cardGradeID);
                }
                Edge.SVA.Model.CardGrade cardGradeModel=new Edge.SVA.BLL.CardGrade().GetModel(Model.CardGradeID);
                foreach (ListItem item in CardTypeID.Items)
                {
                    if (cardGradeModel.CardTypeID.ToString()==item.Value)
                    {
                        item.Selected = true;
                        break;
                    }
                }
                foreach (ListItem item in CardGradeID.Items)
                {
                    if (Model.CardGradeID.ToString()==item.Value)
                    {
                        item.Selected = true;
                        break;
                    }
                }
                if (Model.ApproveStatus == "A")
                {                    
                    if (Model.BatchCardID!=null)
                    {
                        int batchCardID = (int)Model.BatchCardID;
                        BatchCardID.Text = batchCardID.ToString("D12");
                    }
                    else
                    {
                        BatchCardID.Text = "";
                    }
                }
                else
                {
                    BatchCardID.Text = "";
                }
                this.lblApproveStatus.Text = Edge.Web.Tools.DALTool.GetApproveStatusString(Model.ApproveStatus);

                this.lblCreatedBy.Text = Edge.Web.Tools.DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.CreatedOn.Text = Edge.Web.Tools.ConvertTool.ToStringDateTime(Model.CreatedOn.GetValueOrDefault());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.Ord_CardBatchCreate item = this.GetUpdateObject();
            if (item != null)
            {
                Logger.Instance.WriteOperationLog(this.PageName, "Batch Creation Cards  :" + item.CardCreateNumber);
                string msg = "";
                long remainCards = 0;
                string lastCreatedCard = "";

                int checkResult = Controllers.CardController.GetCreatedCardInfo(item.CardGradeID, item.CardCount, ref remainCards, ref lastCreatedCard, ref msg);
                if (checkResult == -1)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.NumberDuplicate);
                    this.JscriptPrintAndFocus(Resources.MessageTips.NumberDuplicate + "  " + msg, "", Resources.MessageTips.WARNING_TITLE, this.CardCount.ClientID);
                    return;
                }
                else if (checkResult == -2)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.ExceededNumberOfRange);
                    this.JscriptPrintAndFocus(Resources.MessageTips.ExceededNumberOfRange, "", Resources.MessageTips.WARNING_TITLE, this.CardCount.ClientID);
                    return;
                }

                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Ord_CardBatchCreate>(item))
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.UpdateSuccess);
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Logger.Instance.WriteOperationLog(this.PageName, Resources.MessageTips.UpdateFailed);
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CardTypeID.SelectedValue))
            {
                CardGradeID.Items.Clear();
                return;
            }
            int cardTypeID = int.Parse(CardTypeID.SelectedValue);
            DataSet cardGradeDS = new Edge.SVA.BLL.CardGrade().GetList(" cardTypeID=" + cardTypeID.ToString());

            Edge.Web.Tools.ControlTool.BindDataSet(CardGradeID, cardGradeDS, "CardGradeID", "CardGradeName1", "CardGradeName2", "CardGradeName3","CardGradeCode");

        }
        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CardGradeID.SelectedValue))
                return;
            int cardGradeID = int.Parse(CardGradeID.SelectedValue);
            Edge.SVA.Model.CardGrade model = new Edge.SVA.BLL.CardGrade().GetModel(cardGradeID);

            if (model != null)
            {
                InitAmount.Text = Edge.Web.Tools.ConvertTool.ToStringAmount(model.CardTypeInitAmount);
                InitPoints.Text = model.CardTypeInitPoints.GetValueOrDefault().ToString();
                ExpiryDate.Text = Edge.Web.Tools.DALTool.GetCardTypeExpiryDate(model);
            }
            //this.CreatedCoupons.Text = bll.GetCount(string.Format("CouponTypeID = {0}", couponTypeID)).ToString();
            //if (model.CampaignID != null)
            //{
            //    try
            //    {
            //        Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(model.CampaignID.GetValueOrDefault());

            //        CampaignID.Text = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
            //    }
            //    catch { }
            //}

            GetCreatedInfo(cardGradeID);

        }

        private void GetCreatedInfo(int cardGradeID)
        {
            Edge.SVA.BLL.Card bll = new Edge.SVA.BLL.Card();
            this.CreatedCards.Text = bll.GetCount(string.Format("CardGradeID = {0}", cardGradeID)).ToString("N00");

            //check created
            string msg = "";
            long remainCards = 0;
            string lastCreatedCard = "";
            Controllers.CardController.GetCreatedCardInfo(cardGradeID, 0, ref remainCards, ref lastCreatedCard, ref msg);
            this.RemainCards.Text = remainCards.ToString("N00");
            this.LastCreatedCards.Text = lastCreatedCard;
        }
    }
}
