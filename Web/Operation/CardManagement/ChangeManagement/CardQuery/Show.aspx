﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery.Show" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });

    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <th colspan="2" align="left">
                所有者信息：
            </th>
        </tr>
        <tr>
            <td align="right" width="25%">
                会员姓名：
            </td>
            <td width="75%">
                <asp:Label ID="MemberEngGivenName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                会员编号：
            </td>
            <td>
                <asp:Label ID="MemberIdentityRef" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                性别：
            </td>
            <td>
                <asp:RadioButtonList ID="MemberSex" runat="server" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1">男</asp:ListItem>
                    <asp:ListItem Value="2">女</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                卡信息：
            </th>
        </tr>
        <tr>
            <td align="right">
                卡类型：
            </td>
            <td>
                <asp:Label ID="CardTypeID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡级别：
            </td>
            <td>
                <asp:Label ID="CardGradID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                优惠劵批次编号：
            </td>
            <td>
                <asp:Label ID="BatchCardID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡号：
            </td>
            <td>
                <asp:Label ID="CardNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡物理编号：
            </td>
            <td>
                <asp:Label ID="VendorCardNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                金额：
            </td>
            <td>
                <asp:Label ID="TotalAmount" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                积分：
            </td>
            <td>
                <asp:Label ID="TotalPoints" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建日期：
            </td>
            <td>
                <asp:Label ID="CardIssueDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                有效日期：
            </td>
            <td>
                <asp:Label ID="CardExpiryDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th colspan="2" align="left">
                优惠劵交易信息：
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="TXNList" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                            <tr>
                                <th width="6%">
                                    序号
                                </th>
                                <th width="6%">
                                    品牌编号
                                </th>
                                <th width="6%">
                                    店铺编号
                                </th>
                                <th width="6%">
                                    服务器编号
                                </th>
                                <th width="6%">
                                    收银机号
                                </th>
                                <th width="15%">
                                    交易类型
                                </th>
                                <th width="10%">
                                    交易编号
                                </th>
                                <th width="10%">
                                    业务日期
                                </th>
                                <th width="10%">
                                    交易日期
                                </th>
                                <th width="6%">
                                    授权号
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%#Eval("IDNumber")%>
                            </td>
                            <td align="center">
                                <%#Eval("BrandCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("StoreCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("ServerCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("RegisterCode")%>
                            </td>
                            <td align="center">
                                <%#Eval("OprIDName")%>
                            </td>
                            <td align="center">
                                <%#Eval("RefTxnNo")%>
                            </td>
                            <td align="center">
                                <%#Eval("BusDate","{0:yyyy-MM-dd}")%>
                            </td>
                            <td align="center">
                                <%#Eval("Txndate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td align="center">
                                <%#Eval("ApprovalCode")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="right">
                    <webdiyer:aspnetpager id="TXNListPager" runat="server" custominfotextalign="Left"
                        firstpagetext="First" horizontalalign="Right" invalidpageindexerrormessage="Page index is not a valid value."
                        lastpagetext="Last" nextpagetext="Next" pageindexboxtype="TextBox" pageindexoutofrangeerrormessage="Page index out of range!"
                        prevpagetext="Prev" showpageindexbox="Always" submitbuttontext="Go" submitbuttonclass="pagerSubmit"
                        textbeforepageindexbox="" onpagechanged="TXNListPager_PageChanged" cssclass="asppager"
                        currentpagebuttonclass="cpb" custominfoclass="asppagercustom" custominfohtml="Current:%CurrentPageIndex%/%PageCount% Total:%RecordCount% "
                        custominfosectionwidth="20%" showcustominfosection="Left" alwaysshow="False" layouttype="Table">
                    </webdiyer:aspnetpager>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="javascript:history.back()"
                        class="submit" />
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <script type="text/javascript">
       
        $(function() {
            $(".msgtablelist tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtablelist tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
</body>
</html>
