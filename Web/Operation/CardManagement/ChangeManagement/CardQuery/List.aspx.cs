﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.Operation.CardManagement.ChangeManagement.CardQuery
{
    public partial class List : Edge.Web.UI.ManagePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.rptListPager.PageSize = webset.ContentPageNum;

                ControlTool.BindCardType(CardTypeID);
                ControlTool.BindCardGrade(CardGradeID);
                ControlTool.BindCardStatus(Status);

               // SetControlText();

                RptBind(GetStrWhere());
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.rptListPager.CurrentPageIndex = 1;
            if (!this.hasInput())
            {
                this.CloseLoading();
                JscriptPrint(Resources.MessageTips.NoSearchCondition, "", Resources.MessageTips.WARNING_TITLE);
                RptBind("");
                return;
            }
            string strWhere = GetStrWhere();

            RptBind(strWhere);
        }

        protected void CardTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardTypeID.SelectedValue))
            {
                Edge.Web.Tools.ControlTool.BindDataSet(this.CardGradeID, null, null, null, null, null, null);
                return;
            }

            int cardTypeID = ConvertTool.ConverType<int>(this.CardTypeID.SelectedValue);

            Edge.Web.Tools.ControlTool.BindCardGrade(this.CardGradeID, cardTypeID);

        }

        protected void CardGradeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CardGradeID.SelectedValue))
            {
                this.CardBatchID.DataSource = Controllers.CardBatchController.GetInstance().GetBatch(10);
                return;
            }
            int cardGradeID = ConvertTool.ConverType<int>(this.CardGradeID.SelectedValue);

            this.CardBatchID.DataSource = Controllers.CardBatchController.GetInstance().GetBatch(10, cardGradeID);
        }

        protected void rptListPager_PageChanged(object sender, EventArgs e)
        {
            RptBind(GetStrWhere());
        }

        private void RptBind(string strWhere)
        {
            if (strWhere.Trim().Length<=3)
            {
                this.rptList.DataSource = null; ;
                this.rptList.DataBind();
                this.rptListPager.RecordCount = 0;
                this.CloseLoading();
                return;
            }

            int currentPage = this.rptListPager.CurrentPageIndex - 1 < 0 ? 0 : this.rptListPager.CurrentPageIndex - 1;

            Edge.SVA.BLL.Card card = new Edge.SVA.BLL.Card();

            this.rptListPager.RecordCount = card.GetCount(strWhere);

            DataSet ds = card.GetList(this.rptListPager.PageSize, currentPage, strWhere, "CardNumber");

            DataTool.AddID(ds, "ID", this.rptListPager.PageSize, currentPage);
            DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");
            DataTool.AddCardGradeName(ds, "CardGradeName", "CardGradeID");
            DataTool.AddCardUID(ds, "VendorCardNumber", "CardNumber");
            DataTool.AddCardBatchCode(ds, "BatchCode", "BatchCardID");
            DataTool.AddCardStatus(ds, "StatusName", "Status");

            if (ds.Tables[0].DefaultView.Count <= 0)
            {
                JscriptPrintAndClose(Resources.MessageTips.NoData, "", Resources.MessageTips.WARNING_TITLE);
                this.rptList.DataSource = null;
                this.rptList.DataBind();
                return;
            }
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();

            this.CloseLoading();
        }

        private string GetStrWhere()
        {
            string strWhere = " 1=1 ";

            if (!string.IsNullOrEmpty(this.CardTypeID.SelectedValue))
            {
                strWhere += string.Format(" and CardTypeID = {0} ", this.CardTypeID.SelectedValue);
            }

            if (!string.IsNullOrEmpty(this.CardGradeID.SelectedValue))
            {
                strWhere += string.Format(" and CardGradeID = {0} ", this.CardGradeID.SelectedValue);
            }
            if (this.CardBatchID.Value > 0)
            {
                strWhere += string.Format(" and BatchCardID = {0} ", this.CardBatchID.Value);
            }
            if (!string.IsNullOrEmpty(this.CardNumber.Text))
            {
                strWhere += string.Format(" and CardNumber = '{0}'  ", this.CardNumber.Text.Trim());
            }
            if (!string.IsNullOrEmpty(this.CardUID.Text))
            {
                strWhere += string.Format(" and CardNumber in (select CardNumber from CardUIDMap where CardUID =  '{0}')  ", this.CardUID.Text.Trim());
            }
            if (!string.IsNullOrEmpty(this.Amount.Text))
            {
                strWhere += string.Format(" and TotalAmount = '{0}'  ", this.Amount.Text.Replace(",","").Trim());
            }
            if (!string.IsNullOrEmpty(this.Point.Text))
            {
                strWhere += string.Format(" and TotalPoints = '{0}' ", this.Point.Text.Replace(",","").Trim());
            }
            if (!string.IsNullOrEmpty(this.Status.Text))
            {
                strWhere += string.Format(" and Status = '{0}'  ", this.Status.SelectedValue);
            }
            if (!string.IsNullOrEmpty(this.CreatedOn.Text))
            {
                strWhere += string.Format(" and Convert(char(10),CreatedOn,120) = '{0}'  ", this.CreatedOn.Text);
            }
            if (!string.IsNullOrEmpty(this.ExpiryDate.Text))
            {
                strWhere += string.Format("and Convert(char(10),CardExpiryDate,120) = '{0}'  ", this.ExpiryDate.Text);
            }
            return strWhere;
        }

        private bool hasInput()
        {
            if (!string.IsNullOrEmpty(this.CardTypeID.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.CardGradeID.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.CardBatchID.Text)) return true;

            if (!string.IsNullOrEmpty(this.CardNumber.Text)) return true;

            if (!string.IsNullOrEmpty(this.CardUID.Text)) return true;

            if (!string.IsNullOrEmpty(this.Status.SelectedValue)) return true;

            if (!string.IsNullOrEmpty(this.Amount.Text)) return true;

            if (!string.IsNullOrEmpty(this.Point.Text)) return true;

            if (!string.IsNullOrEmpty(this.CreatedOn.Text)) return true;

            if (!string.IsNullOrEmpty(this.ExpiryDate.Text)) return true;

            return false;
        }

       
    }
}
