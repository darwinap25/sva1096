﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web
{
    public partial class Test : System.Web.UI.Page
    {
        private string id = string.Empty;
        private string type = string.Empty;
        protected int recordCount = 0;
        protected int pageSize = 15;
        protected int pageIndex = 1;//1为第一页
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.InitData();
            }
        }
        /// <summary>
        /// 页面初始化
        /// </summary>
        private void InitData()
        {
            //搜索控件(注意字段类型，主要用于查询时拼接SQL是否加引号)
            List<ListItem> lstItem = new List<ListItem>() { 
                new ListItem("名称","TaskName|string|text"),
                new ListItem("所在组","GroupId|number|select"),
                new ListItem("类型","IsSimple|number|select"),
                new ListItem("进度","进度百分比|number|text"),
                new ListItem("开始时间","StartTime|dateTime|text"),
                new ListItem("结束时间","EndTime|dateTime|text"),
                new ListItem("参与人数","参与人数|number|text"),
                new ListItem("总积分","任务已通过审核的总积分|number|text"),
                new ListItem("人均分","人均分|number|text"),
                new ListItem("详细数","详细任务数|number|text"),
                new ListItem("未提交数","未提交的任务详细数|number|text"),
                new ListItem("待审核数","未审核的任务详细数|number|text"),
                new ListItem("已通过数","已通过审核的任务详细数|number|text"),
                new ListItem("未通过数","已拒绝的任务详细数|number|text"),
                new ListItem("创建者","创建人名|string|text"),
                new ListItem("创建时间","CreateTime|dateTime|text"),
                new ListItem("修改人","修改人名|string|text"),
                new ListItem("修改时间","EditTime|dateTime|text"),
                new ListItem("审核状态","VerifyState|number|select"),
                new ListItem("有效性","State|number|select")
            };
            this.SearchBox1.TypeList = lstItem;
            List<string> fields = new List<string>();
            foreach (ListItem m in lstItem)
            {
                fields.Add((m.Value.Split('|'))[0]);
            }

            string strSearchMsg = string.Empty;
            string strWhere = Edge.Web.Tools.SearchBox.PublicMethod.GetSearchStrByUrl(Request.QueryString["where"], fields, out strSearchMsg);
            if (strSearchMsg.Length > 0)
            {
                this.SearchBox1.strMsg = strSearchMsg.ToString();
            }


           this.Label1.Text = "生成的查询SQL已经过防注入处理，对查询字段及格式和输入内容都限制过：<br/><br/>" + strWhere;


        }
        protected string GetMore(string type)
        {
            string str = string.Empty;
            switch (type)
            {
                case "group":
                    str = @"<option value=""0"">1组</option><option value=""1"">2组</option><option value=""2"">3组</option>";
                    break;
                case "type":
                    str = @"<option value=""0"">aaa</option><option value=""1"">bbb</option><option value=""2"">ccc</option>";
                    break;
                case "ver":
                    str = @"<option value=""0"">333</option><option value=""1"">1111</option><option value=""2"">xxxx</option>";
                    break;
                case "state":
                    str = @"<option value=""0"">aaaaaaaaaaaaaa</option><option value=""1"">bbbbbbb</option><option value=""2"">cccccccc</option>";
                    break;
            }
            return str;
        }
    }
}
