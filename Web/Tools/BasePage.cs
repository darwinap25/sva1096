﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Reflection;
using System.Web.UI.WebControls;


namespace Edge.Web.Tools
{
    /// <summary>
    /// 设置页面值(根据MVC模式)
    /// </summary>
    /// <typeparam name="bll">页面相关的业务逻辑类</typeparam>
    /// <typeparam name="Moele">页面相关的Model类</typeparam>
    public class BasePage<BLLClass, ModelClass> : Edge.Web.UI.ManagePage where BLLClass : new() where ModelClass :class, new()
    {
        private ModelClass model = default(ModelClass);

        public ModelClass Model
        {
            get { return model; }
            set { model = value; }
        }
        /// <summary>
        /// 加载完成时设置控件值，若需要修改控件值，在子类重写OnLoadComplete在加载完基本后
        /// </summary>
        /// <param name="e"></param>
        protected override void  OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                string id = Request.Params["id"];
                if (string.IsNullOrEmpty(id)) return;

                this.Model =GetObject(id) as ModelClass;
                SetObject(this.Model);
            }

        }
       
        #region 获取、设置控件的值 
        /// <summary>
        /// 当新增自定义控件时，请修改此函数(同时修改设置控件函数)
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        private string GetControlText(System.Web.UI.Control con)
        {
            if (con is Label)
            {
                Label lbl = con as Label;
                return lbl.Text;
            }
            else if (con is TextBox)
            {
                TextBox txt = con as TextBox;
                return txt.Text;
            }
            else if (con is DropDownList)
            {
                DropDownList ddl = con as DropDownList;
                return ddl.SelectedValue;
            }
            else if (con is RadioButtonList)
            {
                RadioButtonList rbl = con as RadioButtonList;
                return rbl.SelectedValue;
            }
            else if (con is HiddenField)
            {
                HiddenField hf = con as HiddenField;
                return hf.Value;
            }
            else if (con is Edge.Web.Controls.UploadFileBox)
            {
                Edge.Web.Controls.UploadFileBox ufb = con as Edge.Web.Controls.UploadFileBox;
                return ufb.Text;
            }
      
          
            return "";
        }

        /// <summary>
        /// 当新增自定义控件时，请修改此函数(同时修改获取控件函数)
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        private void SetControlText(string value, System.Web.UI.Control con)
        {
            if (con is Label)
            {
                Label lbl = con as Label;
                lbl.Text = value == null ? "" : value;
            }
            else if (con is TextBox)
            {
                TextBox txt = con as TextBox;
                txt.Text = value == null ? "" : value;
            }
            else if (con is DropDownList)
            {
                DropDownList ddl = con as DropDownList;
                ddl.SelectedValue = value == null ? "" : value;
            }
            else if (con is RadioButtonList)
            {
                RadioButtonList rbl = con as RadioButtonList;
                rbl.SelectedValue = value == null ? "" : value;
            }
            else if (con is HiddenField)
            {
                HiddenField hf = con as HiddenField;
                hf.Value = value == null ? "" : value;
            }
            else if (con is Edge.Web.Controls.UploadFileBox)
            {
                Edge.Web.Controls.UploadFileBox ufb = con as Edge.Web.Controls.UploadFileBox;
                ufb.Text = value == null ? "" : value;
            }
         
        }

        #endregion

        #region 获取、设置对象的值 
        /// <summary>
        /// 设置Form下Control的值
        /// </summary>
        /// <param name="obj"></param>
        private void SetObject(object obj)
        {
            if (obj == null) return;
            PropertyInfo[] properties = obj.GetType().GetProperties();
            foreach (PropertyInfo p in properties)
            {               
                foreach (System.Web.UI.Control con in this.Form.Controls)
                {
                    if (con.ID != p.Name) continue;

                    object value = p.GetValue(obj, null);
                    string text = GetTextByType(value);
                    SetControlText(text, con);
                    break;
                }
            }
        }

        protected ModelClass GetUpdateObject()
        {
            if (string.IsNullOrEmpty(Request.Params["id"])) return default(ModelClass);

            return GetUpdateObject(Request.Params["id"]);
        }

        protected ModelClass GetUpdateObject(object id)
        {

            ModelClass obj = this.GetObject(id);
            if (obj == null) return default(ModelClass);

            return GetPageObject(obj);
        }
        protected ModelClass GetAddObject()
        {
            ModelClass m = new ModelClass();
            return GetPageObject(m);
        }

        private ModelClass GetPageObject(ModelClass obj)
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();

            foreach (PropertyInfo p in properties)
            {
                foreach (System.Web.UI.Control con in this.Form.Controls)
                {
                    if (con.ID != p.Name) continue;
                    string text = GetControlText(con);
                    object objValue = GetValue(p, text);
                    p.SetValue(obj, objValue, null);
                    break;
                }
            }
            return obj as ModelClass;
        }
        #endregion

        #region Common 私有辅助函数

        private ModelClass GetObject(object id)
        {
            return DALTool.GetObject<BLLClass>(id) as ModelClass;
        }

        private string GetTextByType(object value)
        {
            if (value == null) return "";

            Type type = value.GetType();

            if (type.FullName == "System.String")
            {
                return value.ToString();
            }
            else if (type.FullName == "System.DateTime")
            {
                DateTime dt = (DateTime)value;
                return dt.ToString("yyyy-MM-dd");
            }
            else if (type.FullName == "System.Decimal")
            {
                decimal num = (decimal)value;
                return num.ToString("N");
            }
            else if (type.FullName == "System.Single")
            {
                Single num = (Single)value;
                return num.ToString("N");
            }
            else if (type.FullName == "System.Double")
            {
                Double num = (Double)value;
                return num.ToString("N");
            }
            else if (type.FullName == "System.Int32")
            {
                int num = (int)value;
                return num.ToString("N00");
            }

            return value.ToString();
        }

        private object GetValue(PropertyInfo pi, string value)          //todo:必填字段，页面没加验证。这里要判断
        {
            if (pi.PropertyType.Name == "Nullable`1")             //如int? DateTime?
            {
                if (string.IsNullOrEmpty(value)) return null;
                PropertyInfo pValue = pi.PropertyType.GetProperty("Value");
                return GetValue(pValue, value);
            }
            if (pi.PropertyType.FullName == "System.Int32")
            {
                value = value.Replace(",", "");
            }
           
            return ConvertTool.ChangeType(pi.PropertyType,value);
        }

        #endregion 
    }
}
