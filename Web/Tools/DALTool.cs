﻿using System;
using System.Collections.Generic;
using System.Web;
using Edge.Security.Manager;
using System.Web.Caching;
using Edge.Common;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using Edge.Web.Controllers;
using System.Text;

namespace Edge.Web.Tools
{
    /// <summary>
    /// 数据访问工具
    /// </summary>
    public class DALTool
    {
        #region 根据ID，获取名称

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public static string GetUserName(int userId)
        {
            System.Data.DataRow user = new Edge.Security.Data.User().Retrieve(userId);

            return user == null ? "" : user["UserName"].ToString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public static string GetUserName(int userId, Dictionary<int, string> cache)
        {
            if (userId == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(userId)) return cache[userId];

            System.Data.DataRow user = new Edge.Security.Data.User().Retrieve(userId);
            string result = user == null ? "" : user["UserName"].ToString();

            if (cache != null) cache.Add(userId, result);

            return result;
        }

        /// <summary>
        /// 获取当前登录用户
        /// </summary>
        /// <param name="siteLanguage"></param>
        /// <returns></returns>
        public static User GetCurrentUser()
        {
            Edge.Security.Model.WebSet set = HttpContext.Current.Cache["Cache_Webset"] as Edge.Security.Model.WebSet;
            if (set == null)
            {
                set = new Edge.Security.Manager.WebSet().loadConfig(Edge.Common.Utils.GetXmlMapPath("Configpath"));
            }

            if (HttpContext.Current.Session["UserInfo"] != null)
            {
                return HttpContext.Current.Session["UserInfo"] as User;
            }
            else
            {
                AccountsPrincipal user = new AccountsPrincipal(HttpContext.Current.User.Identity.Name, HttpContext.Current.Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                User currentUser = new Edge.Security.Manager.User(user);

                return currentUser;            
            }
        }

        public static string GetCardIssuerName()
        {
            List<Edge.SVA.Model.CardIssuer> cardIssuers = new SVA.BLL.CardIssuer().GetModelList("");
            if (cardIssuers == null || cardIssuers.Count <= 0) return "";

            return DALTool.GetStringByCulture(cardIssuers[0].CardIssuerName1, cardIssuers[0].CardIssuerName2, cardIssuers[0].CardIssuerName3);

        }

        /// <summary>
        /// 根据ID获取行业名称,若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">行业Id</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetIndustryName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Industry bll = new Edge.SVA.BLL.Industry();
            Edge.SVA.Model.Industry model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.IndustryName1, model.IndustryName2, model.IndustryName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetApproveStatusString(string status)
        {
            switch (status.ToUpper())
            {
                case "P": return "PENDING";
                case "A": return "APPROVED";
                case "V": return "VOID";
                default: return "";


            }
        }

        public static string GetOrderPickingApproveStatusString(string status)
        {
            switch (status.ToUpper())
            {
                case "R": return "PENDING";
                case "P": return "PICKED";
                case "A": return "APPROVED";
                case "V": return "VOID";
                default: return "";
            }
        }

        /// <summary>
        /// 根据ID获取发行商名称,若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">发行商Id</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardIssuerName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardIssuer bll = new Edge.SVA.BLL.CardIssuer();
            Edge.SVA.Model.CardIssuer model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CardIssuerName1, model.CardIssuerName2, model.CardIssuerName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        ///根据ID获取品牌名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetBrandName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Brand bll = new Edge.SVA.BLL.Brand();
            Edge.SVA.Model.Brand model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.BrandName1, model.BrandName2, model.BrandName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        ///根据ID获取品牌名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetBrandCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.Model.Brand model = new Edge.SVA.BLL.Brand().GetModel(id);

            string result = model == null ? "" : model.BrandCode;

            if (cache != null) cache.Add(id, result.ToUpper());

            return result;
        }


        /// <summary>
        ///根据ID获取店铺编号，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">店铺ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetStoreCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.Model.Store model = new Edge.SVA.BLL.Store().GetModel(id);

            string result = model == null ? "" : model.StoreCode;

            if (cache != null) cache.Add(id, result.ToUpper());

            return result;
        }


        /// <summary>
        /// 根据StoreCode获取BrandID
        /// </summary>
        /// <param name="storeCode"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static int GetBrandIDByStoreCode(string storeCode, Dictionary<string, int> cache)
        {
            if (string.IsNullOrEmpty(storeCode)) return 0;

            if (cache != null && cache.ContainsKey(storeCode)) return Tools.ConvertTool.ToInt(cache[storeCode].ToString());

            
            List<Edge.SVA.Model.Store> list = new Edge.SVA.BLL.Store().GetModelList(string.Format("StoreCode='{0}'", storeCode));

            Edge.SVA.Model.Store model = null;

            if (list.Count > 0)
            {
                model = list[0];
            }
            else
            {
                return 0;
            }

            int result = model == null ? 0 : model.BrandID.GetValueOrDefault();

            if (cache != null) cache.Add(storeCode, result);

            return result;
        }

    
        /// <summary>
        ///根据ID获取区域名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetLocationName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.StoreGroup bll = new Edge.SVA.BLL.StoreGroup();
            Edge.SVA.Model.StoreGroup model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.StoreGroupName1, model.StoreGroupName2, model.StoreGroupName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }


        /// <summary>
        ///根据ID获取店铺名称，若存入id == int.MinValue 返回空
        /// </summary>
        /// <param name="id">品牌ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetStoreName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Store bll = new Edge.SVA.BLL.Store();
            Edge.SVA.Model.Store model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.StoreName1, model.StoreName2, model.StoreName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡类型ID，获取卡类型名称
        /// </summary>
        /// <param name="id">卡类型ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardTypeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();
            Edge.SVA.Model.CardType model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CardTypeName1, model.CardTypeName2, model.CardTypeName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetCardTypeCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();
            Edge.SVA.Model.CardType model = bll.GetModel(id);

            string result = model == null ? "" : model.CardTypeCode;

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡级别ID，获取卡级别名称
        /// </summary>
        /// <param name="id">卡级别ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardGradeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();
            Edge.SVA.Model.CardGrade model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CardGradeName1, model.CardGradeName2, model.CardGradeName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetTnxTypeName(int id)
        {
            return new Edge.SVA.BLL.Coupon_Movement().GetTxnType(id);
        }
        /// <summary>
        /// 根据优惠劵类型ID，获取优惠劵名称
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCouponTypeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.Model.CouponType model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CouponTypeName1, model.CouponTypeName2, model.CouponTypeName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetCouponTypeName(string code, Dictionary<string, string> cache)
        {
            if (string.IsNullOrEmpty(code)) return "";

            if (cache != null && cache.ContainsKey(code)) return cache[code];

            Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetImportCouponType(code);

            string result = model == null ? "" : GetStringByCulture(model.CouponTypeName1, model.CouponTypeName2, model.CouponTypeName3);

            if (cache != null) cache.Add(code, result);

            return result;
        }
        /// <summary>
        /// 根据优惠劵类型ID，获取优惠劵名称
        /// </summary>
        /// <param name="id">优惠劵ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCouponTypeCode(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CouponType bll = new Edge.SVA.BLL.CouponType();
            Edge.SVA.Model.CouponType model = bll.GetModel(id);

            string result = model == null ? "" : model.CouponTypeCode;

            if (cache != null) cache.Add(id, result);

            return result;
        }
        /// <summary>
        /// 根据联系方式类型ID，获取联系方式名称
        /// </summary>
        /// <param name="id">联系方式类型ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetSNSTypeName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.SNSType bll = new Edge.SVA.BLL.SNSType();
            Edge.SVA.Model.SNSType model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.SNSTypeName1, model.SNSTypeName2, model.SNSTypeName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据卡类型种类ID，获取卡类型种类名称
        /// </summary>
        /// <param name="id">卡类型种类ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCardTypeNatureName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.CardTypeNature bll = new Edge.SVA.BLL.CardTypeNature();
            Edge.SVA.Model.CardTypeNature model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CardTypeNatureName1, model.CardTypeNatureName2, model.CardTypeNatureName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        /// <summary>
        /// 根据币种ID，获取币种名称
        /// </summary>
        /// <param name="id">币种ID</param>
        /// <param name="cache">缓存已经读取的ID</param>
        /// <returns></returns>
        public static string GetCurrencyName(int id, Dictionary<int, string> cache)
        {
            if (id == int.MinValue) return "";

            if (cache != null && cache.ContainsKey(id)) return cache[id];

            Edge.SVA.BLL.Currency bll = new Edge.SVA.BLL.Currency();
            Edge.SVA.Model.Currency model = bll.GetModel(id);

            string result = model == null ? "" : GetStringByCulture(model.CurrencyName1, model.CurrencyName2, model.CurrencyName3);

            if (cache != null) cache.Add(id, result);

            return result;
        }

        public static string GetUID(string couponNumber, Dictionary<string, string> cache)
        {
            if (string.IsNullOrEmpty(couponNumber)) return couponNumber;

            if (cache != null && cache.ContainsKey(couponNumber)) return cache[couponNumber];

            Edge.SVA.BLL.CouponUIDMap bll = new Edge.SVA.BLL.CouponUIDMap();
            List<Edge.SVA.Model.CouponUIDMap> model = bll.GetModelList(string.Format("CouponNumber = '{0}'", couponNumber));

            if (model == null || model.Count <= 0) return "";

            if (cache != null) cache.Add(model[0].CouponNumber, model[0].CouponUID);

            return model[0].CouponUID;
        }

        public static string GetCardUID(string cardNumber, Dictionary<string, string> cache)
        {
            if (string.IsNullOrEmpty(cardNumber)) return cardNumber;

            if (cache != null && cache.ContainsKey(cardNumber)) return cache[cardNumber];

            Edge.SVA.BLL.CardUIDMap bll = new Edge.SVA.BLL.CardUIDMap();
            List<Edge.SVA.Model.CardUIDMap> model = bll.GetModelList(string.Format("CardNumber = '{0}'", cardNumber));

            if (model == null || model.Count <= 0) return "";

            if (cache != null) cache.Add(model[0].CardNumber, model[0].CardUID);

            return model[0].CardUID;
        }

        public static string GetCouponTypeStatusName(int status)
        {
            switch (status)
            {
                case 0: return Controllers.CouponController.CouponStatus.Dormant.ToString().ToUpper();
                case 1: return Controllers.CouponController.CouponStatus.Issued.ToString().ToUpper();
                case 2: return Controllers.CouponController.CouponStatus.Activated.ToString().ToUpper();
                case 3: return Controllers.CouponController.CouponStatus.Redeemed.ToString().ToUpper();
                case 4: return Controllers.CouponController.CouponStatus.Expired.ToString().ToUpper();
                case 5: return Controllers.CouponController.CouponStatus.Voided.ToString().ToUpper();
                case 6: return Controllers.CouponController.CouponStatus.Recycled.ToString().ToUpper();
            }
            return "";
        }

        public static string GetCardStatusName(int status)
        {
            switch (status)
            {
                case 0: return Controllers.CardController.CardStatus.Dormant.ToString().ToUpper();
                case 1: return Controllers.CardController.CardStatus.Issued.ToString().ToUpper();
                case 2: return Controllers.CardController.CardStatus.Active.ToString().ToUpper();
                case 3: return Controllers.CardController.CardStatus.Redeemed.ToString().ToUpper();
                case 4: return Controllers.CardController.CardStatus.Expired.ToString().ToUpper();
                case 5: return Controllers.CardController.CardStatus.Void.ToString().ToUpper();
            }
            return "";
        }
        
        public static Edge.SVA.Model.CouponType GetCouponType(int couponTypeID, Dictionary<int, Edge.SVA.Model.CouponType> cache)
        {
            if (cache != null && cache.ContainsKey(couponTypeID)) return cache[couponTypeID];

            Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);

            if (cache != null) cache.Add(couponTypeID, couponType);

            return couponType;
        }

        public static string GetBatchCode(int batchID, Dictionary<int, string> cache)
        {
            if (batchID <= 0) return "";

            if (cache != null && cache.ContainsKey(batchID)) return cache[batchID];

            Edge.SVA.BLL.BatchCoupon bll = new Edge.SVA.BLL.BatchCoupon();
            Edge.SVA.Model.BatchCoupon model = bll.GetModel(batchID);

            if (model == null) return "";

            if (cache != null) cache.Add(batchID, model.BatchCouponCode);

            return model.BatchCouponCode;
        }

        public static string GetCardBatchCode(int batchID, Dictionary<int, string> cache)
        {
            if (batchID <= 0) return "";

            if (cache != null && cache.ContainsKey(batchID)) return cache[batchID];

            Edge.SVA.BLL.BatchCard bll = new Edge.SVA.BLL.BatchCard();
            Edge.SVA.Model.BatchCard model = bll.GetModel(batchID);

            if (model == null) return "";

            if (cache != null) cache.Add(batchID, model.BatchCardCode);

            return model.BatchCardCode;
        }

        public static string GetBrandCodeByCouponTypeID(int couponTypeID, Dictionary<int, string> cache)
        {
            if (couponTypeID <= 0) return "";

            if (cache != null && cache.ContainsKey(couponTypeID)) return cache[couponTypeID];

            Edge.SVA.Model.CouponType couponType = new Edge.SVA.BLL.CouponType().GetModel(couponTypeID);
            if (couponType == null) return "";

            Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(couponType.BrandID);

            if (brand == null) return "";

            if (cache != null) cache.Add(couponTypeID, brand.BrandCode);

            return brand.BrandCode;
        }

        public static string GetPasswordFormatName(int format)
        {
            switch (format)
            {
                case 0: return "No Requirement";
                case 1: return "Numbers";
                case 2: return "Alphabets";
                case 3: return "Numbers + Alphabets";
                case 4: return "Numbers +Alphabets +Symbols";
                default: return "";
                
            }
        }
        #endregion

        #region GetObject Update Add Delete

        public static object GetObject<BLL>(object id) where BLL : new()
        {
            return DALTool.ExecuteMethod<BLL>("GetModel", new object[] { id });
        }

        public static bool Update<BLL>(object obj) where BLL : new()
        {
            if (obj == null) return false;
            object result = ExecuteMethod<BLL>("Update", new object[] { obj });
            if (result is bool) return (bool)result;

            return false;
        }

        public static bool Delete<BLL>(object id) where BLL : new()
        {
            if (id == null) return false;
            object result = ExecuteMethod<BLL>("Delete", new object[] { id });
            if (result is bool) return (bool)result;
            if (result is int) return (int)result > 0 ? true : false;


            return false;
        }

        public static int Add<BLL>(object obj) where BLL : new()
        {
            if (obj == null) return -1;
            object result = ExecuteMethod<BLL>("Add", new object[] { obj });

            if (result is int) return (int)result;
            if (result is bool) return (bool)result ? 1 : -1;
            if (result is Guid) return (Guid)result == Guid.Empty ? -1 : 1;

            return 0;
        }

        #endregion

        #region Common

        private static object ExecuteMethod<T>(string method, object[] parameters) where T : new()
        {

            T obj = new T();
            Type type = typeof(T);
            System.Reflection.MethodInfo mi = null;
            try
            {
                mi = type.GetMethod(method);
            }
            catch
            {
                if (mi == null)
                {
                    Type[] args = GetTypes(parameters);
                    mi = type.GetMethod(method, args);
                }
            }
            if (mi == null) return default(T);


            System.Reflection.ParameterInfo[] pis = mi.GetParameters();

            for (int i = 0; i < pis.Length; i++)
            {
                parameters[i] = Convert.ChangeType(parameters[i], pis[i].ParameterType);
            }
            return mi.Invoke(obj, parameters);
        }

        private static Type[] GetTypes(object[] args)
        {
            if (args == null) return Type.EmptyTypes;
            Type[] types = new Type[args.Length];
            for (int i = 0; i < types.Length; i++)
            {
                types[i] = args[i].GetType();
            }
            return types;
        }

        /// <summary>
        /// 根据当前语言区域获取各自语言
        /// </summary>
        /// <param name="en">英文</param>
        /// <param name="zhCN">简体中文</param>
        /// <param name="zhHK">繁体中文</param>
        /// <returns></returns>
        public static string GetStringByCulture(string name1, string name2, string name3)
        {
            switch (System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower())
            {
                case "en-us": return name1;
                case "zh-cn": return name2;
                case "zh-hk": return name3;
                default: return name1;
            }
        }

        #endregion

        #region Common Function
        public static int SODEODCreateJob(DateTime dt)
        {
            int rowEffect = 0;
            int rtnVal = 0;
            dt = DateTime.Today;

            IDataParameter[] parameters = { 
                 new SqlParameter("@jobname", SqlDbType.VarChar,30) ,
                 new SqlParameter("@sql", SqlDbType.VarChar,512) ,
                 new SqlParameter("@servername", SqlDbType.VarChar,30),
                 new SqlParameter("@dbname", SqlDbType.VarChar,30),
                 new SqlParameter("@freqtype", SqlDbType.VarChar,30),
                 new SqlParameter("@time", SqlDbType.VarChar,30)
             };
            parameters[0].Value = "SODEOD";
            parameters[1].Value = @"USE SVA_RRG
GO
declare @Today Date
set @Today=getdate()

exec dbo.EOD 1,@Today";
            parameters[2].Value = "DBRECOVERY1";
            parameters[3].Value = "SVA_RRG";
            parameters[4].Value = "day";
            parameters[5].Value = dt.ToString("HHmmss");
            parameters[6].Direction = ParameterDirection.Output;

            rtnVal = DBUtility.DbHelperSQL.RunProcedure("CreateScheduleJob", parameters, out rowEffect);

            return rtnVal;
        }
        public static int ExecSODEOD(int userID,out DateTime dt)
        {
            int rowEffect=0;
            int rtnVal = 0;
            dt = DateTime.Today;

            IDataParameter[] parameters = { 

                 new SqlParameter("@UserID", SqlDbType.Int) , 

                 new SqlParameter("@BusDate", SqlDbType.Date) 

             };
            parameters[0].Value = userID;
            parameters[1].Direction = ParameterDirection.Output;

            rtnVal=DBUtility.DbHelperSQL.RunProcedure("EOD",parameters,out rowEffect);
            if (parameters[1].Value != null)
            {
                DateTime.TryParse(parameters[1].Value.ToString(),out dt);
            }
            return rtnVal;
        }

        public static string GetREFNOCode(string code)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { 

                 new SqlParameter("@CODE", SqlDbType.VarChar,6) , 

                 new SqlParameter("@REFNO", SqlDbType.NVarChar,50) 

             };
            parameters[0].Value = code;
            parameters[1].Direction = ParameterDirection.Output;

            DBUtility.DbHelperSQL.RunProcedure("GetRefNoString", parameters, "ds");
            if (parameters[1].Value != null)
            {
                rtn = parameters[1].Value.ToString();
            }
            return rtn;
        }

        public static string GetOrdCouponAdjustApproveCode(string couponAdjustNumber)
        {
            string rtn = string.Empty;

            Edge.SVA.BLL.Ord_CouponAdjust_H bll = new Edge.SVA.BLL.Ord_CouponAdjust_H();

            Edge.SVA.Model.Ord_CouponAdjust_H model = bll.GetModel(couponAdjustNumber);
            if (model != null)
            {
                rtn = model.ApprovalCode;
            }
            return rtn;
        }

        public static string GetBusinessDate()
        {
            string rtn = string.Empty;

            DataSet ds = DBUtility.DbHelperSQL.Query("select BusDate from SODEOD where SOD=1 and EOD=0 ");
            if (ds.Tables.Count > 0)
            {
                rtn = Convert.ToDateTime(ds.Tables[0].Rows[0]["BusDate"]).ToString("yyyy-MM-dd");
            }
            return rtn;
        }

        public static string GetSystemDate()
        {
            return System.DateTime.Now.ToString("yyyy-MM-dd");

        }

        public static string GetSystemDateTime()
        {
            return System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        }

        public static string GetCouponTypeExpiryDate(Edge.SVA.Model.CouponType model)
        {
            DateTime nowDate = System.DateTime.Now;

            switch (model.CouponValidityUnit)
            {
                case 0: break;
                case 1: DateTime newYearDate = nowDate.AddYears(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(newYearDate);
                case 2: DateTime newMonthDate = nowDate.AddMonths(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(newMonthDate);
                case 3: DateTime newWeekDate = nowDate.AddDays(model.CouponValidityDuration * 7);
                    return ConvertTool.ToStringDate(newWeekDate);
                case 4: DateTime newDayDate = nowDate.AddDays(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(newDayDate);
                case 5: DateTime newDate = nowDate.AddYears(model.CouponValidityDuration);
                    return ConvertTool.ToStringDate(model.CouponTypeEndDate.Value);
                case 6: DateTime spDate = model.CouponSpecifyExpiryDate.GetValueOrDefault();
                    return ConvertTool.ToStringDate(spDate);

            }

            return GetSystemDate();

        }

        public static string GetCardTypeExpiryDate(Edge.SVA.Model.CardGrade model)
        {
            DateTime nowDate = System.DateTime.Now;

            switch (model.CardValidityUnit)
            {
                case 0: break;
                case 1: DateTime newYearDate = nowDate.AddYears(model.CardValidityDuration.GetValueOrDefault());
                    return ConvertTool.ToStringDate(newYearDate);
                case 2: DateTime newMonthDate = nowDate.AddMonths(model.CardValidityDuration.GetValueOrDefault());
                    return ConvertTool.ToStringDate(newMonthDate);
                case 3: DateTime newWeekDate = nowDate.AddDays(model.CardValidityDuration.GetValueOrDefault() * 7);
                    return ConvertTool.ToStringDate(newWeekDate);
                case 4: DateTime newDayDate = nowDate.AddDays(model.CardValidityDuration.GetValueOrDefault());
                    return ConvertTool.ToStringDate(newDayDate);
                //case 5: DateTime newDate = nowDate.AddYears(model.CardValidityDuration);
                //    return ConvertTool.ToStringDate(model.);

            }

            return GetSystemDate();

        }
        #endregion

        #region Checking Function
        public static bool isHasStoreCodeWithBrandID(string storecode, int brandID, int storeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("StoreCode='" + storecode + "' and BrandID=" + brandID);
            if (storeID > 0)
            {
                sbWhere.Append(" and StoreID <> " + storeID);
            }
            int count = new Edge.SVA.BLL.Store().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查BrandCode是否存在，当brandID=0时是新增时用。
        /// </summary>
        /// <param name="brandCode"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        public static bool isHasBrandCode(string brandCode, int brandID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" BrandCode='" + brandCode + "' ");
            if (brandID > 0)
            {
                sbWhere.Append(" and BrandID <> " + brandID);
            }
            int count = new Edge.SVA.BLL.Brand().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查CampaignCode是否存在，当campaignID=0时是新增时用。
        /// </summary>
        /// <param name="campaignCode"></param>
        /// <param name="campaignID"></param>
        /// <returns></returns>
        public static bool isHasCampaignCode(string campaignCode, int campaignID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CampaignCode='" + campaignCode + "'");
            if (campaignID >0)
            {
                sbWhere.Append(" and CampaignID <> " + campaignID);
            }

            int count = new Edge.SVA.BLL.Campaign().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查EducationCode是否存在，当educationID=0时是新增时用。
        /// </summary>
        /// <param name="educationCode"></param>
        /// <param name="educationID"></param>
        /// <returns></returns>
        public static bool isHasEducationCode(string educationCode, int educationID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("EducationCode='" + educationCode + "'");
            if (educationID > 0)
            {
                sbWhere.Append(" and EducationID <> " + educationID);
            }

            int count = new Edge.SVA.BLL.Education().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查NationCode是否存在，当nationID=0时是新增时用。
        /// </summary>
        /// <param name="nationCode"></param>
        /// <param name="nationID"></param>
        /// <returns></returns>
        public static bool isHasNationCode(string nationCode, int nationID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("NationCode='" + nationCode + "'");
            if (nationID >0)
            {
                sbWhere.Append(" and NationID <> " + nationID);
            }

            int count = new Edge.SVA.BLL.Nation().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查ProfessionCode是否存在，当professionID=0时是新增时用。
        /// </summary>
        /// <param name="professionCode"></param>
        /// <param name="professionID"></param>
        /// <returns></returns>
        public static bool isHasProfessionCode(string professionCode, int professionID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("ProfessionCode='" + professionCode + "'");
            if (professionID >0)
            {
                sbWhere.Append(" and ProfessionID <> " + professionID);
            }

            int count = new Edge.SVA.BLL.Profession().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查IndustryCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasIndustryCode(string industryCode, int industryID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("IndustryCode='" + industryCode + "'");
            if (industryID > 0)
            {
                sbWhere.Append(" and IndustryID <> " + industryID);
            }

            int count = new Edge.SVA.BLL.Industry().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查StoreTypeCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasStoreTypeCode(string storeTypeCode, int storeTypeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("StoreTypeCode='" + storeTypeCode + "'");
            if (storeTypeID > 0)
            {
                sbWhere.Append(" and StoreTypeID <> " + storeTypeID);
            }

            int count = new Edge.SVA.BLL.StoreType().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查CurrencyCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasCurrencyCode(string currencyCode, int currencyID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CurrencyCode='" + currencyCode + "'");
            if (currencyID > 0)
            {
                sbWhere.Append(" and CurrencyID <> " + currencyID);
            }

            int count = new Edge.SVA.BLL.Currency().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查PasswordSettingCode是否存在，当industryID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasPasswordRuleCode(string passwordRuleCode, int passwordRuleID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("PasswordRuleCode='" + passwordRuleCode + "'");
            if (passwordRuleID > 0)
            {
                sbWhere.Append(" and PasswordRuleID <> " + passwordRuleID);
            }

            int count = new Edge.SVA.BLL.PasswordRule().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        ///  检查ReasonCode是否存在，当reasonID=0时是新增时用。
        /// </summary>
        /// <param name="industryCode"></param>
        /// <param name="industryID"></param>
        /// <returns></returns>
        public static bool isHasReasonCode(string reasonCode, int reasonID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("ReasonCode='" + reasonCode + "'");
            if (reasonID > 0)
            {
                sbWhere.Append(" and ReasonID <> " + reasonID);
            }

            int count = new Edge.SVA.BLL.Reason().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查cardTypeCode是否存在，当cardTypeID=0时是新增时用。
        /// </summary>
        /// <param name="cardTypeCode"></param>
        /// <param name="cardTypeID"></param>
        /// <returns></returns>
        public static bool isHasCardTypeCode(string cardTypeCode, int cardTypeID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CardTypeCode='" + cardTypeCode + "'");
            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardType().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查cardGradCode是否存在，当cardGradID=0时是新增时用。
        /// </summary>
        /// <param name="cardGradCode"></param>
        /// <param name="cardGradID"></param>
        /// <returns></returns>
        public static bool isHasCardGradeCode(string cardGradCode, int cardGradID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CardGradeCode='" + cardGradCode + "'");
            if (cardGradID > 0)
            {
                sbWhere.Append(" and CardGradeID <> " + cardGradID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }


        public static bool isHasCardGradeRank(string rank, int cardTypeID,int cardGradID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append(" CardGradeRank='" + rank + "'");
            sbWhere.Append(" and CardTypeID='" + cardTypeID + "'");
            if (cardGradID > 0)
            {
                sbWhere.Append(" and CardGradeID <> " + cardGradID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查customerCode是否存在，当customerID=0时是新增时用。
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public static bool isHasCustomerCode(string customerCode, int customerID)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CustomerCode='" + customerCode + "'");
            if (customerID > 0)
            {
                sbWhere.Append(" and CustomerID <> " + customerID);
            }

            int count = new Edge.SVA.BLL.Customer().GetCount(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 检查DistributeTemplateCode是否存在，当customerID=0时是新增时用。
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public static bool isHasDistributeCode(string distributeCode, int distributeID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("DistributionCode='" + distributeCode + "'");
            if (distributeID > 0)
            {
                sbWhere.Append(" and DistributionID <> " + distributeID);
            }

            int count = new Edge.SVA.BLL.DistributeTemplate().GetCount(sbWhere.ToString());
            return count > 0;
        }


        /// <summary>
        /// 检查organizationCode是否存在，当customerID=0时是新增时用。
        /// </summary>
        /// <param name="organizationCode"></param>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        public static bool isHasOrganizationCode(string organizationCode, int organizationID)
        {

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("OrganizationCode='" + organizationCode + "'");
            if (organizationID > 0)
            {
                sbWhere.Append(" and OrganizationID <> " + organizationID);
            }

            int count = new Edge.SVA.BLL.Organization().GetCount(sbWhere.ToString());
            return count > 0;
        }


        public static bool isEmptyCardNumMaskWithCardType(int cardTypeID)
        {
            if (new Edge.SVA.BLL.CardType().GetCountUnlimited(" [CardTypeID] =" + cardTypeID + "and ([CardNumMask] is null or [CardNumMask]='') ") > 0)
            {
                return true;
            }

            return false;

        }



        public static bool isHasCardCardeCardNumMask(string cardNumMask, string cardNumPattern, int cardGradeID,int cardTypeID)
        {
            if (string.IsNullOrEmpty(cardNumMask) && string.IsNullOrEmpty(cardNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CardNumMask]='" + cardNumMask.ToUpper() + "'" + " and [CardNumPattern]='" + cardNumPattern + "'");

            if (cardGradeID >0)
            {
                sbWhere.Append(" and CardGradeID <> " + cardGradeID);
            }

            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        public static bool isHasCardCardeCardNumMaskWithCardType(string cardNumMask, string cardNumPattern, int cardTypeID)
        {
            if (string.IsNullOrEmpty(cardNumMask) && string.IsNullOrEmpty(cardNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CardNumMask]='" + cardNumMask.ToUpper() + "'" + " and [CardNumPattern]='" + cardNumPattern + "'");

            if (cardTypeID > 0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardGrade().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }


        public static bool isHasCardTypeCardNumMask(string cardNumMask, string cardNumPattern, int cardTypeID)
        {
            if (string.IsNullOrEmpty(cardNumMask) && string.IsNullOrEmpty(cardNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CardNumMask]='" + cardNumMask.ToUpper() + "'" + " and [CardNumPattern]='" + cardNumPattern + "'");
            if (cardTypeID >0)
            {
                sbWhere.Append(" and CardTypeID <> " + cardTypeID);
            }

            int count = new Edge.SVA.BLL.CardType().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        public static bool isHasCouponTypeCouponNumMask(string couponNumMask, string couponNumPattern, int couponTypeID)
        {
            if (string.IsNullOrEmpty(couponNumMask) && string.IsNullOrEmpty(couponNumPattern))
            {
                return false;
            }

            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("[CouponNumMask]='" + couponNumMask.ToUpper() + "'" + " and [CouponNumPattern]='" + couponNumPattern + "'");
            if (couponTypeID > 0)
            {
                sbWhere.Append(" and CouponTypeID <> " + couponTypeID);
            }

            int count = new Edge.SVA.BLL.CouponType().GetCountUnlimited(sbWhere.ToString());
            return count > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numMask">将要写入的Number Mask</param>
        /// <param name="numPattern">将要写入的Number Pattern</param>
        /// <param name="type">0: CardTypre. 1: Cardgrade 2:CouponType</param>
        /// <param name="typeID">@TypeID内容根据@Type的设置. 新增时，可以填写0</param>
        /// <returns></returns>
        public static bool CheckNumberMask(string numMask, string numPattern, int type, int typeID)
        {
            string rtn = string.Empty;

            IDataParameter[] parameters = { new SqlParameter("@Type", SqlDbType.Int) , // 0: CardTypre. 1: Cardgrade 2:CouponType
                                            new SqlParameter("@TypeID", SqlDbType.Int),// @TypeID内容根据@Type的设置. 新增时，可以填写0
                                            new SqlParameter("@NumMask", SqlDbType.VarChar,512), // 将要写入的Number Mask
                                            new SqlParameter("@NumPattern", SqlDbType.VarChar,512),// 将要写入的Number Pattern
                                             new SqlParameter("@ReturnTypeID", SqlDbType.Int)// 正常返回0，有冲突返回冲突的ID，同样根据@Type区分
                                          };
            parameters[0].Value = type;
            parameters[1].Value = typeID;
            parameters[2].Value = numMask;
            parameters[3].Value = numPattern;
            parameters[4].Direction = ParameterDirection.Output;

            int count = 0;
            int result = DBUtility.DbHelperSQL.RunProcedure("CheckNumberMask", parameters, out count);
            if (parameters[4].Value != null)
            {
                rtn = parameters[4].Value.ToString();
            }
            return rtn=="0";

        }
        #endregion

        #region Delete Function Checking
        public static bool isCanDeleteBrand(int brandID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardType().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.Campaign().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.Store().GetCountUnlimited("BrandID=" + brandID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponTypeStoreCondition().GetCount("ConditionID=" + brandID + "and ConditionType=1") > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CardGradeStoreCondition().GetCount("ConditionID=" + brandID + "and ConditionType=1") > 0)
            {
                msg = "";
                return false;
            }
            return true;
        }

        public static bool isCanDeleteCardType(int cardTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardGrade().GetCountUnlimited("CardTypeID=" + cardTypeID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCardGrade(int cardGradeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Card().GetCount("CardGradeID=" + cardGradeID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCouponType(int couponTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Coupon().GetCount("CouponTypeID=" + couponTypeID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCampaign(int campaignID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("CampaignID=" + campaignID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteStore(int storeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CouponTypeStoreCondition().GetCount("ConditionID=" + storeID + "and ConditionType=3") > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CardGradeStoreCondition().GetCount("ConditionID=" + storeID + "and ConditionType=3") > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteIndustry(int industryID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Brand().GetCountUnlimited("IndustryID=" + industryID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteStoreNature(int storeNatureID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Store().GetCountUnlimited("StoreTypeID=" + storeNatureID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeleteCurrency(int currencyID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardType().GetCountUnlimited("CurrencyID=" + currencyID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("CurrencyID=" + currencyID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        public static bool isCanDeletePasswordRuleSetting(int passwordRuleSettingID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.CardGrade().GetCountUnlimited("PasswordRuleID=" + passwordRuleSettingID) > 0)
            {
                msg = "";
                return false;
            }
            else if (new Edge.SVA.BLL.CouponType().GetCountUnlimited("PasswordRuleID=" + passwordRuleSettingID) > 0)
            {
                msg = "";
                return false;
            }

            return true;
        }

        #endregion

        #region Master Edit Function Checking

        public static bool isCardTypeCreatedCard(int cardTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Card().GetCount("CardTypeID=" + cardTypeID) > 0)
            {
                msg = "";
                return true;
            }
            return false;
        }


        public static bool isCardTypeCreatedCardGrade(int cardTypeID, ref string msg)
        {
            msg = "";

            if (new Edge.SVA.BLL.CardGrade().GetCountUnlimited("CardTypeID=" + cardTypeID) > 0)
            {
                msg = "";
                return true;
            }

            return false;
        }


        public static bool isCardGradeCreatedCard(int cardGradeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Card().GetCount("CardGradeID=" + cardGradeID) > 0)
            {
                msg = "";
                return true;
            }

            return false;
        }

        public static bool isCouponTypeCreateCoupon(int couponTypeID, ref string msg)
        {
            msg = "";
            if (new Edge.SVA.BLL.Coupon().GetCount("CouponTypeID=" + couponTypeID) > 0)
            {
                msg = "";
                return true;
            }

            return false;
        }



        #endregion



        public static DataTable GetCouponViewDataTable(DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt.Copy());

            //Edge.Web.Tools.DataTool.AddCouponUID(ds, "CouponUID", "CouponNumber");
            Edge.Web.Tools.DataTool.AddCouponTypeName(ds, "CouponType", "CouponTypeID");
            Edge.Web.Tools.DataTool.AddCouponStatus(ds, "StatusName", "Status");
            //Tools.DataTool.AddBatchCode(ds, "BatchCode", "BatchCouponID");

            return ds.Tables[0];
        }


        public static int GetPreviousCouponStatus(int orgStatus,string couponNumber)
        {
            string strQuery = "select Top 1 OrgStatus from Coupon_movement where CouponNumber = '" + couponNumber + "' and OrgStatus <> NewStatus  and NewStatus="+orgStatus+" order by KeyID desc";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(strQuery);

            if (ds.Tables[0].Rows.Count > 0)
            {
                int status = Tools.ConvertTool.ToInt(ds.Tables[0].Rows[0]["OrgStatus"].ToString());
                return status;
            }
            else
            {
                return 0;
            }
        }


        public static string GetCouponTypeListByStoreIDBingding(int storeID, int storeConditionType)
        {
            string query = "SELECT distinct [CouponTypeID] FROM [CouponTypeStoreCondition_List] where storeid=" + storeID + " and StoreConditionType=" + storeConditionType;

            string strReturn = "";

            DataSet ds = Edge.DBUtility.DbHelperSQL.Query(query);
            if ((ds.Tables[0] != null) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    strReturn += row["CouponTypeID"] + ",";
                }
                strReturn=strReturn.TrimEnd(',');
                return strReturn;
            }
            else
            {
                return "-1";
            }
        }


        public static bool isHasCard(string cardNumber)
        {
            StringBuilder sbWhere = new StringBuilder();
            sbWhere.Append("CardNumber='" + cardNumber + "'");
            int count = new Edge.SVA.BLL.Card().GetCount(sbWhere.ToString());
            return count > 0;
        
        }
        /// <summary>
        /// 根据BrandCode获取BrandID
        /// </summary>
        /// <param name="brandCode"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static int GetBrandIDByBrandCode(string brandCode, Dictionary<string, int> cache)
        {
            if (string.IsNullOrEmpty(brandCode)) return 0;

            if (cache != null && cache.ContainsKey(brandCode)) return Tools.ConvertTool.ToInt(cache[brandCode].ToString());


            List<Edge.SVA.Model.Brand> list = new Edge.SVA.BLL.Brand().GetModelList(string.Format("brandCode='{0}'", brandCode));

            Edge.SVA.Model.Brand model = null;

            if (list.Count > 0)
            {
                model = list[0];
            }
            else
            {
                return 0;
            }

            int result = model == null ? 0 : model.BrandID;

            if (cache != null) cache.Add(brandCode, result);

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="storeCode"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static int GetStoreIDByBrandIDAndStoreCode(string brandID, string storeCode, Dictionary<string, int> cache)
        {
            string key = brandID + "_" + storeCode;

            if (cache != null && cache.ContainsKey(key)) return cache[key];

            Edge.SVA.Model.Store model = null;
            List<Edge.SVA.Model.Store> list = new Edge.SVA.BLL.Store().GetModelList(string.Format(" BrandID='{0}' and StoreCode='{1}'", brandID, storeCode));
            if (list.Count >= 1)
            {
                model = list[0];
            }
            else
            {
                return 0;
            }
            int result = model == null ? 0 : model.StoreID;

            if (cache != null) cache.Add(key, result);

            return result;
        }
    }
}
