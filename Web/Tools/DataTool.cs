﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Edge.Web.Tools
{
    public class DataTool
    {
        /// <summary>
        /// 在DataSet中Table增加发行商名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">发行商ID</param>
        internal static void AddCardIssuerName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardIssuerName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加发行商名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">发行商ID</param>
        internal static void AddCardIssuerName(DataSet ds, string name)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[name] = Edge.Web.Tools.DALTool.GetCardIssuerName();
            }
        }

        /// <summary>
        /// 在DataSet中Table增加品牌名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddBrandName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetBrandName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加品牌名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddBrandCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetBrandCode(id, cache);
            }
        }

        internal static void AddBrandCodeByCardType(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                Edge.SVA.Model.CardType cardtype = new SVA.BLL.CardType().GetModel(id);
                if (cardtype == null) continue;

                row[name] = Edge.Web.Tools.DALTool.GetBrandCode(cardtype.BrandID, cache);
            }
        }

      

        internal static void AddBrandCodeByCouponTypeID(DataSet ds, string name, string couponTypeID)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            Dictionary<int, string> couponTypeCache = new Dictionary<int, string>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[couponTypeID].ToString(), out id) ? id : int.MinValue;

                row[name] = Edge.Web.Tools.DALTool.GetBrandCodeByCouponTypeID(id, cache);
            }
        }


        internal static void AddBatchCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetBatchCode(id, cache);
            }
        }

        internal static void AddCardBatchCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardBatchCode(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加区域名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">区域ID</param>
        internal static void AddLocationName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetLocationName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加店铺编号
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">店铺ID</param>
        internal static void AddStoreCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetStoreCode(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加店铺名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">品牌ID</param>
        internal static void AddStoreName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetStoreName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加卡类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">卡类型ID</param>
        internal static void AddCardTypeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardTypeName(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加卡类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">卡类型ID</param>
        internal static void AddCardTypeCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardTypeCode(id, cache);
            }
        }

        /// <summary>
        /// 在DataSet中Table增加优惠劵类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">优惠劵ID</param>
        internal static void AddCouponTypeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeName(id, cache);
            }
        }

        internal static void AddCouponTypeNameByCode(DataSet ds, string name, string refKey)
        {
            string code = "";
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = row[refKey].ToString().Trim();
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeName(code, cache);
            }

        }


        internal static void AddCouponTypeNameByID(DataSet ds, string name, string refKey)
        {
            int code;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                code = Convert.ToInt32(row[refKey].ToString().Trim());
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeName(code, cache);
            }

        }
        internal static void AddCouponTypeCode(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCouponTypeCode(id, cache);
            }
        }
        /// <summary>
        /// 在DataSet中Table增加联系方式类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">联系方式类型ID</param>
        internal static void AddSNSTypeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetSNSTypeName(id, cache);
            }
        }

        internal static void AddUserName(DataSet ds, string newColumn, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[newColumn] = Edge.Web.Tools.DALTool.GetUserName(id, cache);
            }
        }

        internal static void AddTxnTypeName(DataSet ds, string newColumn, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[newColumn] = Edge.Web.Tools.DALTool.GetTnxTypeName(id);
            }
        }
        /// <summary>
        /// 在DataSet中Table增加兑换方式类型名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">兑换类型ID</param>
        internal static void AddExchangeType(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 1: row[name] = "金额兑换"; break;
                    case 2: row[name] = "积分兑换"; break;
                    case 3: row[name] = "金额+积分兑换"; break;
                    case 4: row[name] = "优惠券兑换"; break;
                    case 5: row[name] = "消费金额兑换"; break;
                }

            }
        }


        /// <summary>
        /// 在DataSet中Table增加状态
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">状态ID</param>
        internal static void AddStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 0: row[name] = "失效"; break;
                    case 1: row[name] = "有效"; break;
                }

            }
        }

        internal static void AddIsActivated(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 0: row[name] = Resources.MessageTips.Inactivated; break;
                    case 1: row[name] = Resources.MessageTips.Activated; break;
                }

            }
        }


        /// <summary>
        /// 性别
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddSex(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                switch (id)
                {
                    case 0: row[name] = Resources.MessageTips.Female; break;
                    case 1: row[name] = Resources.MessageTips.Male; break;
                }

            }
        }
        /// <summary>
        /// 加上CouponStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCouponStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Tools.DALTool.GetCouponTypeStatusName(id);
            }
        }


        /// <summary>
        /// 加上CouponStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCardStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Tools.DALTool.GetCardStatusName(id);
            }
        }

        /// <summary>
        /// 加上CouponPreviousStatus
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="name"></param>
        /// <param name="refKey"></param>
        internal static void AddCouponPreviousStatus(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Controllers.CouponController.GetPreviousCouponStatus(id, row["CouponNumber"].ToString());
            }
        }

        internal static void UpdateCouponStatus(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {
            Edge.SVA.Model.Coupon coupon = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }
                if (coupon == null) continue;
                row[name] = Tools.DALTool.GetCouponTypeStatusName(coupon.Status);
            }
        }

        internal static void UpdateCouponBatch(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {

            Edge.SVA.Model.Coupon coupon = null;
            Dictionary<int, string> batchCache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }
                if (coupon == null) continue;

                row[name] = DALTool.GetBatchCode(coupon.BatchCouponID.GetValueOrDefault(), batchCache);
            }
        }
        internal static void AddCouponApproveStatusName(DataSet ds, string newColunm, string approveStatus)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColunm, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                row[newColunm] = DALTool.GetApproveStatusString(row[approveStatus].ToString());

            }
        }
        internal static void AddOrderPickingApproveStatusName(DataSet ds, string newColunm, string approveStatus)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColunm, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                row[newColunm] = DALTool.GetOrderPickingApproveStatusString(row[approveStatus].ToString());

            }
        }
        internal static void UpdateCouponExpiryDate(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {
            Edge.SVA.Model.Coupon coupon = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }

                if (coupon != null) row[name] = coupon.CouponExpiryDate;
            }
        }

        internal static void UpdateCouponDenomination(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {

            Edge.SVA.Model.Coupon coupon = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }

                if (coupon != null)
                {
                    row[name] = coupon.CouponAmount.GetValueOrDefault();
                }

            }
        }

        internal static void UpdateCouponNumber(DataSet ds, string name, string couponUID, Dictionary<string, Edge.SVA.Model.Coupon> cache)
        {

            Edge.SVA.Model.Coupon coupon = null;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string uid = row[couponUID].ToString();
                if (cache != null && cache.ContainsKey(uid))
                {
                    coupon = cache[uid];
                }
                else
                {
                    coupon = new Edge.SVA.BLL.Coupon().GetModelByUID(uid);
                    if (cache != null) cache.Add(uid, coupon);
                }

                if (coupon != null) row[name] = coupon.CouponNumber;
            }
        }

        internal static void UpdateCouponAmout(DataSet ds, string couponAmount, string couponTypeID)
        {
            Dictionary<int, Edge.SVA.Model.CouponType> cache = new Dictionary<int, SVA.Model.CouponType>();
            Edge.SVA.Model.CouponType couponType = null;
            int id = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[couponTypeID].ToString(), out id) ? id : 0;
                couponType = DALTool.GetCouponType(id, cache);
                if (couponType != null) row[couponAmount] = couponType.CouponTypeAmount;
            }
        }

        internal static void AddPasswordFormat(DataSet ds, string newFormat, string format)
        {

            ds.Tables[0].Columns.Add(new DataColumn(newFormat, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int i = -1;
                i = int.TryParse(row[format].ToString(), out i) ? i : -1;
                row[newFormat] = DALTool.GetPasswordFormatName(i);

            }
        }
        internal static void AddID(DataSet ds, string newColumn, int size, int page)
        {
            long id = size * page;
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(long)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = ++id;
            }
        }

        internal static void AddCouponUID(DataSet ds, string newColumn, string couponNumberColumn)
        {

            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = DALTool.GetUID(row[couponNumberColumn].ToString(), cache);
            }
        }

        internal static void AddCardUID(DataSet ds, string newColumn, string cardNumberColumn)
        {

            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(string)));
            Dictionary<string, string> cache = new Dictionary<string, string>();
            Edge.SVA.BLL.CardUIDMap map = new Edge.SVA.BLL.CardUIDMap();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = DALTool.GetCardUID(row[cardNumberColumn].ToString(), cache);
            }
        }

        internal static void AddColumn(DataSet ds, string newColumn, object value)
        {
            Type type = value != null ? value.GetType() : typeof(object);
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, type));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = value;
            }
        }

        internal static void AddColumn(DataTable dt, string newColumn, object value)
        {
            Type type = value != null ? value.GetType() : typeof(object);
            dt.Columns.Add(new DataColumn(newColumn, type));

            foreach (DataRow row in dt.Rows)
            {
                row[newColumn] = value;
            }
        }

        internal static void AddColumnWithValue(DataSet ds, string newColumn, string value)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColumn, typeof(object)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row[newColumn] = row[value];
            }
        }

        internal static void Sort(DataSet ds, string newColumn, string sortColumn, Type type)
        {
            ds.Tables[0].Columns.Add(newColumn, type);

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i][newColumn] = ConvertTool.ChangeType(type, ds.Tables[0].Rows[i][sortColumn].ToString());
            }
        }

        internal static void ClearEndRow(DataTable dt)
        {
            if (dt == null) return;

            for (int i = dt.Rows.Count - 1; i > 0; i--)
            {
                bool isNull = true;
                for (int col = 0; col < dt.Columns.Count; col++)
                {
                    if (dt.Rows[i][col] != null && dt.Rows[i][col].ToString() != "")
                    {
                        isNull = false;
                        break;
                    }
                }
                if (isNull) dt.Rows.Remove(dt.Rows[i]);
                else break;
            }
        }

        internal static void AddCouponAmount(DataSet ds, string name, string refKey, string couponTypeKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int type = Edge.Web.Tools.ConvertTool.ToInt(row[couponTypeKey].ToString());
                Edge.SVA.Model.CouponType model = new Edge.SVA.BLL.CouponType().GetModel(type);
                if (model != null)
                {
                    if (model.CoupontypeFixedAmount.GetValueOrDefault() == 1)//Fixed
                    {
                        id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                        row[name] = model.CouponTypeAmount.ToString("N02");
                    }
                    else
                    {
                        id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                        row[name] = "--";//Modify by Nathan
                        row[name] = Resources.MessageTips.IsOpenAmount;

                    }
                }
                else
                {
                    id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                    row[name] = "??";
                }
            }
        }

        #region for card operation
        internal static void AddCardApproveStatusName(DataSet ds, string newColunm, string approveStatus)
        {
            ds.Tables[0].Columns.Add(new DataColumn(newColunm, typeof(string)));

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                row[newColunm] = DALTool.GetApproveStatusString(row[approveStatus].ToString());

            }
        }
        /// <summary>
        /// 在DataSet中Table增加卡级别名称
        /// </summary>
        /// <param name="ds">数据源</param>
        /// <param name="name">列名称</param>
        /// <param name="refKey">卡级别ID</param>
        internal static void AddCardGradeName(DataSet ds, string name, string refKey)
        {
            int id = 0;
            ds.Tables[0].Columns.Add(new DataColumn(name, typeof(string)));
            Dictionary<int, string> cache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                id = int.TryParse(row[refKey].ToString(), out id) ? id : int.MinValue;
                row[name] = Edge.Web.Tools.DALTool.GetCardGradeName(id, cache);
            }
        }
        #endregion

        internal static IEnumerable<DataRow> GetPaggingTable(int index, int pageSize, DataTable source)
        {
            if (source == null) return null;
            var result = (from row in source.AsEnumerable()
                          select row).Skip(index * pageSize).Take(10);

            return result;
        }

       
    }
}
