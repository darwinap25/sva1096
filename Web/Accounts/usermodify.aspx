﻿<%@ Page Language="c#" CodeBehind="usermodify.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.usermodify" %>

<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>usermodify</title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <b>您当前的位置：<%=this.PageName %></b></div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                用户名：
            </td>
            <td width="85%">
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                真实姓名：
            </td>
            <td>
                <asp:TextBox ID="txtTrueName" runat="server" Width="200px" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                用户性别：
            </td>
            <td>
                <asp:RadioButtonList ID="rdblSex" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Text="男" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="0" Text="女"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系电话：
            </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" Width="200px" CssClass="input digits"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                电子邮箱：
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="200px" CssClass="input email"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                权限列表：
            </td>
            <td>
                <asp:Label ID="RoleList" Visible="False" runat="server"></asp:Label>
            </td>
        </tr>
        <%-- <tr>
	    <td  align="right">界面风格：
	    </td>
	    <td><asp:DropDownList id="dropStyle" runat="server" Width="200px">
			    <asp:ListItem Value="1">默认蓝</asp:ListItem>
			    <asp:ListItem Value="2">橄榄绿</asp:ListItem>
			    <asp:ListItem Value="3">深红</asp:ListItem>
			    <asp:ListItem Value="4">深绿</asp:ListItem>
		    </asp:DropDownList></td>
    </tr>         --%>
    </table>
    <div style="margin-top: 10px; text-align: center;">
        <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <%--    <div style="margin-top: 10px; text-align: center;">
        <asp:Label ID="RoleList" Visible="False" runat="server"></asp:Label>
    </div>--%>
    <div style="margin-top: 10px; text-align: center;">
        <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
        </asp:Button>
        <asp:Button ID="btnReturn" value="返 回" runat="server" class="submit" OnClick="btnReturn_Click"
            Text="返回" /></div>
    <%--	<uc1:CheckRight id="CheckRight1" runat="server"></uc1:CheckRight>--%>
    </form>
</body>
</html>
