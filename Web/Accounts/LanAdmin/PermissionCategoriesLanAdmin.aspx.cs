﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.LanAdmin
{
    public partial class PermissionCategoriesLanAdmin : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.insertTbl.Visible = false;
                this.modifyTbl.Visible = false;
                //BindCategoryList();
                BindCategoryTree();
                BindLanListByCategory();
                LanTools.BindLanList(this.ddlLanList);
            }
        }

        //private void BindCategoryList()
        //{
        //    DataSet CategoryList = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString()); //todo: 修改成多语言。
        //    this.ddlCategoryList.DataSource = CategoryList.Tables["Categories"];
        //    this.ddlCategoryList.DataValueField = "CategoryID";
        //    this.ddlCategoryList.DataTextField = "Description";
        //    this.ddlCategoryList.DataBind();
        //}

        #region 绑定下拉树
        private void BindCategoryTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());


            this.ddlCategoryList.Items.Clear();
            //加载树
            this.ddlCategoryList.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ddlCategoryList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, ds.Tables[0], blank);

            }
            this.ddlCategoryList.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.ddlCategoryList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }
        #endregion

        private void BindLanListByCategory()
        {
            Edge.Security.Manager.Lan_Accounts_PermissionCategories lanBll = new Edge.Security.Manager.Lan_Accounts_PermissionCategories();
            int CategoryID = int.Parse(this.ddlCategoryList.SelectedValue);
            DataSet LanList = lanBll.GetList(" CategoryID=" + CategoryID);
            LanTools.AddLanDesc(LanList, "LanDesc", "Lan");
            this.rptList.DataSource = LanList;
            this.rptList.DataBind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int CategoryID = Convert.ToInt32(((Label)e.Item.FindControl("lb_id")).Text);
            string strCategoryDesc = ((Label)e.Item.FindControl("lblDescription")).Text;
            string strLan = ((Label)e.Item.FindControl("lblLan")).Text;
            string strLanDesc = ((Label)e.Item.FindControl("lblLanDesc")).Text;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    this.modifyTbl.Visible = true;
                    this.lbCategoryId.Text = CategoryID.ToString();
                    this.lblPermLan.Text = strLan;
                    this.lblPermLanDesc.Text = strLanDesc;
                    this.txtModifyLan.Text = strCategoryDesc;
                    break;

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int CategoryID = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                string strCategoryDesc = ((Label)rptList.Items[i].FindControl("lblDescription")).Text;
                string strLan = ((Label)rptList.Items[i].FindControl("lblLan")).Text;
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    Edge.Security.Manager.Lan_Accounts_PermissionCategories p = new Edge.Security.Manager.Lan_Accounts_PermissionCategories();
                    Edge.Security.Model.Lan_Accounts_PermissionCategories item = new Edge.Security.Model.Lan_Accounts_PermissionCategories();
                    item.Description = strCategoryDesc;
                    item.Lan = strLan;
                    item.CategoryID = CategoryID;
                    p.Delete(item);

                    Logger.Instance.WriteOperationLog(this.PageName, "Delete Permission Categories  Language: " + item.Description + " and Language: " + item.Lan);
                    //保存日志
                    // SaveLogs("" + model.Title); 
                }
            }

            BindLanListByCategory();

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "PermissionCategoriesLanAdmin.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
        }

        protected void btnModifyLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtModifyLan.Text.Trim()))
            {
                this.lbltip3.Text = Resources.MessageTips.NameNotEmpty;
                return;
            }

            //if (new Edge.Security.Manager.Lan_Accounts_PermissionCategories().GetCount(string.Format("Description='{0}' and Lan='{1}'", this.txtNewLan.Text.Trim(), this.ddlLanList.SelectedItem.Value.Trim())) > 0)
            //{
            //    this.lbltip3.Text = Resources.MessageTips.Exists;
            //    return;
            //}

            Edge.Security.Manager.Lan_Accounts_PermissionCategories p = new Edge.Security.Manager.Lan_Accounts_PermissionCategories();
            Edge.Security.Model.Lan_Accounts_PermissionCategories item = new Edge.Security.Model.Lan_Accounts_PermissionCategories();
            item.Description = this.txtModifyLan.Text.Trim();
            item.Lan = this.lblPermLan.Text.Trim();
            item.CategoryID = int.Parse(this.lbCategoryId.Text.Trim());
            p.Update(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Update Permission Categories  Language: " + item.Description + " and Language: " + item.Lan);
            this.modifyTbl.Visible = false;
            BindLanListByCategory();
        }

        protected void btnModifyCancel_Click(object sender, EventArgs e)
        {
            this.modifyTbl.Visible = false;
            this.lbCategoryId.Text = "";
            this.txtModifyLan.Text = "";

        }

        protected void btnInsertLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtNewLan.Text.Trim()))
            {
                this.lbltip1.Text = Resources.MessageTips.NameNotEmpty;
                return;
            }


            if (new Edge.Security.Manager.Lan_Accounts_PermissionCategories().GetCount(string.Format(" Lan='{0}' and CategoryID={1}", this.ddlLanList.SelectedItem.Value.Trim(), Tools.ConvertTool.ToInt(this.ddlCategoryList.SelectedValue))) > 0)
            {
                this.lbltip1.Text = Resources.MessageTips.Exists;
                return;
            }


            Edge.Security.Manager.Lan_Accounts_PermissionCategories bllLan = new Edge.Security.Manager.Lan_Accounts_PermissionCategories();
            Edge.Security.Model.Lan_Accounts_PermissionCategories item = new Edge.Security.Model.Lan_Accounts_PermissionCategories();
            item.Description = this.txtNewLan.Text.Trim();
            item.Lan = this.ddlLanList.SelectedItem.Value;
            item.CategoryID = Tools.ConvertTool.ToInt(this.ddlCategoryList.SelectedValue);
            bllLan.Add(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Add Permission Categories  Language: " + item.Description + " and Language: " + item.Lan);
            this.insertTbl.Visible = false;

            BindLanListByCategory();

        }

        protected void btnInsertCancel_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = false;
            this.txtModifyLan.Text = "";
        }

        protected void ddlCategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLanListByCategory();
        }

        protected void btnDisplayInsertLan_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = true;
            this.lbltip1.Text = "";
            this.lbltip3.Text = "";
            this.txtNewLan.Text = "";
            this.txtModifyLan.Text = "";
        }
    }
}
