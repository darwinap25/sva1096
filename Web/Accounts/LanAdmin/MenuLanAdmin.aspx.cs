﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.LanAdmin
{
    public partial class MenuLanAdmin : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.insertTbl.Visible = false;
                this.modifyTbl.Visible = false;
                LanTools.BindLanList(this.ddlLanList);
                BindTree();
                //BindTreeList();
                BindLanListByNode();
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int NodeID = Convert.ToInt32(((Label)e.Item.FindControl("lb_id")).Text);
            string strNodeDesc = ((Label)e.Item.FindControl("lblDescription")).Text;
            string strLanDesc = ((Label)e.Item.FindControl("lblLanDesc")).Text;
            string strLan = ((Label)e.Item.FindControl("lblLan")).Text;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    this.modifyTbl.Visible = true;
                    this.lbNodeId.Text = NodeID.ToString();
                    this.lblPermLan.Text = strLan;
                    this.lblPermLanDesc.Text = strLanDesc;
                    this.txtModifyLan.Text = strNodeDesc;
                    break;

            }
        }

        protected void btnDisplayInsertLan_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = true;
            this.lbltip1.Text = "";
            this.lbltip3.Text = "";
            this.txtNewLan.Text = "";
            this.txtModifyLan.Text = "";
        }

        protected void btnModifyLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtModifyLan.Text.Trim()))
            {
                this.lbltip3.Text = Resources.MessageTips.NameNotEmpty;
                return;
            }

            //if (new Edge.Security.Manager.Lan_S_Tree().GetCount(string.Format("Text='{0}' and Lan='{1}'", this.txtNewLan.Text.Trim(), this.ddlLanList.SelectedItem.Value.Trim())) > 0)
            //{
            //    this.lbltip3.Text = Resources.MessageTips.Exists;
            //    return;
            //}


            Edge.Security.Manager.Lan_S_Tree p = new Edge.Security.Manager.Lan_S_Tree();
            Edge.Security.Model.Lan_S_Tree item = new Edge.Security.Model.Lan_S_Tree();
            item.Text = this.txtModifyLan.Text.Trim();
            item.Lan = this.lblPermLan.Text.Trim();
            item.NodeID = int.Parse(this.lbNodeId.Text.Trim());
            p.Update(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Update Menu Language: " +item.Text +" and Language: "+ item.Lan);
            this.modifyTbl.Visible = false;
            BindLanListByNode();

        }

        protected void btnModifyCancel_Click(object sender, EventArgs e)
        {
            this.modifyTbl.Visible = false;
            this.lbNodeId.Text = "";
            this.txtModifyLan.Text = "";
        }

        protected void btnInsertLan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtNewLan.Text.Trim()))
            {
                this.lbltip1.Text = Resources.MessageTips.NameNotEmpty;
                return;
            }

            if (new Edge.Security.Manager.Lan_S_Tree().GetCount(string.Format(" Lan='{0}' and NodeID={1}", this.ddlLanList.SelectedItem.Value.Trim(), Tools.ConvertTool.ToInt(this.ddlTreeList.SelectedValue))) > 0)
            {
                this.lbltip1.Text = Resources.MessageTips.Exists;
                return;
            }


            Edge.Security.Manager.Lan_S_Tree bllLan = new Edge.Security.Manager.Lan_S_Tree();
            Edge.Security.Model.Lan_S_Tree item = new Edge.Security.Model.Lan_S_Tree();
            item.Text = this.txtNewLan.Text.Trim();
            item.Lan = this.ddlLanList.SelectedItem.Value;
            item.NodeID = Tools.ConvertTool.ToInt(this.ddlTreeList.SelectedValue);
            bllLan.Add(item);
            Logger.Instance.WriteOperationLog(this.PageName, "Add Menu Language: " + item.Text + " and Language: " + item.Lan);
            this.insertTbl.Visible = false;

            BindLanListByNode();

        }

        protected void btnInsertCancel_Click(object sender, EventArgs e)
        {
            this.insertTbl.Visible = false;
            this.txtModifyLan.Text = "";
        }

        protected void ddlTreeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLanListByNode();
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int NodeID = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                string strNodeDesc = ((Label)rptList.Items[i].FindControl("lblDescription")).Text;
                string strLanDesc = ((Label)rptList.Items[i].FindControl("lblLanDesc")).Text;
                string strLan = ((Label)rptList.Items[i].FindControl("lblLan")).Text;
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    Edge.Security.Manager.Lan_S_Tree p = new Edge.Security.Manager.Lan_S_Tree();
                    Edge.Security.Model.Lan_S_Tree item = new Edge.Security.Model.Lan_S_Tree();
                    item.Text = strNodeDesc;
                    item.Lan = strLan;
                    item.NodeID = NodeID;
                    p.Delete(item);
                    Logger.Instance.WriteOperationLog(this.PageName, "Delete Menu Language: " + item.Text + " and Language: " + item.Lan);
                    //保存日志
                    // SaveLogs("" + model.Title); 
                }
            }

            BindLanListByNode();

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "MenuLanAdmin.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
        }

        //private void BindTreeList()
        //{
        //    Edge.Security.Manager.SysManage sysBll = new Edge.Security.Manager.SysManage();
        //    DataSet TreeList = sysBll.GetTreeListByLan(string.Empty, Session["SiteLanguage"].ToString());
        //    this.ddlTreeList.DataSource = TreeList;
        //    this.ddlTreeList.DataValueField = "NodeID";
        //    this.ddlTreeList.DataTextField = "Text";
        //    this.ddlTreeList.DataBind();
        //}


        #region 绑定下拉树
        private void BindTree()
        {
            Edge.Security.Manager.SysManage sysBll = new Edge.Security.Manager.SysManage();
            DataSet ds;
            ds = sysBll.GetTreeListByLan(string.Empty, Session["SiteLanguage"].ToString());


            this.ddlTreeList.Items.Clear();
            //加载树
            this.ddlTreeList.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["NodeID"].ToString();
                string text = r["Text"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ddlTreeList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, ds.Tables[0], blank);

            }
            this.ddlTreeList.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["NodeID"].ToString();
                string text = r["Text"].ToString();
                text = blank + "『" + text + "』";
                this.ddlTreeList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }
        #endregion

        private void BindLanListByNode()
        {
            Edge.Security.Manager.Lan_S_Tree lanBll = new Edge.Security.Manager.Lan_S_Tree();
            int NodeID = int.Parse(this.ddlTreeList.SelectedValue);
            DataSet LanList = lanBll.GetList(" NodeID=" + NodeID);
            LanTools.AddLanDesc(LanList, "LanDesc", "Lan");
            this.rptList.DataSource = LanList;
            this.rptList.DataBind();
        }

    }
}
