﻿<%@ Page Language="c#" CodeBehind="RoleAdmin.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.Admin.RoleAdmin" %>

<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Index</title>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">--%>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <asp:Label ID="Label2" runat="server"><%=this.PageName %></asp:Label>
            </th>
        </tr>
        <tr>
            <td>
                <strong>点击角色进行权限编辑：</strong>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td width="20">
                        </td>
                        <td>
                            <asp:DataList ID="RoleList" runat="server" RepeatColumns="3" CellPadding="1" Width="100%">
                                <ItemTemplate>
                                    <a href='EditRole.aspx?RoleID=<%# DataBinder.Eval(Container.DataItem, "RoleID") %>'>
                                        <%# DataBinder.Eval(Container.DataItem, "Description") %></a><br />
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server">新增角色名：</asp:Label><asp:TextBox ID="TextBox1"
                    runat="server" MaxLength="30" Columns="20" CssClass="input required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="BtnAdd" runat="server" Text="新增" CssClass="submit" OnClick="BtnAdd_Click" />
            </td>
        </tr>
    </table>
    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="4"></uc1:CheckRight>
    </form>
</body>
</html>
