﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PermissionCategoryAdmin.aspx.cs"
    Inherits="Edge.Web.Accounts.Admin.PermissionCategoryAdmin" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript">

        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
        });


        $(window).resize(function() {
            myWindowResize();
        })

        function myWindowResize() {
            var divheight;
            divheight = $(window).height() - $("#btns").height() - 10;

            $("#mainLeft").height(divheight);
            $("#mainDiv").height(divheight);
        }
    </script>

</head>
<body style="padding: 10px;" onload="myWindowResize()">
    <form id="form1" method="post" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table id="Add" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0"
        class="tree_bgcolor">
        <tr>
            <td align="middle" valign="top" style="background: #FFF;">
                <div id="btns">
                    <asp:Button ID="BtnAdd" runat="server" CssClass="submit cancel" Text="添加" OnClick="BtnAdd_Click" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="submit cancel" Text="删除" OnClick="btnDelete_Click" />
                </div>
                <div id="mainLeft" style="text-align: left; width: 210px; overflow: scroll; font-size: 12px;">
                    <asp:TreeView ID="TreeView1" runat="server" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged">
                    </asp:TreeView>
                </div>
            </td>
            <td style="width: 100%;" valign="top">
                <div id="mainDiv" style="text-align: left; width: 100%; font-size: 12px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
                        <tr>
                            <td colspan="2">
                                信息添加，请详细填写下列信息，带有 *的必须填写。
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="right">
                                *名称：
                            </td>
                            <td width="75%">
                                <asp:TextBox ID="txtName" runat="server" Width="200px" MaxLength="20" CssClass="input required"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                *父类：
                            </td>
                            <td>
                                <asp:DropDownList ID="listTarget" runat="server" Width="200px">
                                    <asp:ListItem Value="0" Selected="True">根目录</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                *排序号：
                            </td>
                            <td>
                                <asp:TextBox ID="txtId" runat="server" MaxLength="10" CssClass="input digit required"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSave" runat="server" Text="提交" CssClass="submit" OnClick="btnSave_Click">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <uc2:checkright ID="CheckRight1" runat="server"></uc2:checkright>
    </form>
</body>
</html>
