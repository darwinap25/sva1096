﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.Admin
{
	/// <summary>
	/// Index 的摘要说明。
	/// </summary>
    public partial class RoleAdmin : Edge.Web.UI.ManagePage
	{

		private DataSet roles;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// 在此处放置用户代码以初始化页面
//			AccountsPrincipal currentPrincipal=new AccountsPrincipal( Context.User.Identity.Name );

//			AccountsPrincipal currentPrincipal = (AccountsPrincipal)Context.User;
//			if (!currentPrincipal.HasPermission((int)AccountsPermissions.CreateRoles))
//			{
//				NewRoleButton.Visible = false;
//				NewRoleDescription.Visible = false;
//			}
//			else 
//			{
//				NewRoleButton.Visible = true;
//				NewRoleDescription.Visible = true;
//			}
			dataBind();
		}
		private void dataBind()
		{
            roles = AccountsTool.GetRoleList(Session["SiteLanguage"].ToString());//todo: 修改成多语言。
			RoleList.DataSource = roles.Tables["Roles"];
			RoleList.DataBind();
		}

        protected void BtnAdd_Click(object sender, EventArgs e)
        {

            
            //if (TextBox1.Text.Trim() == "")
            //{
            //    JscriptPrintAndFocus(Resources.MessageTips.SpecifyRoleName, "", Resources.MessageTips.WARNING_TITLE, TextBox1.ClientID);
            //}
            Role role = new Role();
            role.Description = TextBox1.Text;
            try
            {
                role.Create();

                Logger.Instance.WriteOperationLog(this.PageName, "Add Role " + role.Description);
            }
            catch { }
            TextBox1.Text = "";
            dataBind();
        }

	}
}
