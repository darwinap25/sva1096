﻿<%@ Register TagPrefix="uc1" TagName="CheckRight" Src="~/Controls/CheckRight.ascx" %>

<%@ Page Language="c#" CodeBehind="UserAdmin.aspx.cs" AutoEventWireup="True" Inherits="Edge.Web.Accounts.Admin.UserAdmin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>

</head>
<body style="padding: 10px;">
    <form id="Form1" method="post" runat="server">

    <script type="text/javascript">
      $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« ",
            next_text: " »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }
        
        $(function() {
            $(".msgtable tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtable tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>

    <div class="navigation">
        <b>您当前的位置：<%=this.PageName %></b></div>
    <div class="spClear">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" align="right">
                用户名关键字：
            </td>
            <td width="30%" align="left">
                <asp:TextBox ID="txtKeywords" runat="server" Width="249px" BorderStyle="Groove"></asp:TextBox>
            </td>
            <td width="20%" align="center">
                <asp:Button ID="BtnSearch" runat="server" Text="搜索" class="submit" OnClick="BtnSearch_Click" />
            </td>
        </tr>
    </table>
    <div class="spClear">
    </div>
    <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                <tr>
                    <th width="8%">
                        <input type="checkbox" onclick="checkAll(this);" />选择
                    </th>
                  <%--  <th width="6%">
                        序号
                    </th>--%>
                    <th width="20%">
                        用户名
                    </th>
                    <th width="20%">
                        真实姓名
                    </th>
                    <th width="10%">
                        性别
                    </th>
                    <th width="15%">
                        联系电话
                    </th>
                    <th width="20%">
                        电子邮件
                    </th>
                    <%--                    <th width="10%">
                        所属公司
                    </th>--%>
                    <th width="10%">
                        操作
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                </td>
                <%--<td align="center">
                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                </td>--%>
                <td align="center">
                    <a href='RoleAssignment.aspx?UserID=<%#Eval("UserID")%>&PageIndex=<%#page %>'>
                        <%#Eval("UserName")%></a>
                     <asp:HiddenField runat="server" ID="hf_id" Value='<%#Eval("UserID")%>' > </asp:HiddenField>
                </td>
                <td align="center">
                    <asp:Label ID="lblTrueName" runat="server" Text='<%#Eval("TrueName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblSex" runat="server" Text='<%#Eval("SexName")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblPhone" runat="server" Text='<%#Eval("Phone")%>'></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email")%>'></asp:Label>
                </td>
                <%--                <td align="center">
                    <asp:Label ID="lblDepartmentID" runat="server" Text='<%#Eval("DepartmentID")%>'></asp:Label>
                </td>--%>
                <td align="center">
                    <span class="btn_bg"><a href="../userupdate.aspx?userid=<%#Eval("UserID") %>">修改</a></span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="spClear">
    </div>
    <div style="line-height: 30px; height: 30px;">
        <div id="Pagination" class="right flickr">
        </div>
        <div class="left">
            <span class="btn_bg">
                <asp:LinkButton ID="lbtnDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                    OnClick="lbtnDel_Click">删 除</asp:LinkButton>
               <asp:LinkButton ID="BtnAdd" runat="server" class="submit" OnClick="BtnAdd_Click" >添加新用户</asp:LinkButton>
            </span>
        </div>
    </div>
    <uc1:CheckRight ID="CheckRight1" runat="server" PermissionID="3"></uc1:CheckRight>
    </form>
</body>
</html>
