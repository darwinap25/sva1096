﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.Admin
{
	/// <summary>
	/// PermissionAdmin 的摘要说明。
	/// </summary>
    public partial class PermissionAdmin : Edge.Web.UI.ManagePage
	{

        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

		protected void Page_Load(object sender, System.EventArgs e)
		{
            this.pagesize = webset.ContentPageNum;

			if(!Page.IsPostBack)
			{
				this.TabEdit.Visible=false;
				//SetStyle();

				Button btn = (Button)Page.FindControl("BtnDelCategory");
				btn.Attributes.Add("onclick", string.Format("return confirm('{0}');",Resources.MessageTips.ConfirmDeleteRecord));

				//CategoriesDatabind();

                BindTree();

                //if(this.ClassList.SelectedItem!=null)
                //{
                //    PermissionsDatabind();
                //}

			}
		}
        //private void CategoriesDatabind()
        //{
        //    DataSet CategoriesList = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());//todo: 修改成多语言。
        //    this.ClassList.DataSource = CategoriesList;
        //    this.ClassList.DataTextField = "Description";
        //    this.ClassList.DataValueField = "CategoryID";
        //    this.ClassList.DataBind();
        //}

		private void PermissionsDatabind()
		{
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }


            int CategoryId = int.Parse(this.ClassList.SelectedValue);

            //获得总条数
            this.pcount = AccountsTool.GetPermissionsInCategoryCount(CategoryId, Session["SiteLanguage"].ToString());
            if (this.pcount > 0)
            {
                this.btnDelete.Enabled = true;
            }
            else
            {
                this.btnDelete.Enabled = false;
            }

            DataSet PermissionsList = AccountsTool.GetPermissionsByCategory(CategoryId, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            this.rptList.DataSource = PermissionsList;
            this.rptList.DataBind();
		}



		protected void ClassList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			PermissionsDatabind();		
		}

        #region 绑定下拉树
        private void BindTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());


            this.ClassList.Items.Clear();
            //加载树
            this.ClassList.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.ClassList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, ds.Tables[0], blank);

            }
            this.ClassList.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.ClassList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }
        #endregion

        protected void BtnAddCategory_Click(object sender, EventArgs e)
        {
            //string Category = this.CategoriesName.Text.Trim();
            //if (Category != "")
            //{
            //    PermissionCategories c = new PermissionCategories();
            //    c.Create(Category);
            //    CategoriesDatabind();
            //    if (this.ClassList.SelectedItem != null)
            //    {
            //        PermissionsDatabind();
            //    }
            //    this.CategoriesName.Text = "";
            //}
            //else
            //{
            //    this.lbltip1.Text = Resources.MessageTips.NameNotEmpty;
            //    PermissionsDatabind();
            //}
        }

        protected void BtnDelCategory_Click(object sender, EventArgs e)
        {
            //int CategoryId = int.Parse(this.ClassList.SelectedValue);
            //PermissionCategories c = new PermissionCategories();
            //c.Delete(CategoryId);
            //CategoriesDatabind();
            //if (this.ClassList.SelectedItem != null)
            //{
            //    PermissionsDatabind();
            //}
            //PermissionsDatabind();
        }

        protected void BtnAddPermissions_Click(object sender, EventArgs e)
        {
            string Permissions = this.PermissionsName.Text.Trim();
            if (Permissions != "")
            {
                int CategoryId = int.Parse(this.ClassList.SelectedValue);
                Permissions p = new Permissions();
                p.Create(CategoryId, Permissions);

                Logger.Instance.WriteOperationLog(this.PageName, "Add Permission " + Permissions);

                if (this.ClassList.SelectedItem != null)
                {
                    PermissionsDatabind();
                }
                this.PermissionsName.Text = "";
            }
            else
            {
                this.lbltip2.Text = Resources.MessageTips.NameNotEmpty;
                PermissionsDatabind();
            }
        }

        protected void btnupSave_Click(object sender, EventArgs e)
        {
            if (this.txtNewName.Text.Trim() != "")
            {
                Permissions p = new Permissions();
                p.Update(int.Parse(this.lblPermId.Text), this.txtNewName.Text.Trim());

                Logger.Instance.WriteOperationLog(this.PageName, "Update Permission " + txtNewName.Text.Trim());

                this.TabEdit.Visible = false;
                PermissionsDatabind();
            }
            else
            {
                this.lbltip3.Text = Resources.MessageTips.NameNotEmpty;
                PermissionsDatabind();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.TabEdit.Visible = false;
            PermissionsDatabind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int PermissionsID = Convert.ToInt32(((Label)e.Item.FindControl("lb_id")).Text);
            //string strPermissionsDesc = ((Label)e.Item.FindControl("lblDescription")).Text;
            Edge.Security.Manager.Permissions pBll= new Permissions();
            string strPermissionsDesc = pBll.GetPermissionName(PermissionsID);//获取默认的名字
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    this.TabEdit.Visible = true;
                    this.lblPermId.Text = PermissionsID.ToString();
                    this.txtNewName.Text = strPermissionsDesc;
                    PermissionsDatabind();
                    break;

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int PermissionsID = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                string strPermissionsDesc = ((Label)rptList.Items[i].FindControl("lblDescription")).Text;
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    if (strPermissionsDesc != "帐户管理")
                    {
                        Permissions p = new Permissions();

                        //保存日志
                        // SaveLogs("" + model.Title); 
                        Logger.Instance.WriteOperationLog(this.PageName, "Delete Permission " + strPermissionsDesc);
                        p.Delete(PermissionsID);
                    }
                    else
                    {
                        JscriptPrint(string.Format(Resources.MessageTips.CanNotDeleteRole, strPermissionsDesc), "", Resources.MessageTips.SUCESS_TITLE);

                    }

                }
            }


            JscriptPrint(Resources.MessageTips.DeleteSuccess, "permissionadmin.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);


            PermissionsDatabind();
        }

	}
}
