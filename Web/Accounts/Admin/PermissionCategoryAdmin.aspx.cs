﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.Accounts.Admin
{
    public partial class PermissionCategoryAdmin : Edge.Web.UI.ManagePage
    {
        AccountsPrincipal user;
        User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Button btn = (Button)Page.FindControl("btnDelete");
                btn.Attributes.Add("onclick", string.Format("return confirm('{0}');", Resources.MessageTips.ConfirmDeleteRecord));

                user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                if (Session["UserInfo"] == null)
                {
                    return;
                }
               
                currentUser = (Edge.Security.Manager.User)Session["UserInfo"];
                BindCategoriesTree();
            
            }
        }

        protected void BindCategoriesTree()
        {
            this.lblMsg.Visible = false;

            this.TreeView1.ShowCheckBoxes = TreeNodeTypes.All;
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());
            BindTreeView(ds.Tables[0]);
            BindTree();
        }

        #region 绑定权限类别树
        //邦定根节点
        public void BindTreeView(DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + 0);//　选出所有子节点	

            TreeView1.Nodes.Clear(); // 清空树
            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                string parentid = r["ParentID"].ToString();

                this.TreeView1.Font.Size = FontUnit.Parse("9");

                TreeNode rootnode = new TreeNode();
                rootnode.Text = text;
                rootnode.Value = nodeid;
                rootnode.Expanded = true;

                TreeView1.Nodes.Add(rootnode);

                int sonparentid = int.Parse(nodeid);// or =location
                CreateNode(sonparentid, rootnode, dt);

            }
            this.TreeView1.ShowLines = true;
            this.TreeView1.ShowExpandCollapse = true;//是否显示前面的加减号

        }

        //邦定任意节点
        public void CreateNode(int parentid, TreeNode parentnode, DataTable dt)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);//选出所有子节点			
            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();

                TreeNode node = new TreeNode();
                node.Text = text;
                node.Value = nodeid;
                node.Expanded = false;
                int sonparentid = int.Parse(nodeid);// or =location

                if (parentnode == null)
                {
                    TreeView1.Nodes.Clear();
                    parentnode = new TreeNode();
                    TreeView1.Nodes.Add(parentnode);
                }
                parentnode.ChildNodes.Add(node);
                CreateNode(sonparentid, node, dt);
                //}//endif

            }//endforeach		

        }

        #endregion

        #region 绑定下拉树
        private void BindTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());


            this.listTarget.Items.Clear();
            //加载树
            this.listTarget.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.listTarget.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, ds.Tables[0], blank);

            }
            this.listTarget.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.listTarget.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }
        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (TreeNode tn in this.TreeView1.CheckedNodes)
            {
                int CategoryId = int.Parse(tn.Value);
                PermissionCategories c = new PermissionCategories();
                //保存日志
                // SaveLogs("");
                Logger.Instance.WriteOperationLog(this.PageName, "Delete Category " + tn.Text);
                c.Delete(CategoryId);
            }
            string msg = string.Format(Resources.MessageTips.DeleteMenu, this.TreeView1.CheckedNodes.Count);
            JscriptPrint(msg, "#", Resources.MessageTips.SUCESS_TITLE); 
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ViewState["Oper"] == null)
            {
                this.lblMsg.Visible = true;
                this.lblMsg.Text = Resources.MessageTips.PleaseSelectItem;
                return;        
            }

            string oper = ViewState["Oper"].ToString();

            if (oper == "Add")
            {
                string name = this.txtName.Text;
                int parentID = int.Parse(this.listTarget.SelectedValue);
                int orderID =int.Parse(this.txtId.Text);
                PermissionCategories c = new PermissionCategories();
               int id= c.Create(name,parentID,orderID);

               if (id > 0)
               {
                   Logger.Instance.WriteOperationLog(this.PageName, "Add Category " + name);

                   JscriptPrint(Resources.MessageTips.AddSuccess, "", Resources.MessageTips.SUCESS_TITLE);
                   BindCategoriesTree();
               }
               else
               {
                   JscriptPrint(Resources.MessageTips.AddFailed, "", Resources.MessageTips.FAILED_TITLE);
               }

            }
            else if (oper == "Edit")
            {
                if (string.IsNullOrEmpty(this.TreeView1.SelectedValue))
                {
                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.PleaseSelectItem;
                    return;
                }
                string name = this.txtName.Text;
                int updateID =int.Parse(this.TreeView1.SelectedValue);
                int parentID = int.Parse(this.listTarget.SelectedValue);
                int orderID = int.Parse(this.txtId.Text);
                PermissionCategories c = new PermissionCategories();
                int id = c.Update(updateID,name, parentID, orderID);

                if (id > 0)
                {
                    Logger.Instance.WriteOperationLog(this.PageName, "Update Category " + name);

                    JscriptPrint(Resources.MessageTips.UpdateSuccess, "", Resources.MessageTips.SUCESS_TITLE);
                    BindCategoriesTree();
                }
                else
                {
                    JscriptPrint(Resources.MessageTips.UpdateFailed, "", Resources.MessageTips.FAILED_TITLE);
                }
            }
           
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            this.txtId.Text = "";
            this.txtName.Text = "";
            this.listTarget.SelectedValue = "0";

            ViewState["Oper"] = "Add";
        }

        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            int id = this.TreeView1.SelectedValue == "" ? 0 : int.Parse(this.TreeView1.SelectedValue);

            System.Data.DataRow row = new Edge.Security.Manager.PermissionCategories().GetPermissionCategoriesDetails(id);

            if (row != null)
            {
                this.txtId.Text = row["OrderID"].ToString();
                this.txtName.Text = row["Description"].ToString();
                this.listTarget.SelectedValue = row["ParentID"].ToString();
            }

            ViewState["Oper"] = "Edit";
        }

    }
}
