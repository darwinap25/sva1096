﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Edge.Security.Manager;
using Edge.Web.Tools;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model;
namespace Edge.Web.Accounts.Admin
{
	/// <summary>
	/// EditRole 的摘要说明。
	/// </summary>
    public partial class EditRole : Edge.Web.UI.ManagePage
	{
		protected System.Web.UI.WebControls.Button Button1;
	
		private Role currentRole;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!Page.IsPostBack) 
			{
				Button btn = (Button)Page.FindControl("RemoveRoleButton");
                btn.Attributes.Add("onclick", string.Format("return confirm('{0}');", Resources.MessageTips.ConfirmDeleteRole));
				DoInitialDataBind();
				CategoryDownList_SelectedIndexChanged(sender,e);
			}
		}

		//绑定类别列表
		private void DoInitialDataBind()
		{
            string roleID=string.Empty;
            if (Request["RoleID"]!=null)
            {
             roleID= Request["RoleID"].ToString().Trim();
            }
            //DataSet dsCardIssuer = new Edge.SVA.BLL.CardIssuer().GetAllRecordList();            
            DataSet dsBrandID = new Edge.SVA.BLL.Brand().GetAllRecordList();    
            List<string> list = new List<string>();
            //DataSet dsRelationRoleIssuer = new Edge.SVA.BLL.RelationRoleIssuer().GetList(" RoleID=" +roleID);
            DataSet dsRelationRoleBrand = new Edge.SVA.BLL.RelationRoleBrand().GetList(" RoleID=" + roleID);
                foreach (DataRow item in dsRelationRoleBrand.Tables[0].Rows)
                {
                    string id=item["BrandID"].ToString();
                    if (!list.Contains(id))
                    {
                        list.Add(id);
                    }
                }

            //ControlTool.BindDataSetCheckboxList(this.CheckBoxList1, dsBrandID, "CardIssuerID", "CardIssuerName1", "CardIssuerName2", "CardIssuerName3", list);
                ControlTool.BindDataSetCheckboxList(this.CheckBoxList1, dsBrandID, "BrandID", "BrandName1", "BrandName2", "BrandName3", list);


            currentRole = new Role(Convert.ToInt32(Request["RoleID"]),Session["SiteLanguage"].ToString());//todo: 修改成多语言。
			RoleLabel.Text = Resources.MessageTips.CurrentRole + currentRole.Description;
			this.TxtNewname.Text=currentRole.Description;

            //DataSet allCategories = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            //CategoryDownList.DataSource = allCategories.Tables[0];
            //CategoryDownList.DataTextField = "Description";
            //CategoryDownList.DataValueField = "CategoryID";
            //CategoryDownList.DataBind();

            BindTree();
		}

        #region 绑定下拉树
        private void BindTree()
        {
            DataSet ds;
            ds = AccountsTool.GetAllCategories(Session["SiteLanguage"].ToString());


            this.CategoryDownList.Items.Clear();
            //加载树
            this.CategoryDownList.Items.Add(new ListItem("#", "0"));
            DataRow[] drs = ds.Tables[0].Select("ParentID= " + 0);


            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                //string parentid=r["ParentID"].ToString();
                //string permissionid=r["PermissionID"].ToString();
                text = "╋" + text;
                this.CategoryDownList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank = "├";

                BindNode(sonparentid, ds.Tables[0], blank);

            }
            this.CategoryDownList.DataBind();

        }

        private void BindNode(int parentid, DataTable dt, string blank)
        {
            DataRow[] drs = dt.Select("ParentID= " + parentid);

            foreach (DataRow r in drs)
            {
                string nodeid = r["CategoryID"].ToString();
                string text = r["Description"].ToString();
                text = blank + "『" + text + "』";
                this.CategoryDownList.Items.Add(new ListItem(text, nodeid));
                int sonparentid = int.Parse(nodeid);
                string blank2 = blank + "─";

                BindNode(sonparentid, dt, blank2);
            }
        }
        #endregion


		//选择类别，填充2个listbox
		protected void CategoryDownList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int categoryID=Convert.ToInt32(CategoryDownList.SelectedItem.Value);	
			FillCategoryList(categoryID);		
			SelectCategory( categoryID, false );			
		}


		//填充非权限列表
		private void FillCategoryList(int categoryId)
		{
            currentRole = new Role(Convert.ToInt32(Request["RoleID"]),Session["SiteLanguage"].ToString());//todo: 修改成多语言。
			DataTable categories = currentRole.NoPermissions.Tables["Categories"];
			DataRow currentCategory = categories.Rows.Find( categoryId );

			if (currentCategory != null) 
			{
				DataRow[] permissions = currentCategory.GetChildRows("PermissionCategories");

				CategoryList.Items.Clear();
				foreach (DataRow currentRow in permissions)
				{
					CategoryList.Items.Add(
						new ListItem( (string)currentRow["Description"], Convert.ToString(currentRow["PermissionID"])) );
				}

			}
		}


		//填充已有权限listbox
		private void SelectCategory(int categoryId, bool forceSelection)
		{
            currentRole = new Role(Convert.ToInt32(Request["RoleID"]),Session["SiteLanguage"].ToString());//todo: 修改成多语言。
			DataTable categories = currentRole.Permissions.Tables["Categories"];
			DataRow currentCategory = categories.Rows.Find( categoryId );

			if (currentCategory != null) 
			{
				DataRow[] permissions = currentCategory.GetChildRows("PermissionCategories");

				PermissionList.Items.Clear();
				foreach (DataRow currentRow in permissions)
				{
					PermissionList.Items.Add(
						new ListItem( (string)currentRow["Description"], Convert.ToString(currentRow["PermissionID"])) );
				}
			}


		}

		//增加权限
		protected void AddPermissionButton_Click(object sender, System.EventArgs e)
		{
            
            //if(this.CategoryList.SelectedIndex>-1)
            //{
            //    int currentRole = Convert.ToInt32(Request["RoleID"]);
            //    Role bizRole = new Role(currentRole,Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            //    bizRole.AddPermission( Convert.ToInt32(this.CategoryList.SelectedValue) );			
            //    CategoryDownList_SelectedIndexChanged(sender,e);
            //}
		    
            foreach (ListItem li in this.CategoryList.Items)
            {
                if (li.Selected)
                {
                    int currentRole = Convert.ToInt32(Request["RoleID"]);
                    Role bizRole = new Role(currentRole, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                    bizRole.AddPermission(Convert.ToInt32(li.Value));			
                }
            }
            CategoryDownList_SelectedIndexChanged(sender, e);
		}

		//移除权限
		protected void RemovePermissionButton_Click(object sender, System.EventArgs e)
		{
            //if(this.PermissionList.SelectedIndex>-1)
            //{
            //    int currentRole = Convert.ToInt32(Request["RoleID"]);
            //    Role bizRole = new Role(currentRole,Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            //    bizRole.RemovePermission( Convert.ToInt32(this.PermissionList.SelectedValue) );
            //    CategoryDownList_SelectedIndexChanged(sender,e);
            //}

            foreach (ListItem li in this.PermissionList.Items)
            {
                if (li.Selected)
                {
                    int currentRole = Convert.ToInt32(Request["RoleID"]);
                    Role bizRole = new Role(currentRole, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
                    bizRole.RemovePermission(Convert.ToInt32(li.Value));
                }
            }
            CategoryDownList_SelectedIndexChanged(sender, e);
		}

		protected void RemoveRoleButton_Click(object sender, System.EventArgs e)
		{
			int currentRole = Convert.ToInt32(Request["RoleID"]);
            Role bizRole = new Role(currentRole,Session["SiteLanguage"].ToString());//todo: 修改成多语言。
			bizRole.Delete();
			Server.Transfer("RoleAdmin.aspx");
		}

        protected void BtnUpName_Click(object sender, EventArgs e)
        {
            string newname = this.TxtNewname.Text.Trim();
            int roleID = ConvertTool.ConverType<int>(Request["RoleID"]);
            try
            {
            currentRole = new Role(roleID,Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            currentRole.Description = newname;
            currentRole.Update();

            Logger.Instance.WriteOperationLog(this.PageName, "Update Role " + currentRole.Description);

            //Edge.SVA.BLL.RelationRoleIssuer bll = new Edge.SVA.BLL.RelationRoleIssuer();
            //foreach (ListItem item in this.CheckBoxList1.Items)
            //{
            //    int RoleID=ConvertTool.ToInt(item.Value);
            //    if (item.Selected)
            //    {
            //        if (!bll.Exists(roleID,RoleID))
            //        {
            //            Edge.SVA.Model.RelationRoleIssuer model = new Edge.SVA.Model.RelationRoleIssuer();
            //            model.RoleID = roleID;
            //            model.CardIssuerID = RoleID;
            //            bll.Add(model);
            //        }
            //    } 
            //    else
            //    {
            //        if (bll.Exists(roleID, RoleID))
            //        {
            //            bll.Delete(roleID,RoleID);
            //        }
            //    }
            //}
            Edge.SVA.BLL.RelationRoleBrand bll = new Edge.SVA.BLL.RelationRoleBrand();
            foreach (ListItem item in this.CheckBoxList1.Items)
            {
                int RoleID = ConvertTool.ToInt(item.Value);
                if (item.Selected)
                {
                    if (!bll.Exists(roleID, RoleID))
                    {
                        Edge.SVA.Model.RelationRoleBrand model = new Edge.SVA.Model.RelationRoleBrand();
                        model.RoleID = roleID;
                        model.BrandID = RoleID;
                        bll.Add(model);
                    }
                }
                else
                {
                    if (bll.Exists(roleID, RoleID))
                    {
                        bll.Delete(roleID, RoleID);
                    }
                }
            }
            DoInitialDataBind();
            Edge.SVA.BLL.RelationRoleBrand bll1 = new Edge.SVA.BLL.RelationRoleBrand();
            AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());
            SessionInfo.BrandIDsStr = bll1.GetBrandIDsStr(user.RoleIDList);
            JscriptPrint(Resources.MessageTips.UpdateSuccess, "editrole.aspx?roleid=" + roleID.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            catch (System.Exception ex)
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "editrole.aspx?roleid=" + roleID.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }

        protected void button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("RoleAdmin.aspx");
        }

        protected void cbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            bool status = this.cbSelectAll.Checked;
            foreach (ListItem item in this.CheckBoxList1.Items)
            {
                item.Selected = status;
            }
        }
	}
}
