﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Edge.Web.Index" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href='<%#GetJSThickBoxCssPath() %>' />

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQeruyAlertsPath() %>'></script>

    <link media="screen" href='<%#GetjQeruyAlertsStylePath() %>' type="text/css" rel="stylesheet" />
    <link href='<%#GetjQueryLigeruiCSS() %>' rel="stylesheet" type="text/css" />

    <script src='<%#GetjQueryLigeruiBase() %>' type="text/javascript"></script>

    <script src='<%#GetjQueryLigerui() %>' type="text/javascript"></script>

    <script type="text/javascript">
        var tab = null;
        var accordion = null;
        var tree = null;
        $(function() {
            //页面布局
            $("#global_layout").ligerLayout({ leftWidth: 180, height: '100%', topHeight: 65, bottomHeight: 24, allowTopResize: false, allowBottomResize: false, allowLeftCollapse: true, onHeightChanged: f_heightChanged });

            var height = $(".l-layout-center").height();
            $("#sysMain").height(height);

            //Tab
            // $("#framecenter").ligerTab({ height: height });

            //左边导航面板
            $("#global_left_nav").ligerAccordion({ height: height - 25, speed: null });

            //            $(".l-link").hover(function() {
            //                $(this).addClass("l-link-over");
            //            }, function() {
            //                $(this).removeClass("l-link-over");
            //            });

            // tab = $("#framecenter").ligerGetTabManager();
            //accordion = $("#global_left_nav").ligerGetAccordionManager();
            //tree = $("#global_channel_tree").ligerGetTreeManager();
            //tree.expandAll(); //默认展开所有节点
            //$("#pageloading_bg,#pageloading").hide();


        });

        function f_heightChanged(options) {
            if (tab)
                tab.addHeight(options.diff);
            if (accordion && options.middleHeight - 24 > 0)
                accordion.setHeight(options.middleHeight - 24);

            $("#sysMain").height(options.middleHeight);
        }

        var backA_id = "";
        function GetNode(evt) {
            evt = evt ? evt : (window.event ? window.event : null);
            if (!evt) return true;

            var target = evt.srcElement ? evt.srcElement : evt.target;

            if (!target) return true;
            if (target.tagName.toUpperCase() != "A") return true;

            if (backA_id != "") {
                var backa = document.getElementById(backA_id);
                backa.style.color = ""; //恢复默认
                backa.style.fontWeight = ""; //恢复默认

            }

            var id = target.id;
            if (id == null || id == undefined || id.length <= 0) return true;

            var a = document.getElementById(id);

            a.style.color = "#cc0066";
            a.style.fontWeight = 'bold';

            backA_id = id;

            return true;
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="global_layout" class="layout" style="width: 100%;">
        <!--头部-->
        <div position="top" class="header">
            <div class="header_box">
                <div class="header_right">
                    <span><b>
                        <asp:Label ID="lblAdminName" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;</b>您好，欢迎光临</span>
                    <br />
                    <%-- <asp:LinkButton ID="lbtnExit" runat="server" OnClick="lbtnExit_Click" >安全退出</asp:LinkButton>--%>
                    <asp:Button ID="btnExit" runat="server" Text="退出" CssClass="submit" OnClick="btnExit_Click" />
                </div>
                <a class="veilogo"></a><a class="logo"></a>
            </div>
        </div>
        <!--左边-->
        <div position="left" id="global_left_nav">
            <div title="SVA System" iconcss="menu-icon-model" class="l-scroll">
            Translate__Remain_121_Start
                <asp:TreeView ID="TreeView1" runat="server" onclick="return GetNode(event);">
                </asp:TreeView>
                Translate__Remain_121_End
            </div>
        </div>
        <div position="center" id="framecenter" toolsid="tab-tools-nav">
            <iframe frameborder="0" id="sysMain" name="sysMain" src="AdminCenter.aspx" width="100%"
                height="100%"></iframe>
        </div>
        <div position="bottom" class="footer">
            <div class="copyright">
                Copyright &copy; 2012. VEI. All Rights Reserved.Version:<asp:Label ID="lblVersion" runat="server"
                    ></asp:Label></div>
        </div>
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
