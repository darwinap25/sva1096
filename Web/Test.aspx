﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="Edge.Web.Test" %>

<%@ Register src="Controls/SearchBox.ascx" tagname="SearchBox" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <script src="/js/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="/js/CommonJs.js" type="text/javascript"></script>
    <script src="/js/dynamicCon.js" type="text/javascript"></script>
    <link href="/js/jqueryUi/themes/ui-lightness/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="/js/jqueryUi/themes/ui-lightness/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <script src="/js/jqueryUi/jquery.ui.core.min.js" type="text/javascript"></script>
    <script src="/js/jqueryUi/jquery.ui.datepicker.min.js" type="text/javascript"></script>
    <script src="/js/jqueryUi/i18n/jquery.ui.datepicker-zh-CN.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <div class="searchCon">
        <uc1:SearchBox ID="SearchBox1" runat="server" />
        </div>
    
    </div>
        <script type="text/javascript">
            var funs = {
                Init: function() {
                    //通用搜索
                    funs.search();
                },
                //通用搜索栏
                search: function() {
                    $sel = $(".searchCon").find("select[name='selSearchType']"); //要搜索的字段下拉框
                    $sel.live("change", function() {
                        funs.fieldChangeEvent(this);
                    });
                },
                fieldChangeEvent: function(obj) {
                    $inputObj = $(obj).closest("tr").find("[name='txtSearchValue']"); //要输入的地方
                    var selHtml = "";
                    switch (obj.value) {
                        case "GroupId": //所在组
                            selHtml = '<%=this.GetMore("group")%>';
                            break;
                        case "IsSimple": //类型
                            selHtml = '<%=this.GetMore("type")%>';
                            break;
                        case "VerifyState": //审核状态
                            selHtml = '<%=this.GetMore("ver")%>';
                            break;
                        case "State": //有效性状态
                            selHtml = '<%=this.GetMore("state")%>';
                            break;
                    }
                    if (selHtml != "") {
                        $inputObj.html(selHtml);
                    }
                }
            }
            $(function() {
                funs.Init();
            });
    </script>
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </form>
</body>
</html>
