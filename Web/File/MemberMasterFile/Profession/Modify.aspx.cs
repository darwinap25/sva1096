﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Profession
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Profession ,Edge.SVA.Model.Profession>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        static Edge.SVA.Model.Profession curItem = new Edge.SVA.Model.Profession();
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);
            Edge.SVA.Model.Profession item = this.GetUpdateObject();

            if (Tools.DALTool.isHasProfessionCode(this.ProfessionCode.Text.Trim(), item.ProfessionID))
            {
                JscriptPrint(Resources.MessageTips.ExistProfessionCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.ProfessionCode = item.ProfessionCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Profession>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }

            //Response.Redirect("List.aspx?page=0");
        }
    }
}
