﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MemberMasterFile.Education
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Education,Edge.SVA.Model.Education>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

     
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);
            Edge.SVA.Model.Education item = this.GetUpdateObject();

            if (item == null)
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page="+page.ToString(), Resources.MessageTips.FAILED_TITLE);
                return;
            }

            if (Tools.DALTool.isHasEducationCode(this.EducationCode.Text.Trim(), item.EducationID))
            {
                JscriptPrint(Resources.MessageTips.ExistEducationCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.EducationCode = item.EducationCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Education>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }

            //Response.Redirect("List.aspx?page=0");
        }
    }
}
