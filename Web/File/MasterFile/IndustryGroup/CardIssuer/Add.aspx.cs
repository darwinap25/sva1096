﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardIssuer, Edge.SVA.Model.CardIssuer>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!this.IsPostBack)
            //{
            //    DataSet ds = new Edge.SVA.BLL.Industry().GetAllList();
            //    Edge.Web.Tools.ControlTool.BindDataSet(IndustryID, ds, "IndustryID", "IndustryName1", "IndustryName2", "IndustryName3");
            //}
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Edge.SVA.BLL.CardIssuer bll = new Edge.SVA.BLL.CardIssuer();
            DataSet ds = bll.GetAllList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                JscriptPrint(Resources.MessageTips.ExistIssuer, "List.aspx?page=0", "Failed");
                return;
            }
            Edge.SVA.Model.CardIssuer item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.CardIssuer>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", "Success");
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", "Failed");
            }
        }
    }
}
