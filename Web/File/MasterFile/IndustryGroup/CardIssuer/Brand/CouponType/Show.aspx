﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript">
        $(function () {

            checkIsImportCouponNumber();
            checkIsFixedAmount();
            checkCouponSpecifyExpiryDate();
        });

        //是否会员优惠券
        function checkIsMemberBind() {
            var method = $("input[name='IsMemberBind']:checked").val();
            if (method == '1') {
                $("#issueBrand,#issueLocation,#issueStore,#useBrand,#useLocation,#useStore").hide();

            }
            else {
                $("#issueBrand,#issueLocation,#issueStore,#useBrand,#useLocation,#useStore").show();

            }
        }

        //是否导入优惠券
        function checkIsImportCouponNumber() {
            var method = $("input[name='IsImportCouponNumber']:checked").val();
            if (method == '1') { //导入

                $("#msgtable tr[class=ManualRoles]").hide();
                $("#msgtable tr[class=ImportRoles]").show();
                if ($("#UIDToCouponNumber").val() != "0") $("#CouponNumMaskImportTR,#CouponNumPatternImportTR").hide();
            }
            else {//手动

                $("#msgtable tr[class=ManualRoles]").show();
                $("#msgtable tr[class=ImportRoles]").hide();

                checkCheckDigitMode();
            }

            $("#IsImportCouponNumber,#CouponCheckdigit,#UIDToCouponNumber").hide();
        }


        //是否固定面额
        function checkIsFixedAmount() {
            var method = $("input[name='CoupontypeFixedAmount']:checked").val();
            if (method == '0') {
                $("#CouponTypeAmount").val("");
                $("#CouponTypeAmount").attr('disabled', ' true');
                $("#CouponTypeAmount").removeClass("required number");
            }
            else {
                $("#CouponTypeAmount").removeAttr('disabled');
                $("#CouponTypeAmount").addClass("required number");
            }
        }

        // CheckDigitMode
        function checkCheckDigitMode() {
            var isCouponCheckdigit = $("input[name='CouponCheckdigit']:checked").val();

            if (isCouponCheckdigit == 'False') //没有Check
            {
                $("#CheckDigitModeTR").hide();
            }
            else {
                $("#CheckDigitModeTR").show();
            }
        }



        //是否固定有效期
        function checkCouponSpecifyExpiryDate() {
            var isCouponSpecifyExpiryDate = $("#CouponValidityUnit").val();

            if (isCouponSpecifyExpiryDate == '6') //是固定有效期
            {
                $("#CouponValidityDuration").hide();

                $("#CouponSpecifyExpiryDate").show()
            }
            else {
                $("#CouponValidityDuration").show();

                $("#CouponSpecifyExpiryDate").hide()
            }

            $("#CouponValidityUnit").hide();

        }
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <script type="text/javascript">
         $(function() {
              $(".liTabs").click(function() {parent.jAlert("<%=Edge.Messages.Manager.MessagesTool.instance.GetMessage("10028")%>", "<%=Resources.MessageTips.WARNING_TITLE%>");});
             });
    </script>
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="tabs">
        <ul>
            <li onclick="tabs(1);"><a><span>基本信息</span></a></li>
            <li class="liTabs"><a><span>消费金额转换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>积分转换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>现金转换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>优惠券转换优惠券规则</span></a></li>
            <li class="liTabs"><a><span>组合兑换规则</span></a></li>
        </ul>
    </div>
    <div class="fragment">
        <table id="msgtable" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    基本信息
                </th>
            </tr>
            <tr>
                <td width="25%" align="right">
                    优惠劵类型编号：
                </td>
                <td width="75%">
                    <asp:Label ID="CouponTypeCode" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    描述：
                </td>
                <td>
                    <asp:Label ID="CouponTypeName1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述1：
                </td>
                <td>
                    <asp:Label ID="CouponTypeName2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述2：
                </td>
                <td>
                    <asp:Label ID="CouponTypeName3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    品牌：
                </td>
                <td>
                    <asp:Label ID="BrandID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    活动：
                </td>
                <td>
                    <asp:Label ID="CampaignID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否固定面额：
                </td>
                <td>
                    <asp:Label ID="CoupontypeFixedAmountView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="CoupontypeFixedAmount" runat="server" TabIndex="15" RepeatDirection="Horizontal"
                        Enabled="false">
                        <asp:ListItem Text="是的" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="不是" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    面额：
                </td>
                <td>
                    <asp:Label ID="CouponTypeAmount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    币种：
                </td>
                <td>
                    <asp:Label ID="CurrencyID" runat="server"></asp:Label>
                </td>
            </tr>
              <tr>
                <td align="right">
                    销售货号匹配清单：
                </td>
                <td>
                    <a id="A3" class="thickbox btn_bg" href="SaleProduct/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        查看销售货号</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    指定限购商品：
                </td>
                <td>
                    <a id="A1" class="thickbox btn_bg" href="Product/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        查看限购商品</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    指定限购部门：
                </td>
                <td>
                    <a id="A2" class="thickbox btn_bg" href="Department/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        查看限购部门</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券布局：
                </td>
                <td>
                    <asp:Label ID="CouponTypeLayoutFile" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券图片：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CouponTypePicFile" runat="server" FileType="preview" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否会员优惠券：
                </td>
                <td>
                    <asp:Label ID="IsMemberBindView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsMemberBind" runat="server" Enabled="false" RepeatDirection="Horizontal">
                        <asp:ListItem Text="是的" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="不是" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    发行品牌/店铺：
                </td>
                <td>
                    <a id="issueBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        品牌</a> <a id="issueStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                            店铺</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    使用品牌/店铺：
                </td>
                <td>
                    <a id="useBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        品牌</a> <a id="useStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CouponTypeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                            店铺</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    密码规则：
                </td>
                <td>
                    <asp:Label ID="PasswordRuleID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券面额清空控制：
                </td>
                <td>
                    <asp:Label ID="CouponforfeitControlView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CouponforfeitControl" runat="server" TabIndex="15" Enabled="false"
                        CssClass="dropdownlist required">
                        <asp:ListItem Value="0">直接清空</asp:ListItem>
                        <asp:ListItem Value="1">当日清空</asp:ListItem>
                        <asp:ListItem Value="2">不清空</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    优惠券号码规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    优惠券号码是否导入：
                </td>
                <td>
                    <asp:Label ID="IsImportCouponNumberView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsImportCouponNumber" runat="server" TabIndex="15" RepeatDirection="Horizontal"
                        Enabled="false">
                        <asp:ListItem Text="是" Value="1"></asp:ListItem>
                        <asp:ListItem Text="否" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <th colspan="2" align="left">
                    手动创建编号规则
                </th>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    优惠券号码编号规则：
                </td>
                <td>
                    <asp:Label ID="CouponNumMask" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    优惠券号码编号前缀号码：
                </td>
                <td>
                    <asp:Label ID="CouponNumPattern" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    是否包含校验位：
                </td>
                <td>
                    <asp:Label ID="CouponCheckdigitView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="CouponCheckdigit" runat="server" TabIndex="18" RepeatDirection="Horizontal"
                        Enabled="false">
                        <asp:ListItem Text="是" Value="True"></asp:ListItem>
                        <asp:ListItem Text="否" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="CheckDigitModeTR" class="ManualRoles">
                <td align="right">
                    校验位计算方法：
                </td>
                <td>
                    <asp:Label ID="CheckDigitModeIDView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CheckDigitModeID" runat="server" TabIndex="18" CssClass="dropdownlist"
                        Enabled="false">
                        <asp:ListItem Value="1">EAN13</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    Translate__Special_121_Start是否优惠券号码复制到优惠券物理编号（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="CouponNumberToUIDView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CouponNumberToUID" runat="server" TabIndex="18" CssClass="dropdownlist"
                        SkinID="NoClass" Enabled="false">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <th colspan="2" align="left">
                    导入物理编号规则
                </th>
            </tr>
            <tr class="ImportRoles" id="CouponNumMaskImportTR">
                <td align="right">
                    优惠券号码编号规则：
                </td>
                <td>
                    <asp:Label ID="CouponNumMaskImport" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ImportRoles" id="CouponNumPatternImportTR">
                <td align="right">
                    优惠券号码编号前缀号码：
                </td>
                <td>
                    <asp:Label ID="CouponNumPatternImport" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="IsConsecutiveUIDView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsConsecutiveUID" runat="server" CssClass="required" TabIndex="18"
                        Enabled="false" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="UIDCheckDigitView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="UIDCheckDigit" runat="server" CssClass="required" TabIndex="18"
                        Enabled="false" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start是否优惠券物理编号复制到优惠券号码：（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="UIDToCouponNumberView" runat="server"></asp:Label>
                    <asp:DropDownList ID="UIDToCouponNumber" runat="server" TabIndex="18" CssClass="dropdownlist"
                        Enabled="false" SkinID="NoClass">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    有效期规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    延长有效期值：
                </td>
                <td>
                    <asp:Label ID="CouponValidityDuration" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    延长有效期的单位：
                </td>
                <td>
                    <asp:Label ID="CouponValidityUnitView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CouponValidityUnit" runat="server" Enabled="false" CssClass="dropdownlist">
                        <asp:ListItem Value="1" Selected="True">年</asp:ListItem>
                        <asp:ListItem Value="2">月</asp:ListItem>
                        <asp:ListItem Value="3">星期</asp:ListItem>
                        <asp:ListItem Value="4">天</asp:ListItem>
                        <asp:ListItem Value="6">指定失效日期</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠券固定过期日期：
                </td>
                <td>
                    <asp:Label ID="CouponSpecifyExpiryDate" runat="server" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    激活是否重置有效期：
                </td>
                <td>
                    <asp:Label ID="ActiveResetExpiryDateView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="ActiveResetExpiryDate" runat="server" TabIndex="19" CssClass="required"
                        RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    优惠券转增规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    优惠劵是否可以转赠：
                </td>
                <td>
                    <asp:Label ID="CouponTypeTransferView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CouponTypeTransfer" runat="server" TabIndex="22" CssClass="dropdownlist"
                        Enabled="false">
                        <asp:ListItem Value="0">不允许</asp:ListItem>
                        <asp:ListItem Value="1">同品牌转赠</asp:ListItem>
                        <asp:ListItem Value="2">同卡级别转赠</asp:ListItem>
                        <asp:ListItem Value="3">同卡类型转赠</asp:ListItem>
                        <asp:ListItem Value="4">任意转赠</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    有效性资料
                </th>
            </tr>
            <tr>
                <td align="right">
                    优惠劵类型启用日期：
                </td>
                <td>
                    <asp:Label ID="CouponTypeStartDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    优惠劵类型结束日期：
                </td>
                <td>
                    <asp:Label ID="CouponTypeEndDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    创建时间：
                </td>
                <td>
                    <asp:Label ID="CreatedOn" runat="server" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    创建人：
                </td>
                <td>
                    <asp:Label ID="CreatedBy" runat="server" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    上次修改时间：
                </td>
                <td>
                    <asp:Label ID="UpdatedOn" runat="server" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    上次修改人：
                </td>
                <td>
                    <asp:Label ID="UpdatedBy" runat="server" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="javascript:history.back();"
                            class="submit" /></div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    消费金额转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnConsumeAmountList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <%--                                    <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>--%>
                                    <%--   <th width="6%">
                                        编号
                                    </th>--%>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        金额
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%-- <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                </td>--%>
                                <%--        <td align="center">
                                    <asp:Label ID="lblPointRuleID" runat="server" Text='<%#Eval("KeyID")%>'></asp:Label>
                                </td>--%>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangeConsumeAmount" runat="server" Text='<%#Eval("ExchangeConsumeAmount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnConsumeAmount/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=show">
                                        查看</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <%-- <div style="line-height: 30px; height: 30px;">
                        <div id="Div1" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnConsumeAmountDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                                    OnClick="lbtnEarnConsumeAmountDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnConsumeAmountAdd" runat="server" OnClick="lbtnEarnConsumeAmountAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    积分转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnPointList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <%--  <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>--%>
                                    <%--                                  <th width="6%">
                                        编号
                                    </th>--%>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        积分
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%--   <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                </td>--%>
                                <%--                                <td align="center">
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("KeyID")%>' Visible="false"></asp:Label><a
                                        href="EarnCouponRule/Show.aspx?id=<%#Eval("KeyID") %>"><asp:Label ID="lblPointRuleID"
                                            runat="server" Text='<%#Eval("KeyID")%>'></asp:Label></a>
                                </td>--%>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangePoint" runat="server" Text='<%#Eval("ExchangePoint")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnPoint/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=show">
                                        查看</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <%--<div style="line-height: 30px; height: 30px;">
                        <div id="Div2" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnPointDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                                    OnClick="lbtnEarnPointDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnPointAdd" runat="server" OnClick="lbtnEarnPointAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    现金转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnAmountList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <%--  <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>--%>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        兑换金额
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%--       <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                </td>--%>
                                <%--                                <td align="center">
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("KeyID")%>' Visible="false"></asp:Label><a
                                        href="EarnCouponRule/EarnAmount/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>"><asp:Label ID="lblPointRuleID"
                                            runat="server" Text='<%#Eval("KeyID")%>'></asp:Label></a>
                                </td>--%>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExchangeAmount" runat="server" Text='<%#Eval("ExchangeAmount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnAmount/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID") %>&type=show">
                                        查看</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <%--  <div style="line-height: 30px; height: 30px;">
                        <div id="Div3" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnAmountDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                                    OnClick="lbtnEarnAmountDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnAmountAdd" runat="server" OnClick="lbtnEarnAmountAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    优惠券转换优惠券规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnCouponList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <%--   <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>--%>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        数量
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%--        <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                </td>--%>
                                <td align="center">
                                    <asp:Label ID="lblPointRuleID" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangeCouponCount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatusName" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnCoupon/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID")%>&type=show">
                                        查看</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <%--  <div style="line-height: 30px; height: 30px;">
                        <div id="Div4" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnCouponDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                                    OnClick="lbtnEarnCouponDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnCouponAdd" runat="server" OnClick="lbtnEarnCouponAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    组合转换规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptEarnAmountPointList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtablelist">
                                <tr>
                                    <%--       <th width="6%">
                                        <input type="checkbox" onclick="checkAll(this);" />选择
                                    </th>--%>
                                    <th width="10%">
                                        兑换类型
                                    </th>
                                    <th width="10%">
                                        积分
                                    </th>
                                    <th width="10%">
                                        金额
                                    </th>
                                    <th width="10%">
                                        规则起始日期
                                    </th>
                                    <th width="10%">
                                        规则失效日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                    <th width="10%">
                                        操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%--        <td align="center">
                                    <asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" />
                                </td>--%>
                                <td align="center">
                                    <asp:Label ID="lblPointRuleID" runat="server" Text='<%#Eval("ExchangeTypeName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("ExchangePoint")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("ExchangeAmount")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate","{0:yyyy-MM-dd}")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatusName" runat="server" Text='<%#Eval("StatusName")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <span class="btn_bg"><a href="EarnCouponRule/EarnAmountPoint/Show.aspx?id=<%#Eval("KeyID") %>&CouponTypeID=<%#Eval("CouponTypeID")%>&type=show">
                                        查看</a></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                    <%--  <div style="line-height: 30px; height: 30px;">
                        <div id="Div5" class="right flickr">
                        </div>
                        <div class="left">
                            <span class="btn_bg">
                                <asp:LinkButton ID="lbtnEarnAmountPointDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');"
                                    OnClick="lbtnEarnAmountPointDel_Click">删除</asp:LinkButton>
                                <asp:LinkButton ID="lbtnEarnAmountPointAdd" runat="server" OnClick="lbtnEarnAmountPointAdd_Click">添加</asp:LinkButton>
                            </span>
                        </div>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
