﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponType,Edge.SVA.Model.CouponType>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int tabNum = string.IsNullOrEmpty(Request.Params["tabs"]) ? 1 : int.Parse(Request.Params["tabs"]);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "tabs", "<script type='text/javascript'> $(function() {tabs(" + tabNum + "); });</script>");

            if (!this.IsPostBack)
            {
                Session["CouponTypeConsumeStoreCheckedList"] = null;
                Session["CouponTypeEarnStoreCheckedList"] = null;

                //Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
                //Edge.Web.Tools.ControlTool.BindCouponTypeNature(this.CouponTypeNatureID);
                //Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                //Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);

                EarnCouponRuleRptBind("CouponTypeID=" + Request.Params["id"]);
            }

        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);
  
                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = brand == null ? "":DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);

                Edge.SVA.Model.Campaign campaign = new Edge.SVA.BLL.Campaign().GetModel(Model.CampaignID.GetValueOrDefault());
                string campaignText = campaign == null ? "" : DALTool.GetStringByCulture(campaign.CampaignName1, campaign.CampaignName2, campaign.CampaignName3);
                this.CampaignID.Text = campaign == null ? "" : ControlTool.GetDropdownListText(campaignText, campaign.CampaignCode);

                Edge.SVA.Model.Currency currency = new Edge.SVA.BLL.Currency().GetModel(Model.CurrencyID.GetValueOrDefault());
                string currencyText = currency == null ? "" : DALTool.GetStringByCulture(currency.CurrencyName1, currency.CurrencyName2, currency.CurrencyName3);
                this.CurrencyID.Text = currency == null ? "" : ControlTool.GetDropdownListText(currencyText, currency.CurrencyCode);

                Edge.SVA.Model.PasswordRule password = new Edge.SVA.BLL.PasswordRule().GetModel(Model.PasswordRuleID.GetValueOrDefault());
                string passwordText = password == null ? "" : DALTool.GetStringByCulture(password.Name1, password.Name2, password.Name3);
                this.PasswordRuleID.Text = password == null ? "" : ControlTool.GetDropdownListText(passwordText, password.PasswordRuleCode);

                this.IsMemberBindView.Text = this.IsMemberBind.SelectedItem == null ? "" : this.IsMemberBind.SelectedItem.Text;
                this.IsMemberBind.Visible = false;

                this.CoupontypeFixedAmountView.Text = this.CoupontypeFixedAmount.SelectedItem == null ? "" : this.CoupontypeFixedAmount.SelectedItem.Text;
                this.CoupontypeFixedAmount.Visible = false;

                this.CouponCheckdigitView.Text = this.CouponCheckdigit.SelectedItem == null ? "" : this.CouponCheckdigit.SelectedItem.Text;
                
                this.CouponValidityUnitView.Text = this.CouponValidityUnit.SelectedItem == null ? "" : this.CouponValidityUnit.SelectedItem.Text;

                this.CouponTypeTransferView.Text = this.CouponTypeTransfer.SelectedItem == null ? "" : this.CouponTypeTransfer.SelectedItem.Text;
                this.CouponTypeTransfer.Visible = false;

                this.IsImportCouponNumberView.Text = this.IsImportCouponNumber.SelectedItem == null ? "" : this.IsImportCouponNumber.SelectedItem.Text;

                this.UIDCheckDigitView.Text = this.UIDCheckDigit.SelectedItem == null ? "" : this.UIDCheckDigit.SelectedItem.Text;
                this.UIDCheckDigit.Visible = false;

                this.CheckDigitModeIDView.Text = this.CheckDigitModeID.SelectedItem == null ? "" : this.CheckDigitModeID.SelectedItem.Text;
                this.CheckDigitModeID.Visible = false;

                this.CouponNumberToUIDView.Text = this.CouponNumberToUID.SelectedItem == null ? "" : this.CouponNumberToUID.SelectedItem.Text;
                this.CouponNumberToUID.Visible = false;

                this.IsConsecutiveUIDView.Text = this.IsConsecutiveUID.SelectedItem == null ? "" : this.IsConsecutiveUID.SelectedItem.Text;
                this.IsConsecutiveUID.Visible = false;

                this.UIDToCouponNumberView.Text = this.UIDToCouponNumber.SelectedItem == null ? "" : this.UIDToCouponNumber.SelectedItem.Text;
                //this.UIDToCouponNumber.Visible = false;

                this.ActiveResetExpiryDateView.Text = this.ActiveResetExpiryDate.SelectedItem == null ? "" : this.ActiveResetExpiryDate.SelectedItem.Text;
                this.ActiveResetExpiryDate.Visible = false;

                this.CouponforfeitControlView.Text = this.CouponforfeitControl.SelectedItem == null ? "" : this.CouponforfeitControl.SelectedItem.Text;
                this.CouponforfeitControl.Visible = false;

                if (Model.IsImportCouponNumber.GetValueOrDefault() == 1 && Model.UIDToCouponNumber.GetValueOrDefault() == 0)
                {
                    this.CouponNumMaskImport.Text = Model.CouponNumMask;
                    this.CouponNumPatternImport.Text = Model.CouponNumPattern;
                }
            }
        }

        private void EarnCouponRuleRptBind(string strWhere)
        {
            Edge.SVA.BLL.EarnCouponRule bll = new Edge.SVA.BLL.EarnCouponRule();
            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            Edge.Web.Tools.DataTool.AddExchangeType(ds, "ExchangeTypeName", "ExchangeType");
            Edge.Web.Tools.DataTool.AddStatus(ds, "StatusName", "Status");

            DataView consumeAmountDV = ds.Tables[0].DefaultView;
            consumeAmountDV.RowFilter = "ExchangeType='5'";
            //this.lbtnEarnConsumeAmountDel.Enabled = consumeAmountDV.Count > 0 ? true : false;
            this.rptEarnConsumeAmountList.DataSource = consumeAmountDV;
            this.rptEarnConsumeAmountList.DataBind();

            DataView pointDV = ds.Tables[0].DefaultView;
            pointDV.RowFilter = "ExchangeType='2'";
            //this.lbtnEarnPointDel.Enabled = pointDV.Count > 0 ? true : false;
            this.rptEarnPointList.DataSource = pointDV;
            this.rptEarnPointList.DataBind();

            DataView amountDV = ds.Tables[0].DefaultView;
            amountDV.RowFilter = "ExchangeType='1'";
            //this.lbtnEarnAmountDel.Enabled = amountDV.Count > 0 ? true : false;
            this.rptEarnAmountList.DataSource = amountDV;
            this.rptEarnAmountList.DataBind();

            DataView couponDV = ds.Tables[0].DefaultView;
            couponDV.RowFilter = "ExchangeType='4'";
            //this.lbtnEarnCouponDel.Enabled = couponDV.Count > 0 ? true : false;
            this.rptEarnCouponList.DataSource = couponDV;
            this.rptEarnCouponList.DataBind();

            DataView amountPointDV = ds.Tables[0].DefaultView;
            amountPointDV.RowFilter = "ExchangeType='3'";
            //this.lbtnEarnAmountPointDel.Enabled = amountPointDV.Count > 0 ? true : false;
            this.rptEarnAmountPointList.DataSource = amountPointDV;
            this.rptEarnAmountPointList.DataBind();
        }
    }
}
