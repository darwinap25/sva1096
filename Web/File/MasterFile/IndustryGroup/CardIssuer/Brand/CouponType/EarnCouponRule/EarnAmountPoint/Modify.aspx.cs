﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType.EarnCouponRule.EarnAmountPoint
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.EarnCouponRule, Edge.SVA.Model.EarnCouponRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindCouponType(CouponTypeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                string CouponTypeID = Request.Params["CouponTypeID"];
                this.CouponTypeID.SelectedValue = CouponTypeID;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            Edge.SVA.Model.EarnCouponRule item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
            }

            if (DALTool.Update<Edge.SVA.BLL.EarnCouponRule>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "../../Modify.aspx?id=" + Request.Params["CouponTypeID"] + "&tabs=6", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "../../Modify.aspx?id=" + Request.Params["CouponTypeID"] + "&tabs=6", Resources.MessageTips.FAILED_TITLE);
            }

        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../../Modify.aspx?id=" + Request.Params["CouponTypeID"] + "&tabs=6");
        }
    }
}
