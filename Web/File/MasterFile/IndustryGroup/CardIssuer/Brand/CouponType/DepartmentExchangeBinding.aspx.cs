﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class DepartmentExchangeBinding : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                BindList();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
            if (couponTypeID == 0)
            {

                if(ChcekModel(DepartCode.Text.Trim(),Edge.Web.Tools.ConvertTool.ToInt(this.BrandID.SelectedValue)))
                {
                     this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.Exists;

                    return;
                }
                
                //if (ChcekDB(this.DepartCode.Text.Trim()))
                //{
                    this.lblMsg.Visible = false;

                    Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                    model.BindingType = 2;
                    model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                    model.DepartCode = DepartCode.Text.Trim();
                    model.CouponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
                    AddModelToSession(model);

                    BindList();

                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.AddSuccess;

                //}
                //else
                //{
                //    this.lblMsg.Visible = true;
                //    this.lblMsg.Text = Resources.MessageTips.NotDepartment;

                //}
              
            }
            else
            {

                //if (ChcekDB(this.DepartCode.Text.Trim()))
                //{
                    this.lblMsg.Visible = false;

                    Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                    model.BindingType = 2;
                    model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                    model.DepartCode = DepartCode.Text.Trim();
                    model.CouponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);

                    Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                    if (bllExchangeBinding.Add(model) > 0)
                    {
                        this.lblMsg.Visible = true;
                        this.lblMsg.Text = Resources.MessageTips.AddSuccess;
                        AddModelToSession(model);
                        BindList();
                    }
                    else 
                    {
                        this.lblMsg.Visible = true;
                        this.lblMsg.Text = Resources.MessageTips.AddFailed;
                    }

                //}
                //else
                //{
                //    this.lblMsg.Visible = true;
                //    this.lblMsg.Text = Resources.MessageTips.NotDepartment;

                //}
            }

        }

        private void BindList()
        {
            this.lblMsg.Visible = false;

            int couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
            if (couponTypeID == 0)
            {
                if (Session["DepartmentCouponTypeExchangeBinding"] == null)
                {
                    Session["DepartmentCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
                }

                List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["DepartmentCouponTypeExchangeBinding"];

                DataSet ds =Edge.Web.Tools.ConvertTool.ToDataSet<Edge.SVA.Model.CouponTypeExchangeBinding>(list);

                Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");

                this.rptList.DataSource = ds;
                this.rptList.DataBind();
            }
            else
            {


                Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                DataSet ds = bll.GetList(" CouponTypeID=" + couponTypeID + " and BindingType=2 and ProdCode is null ");

                Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                this.rptList.DataSource = ds.Tables[0].DefaultView;
                this.rptList.DataBind();
            }

            int view = Request.Params["view"] == null ? 0 : int.Parse(Request.Params["view"]);
            if (view == 1)
            {
                this.btnAdd.Visible = false;
                this.lbtnDel.Visible = false;
            }


        }

        private bool ChcekDB(string departCode)
        {
            Edge.SVA.BLL.Department bllDepartment = new Edge.SVA.BLL.Department();
            return bllDepartment.Exists(departCode);
        }


        private bool ChcekModel(string departCode, int brandID)
        {
            if (Session["DepartmentCouponTypeExchangeBinding"] == null)
            {
                return false;
            }
            else
            {
                List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["DepartmentCouponTypeExchangeBinding"];

                foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in list)
                {
                    if ((model.DepartCode == departCode) && (model.BrandID == brandID))
                    {
                        return true;
                    }
                }

                return false;
            }
        }


        private void AddModelToSession(Edge.SVA.Model.CouponTypeExchangeBinding model)
        {

            if (Session["DepartmentCouponTypeExchangeBinding"] == null)
            {
                Session["DepartmentCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
            }

            List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["DepartmentCouponTypeExchangeBinding"];
            list.Add(model);

            Session["DepartmentCouponTypeExchangeBinding"] = list;
        }

        private void RemoveModelFromSession(int couponTypeID,int brandID, string departCode)
        {

            if (Session["DepartmentCouponTypeExchangeBinding"] == null)
            {
                Session["DepartmentCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
            }

            List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["DepartmentCouponTypeExchangeBinding"];
            foreach (Edge.SVA.Model.CouponTypeExchangeBinding item in list)
            {
                if ((couponTypeID == item.CouponTypeID) && (brandID == item.BrandID) && (departCode == item.DepartCode))
                {
                    list.Remove(item); break;
                }
            }

            Session["DepartmentCouponTypeExchangeBinding"] = list;
        }

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            int couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
            if (couponTypeID == 0)
            {
                for (int i = 0; i < rptList.Items.Count; i++)
                {
                    string departCode = ((Label)rptList.Items[i].FindControl("lblDepartCode")).Text;
                    int brandID = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lblBrandID")).Value);

                    CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                    if (cb.Checked)
                    {
                        RemoveModelFromSession(0, brandID, departCode.Trim());
                    }
                }
            }
            else
            {

                for (int i = 0; i < rptList.Items.Count; i++)
                {
                    int keyID = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
                    string departCode = ((Label)rptList.Items[i].FindControl("lblDepartCode")).Text;
                    int brandID = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lblBrandID")).Value);

                    CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                    if (cb.Checked)
                    {
                        Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                        if (bllExchangeBinding.Delete(keyID))
                        {
                            RemoveModelFromSession(couponTypeID, brandID, departCode.Trim());
                        }
                    }
                }

               

            }

            BindList();

        }
    }
}
