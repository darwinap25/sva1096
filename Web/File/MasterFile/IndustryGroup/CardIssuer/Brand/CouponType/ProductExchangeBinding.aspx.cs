﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CouponType
{
    public partial class ProductExchangeBinding : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                BindList();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
            if (couponTypeID == 0)
            {


                if (ChcekModel(ProdCode.Text.Trim(), Edge.Web.Tools.ConvertTool.ToInt(this.BrandID.SelectedValue)))
                {
                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.Exists;

                    return;
                }

                //if (ChcekDB(this.ProdCode.Text.Trim()))
                //{
                    this.lblMsg.Visible = false;

                    Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                    model.BindingType = 2;
                    model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                    model.ProdCode = ProdCode.Text.Trim();
                    model.CouponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
                    AddModelToSession(model);

                    BindList();

                    this.lblMsg.Visible = true;
                    this.lblMsg.Text = Resources.MessageTips.AddSuccess;

                //}
                //else
                //{
                //    this.lblMsg.Visible = true;
                //    this.lblMsg.Text = Resources.MessageTips.NotProduct;

                //}

            }
            else
            {

                //if (ChcekDB(this.ProdCode.Text.Trim()))
                //{
                    this.lblMsg.Visible = false;

                    Edge.SVA.Model.CouponTypeExchangeBinding model = new Edge.SVA.Model.CouponTypeExchangeBinding();
                    model.BindingType = 2;
                    model.BrandID = Edge.Web.Tools.ConvertTool.ToInt(BrandID.SelectedValue);
                    model.ProdCode = ProdCode.Text.Trim();
                    model.CouponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);

                    Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                    if (bllExchangeBinding.Add(model) > 0)
                    {
                        this.lblMsg.Visible = true;
                        this.lblMsg.Text = Resources.MessageTips.AddSuccess;
                        AddModelToSession(model);
                        BindList();
                    }
                    else
                    {
                        this.lblMsg.Visible = true;
                        this.lblMsg.Text = Resources.MessageTips.AddFailed;
                    }

                //}
                //else
                //{
                //    this.lblMsg.Visible = true;
                //    this.lblMsg.Text = Resources.MessageTips.NotProduct;

                //}
            }

        }

        private void BindList()
        {

            this.lblMsg.Visible = false;

            int couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);

            if (couponTypeID == 0)
            {
                if (Session["ProductCouponTypeExchangeBinding"] == null)
                {
                    Session["ProductCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
                }

                List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];


                DataSet ds = Edge.Web.Tools.ConvertTool.ToDataSet<Edge.SVA.Model.CouponTypeExchangeBinding>(list);

                Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");

                this.rptList.DataSource = ds;
                this.rptList.DataBind();
            }
            else
            {
                Edge.SVA.BLL.CouponTypeExchangeBinding bll = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                DataSet ds = bll.GetList(" CouponTypeID=" + couponTypeID + " and BindingType=2 and DepartCode is null ");

                Edge.Web.Tools.DataTool.AddBrandName(ds, "BrandName", "BrandID");
                this.rptList.DataSource = ds.Tables[0].DefaultView;
                this.rptList.DataBind();
            }

            int view = Request.Params["view"] == null ? 0 : int.Parse(Request.Params["view"]);
            if (view == 1)
            {
                this.btnAdd.Visible = false;
                this.lbtnDel.Visible = false;
            }
        }

        private bool ChcekDB(string prodCode)
        {
            Edge.SVA.BLL.Product bll = new Edge.SVA.BLL.Product();
            return bll.Exists(prodCode);
        }


        private bool ChcekModel(string prodCode, int brandID)
        {
            if (Session["ProductCouponTypeExchangeBinding"] == null)
            {
                return false;
            }
            else
            {
                List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];

                foreach (Edge.SVA.Model.CouponTypeExchangeBinding model in list)
                {
                    if ((model.ProdCode == prodCode) && (model.BrandID == brandID))
                    {
                        return true;
                    }
                }

                return false;
            }
        }


        private void AddModelToSession(Edge.SVA.Model.CouponTypeExchangeBinding model)
        {

            if (Session["ProductCouponTypeExchangeBinding"] == null)
            {
                Session["ProductCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
            }

            List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];
            list.Add(model);

            Session["ProductCouponTypeExchangeBinding"] = list;
        }

        private void RemoveModelFromSession(Edge.SVA.Model.CouponTypeExchangeBinding model)
        {

            if (Session["ProductCouponTypeExchangeBinding"] == null)
            {
                Session["ProductCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
            }

            List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];
            foreach (Edge.SVA.Model.CouponTypeExchangeBinding item in list)
            {
                if ((model.CouponTypeID == item.CouponTypeID) && (model.BrandID== item.BrandID) && (model.ProdCode == item.ProdCode))
                {
                    list.Remove(item); break;
                }
            }

            Session["ProductCouponTypeExchangeBinding"] = list;
        }

        private void RemoveModelFromSession(int couponTypeID, int brandID, string prodCode)
        {

            if (Session["ProductCouponTypeExchangeBinding"] == null)
            {
                Session["ProductCouponTypeExchangeBinding"] = new List<Edge.SVA.Model.CouponTypeExchangeBinding>();
            }

            List<Edge.SVA.Model.CouponTypeExchangeBinding> list = (List<Edge.SVA.Model.CouponTypeExchangeBinding>)Session["ProductCouponTypeExchangeBinding"];
            foreach (Edge.SVA.Model.CouponTypeExchangeBinding item in list)
            {
                if ((couponTypeID == item.CouponTypeID) && (brandID == item.BrandID) && (prodCode == item.ProdCode))
                {
                    list.Remove(item); break;
                }
            }

            Session["ProductCouponTypeExchangeBinding"] = list;
        }



        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            int couponTypeID = Request.Params["CouponTypeID"] == null ? 0 : int.Parse(Request.Params["CouponTypeID"]);
            if (couponTypeID == 0)
            {
                for (int i = 0; i < rptList.Items.Count; i++)
                {
                    string departCode = ((Label)rptList.Items[i].FindControl("lblPordCode")).Text;
                    int brandID = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lblBrandID")).Value);

                    CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                    if (cb.Checked)
                    {
                        RemoveModelFromSession(0, brandID, departCode.Trim());
                    }
                }
            }
            else
            {

                for (int i = 0; i < rptList.Items.Count; i++)
                {
                    int keyID = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
                    string departCode = ((Label)rptList.Items[i].FindControl("lblPordCode")).Text;
                    int brandID = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lblBrandID")).Value);

                    CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                    if (cb.Checked)
                    {
                        Edge.SVA.BLL.CouponTypeExchangeBinding bllExchangeBinding = new Edge.SVA.BLL.CouponTypeExchangeBinding();
                        if (bllExchangeBinding.Delete(keyID))
                        {
                            RemoveModelFromSession(couponTypeID, brandID, departCode.Trim());
                        }
                    }
                }


            }


            BindList();
        }
    }
}
