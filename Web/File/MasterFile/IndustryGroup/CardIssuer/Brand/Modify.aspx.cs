﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Brand, Edge.SVA.Model.Brand>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindIndustry(IndustryID);
                Edge.Web.Tools.ControlTool.BindCardIssuer(CardIssuerID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                List<Edge.SVA.Model.CardIssuer> models = new Edge.SVA.BLL.CardIssuer().GetModelList("");
                if (models == null || models.Count <= 0) return;
                this.CardIssuerID.SelectedValue = models[0].CardIssuerID.ToString() ;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName,"Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.Brand item = this.GetUpdateObject();
            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.BrandCode = item.BrandCode.ToUpper();

            }
            if (Edge.Web.Tools.DALTool.isHasBrandCode(item.BrandCode,item.BrandID))
            {
                JscriptPrint(Resources.MessageTips.ExistBrandCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Brand>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }


    }
}
