﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Campaign, Edge.SVA.Model.Campaign>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Tools.ControlTool.BindBrand(BrandID);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Tools.DALTool.isHasCampaignCode(this.CampaignCode.Text.Trim(), 0))
            {
                this.JscriptPrint(Resources.MessageTips.ExistCampaignCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }

            Edge.SVA.Model.Campaign item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.CampaignCode = item.CampaignCode.ToUpper();
            }



            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Campaign>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
