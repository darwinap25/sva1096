﻿using System;

using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Campaign,Edge.SVA.Model.Campaign>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //Tools.ControlTool.BindBrand(BrandID);
                logger.WriteOperationLog(this.PageName, "Show");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                string text = brand == null? "" : DALTool.GetStringByCulture(brand.BrandName1,brand.BrandName2,brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(text, brand.BrandCode);

                //switch (Model.Status.GetValueOrDefault())
                //{
                //    case 1: this.Status.Text = "创建"; break;
                //    case 2: this.Status.Text = "已发布"; break;
                //    case 3: this.Status.Text = "过期"; break;
                //    case 4: this.Status.Text = "作废"; break;
                //    default: this.Status.Text = ""; break;
                    
                //}
            }
        }
       
    }
}
