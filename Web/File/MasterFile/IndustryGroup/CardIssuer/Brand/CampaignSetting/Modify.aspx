﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CampaignSetting.Modify" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");

                }
            });
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                活动编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="CampaignCode" TabIndex="1" runat="server" MaxLength="20" CssClass="input required svaCode" hinttitle="活动编号" hintinfo="Translate__Special_121_StartKey identifier of Campaign. 1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                描述：
            </td>
            <td>
                <asp:TextBox ID="CampaignName1" TabIndex="2" runat="server" MaxLength="512" CssClass="input required" hinttitle="描述" hintinfo="请输入规范的活動名称。不能超過512個字符"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:TextBox ID="CampaignName2" TabIndex="3" runat="server" MaxLength="512" CssClass="input " hinttitle="其他描述1" hintinfo="對活動的描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:TextBox ID="CampaignName3" TabIndex="4" runat="server" MaxLength="512" CssClass="input " hinttitle="其他描述2" hintinfo="對活動的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                详细描述：
            </td>
            <td>
                <asp:TextBox ID="CampaignDetail1" TabIndex="5" runat="server" MaxLength="512" hinttitle="详细描述" hintinfo="请输入规范的活動詳細內容的名称。不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他详细描述1：
            </td>
            <td>
                <asp:TextBox ID="CampaignDetail2" TabIndex="6" runat="server" MaxLength="512" hinttitle="其他详细描述1" hintinfo="對活動詳細內容的描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他详细描述2：
            </td>
            <td>
                <asp:TextBox ID="CampaignDetail3" TabIndex="7" runat="server" MaxLength="512" hinttitle="其他详细描述2" hintinfo="對活動詳細內容的另一個描述,不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                品牌：
            </td>
            <td>
                <asp:DropDownList ID="BrandID" runat="server" TabIndex="8" SkinID="NoClass" CssClass="dropdownlist required">
                </asp:DropDownList><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="CampaignPicFile" tabIndex="9" runat="server" SubSaveFilePath="Images/Campaign"  hinttitle="图片" hintinfo="Translate__Special_121_Start點擊按鈕進行上傳，上傳的文件支持JPG,GIF，PNG，BMP,文件大小不能超過10240KBTranslate__Special_121_End"/>
            </td>
        </tr>
        <tr>
            <td align="right">
                活动启用日期：
            </td>
            <td>
                <asp:TextBox ID="StartDate" TabIndex="10" runat="server" MaxLength="512" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'EndDate\',{d:0});}'})"
                    CssClass="input required dateISO" hinttitle="活动启用日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                活动结束日期：
            </td>
            <td>
                <asp:TextBox ID="EndDate" TabIndex="11" runat="server" MaxLength="512" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'StartDate\',{d:0});}'})"
                    CssClass="input required dateISO" hinttitle="活动结束日期" hintinfo="日期格式：YYYY-MM-DD"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnUpdate" runat="server" Text="提 交" OnClick="btnUpdate_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2" >
                *为必填项
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
