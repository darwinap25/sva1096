﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify</title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSPaginationPath() %>'></script>
    <link rel="stylesheet" type="text/css" href='<%#GetPaginationCssPath() %>' />
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
    <script type="text/javascript">
        $(function () {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function (label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });

            tabs(1);

            checkIsAllowStoreValue();

            checkIsImportCardNumber();

        });

        //是否允许充值
        function checkIsAllowStoreValue() {
            var method = $("input[name='IsAllowStoreValue']:checked").val();
            if (method == '0') {
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").val("");
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").attr('disabled', ' true');
                $(".storeValue").hide();
            }
            else {
                $("#CardGradeMaxAmount,#MinAmountPreAdd,#MaxAmountPreAdd").removeAttr('disabled');
                $(".storeValue").show();
            }
        }
        $(function () {
            var value = $("#CardPointToAmountRate").val();
            if (value == null || value == undefined || value.length <= 0) return;
            var reg = /(\.\d{1,2})?$/gi;
            value = value.replace(reg, "");
            $("#CardPointToAmountRate").val(value)
        });

        //是否导入优惠券
        function checkIsImportCardNumber() {
            var method = $("input[name='IsImportUIDNumber']:checked").val();
            if (method == '1') { //导入
                $("#msgtable tr[class=ManualRoles]").hide();
                $("#msgtable tr[class=ImportRoles]").show();
                if ($("#UIDToCardNumber").val() != "0") $("#CardNumMaskImportTR,#CardNumPatternImportTR").hide();
              
            }
            else {//手动

                $("#msgtable tr[class=ManualRoles]").show();
                $("#msgtable tr[class=ImportRoles]").hide();

                checkCheckDigitMode();
              
            }
            $("#IsImportUIDNumber,#UIDToCardNumber,#CardCheckdigit").hide();
        }


        // CheckDigitMode
        function checkCheckDigitMode() {
            var isCardCheckdigit = $("input[name='CardCheckdigit']:checked").val();

            if (isCardCheckdigit == '0') //没有Check
            {

                $("#CheckDigitModeTR").hide();
            }
            else {
                $("#CheckDigitModeTR").show();
            }
        }
      
    </script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <script type="text/javascript">
         $(function() {
              $(".liTabs").click(function() {parent.jAlert("<%=Edge.Messages.Manager.MessagesTool.instance.GetMessage("10028")%>", "<%=Resources.MessageTips.WARNING_TITLE%>");});
             });
    </script>
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <div id="tabs">
        <ul>
            <li onclick="tabs(1);"><a><span>基本信息</span></a></li>
            <li class="liTabs"><a><span>有效期规则</span></a></li>
            <li class="liTabs"><a><span>获取积分规则</span></a></li>
        </ul>
    </div>
    <div class="fragment">
        <table id="msgtable" width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    基本信息
                </th>
            </tr>
            <tr>
                <td width="25%" align="right">
                    卡级别编号：
                </td>
                <td width="75%">
                    <asp:Label ID="CardGradeCode" TabIndex="1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    描述：
                </td>
                <td>
                    <asp:Label ID="CardGradeName1" TabIndex="2" runat="server" MaxLength="512"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述1：
                </td>
                <td>
                    <asp:Label ID="CardGradeName2" TabIndex="3" runat="server" MaxLength="512"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    其他描述2：
                </td>
                <td>
                    <asp:Label ID="CardGradeName3" TabIndex="4" runat="server" MaxLength="512"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡类型：
                </td>
                <td>
                    <asp:Label ID="CardTypeID" TabIndex="5" runat="server">  </asp:Label>
                   
                </td>
            </tr>
            <tr>
                <td align="right">
                    条款条例：
                </td>
                <td>
                    <asp:Label ID="CardGradeNotes" TabIndex="6" runat="server" TextMode="MultiLine" Height="100"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡图片：
                </td>
                <td>
                    <ufb:UploadFileBox ID="CardGradePicFile" TabIndex="7" runat="server" SubSaveFilePath="Images/CardGrade"
                        FileType="preview" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡设计模板：
                </td>
                <td>
                    <asp:Label ID="CardGradeLayoutFile" TabIndex="8" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡结算单模板：
                </td>
                <td>
                    <asp:Label ID="CardGradeStatementFile" TabIndex="9" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    密码规则：
                </td>
                <td>
                    <asp:Label ID="PasswordRuleID" runat="server"></asp:Label>
                   
                </td>
            </tr>
            <tr>
                <td align="right">
                    活动：
                </td>
                <td>
                    <asp:Label ID="CampaignID" runat="server"></asp:Label>
                    
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡级别等级：
                </td>
                <td>
                    <asp:Label ID="CardGradeRank" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    卡号规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    卡号码是否导入：
                </td>
                <td>
                    <asp:Label ID="IsImportUIDNumberView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsImportUIDNumber" runat="server" CssClass="required" TabIndex="7"
                        RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <th colspan="2" align="left">
                    手动创建编号规则
                </th>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    卡号编码规则：
                </td>
                <td>
                    <asp:Label ID="CardNumMask" TabIndex="6" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    卡号前缀号码：
                </td>
                <td>
                    <asp:Label ID="CardNumPattern" TabIndex="7" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    卡号是否添加校验位：
                </td>
                <td>
                    <asp:Label ID="CardCheckdigitView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="CardCheckdigit" runat="server" TabIndex="13" Enabled="false"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="CheckDigitModeTR" class="ManualRoles">
                <td align="right">
                    校验位计算方法：
                </td>
                <td>
                    <asp:Label ID="CheckDigitModeIDView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CheckDigitModeID" runat="server" TabIndex="11" CssClass="dropdownlist"
                        SkinID="NoClass" Enabled="false">
                        <asp:ListItem Value="1" Text="EAN13"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ManualRoles">
                <td align="right">
                    Translate__Special_121_Start 是否复制卡号到卡物理编号（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="CardNumberToUIDView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CardNumberToUID" runat="server" TabIndex="11" CssClass="dropdownlist"
                        SkinID="NoClass" Enabled="false">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <th colspan="2" align="left">
                    导入物理编号规则
                </th>
            </tr>
            <tr class="ImportRoles" id="CardNumMaskImportTR">
                <td align="right">
                    卡号编码规则：
                </td>
                <td>
                    <asp:Label ID="CardNumMaskImport" TabIndex="6" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ImportRoles" id="CardNumPatternImportTR">
                <td align="right">
                    卡号前缀号码：
                </td>
                <td>
                    <asp:Label ID="CardNumPatternImport" TabIndex="7" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否连续（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="IsConsecutiveUIDView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsConsecutiveUID" runat="server" CssClass="required" TabIndex="11"
                        RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否包含校验位（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="UIDCheckDigitView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="UIDCheckDigit" runat="server" CssClass="required" TabIndex="11"
                        RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="ImportRoles">
                <td align="right">
                    Translate__Special_121_Start 是否复制卡物理编号到卡号（是/否）：Translate__Special_121_End
                </td>
                <td>
                    <asp:Label ID="UIDToCardNumberView" runat="server"></asp:Label>
                    <asp:DropDownList ID="UIDToCardNumber" runat="server" TabIndex="11" CssClass="dropdownlist"
                        SkinID="NoClass" Enabled="false">
                        <asp:ListItem Value="1">全部复制</asp:ListItem>
                        <asp:ListItem Value="0">绑定</asp:ListItem>
                        <asp:ListItem Value="2">复制去掉校验位</asp:ListItem>
                        <asp:ListItem Value="3">复制加上校验位</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    使用规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    发行品牌/店铺：
                </td>
                <td>
                    <%-- <asp:TextBox ID="EarnStoreCondition" TabIndex="18" runat="server" Enabled="false"
                    CssClass="input"></asp:TextBox>--%>
                    <%--       <a id="store1" enableviewstate="false" class="thickbox btn_bg" href="../../BrandLocationStore.aspx?Url=&id=<%=Request.QueryString["id"].ToString()%>&type=2&storetype=1&view=1&height=560&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        查看</a>--%>
                    <a id="issueBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        品牌</a> <a id="issueStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=1&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                            店铺</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    使用品牌/店铺：
                </td>
                <td>
                    <%-- <asp:TextBox ID="ConsumeStoreCondition" TabIndex="19" runat="server" Enabled="false"
                    CssClass="input"></asp:TextBox>--%>
                    <%--  <a id="store2" enableviewstate="false" class="thickbox btn_bg" href="../../BrandLocationStore.aspx?Url=&id=<%=Request.QueryString["id"].ToString()%>&type=2&storetype=2&view=1&height=560&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        查看</a>--%>
                    <a id="useBrand" class="thickbox btn_bg" href="Brand/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                        品牌</a>
                    <%--<a id="useLocation" class="thickbox btn_bg" href="Location/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                                区域</a> --%><a id="useStore" class="thickbox btn_bg" href="Store/List.aspx?Url=&CardGradeID=<%=Request.QueryString["id"].ToString()%>&type=1&StoreConditionTypeID=2&height=500&amp;width=800&amp;TB_iframe=True&amp;keepThis=False">
                                    店铺</a>
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否允许储值：
                </td>
                <td>
                    <asp:Label ID="IsAllowStoreValueView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsAllowStoreValue" TabIndex="10" runat="server" Enabled="false"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="是"></asp:ListItem>
                        <asp:ListItem Value="0" Text="否"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    是否允许积分：
                </td>
                <td>
                    <asp:Label ID="IsAllowConsumptionPointView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="IsAllowConsumptionPoint" TabIndex="11" runat="server" CssClass="required"
                        Enabled="false" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="是"></asp:ListItem>
                        <asp:ListItem Value="0" Text="否"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    初始金额：
                </td>
                <td>
                    <asp:Label ID="CardTypeInitAmount" TabIndex="11" runat="server" MaxLength="20"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    初始积分：
                </td>
                <td>
                    <asp:Label ID="CardTypeInitPoints" TabIndex="12" runat="server" MaxLength="20"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡最大現金賬套金額：
                </td>
                <td>
                    <asp:Label ID="CardGradeMaxAmount" TabIndex="13" runat="server" MaxLength="512"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    卡最大积分帐套积分：
                </td>
                <td>
                    <asp:Label ID="CardGradeMaxPoint" TabIndex="13" runat="server" MaxLength="512"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最低充值现金帐套金额：
                </td>
                <td>
                    <asp:Label ID="MinAmountPreAdd" TabIndex="14" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最低充值积分帐套积分：
                </td>
                <td>
                    <asp:Label ID="MinPointPreAdd" TabIndex="14" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最高充值现金帐套金额：
                </td>
                <td>
                    <asp:Label ID="MaxAmountPreAdd" TabIndex="15" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最高充值积分帐套积分：
                </td>
                <td>
                    <asp:Label ID="MaxPointPreAdd" TabIndex="15" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    折扣最高值：
                </td>
                <td>
                    <asp:Label ID="CardGradeDiscCeiling" TabIndex="16" runat="server" MaxLength="20"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小消费积分（每次）：
                </td>
                <td>
                    <asp:Label ID="CardConsumeBasePoint" TabIndex="17" runat="server" MaxLength="20"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小剩余积分：
                </td>
                <td>
                    <asp:Label ID="MinBalancePoint" TabIndex="18" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小消费金额（每次）：
                </td>
                <td>
                    <asp:Label ID="MinConsumeAmount" TabIndex="18" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    最小剩余金额：
                </td>
                <td>
                    <asp:Label ID="MinBalanceAmount" TabIndex="18" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    过期后是否清空帐套金额：
                </td>
                <td>
                    <asp:Label ID="ForfeitAmountAfterExpiredView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="ForfeitAmountAfterExpired" TabIndex="18" runat="server"
                        Enabled="false" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">不清零</asp:ListItem>
                        <asp:ListItem Value="1">清零</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    过期后是否清空帐套积分：
                </td>
                <td>
                    <asp:Label ID="ForfeitPointAfterExpiredView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="ForfeitPointAfterExpired" TabIndex="18" runat="server" Enabled="false"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">不清零</asp:ListItem>
                        <asp:ListItem Value="1">清零</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    升级规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    升级条件：
                </td>
                <td>
                    <asp:Label ID="CardGradeUpdMethodView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CardGradeUpdMethod" TabIndex="19" runat="server" Enabled="false">
                        <asp:ListItem Selected="True" Value=""></asp:ListItem>
                        <asp:ListItem Value="0" title="无">无</asp:ListItem>
                        <asp:ListItem Value="3" title="单笔消费">单笔消费</asp:ListItem>
                        <asp:ListItem Value="1" title="年累积购买值大于">年累积购买值大于</asp:ListItem>
                        <asp:ListItem Value="2" title="年累计积分值大于">年累计积分值大于</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    界限值：
                </td>
                <td>
                    <asp:Label ID="CardGradeUpdThreshold" TabIndex="20" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    有效期规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    <div align="right">
                        有效期长度：</div>
                </td>
                <td>
                    <asp:Label ID="CardValidityDuration" TabIndex="21" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    有效期长度单位：
                </td>
                <td>
                    <asp:Label ID="CardValidityUnitView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CardValidityUnit" TabIndex="2" runat="server" Enabled="false">
                        <asp:ListItem Value="0" Selected="True">永久</asp:ListItem>
                        <asp:ListItem Value="1">年</asp:ListItem>
                        <asp:ListItem Value="2">月</asp:ListItem>
                        <asp:ListItem Value="3">星期</asp:ListItem>
                        <asp:ListItem Value="4">天</asp:ListItem>
                        <asp:ListItem Value="5">有效期不变更</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    激活是否重置有效期：
                </td>
                <td>
                    <asp:Label ID="ActiveResetExpiryDateView" runat="server"></asp:Label>
                    <asp:RadioButtonList ID="ActiveResetExpiryDate" runat="server" TabIndex="3" CssClass="required"
                        Enabled="false" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <div align="right">
                        宽限期值：</div>
                </td>
                <td>
                    <asp:Label ID="GracePeriodValue" TabIndex="23" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    宽限期单位：
                </td>
                <td>
                    <asp:Label ID="GracePeriodUnitView" runat="server"></asp:Label>
                    <asp:DropDownList ID="GracePeriodUnit" TabIndex="23" runat="server" Enabled="false">
                        <%--<asp:ListItem Value="0" Selected="True">永久</asp:ListItem>--%>
                        <asp:ListItem Value="1">年</asp:ListItem>
                        <asp:ListItem Value="2">月</asp:ListItem>
                        <asp:ListItem Value="3">星期</asp:ListItem>
                        <asp:ListItem Value="4">天</asp:ListItem>
                        <%--<asp:ListItem Value="5">有效期不变更</asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <th colspan="2" align="left">
                    转赠/转换规则
                </th>
            </tr>
            <tr>
                <td align="right">
                    积分转换现金规则：
                </td>
                <td>
                    <asp:Label ID="CardPointToAmountRate" TabIndex="22" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    现金转换积分规则：
                </td>
                <td>
                    <asp:Label ID="CardAmountToPointRate" TabIndex="23" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    积分转换规则（转出）：
                </td>
                <td>
                    <asp:Label ID="CardPointTransferView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CardPointTransfer" TabIndex="24" runat="server" Enabled="false">
                        <asp:ListItem Selected="True" Value="0">不允许</asp:ListItem>
                        <asp:ListItem Value="1">允许同卡级别</asp:ListItem>
                        <asp:ListItem Value="2">允许同卡类型</asp:ListItem>
                        <asp:ListItem Value="3">允许同品牌</asp:ListItem>
                        <asp:ListItem Value="4">允许同发行商</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最小转换积分：
                </td>
                <td>
                    <asp:Label ID="MinPointPreTransfer" TabIndex="25" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最大转换积分：
                </td>
                <td>
                    <asp:Label ID="MaxPointPreTransfer" TabIndex="26" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每天最大转赠积分：
                </td>
                <td>
                    <asp:Label ID="DayMaxPointTransfer" TabIndex="27" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    现金转换规则：
                </td>
                <td>
                    <asp:Label ID="CardAmountTransferView" runat="server"></asp:Label>
                    <asp:DropDownList ID="CardAmountTransfer" TabIndex="28" runat="server" Enabled="false">
                        <asp:ListItem Selected="True" Value="0">不允许</asp:ListItem>
                        <asp:ListItem Value="1">允许同卡级别</asp:ListItem>
                        <asp:ListItem Value="2">允许同卡类型</asp:ListItem>
                        <asp:ListItem Value="3">允许同品牌</asp:ListItem>
                        <asp:ListItem Value="4">允许同发行商</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最小转换金额：
                </td>
                <td>
                    <asp:Label ID="MinAmountPreTransfer" TabIndex="29" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每笔交易最大转换金额：
                </td>
                <td>
                    <asp:Label ID="MaxAmountPreTransfer" TabIndex="30" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    每天最大转赠金额：
                </td>
                <td>
                    <asp:Label ID="DayMaxAmountTransfer" TabIndex="31" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="javascript:history.back();"
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    有效期规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptExtensionRuleList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgRept">
                                <tr>
                                    <th width="6%">
                                        编号
                                    </th>
                                    <th width="10%">
                                        规则类型
                                    </th>
                                    <th width="10%">
                                        规则记录序号
                                    </th>
                                    <th width="10%">
                                        开始日期
                                    </th>
                                    <th width="10%">
                                        结束日期
                                    </th>
                                    <th width="10%">
                                        状态
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("ExtensionRuleID")%>' Visible="false"></asp:Label><a
                                        href="CardExtensionRule/show.aspx?id=<%#Eval("ExtensionRuleID") %>"><asp:Label ID="lbCardTypeCode"
                                            runat="server" Text='<%#Eval("ExtensionRuleID")%>'></asp:Label></a>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("RuleType")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("ExtensionRuleSeqNo")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fragment">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
            <tr>
                <th colspan="2" align="left">
                    获取积分规则
                </th>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Repeater ID="rptPointRuleList" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgRept">
                                <tr>
                                    <th width="6%">
                                        编号
                                    </th>
                                    <th width="10%">
                                        记录序号
                                    </th>
                                    <th width="10%">
                                        计算方式
                                    </th>
                                    <th width="10%">
                                        开始日期
                                    </th>
                                    <th width="10%">
                                        结束日期
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lb_id" runat="server" Text='<%#Eval("PointRuleID")%>' Visible="false"></asp:Label><a
                                        href="PointRule/show.aspx?id=<%#Eval("PointRuleID") %>"><asp:Label ID="lblPointRuleID"
                                            runat="server" Text='<%#Eval("PointRuleID")%>'></asp:Label></a>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRuleType" runat="server" Text='<%#Eval("PointRuleSeqNo")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblExtensionRuleSeqNo" runat="server" Text='<%#Eval("PointRuleOper")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#Eval("EndDate")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="spClear">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div align="center">
                        <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                            class="submit" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <script type="text/javascript">

        $(function () {
            $(".msgRept tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgRept tr").hover(
			    function () {
			        $(this).addClass("tr_hover_col");
			    },
			    function () {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
