﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardGrade, Edge.SVA.Model.CardGrade>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            int tabNum = string.IsNullOrEmpty(Request.Params["tabs"]) ? 1 : int.Parse(Request.Params["tabs"]);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "tabs", "<script type='text/javascript'> $(function() {tabs(" + tabNum + "); });</script>");
            
            if (!this.IsPostBack)
            {


                Edge.Web.Tools.ControlTool.BindCardType(CardTypeID);
                Edge.Web.Tools.ControlTool.BindCampaign(this.CampaignID);
                Edge.Web.Tools.ControlTool.BindPasswordRule(this.PasswordRuleID);


                CardExtensionRuleRptBind("CardGradeID=" + Request.Params["id"]);
                CardPointRuleRptBind("CardGradeID=" + Request.Params["id"]);

              
                this.lbtnDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                this.lbtnPointRuleDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";

            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                this.CardPointToAmountRate.Text = ((int)Model.CardPointToAmountRate.GetValueOrDefault()).ToString("N00");       //todo: 数据库类型不正确
                this.CardGradeDiscCeiling.Text = ((int)(Model.CardGradeDiscCeiling.GetValueOrDefault() * 100)).ToString();

                string msg = "";
              

                if (DALTool.isCardGradeCreatedCard(Model.CardGradeID, ref msg))//已经创建卡
                {
                    IsImportUIDNumber.Enabled = false;
                    this.CardNumMask.Enabled = false;
                    this.CardNumPattern.Enabled = false;
                    this.CardCheckdigit.Enabled = false;
                    this.CardNumberToUID.Enabled = false;
                    this.CheckDigitModeID.Enabled = false;
                    this.IsConsecutiveUID.Enabled = false;
                    this.UIDCheckDigit.Enabled = false;
                    this.UIDToCardNumber.Enabled = false;


                    CardGradeCode.Enabled = false;
                    IsImportUIDNumber.Enabled = false;
                    IsAllowStoreValue.Enabled = false;
                    IsAllowConsumptionPoint.Enabled = false;
                    CardTypeInitAmount.Enabled = false;
                    CardTypeInitPoints.Enabled = false;
                    CardGradeMaxAmount.Enabled = false;
                    CardGradeMaxPoint.Enabled = false;
                    MinAmountPreAdd.Enabled = false;
                    MinPointPreAdd.Enabled = false;
                    MaxAmountPreAdd.Enabled = false;
                    MaxPointPreAdd.Enabled = false;
                    CardGradeDiscCeiling.Enabled = false;
                    CardConsumeBasePoint.Enabled = false;
                    MinBalancePoint.Enabled = false;
                    MinConsumeAmount.Enabled = false;
                    MinBalanceAmount.Enabled = false;
                    ForfeitAmountAfterExpired.Enabled = false;
                    ForfeitPointAfterExpired.Enabled = false;
                    CardGradeUpdMethod.Enabled = false;
                    CardGradeUpdThreshold.Enabled = false;
                    CardValidityDuration.Enabled = false;
                    CardValidityUnit.Enabled = false;
                    ActiveResetExpiryDate.Enabled = false;
                    GracePeriodValue.Enabled = false;
                    GracePeriodUnit.Enabled = false;
                    CardPointToAmountRate.Enabled = false;
                    CardAmountToPointRate.Enabled = false;
                    CardPointTransfer.Enabled = false;
                    MinPointPreTransfer.Enabled = false;
                    MaxPointPreTransfer.Enabled = false;
                    DayMaxPointTransfer.Enabled = false;
                    CardAmountTransfer.Enabled = false;
                    MinAmountPreTransfer.Enabled = false;
                    MaxAmountPreTransfer.Enabled = false;
                    DayMaxAmountTransfer.Enabled = false;
                }
                else//未创建卡
                {
                    if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)//手动
                    {
                        if (!string.IsNullOrEmpty(CardNumMask.Text.Trim())) //号码规则不为空
                        {
                            Edge.SVA.Model.CardType type = new Edge.SVA.BLL.CardType().GetModel(Model.CardTypeID);
                            if (type != null)
                            {
                                if ((type.CardNumMask == Model.CardNumMask) && (type.CardNumPattern == Model.CardNumPattern))//号码规则继承CardType
                                {
                                    this.CardNumMask.Enabled = false;
                                    this.CardNumPattern.Enabled = false;
                                    this.CardCheckdigit.Enabled = false;
                                    this.CardNumberToUID.Enabled = false;
                                    this.CheckDigitModeID.Enabled = false;
                                }
                            }

                            IsImportUIDNumber.Enabled = false;
                        }
                    }
                    else//导入
                    {
                        if (Model.UIDToCardNumber.GetValueOrDefault() == 0)
                        {
                            Edge.SVA.Model.CardType type = new Edge.SVA.BLL.CardType().GetModel(Model.CardTypeID);
                            if (type != null)
                            {
                                if ((type.CardNumMask == Model.CardNumMask) && (type.CardNumPattern == Model.CardNumPattern))//号码规则继承CardType
                                {
                                    this.CardNumMaskImport.Enabled = false;
                                    this.CardNumPatternImport.Enabled = false;
                                }
                            }
                        }
                        this.IsConsecutiveUID.Enabled = false;
                        this.UIDCheckDigit.Enabled = false;
                        this.UIDToCardNumber.Enabled = false;
                        IsImportUIDNumber.Enabled = false;
                    }
                }

                this.CardNumMaskImport.Text = Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0 ? Model.CardNumMask : "";
                this.CardNumPatternImport.Text = Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0 ? Model.CardNumPattern : "";


                //Html处理
                this.CardGradeNotes.Text = Edge.Common.Utils.ToTxt(Model.CardGradeNotes);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.CardGrade item = this.GetUpdateObject();

            if (item == null)
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);               
                return;
            }

            if (Edge.Web.Tools.DALTool.isHasCardGradeCode(item.CardGradeCode.Trim(), item.CardGradeID))
            {
                JscriptPrint(Resources.MessageTips.ExistCardGradeCode, "", Resources.MessageTips.WARNING_TITLE);
                this.CardGradeCode.Focus();
                return;
            }


            if (Edge.Web.Tools.DALTool.isHasCardGradeRank(this.CardGradeRank.Text.Trim(), item.CardTypeID, item.CardGradeID))
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistCardGradeRank, "", Resources.MessageTips.WARNING_TITLE, CardGradeRank.ClientID);
                return;
            }

  
            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardGradeDiscCeiling = string.IsNullOrEmpty(this.CardGradeDiscCeiling.Text) ? 0 : decimal.Parse(this.CardGradeDiscCeiling.Text.Trim()) / 100;
            item.CardGradeCode = item.CardGradeCode.ToUpper();
            item.CardNumMask = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.Trim() : item.CardNumMask;
            item.CardNumPattern = item.IsImportUIDNumber.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.Trim() : item.CardNumPattern;
            item.CardNumMask = item.CardNumMask.ToUpper();

            if (item.IsImportUIDNumber.GetValueOrDefault() == 0)
            {
                if (Tools.DALTool.isHasCardCardeCardNumMask(item.CardNumMask, item.CardNumPattern, 0, item.CardTypeID))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                    return;
                }

                if ((Tools.DALTool.isEmptyCardNumMaskWithCardType(item.CardTypeID)) && string.IsNullOrEmpty(this.CardNumMask.Text.Trim()))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90293"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                    return;
                }

                //if ((this.CardNumMask.Enabled == true) && (this.CardNumPattern.Enabled == true))//如果不是继承
                //{
                    if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 1, item.CardGradeID))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                        return;
                    }
               // }
            }
            else if (item.IsImportUIDNumber.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 0)
            {
                if (Tools.DALTool.isHasCardCardeCardNumMask(item.CardNumMask, item.CardNumPattern, 0, item.CardTypeID))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                    return;
                }

                if ((Tools.DALTool.isEmptyCardNumMaskWithCardType(item.CardTypeID)) && string.IsNullOrEmpty(item.CardNumMask))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90293"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                    return;
                }

                //if ((this.CardNumMask.Enabled == true) && (this.CardNumPattern.Enabled == true))//如果不是继承
                //{
                    if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 1, item.CardGradeID))
                    {
                        this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                        return;
                    }
               // }

            }
            //Html处理
            item.CardGradeNotes = Edge.Common.Utils.ToHtml(this.CardGradeNotes.Text);
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardGrade>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }

        #region CardExtensionRule操作
        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptExtensionRuleList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptExtensionRuleList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptExtensionRuleList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};",id.ToString());
                }
            }
            Response.Redirect("CardExtensionRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
        }

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("CardExtensionRule/Add.aspx?CardGradeID=" + Request.Params["id"]);
        }

        private void CardExtensionRuleRptBind(string strWhere)
        {

            Edge.SVA.BLL.CardExtensionRule bll = new Edge.SVA.BLL.CardExtensionRule();

            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            if (ds.Tables[0].Rows.Count > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            this.rptExtensionRuleList.DataSource = ds.Tables[0].DefaultView;
            this.rptExtensionRuleList.DataBind();
        }
        #endregion


        #region PointRule 操作
        protected void lbtnPointRuleDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptPointRuleList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptPointRuleList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptPointRuleList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("PointRule/Delete.aspx?id=" + Request.Params["id"] + "&ids=" + ids);
            
        }

        protected void lbtnPointRuleAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("PointRule/Add.aspx?CardGradeID=" + Request.Params["id"]);
        }

        private void CardPointRuleRptBind(string strWhere)
        {

            Edge.SVA.BLL.PointRule bll = new Edge.SVA.BLL.PointRule();

 
            DataSet ds = new DataSet();
            ds = bll.GetList(strWhere);

            if (ds.Tables[0].Rows.Count > 0)
            {
                this.lbtnPointRuleDel.Enabled = true;
            }
            else
            {
                this.lbtnPointRuleDel.Enabled = false;
            }
            this.rptPointRuleList.DataSource = ds.Tables[0].DefaultView;
            this.rptPointRuleList.DataBind();
        }
        #endregion


    }
}
