﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade.CardExtensionRule.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：显示卡有效期规则</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                显示卡有效期规则
            </th>
        </tr>
 <%--       <tr>
            <td  align="right">
                ID：
            </td>
            <td >
                <asp:Label ID="ExtensionRuleID" runat="server"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td width="25%" align="right">
                规则类型：
            </td>
            <td width="75%">
                <asp:Label ID="RuleType" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡类型：
            </td>
            <td>
                <asp:DropDownList ID="CardTypeID" TabIndex="2" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                卡等级：
            </td>
            <td>
                <asp:DropDownList ID="CardGradeID" TabIndex="3" runat="server" CssClass="dropdownlist"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                规则记录序号：
            </td>
            <td>
                <asp:Label ID="ExtensionRuleSeqNo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                最大增加数：
            </td>
            <td>
                <asp:Label ID="MaxLimit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                增值金额：
            </td>
            <td>
                <asp:Label ID="RuleAmount" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                延长日期：
            </td>
            <td>
                <asp:Label ID="Extension" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                有效期延长单位：
            </td>
            <td>
                <asp:Label ID="ExtensionUnit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                开始日期：
            </td>
            <td>
                <asp:Label ID="StartDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                结束日期：
            </td>
            <td>
                <asp:Label ID="EndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                状态：
            </td>
            <td>
                <asp:Label ID="Status" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="249px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="history.back(); " class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
