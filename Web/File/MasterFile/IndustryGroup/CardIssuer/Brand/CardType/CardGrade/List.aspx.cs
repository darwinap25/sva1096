﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.CardGrade
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;
         
            if (!Page.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "List");
                this.lbtnDel.OnClientClick = "return checkAndConfirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                RptBind("CardGradeID>0", "CardGradeCode");
            }
        }

        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.CardGrade bll = new Edge.SVA.BLL.CardGrade();

            //获得总条数
            this.pcount = bll.GetCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);

            AddNames(ds);
            Tools.DataTool.AddBrandCodeByCardType(ds, "BrandCode", "CardTypeID");
            Tools.DataTool.AddCardTypeCode(ds, "CardTypeCode", "CardTypeID");
           
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }

        
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("Delete.aspx?ids=" + ids);


        }

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }

        private static void AddNames(DataSet ds)
        {
            Edge.Web.Tools.DataTool.AddCardTypeName(ds, "CardTypeName", "CardTypeID");

            ds.Tables[0].Columns.Add(new DataColumn("BrandName", typeof(string)));
            ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));

            Edge.SVA.BLL.CardType bllCardType = new Edge.SVA.BLL.CardType();
            Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
            Dictionary<int, string> brandCache = new Dictionary<int, string>();
            Dictionary<int, string> cardIssuerCache = new Dictionary<int, string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Edge.SVA.Model.CardType mCardType = bllCardType.GetModel(int.Parse(row["CardTypeID"].ToString()));
                row["BrandName"] = Edge.Web.Tools.DALTool.GetBrandName(mCardType.BrandID, brandCache);

                Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(mCardType.BrandID);
                row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetCardIssuerName(mBrand.CardIssuerID.GetValueOrDefault(), cardIssuerCache);
            }
        }
    }
}
