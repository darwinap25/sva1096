﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType.AccountType.List" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script src='<%#ResolveUrl("~/js/jquery-1.4.1.min.js") %>' type="text/javascript"></script>

    <script src='<%#ResolveUrl("~/js/jquery.pagination.js") %>' type="text/javascript"></script>

    <script src='<%#ResolveUrl("~/js/function.js") %>' type="text/javascript"></script>
    <link href='<%#ResolveUrl("~/Style/pagination.css") %>' rel="stylesheet" type="text/css" />
</head>
<body style="padding:10px;">
<%--    <form id="form1" runat="server">
   <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：店铺类型管理</b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <asp:Repeater ID="rptList" runat="server">
    <HeaderTemplate>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
      <tr>
        <th width="6%">选择</th>
        <th width="6%">编号</th>
        <th width="10%">英文名称</th>
        <th width="10%">简体中文名称</th>
        <th width="10%">繁体中文名称</th>
         <th width="13%">创建时间</th>
        <th width="13%">修改时间</th>
        <th width="10%">操作</th>
      </tr>
      </HeaderTemplate>
      <ItemTemplate>
      <tr>
        <td align="center"><asp:CheckBox ID="cb_id" CssClass="checkall" runat="server" /></td>
        <td align="center"><a href="show.aspx?id=<%#Eval("CardTypeID") %>"><asp:Label ID="lb_id" runat="server" Text='<%#Eval("CardTypeID")%>'></asp:Label></a></td>
        <td align="center"><asp:Label ID="lblIndustryName1" runat="server" Text='<%#Eval("CardTypeSN1")%>'></asp:Label></td>
        <td align="center"><asp:Label ID="lblIndustryName2" runat="server" Text='<%#Eval("CardTypeSN2")%>'></asp:Label></td>
        <td align="center"><asp:Label ID="lblIndustryName3" runat="server" Text='<%#Eval("CardTypeSN3")%>'></asp:Label></td>
        <td align="center"><asp:Label ID="lblCreatedOn" runat="server" Text='<%#Eval("CreatedOn")%>'></asp:Label></td>
        <td align="center"><asp:Label ID="lblUpdatedOn" runat="server" Text='<%#Eval("UpdatedOn")%>'></asp:Label></td>
        <td align="center"><span class="btn_bg"> <a href="modify.aspx?id=<%#Eval("CardTypeID") %>">修改</a></span></td>
      </tr>
      </ItemTemplate>
      <FooterTemplate>
      </table>
      </FooterTemplate>
      </asp:Repeater>
        <div class="spClear"></div>
    <div style="line-height:30px;height:30px;">
    <div id="Pagination" class="right flickr"></div>
    <div class="left">
        <span class="btn_all" onclick="checkAll(this);">全选</span>
        <span class="btn_bg">
          <asp:LinkButton ID="lbtnDel" runat="server" OnClientClick="return confirm( 'Are you sure? ');" onclick="lbtnDel_Click">删除</asp:LinkButton>
           <asp:LinkButton ID="lbtnAdd" runat="server" onclick="lbtnAdd_Click" >添加</asp:LinkButton>
        </span>
    </div>
	</div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
    <script type="text/javascript">
        
          $(function() {
            //分页参数设置
            $("#Pagination").pagination(<%=pcount %>, {
            callback: pageselectCallback,
            prev_text: "« 上一页",
            next_text: "下一页 »",
            items_per_page:<%=pagesize %>,
		    num_display_entries:3,
		    current_page:<%=page %>,
		    num_edge_entries:2,
		    link_to:"?page=__id__"
           });
        });
        function pageselectCallback(page_id, jq) {
           //alert(page_id); 回调函数，进一步使用请参阅说明文档
        }

        $(function() {
            $(".msgtable tr:nth-child(odd)").addClass("tr_bg"); //隔行变色
            $(".msgtable tr").hover(
			    function() {
			        $(this).addClass("tr_hover_col");
			    },
			    function() {
			        $(this).removeClass("tr_hover_col");
			    }
		    );
        });
    </script>--%>
</body>
</html>
