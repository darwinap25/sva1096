﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class Show : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardType, Edge.SVA.Model.CardType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "Show");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!Page.IsPostBack)
            {
                ViewState["CardTypeID"] = Model.CardTypeID;

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = brand == null ? "" : DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                this.BrandID.Text = brand == null ? "" : ControlTool.GetDropdownListText(brandText, brand.BrandCode);
                
                Edge.SVA.Model.Currency currency = new Edge.SVA.BLL.Currency().GetModel(Model.CurrencyID.GetValueOrDefault());
                string currencyText = currency == null ? "" : DALTool.GetStringByCulture(currency.CurrencyName1, currency.CurrencyName2, currency.CurrencyName3);
                this.CurrencyID.Text = currency == null ? "" : ControlTool.GetDropdownListText(currencyText, currency.CurrencyCode);

                this.CreatedBy.Text = DALTool.GetUserName(Model.CreatedBy.GetValueOrDefault());
                this.UpdatedBy.Text = DALTool.GetUserName(Model.UpdatedBy.GetValueOrDefault());

                this.CreatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.CreatedOn);
                this.UpdatedOn.Text = Edge.Utils.Tools.StringHelper.GetDateTimeString(Model.UpdatedOn);

                this.CardCheckdigitView.Text = this.CardCheckdigit.SelectedItem == null ? "" : this.CardCheckdigit.SelectedItem.Text;
               // this.CardCheckdigit.Visible = false;

                this.CardMustHasOwnerView.Text = this.CardMustHasOwner.SelectedItem == null ? "" : this.CardMustHasOwner.SelectedItem.Text;
                this.CardMustHasOwner.Visible = false;

                this.IsImportUIDNumberView.Text = this.IsImportUIDNumber.SelectedItem == null ? "" : this.IsImportUIDNumber.SelectedItem.Text;
               // this.IsImportUIDNumber.Visible = false;

                this.CheckDigitModeIDView.Text = this.CheckDigitModeID.SelectedItem == null ? "" : this.CheckDigitModeID.SelectedItem.Text;
                this.CheckDigitModeID.Visible = false;

                this.CardNumberToUIDView.Text = this.CardNumberToUID.SelectedItem == null ? "" : this.CardNumberToUID.SelectedItem.Text;
                this.CardNumberToUID.Visible = false;

                this.UIDCheckDigitView.Text = this.UIDCheckDigit.SelectedItem == null ? "" : this.UIDCheckDigit.SelectedItem.Text;
                this.UIDCheckDigit.Visible = false;

                this.IsConsecutiveUIDView.Text = this.IsConsecutiveUID.SelectedItem == null ? "" : this.IsConsecutiveUID.SelectedItem.Text;
                this.IsConsecutiveUID.Visible = false;

                this.UIDToCardNumberView.Text = this.UIDToCardNumber.SelectedItem == null ? "" : this.UIDToCardNumber.SelectedItem.Text;
                //this.UIDToCardNumber.Visible = false;

                this.CashExpiredateView.Text = this.CashExpiredate.SelectedItem == null ? "" : this.CashExpiredate.SelectedItem.Text;
                this.CashExpiredate.Visible = false;

                this.PointExpiredateView.Text = this.PointExpiredate.SelectedItem == null ? "" : this.PointExpiredate.SelectedItem.Text;
                this.PointExpiredate.Visible = false;

                this.IsPhysicalCardView.Text = this.IsPhysicalCard.SelectedItem == null ? "" : this.IsPhysicalCard.SelectedItem.Text;
                this.IsPhysicalCard.Visible = false;

                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMaskImport.Text = Model.CardNumMask;
                    this.CardNumPatternImport.Text = Model.CardNumPattern;
                }

                this.rptCardGradeListPager.CurrentPageIndex = 1;

                BindCardGrad();

            }
        }

        private void BindCardGrad()
        {
            this.rptCardGradeListPager.PageSize = webset.ContentPageNum;
            this.rptCardGradeListPager.RecordCount = new Edge.SVA.BLL.CardGrade().GetCount("CardTypeID='" + ViewState["CardTypeID"].ToString() + "'");
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(this.rptCardGradeListPager.PageSize, this.rptCardGradeListPager.CurrentPageIndex - 1, "CardTypeID='" + ViewState["CardTypeID"].ToString() + "'", "CardGradeCode");
            if (ds.Tables[0] != null)
            {
                this.rptCardGradeList.DataSource = ds.Tables[0].DefaultView;
                this.rptCardGradeList.DataBind();
            }
            else
            {
                this.rptCardGradeList.DataSource = null;
                this.rptCardGradeList.DataBind();
            }

        }

        protected void rptCardGradeListPager_PageChanged(object sender, EventArgs e)
        {
            BindCardGrad();
        }
    }
}
