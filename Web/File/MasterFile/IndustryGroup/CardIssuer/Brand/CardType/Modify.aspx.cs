﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Text;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardType, Edge.SVA.Model.CardType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);


            }
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                ViewState["CardTypeID"] = Model.CardTypeID;

                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID);
                string brandText = DALTool.GetStringByCulture(brand.BrandName1, brand.BrandName2, brand.BrandName3);
                string brandID = brand.BrandID.ToString();
                this.BrandID.Items.Add(new ListItem() { Text = brandText, Value = brandID });

                string msg = "";

                if (Model.IsImportUIDNumber.GetValueOrDefault() == 1 && Model.UIDToCardNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMaskImport.Text = Model.CardNumMask;
                    this.CardNumPatternImport.Text = Model.CardNumPattern;
                }
                else if(Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                {
                    this.CardNumMask.Text = Model.CardNumMask;
                    this.CardNumPattern.Text = Model.CardNumPattern;
                }
                if (DALTool.isCardTypeCreatedCard(Model.CardTypeID, ref msg))//如果已经创建卡就不能修改其他规则
                {
                    if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                    {
                        this.CardNumMask.Enabled = false;
                        this.CardNumPattern.Enabled = false;
                        this.CardCheckdigit.Enabled = false;
                        this.CardNumberToUID.Enabled = false;
                        this.CheckDigitModeID.Enabled = false;
                    }
                    else
                    {
                        this.IsConsecutiveUID.Enabled = false;
                        this.UIDCheckDigit.Enabled = false;
                        this.UIDToCardNumber.Enabled = false;
                        this.CardNumMaskImport.Enabled = false;
                        this.CardNumPatternImport.Enabled = false; 
                    }

                    this.CardTypeCode.Enabled = false;
                    this.CardMustHasOwner.Enabled = false;
                    this.CashExpiredate.Enabled = false;
                    this.PointExpiredate.Enabled = false;
                    this.IsPhysicalCard.Enabled = false;
                    this.IsImportUIDNumber.Enabled = false;
                }
                else//未创建卡
                {
                    if (DALTool.isCardTypeCreatedCardGrade(Model.CardTypeID, ref msg))//已创建CardGrade
                    {
                        if (Model.IsImportUIDNumber.GetValueOrDefault() == 0)
                        {
                            if (string.IsNullOrEmpty(Model.CardNumMask) && string.IsNullOrEmpty(Model.CardNumPattern))//Card Type 无卡规则
                            {
                                this.CardNumMask.Enabled = false;
                                this.CardNumPattern.Enabled = false;
                                this.CardCheckdigit.Enabled = false;
                                this.CardNumberToUID.Enabled = false;
                                this.CheckDigitModeID.Enabled = false;
                            }


                        }
                        else
                        {
                            if (string.IsNullOrEmpty(Model.CardNumMask) && string.IsNullOrEmpty(Model.CardNumPattern))//Card Type 无卡规则
                            {
                                this.CardNumMaskImport.Enabled = false;
                                this.CardNumPatternImport.Enabled = false;
                            }
                            //this.IsConsecutiveUID.Enabled = false;
                            //this.UIDCheckDigit.Enabled = false;
                            //this.UIDToCardNumber.Enabled = false;
                        }

                        this.IsImportUIDNumber.Enabled = false;
                    }
                }


                this.rptCardGradeListPager.CurrentPageIndex = 1;
                BindCardGrad();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.CardType item = this.GetUpdateObject();

            if (!ValidObject(item)) return;
     
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.CardType>(item))
            {
                //Update card grade
                if (this.IsImportUIDNumber.SelectedValue == "0")//手动
                {
                    if ((this.CardNumMask.Enabled) && (!string.IsNullOrEmpty(CardNumMask.Text.Trim())))//号码规则可以修改，并且不为空的时候更新
                    {
                        List<Edge.SVA.Model.CardGrade> cardGradeList = new Edge.SVA.BLL.CardGrade().GetModelList(" CardTypeID='" + item.CardTypeID + "'");
                        foreach (Edge.SVA.Model.CardGrade grade in cardGradeList)
                        {
                            grade.CardNumMask = item.CardNumMask.ToUpper();
                            grade.CardNumPattern = item.CardNumPattern.Trim();
                            grade.CardCheckdigit = item.CardCheckdigit.GetValueOrDefault();
                            grade.CheckDigitModeID = item.CheckDigitModeID.GetValueOrDefault();
                            grade.CardNumberToUID = item.CardNumberToUID.GetValueOrDefault();
                            new Edge.SVA.BLL.CardGrade().Update(grade);
                        }
                    }

                }
                else//导入
                {
                    if (IsConsecutiveUID.Enabled && UIDCheckDigit.Enabled && UIDToCardNumber.Enabled)//可以修改的时候更新
                    {
                        List<Edge.SVA.Model.CardGrade> cardGradeList = new Edge.SVA.BLL.CardGrade().GetModelList(" CardTypeID='" + item.CardTypeID + "'");
                        foreach (Edge.SVA.Model.CardGrade grade in cardGradeList)
                        {
                            grade.IsConsecutiveUID = item.IsConsecutiveUID;
                            grade.UIDCheckDigit = item.UIDCheckDigit;
                            grade.UIDToCardNumber = item.UIDToCardNumber;
                            new Edge.SVA.BLL.CardGrade().Update(grade);
                        }
                    }
                }
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }

        private bool ValidObject(SVA.Model.CardType item)
        {
            if (item == null) return false;

            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardTypeCode = item.CardTypeCode.ToUpper();
            item.CardNumMask = item.CardNumMask.ToUpper();

            if (Edge.Web.Tools.DALTool.isHasCardTypeCode(item.CardTypeCode, item.CardTypeID))
            {
                JscriptPrint(Resources.MessageTips.ExistCardTypeCode, "", Resources.MessageTips.WARNING_TITLE);
                return false;
            }
            try
            {
                if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    //-------------------------------导入-------------------------------
                    item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
                    item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
                    item.CardCheckdigit = null;
                    item.CardNumberToUID = null;

                    if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
           
                }
                else
                {
                    //-------------------------------手动-------------------------------
                    item.IsConsecutiveUID = null;
                    item.UIDCheckDigit = null;
                    item.UIDToCardNumber = null;

                    if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                   
                }
            }
            catch (Exception ex)
            {
                JscriptPrint(ex.Message, "", Resources.MessageTips.WARNING_TITLE);
                return false;
            }

            if (!string.IsNullOrEmpty(item.CardNumMask) && !string.IsNullOrEmpty(item.CardNumPattern))
            {
                if (item.UIDToCardNumber.GetValueOrDefault() == 0 && !Controllers.RegexController.GetInstance().IsNumMask(item.CardNumMask, item.CardNumPattern))
                {
                    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90383"), "", Resources.MessageTips.WARNING_TITLE, clientID);
                    return false;
                }
                       
                if (Tools.DALTool.isHasCardTypeCardNumMask(item.CardNumMask, item.CardNumPattern, item.CardTypeID))
                {
                    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
                    return false;
                }

                if (Tools.DALTool.isHasCardCardeCardNumMaskWithCardType(item.CardNumMask, item.CardNumPattern, item.CardTypeID))
                {
                    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
                    return false;
                }


                if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, item.CardTypeID))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                    return false;
                }
            }

            return true;
        }

        private void BindCardGrad()
        {
            this.rptCardGradeListPager.PageSize = webset.ContentPageNum;
            this.rptCardGradeListPager.RecordCount = new Edge.SVA.BLL.CardGrade().GetCount("CardTypeID='" + ViewState["CardTypeID"].ToString() + "'");
            DataSet ds = new Edge.SVA.BLL.CardGrade().GetList(this.rptCardGradeListPager.PageSize, this.rptCardGradeListPager.CurrentPageIndex - 1, "CardTypeID='" + ViewState["CardTypeID"].ToString() + "'", "CardGradeCode");
            if (ds.Tables[0] != null)
            {
                this.rptCardGradeList.DataSource = ds.Tables[0].DefaultView;
                this.rptCardGradeList.DataBind();
            }
            else
            {
                this.rptCardGradeList.DataSource = null;
                this.rptCardGradeList.DataBind();
            }

        }

        protected void rptCardGradeListPager_PageChanged(object sender, EventArgs e)
        {
            BindCardGrad();
        }
    }
}
