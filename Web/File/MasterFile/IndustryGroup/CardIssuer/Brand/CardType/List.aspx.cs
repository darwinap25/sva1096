﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;

            if (!Page.IsPostBack)
            {
                logger.WriteOperationLog(this.PageName, "List");
                this.lbtnDel.OnClientClick = "return checkAndConfirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                RptBind("CardTypeID>=0", "CardTypeCode");
               
            }
           
        }
        #region 数据列表绑定

        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.CardType bll = new Edge.SVA.BLL.CardType();

            //获得总条数
            this.pcount = bll.GetCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);

            Edge.Web.Tools.DataTool.AddBrandName(ds,"BrandName","BrandID");
            Tools.DataTool.AddBrandCode(ds, "BrandCode", "BrandID");

            ds.Tables[0].Columns.Add(new DataColumn("CardIssuerName", typeof(string)));
            Edge.SVA.BLL.Brand bllBrand = new Edge.SVA.BLL.Brand();
            Edge.SVA.BLL.CardIssuer bllCardIssuer = new Edge.SVA.BLL.CardIssuer();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Edge.SVA.Model.Brand mBrand = bllBrand.GetModel(int.Parse(row["BrandID"].ToString()));
                if (mBrand != null)
                {
                    Edge.SVA.Model.CardIssuer mCardIssuer = bllCardIssuer.GetModel(mBrand.CardIssuerID.GetValueOrDefault());
                    if (mCardIssuer != null) row["CardIssuerName"] = Edge.Web.Tools.DALTool.GetStringByCulture(mCardIssuer.CardIssuerName1, mCardIssuer.CardIssuerName2, mCardIssuer.CardIssuerName3);
                }

            }

            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }

        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            string ids = "";
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("lb_id")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    ids += string.Format("{0};", id.ToString());
                }
            }
            Response.Redirect("Delete.aspx?ids=" + ids);
        }

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }
    }
}
