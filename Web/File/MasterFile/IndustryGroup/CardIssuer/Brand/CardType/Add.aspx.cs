﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;
using System.Text;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand.CardType
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardType, Edge.SVA.Model.CardType>
    {
        Tools.Logger logger = Tools.Logger.Instance;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                Edge.Web.Tools.ControlTool.BindCurrency(this.CurrencyID);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");

            if (Edge.Web.Tools.DALTool.isHasCardTypeCode(this.CardTypeCode.Text.Trim(), 0))
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistCardTypeCode, "", Resources.MessageTips.WARNING_TITLE, this.CardTypeCode.ClientID);
                return;
            }

            Edge.SVA.Model.CardType item = this.GetAddObject();
            if (!ValidObject(item)) return;

            if (Tools.DALTool.Add<Edge.SVA.BLL.CardType>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }

        private bool ValidObject(SVA.Model.CardType item)
        {
            if (item == null) return false;


            item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.CreatedOn = System.DateTime.Now;
            item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
            item.UpdatedOn = System.DateTime.Now;
            item.CardTypeCode = item.CardTypeCode.ToUpper();
            item.CardNumMask = item.CardNumMask.ToUpper();
            try
            {
                if (item.IsImportUIDNumber.GetValueOrDefault() == 1)
                {
                    //-------------------------------导入-------------------------------
                    item.CardNumMask = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumMaskImport.Text.ToUpper().Trim() : "";
                    item.CardNumPattern = item.UIDToCardNumber.GetValueOrDefault() == 0 ? this.CardNumPatternImport.Text.ToUpper().Trim() : "";
                    item.CardCheckdigit = null;
                    item.CardNumberToUID = null;

                    if (item.UIDCheckDigit.GetValueOrDefault() == 1 && item.UIDToCardNumber.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.UIDCheckDigit.GetValueOrDefault() == 0 && item.UIDToCardNumber.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
                else
                {
                    //-------------------------------手动-------------------------------
                    item.IsConsecutiveUID = null;
                    item.UIDCheckDigit = null;
                    item.UIDToCardNumber = null;

                    if (item.CardCheckdigit.GetValueOrDefault() == 1 && item.CardNumberToUID.GetValueOrDefault() == 3) throw new Exception("Check Digit Can Not Add");
                    if (item.CardCheckdigit.GetValueOrDefault() == 0 && item.CardNumberToUID.GetValueOrDefault() == 2) throw new Exception("No Check Digit Can Not Delete");
                }
            }
            catch (Exception ex)
            {
                JscriptPrint(ex.Message, "", Resources.MessageTips.WARNING_TITLE);
                return false;
            }

            if (!string.IsNullOrEmpty(item.CardNumMask) && !string.IsNullOrEmpty(item.CardNumPattern)) 
            {
                if (item.UIDToCardNumber.GetValueOrDefault() == 0 && !Controllers.RegexController.GetInstance().IsNumMask(item.CardNumMask, item.CardNumPattern))
                {
                    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90383"), "", Resources.MessageTips.WARNING_TITLE, clientID);
                    return false;
                }

                if (Tools.DALTool.isHasCardTypeCardNumMask(item.CardNumMask, item.CardNumPattern, 0))
                {
                    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
                    return false;
                }

                if (Tools.DALTool.isHasCardCardeCardNumMask(item.CardNumMask, item.CardNumPattern, 0, 0))
                {
                    string clientID = item.IsImportUIDNumber.GetValueOrDefault() == 1 ? this.CardNumMaskImport.ClientID : this.CardNumMask.ClientID;
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, clientID);
                    return false;
                }

                if (!Tools.DALTool.CheckNumberMask(this.CardNumMask.Text.Trim(), this.CardNumPattern.Text.Trim(), 0, 0))
                {
                    this.JscriptPrintAndFocus(Messages.Manager.MessagesTool.instance.GetMessage("90393"), "", Resources.MessageTips.WARNING_TITLE, this.CardNumMask.ClientID);
                    return false;
                }
            }

            return true;
        }
    }
}
