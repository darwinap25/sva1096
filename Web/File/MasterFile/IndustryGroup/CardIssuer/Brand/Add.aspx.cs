﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Edge.Security.Manager;

namespace Edge.Web.File.MasterFile.IndustryGroup.CardIssuer.Brand
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Brand, Edge.SVA.Model.Brand>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindIndustry(IndustryID);
                Edge.Web.Tools.ControlTool.BindCardIssuer(CardIssuerID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!this.IsPostBack)
            {
                List<Edge.SVA.Model.CardIssuer> models =  new Edge.SVA.BLL.CardIssuer().GetModelList("");
                if (models == null || models.Count <= 0) return ;

                this.CardIssuerID.SelectedValue = models[0].CardIssuerID.ToString();
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.isHasBrandCode(this.BrandCode.Text.Trim(), 0))
            {
                JscriptPrint(Resources.MessageTips.ExistBrandCode, "", Resources.MessageTips.WARNING_TITLE);
                this.BrandCode.Focus();
                return;
            }

            Edge.SVA.Model.Brand item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.BrandCode = item.BrandCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.Brand>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
 
        }

     

    }
}
