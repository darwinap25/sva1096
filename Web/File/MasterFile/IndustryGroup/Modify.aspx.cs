﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.MasterFile.IndustryGroup
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Industry, Edge.SVA.Model.Industry>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName,"Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.Industry item = this.GetUpdateObject();

   
            if (Tools.DALTool.isHasIndustryCode(this.IndustryCode.Text.Trim(), item.IndustryID))
            {
                JscriptPrint(Resources.MessageTips.ExistIndustryCode, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.IndustryCode = item.IndustryCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Industry>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
