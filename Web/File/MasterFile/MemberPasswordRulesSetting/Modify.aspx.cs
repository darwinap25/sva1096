﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.MemberPasswordRulesSetting
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            if (this.MemberPWDRule.SelectedValue == "1")
            {
                System.Data.DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("MemberPWDRule = 1");
                if (ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Rows[0]["PasswordRuleID"].ToString() == Request.Params["id"])
                {
                    Edge.SVA.Model.PasswordRule item = this.GetUpdateObject();

                    if (item != null)
                    {
                        item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                        item.UpdatedOn = DateTime.Now;
                        item.PasswordRuleCode = item.PasswordRuleCode.ToUpper();
                    }

                    if (DALTool.Update<Edge.SVA.BLL.PasswordRule>(item))
                    {
                        JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
                    }
                    else
                    {
                        JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
                    }
                }
                else
                {
                    JscriptPrint(Resources.MessageTips.ExistMemberRule, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
            }

            


        }
    }
}
