﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Organization
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Organization, Edge.SVA.Model.Organization>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.isHasOrganizationCode(this.OrganizationCode.Text.Trim(), 0))
            {
                JscriptPrint(Resources.MessageTips.ExistOrganizationCode, "", Resources.MessageTips.WARNING_TITLE);
                this.OrganizationCode.Focus();
                return;
            }


            if (!Edge.Web.Tools.DALTool.isHasCard(this.CardNumber.Text.Trim()))
            {
                JscriptPrint(Resources.MessageTips.NotExistCard, "", Resources.MessageTips.WARNING_TITLE);
                this.CardNumber.Focus();
                return;
            }

            Edge.SVA.Model.Organization item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.OrganizationCode = item.OrganizationCode.ToUpper();
            }

            if (DALTool.Add<Edge.SVA.BLL.Organization>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}