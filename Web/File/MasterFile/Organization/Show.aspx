﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.Organization.Show" %>

<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <td width="25%" align="right">
                捐赠组织编号：
            </td>
            <td width="75%">
                <asp:Label ID="OrganizationCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                捐赠组织描述：
            </td>
            <td>
                <asp:Label ID="OrganizationName1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:Label ID="OrganizationName2" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:Label ID="OrganizationName3" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                关联的卡号：
            </td>
            <td>
                <asp:Label ID="CardNumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                累计获得的积分：
            </td>
            <td>
                <asp:Label ID="CumulativePoints" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                累计获得的金额：
            </td>
            <td>
                <asp:Label ID="CumulativeAmt" runat="server"></asp:Label>
            </td>
        </tr>
        <%-- <tr>
            <td align="right">
                机构类型：
            </td>
            <td>
                <asp:Label ID="OrganizationType" runat="server" ></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                图片路径：
            </td>
            <td>
                <ufb:UploadFileBox ID="OrganizationPicFile" runat="server" FileType="preview" />
            </td>
        </tr>
        <tr>
            <td align="right">
                是否调用第三方接口：
            </td>
            <td>
                <asp:Label ID="CallInterfaceView" runat="server"></asp:Label>
                <asp:RadioButtonList ID="CallInterface" runat="server" TabIndex="10" RepeatDirection="Horizontal"
                    Enabled="false">
                    <asp:ListItem Text="调用" Value="1"></asp:ListItem>
                    <asp:ListItem Text="不调用" Value="0" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="javascript:history.back();"
                        class="submit" /></div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
