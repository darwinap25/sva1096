﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Location.Store
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store,Edge.SVA.Model.Store>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Edge.Web.Tools.ControlTool.BindBrand(BrandID);
                Edge.Web.Tools.ControlTool.BindStoreType(StoreTypeID);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!this.IsPostBack)
            {
                Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Model.BrandID.GetValueOrDefault());
                if (brand != null)
                {
                    Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                    this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
                }
                else
                {
                    this.lblCardIssuer.Text = "";
                }

             }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.Store item = this.GetUpdateObject();

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
                item.StoreCode = item.StoreCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.isHasStoreCodeWithBrandID(item.StoreCode, item.BrandID.GetValueOrDefault(), item.StoreID))
            {
                JscriptPrint(Resources.MessageTips.ExistStoreCodeInBrand, "", Resources.MessageTips.WARNING_TITLE);
                return;
            }
            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.Store>(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }


     
      
    }
}
