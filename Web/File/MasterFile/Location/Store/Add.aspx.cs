﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using System.Data;
using Edge.Web.Tools;

namespace Edge.Web.File.MasterFile.Location.Store
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Store,Edge.SVA.Model.Store>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ControlTool.BindStoreType(this.StoreTypeID);
                ControlTool.BindBrand(BrandID);
                this.lblCardIssuer.Text = DALTool.GetCardIssuerName();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Add");
            if (Edge.Web.Tools.DALTool.isHasStoreCodeWithBrandID(this.StoreCode.Text.Trim(), Tools.ConvertTool.ToInt(this.BrandID.Text.Trim()), 0))
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistStoreCodeInBrand, "", Resources.MessageTips.WARNING_TITLE,this.BrandID.ClientID);
                return;
            }

            Edge.SVA.Model.Store item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = DALTool.GetCurrentUser().UserID;
                item.CreatedOn = DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.StoreCode = item.StoreCode.ToUpper();
            }

            if (DALTool.Add<Edge.SVA.BLL.Store>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
           
        }

        protected void BrandID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlTool.AddTitle(this.BrandID);

            Edge.SVA.Model.Brand brand = new Edge.SVA.BLL.Brand().GetModel(Tools.ConvertTool.ToInt(this.BrandID.SelectedValue.Trim()));
            if (brand != null)
            {
                Edge.SVA.Model.CardIssuer cardIssuer = new Edge.SVA.BLL.CardIssuer().GetModel(brand.CardIssuerID.GetValueOrDefault());
                this.lblCardIssuer.Text = cardIssuer == null ? "" : DALTool.GetStringByCulture(cardIssuer.CardIssuerName1, cardIssuer.CardIssuerName2, cardIssuer.CardIssuerName3);
            }
            else
            {
                this.lblCardIssuer.Text = "";
            }
        }
      

       
    }
}
