﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Store.Show" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>
    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>
    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>
    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>
</head>
<body style="padding: 10px;">
    <form id="form1" runat="server" enableviewstate="false">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="6" align="left">
                <%=this.PageName %>
            </th>
        </tr>
        <tr>
            <td align="right">
                店铺编号：
            </td>
            <td>
                <asp:Label ID="StoreCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" align="right">
                描述：
            </td>
            <td width="75%">
                <asp:Label ID="StoreName1" TabIndex="1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:Label ID="StoreName2" TabIndex="2" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:Label ID="StoreName3" TabIndex="4" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                发行商：
            </td>
            <td>
                <asp:Label ID="lblCardIssuer" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺所属品牌列表：
            </td>
            <td>
                <asp:Label ID="BrandID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺分组：
            </td>
            <td>
                <asp:Label ID="StoreTypeID" runat="server"></asp:Label>
                <%--<asp:DropDownList ID="StoreTypeID" runat="server" CssClass="dropdownlist"  Enabled="false" SkinID="NoClass">
                </asp:DropDownList>--%>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺所在国家：
            </td>
            <td>
                <asp:Label ID="StoreCountry" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店鋪所在的省份：
            </td>
            <td>
                <asp:Label ID="StoreProvince" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店鋪所在的城市：
            </td>
            <td>
                <asp:Label ID="StoreCity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺地址：
            </td>
            <td>
                <asp:Label ID="StoreAddressDetail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺经度坐标：
            </td>
            <td>
                <asp:Label ID="StoreLongitude" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺纬度坐标：
            </td>
            <td>
                <asp:Label ID="StoreLatitude" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                电话号码：
            </td>
            <td>
                <asp:Label ID="StoreTel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                传真号码：
            </td>
            <td>
                <asp:Label ID="StoreFax" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:Label ID="Contact" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                营业开始时间：
            </td>
            <td>
                <asp:Label ID="StoreOpenTime" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                营业结束时间：
            </td>
            <td>
                <asp:Label ID="StoreCloseTime" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="StorePicFile" runat="server" SubSaveFilePath="Images/Store"
                    FileType="preview" />
            </td>
        </tr>
        <tr>
            <td align="right">
                创建时间：
            </td>
            <td>
                <asp:Label ID="CreatedOn" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                创建人：
            </td>
            <td>
                <asp:Label ID="CreatedBy" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改时间：
            </td>
            <td>
                <asp:Label ID="UpdatedOn" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                上次修改人：
            </td>
            <td>
                <asp:Label ID="UpdatedBy" runat="server" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <input type="button" name="button1" value="返 回" onclick="javascript:history.back();"
                        class="submit" />
                </div>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
