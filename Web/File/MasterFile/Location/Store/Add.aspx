﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Edge.Web.File.MasterFile.Location.Store.Add" %>

<%@ Register Src="~/Controls/checkright.ascx" TagName="checkright" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/UploadFileBox.ascx" TagName="UploadFileBox" TagPrefix="ufb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src='<%#GetjQueryPath() %>'></script>

    <script type="text/javascript" src='<%#GetjQueryValidatePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSMultiLanguagePath() %>'></script>

    <script type="text/javascript" src='<%#GetJSFunctionPath()%>'></script>

    <script type="text/javascript" src='<%#GetjQueryFormPath()%>'></script>

    <script type="text/javascript" src='<%#GetMy97DatePickerPath() %>'></script>

    <script type="text/javascript" src='<%#GetJSThickBoxPath() %>'></script>

    <script type="text/javascript">
        $(function() {
            //表单验证JS
            $("#form1").validate({
                //出错时添加的标签
                errorElement: "span",
                success: function(label) {
                    //正确时的样式
                    label.text(" ").addClass("success");
                }
            });
          
        });
    </script>

</head>
<body style="padding: 10px;">
    <form id="form1" runat="server">
    <div class="navigation">
        <span class="back"><a href="#"></a></span><b>您当前的位置：<%=this.PageName %></b>
    </div>
    <div style="padding-bottom: 10px;">
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtable">
        <tr>
            <th colspan="2" align="left">
                基本资料
            </th>
        </tr>
        <tr>
            <td width="25%" align="right">
                店铺编号：
            </td>
            <td width="75%">
                <asp:TextBox ID="StoreCode" TabIndex="1" runat="server"  MaxLength="20"
                    CssClass="input required svaCode" hinttitle="店铺编号" hintinfo="Translate__Special_121_Start1~20個字符，必須输入數字或者字母，不允許输入其他符號。例如：%&*Translate__Special_121_End"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                描述：
            </td>
            <td>
                <asp:TextBox ID="StoreName1" TabIndex="2" runat="server" MaxLength="512" CssClass="input required" hinttitle="描述" hintinfo="不能超過512個字符"></asp:TextBox><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述1：
            </td>
            <td>
                <asp:TextBox ID="StoreName2" TabIndex="3" runat="server" MaxLength="512" CssClass="input " hinttitle="其他描述1" hintinfo="對描述的一個補充。不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                其他描述2：
            </td>
            <td>
                <asp:TextBox ID="StoreName3" TabIndex="4" runat="server" MaxLength="512" CssClass="input " hinttitle="其他描述2" hintinfo="對描述的另一個補充。不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                发行商：
            </td>
            <td>
               <asp:TextBox ID="lblCardIssuer" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺所属品牌列表：
            </td>
            <td>
                <asp:DropDownList ID="BrandID" runat="server" TabIndex="6" SkinID="NoClass" 
                    CssClass="dropdownlist required cancel">
                </asp:DropDownList><span class="star">*</span>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺分组：
            </td>
            <td>
                <asp:DropDownList ID="StoreTypeID" runat="server" TabIndex="7" CssClass="dropdownlist "
                    SkinID="NoClass">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺所在国家：
            </td>
            <td>
                <asp:TextBox ID="StoreCountry" runat="server" TabIndex="8" MaxLength="512" CssClass="input " hinttitle="店铺所在国家" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店鋪所在的省份：
            </td>
            <td>
                <asp:TextBox ID="StoreProvince" runat="server" TabIndex="9" MaxLength="512" CssClass="input " hinttitle="店鋪所在的省份" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店鋪所在的城市：
            </td>
            <td>
                <asp:TextBox ID="StoreCity" runat="server" TabIndex="10" MaxLength="512" CssClass="input " hinttitle="店鋪所在的城市" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺地址：
            </td>
            <td>
                <asp:TextBox ID="StoreAddressDetail" runat="server" TabIndex="11" MaxLength="512"
                    CssClass="input " hinttitle="店铺地址" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺经度坐标：
            </td>
            <td>
                <asp:TextBox ID="StoreLongitude" runat="server" TabIndex="12" MaxLength="512" CssClass="input " hinttitle="店铺经度坐标" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺纬度坐标：
            </td>
            <td>
                <asp:TextBox ID="StoreLatitude" runat="server" TabIndex="13" MaxLength="512" CssClass="input " hinttitle="店铺纬度坐标" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                电话号码：
            </td>
            <td>
                <asp:TextBox ID="StoreTel" runat="server" TabIndex="14" MaxLength="512" CssClass="input " hinttitle="电话号码" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                传真号码：
            </td>
            <td>
                <asp:TextBox ID="StoreFax" runat="server" TabIndex="15" MaxLength="512" CssClass="input " hinttitle="传真号码" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td align="right">
                联系人：
            </td>
            <td>
                <asp:TextBox ID="Contact" runat="server" TabIndex="15" MaxLength="512" CssClass="input " hinttitle="联系人" hintinfo="不能超過512個字符"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                营业开始时间：
            </td>
            <td>
                <asp:TextBox ID="StoreOpenTime" runat="server" TabIndex="16" MaxLength="512" onfocus="WdatePicker({dateFmt:'HH:mm:ss'})"
                    CssClass="input" hinttitle="营业开始时间" hintinfo="时间格式：HH-MM-SS"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                营业结束时间：
            </td>
            <td>
                <asp:TextBox ID="StoreCloseTime" runat="server" TabIndex="17" MaxLength="512" onfocus="WdatePicker({dateFmt:'HH:mm:ss'})"
                    CssClass="input " hinttitle="营业结束时间" hintinfo="Translate__Special_121_Start时间格式：HH-MM-SSTranslate__Special_121_End"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                店铺图片：
            </td>
            <td>
                <ufb:UploadFileBox ID="StorePicFile" TabIndex="18" runat="server" SubSaveFilePath="Images/Store"
                    FileType="pictures" hinttitle="店铺图片" hintinfo="Translate__Special_121_Start点击按钮进行上传，上传的文件支持JPG，GIF和PNG，BMP文件大小不能超过10240KBTranslate__Special_121_End" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="提交" OnClick="btnAdd_Click" CssClass="submit">
                    </asp:Button>
                    <input type="button" name="button1" value="返 回" onclick="location.href= 'List.aspx' "
                        class="submit" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="showMessage" colspan="2" >
                *为必填项
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; text-align: center;">
    </div>
    <uc2:checkright ID="Checkright1" runat="server" />
    </form>
</body>
</html>
