﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.CurrencyExchangeRate
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.Currency,Edge.SVA.Model.Currency>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

       
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, " Update success ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.Currency item = this.GetUpdateObject();

            if (Tools.DALTool.isHasCurrencyCode(this.CurrencyCode.Text.Trim(),item.CurrencyID))
            {
                this.JscriptPrintAndFocus(Resources.MessageTips.ExistCurrencyCode, "", Resources.MessageTips.WARNING_TITLE, this.CurrencyCode.ClientID);
                return;
            }


            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
                item.CurrencyCode = item.CurrencyCode.ToUpper();
            }
            if (DALTool.Update<Edge.SVA.BLL.Currency>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, string.Format("Update Success.   Code:{0}", item.CurrencyCode));
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, string.Format("Update Failed.   Code:{0}", item == null ? "No Data" : item.CurrencyCode));
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
