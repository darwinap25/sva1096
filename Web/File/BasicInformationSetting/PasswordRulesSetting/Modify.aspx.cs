﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.PasswordRulesSetting
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.PasswordRule,Edge.SVA.Model.PasswordRule>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            if (this.MemberPWDRule.SelectedValue == "1" )
            {
                System.Data.DataSet ds = new Edge.SVA.BLL.PasswordRule().GetList("MemberPWDRule = 1");
                if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["PasswordRuleID"].ToString() != Request.Params["id"])
                {
                    JscriptPrint(Resources.MessageTips.ExistMemberRule, "", Resources.MessageTips.WARNING_TITLE);
                    return;
                }
            }

            Edge.SVA.Model.PasswordRule item = this.GetUpdateObject();
            if (Tools.DALTool.isHasPasswordRuleCode(this.PasswordRuleCode.Text.Trim(), item.PasswordRuleID))
            {
                this.JscriptPrintAndFocus(Resources.MessageTips.ExistPasswordSettingCode, "", Resources.MessageTips.WARNING_TITLE, this.PasswordRuleCode.ClientID);
                return;
            }
            Edge.SVA.BLL.PasswordRule bll = new Edge.SVA.BLL.PasswordRule();
            if (bll.GetCount(string.Format("PWDSecurityLevel = {0} and PasswordRuleID <> {1}", Tools.ConvertTool.ConverType<int>(this.PWDSecurityLevel.Text), Tools.ConvertTool.ConverType<int>(Request.Params["id"]))) > 0)
            {
                JscriptPrintAndFocus(Resources.MessageTips.ExistPasswordSecurity, "", Resources.MessageTips.WARNING_TITLE, this.PWDSecurityLevel.ClientID);
                return;
            }
            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
                item.PasswordRuleCode = item.PasswordRuleCode.ToUpper();
                item.PWDMinLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMinLength.GetValueOrDefault();
                item.PWDMaxLength = item.PWDStructure.GetValueOrDefault() == 0 ? 0 : item.PWDMaxLength.GetValueOrDefault();
            }
            if (DALTool.Update<Edge.SVA.BLL.PasswordRule>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Update\t Code:" + item.PasswordRuleCode);
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "PasswordRulesSetting Update\t Code:" + item == null ? "No Data" : item.PasswordRuleCode);
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
