﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.CardTypeNature
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CardTypeNature,Edge.SVA.Model.CardTypeNature>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.CardTypeNature item = this.GetAddObject();
            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.CardTypeNature>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
          
        }
    }
}
