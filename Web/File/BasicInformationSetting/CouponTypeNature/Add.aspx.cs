﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.CouponTypeNature
{
    public partial class Add : Edge.Web.Tools.BasePage<Edge.SVA.BLL.CouponTypeNature,Edge.SVA.Model.CouponTypeNature>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Edge.SVA.Model.CouponTypeNature item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.CouponTypeNature>(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
         
        }
    }
}
