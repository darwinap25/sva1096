﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Edge.Web.File.BasicInformationSetting.TermsAndConditions
{
    public partial class List : Edge.Web.UI.ManagePage
    {
        public int pcount;                      //总条数
        public int page;                        //当前页
        public int pagesize;                    //设置每页显示的大小

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagesize = webset.ContentPageNum;

            if (!Page.IsPostBack)
            {
                this.lbtnDel.OnClientClick = "return confirm( '" + Resources.MessageTips.ConfirmDeleteRecord + " ');";
                RptBind("MemberClauseID>0", "MemberClauseID");

            }
        }



        #region 数据列表绑定
        private void RptBind(string strWhere, string orderby)
        {
            if (!int.TryParse(Request.Params["page"] as string, out this.page))
            {
                this.page = 0;
            }

            Edge.SVA.BLL.MemberClause bll = new Edge.SVA.BLL.MemberClause();

            //获得总条数
            this.pcount = bll.GetCount(strWhere);
            if (this.pcount > 0)
            {
                this.lbtnDel.Enabled = true;
            }
            else
            {
                this.lbtnDel.Enabled = false;
            }

            DataSet ds = new DataSet();
            ds = bll.GetList(this.pagesize, this.page, strWhere, orderby);
            this.rptList.DataSource = ds.Tables[0].DefaultView;
            this.rptList.DataBind();
        }
        #endregion

        protected void lbtnDel_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((Label)rptList.Items[i].FindControl("lb_id")).Text);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("cb_id");
                if (cb.Checked)
                {
                    Edge.SVA.BLL.MemberClause bll = new Edge.SVA.BLL.MemberClause();
                    //保存日志
                    // SaveLogs("");
                    bll.Delete(id);
                }
            }

            JscriptPrint(Resources.MessageTips.DeleteSuccess, "List.aspx?page=0", "Success");
        }

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }
    }
}
