﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.TermsAndConditions
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.MemberClause, Edge.SVA.Model.MemberClause>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            User currentUser = new Edge.Security.Manager.User(user);

            Edge.SVA.Model.MemberClause item = (Edge.SVA.Model.MemberClause)DALTool.GetObject <Edge.SVA.BLL.MemberClause>(int.Parse(Request.Params["id"]));
            item.MemberClauseDesc1 = this.MemberClauseDesc1.Text.Trim();
            item.MemberClauseDesc2 = this.MemberClauseDesc2.Text.Trim();
            item.MemberClauseDesc3 = this.MemberClauseDesc3.Text.Trim();
            item.MemberClauseCode = this.MemberCode.Text.Trim();
           
            Edge.SVA.BLL.MemberClause bll = new Edge.SVA.BLL.MemberClause();
            if (bll.Update(item))
            {
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }

            //Response.Redirect("List.aspx?page=0");
        }
    }
}
