﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.TermsAndConditions
{
    public partial class Add : Edge.Web.UI.ManagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            AccountsPrincipal user = new AccountsPrincipal(Context.User.Identity.Name, Session["SiteLanguage"].ToString());//todo: 修改成多语言。
            User currentUser = new Edge.Security.Manager.User(user);

            Edge.SVA.Model.MemberClause item = new Edge.SVA.Model.MemberClause();
            item.MemberClauseDesc1 = this.MemberClauseDesc1.Text.Trim();
            item.MemberClauseDesc2 = this.MemberClauseDesc2.Text.Trim();
            item.MemberClauseDesc3 = this.MemberClauseDesc3.Text.Trim();
            item.MemberClauseCode = this.MemberCode.Text.Trim();

            Edge.SVA.BLL.MemberClause bll = new Edge.SVA.BLL.MemberClause();
            if (bll.Add(item) > 0)
            {
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }
            //Response.Redirect("List.aspx?page=0");
        }
    }
}
