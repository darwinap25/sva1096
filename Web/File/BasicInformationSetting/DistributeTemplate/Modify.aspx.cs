﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Web.Tools;

namespace Edge.Web.File.BasicInformationSetting.DistributeTemplate
{
    public partial class Modify : Tools.BasePage<Edge.SVA.BLL.DistributeTemplate, Edge.SVA.Model.DistributeTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Update ");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.DistributeTemplate item = this.GetUpdateObject();

            if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), item.DistributionID))
            {
                this.JscriptPrint(Resources.MessageTips.ExistDistributeTemplateCode, "", Resources.MessageTips.WARNING_TITLE);
                this.DistributionCode.Focus();
                return;
            }

            if (item != null)
            {
                item.UpdatedBy = DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = DateTime.Now;
                item.DistributionCode = item.DistributionCode.ToUpper();
            
            }
            if (DALTool.Update<Edge.SVA.BLL.DistributeTemplate>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Update\t Code:" + item.DistributionCode);
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Update\t Code:" + item == null ? "No Data" : item.DistributionCode);
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }
        }
    }
}
