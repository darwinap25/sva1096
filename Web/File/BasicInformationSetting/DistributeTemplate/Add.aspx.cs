﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Edge.Web.File.BasicInformationSetting.DistributeTemplate
{
    public partial class Add : Tools.BasePage<Edge.SVA.BLL.DistributeTemplate,Edge.SVA.Model.DistributeTemplate>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName," Add ");
            if (Tools.DALTool.isHasDistributeCode(this.DistributionCode.Text.Trim(), 0))
            {
                this.JscriptPrint(Resources.MessageTips.ExistDistributeTemplateCode, "", Resources.MessageTips.WARNING_TITLE);
                this.DistributionCode.Focus();
                return;
            }
            Edge.SVA.Model.DistributeTemplate item = this.GetAddObject();

            if (item != null)
            {
                item.CreatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.CreatedOn = System.DateTime.Now;
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.DistributionCode = item.DistributionCode.ToUpper();
            }
            if (Edge.Web.Tools.DALTool.Add<Edge.SVA.BLL.DistributeTemplate>(item) > 0)
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Add Success\tCode:" + item.DistributionCode);
                JscriptPrint(Resources.MessageTips.AddSuccess, "List.aspx?page=0", Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "DistributeTemplate Add Failed\tCode:" + item == null ? "No Data" : item.DistributionCode);
                JscriptPrint(Resources.MessageTips.AddFailed, "List.aspx?page=0", Resources.MessageTips.FAILED_TITLE);
            }

        }
    }
}
