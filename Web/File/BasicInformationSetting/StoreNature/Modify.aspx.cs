﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edge.Security.Manager;

namespace Edge.Web.File.BasicInformationSetting.StoreNature
{
    public partial class Modify : Edge.Web.Tools.BasePage<Edge.SVA.BLL.StoreType,Edge.SVA.Model.StoreType>
    {
        Tools.Logger logger = Tools.Logger.Instance;
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            logger.WriteOperationLog(this.PageName, "Update");
            int page = 0;
            int.TryParse(Request.Params["page"], out page);

            Edge.SVA.Model.StoreType item = this.GetUpdateObject();

            if (Tools.DALTool.isHasStoreTypeCode(this.StoreTypeCode.Text.Trim(), item.StoreTypeID))
            {
                this.JscriptPrint(Resources.MessageTips.ExistStoreGroupCode, "", Resources.MessageTips.WARNING_TITLE);
                this.StoreTypeCode.Focus();
                return;
            }

            if (item != null)
            {
                item.UpdatedBy = Edge.Web.Tools.DALTool.GetCurrentUser().UserID;
                item.UpdatedOn = System.DateTime.Now;
                item.StoreTypeCode = item.StoreTypeCode.ToUpper();
            }

            if (Edge.Web.Tools.DALTool.Update<Edge.SVA.BLL.StoreType>(item))
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Delete\t Code:" + item.StoreTypeCode);
                JscriptPrint(Resources.MessageTips.UpdateSuccess, "List.aspx?page=" + page.ToString(), Resources.MessageTips.SUCESS_TITLE);
            }
            else
            {
                Tools.Logger.Instance.WriteOperationLog(this.PageName, "Store Delete\t Code:" + item == null ? "No Data" : item.StoreTypeCode);
                JscriptPrint(Resources.MessageTips.UpdateFailed, "List.aspx?page=" + page.ToString(), Resources.MessageTips.FAILED_TITLE);
            }

            //Response.Redirect("List.aspx?page=0");
        }
    }
}
