﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.Model.Domain
{
    public class EarnCouponRuleViewModel
    {
        private EarnCouponRule mainTable = new EarnCouponRule();

        public EarnCouponRule MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
