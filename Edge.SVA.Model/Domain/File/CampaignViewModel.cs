﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.SVA.Model.Domain.File
{
    public class CampaignViewModel
    {
        Campaign mainTable = new Campaign();

        public Campaign MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        private List<KeyValue> cardGradeList = new List<KeyValue>();

        public List<KeyValue> CardGradeList
        {
            get { return cardGradeList; }
            set { cardGradeList = value; }
        }


        private List<KeyValue> couponTypeList = new List<KeyValue>();

        public List<KeyValue> CouponTypeList
        {
            get { return couponTypeList; }
            set { couponTypeList = value; }
        }

    }
}
