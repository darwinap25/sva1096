﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.Model.Domain.File
{
   public class PointRuleViewModel
    {
       PointRule mainTable = new PointRule();

       public PointRule MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

       DataTable subTable = new DataTable();

       public DataTable SubTable
       {
           get { return subTable; }
           set { subTable = value; }
       }

       private string dayCode;

       public string DayCode
       {
           get { return dayCode; }
           set { dayCode = value; }
       }

       private string monthCode;

       public string MonthCode
       {
           get { return monthCode; }
           set { monthCode = value; }
       }

       private string weekCode;

       public string WeekCode
       {
           get { return weekCode; }
           set { weekCode = value; }
       }
    }
}
