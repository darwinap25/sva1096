﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class CSVXMLMointoringRuleViewModel
    {
        CSVXMLMointoringRule mainTable = new CSVXMLMointoringRule();

        public CSVXMLMointoringRule MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
