﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class MemberClauseViewModel
    {
        MemberClause mainTable = new MemberClause();

        public MemberClause MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
