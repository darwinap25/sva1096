﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
   public class StoreGroupViewModel
    {
        StoreGroup mainTable = new StoreGroup();

        public StoreGroup MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

    }
}
