﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File.BasicViewModel
{
    public class CardGradeHoldCouponRuleViewModel
    {
        private CardGradeHoldCouponRule mainTable = new CardGradeHoldCouponRule();

        public CardGradeHoldCouponRule MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        private string brandDescription = string.Empty;

        public string BrandDescription
        {
            get { return brandDescription; }
            set { brandDescription = value; }
        }
        private string couponTypeNatureDescription = string.Empty;

        public string CouponTypeNatureDescription
        {
            get { return couponTypeNatureDescription; }
            set { couponTypeNatureDescription = value; }
        }
        private string couponTypeDescription = string.Empty;

        public string CouponTypeDescription
        {
            get { return couponTypeDescription; }
            set { couponTypeDescription = value; }
        }
    }
}
