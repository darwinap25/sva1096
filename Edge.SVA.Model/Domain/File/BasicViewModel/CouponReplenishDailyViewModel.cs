﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File.BasicViewModel
{
    public class CouponReplenishDailyViewModel
    {
        private CouponReplenishDaily mainTable = new CouponReplenishDaily();

        public CouponReplenishDaily MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
