﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class SupplierViewModel
    {
        Supplier mainTable = new Supplier();

        public Supplier MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
