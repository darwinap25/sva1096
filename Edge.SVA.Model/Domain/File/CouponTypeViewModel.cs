﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.File.BasicViewModel;

namespace Edge.SVA.Model.Domain.File
{
    public class CouponTypeViewModel
    {
        private CouponType couponGrade = new CouponType();

        public CouponType MainTable
        {
            get { return couponGrade; }
            set { couponGrade = value; }
        }
        private MemberClause memberClause = new MemberClause();

        public MemberClause MemberClause
        {
            get { return memberClause; }
            set { memberClause = value; }
        }

        private List<CouponReplenishDailyViewModel> couponReplenishDailyList = new List<CouponReplenishDailyViewModel>();

        public List<CouponReplenishDailyViewModel> CouponReplenishDailyViewModelList
        {
            get { return couponReplenishDailyList; }
        }
        private List<CouponReplenishDailyViewModel> add;

        public List<CouponReplenishDailyViewModel> CouponReplenishDailyViewModelListAdd
        {
            get
            {
                if (add == null)
                {
                    add = new List<CouponReplenishDailyViewModel>();
                }
                return add;
            }
            set { add = value; }
        }
        //private List<CouponReplenishDailyViewModel> del;

        //public List<CouponReplenishDailyViewModel> CouponReplenishDailyViewModelListDelete
        //{
        //    get
        //    {
        //        if (del == null)
        //        {
        //            del = new List<CouponReplenishDailyViewModel>();
        //        }
        //        return del;
        //    }
        //}
    }
}
