﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
   public class CurrencyViewModel
    {
       Currency mainTable = new  Currency();

       public Currency MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
