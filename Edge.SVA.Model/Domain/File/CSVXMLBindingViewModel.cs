﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class CSVXMLBindingViewModel
    {
        CSVXMLBinding mainTable = new CSVXMLBinding();

        public CSVXMLBinding MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
