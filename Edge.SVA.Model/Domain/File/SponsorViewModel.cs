﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class SponsorViewModel
    {
        Sponsor mainTable = new Sponsor();

        public Sponsor MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
