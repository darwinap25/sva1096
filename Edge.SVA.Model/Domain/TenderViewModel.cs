﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.File
{
    public class TenderViewMode
    {
        TENDER mainTable = new TENDER();

        public TENDER MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
