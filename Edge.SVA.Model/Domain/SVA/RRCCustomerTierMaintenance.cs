﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.SVA
{
    [Serializable]
    public partial class RRCCustomerTierMaintenance
    {
        public RRCCustomerTierMaintenance()
        { }
        #region Model

        private int _Id;
        private int _RRCCustomerTierCode;
        private string _RRCCustomerTier;

        public int Id
        {
            set { _Id = value; }
            get { return _Id; }
        }

        public int RRCCustomerTierCode
        {
            set { _RRCCustomerTierCode = value; }
            get { return _RRCCustomerTierCode; }
        }

        public string RRCCustomerTier
        {
            set { _RRCCustomerTier = value; }
            get { return _RRCCustomerTier; }
        }

        #endregion Model
    }
}
