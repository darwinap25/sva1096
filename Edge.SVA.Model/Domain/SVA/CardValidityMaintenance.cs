﻿using System;
namespace Edge.SVA.Model.Domain.SVA
{
    [Serializable]
    public partial class CardValidityMaintenance
    {
        public CardValidityMaintenance()
        { }
        #region Model

        private int _Id;
        private int _CardValidityInYears;
        private string _ExpiryDateField;
        private int _RRCTypeOfApplicationCode;
        private int _RRCCustomerTierCode;
        private int _WithCondition;
        private string _ConditionField1;
        private string _Operation;
        private string _ConditionField2;
        private int _ConditionalExpiryPeriodInMonths;
        private string _LogicalOperator;
        private int _EndOfMonth;

        public int Id
        {
            set { _Id = value; }
            get { return _Id; }
        }

        public int CardValidityInYears
        {
            set { _CardValidityInYears = value; }
            get { return _CardValidityInYears; }
        }

        public string ExpiryDateField
        {
            set { _ExpiryDateField = value; }
            get { return _ExpiryDateField; }
        }

        public int RRCTypeOfApplicationCode
        {
            set { _RRCTypeOfApplicationCode = value; }
            get { return _RRCTypeOfApplicationCode; }
        }

        public int RRCCustomerTierCode
        {
            set { _RRCCustomerTierCode = value; }
            get { return _RRCCustomerTierCode; }
        }

        public int WithCondition
        {
            set { _WithCondition = value; }
            get { return _WithCondition; }
        }

        public string ConditionField1
        {
            set { _ConditionField1 = value; }
            get { return _ConditionField1; }
        }

        public string Operation
        {
            set { _Operation = value; }
            get { return _Operation; }
        }

        public string ConditionField2
        {
            set { _ConditionField2 = value; }
            get { return _ConditionField2; }
        }

        public int ConditionalExpiryPeriodInMonths
        {
            set { _ConditionalExpiryPeriodInMonths = value; }
            get { return _ConditionalExpiryPeriodInMonths; }
        }

        public string LogicalOperator
        {
            set { _LogicalOperator = value; }
            get { return _LogicalOperator; }
        }

        public int EndOfMonth
        {
            set { _EndOfMonth = value; }
            get { return _EndOfMonth; }
        }
        #endregion Model


    }
}
