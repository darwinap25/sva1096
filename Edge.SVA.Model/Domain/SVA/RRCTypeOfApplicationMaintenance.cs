﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.SVA
{
    [Serializable]
    public partial class RRCTypeOfApplicationMaintenance
    {
        public RRCTypeOfApplicationMaintenance()
        { }

        #region Model

        public int _Id;
        public int _RRCTypeOfApplicationCode;
        public string _RRCTypeOfApplication;

        public int Id
        {
            set { _Id = value; }
            get { return _Id; }
        }

        public int RRCTypeOfApplicationCode
        {
            set { _RRCTypeOfApplicationCode = value; }
            get { return _RRCTypeOfApplicationCode; }
        }

        public string RRCTypeOfApplication
        {
            set { _RRCTypeOfApplication = value; }
            get { return _RRCTypeOfApplication; }
        }

        #endregion Model
    }
}
