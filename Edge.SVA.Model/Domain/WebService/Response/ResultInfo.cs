﻿using System;
using System.Collections.Generic;

namespace Edge.SVA.Model.Domain.WebService.Response
{

    public class ResultInfo
    {

        private string _mEMBERID;
        public string MEMBERID
        {
            get
            {
                return _mEMBERID;
            }
            set
            {
                _mEMBERID = value;
            }
        }

        private string _mSGACCOUNT;
        public string MSGACCOUNT
        {
            get
            {
                return _mSGACCOUNT;
            }
            set
            {
                _mSGACCOUNT = value;
            }
        }

        private string _mSGACCOUNTTYPE;
        public string MSGACCOUNTTYPE
        {
            get
            {
                return _mSGACCOUNTTYPE;
            }
            set
            {
                _mSGACCOUNTTYPE = value;
            }
        }

        private string _rESULT;
        public string RESULT
        {
            get
            {
                return _rESULT;
            }
            set
            {
                _rESULT = value;
            }
        }

    }
}
