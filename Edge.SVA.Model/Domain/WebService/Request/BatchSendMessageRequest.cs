﻿using System;
using System.Collections.Generic;

namespace Edge.SVA.Model.Domain.WebService.Request
{

    public class BatchSendMessageRequest
    {

        private string _action;
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
            }
        }

        private string _type;
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        private string _language;
        public string Language
        {
            get
            {
                return _language;
            }
            set
            {
                _language = value;
            }
        }

        private MemberAccountInfo[] _memberAccountInfo;
        public MemberAccountInfo[] MemberAccountInfo
        {
            get
            {
                return _memberAccountInfo;
            }
            set
            {
                _memberAccountInfo = value;
            }
        }

    }
}
