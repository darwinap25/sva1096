﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;

namespace Edge.SVA.Model.Domain.Operation
{
    public class CardAdjustViewModel
    {
        Ord_CardAdjust_H mainTable = new Ord_CardAdjust_H();

        public Ord_CardAdjust_H MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        private DataTable cardTable;

        public DataTable CardTable
        {
            get { return cardTable; }
            set { cardTable = value; }
        }

        //Add By Nathan 20140609 ++
        private List<KeyValue> cardList = new List<KeyValue>();

        public List<KeyValue> CardList
        {
            get { return cardList; }
            set { cardList = value; }
        }

        private List<KeyValue> batchCardIDList = new List<KeyValue>();

        public List<KeyValue> BatchCardIDList
        {
            get { return batchCardIDList; }
            set { batchCardIDList = value; }
        }

        //Add By Nathan 20140609 --
    }
}
