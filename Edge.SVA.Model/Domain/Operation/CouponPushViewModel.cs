﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.Model.Domain.Operation
{
    public class CouponPushViewModel
    {
        Ord_CouponPush_D searchTable = new Ord_CouponPush_D();

        public Ord_CouponPush_D SearchTable
        {
            get { return searchTable; }
            set { searchTable = value; }
        }

        private DataTable couponTable;

        public DataTable CouponTable
        {
            get { return couponTable; }
            set { couponTable = value; }
        }

        private int couponQty;

        public int CouponQty
        {
            get { return couponQty; }
            set { couponQty = value; }
        }

    }
}
