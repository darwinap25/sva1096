﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain.Operation
{
    public class ImportMemberInfos1ViewModel
    {
        Ord_CreateMember_H mainTable = new Ord_CreateMember_H();

        public Ord_CreateMember_H MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        Ord_CreateMember_D subTable = new Ord_CreateMember_D();

        public Ord_CreateMember_D SubTable
        {
            get { return subTable; }
            set { subTable = value; }
        }
    }
}
