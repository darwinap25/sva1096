﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.Model.Domain.Operation
{
   public class CardChangePointsViewModel
    {
        Ord_TradeManually_H mainTable = new Ord_TradeManually_H();

        public Ord_TradeManually_H MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        DataTable subTable = new DataTable();

        public DataTable SubTable
        {
            get { return subTable; }
            set { subTable = value; }
        }

        List<Ord_TradeManually_D> tradeList = new List<Ord_TradeManually_D>();

        public List<Ord_TradeManually_D> TradeList
        {
            get { return tradeList; }
            set { tradeList = value; }
        }
    }
}
