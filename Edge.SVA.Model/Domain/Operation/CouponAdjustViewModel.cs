﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.SVA.Model.Domain.WebInterfaces;
using Edge.SVA.Model.Domain.Surpport;
using System.Data;

namespace Edge.SVA.Model.Domain.Operation
{
    public class CouponAdjustViewModel
    {
        Ord_CouponAdjust_H mainTable = new Ord_CouponAdjust_H();

        public Ord_CouponAdjust_H MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        private List<KeyValue> couponTypeList = new List<KeyValue>();

        public List<KeyValue> CouponTypeList
        {
            get { return couponTypeList; }
            set { couponTypeList = value; }
        }

        private List<KeyValue> batchCouponIDList = new List<KeyValue>();

        public List<KeyValue> BatchCouponIDList
        {
            get { return batchCouponIDList; }
            set { batchCouponIDList = value; }
        }

        private DataTable couponTable ;

        public DataTable CouponTable
        {
            get { return couponTable; }
            set { couponTable = value; }
        }

        private double couponTypeAmount;

        public double CouponTypeAmount
        {
            get { return couponTypeAmount; }
            set { couponTypeAmount = value; }
        }
    }

}
