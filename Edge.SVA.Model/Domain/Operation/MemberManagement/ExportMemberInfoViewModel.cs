﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.Model.Domain.Operation.MemberManagement
{
   public class ExportMemberInfoViewModel
    {
        DataTable mainTable = new DataTable();

        public DataTable MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }
    }
}
