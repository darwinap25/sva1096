﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Edge.SVA.Model.Domain.Operation
{
   public class MemberOrderViewModel
    {
        Sales_H mainTable = new Sales_H();

        public Sales_H MainTable
        {
            get { return mainTable; }
            set { mainTable = value; }
        }

        DataTable detailTable = new DataTable();

        public DataTable DetailTable
        {
            get { return detailTable; }
            set { detailTable = value; }
        }

        DataTable tendTable = new DataTable();

        public DataTable TendTable
        {
            get { return tendTable; }
            set { tendTable = value; }
        }

        private DataTable headTable = new DataTable();

        public DataTable HeadTable
        {
            get { return headTable; }
            set { headTable = value; }
        }

        private DataTable memberTable = new DataTable();

        public DataTable MemberTable
        {
            get { return memberTable; }
            set { memberTable = value; }
        }
    }
}
