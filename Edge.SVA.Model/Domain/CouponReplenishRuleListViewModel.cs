﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Domain
{
    public class CouponReplenishRuleListViewModel
    {
        private List<CouponReplenishRuleViewModel> couponReplenishRuleViewModelList = new List<CouponReplenishRuleViewModel>();
        
        public List<CouponReplenishRuleViewModel> CouponReplenishRuleViewModelList
        {
            get { return couponReplenishRuleViewModelList; }
        }
    }
}
