﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵大类表。
	///1：现金劵。2：折扣优惠劵。3：货品兑换卷。4：印花
	/// </summary>
	[Serializable]
	public partial class CouponTypeNature
	{
		public CouponTypeNature()
		{}
		#region Model
		private int _coupontypenatureid;
		private string _coupontypenaturename1;
		private string _coupontypenaturename2;
		private string _coupontypenaturename3;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CouponTypeNatureID
		{
			set{ _coupontypenatureid=value;}
			get{return _coupontypenatureid;}
		}
		/// <summary>
		/// 优惠劵类型种类名称1
		/// </summary>
		public string CouponTypeNatureName1
		{
			set{ _coupontypenaturename1=value;}
			get{return _coupontypenaturename1;}
		}
		/// <summary>
		/// 优惠劵类型种类名称2
		/// </summary>
		public string CouponTypeNatureName2
		{
			set{ _coupontypenaturename2=value;}
			get{return _coupontypenaturename2;}
		}
		/// <summary>
		/// 优惠劵类型种类名称3
		/// </summary>
		public string CouponTypeNatureName3
		{
			set{ _coupontypenaturename3=value;}
			get{return _coupontypenaturename3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

