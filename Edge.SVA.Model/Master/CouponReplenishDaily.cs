﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Coupon每日自动补货设置表
	/// </summary>
	[Serializable]
	public partial class CouponReplenishDaily
	{
		public CouponReplenishDaily()
		{}
		#region Model
		private int _keyid;
		private int _coupontypeid;
		private DateTime? _starttime;
		private DateTime? _endtime;
		private int? _maxreplenishcount;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private int? _weekdaynum;
		/// <summary>
		/// 自增长ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// CouponType ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 起始时间
		/// </summary>
		public DateTime? StartTime
		{
			set{ _starttime=value;}
			get{return _starttime;}
		}
		/// <summary>
		/// 结束时间
		/// </summary>
		public DateTime? EndTime
		{
			set{ _endtime=value;}
			get{return _endtime;}
		}
		/// <summary>
		/// 最大补货数量
		/// </summary>
		public int? MaxReplenishCount
		{
			set{ _maxreplenishcount=value;}
			get{return _maxreplenishcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? WeekDayNum
		{
			set{ _weekdaynum=value;}
			get{return _weekdaynum;}
		}
		#endregion Model

	}
}

