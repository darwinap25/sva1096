﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 每个CardGrade 允许持有的指定 coupontype 的数量。
	/// </summary>
	[Serializable]
	public partial class CardGradeHoldCouponRule
	{
		public CardGradeHoldCouponRule()
		{}
		#region Model
		private int _keyid;
		private int _cardgradeid;
		private int _coupontypeid;
		private int? _holdcount=0;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private int? _ruletype;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 卡等级ID
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// coupontypeid
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 允许持有数量
		/// </summary>
		public int? HoldCount
		{
			set{ _holdcount=value;}
			get{return _holdcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RuleType
		{
			set{ _ruletype=value;}
			get{return _ruletype;}
		}
		#endregion Model

	}
}

