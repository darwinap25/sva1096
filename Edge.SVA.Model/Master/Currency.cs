﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 币种表
	/// </summary>
	[Serializable]
	public partial class Currency
	{
		public Currency()
		{}
		#region Model
		private int _currencyid;
		private string _currencycode;
		private string _currencyname1;
		private string _currencyname2;
		private string _currencyname3;
		private decimal? _currencyrate=1M;
		private int? _islocalcurrency=1;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CurrencyID
		{
			set{ _currencyid=value;}
			get{return _currencyid;}
		}
		/// <summary>
		/// 币种编码
		/// </summary>
		public string CurrencyCode
		{
			set{ _currencycode=value;}
			get{return _currencycode;}
		}
		/// <summary>
		/// 币种名称1
		/// </summary>
		public string CurrencyName1
		{
			set{ _currencyname1=value;}
			get{return _currencyname1;}
		}
		/// <summary>
		/// 币种名称2
		/// </summary>
		public string CurrencyName2
		{
			set{ _currencyname2=value;}
			get{return _currencyname2;}
		}
		/// <summary>
		/// 币种名称3
		/// </summary>
		public string CurrencyName3
		{
			set{ _currencyname3=value;}
			get{return _currencyname3;}
		}
		/// <summary>
		/// 与本币汇率。 如果是本币，值为1
		/// </summary>
		public decimal? CurrencyRate
		{
			set{ _currencyrate=value;}
			get{return _currencyrate;}
		}
		/// <summary>
		/// 是否本币。默认1
		/// </summary>
		public int? IsLocalCurrency
		{
			set{ _islocalcurrency=value;}
			get{return _islocalcurrency;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

