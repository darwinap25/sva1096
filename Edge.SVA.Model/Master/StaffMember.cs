﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Master
{

    public class StaffMember
    {

        public StaffMember()
        {

        }


        private string _staffno;
        private string _company;
        private string _engname;
        private string _alias;
        private DateTime? _joined;
        private DateTime? _resigned;
        private string _sex;
        private string _dob;
        private string _emailaddr1;
        private string _emailaddr2;
        private string _mobilenumber;


        public string StaffNo
        {
            set { _staffno = value; }
            get { return _staffno; }
        }

        public string Company
        {
            set { _company = value; }
            get { return _company; }
        }


        public string EngName
        {
            set { _engname = value; }
            get { return _engname; }
        }


        public string Alias
        {
            set { _alias = value; }
            get { return _alias; }
        }


        public DateTime? Joined
        {
            set { _joined = value; }
            get { return _joined; }
        }


        public DateTime? Resigned
        {
            set { _resigned = value; }
            get { return _resigned; }
        }

        public string Sex
        {
            set { _sex = value; }
            get { return _sex; }
        }


        public string DOB
        {
            set { _dob = value; }
            get { return _dob; }
        }


        public string EmailAddr1
        {
            set { _emailaddr1 = value; }
            get { return _emailaddr1; }
        }

        public string EmailAddr2
        {
            set { _emailaddr2 = value; }
            get { return _emailaddr2; }
        }


        public string MobileNumber
        {
            set { _mobilenumber = value; }
            get { return _mobilenumber; }
        }

    }
}
