﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 面向供应商的订货单，明细表
    ///如果要求向供应商提供CouponNumber。  并且要求每个Number都记录下来。 那么OrderQty=1， FirstCouponNumber=EndCouponNumber
    /// </summary>
    [Serializable]
    public partial class Ord_OrderToSupplier_D
    {
        public Ord_OrderToSupplier_D()
        { }
        #region Model
        private int _keyid;
        private string _ordersuppliernumber;
        private int? _coupontypeid;
        private int? _orderqty;
        private string _firstcouponnumber;
        private string _endcouponnumber;
        private string _batchcouponcode;
        private int? _packageqty = 1;
        private int? _orderroundupqty = 1;
        /// <summary>
        /// 自增长主键
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 提交供应商订单单号，外键
        /// </summary>
        public string OrderSupplierNumber
        {
            set { _ordersuppliernumber = value; }
            get { return _ordersuppliernumber; }
        }
        /// <summary>
        /// 优惠劵类型ID
        /// </summary>
        public int? CouponTypeID
        {
            set { _coupontypeid = value; }
            get { return _coupontypeid; }
        }
        /// <summary>
        /// 订货数量
        /// </summary>
        public int? OrderQty
        {
            set { _orderqty = value; }
            get { return _orderqty; }
        }
        /// <summary>
        /// 提供给供应商的首coupon号。（如果IsProvideNumber=1，那么需要提供Coupon号码）
        /// </summary>
        public string FirstCouponNumber
        {
            set { _firstcouponnumber = value; }
            get { return _firstcouponnumber; }
        }
        /// <summary>
        /// 提供给供应商的末coupon号
        /// </summary>
        public string EndCouponNumber
        {
            set { _endcouponnumber = value; }
            get { return _endcouponnumber; }
        }
        /// <summary>
        /// FirstCouponNumber的批次号编码
        /// </summary>
        public string BatchCouponCode
        {
            set { _batchcouponcode = value; }
            get { return _batchcouponcode; }
        }
        /// <summary>
        /// RRG 要求增加，保存多少包
        /// </summary>
        public int? PackageQty
        {
            set { _packageqty = value; }
            get { return _packageqty; }
        }
        /// <summary>
        /// RRG 要求增加，保存一包中有多少张
        /// </summary>
        public int? OrderRoundUpQty
        {
            set { _orderroundupqty = value; }
            get { return _orderroundupqty; }
        }
        #endregion Model

    }
}

