﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡等级表
	/// </summary>
	[Serializable]
	public partial class CardIssueHistory
	{
		public CardIssueHistory()
		{}
		#region Model
		private int _cardissuetxnid;
		private string _cardnumber;
		private string _cardtypeid;
		private string _cardissuenotes;
		private DateTime? _createdon;
		private DateTime? _updatedon;
		private string _updatedby;
		/// <summary>
		/// 
		/// </summary>
		public int CardIssueTxnID
		{
			set{ _cardissuetxnid=value;}
			get{return _cardissuetxnid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardIssueNotes
		{
			set{ _cardissuenotes=value;}
			get{return _cardissuenotes;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

