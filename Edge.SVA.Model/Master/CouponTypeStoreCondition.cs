﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵的店铺条件
	/// </summary>
	[Serializable]
	public partial class CouponTypeStoreCondition
	{
		public CouponTypeStoreCondition()
		{}
		#region Model
		private int _coupontypestoreconditionid;
		private int _coupontypeid;
		private int? _storeconditiontype;
		private int? _conditiontype;
		private int? _conditionid;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CouponTypeStoreConditionID
		{
			set{ _coupontypestoreconditionid=value;}
			get{return _coupontypestoreconditionid;}
		}
		/// <summary>
		/// coupontype ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 条件类型。 1：发布店铺。2：使用店铺
		/// </summary>
		public int? StoreConditionType
		{
			set{ _storeconditiontype=value;}
			get{return _storeconditiontype;}
		}
		/// <summary>
		/// 输入的条件类型。1：brand。2：Location。3：store
		/// </summary>
		public int? ConditionType
		{
			set{ _conditiontype=value;}
			get{return _conditiontype;}
		}
		/// <summary>
		/// 条件具体的ID。 例如：ConditionType=1时，输入的为brandID
		/// </summary>
		public int? ConditionID
		{
			set{ _conditionid=value;}
			get{return _conditionid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

