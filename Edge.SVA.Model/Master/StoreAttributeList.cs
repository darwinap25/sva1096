﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 店铺的属性列表 （store 子表）
	/// </summary>
	[Serializable]
	public partial class StoreAttributeList
	{
		public StoreAttributeList()
		{}
		#region Model
		private int _keyid;
		private int _storeid;
		private int _said;
		/// <summary>
		/// 自增长ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 店铺主键
		/// </summary>
		public int StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		/// <summary>
		/// 属性表主键
		/// </summary>
		public int SAID
		{
			set{ _said=value;}
			get{return _said;}
		}
		#endregion Model

	}
}

