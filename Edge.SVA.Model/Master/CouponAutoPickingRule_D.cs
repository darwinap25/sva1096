﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// Coupon订单（店铺给总部的）自动产生pickup单的规则设置表   子表
    ///（BrandID和storeID ，两者只需填其一）
    /// </summary>
    [Serializable]
    public partial class CouponAutoPickingRule_D
    {
        public CouponAutoPickingRule_D()
        { }
        #region Model
        private int _keyid;
        private string _couponautopickingrulecode;
        private int? _brandid;
        private int? _storeid;
        /// <summary>
        /// 自增长主键
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 优惠劵订单自动提货规则编码
        /// </summary>
        public string CouponAutoPickingRuleCode
        {
            set { _couponautopickingrulecode = value; }
            get { return _couponautopickingrulecode; }
        }
        /// <summary>
        /// 品牌ID
        /// </summary>
        public int? BrandID
        {
            set { _brandid = value; }
            get { return _brandid; }
        }
        /// <summary>
        /// 店铺ID
        /// </summary>
        public int? StoreID
        {
            set { _storeid = value; }
            get { return _storeid; }
        }
        #endregion Model

    }
}

