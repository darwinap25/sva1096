﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡发行方
	/// </summary>
	[Serializable]
	public partial class CardIssuer
	{
		public CardIssuer()
		{}
		#region Model
		private int _cardissuerid;
		private string _cardissuername1;
		private string _cardissuername2;
		private string _cardissuername3;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		private string _cardissuerdesc1;
		private string _cardissuerdesc2;
		private string _cardissuerdesc3;
		private int? _domesticcurrencyid;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int CardIssuerID
		{
			set{ _cardissuerid=value;}
			get{return _cardissuerid;}
		}
		/// <summary>
		/// 发行方名称1
		/// </summary>
		public string CardIssuerName1
		{
			set{ _cardissuername1=value;}
			get{return _cardissuername1;}
		}
		/// <summary>
		/// 发行方名称2
		/// </summary>
		public string CardIssuerName2
		{
			set{ _cardissuername2=value;}
			get{return _cardissuername2;}
		}
		/// <summary>
		/// 发行方名称3
		/// </summary>
		public string CardIssuerName3
		{
			set{ _cardissuername3=value;}
			get{return _cardissuername3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardIssuerDesc1
		{
			set{ _cardissuerdesc1=value;}
			get{return _cardissuerdesc1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardIssuerDesc2
		{
			set{ _cardissuerdesc2=value;}
			get{return _cardissuerdesc2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardIssuerDesc3
		{
			set{ _cardissuerdesc3=value;}
			get{return _cardissuerdesc3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DomesticCurrencyID
		{
			set{ _domesticcurrencyid=value;}
			get{return _domesticcurrencyid;}
		}
		#endregion Model

	}
}

