﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 积分计算规则的货品条件。
	/// </summary>
	[Serializable]
	public partial class PointRule_Item
	{
		public PointRule_Item()
		{}
		#region Model
		private int _keyid;
		private string _pointrulecode;
		private int _entitytype;
		private string _entitynum;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 积分规则编码
		/// </summary>
		public string PointRuleCode
		{
			set{ _pointrulecode=value;}
			get{return _pointrulecode;}
		}
		/// <summary>
		/// 积分规则适用类型
        //1、EntityNum内容为ProductBrandCode
        //2、EntityNum内容为DepartCode
        //3、EntityNum内容为ProdCode
		/// </summary>
		public int EntityType
		{
			set{ _entitytype=value;}
			get{return _entitytype;}
		}
		/// <summary>
		/// 积分规则适用编码
		/// </summary>
		public string EntityNum
		{
			set{ _entitynum=value;}
			get{return _entitynum;}
		}
		#endregion Model

	}
}

