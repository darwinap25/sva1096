﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Master
{
    [Serializable]
    public partial class Ord_ImportMemberSegment_ImportErrors
    {
        public Ord_ImportMemberSegment_ImportErrors()
        { }
        #region Model

        private string _importmembersegmentnumber;
        private string _rrccardnumber;
        private string _externalbuyingunit;


        public string ImportMemberSegmentNumber
        {
            set { _importmembersegmentnumber = value; }
            get { return _importmembersegmentnumber; }
        }

        public string RRC_CardNumber
        {
            set { _rrccardnumber = value; }
            get { return _rrccardnumber; }
        }

        public string ExtrenalBuyingUnit
        {
            set { _externalbuyingunit = value; }
            get { return _externalbuyingunit; }
        }
        #endregion
    }
}
