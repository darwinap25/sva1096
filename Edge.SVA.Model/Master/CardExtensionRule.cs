﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡有效期延长规则
	///   增值规则阶梯设置：
	///   增值满     延长时间
	///   100           90 天
	///   200           180天
	///   500           360天
	///   匹配时按照金额排序，按先匹配的执行', 
	/// </summary>
	[Serializable]
	public partial class CardExtensionRule
	{
		public CardExtensionRule()
		{}
		#region Model
		private int _extensionruleid;
		private int _ruletype=0;
		private int? _cardtypeid;
		private int? _cardgradeid;
		private int _extensionruleseqno;
		private int? _maxlimit;
		private decimal? _ruleamount;
		private int? _extension;
		private int? _extensionunit=0;
		private int? _extendtype=0;
		private DateTime? _startdate;
		private DateTime? _enddate;
		private int? _status=0;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
        private DateTime? _specifyexpirydate;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int ExtensionRuleID
		{
			set{ _extensionruleid=value;}
			get{return _extensionruleid;}
		}
		/// <summary>
		/// 规则类型：0：卡有效期延长规则。 1：金额有效期规则。 2：积分有效期规则
		/// </summary>
		public int RuleType
		{
			set{ _ruletype=value;}
			get{return _ruletype;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 卡等级ID
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 规则记录序号
		/// </summary>
		public int ExtensionRuleSeqNo
		{
			set{ _extensionruleseqno=value;}
			get{return _extensionruleseqno;}
		}
		/// <summary>
		/// 按照ExtensionUnit单位， 最大增加数
		/// </summary>
		public int? MaxLimit
		{
			set{ _maxlimit=value;}
			get{return _maxlimit;}
		}
		/// <summary>
		/// 增值金额
		/// </summary>
		public decimal? RuleAmount
		{
			set{ _ruleamount=value;}
			get{return _ruleamount;}
		}
		/// <summary>
		/// Expirydate 延长日期，单位看ExtendDateUnit
		/// </summary>
		public int? Extension
		{
			set{ _extension=value;}
			get{return _extension;}
		}
		/// <summary>
		/// 有效期延长单位 0：永久。 1：年。 2：月。 3：星期。 4：天。5: 有效期不变更 6: 指定有效期。
		/// </summary>
		public int? ExtensionUnit
		{
			set{ _extensionunit=value;}
			get{return _extensionunit;}
		}
		/// <summary>
		/// 有效期延长方式：0：增值时重置有效期。（以当前日期为基础增加有效期） 
        ///1：增值时延长原有有效期。（以原有有效期为基础增加有效期） 
        ///2：增值时不变更有效期，忽略cardtype，coupontype的有效期延长设置。
		/// </summary>
		public int? ExtendType
		{
			set{ _extendtype=value;}
			get{return _extendtype;}
		}
		/// <summary>
		/// 开始日期
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		/// 结束日期
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		/// 0： 无效。  1：有效
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
        /// <summary>
        /// 
        /// </summary>
        public DateTime? SpecifyExpiryDate
        {
            set { _specifyexpirydate = value; }
            get { return _specifyexpirydate; }
        }
		#endregion Model

	}
}

