﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Master
{
    [Serializable]
    public partial class Ord_ImportMemberSegment_H
    {
        public Ord_ImportMemberSegment_H()
        { }
        #region Model

        private string _importmembersegmentnumber;
        private string _description;
        private string _note;
        private string _approvalcode;
        private string _approvestatus;
        private string _action;
        private string _filename;
        private string _importstatus;
        private DateTime? _approveon;
        private string _approveby;
        private DateTime? _createdon = DateTime.Now;
        private string _createdby;
        private DateTime? _updatedon = DateTime.Now;
        private string _updatedby;
        private DateTime? _createdbusdate;
        private DateTime? _approvebusdate;


        public string ImportMemberSegmentNumber
        {
            set { _importmembersegmentnumber = value; }
            get { return _importmembersegmentnumber; }
        }

        /// <summary>
        /// 单据描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// Remarks
        /// </summary>
        public string Note
        {
            set { _note = value; }
            get { return _note; }
        }
        /// <summary>
        /// 批核时产生授权号，并通知前台
        /// </summary>
        public string ApprovalCode
        {
            set { _approvalcode = value; }
            get { return _approvalcode; }
        }
        /// <summary>
        /// 单据状态。状态： P：prepare。  A:Approve 。 V：Void
        /// </summary>
        public string ApproveStatus
        {
            set { _approvestatus = value; }
            get { return _approvestatus; }
        }

        /// <summary>
        /// Segment Ation： A: Attach。  D: Detach
        /// </summary>
        public string Action
        {
            set { _action = value; }
            get { return _action; }
        }

        /// <summary>
        /// filename
        /// </summary>
        public string FileName
        {
            set { _filename = value; }
            get { return _filename; }
        }

        /// <summary>
        /// 单据状态。状态： S：SUCCESS。  F: FAILED
        /// </summary>
        public string ImportStatus
        {
            set { _importstatus = value; }
            get { return _importstatus; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ApproveOn
        {
            set { _approveon = value; }
            get { return _approveon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ApproveBy
        {
            set { _approveby = value; }
            get { return _approveby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        /// <summary>
        /// 单据创建时的busdate
        /// </summary>
        public DateTime? CreatedBusDate
        {
            set { _createdbusdate = value; }
            get { return _createdbusdate; }
        }
        /// <summary>
        /// 单据批核时的busdate
        /// </summary>
        public DateTime? ApproveBusDate
        {
            set { _approvebusdate = value; }
            get { return _approvebusdate; }
        }
        #endregion Model

    }
}

