﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Card号码和UID对照表。
	///（UID：客户提供的卡号码）
	/// </summary>
	[Serializable]
	public partial class CardUIDMap
	{
		public CardUIDMap()
		{}
		#region Model
		private string _carduid;
		private string _importcardnumber;
		private int? _batchcardid;
		private int? _cardgradeid;
		private string _cardnumber;
		private int? _status;
		private DateTime? _createdon;
		private int? _createdby;
		/// <summary>
		/// 优惠劵导入号码，主键
		/// </summary>
		public string CardUID
		{
			set{ _carduid=value;}
			get{return _carduid;}
		}
		/// <summary>
		/// 单据号码
		/// </summary>
		public string ImportCardNumber
		{
			set{ _importcardnumber=value;}
			get{return _importcardnumber;}
		}
		/// <summary>
		/// Card的 batchID
		/// </summary>
		public int? BatchCardID
		{
			set{ _batchcardid=value;}
			get{return _batchcardid;}
		}
		/// <summary>
		/// 指定的CardGradeID
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 绑定的CardNumber
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 状态。0：无效。 1：有效
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		#endregion Model

	}
}

