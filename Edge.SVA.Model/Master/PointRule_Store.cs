﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 积分规则适用店铺表。
	///（规则： 如果一个店铺有指定规则， 那么就用指定规则， 通用规则不能用。   只有没有指定规则时，才可以用通用规则）
	/// </summary>
	[Serializable]
	public partial class PointRule_Store
	{
		public PointRule_Store()
		{}
		#region Model
		private int _keyid;
		private string _pointrulecode;
		private int _storeid;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 积分规则编码
		/// </summary>
		public string PointRuleCode
		{
			set{ _pointrulecode=value;}
			get{return _pointrulecode;}
		}
		/// <summary>
		/// 店铺ID
		/// </summary>
		public int StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		#endregion Model

	}
}

