﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 导入Card的UID。
	/// </summary>
	[Serializable]
	public partial class Ord_ImportCardUID_H
	{
		public Ord_ImportCardUID_H()
		{}
		#region Model
		private string _importcardnumber;
		private string _importcarddesc1;
		private string _importcarddesc2;
		private string _importcarddesc3;
		private int? _needactive=0;
		private int? _neednewbatch=1;
		private int? _cardcount;
		private string _approvalcode;
		private string _approvestatus;
		private DateTime? _approveon;
		private int? _approveby;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		private DateTime? _createdbusdate;
		private DateTime? _approvebusdate;
		/// <summary>
		/// 主键。导入单号
		/// </summary>
		public string ImportCardNumber
		{
			set{ _importcardnumber=value;}
			get{return _importcardnumber;}
		}
		/// <summary>
		/// 单据描述1
		/// </summary>
		public string ImportCardDesc1
		{
			set{ _importcarddesc1=value;}
			get{return _importcarddesc1;}
		}
		/// <summary>
		/// 单据描述2
		/// </summary>
		public string ImportCardDesc2
		{
			set{ _importcarddesc2=value;}
			get{return _importcarddesc2;}
		}
		/// <summary>
		/// 单据描述3
		/// </summary>
		public string ImportCardDesc3
		{
			set{ _importcarddesc3=value;}
			get{return _importcarddesc3;}
		}
		/// <summary>
		/// 绑定时激活Card。0：不激活。1：激活
		/// </summary>
		public int? NeedActive
		{
			set{ _needactive=value;}
			get{return _needactive;}
		}
		/// <summary>
		/// 是否先创建新批次的Card，使用这些新的Card来绑定。
        //0：不创建，使用已有的Card，没有足够Card则报错。
///1：先创建此数量的Card，再绑定这些Card。
		/// </summary>
		public int? NeedNewBatch
		{
			set{ _neednewbatch=value;}
			get{return _neednewbatch;}
		}
		/// <summary>
		/// Card的数量
		/// </summary>
		public int? CardCount
		{
			set{ _cardcount=value;}
			get{return _cardcount;}
		}
		/// <summary>
		/// 批核时产生授权号，并通知前台
		/// </summary>
		public string ApprovalCode
		{
			set{ _approvalcode=value;}
			get{return _approvalcode;}
		}
		/// <summary>
		/// 单据状态。状态： P：prepare。  A:Approve 。 V：Void
		/// </summary>
		public string ApproveStatus
		{
			set{ _approvestatus=value;}
			get{return _approvestatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ApproveOn
		{
			set{ _approveon=value;}
			get{return _approveon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ApproveBy
		{
			set{ _approveby=value;}
			get{return _approveby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 单据创建时的busdate
		/// </summary>
		public DateTime? CreatedBusDate
		{
			set{ _createdbusdate=value;}
			get{return _createdbusdate;}
		}
		/// <summary>
		/// 单据批核时的busdate
		/// </summary>
		public DateTime? ApproveBusDate
		{
			set{ _approvebusdate=value;}
			get{return _approvebusdate;}
		}
		#endregion Model

	}
}

