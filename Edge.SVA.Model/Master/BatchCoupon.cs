﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Coupon批次表
	/// </summary>
	[Serializable]
	public partial class BatchCoupon
	{
		public BatchCoupon()
		{}
		#region Model
		private int _batchcouponid;
		private string _batchcouponcode;
		private int? _seqfrom;
		private int? _seqto;
		private int _qty=1;
		private int _coupontypeid;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int BatchCouponID
		{
			set{ _batchcouponid=value;}
			get{return _batchcouponid;}
		}
		/// <summary>
		/// 批次ID。 组成规则： CardTypeID（或CouponTypeID） + 序号
		/// </summary>
		public string BatchCouponCode
		{
			set{ _batchcouponcode=value;}
			get{return _batchcouponcode;}
		}
		/// <summary>
		/// 序号范围（开始序号）
		/// </summary>
		public int? SeqFrom
		{
			set{ _seqfrom=value;}
			get{return _seqfrom;}
		}
		/// <summary>
		/// 序号范围（结束序号）
		/// </summary>
		public int? SeqTo
		{
			set{ _seqto=value;}
			get{return _seqto;}
		}
		/// <summary>
		/// 批次创建数量
		/// </summary>
		public int Qty
		{
			set{ _qty=value;}
			get{return _qty;}
		}
		/// <summary>
		/// 优惠劵类型ID，外键
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

