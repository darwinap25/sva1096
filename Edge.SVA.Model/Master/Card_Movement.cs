﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Card2ù×÷μ?á÷??±í?￡
	///í¨1yinsert′￥·￠?÷￡????ó′?±í????à′2ù×÷Card±í￡?CardCashDetail±í?￡
	/// </summary>
	[Serializable]
	public partial class Card_Movement
	{
		public Card_Movement()
		{}
		#region Model
		private int _keyid;
		private int _oprid;
		private string _cardnumber;
		private int? _refkeyid;
		private int? _refreceivekeyid;
		private string _reftxnno;
		private decimal _openbal;
		private decimal _amount;
		private decimal _closebal;
		private int? _points;
		private DateTime? _busdate;
		private DateTime? _txndate;
		private DateTime? _orgexpirydate;
		private DateTime? _newexpirydate;
		private int? _orgstatus;
		private int? _newstatus;
		private int? _cardcashdetailid;
		private int? _cardpointdetailid;
		private int? _tenderid;
		private string _additional;
		private string _remark;
		private string _approvalcode;
		private string _securitycode;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private int? _storeid;
		private string _servercode;
		private string _registercode;
		private int? _openpoint=0;
		private int? _closepoint=0;
		/// <summary>
		/// ×ó??3¤?÷?ü?￡
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 2ù×÷ID?￡￡¨??receiveTxn?DOprID￡?
		/// </summary>
		public int OprID
		{
			set{ _oprid=value;}
			get{return _oprid;}
		}
		/// <summary>
		/// ?¨o?
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// è?1?óD?à1?áa????￡??òì?D′Card_movement μ?1?áa????μ?ID
		/// </summary>
		public int? RefKeyID
		{
			set{ _refkeyid=value;}
			get{return _refkeyid;}
		}
		/// <summary>
		/// ì?D′1?áaμ?ReceiveTxnμ?KeyID?￡
		/// </summary>
		public int? RefReceiveKeyID
		{
			set{ _refreceivekeyid=value;}
			get{return _refreceivekeyid;}
		}
		/// <summary>
		/// ì?D′1?áaμ?Receivetxnμ?TxnNo?￡
		/// </summary>
		public string RefTxnNo
		{
			set{ _reftxnno=value;}
			get{return _reftxnno;}
		}
		/// <summary>
		/// 2ù×÷???°?¨óà???￡?e??
		/// </summary>
		public decimal OpenBal
		{
			set{ _openbal=value;}
			get{return _openbal;}
		}
		/// <summary>
		/// 2ù×÷êy???￡ ?ù?YOprID￡?′?′|?é?üê??e???ò???y·?
		/// </summary>
		public decimal Amount
		{
			set{ _amount=value;}
			get{return _amount;}
		}
		/// <summary>
		/// 2ù×÷??oó?¨μ?óà?? ￡¨?e??￡?
		/// </summary>
		public decimal CloseBal
		{
			set{ _closebal=value;}
			get{return _closebal;}
		}
		/// <summary>
		/// 2ù×÷?y·??μ
		/// </summary>
		public int? Points
		{
			set{ _points=value;}
			get{return _points;}
		}
		/// <summary>
		/// ??ò×è??ú
		/// </summary>
		public DateTime? BusDate
		{
			set{ _busdate=value;}
			get{return _busdate;}
		}
		/// <summary>
		/// ??ò×ê±??
		/// </summary>
		public DateTime? Txndate
		{
			set{ _txndate=value;}
			get{return _txndate;}
		}
		/// <summary>
		/// 2ù×÷?°?¨μ?óDD§?ú￡???receivetxn?Dμ?OrgExpiryDate
		/// </summary>
		public DateTime? OrgExpiryDate
		{
			set{ _orgexpirydate=value;}
			get{return _orgexpirydate;}
		}
		/// <summary>
		/// 2ù×÷?°?¨μ?óDD§?ú￡???receivetxn?Dμ?NewExpiryDate
		/// </summary>
		public DateTime? NewExpiryDate
		{
			set{ _newexpirydate=value;}
			get{return _newexpirydate;}
		}
		/// <summary>
		/// ′?2ù×÷?°Card×′ì?
		/// </summary>
		public int? OrgStatus
		{
			set{ _orgstatus=value;}
			get{return _orgstatus;}
		}
		/// <summary>
		/// ′?2ù×÷oócard×′ì?
		/// </summary>
		public int? NewStatus
		{
			set{ _newstatus=value;}
			get{return _newstatus;}
		}
		/// <summary>
		/// è?1?ê??àóDD§?ú￡??òDèòaì?D′CardCashDetail±í￡??aà?ì?D′CardCashDetail±íμ?D???ID
		/// </summary>
		public int? CardCashDetailID
		{
			set{ _cardcashdetailid=value;}
			get{return _cardcashdetailid;}
		}
		/// <summary>
		/// è?1?ê??y·??àóDD§?ú￡??òDèòaì?D′CardPointDetail±í￡??aà?ì?D′CardPointDetail±íμ?D???ID
		/// </summary>
		public int? CardPointDetailID
		{
			set{ _cardpointdetailid=value;}
			get{return _cardpointdetailid;}
		}
		/// <summary>
		/// ?§??′ú??
		/// </summary>
		public int? TenderID
		{
			set{ _tenderid=value;}
			get{return _tenderid;}
		}
		/// <summary>
		/// ???óD??￠?￡′ó????ò?DD?¨×a??ê±￡?????ò?DD?¨o?
		/// </summary>
		public string Additional
		{
			set{ _additional=value;}
			get{return _additional;}
		}
		/// <summary>
		/// ±?×￠
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// ê1ó?ò?DD?¨?§??oó·μ??μ?o???
		/// </summary>
		public string ApprovalCode
		{
			set{ _approvalcode=value;}
			get{return _approvalcode;}
		}
		/// <summary>
		/// D￡?é×????￡ ￡¨?¤á?￡?
		/// </summary>
		public string SecurityCode
		{
			set{ _securitycode=value;}
			get{return _securitycode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ServerCode
		{
			set{ _servercode=value;}
			get{return _servercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RegisterCode
		{
			set{ _registercode=value;}
			get{return _registercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? OpenPoint
		{
			set{ _openpoint=value;}
			get{return _openpoint;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ClosePoint
		{
			set{ _closepoint=value;}
			get{return _closepoint;}
		}
		#endregion Model

	}
}

