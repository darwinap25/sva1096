﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵绑定的兑换列表
	/// </summary>
	[Serializable]
	public partial class CouponTypeExchangeBinding
	{
		public CouponTypeExchangeBinding()
		{}
		#region Model
		private int _keyid;
		private int _coupontypeid;
		private int? _brandid;
		private string _prodcode;
		private string _departcode;
		private int _bindingtype=1;
		private string _tendercode;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 优惠劵类型ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 品牌主键
		/// </summary>
		public int? BrandID
		{
			set{ _brandid=value;}
			get{return _brandid;}
		}
		/// <summary>
		/// 货品编码。（主键）
		/// </summary>
		public string ProdCode
		{
			set{ _prodcode=value;}
			get{return _prodcode;}
		}
		/// <summary>
		/// 部门编码
		/// </summary>
		public string DepartCode
		{
			set{ _departcode=value;}
			get{return _departcode;}
		}
		/// <summary>
		/// 绑定类型。1：兑换货品的绑定。
        // 2：coupon使用条件的货品绑定。
        //3：兑换货品的绑定。 
        //4：兑换货品的绑定。（全额兑换货品。货品价格可以变动）
        //5：Tender绑定
		/// </summary>
		public int BindingType
		{
			set{ _bindingtype=value;}
			get{return _bindingtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TenderCode
		{
			set{ _tendercode=value;}
			get{return _tendercode;}
		}
		#endregion Model

	}
}

