﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// 店铺属性表基础。
    /// </summary>
    [Serializable]
    public partial class Store_Attribute
    {
        public Store_Attribute()
        { }
        #region Model
        private int _said;
        private string _sacode;
        private string _sadesc1;
        private string _sadesc2;
        private string _sadesc3;
        private string _sapic;
        /// <summary>
        /// 自增长主键
        /// </summary>
        public int SAID
        {
            set { _said = value; }
            get { return _said; }
        }
        /// <summary>
        /// 店铺属性编码
        /// </summary>
        public string SACode
        {
            set { _sacode = value; }
            get { return _sacode; }
        }
        /// <summary>
        /// 描述1
        /// </summary>
        public string SADesc1
        {
            set { _sadesc1 = value; }
            get { return _sadesc1; }
        }
        /// <summary>
        /// 描述2
        /// </summary>
        public string SADesc2
        {
            set { _sadesc2 = value; }
            get { return _sadesc2; }
        }
        /// <summary>
        /// 描述3
        /// </summary>
        public string SADesc3
        {
            set { _sadesc3 = value; }
            get { return _sadesc3; }
        }
        /// <summary>
        /// 店铺属性图片
        /// </summary>
        public string SAPic
        {
            set { _sapic = value; }
            get { return _sapic; }
        }
        #endregion Model

    }
}

