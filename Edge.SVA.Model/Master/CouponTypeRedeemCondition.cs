﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵使用条件设置表。
	/// </summary>
	[Serializable]
	public partial class CouponTypeRedeemCondition
	{
		public CouponTypeRedeemCondition()
		{}
		#region Model
		private int _keyid;
		private int? _coupontypeid;
		private int? _cardtypeid;
		private int? _cardgradeid;
		private int? _redeemlimittype;
		private int? _limitvalue;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 主键，自增长
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 优惠劵类型D
		/// </summary>
		public int? CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 优惠劵使用指定的会员卡类型
		/// </summary>
		public int? CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 优惠劵使用指定的会员卡等级
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 使用Coupon的条件类型。
        //1、整单交易总金额大于等于LimitValue
        //2、会员卡累计消费金额大于等于LimitValue
        //3、会员卡积分大于等于LimitValue
        //4、交易单中，在CouponTypeExchangeBinding中指定的消费货品，总金额大于等于LimitValue
        //5、交易单中，在CouponTypeExchangeBinding中指定的消费货品，总数量大于等于LimitValue
		/// </summary>
		public int? RedeemLimitType
		{
			set{ _redeemlimittype=value;}
			get{return _redeemlimittype;}
		}
		/// <summary>
		/// 根据RedeemLimitType的不同，值为不同含义 
		/// </summary>
		public int? LimitValue
		{
			set{ _limitvalue=value;}
			get{return _limitvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

