﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 批次表
	/// </summary>
	[Serializable]
	public partial class Batch
	{
		public Batch()
		{}
		#region Model
		private int _batchid;
		private string _batchcode;
		private int? _seqfrom;
		private int? _seqto;
		private decimal? _initamount;
		private int? _qty;
		private int _batchtype;
		private string _typeid;
		private int? _active=0;
		private int? _redeem=0;
		private int? _expiry=0;
		private int? _void=0;
		private int? _abnormal;
		private DateTime? _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int BatchID
		{
			set{ _batchid=value;}
			get{return _batchid;}
		}
		/// <summary>
		/// 批次ID。 组成规则： CardTypeID（或CouponTypeID） + 序号
		/// </summary>
		public string BatchCode
		{
			set{ _batchcode=value;}
			get{return _batchcode;}
		}
		/// <summary>
		/// 序号范围（开始序号）
		/// </summary>
		public int? SeqFrom
		{
			set{ _seqfrom=value;}
			get{return _seqfrom;}
		}
		/// <summary>
		/// 序号范围（结束序号）
		/// </summary>
		public int? SeqTo
		{
			set{ _seqto=value;}
			get{return _seqto;}
		}
		/// <summary>
		/// 创建时的初始金额。
		/// </summary>
		public decimal? InitAmount
		{
			set{ _initamount=value;}
			get{return _initamount;}
		}
		/// <summary>
		/// 批次创建数量
		/// </summary>
		public int? Qty
		{
			set{ _qty=value;}
			get{return _qty;}
		}
		/// <summary>
		/// 创建批次类型：0： Card.   1：Coupon
		/// </summary>
		public int BatchType
		{
			set{ _batchtype=value;}
			get{return _batchtype;}
		}
		/// <summary>
		/// 创建的类型。 根据BatchType， 当BatchType=0，此处记录CardTypeID。当BatchType=1，此处记录CouponTypeID。
		/// </summary>
		public string TypeID
		{
			set{ _typeid=value;}
			get{return _typeid;}
		}
		/// <summary>
		/// 批次中激活数量
		/// </summary>
		public int? Active
		{
			set{ _active=value;}
			get{return _active;}
		}
		/// <summary>
		/// 批次中使用数量
		/// </summary>
		public int? Redeem
		{
			set{ _redeem=value;}
			get{return _redeem;}
		}
		/// <summary>
		/// 批次中过期数量
		/// </summary>
		public int? Expiry
		{
			set{ _expiry=value;}
			get{return _expiry;}
		}
		/// <summary>
		/// 批次中作废数量
		/// </summary>
		public int? Void
		{
			set{ _void=value;}
			get{return _void;}
		}
		/// <summary>
		/// 批次中异常数量
		/// </summary>
		public int? Abnormal
		{
			set{ _abnormal=value;}
			get{return _abnormal;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

