﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 系统用户消息通知设定。头表
	/// </summary>
	[Serializable]
	public partial class UserMessageSetting_H
	{
		public UserMessageSetting_H()
		{}
		#region Model
		private string _usermessagecode;
		private string _usermessagedesc;
		private DateTime? _startdate= DateTime.Now;
		private DateTime? _enddate= DateTime.Now;
		private int? _status;
		private int? _senduserid;
		private int? _usermessagetype;
		private string _usermessagetitle;
		private string _usermessagecontent;
		private int? _triggetype=1;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 通知编号
		/// </summary>
		public string UserMessageCode
		{
			set{ _usermessagecode=value;}
			get{return _usermessagecode;}
		}
		/// <summary>
		/// 通知描述
		/// </summary>
		public string UserMessageDesc
		{
			set{ _usermessagedesc=value;}
			get{return _usermessagedesc;}
		}
		/// <summary>
		/// 开始日期
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		/// 结束日期
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		/// 有效状态。 0：无效。1：有效
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 发送人员ID
		/// </summary>
		public int? SendUserID
		{
			set{ _senduserid=value;}
			get{return _senduserid;}
		}
		/// <summary>
		/// 用户消息大类： （选项内容固定）
		/// </summary>
		public int? UserMessageType
		{
			set{ _usermessagetype=value;}
			get{return _usermessagetype;}
		}
		/// <summary>
		/// 消息标题
		/// </summary>
		public string UserMessageTitle
		{
			set{ _usermessagetitle=value;}
			get{return _usermessagetitle;}
		}
		/// <summary>
		/// 消息内容。
		/// </summary>
		public string UserMessageContent
		{
			set{ _usermessagecontent=value;}
			get{return _usermessagecontent;}
		}
		/// <summary>
		/// 触发条件。0：创建时。  1：批核时。
		/// </summary>
		public int? TriggeType
		{
			set{ _triggetype=value;}
			get{return _triggetype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

