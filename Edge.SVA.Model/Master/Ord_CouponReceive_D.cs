﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 收货确认单子表
	///（需要逐个Coupon收货时， OrderQty=ActiveQTY=1， FirstCouponNumber=EndCouponNumber）
	/// </summary>
	[Serializable]
	public partial class Ord_CouponReceive_D
	{
		public Ord_CouponReceive_D()
		{}
		#region Model
		private int _keyid;
		private string _couponreceivenumber;
		private int? _coupontypeid;
		private string _description;
		private int? _orderqty;
		private int? _actualqty;
		private string _firstcouponnumber;
		private string _endcouponnumber;
		private string _batchcouponcode;
		private int? _couponstockstatus=2;
		private DateTime? _receivedatetime= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 订单编号，主键
		/// </summary>
		public string CouponReceiveNumber
		{
			set{ _couponreceivenumber=value;}
			get{return _couponreceivenumber;}
		}
		/// <summary>
		/// 优惠劵类型ID
		/// </summary>
		public int? CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 描述。（例如改为损坏状态时填写的具体描述）
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 订货的订单数量
		/// </summary>
		public int? OrderQTY
		{
			set{ _orderqty=value;}
			get{return _orderqty;}
		}
		/// <summary>
		/// 实际收到的数量
		/// </summary>
		public int? ActualQTY
		{
			set{ _actualqty=value;}
			get{return _actualqty;}
		}
		/// <summary>
		/// 实际收货批次的首coupon号
		/// </summary>
		public string FirstCouponNumber
		{
			set{ _firstcouponnumber=value;}
			get{return _firstcouponnumber;}
		}
		/// <summary>
		/// 实际收货批次的末coupon号
		/// </summary>
		public string EndCouponNumber
		{
			set{ _endcouponnumber=value;}
			get{return _endcouponnumber;}
		}
		/// <summary>
		/// FirstCouponNumber的批次号编码
		/// </summary>
		public string BatchCouponCode
		{
			set{ _batchcouponcode=value;}
			get{return _batchcouponcode;}
		}
		/// <summary>
		/// 收到的Coupon状况。默认 2 2. Good For Release (總部確認收貨) 3. Damaged (總部發現有優惠劵損壞)
		/// </summary>
		public int? CouponStockStatus
		{
			set{ _couponstockstatus=value;}
			get{return _couponstockstatus;}
		}
		/// <summary>
		/// 创建此明细的时间
		/// </summary>
		public DateTime? ReceiveDateTime
		{
			set{ _receivedatetime=value;}
			get{return _receivedatetime;}
		}
		#endregion Model

	}
}

