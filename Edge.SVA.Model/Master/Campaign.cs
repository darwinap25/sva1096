﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 活动表
	/// </summary>
	[Serializable]
	public partial class Campaign
	{
		public Campaign()
		{}
		#region Model
		private int _campaignid;
		private string _campaigncode;
		private string _campaignname1;
		private string _campaignname2;
		private string _campaignname3;
		private string _campaigndetail1;
		private string _campaigndetail3;
		private string _campaigndetail2;
		private int _campaigntype=1;
		private string _campaignpicfile;
		private int? _status;
		private int? _brandid;
		private DateTime? _startdate;
		private DateTime? _enddate;
		private DateTime? _createdon= DateTime.Now;
		private int? _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private int? _updatedby;
		/// <summary>
		/// 活动表主键
		/// </summary>
		public int CampaignID
		{
			set{ _campaignid=value;}
			get{return _campaignid;}
		}
		/// <summary>
		/// 活动表code
		/// </summary>
		public string CampaignCode
		{
			set{ _campaigncode=value;}
			get{return _campaigncode;}
		}
		/// <summary>
		/// 活动名称
		/// </summary>
		public string CampaignName1
		{
			set{ _campaignname1=value;}
			get{return _campaignname1;}
		}
		/// <summary>
		/// 活动名称
		/// </summary>
		public string CampaignName2
		{
			set{ _campaignname2=value;}
			get{return _campaignname2;}
		}
		/// <summary>
		/// 活动名称
		/// </summary>
		public string CampaignName3
		{
			set{ _campaignname3=value;}
			get{return _campaignname3;}
		}
		/// <summary>
		/// 活动详细描述1
		/// </summary>
		public string CampaignDetail1
		{
			set{ _campaigndetail1=value;}
			get{return _campaigndetail1;}
		}
		/// <summary>
		/// 活动详细描述3
		/// </summary>
		public string CampaignDetail3
		{
			set{ _campaigndetail3=value;}
			get{return _campaigndetail3;}
		}
		/// <summary>
		/// 活动详细描述2
		/// </summary>
		public string CampaignDetail2
		{
			set{ _campaigndetail2=value;}
			get{return _campaigndetail2;}
		}
		/// <summary>
		/// 活动类型。1：coupon。 2：promotion   默认1
		/// </summary>
		public int CampaignType
		{
			set{ _campaigntype=value;}
			get{return _campaigntype;}
		}
		/// <summary>
		/// 活动图标文件名
		/// </summary>
		public string CampaignPicFile
		{
			set{ _campaignpicfile=value;}
			get{return _campaignpicfile;}
		}
		/// <summary>
		/// 1、創建。2、已發布。3、過期。4、作廢
		/// </summary>
		public int? Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 品牌ID
		/// </summary>
		public int? BrandID
		{
			set{ _brandid=value;}
			get{return _brandid;}
		}
		/// <summary>
		/// 生效日期
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		/// 失效日期
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

