﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 销售单主表（字段暂定）
	///表中会员部分，为本次交易中使用的会员资料。（比如CU_CardNumber是本次交易使用的卡，没有指定的话使用默认值）
	/// </summary>
	[Serializable]
	public partial class Sales_H
	{
		public Sales_H()
		{}
		#region Model
		private string _txnno;
		private string _storecode;
		private string _registercode;
		private string _servercode;
		private DateTime _busdate= DateTime.Now;
		private DateTime _txndate= DateTime.Now;
		private int _salestype=0;
		private string _cashierid;
		private string _salesmanid;
		private string _channel;
		private decimal? _totalamount=0M;
		private int _status;
		private int? _memberid;
		private string _membername;
		private string _membermobilephone;
		private string _cardnumber;
		private string _memberaddress;
		private string _deliverby;
		private string _deliveryaddress;
		private DateTime? _deliverstarton;
		private DateTime? _deliverendon;
		private string _deliverylongitude;
		private string _deliverylatitude;
		private string _contact;
		private string _contactphone;
		private string _approvedby;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private string _updatedby;
		private string _approvalcode;
		private string _salesreceipt;
		private byte[] _salesreceiptbin;
		private string _brandcode;
		private string _salestag;
		private string _additional;
		private int? _logisticsproviderid;
		private DateTime? _paymentdoneon;
		private string _deliverynumber;
		private int? _pickuptype=1;
		private string _pickupstorecode;
		private int? _codflag=0;
		/// <summary>
		/// 交易单号（主键）
		/// </summary>
		public string TxnNo
		{
			set{ _txnno=value;}
			get{return _txnno;}
		}
		/// <summary>
		/// 店铺编号
		/// </summary>
		public string StoreCode
		{
			set{ _storecode=value;}
			get{return _storecode;}
		}
		/// <summary>
		/// 收银机号
		/// </summary>
		public string RegisterCode
		{
			set{ _registercode=value;}
			get{return _registercode;}
		}
		/// <summary>
		/// 服务器ID
		/// </summary>
		public string ServerCode
		{
			set{ _servercode=value;}
			get{return _servercode;}
		}
		/// <summary>
		/// 交易日期。（交易结算日期，无时间）
		/// </summary>
		public DateTime BusDate
		{
			set{ _busdate=value;}
			get{return _busdate;}
		}
		/// <summary>
		/// 交易时间。（系统时间）
		/// </summary>
		public DateTime TxnDate
		{
			set{ _txndate=value;}
			get{return _txndate;}
		}
		/// <summary>
		/// 交易类型。 默认0， 普通交易
		/// </summary>
		public int SalesType
		{
			set{ _salestype=value;}
			get{return _salestype;}
		}
		/// <summary>
		/// 收银员ID
		/// </summary>
		public string CashierID
		{
			set{ _cashierid=value;}
			get{return _cashierid;}
		}
		/// <summary>
		/// 促销员ID
		/// </summary>
		public string SalesManID
		{
			set{ _salesmanid=value;}
			get{return _salesmanid;}
		}
		/// <summary>
		/// 交易来源通道。（具体未定）
		/// </summary>
		public string Channel
		{
			set{ _channel=value;}
			get{return _channel;}
		}
		/// <summary>
		/// 交易总金额
		/// </summary>
		public decimal? TotalAmount
		{
			set{ _totalamount=value;}
			get{return _totalamount;}
		}
		/// <summary>
		/// 交易状态。
        //-- 交易状态：1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货. 5:交易完成. 6:交付运送. 8:拒收。9:已延迟
        //-- 不需要快递状态: 1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货. 5:交易完成.
        //-- 需要快递状态: 1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货. 6:交付运送. 5:交易完成. (6 和 5 之间可能插入 8, 9)
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 会员的memberID
		/// </summary>
		public int? MemberID
		{
			set{ _memberid=value;}
			get{return _memberid;}
		}
		/// <summary>
		/// 会员姓名
		/// </summary>
		public string MemberName
		{
			set{ _membername=value;}
			get{return _membername;}
		}
		/// <summary>
		/// 会员的检索号码（手机号）
		/// </summary>
		public string MemberMobilePhone
		{
			set{ _membermobilephone=value;}
			get{return _membermobilephone;}
		}
		/// <summary>
		/// 会员卡号码
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 会员地址
		/// </summary>
		public string MemberAddress
		{
			set{ _memberaddress=value;}
			get{return _memberaddress;}
		}
		/// <summary>
		/// 送货员
		/// </summary>
		public string DeliverBy
		{
			set{ _deliverby=value;}
			get{return _deliverby;}
		}
		/// <summary>
		/// 送货地址
		/// </summary>
		public string DeliveryAddress
		{
			set{ _deliveryaddress=value;}
			get{return _deliveryaddress;}
		}
		/// <summary>
		/// 起送时间
		/// </summary>
		public DateTime? DeliverStartOn
		{
			set{ _deliverstarton=value;}
			get{return _deliverstarton;}
		}
		/// <summary>
		/// 送达时间
		/// </summary>
		public DateTime? DeliverEndOn
		{
			set{ _deliverendon=value;}
			get{return _deliverendon;}
		}
		/// <summary>
		/// 送货坐标，经度
		/// </summary>
		public string DeliveryLongitude
		{
			set{ _deliverylongitude=value;}
			get{return _deliverylongitude;}
		}
		/// <summary>
		/// 送货坐标，纬度
		/// </summary>
		public string DeliveryLatitude
		{
			set{ _deliverylatitude=value;}
			get{return _deliverylatitude;}
		}
		/// <summary>
		/// 收货联系人
		/// </summary>
		public string Contact
		{
			set{ _contact=value;}
			get{return _contact;}
		}
		/// <summary>
		/// 联系人电话
		/// </summary>
		public string ContactPhone
		{
			set{ _contactphone=value;}
			get{return _contactphone;}
		}
		/// <summary>
		/// 批核人
		/// </summary>
		public string Approvedby
		{
			set{ _approvedby=value;}
			get{return _approvedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ApprovalCode
		{
			set{ _approvalcode=value;}
			get{return _approvalcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SalesReceipt
		{
			set{ _salesreceipt=value;}
			get{return _salesreceipt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public byte[] SalesReceiptBIN
		{
			set{ _salesreceiptbin=value;}
			get{return _salesreceiptbin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BrandCode
		{
			set{ _brandcode=value;}
			get{return _brandcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SalesTag
		{
			set{ _salestag=value;}
			get{return _salestag;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Additional
		{
			set{ _additional=value;}
			get{return _additional;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LogisticsProviderID
		{
			set{ _logisticsproviderid=value;}
			get{return _logisticsproviderid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? PaymentDoneOn
		{
			set{ _paymentdoneon=value;}
			get{return _paymentdoneon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DeliveryNumber
		{
			set{ _deliverynumber=value;}
			get{return _deliverynumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PickupType
		{
			set{ _pickuptype=value;}
			get{return _pickuptype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PickupStoreCode
		{
			set{ _pickupstorecode=value;}
			get{return _pickupstorecode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CODFlag
		{
			set{ _codflag=value;}
			get{return _codflag;}
		}
		#endregion Model

	}
}

