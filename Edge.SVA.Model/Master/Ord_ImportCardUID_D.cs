﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 导入Card的UID的 子表。
	/// </summary>
	[Serializable]
	public partial class Ord_ImportCardUID_D
	{
		public Ord_ImportCardUID_D()
		{}
		#region Model
		private int _keyid;
		private string _importcardnumber;
		private int? _cardgradeid;
		private string _carduid;
		private DateTime? _expirydate;
		private string _batchcode;
		private decimal? _denomination;
		/// <summary>
		/// 主键，自增长
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 单据号码
		/// </summary>
		public string ImportCardNumber
		{
			set{ _importcardnumber=value;}
			get{return _importcardnumber;}
		}
		/// <summary>
		/// 卡等级ID
		/// </summary>
		public int? CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 实体卡ID 
		/// </summary>
		public string CardUID
		{
			set{ _carduid=value;}
			get{return _carduid;}
		}
		/// <summary>
		/// 特定设置有效期。null表示使用coupon自己的有效期。否则使用此处的值更新到coupon
		/// </summary>
		public DateTime? ExpiryDate
		{
			set{ _expirydate=value;}
			get{return _expirydate;}
		}
		/// <summary>
		/// 导入的BatchCode，如果有值，则使用这个值update到绑定的Card的BatchCard->BatchCardCode中
		/// </summary>
		public string BatchCode
		{
			set{ _batchcode=value;}
			get{return _batchcode;}
		}
		/// <summary>
		/// 如果填写了值，即为card初始金额，填写TotalAmount
		/// </summary>
		public decimal? Denomination
		{
			set{ _denomination=value;}
			get{return _denomination;}
		}
		#endregion Model

	}
}

