﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 批次表
	/// </summary>
	[Serializable]
	public partial class BatchActive
	{
		public BatchActive()
		{}
		#region Model
		private string _number;
		private string _cardtype;
		private string _batchid;
		private decimal _cardamount;
		private decimal _totalamount;
		private int _cardcount;
		private DateTime? _createdon;
		private string _createdby;
		private string _remark;
		private string _serverid;
		private string _shopid;
		private string _posid;
		private string _txnno;
		private DateTime? _busdate;
		private DateTime? _txndate;
		/// <summary>
		/// 
		/// </summary>
		public string Number
		{
			set{ _number=value;}
			get{return _number;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardType
		{
			set{ _cardtype=value;}
			get{return _cardtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BatchID
		{
			set{ _batchid=value;}
			get{return _batchid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal CardAmount
		{
			set{ _cardamount=value;}
			get{return _cardamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal TotalAmount
		{
			set{ _totalamount=value;}
			get{return _totalamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int CardCount
		{
			set{ _cardcount=value;}
			get{return _cardcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ServerID
		{
			set{ _serverid=value;}
			get{return _serverid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ShopID
		{
			set{ _shopid=value;}
			get{return _shopid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POSID
		{
			set{ _posid=value;}
			get{return _posid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TxnNo
		{
			set{ _txnno=value;}
			get{return _txnno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? BusDate
		{
			set{ _busdate=value;}
			get{return _busdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? TxnDate
		{
			set{ _txndate=value;}
			get{return _txndate;}
		}
		#endregion Model

	}
}

