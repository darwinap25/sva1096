﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡金额记录明细表。（记录每一笔增加的记录，以及每一笔的余额）
	/// </summary>
	[Serializable]
	public partial class CardCashDetail
	{
		public CardCashDetail()
		{}
		#region Model
		private int _keyid;
		private string _cardnumber;
		private int _cardtypeid;
		private string _tendercode;
		private decimal? _tenderrate=1M;
		private decimal _addamount;
		private decimal? _balanceamount;
		private decimal? _forfeitamount;
		private DateTime? _expirydate;
		private int _status=1;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private string _updatedby;
		/// <summary>
		/// 
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 卡号
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 充值支付用的Tender（预留字段）
		/// </summary>
		public string TenderCode
		{
			set{ _tendercode=value;}
			get{return _tendercode;}
		}
		/// <summary>
		/// 充值支付用的Tender的汇率（预留字段）
		/// </summary>
		public decimal? TenderRate
		{
			set{ _tenderrate=value;}
			get{return _tenderrate;}
		}
		/// <summary>
		/// 充值金额
		/// </summary>
		public decimal AddAmount
		{
			set{ _addamount=value;}
			get{return _addamount;}
		}
		/// <summary>
		/// 余额
		/// </summary>
		public decimal? BalanceAmount
		{
			set{ _balanceamount=value;}
			get{return _balanceamount;}
		}
		/// <summary>
		/// 丢弃金额
		/// </summary>
		public decimal? ForfeitAmount
		{
			set{ _forfeitamount=value;}
			get{return _forfeitamount;}
		}
		/// <summary>
		/// 有效期
		/// </summary>
		public DateTime? ExpiryDate
		{
			set{ _expirydate=value;}
			get{return _expirydate;}
		}
		/// <summary>
		/// 状态。0：无效。1：有效
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

