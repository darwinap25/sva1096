﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 颜色表
	/// </summary>
	[Serializable]
	public partial class Color
	{
		public Color()
		{}
		#region Model
		private int _colorid;
		private string _colorcode;
		private string _colorname1;
		private string _colorname2;
		private string _colorname3;
		private string _colorpicfile;
		/// <summary>
		///11 主键ID
		/// </summary>
		public int ColorID
		{
			set{ _colorid=value;}
			get{return _colorid;}
		}
		/// <summary>
		///11 RGB编号。 e.g. FFFFFF
		/// </summary>
		public string ColorCode
		{
			set{ _colorcode=value;}
			get{return _colorcode;}
		}
		/// <summary>
		///11 颜色名称1
		/// </summary>
		public string ColorName1
		{
			set{ _colorname1=value;}
			get{return _colorname1;}
		}
		/// <summary>
		///11 颜色名称2
		/// </summary>
		public string ColorName2
		{
			set{ _colorname2=value;}
			get{return _colorname2;}
		}
		/// <summary>
		///11 颜色名称3
		/// </summary>
		public string ColorName3
		{
			set{ _colorname3=value;}
			get{return _colorname3;}
		}
		/// <summary>
		///11 颜色图片文件
		/// </summary>
		public string ColorPicFile
		{
			set{ _colorpicfile=value;}
			get{return _colorpicfile;}
		}
		#endregion Model

	}
}

