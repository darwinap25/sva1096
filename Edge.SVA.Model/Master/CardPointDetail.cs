﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 卡积分记录明细表。（记录每一笔增加的记录，以及每一笔的余额）
	/// </summary>
	[Serializable]
	public partial class CardPointDetail
	{
		public CardPointDetail()
		{}
		#region Model
		private int _keyid;
		private string _cardnumber;
		private int _cardtypeid;
		private int _addpoint;
		private int? _balancepoint;
		private int? _forfeitpoint;
		private DateTime? _expirydate;
		private int _status=1;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private string _updatedby;
		/// <summary>
		/// 自增长主键
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 卡号
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 卡类型ID
		/// </summary>
		public int CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 增加的积分值。
		/// </summary>
		public int AddPoint
		{
			set{ _addpoint=value;}
			get{return _addpoint;}
		}
		/// <summary>
		/// 积分余额
		/// </summary>
		public int? BalancePoint
		{
			set{ _balancepoint=value;}
			get{return _balancepoint;}
		}
		/// <summary>
		/// 被丢弃的积分。
		/// </summary>
		public int? ForfeitPoint
		{
			set{ _forfeitpoint=value;}
			get{return _forfeitpoint;}
		}
		/// <summary>
		/// 单笔积分有效期。
		/// </summary>
		public DateTime? ExpiryDate
		{
			set{ _expirydate=value;}
			get{return _expirydate;}
		}
		/// <summary>
		/// 状态。0：积分无效。 1：有效。
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

