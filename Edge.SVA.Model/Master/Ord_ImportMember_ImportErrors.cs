﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Master
{
    [Serializable]
    public partial class Ord_ImportMember_ImportErrors
    {
        public Ord_ImportMember_ImportErrors()
        { }
        #region Model
        private string _importmembernumber;
        private string _rrccardnumber;
        private string _rrccustomertiercode;
        private string _rrcbusinessowner;
        private string _rrcemailsubsciption;
        private string _rrcsmssubscription;

        public string ImportMemberNumber
        {
            set { _importmembernumber = value; }
            get { return _importmembernumber; }
        }

        public string RRC_CardNumber
        {
            set { _rrccardnumber = value; }
            get { return _rrccardnumber; }
        }

        public string RRC_CustomerTierCode
        {
            set { _rrccustomertiercode = value; }
            get { return _rrccustomertiercode; }
        }

        public string RRC_BusinessOwner
        {
            set { _rrcbusinessowner = value; }
            get { return _rrcbusinessowner; }
        }

        public string RRC_EmailSubscription
        {
            set { _rrcemailsubsciption = value; }
            get { return _rrcemailsubsciption; }
        }

        public string RRC_SMSSubscription
        {
            set { _rrcsmssubscription = value; }
            get { return _rrcsmssubscription; }
        }

        

        #endregion
    }
}
