﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Campaign和CardGrade的关系表。 联合主键
	/// </summary>
	[Serializable]
	public partial class CampaignCardGrade
	{
		public CampaignCardGrade()
		{}
		#region Model
		private int _campaignid;
		private int _cardgradeid;
		/// <summary>
		/// 活动ID
		/// </summary>
		public int CampaignID
		{
			set{ _campaignid=value;}
			get{return _campaignid;}
		}
		/// <summary>
		/// 卡级别ID
		/// </summary>
		public int CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		#endregion Model

	}
}

