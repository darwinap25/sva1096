﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// Coupon自动补货规则设置表. 
    ///根据设置，自动创建order单。 （可能直接批核，可能不是。）
    ///需要设定storetypeid， 根据storetypeid， 子表中的Store 必须根据此storetype 来过滤。
    ///注：自动补货规则包含两种，根据发起的店铺类型不同，产生不同的order。   店铺的只能给后台， 后台的只能给供应商。 两种不同的订单。
    /// </summary>
    [Serializable]
    public partial class CouponReplenishRule_H
    {
        public CouponReplenishRule_H()
        { }
        #region Model
        private string _couponreplenishcode;
        private string _description;
        private DateTime? _startdate;
        private DateTime? _enddate;
        private int? _status = 1;
        private int? _brandid;
        private int _coupontypeid;
        private int _storetypeid;
        private int? _autocreateorder = 1;
        private int? _autoapproveorder = 0;
        private DateTime? _createdon = DateTime.Now;
        private DateTime? _updatedon = DateTime.Now;
        private int? _createdby;
        private int? _updatedby;
        private int? _DayFlagID;
        private int? _WeekFlagID;
        private int? _MonthFlagID;
        private DateTime? _ActiveTime;
        /// <summary>
        /// 优惠劵自动补货规则编码
        /// </summary>
        public string CouponReplenishCode
        {
            set { _couponreplenishcode = value; }
            get { return _couponreplenishcode; }
        }
        /// <summary>
        /// 规则描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime? StartDate
        {
            set { _startdate = value; }
            get { return _startdate; }
        }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? EndDate
        {
            set { _enddate = value; }
            get { return _enddate; }
        }
        /// <summary>
        /// 是否生效。 0：无效，1：生效。 默认生效
        /// </summary>
        public int? Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// Coupon的品牌ID
        /// </summary>
        public int? BrandID
        {
            set { _brandid = value; }
            get { return _brandid; }
        }
        /// <summary>
        /// 根据brandid选择 CouponType ID
        /// </summary>
        public int CouponTypeID
        {
            set { _coupontypeid = value; }
            get { return _coupontypeid; }
        }
        /// <summary>
        /// 店铺类型ID。1：总部。 2：店铺

        /// </summary>
        public int StoreTypeID
        {
            set { _storetypeid = value; }
            get { return _storetypeid; }
        }
        /// <summary>
        /// 是否自动创建订单
        /// </summary>
        public int? AutoCreateOrder
        {
            set { _autocreateorder = value; }
            get { return _autocreateorder; }
        }
        /// <summary>
        /// 自动创建的订单，是否直接批核
        /// </summary>
        public int? AutoApproveOrder
        {
            set { _autoapproveorder = value; }
            get { return _autoapproveorder; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn
        {
            set { _createdon = value; }
            get { return _createdon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdatedOn
        {
            set { _updatedon = value; }
            get { return _updatedon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        /// </summary>
        public int? DayFlagID
        {
            set { _DayFlagID = value; }
            get { return _DayFlagID; }
        }
        /// </summary>
        public int? WeekFlagID
        {
            set { _WeekFlagID = value; }
            get { return _WeekFlagID; }
        }
        /// </summary>
        public int? MonthFlagID
        {
            set { _MonthFlagID = value; }
            get { return _MonthFlagID; }
        }
        /// </summary>
        public DateTime? ActiveTime
        {
            set { _ActiveTime = value; }
            get { return _ActiveTime; }
        }
        #endregion Model

    }
}

