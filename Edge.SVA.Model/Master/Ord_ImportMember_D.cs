﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Ord_ImportMember_H的子表。 当导入Member数据时（MII），那么这个子表不需要填充。
	///当导入Card数据时（CNC），需要保存导入的数据。
	///
	/// </summary>
	[Serializable]
	public partial class Ord_ImportMember_D
	{
		public Ord_ImportMember_D()
		{}
		#region Model
		private int _keyid;
		private string _importmembernumber;
		private string _rrccardnumber;
		private string _oldcardnumber;
		private string _oldrrccardnumber;
		private string _lname;
		private string _fname;
		private string _vppts;
		private string _retailid;
		private string _storeid;
		private int? _cardstatus;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 单据号，主键
		/// </summary>
		public string ImportMemberNumber
		{
			set{ _importmembernumber=value;}
			get{return _importmembernumber;}
		}
		/// <summary>
		/// 新的卡号
		/// </summary>
		public string RRCCardNumber
		{
			set{ _rrccardnumber=value;}
			get{return _rrccardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OLDCardNumber
		{
			set{ _oldcardnumber=value;}
			get{return _oldcardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OLDRRCCardNumber
		{
			set{ _oldrrccardnumber=value;}
			get{return _oldrrccardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LName
		{
			set{ _lname=value;}
			get{return _lname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FName
		{
			set{ _fname=value;}
			get{return _fname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string VPPTS
		{
			set{ _vppts=value;}
			get{return _vppts;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RetailID
		{
			set{ _retailid=value;}
			get{return _retailid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardStatus
		{
			set{ _cardstatus=value;}
			get{return _cardstatus;}
		}
		#endregion Model

	}
}

