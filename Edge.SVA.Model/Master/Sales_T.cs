﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// ?úê?μ￥×ó±í￡¨?§????μ￥±í￡?
	/// </summary>
	[Serializable]
	public partial class Sales_T
	{
		public Sales_T()
		{}
		#region Model
		private int _keyid;
		private string _txnno;
		private int _seqno;
		private int _tenderid;
		private string _tendercode;
		private string _tendername;
		private decimal? _tenderamount;
		private decimal? _localamount;
		private decimal? _exchangerate=1M;
		private string _additional;
		private DateTime? _createdon= DateTime.Now;
		private string _createdby;
		private DateTime? _updatedon= DateTime.Now;
		private string _updatedby;
		/// <summary>
		///11 ×???3¤?÷?ü
		/// </summary>
		public int KeyID
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		///11 ??ò×μ￥o?￡¨?÷?ü￡?
		/// </summary>
		public string TxnNo
		{
			set{ _txnno=value;}
			get{return _txnno;}
		}
		/// <summary>
		///11 ??ò×μ￥???·Dòo?
		/// </summary>
		public int SeqNo
		{
			set{ _seqno=value;}
			get{return _seqno;}
		}
		/// <summary>
		///11 Tender?÷?ü
		/// </summary>
		public int TenderID
		{
			set{ _tenderid=value;}
			get{return _tenderid;}
		}
		/// <summary>
		///11 ?§??ààDí±à??
		/// </summary>
		public string TenderCode
		{
			set{ _tendercode=value;}
			get{return _tendercode;}
		}
		/// <summary>
		///11 ?§??ààDí??3?
		/// </summary>
		public string TenderName
		{
			set{ _tendername=value;}
			get{return _tendername;}
		}
		/// <summary>
		///11 ?§???e??
		/// </summary>
		public decimal? TenderAmount
		{
			set{ _tenderamount=value;}
			get{return _tenderamount;}
		}
		/// <summary>
		///11 ?§??±?±ò?e?? ￡¨LocalAmount=TenderAmount*ExchangeRate￡?
		/// </summary>
		public decimal? LocalAmount
		{
			set{ _localamount=value;}
			get{return _localamount;}
		}
		/// <summary>
		///11 ??±òoí±?±òμ????ê
		/// </summary>
		public decimal? ExchangeRate
		{
			set{ _exchangerate=value;}
			get{return _exchangerate;}
		}
		/// <summary>
		///11 ???óD??￠
		/// </summary>
		public string Additional
		{
			set{ _additional=value;}
			get{return _additional;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		///11 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		///11 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

