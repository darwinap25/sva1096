﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 优惠劵表
	/// </summary>
	[Serializable]
	public partial class Coupon
	{
		public Coupon()
		{}
		#region Model
		private string _couponnumber;
		private int _coupontypeid;
		private DateTime _couponissuedate;
		private DateTime _couponexpirydate;
		private DateTime? _couponactivedate;
		private int? _storeid;
		private int? _batchcouponid;
		private int _status=0;
		private string _couponpassword;
		private string _cardnumber;
		private DateTime _createdon= DateTime.Now;
		private DateTime? _updatedon= DateTime.Now;
		private int? _createdby;
		private int? _updatedby;
		private decimal? _couponamount;
		private int? _redeemstoreid;
		private int? _pickupflag=0;
		private int? _locatestoreid;
		private string _initcardnumber;
		private int? _stockstatus;
		/// <summary>
		/// 优惠劵号码
		/// </summary>
		public string CouponNumber
		{
			set{ _couponnumber=value;}
			get{return _couponnumber;}
		}
		/// <summary>
		/// 优惠劵类型ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		/// <summary>
		/// 优惠券发行日期
		/// </summary>
		public DateTime CouponIssueDate
		{
			set{ _couponissuedate=value;}
			get{return _couponissuedate;}
		}
		/// <summary>
		/// 优惠券过期日期
		/// </summary>
		public DateTime CouponExpiryDate
		{
			set{ _couponexpirydate=value;}
			get{return _couponexpirydate;}
		}
		/// <summary>
		/// 优惠劵激活日期
		/// </summary>
		public DateTime? CouponActiveDate
		{
			set{ _couponactivedate=value;}
			get{return _couponactivedate;}
		}
		/// <summary>
		/// 领取优惠劵的店铺代码
		/// </summary>
		public int? StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		/// <summary>
		/// 创建时的批次号
		/// </summary>
		public int? BatchCouponID
		{
			set{ _batchcouponid=value;}
			get{return _batchcouponid;}
		}
		/// <summary>
		/// 优惠劵状态。0：未被领用。 1：已被领取（激活）。 2：已被使用。 3：过期。 4：作废。
		/// </summary>
		public int Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 优惠劵密码。 存储内容为 MD5加密的密文。
		/// </summary>
		public string CouponPassword
		{
			set{ _couponpassword=value;}
			get{return _couponpassword;}
		}
		/// <summary>
		/// 主卡卡号
		/// </summary>
		public string CardNumber
		{
			set{ _cardnumber=value;}
			get{return _cardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CreatedOn
		{
			set{ _createdon=value;}
			get{return _createdon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedOn
		{
			set{ _updatedon=value;}
			get{return _updatedon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? CouponAmount
		{
			set{ _couponamount=value;}
			get{return _couponamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RedeemStoreID
		{
			set{ _redeemstoreid=value;}
			get{return _redeemstoreid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PickupFlag
		{
			set{ _pickupflag=value;}
			get{return _pickupflag;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LocateStoreID
		{
			set{ _locatestoreid=value;}
			get{return _locatestoreid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string InitCardNumber
		{
			set{ _initcardnumber=value;}
			get{return _initcardnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? StockStatus
		{
			set{ _stockstatus=value;}
			get{return _stockstatus;}
		}
		#endregion Model

	}
}

