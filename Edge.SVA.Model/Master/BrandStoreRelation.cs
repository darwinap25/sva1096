﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 品牌店铺关系表
	/// </summary>
	[Serializable]
	public partial class BrandStoreRelation
	{
		public BrandStoreRelation()
		{}
		#region Model
		private int _brandstorerelationid;
		private int? _brandid;
		private int? _storeid;
		/// <summary>
		/// 主键ID
		/// </summary>
		public int BrandStoreRelationID
		{
			set{ _brandstorerelationid=value;}
			get{return _brandstorerelationid;}
		}
		/// <summary>
		/// 品牌表，外键
		/// </summary>
		public int? BrandID
		{
			set{ _brandid=value;}
			get{return _brandid;}
		}
		/// <summary>
		/// 店铺表，外键
		/// </summary>
		public int? StoreID
		{
			set{ _storeid=value;}
			get{return _storeid;}
		}
		#endregion Model

	}
}

