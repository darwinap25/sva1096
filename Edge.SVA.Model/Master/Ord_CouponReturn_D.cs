﻿using System;
namespace Edge.SVA.Model
{
    /// <summary>
    /// t退货单子表
    ///
    /// </summary>
    [Serializable]
    public partial class Ord_CouponReturn_D
    {
        public Ord_CouponReturn_D()
        { }
        #region Model
        private int _keyid;
        private string _couponreturnnumber;
        private int? _coupontypeid;
        private string _description;
        private int? _orderqty;
        private int? _actualqty;
        private string _firstcouponnumber;
        private string _endcouponnumber;
        private string _batchcouponcode;
        /// <summary>
        /// 
        /// </summary>
        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }
        /// <summary>
        /// 订单编号，主键
        /// </summary>
        public string CouponReturnNumber
        {
            set { _couponreturnnumber = value; }
            get { return _couponreturnnumber; }
        }
        /// <summary>
        /// 优惠劵类型ID
        /// </summary>
        public int? CouponTypeID
        {
            set { _coupontypeid = value; }
            get { return _coupontypeid; }
        }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 订货的订单数量
        /// </summary>
        public int? OrderQTY
        {
            set { _orderqty = value; }
            get { return _orderqty; }
        }
        /// <summary>
        /// 实际收到的数量
        /// </summary>
        public int? ActualQTY
        {
            set { _actualqty = value; }
            get { return _actualqty; }
        }
        /// <summary>
        /// 实际收货批次的首coupon号
        /// </summary>
        public string FirstCouponNumber
        {
            set { _firstcouponnumber = value; }
            get { return _firstcouponnumber; }
        }
        /// <summary>
        /// 实际收货批次的末coupon号
        /// </summary>
        public string EndCouponNumber
        {
            set { _endcouponnumber = value; }
            get { return _endcouponnumber; }
        }
        /// <summary>
        /// FirstCouponNumber的批次号编码
        /// </summary>
        public string BatchCouponCode
        {
            set { _batchcouponcode = value; }
            get { return _batchcouponcode; }
        }
        #endregion Model

    }
}

