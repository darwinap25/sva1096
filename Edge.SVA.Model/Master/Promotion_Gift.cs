﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// 促销礼品表。
	/// </summary>
	[Serializable]
	public partial class Promotion_Gift
	{
		public Promotion_Gift()
		{}
		#region Model
		private string _promotioncode;
		private int _giftseq;
		private int? _promotiongifttype=1;
		private decimal? _promotionvalue;
		private decimal? _promotionadjvalue=0M;
		/// <summary>
		///11 促销编码
		/// </summary>
		public string PromotionCode
		{
			set{ _promotioncode=value;}
			get{return _promotioncode;}
		}
		/// <summary>
		///11 序号
		/// </summary>
		public int GiftSeq
		{
			set{ _giftseq=value;}
			get{return _giftseq;}
		}
		/// <summary>
		///11 促销实现方式.. 默认1
		///1：现金返还 (固定金额)
		///2：现金返还 (消费金额的百分数)  例如  0.1.  消费1000， 返还100
		///3：积分返还 （固定值）
		///4：积分返还 （消费金额的系数）。 例如 0.1.   消费20， 返还2 分 
		///5：赠送Coupon。
        ///以上的解释是不对的
        ///1. 金額返還   2. 積分返還   3. 優惠券贈送  4. 根據消費金額返還指定比例積分  5. 根據消費金額返還指定比例金額  
		/// </summary>
		public int? PromotionGiftType
		{
			set{ _promotiongifttype=value;}
			get{return _promotiongifttype;}
		}
		/// <summary>
		///11 促销内容值。（根据PromotionGiftType 中设置，决定内容。）
		///PromotionGiftType = 5 时， 存放CouponTypeID
		/// </summary>
		public decimal? PromotionValue
		{
			set{ _promotionvalue=value;}
			get{return _promotionvalue;}
		}
		/// <summary>
		///11 促销值的调整值。 应对以下情况：
		///满20￥开始，每10￥加送1 分。  即： 20￥ 1分、30￥ 2分、40￥ 3分......
		///此时，这里可以填写   -1。  按照线性规则计算得出的积分 - 1 后就是能获得的促销。
		/// </summary>
		public decimal? PromotionAdjValue
		{
			set{ _promotionadjvalue=value;}
			get{return _promotionadjvalue;}
		}
		#endregion Model

	}
}

