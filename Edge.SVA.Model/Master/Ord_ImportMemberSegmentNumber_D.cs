﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edge.SVA.Model.Master
{
    [Serializable]
    public partial class Ord_ImportMemberSegmentNumber_D
    {
        public Ord_ImportMemberSegmentNumber_D()
        { }

        private int _keyid;
        private int _rrc_retailerid;
        private int _segmentid;
        private int _buyingunitinternalkey;
        private string _rrc_cadnumber;


        public int KeyID
        {
            set { _keyid = value; }
            get { return _keyid; }
        }

        public int RRC_RetailerID
        {
            set { _rrc_retailerid = value; }
            get { return _rrc_retailerid; }
        }

        public int SegmentID
        {
            set { _segmentid = value; }
            get { return _segmentid; }
        }

        public int BuyingUnitInternaKey
        {
            set { _buyingunitinternalkey = value; }
            get { return _buyingunitinternalkey; }
        }

        public string RRC_CardNumber
        {
            set { _rrc_cadnumber = value; }
            get { return _rrc_cadnumber; }
        }

    }
}
