﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Card操作的流水表。
	///通过insert触发器，增加此表记录来操作Card表，CardCashDetail表。
	/// </summary>
	[Serializable]
	public partial class CardAutoAddValueRule
	{
		public CardAutoAddValueRule()
		{}
		#region Model
		private Guid _autoaddruleid;
		private string _cardtypeid;
		private string _cardgradeid;
		private int? _autoaddruleseqno;
		private string _ruledesc;
		private int _addamount;
		private DateTime? _startdate;
		private DateTime? _enddate;
		private int? _interval=1;
		private int? _intervalunit=1;
		private int? _dayofinterval;
		private DateTime? _lastexecdate= DateTime.Now;
		private int? _executedcount;
		private int? _justforbirth=0;
		private DateTime? _updateddate= DateTime.Now;
		private DateTime? _createddate= DateTime.Now;
		private string _createdby;
		private string _updatedby;
		/// <summary>
		/// 
		/// </summary>
		public Guid AutoAddRuleID
		{
			set{ _autoaddruleid=value;}
			get{return _autoaddruleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardTypeID
		{
			set{ _cardtypeid=value;}
			get{return _cardtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardGradeID
		{
			set{ _cardgradeid=value;}
			get{return _cardgradeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AutoAddRuleSeqNo
		{
			set{ _autoaddruleseqno=value;}
			get{return _autoaddruleseqno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RuleDesc
		{
			set{ _ruledesc=value;}
			get{return _ruledesc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int AddAmount
		{
			set{ _addamount=value;}
			get{return _addamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? StartDate
		{
			set{ _startdate=value;}
			get{return _startdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? EndDate
		{
			set{ _enddate=value;}
			get{return _enddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Interval
		{
			set{ _interval=value;}
			get{return _interval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IntervalUnit
		{
			set{ _intervalunit=value;}
			get{return _intervalunit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DayOfInterval
		{
			set{ _dayofinterval=value;}
			get{return _dayofinterval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastExecDate
		{
			set{ _lastexecdate=value;}
			get{return _lastexecdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ExecutedCount
		{
			set{ _executedcount=value;}
			get{return _executedcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? JustForBirth
		{
			set{ _justforbirth=value;}
			get{return _justforbirth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UpdatedDate
		{
			set{ _updateddate=value;}
			get{return _updateddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatedDate
		{
			set{ _createddate=value;}
			get{return _createddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreatedBy
		{
			set{ _createdby=value;}
			get{return _createdby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdatedBy
		{
			set{ _updatedby=value;}
			get{return _updatedby;}
		}
		#endregion Model

	}
}

