﻿using System;
namespace Edge.SVA.Model
{
	/// <summary>
	/// Campaign和CouponType的关系表。 联合主键
	/// </summary>
	[Serializable]
	public partial class CampaignCouponType
	{
		public CampaignCouponType()
		{}
		#region Model
		private int _campaignid;
		private int _coupontypeid;
		/// <summary>
		/// 活动ID
		/// </summary>
		public int CampaignID
		{
			set{ _campaignid=value;}
			get{return _campaignid;}
		}
		/// <summary>
		/// Coupon Type ID
		/// </summary>
		public int CouponTypeID
		{
			set{ _coupontypeid=value;}
			get{return _coupontypeid;}
		}
		#endregion Model

	}
}

